<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type='text/javascript' src='<s:url value="/dwr/interface/UserAction.js"/>'></script>
</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:url id="urlProcess" namespace="/admin/usersessionlog" action="search_usersessionlog" includeContext="false"/>
                                <s:form id="userSessionLogForm" method="post" action="%{urlProcess}" cssClass="well form-horizontal">

                                <fieldset>
                                    <legend align="left">User Session Log Information</legend>

                                    <table>
                                        <tr>
                                            <td width="10%" align="center">
                                                <%@ include file="/pages/common/message.jsp" %>
                                            </td>
                                        </tr>
                                    </table>

                                    <table>
                                        <tr>
                                            <td>
                                                <label class="control-label" for="userSessionLog.flag">Status Session :</label>
                                            </td>

                                            <td>
                                                <table>
                                                    <s:select list="#{'N':'Non-Active'}" id="flag" name="userSessionLog.flag"
                                                              headerKey="Y" headerValue="Active" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="control-label" for="userSessionLog.userName">User :</label>
                                            </td>

                                            <td>
                                                <table>
                                                    <s:textfield id="userId" name="userSessionLog.userName" required="false" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                    <script>
                                                        var functions, mapped;
                                                        $('#userId').typeahead({
                                                            minLength: 2,
                                                            source: function (query, process) {
                                                                functions = [];
                                                                mapped = {};

                                                                var data = [];
                                                                dwr.engine.setAsync(false);
                                                                UserAction.initComboUser(query, function (listdata) {
                                                                    data = listdata;
                                                                });

                                                                $.each(data, function (i, item) {
                                                                    var labelItem = item.userId + '-' + item.username;
                                                                    mapped[labelItem] = { id: item.userId, label: labelItem };
                                                                    functions.push(labelItem);
                                                                });

                                                                process(functions);
                                                            },
                                                            updater: function (item) {
                                                                var selectedObj = mapped[item];
                                                                return selectedObj.id;
                                                            }
                                                        });
                                                    </script>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <table width="100%" align="left">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <label class="control-label" for="userSessionLog.stLoginTimestampFrom"> Login Time : </label>
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <sj:datepicker id="loginTimestampFrom" name="userSessionLog.stLoginTimestampFrom" displayFormat="dd-mm-yy"
                                                                                           showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />

                                                                        </table>
                                                                    </td>

                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr align="left">
                                                                    <td>
                                                                        <label class="control-label" for="userSessionLog.stLoginTimestampTo"> To : </label>
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <sj:datepicker id="loginTimestampTo" name="userSessionLog.stLoginTimestampTo" displayFormat="dd-mm-yy"
                                                                                           showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small"/>

                                                                        </table>

                                                                    </td>
                                                                </tr>

                                                            </table>
                                                        </td>

                                                    </tr>
                                                </table>

                                            </td>

                                        </tr>
                                    </table>

                                </fieldset>


                                <div id="actions" class="form-actions">
                                    <table>
                                        <tr>
                                            <td>
                                                <table>

                                                    <sj:dialog id="waiting_dialog_search" openTopics="showDialogSearch" closeTopics="closeDialogSearch" modal="true"
                                                               resizable="false" position="center" height="250" width="600" autoOpen="false" title="Searching...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                    </sj:dialog>
                                                    <sj:submit type="button" cssClass="btn btn-primary" formIds="userSessionLogForm" id="search" name="search"
                                                               onClickTopics="showDialogSearch" onCompleteTopics="closeDialogSearch">
                                                        <i class="icon-search icon-white"></i>
                                                        Search
                                                    </sj:submit>

                                                </table>
                                            </td>

                                            <td>
                                                <table>
                                                    <button type="button" class="btn" onclick="window.location.href='<s:url action="initForm_usersessionlog"/>'">
                                                        <i class="icon-repeat"></i> Reset
                                                    </button>
                                                </table>
                                            </td>

                                        </tr>
                                    </table>
                                </div>


                                <table width="100%">
                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="view_dialog" openTopics="showDialog" modal="true" resizable="false" cssStyle="text-align:left;"
                                                       position="center" height="500" width="700" autoOpen="false" title="Kill Session"
                                                    >
                                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                            </sj:dialog>

                                            <s:set name="listOfUserSessionLog" value="#session.listOfResult" scope="request"/>
                                            <display:table name="listOfUserSessionLog" class="table table-condensed table-striped table-hover"
                                                           requestURI="paging_displaytag.action" id="row" pagesize="30" export="true" style="font-size:11">

                                                <display:column media="html" title="<small>Kill Session</small>" style="text-align:center;font-size:11">
                                                    <s:if test = "%{#attr.row.enabledKill}" >
                                                        <s:url var="urlViewKill" namespace="/admin/usersessionlog" action="view_usersessionlog"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.stId" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialog" href="%{urlViewKill}">
                                                            <img border="0" src="<s:url value="/pages/images/kill_session.ico"/>" name="icon_kill">
                                                        </sj:a>
                                                    </s:if>
                                                    <s:else>
                                                        <img border="0" src="<s:url value="/pages/images/icon_trash_error.ico"/>" name="icon_error">
                                                    </s:else>
                                                </display:column>

                                                <display:column property="stId" sortable="true" title="Id"/>
                                                <display:column property="sessionId" sortable="true" title="Session.Id"/>
                                                <display:column property="userName" sortable="true" title="User"/>
                                                <display:column property="companyName" sortable="true" title="Company"/>
                                                <display:column property="branchName" sortable="true" title="Branch"/>
                                                <display:column property="areaName" sortable="true" title="Area"/>
                                                <display:column property="ipAddress" sortable="true" title="Ip.Address"/>
                                                <display:column property="loginTimestamp" sortable="true" title="Login Time"
                                                                decorator="com.neurix.common.displaytag.LongDateWrapper"/>
                                                <display:column property="logoutTimestamp" sortable="true" title="Logout Time"
                                                                decorator="com.neurix.common.displaytag.LongDateWrapper"/>
                                                <display:setProperty name="paging.banner.item_name">UserSessionLog</display:setProperty>
                                                <display:setProperty name="paging.banner.items_name">UserSessionLogs</display:setProperty>
                                                <display:setProperty name="export.excel.filename">UserSessionLogs.xls</display:setProperty>
                                                <display:setProperty name="export.csv.filename">UserSessionLogs.csv</display:setProperty>
                                                <display:setProperty name="export.pdf.filename">UserSessionLogs.pdf</display:setProperty>

                                            </display:table>

                                        </td>
                                    </tr>
                                </table>

                                </s:form>

                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>

</body>
</html>


