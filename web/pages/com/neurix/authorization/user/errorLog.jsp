<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
             <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:url id="urlProcess" namespace="/admin/errorlog" action="search_errorlog" includeContext="false"/>
                                <s:form id="errorLogForm" method="post" action="%{urlProcess}" cssClass="well form-horizontal">

                                <fieldset>
                                    <legend align="left">Error Log Information</legend>

                                    <table>
                                        <tr>
                                            <td width="10%" align="center">
                                                <%@ include file="/pages/common/message.jsp" %>
                                            </td>
                                        </tr>
                                    </table>



                                    <table>
                                        <tr>
                                            <td>
                                                <label class="control-label" for="errorLog.errorId">Error Id :</label>
                                            </td>

                                            <td>
                                                <table>
                                                    <s:textfield id="errorId" name="errorLog.errorId" required="false"/>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="control-label" for="errorLog.moduleMethod">Module Method :</label>
                                            </td>

                                            <td>
                                                <table>
                                                    <s:textfield id="method" name="errorLog.moduleMethod" required="false"/>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="control-label" for="errorLog.message">Message :</label>
                                            </td>

                                            <td>
                                                <table>
                                                    <s:textfield id="message" name="errorLog.message" required="false"/>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="control-label" for="errorLog.userId">User :</label>
                                            </td>

                                            <td>
                                                <table>
                                                    <s:textfield id="userId" name="errorLog.userId" required="false"/>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="control-label" for="errorLog.branchId">Unit :</label>
                                            </td>

                                            <td>
                                                <table>
                                                    <s:textfield id="branchId" name="errorLog.branchId" required="false"/>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table>
                                        <tr>
                                            <td>
                                                <table width="100%" align="left">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <label class="control-label" for="errorLog.stErrorTimestampFrom"> Created Date : </label>
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <sj:datepicker id="errorTimestampFrom" name="errorLog.stErrorTimestampFrom" displayFormat="dd-mm-yy"
                                                                                           showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />

                                                                        </table>
                                                                    </td>

                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr align="left">
                                                                    <td>
                                                                        <label class="control-label" for="errorLog.stErrorTimestampTo"> To : </label>
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <sj:datepicker id="errorTimestampTo" name="errorLog.stErrorTimestampTo" displayFormat="dd-mm-yy"
                                                                                           showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small"/>

                                                                        </table>

                                                                    </td>
                                                                </tr>

                                                            </table>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>
                                    </table>

                                </fieldset>


                                <div id="actions" class="form-actions">
                                    <table>
                                        <tr>
                                            <td>
                                                <table>

                                                    <sj:dialog id="waiting_dialog_search" openTopics="showDialogSearch" closeTopics="closeDialogSearch" modal="true"
                                                               resizable="false" position="center" height="250" width="600" autoOpen="false" title="Searching...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                    </sj:dialog>
                                                    <sj:submit type="button" cssClass="btn btn-primary" formIds="errorLogForm" id="search" name="search"
                                                               onClickTopics="showDialogSearch" onCompleteTopics="closeDialogSearch">
                                                        <i class="icon-search icon-white"></i>
                                                        Search
                                                    </sj:submit>

                                                </table>
                                            </td>

                                            <td>
                                                <table>
                                                    <button type="button" class="btn" onclick="window.location.href='<s:url action="initForm_errorlog"/>'">
                                                    <i class="icon-repeat"></i> Reset
                                                    </button>
                                                </table>
                                            </td>

                                        </tr>
                                    </table>
                                </div>


                                <table width="100%">
                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="view_dialog" openTopics="showDialog" modal="true" resizable="false" cssStyle="text-align:left;"
                                                       position="center" height="600" width="500" autoOpen="false" title="View Detail"
                                                    >
                                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                            </sj:dialog>

                                            <s:set name="listOfErrorLog" value="#session.listOfResult" scope="request"/>
                                            <display:table name="listOfErrorLog" class="table table-condensed table-striped table-hover"
                                                           requestURI="paging_displaytag.action" id="row" export="true" pagesize="30" style="font-size:11">

                                                <display:column media="html" title="View">
                                                    <s:url var="urlView" namespace="/admin/errorlog" action="view_errorlog" escapeAmp="false">
                                                        <s:param name="id"><s:property value="#attr.row.errorId"/></s:param>
                                                    </s:url>
                                                    <sj:a onClickTopics="showDialog" href="%{urlView}">
                                                        <img border="0" src="<s:url value="/pages/images/icon_lup.ico"/>" name="icon_lup">
                                                    </sj:a>
                                                </display:column>


                                                <display:column property="errorId" sortable="true" title="Error.Id"/>
                                                <display:column property="moduleMethod" sortable="true" title="Module Method"/>
                                                <display:column property="message" sortable="true" title="Message"/>
                                                <display:column property="userId" sortable="true" title="User"/>
                                                <display:column property="branchId" sortable="true" title="Unit"/>
                                                <display:column property="errorTimestamp" sortable="true" title="CreatedDate"
                                                                decorator="com.neurix.common.displaytag.LongDateWrapper"/>
                                                <display:setProperty name="paging.banner.item_name">ErrorLog</display:setProperty>
                                                <display:setProperty name="paging.banner.items_name">ErrorLogs</display:setProperty>
                                                <display:setProperty name="export.excel.filename">ErrorLogs.xls</display:setProperty>
                                                <display:setProperty name="export.csv.filename">ErrorLogs.csv</display:setProperty>
                                                <display:setProperty name="export.pdf.filename">ErrorLogs.pdf</display:setProperty>

                                            </display:table>

                                        </td>
                                    </tr>
                                </table>

                                </s:form>

                            </div>
                        </td>
                    </tr>
                </table>
             </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>

</body>
</html>


