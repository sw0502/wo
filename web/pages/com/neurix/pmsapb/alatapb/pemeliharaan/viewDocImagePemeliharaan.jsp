<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">

        function closeActivation() {
            $('#view_dialog_ViewProject').dialog('close');
        }


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="aktivasiSKArealForm" method="post" namespace="/project" action="" enctype="multipart/form-data"
                    cssClass="well form-horizontal">

                <fieldset>
                    <%--<legend align="left">Detail Project</legend>--%>

                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>
                    <table>
                            <tr>
                                <td>
                                    <img src="<s:property value="pemeliharaan.filePath"/>" width="600px" onerror="this.src='/pmsapb/pages/images/not_found.gif'"/>
                                    <%--<img src="<s:property value="alatDetail.filePath"/>" width="600px" onerror="this.src='/pmsapb/pages/images/not_found.gif'"/>--%>
                                </td>
                            </tr>
                    </table>

                </fieldset>
                <div id="actions" class="form-actions">
                    <div id="crud">
                        <table>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table>
                    <tr>
                        <td>
                            <table>
                            </table>
                        </td>
                    </tr>
                </table>

            </s:form>

            <table>
            </table>

        </td>
    </tr>
</table>

</body>
</html>


