<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        function closeActivation(){
            $('#view_dialog_catatan').dialog('close');
//            document.alatDetailForm.action='search_alatdetail.action';
//            document.alatDetailForm.submit();
        }

    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="" method="post" namespace="" action=""
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">History Pemeliharaan Alat</legend>

                    <table >
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>

                    </table>


                </fieldset>

                <sj:dialog id="view_dialog_View_doc" openTopics="showDialogViewDoc" modal="true" resizable="false" cssStyle="text-align:left;"
                           position="center" height="600" width="1000" autoOpen="false" title="View Document">
                    <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                </sj:dialog>


                <div style="overflow: scroll">
                    <table width="100%" style="overflow: scroll">
                        <tr>
                            <td align="center">
                                <s:set name="listOfResult" value="#session.listOfResultCatatan" scope="request" />
                                <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="100">

                                    <display:column property="idPemeliharaan" sortable="true" title="<small>No. Catatan</small>" />
                                    <display:column property="idAlatDetail" sortable="true" title="<small>No. Detail Alat</small>" />
                                    <display:column property="catatan" sortable="true" title="<small>Catatan</small>" />
                                    <display:column property="createdDate" sortable="true" title="<small>Created Date</small>" />
                                    <display:column property="createdDateWho" sortable="true" title="<small>Created Date Who</small>" />
                                    <display:column media="html" title="<small>View</small>" style="text-align:center;font-size:9">
                                            <s:url var="urlViewDoc" namespace="/pemeliharaan" action="viewDoc_pemeliharaan"
                                                   escapeAmp="false">
                                                <s:param name="id"><s:property value="#attr.row.urlDoc" /></s:param>
                                            </s:url>
                                            <sj:a onClickTopics="showDialogViewDoc" href="%{urlViewDoc}">
                                                <img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print" width="25px">
                                            </sj:a>
                                    </display:column>
                                    <display:column property="downloadPath" sortable="false" title="<small>Downlaod File</small>"  />

                                    <%--<display:column property="namaAlatDetail" sortable="true" title="<small>Jenis Alat</small>" />--%>
                                    <%--<display:column property="kodeApp" sortable="true" title="<small>Kode APP</small>"  />--%>
                                    <%--<display:column property="namaApp" sortable="true" title="<small>Wilayah APP</small>" />--%>
                                    <%--<display:column property="noGi" sortable="true" title="<small>No. GI</small>" />--%>
                                    <%--<display:column property="namaGi" sortable="true" title="<small>Gardu</small>" />--%>
                                    <%--<display:column property="noGiTujuan" sortable="true" title="<small>No. Gardu Tujuan</small>" />--%>
                                    <%--<display:column property="namaGiTujuan" sortable="true" title="<small>Gardu Tujuan</small>" />--%>
                                    <%--<display:column property="note" sortable="true" title="<small>Catatan</small>" />--%>
                                    <%--<display:column property="lastUpdateWho" sortable="true" title="<small>di Update Oleh</small>" />--%>
                                    <%--<display:column property="lastUpdate" sortable="true" title="<small>di Update Tgl</small>" />--%>

                                    <%--<display:setProperty name="paging.banner.item_name">Project</display:setProperty>--%>
                                    <%--<display:setProperty name="paging.banner.items_name">Project</display:setProperty>--%>
                                    <%--<display:setProperty name="export.excel.filename">AlatDetail.xls</display:setProperty>--%>
                                    <%--<display:setProperty name="export.csv.filename">AlatDetail.csv</display:setProperty>--%>
                                </display:table>

                            </td>
                        </tr>

                    </table>
                </div>


                <table>
                    <tr>
                        <td>

                            <button type="button" class="btn btn-default" onclick="closeActivation();">
                                <i class="icon-remove-circle icon-black"></i> Close
                            </button>
                        </td>
                    </tr>
                </table>



            </s:form>

        </td>
    </tr>
</table>

</body>
</html>


