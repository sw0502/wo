<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type="text/javascript">


        $.subscribe('beforeProcessSave', function (event, data) {

            var idAlatDetail = document.getElementById('kodeAlatDetail').value;
            var kodeAlat = document.getElementById('kodeAlatApb').value;
//            alert(idAlatDetail);

//             var surveyId = document.getElementById('surveyId').value;

            if (idAlatDetail != ''&& kodeAlat != '') {

                if (confirm('Do you want to save this record?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialog');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (document.getElementById("idAlatDetail").value == '') {
                    msg = 'Field <strong>ID ALat Detail</strong> is required.' + '<br/>';
                }
                var msg = "";
                if (document.getElementById("kodeAlat").value == '') {
                    msg = 'Field <strong>Kode Alat</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessage').innerHTML = msg;

                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialog', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialog');
//                $('#view_dialog_edit_task').dialog('close');
            }
        });

        $.subscribe('errorDialog', function (event, data) {
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function cancelBtn() {
            $('#view_dialog_edit').dialog('close');
        };

        function callSearchFunction() {
            $('#info_dialog').dialog('close');
            $('#view_dialog_edit').dialog('close');
            document.pemeliharaanForm.action='search_pemeliharaan.action';
            document.pemeliharaanForm.submit();
        };

//        var dataFrekuensiTx = document.getElementById("enFrekuensiTx").value;
//        var dataFrekuensiRx = document.getElementById("enFrekuensiRx").value;
//        var dataDaya = document.getElementById("enDaya").value;
//        var dataTransmisi = document.getElementById("enTransmisi").value;
//        var dataJenisTp = document.getElementById("enJenisTp").value;
//
//        if (dataFrekuensiTx == 'Y'){
//            document.getElementById("disFrekuensiTx").style.display = 'block';
//        }
//        if (dataFrekuensiRx == 'Y'){
//            document.getElementById("disFrekuensiRx").style.display = 'block';
//        }
//        if (dataDaya == 'Y'){
//            document.getElementById("disDaya").style.display = 'block';
//        }
//        if (dataTransmisi == 'Y'){
//            document.getElementById("disTransmisi").style.display = 'block';
//        }
//        if (dataJenisTp == 'Y'){
//            document.getElementById("disJenisTp").style.display = 'block';
//        }

    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="editCatatanForm" method="post" namespace="/pemeliharaan" action="saveEdit_pemeliharaan" enctype="multipart/form-data"
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">Insert Catatan Pemeliharaan</legend>

                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Id Alat Detail :</small></label >
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kodeAlatDetail" name="alatDetail.idAlatDetail" cssClass="textForm" disabled="true"/>
                                    <s:hidden name="alatDetail.idAlatDetail"/>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <label class="control-label"><small>Tanggal Pasang :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <sj:datepicker id="tglPasangFormEdit" name="alatDetail.stTglPasang" displayFormat="dd-mm-yy"
                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" disabled="true" />

                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>No. SAP :</small></label >
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noSap" name="alatDetail.noSap" cssClass="textForm" disabled="true"/>
                                    <s:hidden name="alatDetail.noSap"/>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <label class="control-label"><small>Tanggal Operasi :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <sj:datepicker id="tglOprsFormEdit" name="alatDetail.stTglOprs" displayFormat="dd-mm-yy"
                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" disabled="true" />
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Kode Alat :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kodeAlatApb" name="alatDetail.kodeAlat" cssClass="textForm" disabled="true"/>
                                    <s:hidden name="alatDetail.kodeAlat"/>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <label class="control-label"><small>Keterangan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="keterangan" name="alatDetail.keterangan" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Kode APP :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kodeApp" name="alatDetail.kodeApp" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                            <%--<s:hidden id="enJenisTp" name="alatDetail.enJenisTp"/>--%>
                            <%--<div id="disJenisTp" style="display: none">--%>
                                <td>
                                    <table>
                                        <label class="control-label"><small>jenis TP :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="jenisTp" name="alatDetail.jenisTp" cssClass="textForm" disabled="true"/>
                                    </table>
                                </td>
                            <%--</div>--%>

                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label">No. GI :</label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noGi" name="alatDetail.noGi" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                            <%--<s:hidden id="enFrekuensiTx" name="alatDetail.enFrekuensiTx"/>--%>
                            <%--<div id="disFrekuensiTx" style="display: none;">--%>
                                <td>
                                    <table>
                                        <label class="control-label"><small>Frekuensi TX :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="frekuensiTx" name="alatDetail.frekuensiTx" cssClass="textForm" disabled="true"/>
                                    </table>
                                </td>
                            <%--</div>--%>
                            
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label">Nama GI :</label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaGi" name="alatDetail.namaGi" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="namaGiH" name="alatDetail.namaGi" />
                                </table>
                            </td>
                            <%--<s:hidden id="enFrekueniRx" name="alatDetail.enFrekuensiRx"></s:hidden>--%>
                            <%--<div id="disFrekuensiRx" style="display: none">--%>
                                <td>
                                    <table>
                                        <label class="control-label"><small>Frekuensi RX :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="frekuensiRx" name="alatDetail.frekuensiRx" cssClass="textForm" disabled="true"/>
                                    </table>
                                </td>
                            <%--</div>--%>
                            
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>No. Gi Tujuan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noGiTujuan" name="alatDetail.noGiTujuan" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                            <%--<s:hidden id="enDaya" name="alatDetail.enDaya" />--%>
                            <%--<div id="disDaya" style="display: none">--%>
                                <td>
                                    <table>
                                        <label class="control-label"><small>Daya :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="daya" name="alatDetail.daya" cssClass="textForm" disabled="true"/>
                                    </table>
                                </td>
                            <%--</div>--%>
                            
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Nama Gi Tujuan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaGiTujuan" name="alatDetail.namaGiTujuan" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="namaGiTujuanH" name="alatDetail.namaGiTujuan"/>
                                </table>
                            </td>
                            <%--<s:hidden id="enTransmisi" name="alatDetail.enTransmisi" />--%>
                            <%--<div id="disTransmisi" style="display: none;">--%>
                                <td>
                                    <table>
                                        <label class="control-label"><small>Transmisi :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="transmisi" name="alatDetail.transmisi" cssClass="textForm" disabled="true"/>
                                    </table>
                                </td>
                            <%--</div>--%>
                            
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Jenis Alat :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaAlatDetail" name="alatDetail.namaAlatDetail" cssClass="textForm" disabled="true"/>
                                    <s:hidden name="alatDetail.namaAlatDetail"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Merk :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="merk" name="alatDetail.merk" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Tegangan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="tegangan" name="alatDetail.tegangan" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Type ID :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="idType" name="alatDetail.typeId" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>No. Series :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noSeries" name="alatDetail.noSeries" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Kode Status :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kdStatus" name="alatDetail.kdStatus" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>

                        </tr>
                        <tr>

                        </tr>
                        <tr>

                        </tr>
                        <tr>

                        </tr>
                        <tr>

                        </tr>
                        <tr>

                    </tr><tr>

                    </tr><tr>

                    </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Catatan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textarea id="note" name="pemeliharaan.catatan" disabled="false" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Upload Documment Pemeliharaan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:file id="upload" name="upload" cssClass="btn-mini" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                </table>
                            </td>
                        </tr>

                    </table>
                </fieldset>

                <div id="actions" class="form-actions">
                    <table>
                        <tr>
                            <td></td>
                            <td>
                                <div id="crud">
                                    <table>

                                        <tr>
                                            <td align="center">

                                                <table>

                                                    <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="editCatatanForm" id="save" name="save"
                                                               onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialog,successDialog"
                                                               onSuccessTopics="successDialog" onErrorTopics="errorDialog" >
                                                        <i class="icon-ok-sign icon-white"></i>
                                                        Save
                                                    </sj:submit>

                                                    <sj:dialog id="waiting_dialog_edit" openTopics="showDialog" closeTopics="closeDialogEditTask" modal="true"
                                                               resizable="false"
                                                               height="250" width="600" autoOpen="false" title="Saving ...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                    </sj:dialog>

                                                    <sj:dialog id="info_dialog" openTopics="showInfoDialog" modal="true" resizable="false" closeOnEscape="false"
                                                               position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                               buttons="{
                                                                    'OK':function() {
                                                                               callSearchFunction();
                                                                         }
                                                                 }"
                                                    >
                                                        <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                        Record has been saved successfully.
                                                    </sj:dialog>

                                                    <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                               position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                               buttons="{
                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                    <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                               position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                               buttons="{
                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                <br/>
                                                                <center><div id="errorValidationMessage"></div></center>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>


                                                </table>
                                            </td>

                                            <td>
                                                <button type="button" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelBtn();">
                                                    <i class="icon-remove-circle "/> Cancel
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>

            </s:form>


        </td>
    </tr>
</table>

</body>
</html>


