<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">

        function closeActivation() {
            $('#view_dialog_ViewProject').dialog('close');
        }


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="aktivasiSKArealForm" method="post" namespace="/project" action="" enctype="multipart/form-data"
                    cssClass="well form-horizontal">

                <fieldset>
                    <%--<legend align="left">Detail Project</legend>--%>

                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>
                    <table>
                            <tr>
                                <td>
                                    <object data="<s:property value="pemeliharaan.filePath"/>" type="application/pdf" width="800" height="900">
                                        <%--<p>Alternative text - include a link <a href="myfile.pdf">to the PDF!</a></p>--%>
                                    </object>
                                    <%--<iframe src="<s:property value="project.pathfile"/>" width="100%" height="100%">--%>
                                        <%--This browser does not support PDFs. Please download the PDF to view it: <a href="<s:property value="project.pathfile"/>">Download PDF</a>--%>
                                    <%--</iframe>--%>
                                        <%--<iframe src="http://docs.google.com/gview?url=<s:property value="project.pathfile"/>&embedded=true" style="width:600px; height:600px;" frameborder="0"></iframe>--%>
                                    <%--<iframe src="localhost:8080<s:property value="project.pathfile"/>&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>--%>
                                </td>
                            </tr>
                            <%--<tr>--%>
                                <%--<td>--%>
                                    <%--<img src="<s:property value="project.pathfile"/>" width="900px"/>--%>
                                <%--</td>--%>
                            <%--</tr>--%>
                    </table>

                </fieldset>
                <div id="actions" class="form-actions">
                    <div id="crud">
                        <table>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table>
                    <tr>
                        <td>
                            <table>
                            </table>
                        </td>
                    </tr>
                </table>

            </s:form>

            <table>
            </table>

        </td>
    </tr>
</table>

</body>
</html>


