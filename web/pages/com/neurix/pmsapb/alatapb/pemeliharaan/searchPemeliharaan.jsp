<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type="text/javascript">

    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="pemeliharaanForm" method="post"  namespace="/pemeliharaan" action="search_pemeliharaan"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Search Pemeliraan Alat</legend>

                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="pemeliharaan.noSap">No. SAP :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noSap" name="pemeliharaan.noSap" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="pemeliharaan.idAlatDetail">Kode Detail Alat :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="idAlatDetail" name="pemeliharaan.idAlatDetail" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="pemeliharaan.kodeAlat">Kode Alat :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="kodeAlat" name="pemeliharaan.kodeAlat" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="pemeliharaan.namaAlatDetail">Nama Alat :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaAlatDetail" name="pemeliharaan.namaAlatDetail" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <table>
                                            <tr>

                                                <td>
                                                    <table>

                                                        <sj:dialog id="waiting_dialog" openTopics="showDialogSearch" closeTopics="closeDialogSearch" modal="true" resizable="false"
                                                                   position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                                            Please don't close this window, server is processing your request ...
                                                            </br>
                                                            </br>
                                                            </br>
                                                            <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                        </sj:dialog>
                                                        <sj:submit type="button" cssClass="btn btn-primary" formIds="pemeliharaanForm" id="searchAlatDetail" name="searchAlatDetail"
                                                                   onClickTopics="showDialogSearch" onCompleteTopics="closeDialogSearch">
                                                            <i class="icon-search icon-white"></i>
                                                            Search
                                                        </sj:submit>

                                                    </table>
                                                </td>
                                                <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<sj:dialog id="view_dialog_add" openTopics="showDialogAdd" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                                                                   <%--position="center" height="550" width="950" autoOpen="false" title="Tambah Alat Detail"--%>
                                                        <%-->--%>
                                                            <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                                                        <%--</sj:dialog>--%>

                                                        <%--<s:url var="urlAdd" namespace="/alatdetail" action="initAdd_alatdetail"--%>
                                                               <%--escapeAmp="false">--%>
                                                        <%--</s:url>--%>
                                                        <%--<sj:a onClickTopics="showDialogAdd" href="%{urlAdd}" cssClass="btn btn-info">--%>
                                                            <%--<i class="icon-plus-sign icon-white"></i>--%>
                                                            <%--Add Data Detail Alat--%>
                                                        <%--</sj:a>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>


                                                <td>
                                                    <table>

                                                        <button type="button" class="btn" onclick="window.location.href='<s:url action="initForm_pemeliharaan"/>' ">
                                                            <i class="icon-repeat"></i> Reset
                                                        </button>

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>



                                    <sj:dialog id="view_dialog_log" openTopics="showDialogLog" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="1000" autoOpen="false" title="History Pemeliharaan Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete" openTopics="showDialogDelete" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="700" width="600" autoOpen="false" title="Delete Data Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit" openTopics="showDialogEditAlatDetail" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="600" width="900" autoOpen="false" title="Edit data Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_alat_detail" openTopics="showDialogViewAlatDetail" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="700" width="600" autoOpen="false" title="View Data Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="80%">
                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfAlatDetail" value="#session.listOfResult" scope="request" />
                                                <display:table name="listOfAlatDetail" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_alatdetail.action" export="true" id="row" pagesize="50" style="font-size:10">

                                                    <display:column media="html" title="<small>View Catatan</small>" style="text-align:center;font-size:9">

                                                        <s:url var="urlView" namespace="/pemeliharaan" action="initViewCatatan_pemeliharaan"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogLog" href="%{urlView}">
                                                            <img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print" width="25px">
                                                        </sj:a>

                                                    </display:column>

                                                    <display:column media="html" title="<small>View Detail Alat</small>" style="text-align:center;font-size:9">

                                                        <s:url var="urlView" namespace="/alatdetail" action="viewAlatDetail_alatdetail"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogViewAlatDetail" href="%{urlView}">
                                                            <img border="0" src="<s:url value="/pages/images/view_detail_project.png"/>" name="icon_print" width="25px">
                                                        </sj:a>

                                                    </display:column>
                                                    <display:column media="html" title="<small>Insert Catatan </small>" style="text-align:center;font-size:9">

                                                        <s:url var="urlEdit" namespace="/pemeliharaan" action="initEdit_pemeliharaan"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogEditAlatDetail" href="%{urlEdit}">
                                                            <img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_print" width="25px">
                                                        </sj:a>

                                                    </display:column>

                                                    <display:column property="idAlatDetail" sortable="true" title="<small>No. Detail Alat</small>" />
                                                    <display:column property="noSap" sortable="true" title="<small>No. SAP</small>" />
                                                    <display:column property="kodeAlat" sortable="true" title="<small>Kode Alat</small>" />
                                                    <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                                    <display:column property="noSeries" sortable="true" title="<small>No. Series</small>" />
                                                    <display:column property="namaAlatDetail" sortable="true" title="<small>Jenis Alat</small>" />
                                                    <display:column property="kodeApp" sortable="true" title="<small>Kode APP</small>"  />
                                                    <display:column property="namaApp" sortable="true" title="<small>Wilayah APP</small>" />
                                                    <display:column property="noGi" sortable="true" title="<small>No. GI</small>" />
                                                    <display:column property="namaGi" sortable="true" title="<small>Gardu</small>" />
                                                    <display:column property="noGiTujuan" sortable="true" title="<small>No. Gardu Tujuan</small>" />
                                                    <display:column property="namaGiTujuan" sortable="true" title="<small>Gardu Tujuan</small>" />
                                                    <%--<display:column property="note" sortable="true" title="<small>Catatan</small>" />--%>

                                                    <display:setProperty name="paging.banner.item_name">Project</display:setProperty>
                                                    <display:setProperty name="paging.banner.items_name">Project</display:setProperty>
                                                    <display:setProperty name="export.excel.filename">AlatDetail.xls</display:setProperty>
                                                    <display:setProperty name="export.csv.filename">AlatDetail.csv</display:setProperty>
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                </s:form>



                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>


