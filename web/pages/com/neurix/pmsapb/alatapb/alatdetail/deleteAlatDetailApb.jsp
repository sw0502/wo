<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">


        $.subscribe('beforeProcessSave', function (event, data) {
            var idAlatDetail = document.getElementById('kodeAlatDetail').value;
            var kodeAlat = document.getElementById('kodeAlatApb').value;
//            var kodeApp = document.getElementById('kodeApp').value;

            if (idAlatDetail != ''&& kodeAlat != '') {

                if (confirm('Do you want to save this record?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialogEditTask');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (document.getElementById("idAlatDetail").value == '') {
                    msg = 'Field <strong>ID ALat Detail</strong> is required.' + '<br/>';
                }
                var msg = "";
                if (document.getElementById("kodeAlat").value == '') {
                    msg = 'Field <strong>Kode Alat</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessage').innerHTML = msg;

                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialogEditTask', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialogEditTask');
//                $('#view_dialog_edit_task').dialog('close');
            }
        });

        $.subscribe('errorDialogEditTask', function (event, data) {
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function cancelBtn() {
            $('#view_dialog_delete').dialog('close');
        };

        function callSearchFunction() {
            $('#info_dialogTask').dialog('close');
            $('#view_dialog_delete').dialog('close');
            document.alatDetailForm.action='search_alatdetail.action';
            document.alatDetailForm.submit();
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="deleteAlatDetailForm" method="post" namespace="/alatdetail" action="saveDelete_alatdetail" enctype="multipart/form-data"
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">Edit Alat Detail</legend>

                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Id Alat Detail :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kodeAlatDetail" name="alatDetail.idAlatDetail" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="idAlatDetail" name="alatDetail.idAlatDetail"/>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>No. SAP :</small></label >
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noSap" name="alatDetail.noSap" cssClass="textForm" disabled="true"/>
                                    <s:hidden name="alatDetail.noSap"/>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>No. Aktiva :</small></label >
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noAktiva" name="alatDetail.noAktiva" cssClass="textForm" disabled="true"/>
                                    <s:hidden name="alatDetail.noAktiva"/>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Kode Alat :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kodeAlatApb" name="alatDetail.kodeAlat" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="kodeAlatApabHidden" name="alatDetail.kodeAlat"/>

                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Kode APP :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kodeApp" name="alatDetail.kodeApp" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="kodeAppHiden" name="alatDetail.kodeApp"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label">No. GI :</label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noGi" name="alatDetail.noGi" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="noGiHidden" name="alatDetail.noGi"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label">Nama GI :</label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaGi" name="alatDetail.namaGi" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="namaGiHidden" name="alatDetail.namaGi"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>No. Gi Tujuan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noGiTujuan" name="alatDetail.noGiTujuan" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="noGiTujuanHidden" name="alatDetail.noGiTujuan"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Nama Gi Tujuan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaGiTujuan" name="alatDetail.namaGiTujuan" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="namaGiTujuanHidden" name="alatDetail.namaGiTujuan"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Nama Alat :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaAlatDetail" name="alatDetail.namaAlatDetail" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="namaAlatDetailHidden" name="alatDetail.namaAlatDetail"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Merk :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="merk" name="alatDetail.merk" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="merkHidden" name="alatDetail.merk"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Tegangan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="tegangan" name="alatDetail.tegangan" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="teganganHidden" name="alatDetail.tegangan"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Type ID :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="idType" name="alatDetail.typeId" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="idTypeHidden" name="alatDetail.typeId"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>No. Series :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noSeries" name="alatDetail.noSeries" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="noSeriesHidden" name="alatDetail.noSeries"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Kode Status :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kdStatus" name="alatDetail.kdStatus" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="kdStatusHidden" name="alatDetail.kdStatus"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label" for="alatDetail.status">Status Alat :</label>
                                </table>
                            </td>

                            <td>
                                <table>
                                    <s:select list="#{'Active':'Active','Non-Active':'Non-Active','Gudang':'Gudang'}" id="status" name="alatDetail.status"
                                              headerKey="" headerValue="[Select one]" cssClass="textForm" disabled="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;"/>
                                </table>
                            </td>
                        </tr>
                        <%--<tr>--%>
                            <%--<td>--%>
                                <%--<table>--%>
                                    <%--<label class="control-label"><small> Pax :</small></label>--%>
                                <%--</table>--%>
                            <%--</td>--%>
                            <%--<td>--%>
                                <%--<table>--%>
                                    <%--<s:textfield id="pax" name="alatDetail.pax" cssClass="textForm" disabled="true" />--%>
                                <%--</table>--%>
                            <%--</td>--%>
                        <%--</tr>--%>
                        <%--<tr>--%>
                            <%--<td>--%>
                                <%--<table>--%>
                                    <%--<label class="control-label"><small> Type Pax :</small></label>--%>
                                <%--</table>--%>
                            <%--</td>--%>
                            <%--<td>--%>
                                <%--<table>--%>
                                    <%--<s:textfield id="typePax" name="alatDetail.typePax" cssClass="textForm" disabled="true"/>--%>
                                <%--</table>--%>
                            <%--</td>--%>
                        <%--</tr>--%>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Power Supply :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="powerSupply" name="alatDetail.powerSupply" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Ruang Terpasang :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="ruangTerpasang" name="alatDetail.ruangTerpasang" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Type Switch :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="typeSwitch" name="alatDetail.typeSwitch" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Latitude :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="lat" name="alatDetail.lat" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Longtitude :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="longt" name="alatDetail.longt" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small> Techidentno :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="techidentno" name="alatDetail.techidentno" cssClass="textForm" disabled="true"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Tanggal Pasang :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="tglPasang" name="alatDetail.stTglPasang" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="tglPasangHidden" name="alatDetail.stTglPasang"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Tanggal Operasi :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="tglOprs" name="alatDetail.stTglOprs" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="tglOprsHidden" name="alatDetail.stTglOprs"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Keterangan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="keterangan" name="alatDetail.keterangan" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="keteranganHidden" name="alatDetail.keterangan"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>jenis TP :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="jenisTp" name="alatDetail.jenisTp" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="jenisTpHidden" name="alatDetail.jenisTp"/>

                                </table>
                            </td>
                        </tr><tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Frekuensi TX :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="frekuensiTx" name="alatDetail.frekuensiTx" cssClass="textForm" disabled="true"/>
                                <s:hidden id="frekuensiTxHidden" name="alatDetail.frekuensiTx"/>

                            </table>
                        </td>
                    </tr><tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Frekuensi RX :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="frekuensiRx" name="alatDetail.frekuensiRx" cssClass="textForm" disabled="true"/>
                                <s:hidden id="frekuensiRxHidden" name="alatDetail.frekuensiRx"/>

                            </table>
                        </td>
                    </tr><tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Daya :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="daya" name="alatDetail.daya" cssClass="textForm" disabled="true"/>
                                <s:hidden id="dayaHidden" name="alatDetail.daya"/>

                            </table>
                        </td>
                    </tr><tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Transmisi :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="transmisi" name="alatDetail.transmisi" cssClass="textForm" disabled="true"/>
                                <s:hidden id="transmisiHidden" name="alatDetail.transmisi"/>
                            </table>
                        </td>
                    </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Catatan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textarea id="note" name="alatDetail.note" disabled="true" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden id="note" name="alatDetail.note"/>
                                    <s:hidden id="urlFoto" name="alatDetail.urlFoto"/>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>

                <div id="actions" class="form-actions">
                    <table>
                        <tr>
                            <td></td>
                            <td>
                                <div id="crud">
                                    <table>

                                        <tr>
                                            <td align="center">

                                                <table>

                                                    <sj:submit targets="crud" type="button" cssClass="btn btn-warning" formIds="deleteAlatDetailForm" id="save" name="save"
                                                               onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialogEditTask,successDialogEditTask"
                                                               onSuccessTopics="successDialogEditTask" onErrorTopics="errorDialogEditTask" >
                                                        <i class="icon-ok-sign icon-white"></i>
                                                        Delete
                                                    </sj:submit>

                                                    <sj:dialog id="waiting_dialog_editTask" openTopics="showDialogEditTask" closeTopics="closeDialogEditTask" modal="true"
                                                               resizable="false"
                                                               height="250" width="600" autoOpen="false" title="Saving ...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                    </sj:dialog>

                                                    <sj:dialog id="info_dialogTask" openTopics="showInfoDialogEditTask" modal="true" resizable="false" closeOnEscape="false"
                                                               position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                               buttons="{
                                                                    'OK':function() {
                                                                               callSearchFunction();
                                                                         }
                                                                 }"
                                                    >
                                                        <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                        Record has been saved successfully.
                                                    </sj:dialog>

                                                    <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                               position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                               buttons="{
                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                    <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                               position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                               buttons="{
                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                <br/>
                                                                <center><div id="errorValidationMessage"></div></center>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>


                                                </table>
                                            </td>

                                            <td>
                                                <button type="button" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelBtn();">
                                                    <i class="icon-remove-circle "/> Cancel
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>

            </s:form>


        </td>
    </tr>
</table>

</body>
</html>


