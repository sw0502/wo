<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>--%>
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/AppAction.js"/>'></script>--%>
    <script type="text/javascript">


        $.subscribe('beforeProcessSave', function (event, data) {

            var kodeApp = document.getElementById('kodeApp').value;
            var noGi = document.getElementById('noGi').value;
            var kodeAlat = document.getElementById('kodeAlatApb').value;
            var namaGi = document.getElementById('namaGi').value;
//            alert(namaGi);

//             var surveyId = document.getElementById('surveyId').value;

            if (kodeApp != ''&& kodeAlat != '' && noGi != '') {

                if (confirm('Do you want to save this record?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialog');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (document.getElementById("kodeApp").value == '') {
                    msg = 'Field <strong>Kode App</strong> is required.' + '<br/>';
                }
                var msg = "";
                if (document.getElementById("kodeAlat").value == '') {
                    msg = 'Field <strong>Kode Alat</strong> is required.' + '<br/>';
                }
                var msg = "";
                if (document.getElementById("noGi").value == '') {
                    msg = 'Field <strong>No. GI</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessage').innerHTML = msg;

                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialog', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialog');
//                $('#view_dialog_edit_task').dialog('close');
            }
        });

        $.subscribe('errorDialog', function (event, data) {
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function cancelBtn() {
            $('#view_dialog_add').dialog('close');
        };

        function callSearchFunction() {
            $('#info_dialog').dialog('close');
            $('#view_dialog_add').dialog('close');
            document.alatDetailForm.action='search_alatdetail.action';
            document.alatDetailForm.submit();
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="addAlatDetailForm" method="post" namespace="/alatdetail" action="saveAdd_alatdetail"
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">Add Alat Detail</legend>

                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>
                        <%--<tr>--%>
                            <%--<td>--%>
                                <%--<table>--%>
                                    <%--<label class="control-label"><small>Id Alat Detail :</small></label>--%>
                                <%--</table>--%>
                            <%--</td>--%>
                            <%--<td>--%>
                                <%--<table>--%>
                                    <%--<s:textfield id="kodeAlatDetail" name="alatDetail.idAlatDetail" cssClass="textForm"/>--%>
                                <%--</table>--%>
                            <%--</td>--%>
                        <%--</tr>--%>

                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small>Kode Jenis Alat :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kodeAlatApb" name="alatDetail.kodeAlat" cssClass="textForm"/>
                                    <script>
                                        var functions, mapped;
                                        $('#kodeAlatApb').typeahead({
                                            minLength: 1,
                                            source: function (query, process) {
                                                functions = [];
                                                mapped = {};

                                                var data = [];
                                                dwr.engine.setAsync(false);
                                                AlatDetailAction.initComboAlat(query, function (listdata) {
                                                    data = listdata;
                                                });

                                                $.each(data, function (i, item) {
                                                    var labelItem = item.namaAlat;
                                                    var enJenisTp = item.enJenisTp;
                                                    var enDaya = item.enDaya;
                                                    var enTransmisi = item.enTransmisi;
                                                    var enTegOprs = item.enTegOprs;
                                                    var enFrekuensiTx = item.enFrekuensiTx;
                                                    var enFrekuensiRx = item.enFrekuensiRx;
                                                    mapped[labelItem] = { id: item.kodeAlat, label: labelItem, jt: enJenisTp, dy: enDaya, tr: enTransmisi, to: enTegOprs, tx: enFrekuensiTx, rx: enFrekuensiRx};
                                                    functions.push(labelItem);
                                                });

                                                process(functions);
                                            },
                                            updater: function (item) {
                                                var selectedObj = mapped[item];
                                                var namaAlat = selectedObj.label;
                                                var dataFrekuensiTx = selectedObj.tx;
                                                var dataFrekuensiRx = selectedObj.rx;
                                                var dataDaya = selectedObj.dy;
                                                var dataTransmisi = selectedObj.tr;
                                                var dataJenisTp = selectedObj.jt;
                                                var dataTegOprs = selectedObj.to;

                                                document.getElementById("namaAlatDetailAdd").value = namaAlat;
                                                document.getElementById("kodeAlatApb").value = selectedObj.id;
//                                                alert(dataFrekuensiRx+"-"+dataFrekuensiTx+"-"+dataDaya+"-"+dataTransmisi+"-"+dataJenisTp+"-"+dataTegOprs);


                                                if (dataFrekuensiTx == 'Y'){
                                                    document.getElementById("disFrekuensiTx").style.display = 'block';
                                                } else {
                                                    document.getElementById("disFrekuensiTx").style.display = 'none';
                                                }
                                                if (dataFrekuensiRx == 'Y'){
                                                    document.getElementById("disFrekuensiRx").style.display = 'block';
                                                } else {
                                                    document.getElementById("disFrekuensiRx").style.display = 'none';
                                                }
                                                if (dataDaya == 'Y'){
                                                    document.getElementById("disDaya").style.display = 'block';
                                                } else {
                                                    document.getElementById("disDaya").style.display = 'none';
                                                }
                                                if (dataTransmisi == 'Y'){
                                                    document.getElementById("disTransmisi").style.display = 'block';
                                                } else {
                                                    document.getElementById("disTransmisi").style.display = 'none';
                                                }
                                                if (dataJenisTp == 'Y'){
                                                    document.getElementById("disJenisTp").style.display = 'block';
                                                } else {
                                                    document.getElementById("disJenisTp").style.display = 'none';
                                                }
                                                if (dataTegOprs == 'Y'){
                                                    document.getElementById("disTegOprs").style.display = 'block';
                                                } else {
                                                    document.getElementById("disTegOprs").style.display = 'none';
                                                }

                                                return selectedObj.id;

                                            }
                                        });
                                        //
                                        //
                                    </script>
                                </table>
                            </td>
                        </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small>Nama Jenis Alat :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="namaAlatDetailAdd" name="alatDetail.namaAlatDetail" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr id="disJenisTp" style="display: none">
                                <td>
                                    <table>
                                        <label class="control-label"><small>jenis TP :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="jenisTp" name="alatDetail.jenisTp" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr id="disFrekuensiTx" style="display: none">
                                <td>
                                    <table>
                                        <label class="control-label"><small>Frekuensi TX :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="frekuensiTx" name="alatDetail.frekuensiTx" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr id="disFrekuensiRx" style="display: none">
                                <td>
                                    <table>
                                        <label class="control-label"><small>Frekuensi RX :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="frekuensiRx" name="alatDetail.frekuensiRx" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr id="disDaya" style="display: none">
                                <td>
                                    <table>
                                        <label class="control-label"><small>Daya :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="daya" name="alatDetail.daya" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr id="disTransmisi" style="display: none">
                                <td>
                                    <table>
                                        <label class="control-label"><small>Transmisi :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="transmisi" name="alatDetail.transmisi" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>

                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label">No SAP :</label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="noSap" name="alatDetail.noSap" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label">No. Aktiva :</label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="noAktiva" name="alatDetail.noAktiva" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>

                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small>Kode APP :</small></label>

                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kodeApp" name="alatDetail.kodeApp" cssClass="textForm"/>
                                    <script>
                                        var functions, mapped;
                                        $('#kodeApp').typeahead({
                                            minLength: 1,
                                            source: function (query, process) {
                                                functions = [];
                                                mapped = {};

                                                var data = [];
                                                dwr.engine.setAsync(false);
                                                AlatDetailAction.initComboApp(query, function (listdata) {
                                                    data = listdata;
                                                });

                                                $.each(data, function (i, item) {
                                                    var labelItem = item.namaWilayah;
                                                    mapped[labelItem] = { id: item.kodeApp, label: labelItem };
                                                    functions.push(labelItem);
                                                });

                                                process(functions);
                                            },
                                            updater: function (item) {
                                                var selectedObj = mapped[item];
                                                var namaApp = selectedObj.label;
                                                document.getElementById("namaApp").value = namaApp;
                                                document.getElementById("namaAppH").value = namaApp;

                                                return selectedObj.id;
                                            }
                                        });
                                        //
                                        //
                                    </script>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label">Nama App :</label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaApp" name="alatDetail.namaApp" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="namaAppH" name="alatDetail.namaApp"/>
                                </table>
                            </td>
                        </tr>

                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label">No. GI :</label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noGi" name="alatDetail.noGi" cssClass="textForm"/>
                                    <script>
                                        var functions, mapped;
                                        $('#noGi').typeahead({
                                            minLength: 1,
                                            source: function (query, process) {
                                                functions = [];
                                                mapped = {};

                                                var data = [];
                                                dwr.engine.setAsync(false);
                                                AlatDetailAction.initComboGardu(query, function (listdata) {
                                                    data = listdata;
                                                });

                                                $.each(data, function (i, item) {
                                                    var labelItem = item.namaGarduInduk;
                                                    mapped[labelItem] = { id: item.nomorGarduInduk, label: labelItem };
                                                    functions.push(labelItem);
                                                });

                                                process(functions);
                                            },
                                            updater: function (item) {
                                                var selectedObj = mapped[item];
                                                var namaGi = selectedObj.label;
                                                document.getElementById("namaGi").value = namaGi;
                                                document.getElementById("namaGiH").value = namaGi;

                                                return selectedObj.id;
                                            }
                                        });
                                        //
                                        //
                                    </script>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label">Nama GI :</label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaGi" name="alatDetail.namaGi" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="namaGiH" name="alatDetail.namaGi"/>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small>No. Gi Tujuan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noGiTujuan" name="alatDetail.noGiTujuan" cssClass="textForm"/>
                                    <script>
                                        var functions, mapped;
                                        $('#noGiTujuan').typeahead({
                                            minLength: 1,
                                            source: function (query, process) {
                                                functions = [];
                                                mapped = {};

                                                var data = [];
                                                dwr.engine.setAsync(false);
                                                AlatDetailAction.initComboGardu(query, function (listdata) {
                                                    data = listdata;
                                                });

                                                $.each(data, function (i, item) {
                                                    var labelItem = item.namaGarduInduk;
                                                    mapped[labelItem] = { id: item.nomorGarduInduk, label: labelItem };
                                                    functions.push(labelItem);
                                                });

                                                process(functions);
                                            },
                                            updater: function (item) {
                                                var selectedObj = mapped[item];
                                                var namaGiTujuan = selectedObj.label;
                                                document.getElementById("namaGiTujuan").value = namaGiTujuan;
                                                document.getElementById("namaGiTujuanH").value = namaGiTujuan;

                                                return selectedObj.id;
                                            }
                                        });
                                        //
                                        //
                                    </script>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small>Nama Gi Tujuan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaGiTujuan" name="alatDetail.namaGiTujuan" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="namaGiTujuanH" name="alatDetail.namaGiTujuan"/>
                                </table>
                            </td>
                        </tr>

                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small> Merk :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="merk" name="alatDetail.merk" cssClass="textForm"/>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small>Tegangan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="tegangan" name="alatDetail.tegangan" cssClass="textForm"/>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small> Type ID :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="idType" name="alatDetail.typeId" cssClass="textForm"/>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small>No. Series :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noSeries" name="alatDetail.noSeries" cssClass="textForm"/>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small> Kode Status :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kdStatus" name="alatDetail.kdStatus" cssClass="textForm"/>
                                </table>
                            </td>
                        </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                    <label class="control-label" for="alatDetail.status">Status Alat :</label>
                                    </table>
                                </td>

                                <td>
                                    <table>
                                        <s:select list="#{'Active':'Active','Non-Active':'Non-Active','Gudang':'Gudang'}" id="status" name="alatDetail.status"
                                                  headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;"/>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small> Pax :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="pax" name="alatDetail.pax" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small> Type Pax :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="typePax" name="alatDetail.typePax" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small> Power Supply :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="powerSupply" name="alatDetail.powerSupply" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small> Ruang Terpasang :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="ruangTerpasang" name="alatDetail.ruangTerpasang" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small> Type Switch :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="typeSwitch" name="alatDetail.typeSwitch" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small> Latitude :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="lat" name="alatDetail.lat" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small> Longtitude :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="longt" name="alatDetail.longt" cssClass="textForm"/>
                                </table>
                            </td>
                            </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small> Techidentno :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textfield id="techidentno" name="alatDetail.techidentno" cssClass="textForm"/>
                                    </table>
                                </td>
                            </tr>


                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small>Tanggal Pasang :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <sj:datepicker id="tglPasangFormAdd" name="alatDetail.stTglPasang" displayFormat="dd-mm-yy"
                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />

                                </table>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small>Tanggal Operasi :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <sj:datepicker id="tglOprsFormAdd" name="alatDetail.stTglOprs" displayFormat="dd-mm-yy"
                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                                </table>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <table>
                                    <label class="control-label"><small>Keterangan :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="keterangan" name="alatDetail.keterangan" cssClass="textForm"/>
                                </table>
                            </td>
                        </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small>Catatan :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:textarea id="note" name="alatDetail.note" disabled="false" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>                                    </table>
                                </td>
                            </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small>Upload Foto Alat :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:file id="uploadFront" name="uploadFront" cssClass="btn-mini" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: block">
                                <td>
                                    <table>
                                        <label class="control-label"><small>Upload Documment :</small></label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <s:file id="uploadDoc" name="uploadDoc" cssClass="btn-mini" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    </table>
                                </td>
                            </tr>
                            <%--<tr>--%>
                                <%--<td>--%>
                                    <%--<table>--%>
                                        <%--<label class="control-label"><small>Upload Foto Alat Belakang :</small></label>--%>
                                    <%--</table>--%>
                                <%--</td>--%>
                                <%--<td>--%>
                                    <%--<table>--%>
                                        <%--<s:file id="uploadBack" name="uploadBack" cssClass="btn-mini" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                                    <%--</table>--%>
                                <%--</td>--%>
                            <%--</tr>--%>

                            <%--<tr>--%>
                                <%--<td>--%>
                                    <%--<table>--%>
                                        <%--<label class="control-label"><small>Upload Foto Alat Samping Kiri :</small></label>--%>
                                    <%--</table>--%>
                                <%--</td>--%>
                                <%--<td>--%>
                                    <%--<table>--%>
                                        <%--<s:file id="uploadLeft" name="uploadLeft" cssClass="btn-mini" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                                    <%--</table>--%>
                                <%--</td>--%>
                            <%--</tr>--%>
                            <%--<tr>--%>
                                <%--<td>--%>
                                    <%--<table>--%>
                                        <%--<label class="control-label"><small>Upload Foto Alat Samping Kanan :</small></label>--%>
                                    <%--</table>--%>
                                <%--</td>--%>
                                <%--<td>--%>
                                    <%--<table>--%>
                                        <%--<s:file id="uploadRight" name="uploadRight" cssClass="btn-mini" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                                    <%--</table>--%>
                                <%--</td>--%>
                            <%--</tr>--%>

                    </table>
                </fieldset>

                <div id="actions" class="form-actions">
                    <table>
                        <tr>
                            <td></td>
                            <td>
                                <div id="crud">
                                    <table>

                                        <tr>
                                            <td align="center">

                                                <table>

                                                    <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="addAlatDetailForm" id="save" name="save"
                                                               onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialog,successDialog"
                                                               onSuccessTopics="successDialog" onErrorTopics="errorDialog" >
                                                        <i class="icon-ok-sign icon-white"></i>
                                                        Save
                                                    </sj:submit>

                                                    <sj:dialog id="waiting_dialog_add" openTopics="showDialog" closeTopics="closeDialogEditTask" modal="true"
                                                               resizable="false"
                                                               height="250" width="600" autoOpen="false" title="Saving ...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                    </sj:dialog>

                                                    <sj:dialog id="info_dialog" openTopics="showInfoDialog" modal="true" resizable="false" closeOnEscape="false"
                                                               position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                               buttons="{
                                                                    'OK':function() {
                                                                               callSearchFunction();
                                                                         }
                                                                 }"
                                                    >
                                                        <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                        Record has been saved successfully.
                                                    </sj:dialog>

                                                    <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                               position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                               buttons="{
                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                    <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                               position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                               buttons="{
                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                <br/>
                                                                <center><div id="errorValidationMessage"></div></center>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>


                                                </table>
                                            </td>

                                            <td>
                                                <button type="button" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelBtn();">
                                                    <i class="icon-remove-circle "/> Cancel
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>

            </s:form>


        </td>
    </tr>
</table>

</body>
</html>


