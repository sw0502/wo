<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        function closeActivation(){
            $('#view_dialog_log').dialog('close');
//            document.alatDetailForm.action='search_alatdetail.action';
//            document.alatDetailForm.submit();
        }

    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="listTaskProject" method="post" namespace="" action="viewTaskList_project"
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">History Perubahan Data Alat</legend>

                    <table >
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>

                    </table>


                </fieldset>


                <div style="overflow: scroll">
                    <table width="100%" style="overflow: scroll">
                        <tr>
                            <td align="center">
                                <s:set name="listOfResult" value="#session.listOfResultLogAlat" scope="request" />
                                <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="100">

                                    <display:column property="idLogAlatDetail" sortable="true" title="<small>No. Log</small>" />
                                    <display:column property="idAlatDetail" sortable="true" title="<small>No. Detail Alat</small>" />
                                    <display:column property="kodeAlat" sortable="true" title="<small>Kode Alat</small>" />
                                    <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                    <display:column property="noSeries" sortable="true" title="<small>No. Series</small>" />
                                    <display:column property="namaAlatDetail" sortable="true" title="<small>Jenis Alat</small>" />
                                    <display:column property="kodeApp" sortable="true" title="<small>Kode APP</small>"  />
                                    <display:column property="namaApp" sortable="true" title="<small>Wilayah APP</small>" />
                                    <display:column property="noGi" sortable="true" title="<small>No. GI</small>" />
                                    <display:column property="namaGi" sortable="true" title="<small>Gardu</small>" />
                                    <display:column property="noGiTujuan" sortable="true" title="<small>No. Gardu Tujuan</small>" />
                                    <display:column property="namaGiTujuan" sortable="true" title="<small>Gardu Tujuan</small>" />
                                    <display:column property="note" sortable="true" title="<small>Catatan</small>" />
                                    <display:column property="status" sortable="true" title="<small>Status Alat</small>" />
                                    <display:column property="lastUpdateWho" sortable="true" title="<small>di Update Oleh</small>" />
                                    <display:column property="lastUpdate" sortable="true" title="<small>di Update Tgl</small>" />

                                    <%--<display:setProperty name="paging.banner.item_name">Project</display:setProperty>--%>
                                    <%--<display:setProperty name="paging.banner.items_name">Project</display:setProperty>--%>
                                    <%--<display:setProperty name="export.excel.filename">AlatDetail.xls</display:setProperty>--%>
                                    <%--<display:setProperty name="export.csv.filename">AlatDetail.csv</display:setProperty>--%>
                                </display:table>

                            </td>
                        </tr>

                    </table>
                </div>


                <table>
                    <tr>
                        <td>

                            <button type="button" class="btn btn-default" onclick="closeActivation();">
                                <i class="icon-remove-circle icon-black"></i> Close
                            </button>
                        </td>
                    </tr>
                </table>



            </s:form>

        </td>
    </tr>
</table>

</body>
</html>


