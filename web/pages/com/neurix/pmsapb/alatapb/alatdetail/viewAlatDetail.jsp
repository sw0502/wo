<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        var dataFrekuensiTx = document.getElementById("enFrekuensiTx").value;
        var dataFrekuensiRx = document.getElementById("enFrekuensiRx").value;
        var dataDaya = document.getElementById("enDaya").value;
        var dataTransmisi = document.getElementById("enTransmisi").value;
        var dataJenisTp = document.getElementById("enJenisTp").value;

        if (dataFrekuensiTx == 'Y'){
            document.getElementById("disFrekuensiTx").style.display = 'block';
        }
        if (dataFrekuensiRx == 'Y'){
            document.getElementById("disFrekuensiRx").style.display = 'block';
        }
        if (dataDaya == 'Y'){
            document.getElementById("disDaya").style.display = 'block';
        }
        if (dataTransmisi == 'Y'){
            document.getElementById("disTransmisi").style.display = 'block';
        }
        if (dataJenisTp == 'Y'){
            document.getElementById("disJenisTp").style.display = 'block';
        }

        function cancelBtn() {
            $('#view_dialog_alat_detail').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<sj:dialog id="view_dialog_View_doc" openTopics="showDialogViewDoc" modal="true" resizable="false" cssStyle="text-align:left;"
           position="center" height="500" width="600" autoOpen="false" title="View Document">
    <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
</sj:dialog>

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="" method="post" namespace="/project" action="" enctype="multipart/form-data"
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">Detail Alat</legend>

                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr >
                            <td>
                                <img src="<s:property value="alatDetail.imgFront"/>"onerror="this.src='/pmsapb/pages/images/not_found.gif'" width="500px"/>
                                    <%--<div id="myCarousel" class="carousel slide">--%>
                                    <%--&lt;%&ndash;<ol class="carousel-indicators">&ndash;%&gt;--%>
                                    <%--&lt;%&ndash;<li data-target="#myCarousel" data-slide-to="0" class="active"></li>&ndash;%&gt;--%>
                                    <%--&lt;%&ndash;<li data-target="#myCarousel" data-slide-to="1"></li>&ndash;%&gt;--%>
                                    <%--&lt;%&ndash;<li data-target="#myCarousel" data-slide-to="2"></li>&ndash;%&gt;--%>
                                    <%--&lt;%&ndash;<li data-target="#myCarousel" data-slide-to="3"></li>&ndash;%&gt;--%>
                                    <%--&lt;%&ndash;</ol>&ndash;%&gt;--%>
                                    <%--<!-- Carousel items -->--%>
                                    <%--<div class="carousel-inner">--%>
                                    <%--<div class="active item">--%>
                                    <%--<img src="<s:property value="alatDetail.imgFront"/>"onerror="this.src='/pmsapb/pages/images/not_found.gif'" width="500px"/>--%>
                                    <%--</div>--%>
                                    <%--<div class="item">--%>
                                    <%--<img src="<s:property value="alatDetail.imgBack"/>" onerror="this.src='/pmsapb/pages/images/not_found.gif'" width="500px"/>--%>
                                    <%--</div>--%>
                                    <%--<div class="item">--%>
                                    <%--<img src="<s:property value="alatDetail.imgLeft"/>" onerror="this.src='/pmsapb/pages/images/not_found.gif'" width="500px"/>--%>
                                    <%--</div>--%>
                                    <%--<div class="item">--%>
                                    <%--<img src="<s:property value="alatDetail.imgRight"/>" onerror="this.src='/pmsapb/pages/images/not_found.gif'" width="500px"/>--%>
                                    <%--</div>--%>
                                    <%--</div>--%>
                                    <%--<!-- Carousel nav -->--%>
                                    <%--<a class="carousel-control left" href="#myCarousel" data-slide="prev" style="margin-top: 70px">&lsaquo;</a>--%>
                                    <%--<a class="carousel-control right" href="#myCarousel" data-slide="next" style="margin-top: 70px">&rsaquo;</a>--%>
                                    <%--</div>--%>
                            </td>
                        </tr>
                            <%--<tr>--%>
                            <%--<td>--%>
                            <%--<img src="<s:property value="alatDetail.imgFront"/>" onerror="this.src='/pmsapb/pages/images/not_found.gif'" style="padding-left: 10px" width="200px"/>--%>
                            <%--</td>--%>
                            <%--</tr>--%>
                            <%--<tr>--%>
                            <%--<td>--%>
                            <%--<img src="<s:property value="alatDetail.imgBack"/>" onerror="this.src='/pmsapb/pages/images/not_found.gif'" style="padding-left: 10px" width="200px"/>--%>
                            <%--</td>--%>
                            <%--</tr>--%>
                            <%--<tr>--%>
                            <%--<td>--%>
                            <%--<img src="<s:property value="alatDetail.imgLeft"/>" onerror="this.src='/pmsapb/pages/images/not_found.gif'" style="padding-left: 10px" width="200px"/>--%>
                            <%--</td>--%>
                            <%--</tr>--%>
                            <%--<tr>--%>
                            <%--<td>--%>
                            <%--<img src="<s:property value="alatDetail.imgRight"/>" onerror="this.src='/pmsapb/pages/images/not_found.gif'" style="padding-left: 10px" width="200px"/>--%>
                            <%--</td>--%>
                            <%--</tr>--%>
                    </table>

                    <table>
                        <tr>
                            <td>
                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>ID Alat Detail : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.idAlatDetail"/>
                                </div>
                            </td>
                            <%--<td rowspan="20">--%>
                        <%----%>
                            <%--</td>--%>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>Kode Alat : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.kodeAlat"/>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>Kode APP : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.kodeApp"/> - <s:property value="alatDetail.namaApp"/>
                                </div>
                            </td>
                        </tr >

                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>No. GI : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.noGi"/>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>Nama GI : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.namaGi"/>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>No. GI Tujuan : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.noGiTujuan"/>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>Nama GI Tujuan : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.namaGiTujuan"/>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>Nama Alat : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.namaAlatDetail"/>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>Merk : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.merk"/>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>Tegangan : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.tegangan"/>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: block">
                            <td>
                                <label class="control-label"><small>Type ID : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="alatDetail.typeId"/>
                                </div>
                            </td>
                        </tr >
                            </td>
                            <td>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>No. Series : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.noSeries"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Kode Status : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.kdStatus"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Status Alat : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.status"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Power Supply : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.powerSupply"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Ruang Terpasang : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.ruangTerpasang"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Latitude : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.lat"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Longtitude : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.longt"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Techidentno : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.techidentno"/>
                                        </div>
                                    </td>
                                </tr>



                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Kapasitas : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.kapasitas"/>
                                        </div>
                                    </td>
                                </tr>
                                    <tr style="display: block">
                                        <td>
                                            <label class="control-label"><small>Fungsi : </small></label>
                                        </td>
                                        <td>
                                            <div class="label">
                                                <s:property value="alatDetail.fungsi"/>
                                            </div>
                                        </td>
                                    </tr>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Tegangan Input: </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.teganganInput"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Tegangan Output: </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.teganganOutput"/>
                                        </div>
                                    </td>
                                </tr>



                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Tanggal Pasang : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.tglPasang"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display: block">
                                    <td>
                                        <label class="control-label"><small>Tanggal Operasi : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.tglOprs"/>
                                        </div>
                                    </td>
                                </tr>


                                    <tr id="disJenisTp" style="display: none">
                                        <td>
                                            <label class="control-label"><small>Jenis TP : </small></label>
                                        </td>
                                        <td>
                                            <div class="label">
                                                <s:property value="alatDetail.jenisTp"/>
                                                <s:hidden id="enJenisTp" name="alatDetail.enJenisTp"/>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr id="disFrekuensiTx" style="display: none;">
                                        <td>
                                            <label class="control-label"><small>Frekuensi TX : </small></label>
                                        </td>
                                        <td>
                                            <div class="label">
                                                <s:property value="alatDetail.frekuensiTx"/>
                                                <s:hidden id="enFrekuensiTx" name="alatDetail.enFrekuensiTx"/>
                                            </div>
                                        </td>
                                    </tr>


                                <tr id="disFrekuensiRx" style="display: none">
                                    <td>
                                        <label class="control-label"><small>Frekuensi RX : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.frekuensiRx"/>
                                            <s:hidden id="enFrekuensiRx" name="alatDetail.enFrekuensiRx"/>
                                        </div>
                                    </td>
                                </tr>

                                <tr id="disDaya" style="display: none">
                                    <td>
                                        <label class="control-label"><small>Daya : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.daya"/>
                                            <s:hidden id="enDaya" name="alatDetail.enDaya"/>
                                        </div>
                                    </td>
                                </tr>

                                <tr id="disTransmisi" style="display: none">
                                    <td>
                                        <label class="control-label"><small>Transmisi : </small></label>
                                    </td>
                                    <td>
                                        <div class="label">
                                            <s:property value="alatDetail.transmisi"/>
                                            <s:hidden id="enTransmisi" name="alatDetail.enTransmisi"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:block;">
                                    <td>
                                        <label class="control-label"><small>Detail :</small></label>
                                    </td>
                                    <td>
                                        <s:url var="urlViewDoc" namespace="/alatdetail" action="viewSpesifikasi_alatdetail"
                                               escapeAmp="false">
                                            <s:param name="id"><s:property value="alatDetail.idAlatDetail" /></s:param>
                                            <s:param name="kodeAlat"><s:property value="alatDetail.kodeAlat" /></s:param>
                                        </s:url>
                                        <sj:a onClickTopics="showDialogViewDoc" href="%{urlViewDoc}">
                                            <img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print" width="25px">
                                        </sj:a>
                                    </td>
                                </tr>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>

                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-default" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelBtn();">
                                    <i class="icon-remove-circle "/> Cancel
                                </button>
                            </td>
                        </tr>
                    </table>

                </fieldset>
            </s:form>

            <table>
                <%--<tr>--%>
                <%--<td>--%>
                <%--<img src="pages/images/doc/irvean_images.jpg">--%>
                <%--</td>--%>
                <%--</tr>--%>
            </table>

        </td>
        <td>
            <tr>
                <td>

                </td>
            </tr>
        </td>
    </tr>
</table>

</body>
</html>


