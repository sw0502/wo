<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">

        $.subscribe('beforeProcessSave', function (event, data) {


            var stStartDate = document.getElementById('stStartDate').value;
            var stEndDate = document.getElementById('stEndDate').value;

            if (stStartDate !='' && stEndDate !='' ) {

                //dd-mm-yyyy

                var startDateValue = stStartDate.split('-');
                var endDateValue = stEndDate.split('-');

                var startDate=new Date();
                startDate.setFullYear(startDateValue[2],(startDateValue[1] - 1 ),startDateValue[0]);

                var endDate=new Date();
                endDate.setFullYear(endDateValue[2],(endDateValue[1] - 1 ),endDateValue[0]);

                var nowDate = new Date();

                if (nowDate <= startDate &&  startDate < endDate) {
                    if (confirm('Do you want to save this record ?')) {
                        event.originalEvent.options.submit = true;
                        $.publish('showDialog');

                    } else {
                        // Cancel Submit comes with 1.8.0
                        event.originalEvent.options.submit = false;
                    }
                } else {

                    event.originalEvent.options.submit = false;

                    var msg = 'Field <strong>Start Date must equal or greater than Now Date, and End Date must greater than Start Date</strong>.' + '<br/>';

                    document.getElementById('errorValidationMessage').innerHTML = msg;

                    $.publish('showErrorValidationDialog');

                }
            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (document.getElementById("stStartDate").value == '') {
                    msg = msg + 'Field <strong>Start Date</strong> is required.' + '<br/>';
                }

                if (document.getElementById("stEndDate").value == '') {
                    msg = msg + 'Field <strong>End Date</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessage').innerHTML = msg;

                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialog', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialog');
            }
        });

        $.subscribe('errorDialog', function (event, data) {

            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function callSearchFunction() {
            $('#info_dialog').dialog('close');

            document.editSKArealForm.action='searchHcmEdit_skareal.action';
            document.editSKArealForm.submit();

        };

        function closeActivation() {
            $('#view_dialog_ViewProject').dialog('close');
        }


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="aktivasiSKArealForm" method="post" namespace="/project" action="" enctype="multipart/form-data"
                    cssClass="well form-horizontal">

                <fieldset>
                    <%--<legend align="left">Detail Project</legend>--%>

                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>
                    <table>
                            <tr>
                                <td>
                                    <object data="<s:property value="alatDetail.filePath"/>" type="application/pdf" width="800" height="900">
                                        <%--<p>Alternative text - include a link <a href="myfile.pdf">to the PDF!</a></p>--%>
                                    </object>
                                    <%--<iframe src="<s:property value="project.pathfile"/>" width="100%" height="100%">--%>
                                        <%--This browser does not support PDFs. Please download the PDF to view it: <a href="<s:property value="project.pathfile"/>">Download PDF</a>--%>
                                    <%--</iframe>--%>
                                        <%--<iframe src="http://docs.google.com/gview?url=<s:property value="project.pathfile"/>&embedded=true" style="width:600px; height:600px;" frameborder="0"></iframe>--%>
                                    <%--<iframe src="localhost:8080<s:property value="project.pathfile"/>&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>--%>
                                </td>
                            </tr>
                            <%--<tr>--%>
                                <%--<td>--%>
                                    <%--<img src="<s:property value="project.pathfile"/>" width="900px"/>--%>
                                <%--</td>--%>
                            <%--</tr>--%>
                    </table>

                </fieldset>
                <div id="actions" class="form-actions">
                    <div id="crud">
                        <table>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table>
                    <tr>
                        <td>
                            <table>
                            </table>
                        </td>
                    </tr>
                </table>

            </s:form>

            <table>
            </table>

        </td>
    </tr>
</table>

</body>
</html>


