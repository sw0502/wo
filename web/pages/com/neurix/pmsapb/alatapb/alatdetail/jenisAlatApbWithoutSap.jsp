<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AppAction.js"/>'></script>
    <script type="text/javascript">
//        function cek(){
//            if (nosap != ''){
//                var nosap = document.getElementById('noSap').value;
//                window.location.href='cekTersedia_alatdetail.action?id='+nosap;
//            } else {
//                alert("No. Sap Harus Diisi")
//            }
//        }
//
        function choose(){
             document.alatForm.submit();
        }
    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="alatForm" method="post"  namespace="/alatdetail" action="addAlat_alatdetail"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Choose Alat</legend>

                                        <table>
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <%--<tr>--%>
                                                            <%--<td>--%>
                                                                <%--<label class="control-label" >No. SAP :</label>--%>
                                                            <%--</td>--%>

                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                    <%--<s:textfield id="noSap" name="alatDetail.noSap" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                        <%--</tr>--%>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" >Jenis Alat :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:select list="#{'64':'TELEPROTEKSI','63':'PLC','65':'PAX', '68':'MUX', '62':'RTU', '76':'SWITCH' ,'21':'RELAY','24':'BATTERY','25':'RECTIFIER', '95':'SAS'}" id="kodeAlat" name="alatDetail.kodeAlat"
                                                                              headerKey="" headerValue="[Select One]" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                </table>
                                                            </td>
                                                            <s:hidden name="tipe" value="N"></s:hidden>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <table>
                                            <tr>

                                                <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<button type="button" class="btn btn-primary" onclick="cek()">--%>
                                                            <%--<i class="icon-search icon-white"></i>--%>
                                                            <%--Cek--%>
                                                        <%--</button>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>




                                                <%--<td>--%>
                                                    <%--<table>--%>

                                                        <%--<sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"--%>
                                                                   <%--position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">--%>
                                                            <%--Please don't close this window, server is processing your request ...--%>
                                                            <%--</br>--%>
                                                            <%--</br>--%>
                                                            <%--</br>--%>
                                                            <%--<img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">--%>
                                                        <%--</sj:dialog>--%>
                                                        <%--<sj:submit type="button" cssClass="btn btn-primary" formIds="alatForm" id="search" name="search"--%>
                                                                   <%--onClickTopics="showDialog" onCompleteTopics="closeDialog">--%>
                                                            <%--<i class="icon-plus icon-white"></i>--%>
                                                            <%--Choose--%>
                                                        <%--</sj:submit>--%>

                                                    <%--</table>--%>
                                                <%--</td>--%>

                                                <td>
                                                    <table>

                                                        <button type="button" class="btn btn-primary" onclick="choose()">
                                                           <i class="icon-plus icon-white"></i>
                                                            Choose
                                                        </button>

                                                    </table>
                                                </td>


                                                <td>
                                                    <table>

                                                        <button type="button" class="btn" onclick="window.location.href='<s:url action="initForm_alatdetail"/>'">
                                                            <i class="icon-repeat"></i> Back
                                                        </button>

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>



                                    <sj:dialog id="view_dialog_log" openTopics="showDialogLog" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="1000" autoOpen="false" title="History Log Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete" openTopics="showDialogDelete" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="700" width="600" autoOpen="false" title="Delete Data Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit" openTopics="showDialogEditAlatDetail" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="800" width="700" autoOpen="false" title="Edit data Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_alat_detail" openTopics="showDialogViewAlatDetail" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="700" width="600" autoOpen="false" title="View Data Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_catatan" openTopics="showDialogViewCatatan" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="1000" autoOpen="false" title="History Pemeliharaan Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="waiting_dialog_print" openTopics="showDialogPrintQR" closeTopics="closeDialogPrintQR" modal="true" resizable="false"
                                               position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                        Please don't close this window, server is processing your request ...
                                        </br>
                                        </br>
                                        </br>
                                        <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                    </sj:dialog>

                                    <%--<table width="100%">--%>
                                        <%--<tr>--%>
                                            <%--<td align="center">--%>

                                                <%--<s:set name="listOfAlatDetail" value="#session.listOfResult" scope="request" />--%>
                                                <%--<display:table name="listOfAlatDetail" class="table table-condensed table-striped table-hover"--%>
                                                               <%--requestURI="paging_displaytag_alatdetail.action" export="true" id="row" pagesize="50" style="font-size:10">--%>

                                                <%--<display:column property="idAlatDetail" sortable="true" title="<small>No. Detail Alat</small>" />--%>
                                                <%--<display:column property="noSap" sortable="true" title="<small>No. SAP</small>" />--%>
                                                <%--<display:column property="noAktiva" sortable="true" title="<small>No. Aktiva</small>" />--%>
                                                <%--<display:column property="kodeAlat" sortable="true" title="<small>Kode Alat</small>" />--%>
                                                <%--<display:column property="merk" sortable="true" title="<small>Merk</small>" />--%>
                                                <%--<display:column property="thumbFoto" sortable="true" title="<small>Foto Alat</small>" />--%>
                                                <%--<display:column property="noSeries" sortable="true" title="<small>No. Series</small>" />--%>
                                                <%--<display:column property="namaAlatDetail" sortable="true" title="<small>Jenis Alat</small>" />--%>
                                                <%--<display:column property="kodeApp" sortable="true" title="<small>Kode APP</small>"  />--%>
                                                <%--<display:column property="namaApp" sortable="true" title="<small>Wilayah APP</small>" />--%>
                                                <%--<display:column property="noGi" sortable="true" title="<small>No. GI</small>" />--%>
                                                <%--<display:column property="namaGi" sortable="true" title="<small>Gardu</small>" />--%>
                                                <%--<display:column property="noGiTujuan" sortable="true" title="<small>No. Gardu Tujuan</small>" />--%>
                                                <%--<display:column property="namaGiTujuan" sortable="true" title="<small>Gardu Tujuan</small>" />--%>
                                                <%--<display:column property="status" sortable="true" title="<small>Status Alat</small>" />--%>




                                                <%--<display:setProperty name="paging.banner.item_name">Project</display:setProperty>--%>
                                                <%--<display:setProperty name="paging.banner.items_name">Project</display:setProperty>--%>
                                                <%--<display:setProperty name="export.excel.filename">AlatDetail.xls</display:setProperty>--%>
                                                <%--<display:setProperty name="export.csv.filename">AlatDetail.csv</display:setProperty>--%>

                                                <%--</display:table>--%>
                                            <%--</td>--%>

                                        <%--</tr>--%>
                                    <%--</table>--%>

                                </s:form>



                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>