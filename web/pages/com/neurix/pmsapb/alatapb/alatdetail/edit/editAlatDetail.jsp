<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AppAction.js"/>'></script>
    <script type="text/javascript">

    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="editForm" method="post"  namespace="/alatdetail" action="saveEdit_alatdetail" enctype="multipart/form-data"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Edit Data Alat</legend>

                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Kode App :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:textfield id="kodeApp" name="alatDetail.kodeApp" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />

                                                                    <script>
                                                                        var functions, mapped;
                                                                        $('#kodeApp').typeahead({
                                                                            minLength: 1,
                                                                            source: function (query, process) {
                                                                                functions = [];
                                                                                mapped = {};

                                                                                var data = [];
                                                                                dwr.engine.setAsync(false);
                                                                                AppAction.initComboAppSearch(query, function (listdata) {
                                                                                    data = listdata;
                                                                                });

                                                                                $.each(data, function (i, item) {
                                                                                    var labelItem = item.namaWilayah;
                                                                                    mapped[labelItem] = { id: item.kodeApp, label: labelItem };
                                                                                    functions.push(labelItem);
                                                                                });

                                                                                process(functions);
                                                                            },
                                                                            updater: function (item) {
                                                                                var selectedObj = mapped[item];
                                                                                //var namaMember = selectedObj.label;
                                                                                //document.getElementById("namaMember").value = namaMember;
                                                                                return selectedObj.id;
                                                                            }
                                                                        });
                                                                    </script>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="alatDetail.noSap">No. SAP :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noSap" name="alatDetail.noSap" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" >Gardu Induk :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noGi" name="alatDetail.noGi" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    <script>
                                                                        var functions, mapped;
                                                                        $('#noGi').typeahead({
                                                                            minLength: 1,
                                                                            source: function (query, process) {
                                                                                functions = [];
                                                                                mapped = {};

                                                                                var data = [];
                                                                                dwr.engine.setAsync(false);
                                                                                AlatDetailAction.initComboGardu(query, function (listdata) {
                                                                                    data = listdata;
                                                                                });

                                                                                $.each(data, function (i, item) {
                                                                                    var labelItem = item.namaGarduInduk;
                                                                                    mapped[labelItem] = { id: item.nomorGarduInduk, label: labelItem };
                                                                                    functions.push(labelItem);
                                                                                });

                                                                                process(functions);
                                                                            },
                                                                            updater: function (item) {
                                                                                var selectedObj = mapped[item];
                                                                                var namaGi = selectedObj.label;
//                                                                                document.getElementById("namaGi").value = namaGi;
//                                                                                document.getElementById("namaGiH").value = namaGi;

                                                                                return selectedObj.id;
                                                                            }
                                                                        });
                                                                        //
                                                                        //
                                                                    </script>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Nama Gi :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaGi" name="alatDetail.namaGi" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Bidang :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaBidang" name="alatDetail.namaBidang" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                    <s:hidden name="alatDetail.idAlatDetail"/>
                                                                    <s:hidden name="alatDetail.idBidang"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Sub Bidang :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaSubBidang" name="alatDetail.namaSubBidang" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                    <s:hidden name="alatDetail.idSubBidang"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Nama Peralatan :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaAlatDetail" name="alatDetail.namaAlatDetail" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">No. Peralatan :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="kodeAlat" name="alatDetail.kodeAlat" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                    <s:hidden name="kodeAlat"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Terpasang di ruang :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="ruangTerpasang" name="alatDetail.ruangTerpasang" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Nomor Seri :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noSeries" name="alatDetail.noSeries" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Type :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="typeId" name="alatDetail.typeId" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Merk :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="merk" name="alatDetail.merk" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="alatDetail.mode">Mode :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:select list="#{'Integrated':'Integrated','Independent':'Independent'}" id="mode" name="alatDetail.mode"
                                                                              headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label">No. Aktiva :</label>
                                                        </td>

                                                        <td>
                                                            <table>
                                                                <s:textfield id="noAktiva" name="alatDetail.noAktiva" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label"><T>TECHIDENTNO :</T></label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="techidentno" name="alatDetail.techidentno" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="alatDetail.status">Status Alat :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:select list="#{'Active':'Active','Non-Active':'Non-Active','Gudang':'Gudang'}" id="status1" name="alatDetail.status"
                                                                              headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" >Tanggal Operasi :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <sj:datepicker id="projectStartDateForm" name="alatDetail.stTglOprs" displayFormat="dd-mm-yy"
                                                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />

                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Power Supply :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="powerSupply" name="alatDetail.powerSupply" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr style="display: block">
                                                <td>
                                                    <table>
                                                        <label class="control-label"><small>Gi Tujuan :</small></label>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table>
                                                        <s:textfield id="noGiTujuan" name="alatDetail.noGiTujuan"  cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                        <script>
                                                            var functions, mapped;
                                                            $('#noGiTujuan').typeahead({
                                                                minLength: 1,
                                                                source: function (query, process) {
                                                                    functions = [];
                                                                    mapped = {};

                                                                    var data = [];
                                                                    dwr.engine.setAsync(false);
                                                                    AlatDetailAction.initComboGardu(query, function (listdata) {
                                                                        data = listdata;
                                                                    });

                                                                    $.each(data, function (i, item) {
                                                                        var labelItem = item.namaGarduInduk;
                                                                        mapped[labelItem] = { id: item.nomorGarduInduk, label: labelItem };
                                                                        functions.push(labelItem);
                                                                    });

                                                                    process(functions);
                                                                },
                                                                updater: function (item) {
                                                                    var selectedObj = mapped[item];
                                                                    var namaGiTujuan = selectedObj.label;
                                                                    document.getElementById("namaGiTujuan").value = namaGiTujuan;
                                                                    document.getElementById("namaGiTujuanH").value = namaGiTujuan;

                                                                    return selectedObj.id;
                                                                }
                                                            });
                                                            //
                                                            //
                                                        </script>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Nama Gi Tujuan :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaGiTujuan" name="alatDetail.namaGiTujuan" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <s:if test="isEnablePlc()">

                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Bandwith :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="bandwith" name="alatDetail.bandwith" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Frekuensi Tx :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="frekuensiTx" name="alatDetail.frekuensiTx" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Frekuensi Rx :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="frekuensiRx" name="alatDetail.frekuensiRx" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </s:if>

                                            <s:if test="isEnableRectifier()">
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Type :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="typeRect" name="alatDetail.typeId" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Nama Rectifier :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="rectifierName" name="alatDetail.rectifierName" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Tegangan Input (VAC) :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="teganganInput" name="alatDetail.teganganInput" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">(PHASE) :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="phase" name="alatDetail.phase" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Tegangan Output :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="teganganOutput" name="alatDetail.teganganOutput" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Kapasitas (A) :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="kapasitasRect" name="alatDetail.kapasitas" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">MCB input (A) :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="mcbInput" name="alatDetail.mcbInput" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">MCB output (A) :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="mcbOutput" name="alatDetail.mcbOutput" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Fungsi :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="fungsirect" name="alatDetail.fungsi" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </s:if>

                                            <s:if test="isEnableBattery()">
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Nama Battery :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="batteryName" name="alatDetail.batteryName" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Tegangan :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="tegangan" name="alatDetail.tegangan" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Kapasitas (AH) :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="kapasitasBat" name="alatDetail.kapasitas" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Fuse/MCB (A) :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="fuse" name="alatDetail.fuse" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Fungsi :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="fungsi" name="alatDetail.fungsi" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </s:if>

                                            <s:if test="isEnableRtu()">
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Nama RTU :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="namaRtu" name="alatDetail.namaRtu" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Id RTU :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="idRtu" name="alatDetail.idRtu" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </s:if>

                                            <s:if test="isEnableSwitch()">
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">Type Switch :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="typeSwitch" name="alatDetail.typeId" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </s:if>


                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Upload Foto :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:file id="uploadFront" name="uploadFront" cssClass="btn-mini" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <div id="crud">
                                            <table>
                                                <tr>

                                                    <td>
                                                        <table>

                                                                <%--<sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"--%>
                                                                <%--position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">--%>
                                                                <%--Please don't close this window, server is processing your request ...--%>
                                                                <%--</br>--%>
                                                                <%--</br>--%>
                                                                <%--</br>--%>
                                                                <%--<img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">--%>
                                                                <%--</sj:dialog>--%>
                                                                <%--<sj:submit type="button" cssClass="btn btn-primary" formIds="addProjectForm" id="save" name="save"--%>
                                                                <%--onClickTopics="showDialog" onCompleteTopics="closeDialog">--%>
                                                                <%--<i class="icon-ok icon-white"></i>--%>
                                                                <%--Save--%>
                                                                <%--</sj:submit>--%>

                                                                    <sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"
                                                                               position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                                                        Please don't close this window, server is processing your request ...
                                                                        </br>
                                                                        </br>
                                                                        </br>
                                                                        <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                                    </sj:dialog>
                                                                    <sj:submit type="button" cssClass="btn btn-primary" formIds="editForm" id="search" name="search"
                                                                               onClickTopics="showDialog" onCompleteTopics="closeDialog">
                                                                        Save
                                                                    </sj:submit>
                                                        </table>
                                                    </td>

                                                    <%--<td>--%>
                                                        <%--<table>--%>
                                                            <sj:dialog id="view_dialog_add_user" openTopics="showDialogAddUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                       position="center" height="400" width="600" autoOpen="false" title="Spesifikasi"
                                                            >
                                                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                            </sj:dialog>

                                                            <%--<s:url var="urlAddUser" namespace="/alatdetail" action="addKapasitasMux_alatdetail"--%>
                                                                   <%--escapeAmp="false">--%>
                                                            <%--</s:url>--%>
                                                            <%--<sj:a onClickTopics="showDialogAddUser" href="%{urlAddUser}" cssClass="btn btn-warning">--%>
                                                                <%--<i class=""></i>--%>
                                                                <%--<small>Add Kapasitas Mux</small>--%>
                                                            <%--</sj:a>--%>
                                                        <%--</table>--%>
                                                    <%--</td>--%>

                                                    <%--<td>--%>
                                                        <%--<table>--%>

                                                            <%--<button type="button" class="btn" onclick="window.location.href='<s:url action="initAdd_alatdetail"/>' ">--%>
                                                                <%--<i class="icon-repeat"></i> Reset--%>
                                                            <%--</button>--%>

                                                        <%--</table>--%>
                                                    <%--</td>--%>


                                                    <td>
                                                        <table>

                                                            <button type="button" class="btn btn-default" onclick="window.location.href='<s:url action="initForm_alatdetail"/>' ">
                                                                <i class="icon-chevron-left icon-black"></i> Back
                                                            </button>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <sj:dialog id="view_dialog_edit_user" openTopics="showDialogEditUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Edit User">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit_issue" openTopics="showDialogEditIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Edit Issue">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_issue" openTopics="showDialogDeleteIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Delete Issue">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_user" openTopics="showDialogDeleteUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Delete User">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit_task" openTopics="showDialogEditTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Edit a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_task" openTopics="showDialogDeleteTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Delete a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_add_member_task" openTopics="showDialogAddMemberTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Add Task Team">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <s:if test="isEnableTp()">
                                        <table width="20%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        TP Channel :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfAlat" value="#session.listOfTpChanel" scope="request" />
                                                    <display:table name="listOfAlat" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.chanelId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">tpchanel</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="chanel" sortable="true" title="<small>Channel</small>" />
                                                        <display:column property="fungsiChanel" sortable="true" title="<small>fungsi Channel</small>" />
                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>
                                    </s:if>

                                    <s:if test="isEnablePlc()">
                                        <table width="20%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        PLC Channel :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfAlat" value="#session.listOfPlcChanel" scope="request" />
                                                    <display:table name="listOfAlat" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.chanelId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">plcchanel</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="chanel" sortable="true" title="<small>Channel</small>" />
                                                        <display:column property="fungsiChanel" sortable="true" title="<small>Fungsi Channel</small>" />
                                                        <display:column property="frekuensi" sortable="true" title="<small>Frekuensi</small>" />
                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>
                                    </s:if>

                                    <s:if test="isEnableSwitch()">
                                        <table width="20%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        Switch Port :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfAlat" value="#session.listOfSwitchPort" scope="request" />
                                                    <display:table name="listOfAlat" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.switchPortId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">switchport</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="type" sortable="true" title="<small>Type</small>" />
                                                        <display:column property="port" sortable="true" title="<small>Port</small>" />
                                                        <display:column property="fungsiPort" sortable="true" title="<small>fungsi Port</small>" />
                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>
                                    </s:if>

                                    <s:if test="isEnableMux()">
                                        <table width="40%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        Kapasitas Mux :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfAlat" value="#session.listOfKapasitasMux" scope="request" />
                                                    <display:table name="listOfAlat" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.kapasitasId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">kapasitas</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="namaModul" sortable="true" title="<small>Nama Modul</small>" />
                                                        <display:column property="timeSlot" sortable="true" title="<small>Time Slot</small>" />
                                                        <display:column property="fungsi" sortable="true" title="<small>Fungsi</small>" />
                                                        <display:column property="kapasitas" sortable="true" title="<small>Kapasitas</small>" />
                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>
                                    </s:if>

                                    <%--<s:if test="isEnableBattery()">--%>
                                        <%--<table width="20%">--%>
                                            <%--<tr align="center">--%>
                                                <%--<td>--%>
                                                    <%--<h5>--%>
                                                        <%--Battery :--%>
                                                    <%--</h5>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>

                                            <%--<tr>--%>
                                                <%--<td align="center">--%>

                                                    <%--<s:set name="listOfAlat" value="#session.listOfBattery" scope="request" />--%>
                                                    <%--<display:table name="listOfAlat" class="table table-condensed table-striped table-hover"--%>
                                                                   <%--requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">--%>

                                                        <%--<display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">--%>

                                                            <%--<s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"--%>
                                                                   <%--escapeAmp="false">--%>
                                                                <%--<s:param name="id"><s:property value="#attr.row.batteryId" /></s:param>--%>
                                                                <%--<s:param name="status">edit</s:param>--%>
                                                                <%--<s:param name="tipe">battery</s:param>--%>
                                                            <%--</s:url>--%>
                                                            <%--<sj:a onClickTopics="showDialogAddUser" href="%{urlView}">--%>
                                                                <%--<img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">--%>
                                                            <%--</sj:a>--%>

                                                        <%--</display:column>--%>

                                                        <%--<display:column property="batteryName" sortable="true" title="<small>Battery</small>" />--%>
                                                        <%--<display:column property="tegangan" sortable="true" title="<small>Tegangan</small>" />--%>
                                                        <%--<display:column property="jumlahCell" sortable="true" title="<small>Jumlah Cell</small>" />--%>
                                                        <%--<display:column property="merk" sortable="true" title="<small>Merk</small>" />--%>
                                                        <%--<display:column property="type" sortable="true" title="<small>Type</small>" />--%>
                                                        <%--<display:column property="kapasitas" sortable="true" title="<small>Kapasitas (AH)</small>" />--%>
                                                        <%--<display:column property="fuse" sortable="true" title="<small>Fuse/MCB</small>" />--%>
                                                        <%--<display:column property="fungsi" sortable="true" title="<small>Fungsi</small>" />--%>
                                                    <%--</display:table>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>
                                        <%--</table>--%>
                                    <%--</s:if>--%>


                                    <s:if test="isEnableRectifier()">
                                        <table width="20%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        Kabel Supply :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfAlat" value="#session.listOfRectifier" scope="request" />
                                                    <display:table name="listOfAlat" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.rectifierId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">rectifier</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="panjangAcdb" sortable="true" title="<small>Panjang ACDB</small>" />
                                                        <display:column property="typeAcdb" sortable="true" title="<small>Type Kabel ACDB</small>" />
                                                        <display:column property="panjangBattery" sortable="true" title="<small>Panjang Battery</small>" />
                                                        <display:column property="typeBattery" sortable="true" title="<small>Type Kabel Battery</small>" />
                                                        <display:column property="panjangDcdb" sortable="true" title="<small>Panjang DCDB</small>" />
                                                        <display:column property="typeDcdb" sortable="true" title="<small>Type Kabel DCDB</small>" />

                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>
                                    </s:if>



                                    <s:if test="isEnableRtu()">
                                        <table width="20%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        Modul RTU :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfModulRtu" value="#session.listOfModulRtu" scope="request" />
                                                    <display:table name="listOfModulRtu" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.modulId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">modulrtu</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="modul" sortable="true" title="<small>Modul</small>" />
                                                        <display:column property="qty" sortable="true" title="<small>Qty</small>" />
                                                        <display:column property="jumlahPoint" sortable="true" title="<small>Jumlah Point</small>" />
                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>

                                        <table width="50%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        Bay RTU :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfBayRtu" value="#session.listOfBayRtu" scope="request" />
                                                    <display:table name="listOfBayRtu" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.bayId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">bayrtu</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="namaBay" sortable="true" title="<small>Nama Bay</small>" />
                                                        <display:column property="jumlahDi" sortable="true" title="<small>Jumlah DI</small>" />
                                                        <display:column property="jumlahDo" sortable="true" title="<small>Jumlah DO</small>" />
                                                        <display:column property="jumlahAo" sortable="true" title="<small>Jumlah AO</small>" />
                                                        <display:column property="jumlahAi" sortable="true" title="<small>Jumlah AI</small>" />
                                                        <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                                        <display:column property="ratioVt" sortable="true" title="<small>Ratio VT</small>" />
                                                        <display:column property="ratioCt" sortable="true" title="<small>Ratio CT</small>" />
                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>
                                    </s:if>

                                    <s:if test="isEnableProteksi()">
                                        <table width="40%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        Setting :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfProteksi" value="#session.listOfProteksiSetting" scope="request" />
                                                    <display:table name="listOfProteksi" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.proteksiSettingId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">proteksi</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="tahap" sortable="true" title="<small>Tahap</small>" />
                                                        <display:column property="arus" sortable="true" title="<small>Arus</small>" />
                                                        <display:column property="waktu" sortable="true" title="<small>Waktu</small>" />
                                                        <display:column property="target" sortable="true" title="<small>Target</small>" />
                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>
                                    </s:if>

                                    <s:if test="isEnableSas()">
                                        <table width="60%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        RTU Station :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfSasStation" value="#session.listOfSasStation" scope="request" />
                                                    <display:table name="listOfSasStation" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.sasStationId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">station</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="namaSatation" sortable="true" title="<small>Nama Satation</small>" />
                                                        <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                                        <display:column property="tipeStation" sortable="true" title="<small>Jenis Station</small>" />
                                                        <display:column property="tipe" sortable="true" title="<small>Tipe</small>" />
                                                        <display:column property="namaSoftWare" sortable="true" title="<small>Nama Software</small>" />
                                                        <display:column property="versiSoftware" sortable="true" title="<small>Versi Software</small>" />
                                                        <display:column property="jumlah" sortable="true" title="<small>Jumlah</small>" />
                                                        <display:column property="sn" sortable="true" title="<small>Serial Number</small>" />
                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>

                                        <table width="80%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        RTU Control :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfSasStation" value="#session.listOfSasControl" scope="request" />
                                                    <display:table name="listOfSasStation" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.sasControlId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">control</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="namaControl" sortable="true" title="<small>Nama</small>" />
                                                        <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                                        <display:column property="tipeControl" sortable="true" title="<small>Jenis</small>" />
                                                        <display:column property="tipe" sortable="true" title="<small>Tipe</small>" />
                                                        <display:column property="namaSoftWare" sortable="true" title="<small>Nama Software</small>" />
                                                        <display:column property="versiSoftware" sortable="true" title="<small>Versi Software</small>" />
                                                        <display:column property="jumlahDi" sortable="true" title="<small>Jumlah DI</small>" />
                                                        <display:column property="jumlahDo" sortable="true" title="<small>Jumlah DO</small>" />
                                                        <display:column property="jumlahAi" sortable="true" title="<small>Jumlah AI</small>" />
                                                        <display:column property="ratioVt" sortable="true" title="<small>Ratio VT</small>" />
                                                        <display:column property="ratioCt" sortable="true" title="<small>RATIO CT</small>" />
                                                        <display:column property="sn" sortable="true" title="<small>Serial Number</small>" />
                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>

                                        <table width="60%">
                                            <tr align="center">
                                                <td>
                                                    <h5>
                                                        RTU Perangkat :
                                                    </h5>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center">

                                                    <s:set name="listOfSasPerangkat" value="#session.listOfSasPerangkat" scope="request" />
                                                    <display:table name="listOfSasPerangkat" class="table table-condensed table-striped table-hover"
                                                                   requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                        <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                                            <s:url var="urlView" namespace="/alatdetail" action="initSpecEdit_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.sasPerangkatId" /></s:param>
                                                                <s:param name="status">edit</s:param>
                                                                <s:param name="tipe">perangkat</s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlView}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">
                                                            </sj:a>

                                                        </display:column>

                                                        <display:column property="namaPerangkat" sortable="true" title="<small>Nama</small>" />
                                                        <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                                        <display:column property="tipePerangkat" sortable="true" title="<small>Jenis</small>" />
                                                        <display:column property="type" sortable="true" title="<small>Tipe</small>" />
                                                        <display:column property="jumlah" sortable="true" title="<small>Jumlah</small>" />
                                                        <display:column property="sn" sortable="true" title="<small>Serial Number</small>" />
                                                    </display:table>
                                                </td>
                                            </tr>
                                        </table>
                                    </s:if>
                                </s:form>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>


