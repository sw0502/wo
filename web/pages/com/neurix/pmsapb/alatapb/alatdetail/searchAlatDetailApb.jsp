<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AppAction.js"/>'></script>
    <script type="text/javascript">

    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="alatDetailForm" method="post"  namespace="/alatdetail" action="search_alatdetail"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Search Alat Detail</legend>

                                        <table>
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                        <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="alatDetail.kodeApp">Kode APP :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="kodeAppSearch" name="alatDetail.kodeApp" cssClass="textForm"/>
                                                                    <script>
                                                                        var functions, mapped;
                                                                        $('#kodeAppSearch').typeahead({
                                                                            minLength: 1,
                                                                            source: function (query, process) {
                                                                                functions = [];
                                                                                mapped = {};

                                                                                var data = [];
                                                                                dwr.engine.setAsync(false);
                                                                                AppAction.initComboAppSearch(query, function (listdata) {
                                                                                    data = listdata;
                                                                                });

                                                                                $.each(data, function (i, item) {
                                                                                    var labelItem = item.kodeApp+" - "+item.namaWilayah;
                                                                                    mapped[labelItem] = { id: item.kodeApp, label: item.namaWilayah };
                                                                                    functions.push(labelItem);
                                                                                });

                                                                                process(functions);
                                                                            },
                                                                            updater: function (item) {
                                                                                var selectedObj = mapped[item];
                                                                                //var namaMember = selectedObj.label;
                                                                                //document.getElementById("namaMember").value = namaMember;
                                                                                return selectedObj.id;
                                                                            }
                                                                        });
                                                                    </script>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="alatDetail.noGi">No. GI :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noGiSearch" name="alatDetail.noGi" cssClass="textForm"/>
                                                                    <script>
                                                                        var functions, mapped;
                                                                        $('#noGiSearch').typeahead({
                                                                            minLength: 1,
                                                                            source: function (query, process) {
                                                                                functions = [];
                                                                                mapped = {};

                                                                                var data = [];
                                                                                dwr.engine.setAsync(false);
                                                                                AlatDetailAction.initComboGarduSearch(query, function (listdata) {
                                                                                    data = listdata;
                                                                                });

                                                                                $.each(data, function (i, item) {
                                                                                    var labelItem = item.nomorGarduInduk+" - "+item.namaGarduInduk;
                                                                                    mapped[labelItem] = { id: item.nomorGarduInduk, label: item.namaGarduInduk };
                                                                                    functions.push(labelItem);
                                                                                });

                                                                                process(functions);
                                                                            },
                                                                            updater: function (item) {
                                                                                var selectedObj = mapped[item];
//                                                                                var namaGi = selectedObj.label;
//                                                                                document.getElementById("namaGi").value = namaGi;
//                                                                                document.getElementById("namaGiH").value = namaGi;

                                                                                return selectedObj.id;
                                                                            }
                                                                        });
                                                                        //
                                                                        //
                                                                    </script>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="alatDetail.noSap">No. SAP :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noSap" name="alatDetail.noSap" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <%--<tr>--%>
                                                <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<tr>--%>
                                                            <%--<td>--%>
                                                                <%--<label class="control-label" for="alatDetail.noSap">No. Aktiva :</label>--%>
                                                            <%--</td>--%>

                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                    <%--<s:textfield id="noAktiva" name="alatDetail.noAktiva" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                        <%--</tr>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>

                                            <%--<tr>--%>
                                                <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<tr>--%>
                                                            <%--<td>--%>
                                                                <%--<label class="control-label" for="alatDetail.idAlatDetail">Kode Detail Alat :</label>--%>
                                                            <%--</td>--%>

                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                    <%--<s:textfield id="idAlatDetail" name="alatDetail.idAlatDetail" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                        <%--</tr>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>


                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="alatDetail.kodeAlat">Jenis Alat :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="kodeAlat" name="alatDetail.kodeAlat" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    <script>
                                                                        var functions, mapped;
                                                                        $('#kodeAlat').typeahead({
                                                                            minLength: 1,
                                                                            source: function (query, process) {
                                                                                functions = [];
                                                                                mapped = {};

                                                                                var data = [];
                                                                                dwr.engine.setAsync(false);
                                                                                AlatDetailAction.initComboAlat(query, function (listdata) {
                                                                                    data = listdata;
                                                                                });

                                                                                $.each(data, function (i, item) {
                                                                                    var labelItem = item.kodeAlat + " - " + item.namaAlat + " - " + item.keterangan;
                                                                                    mapped[labelItem] = { id: item.kodeAlat, label: item.namaAlat};
                                                                                    functions.push(labelItem);
                                                                                });

                                                                                process(functions);
                                                                            },
                                                                            updater: function (item) {
                                                                                var selectedObj = mapped[item];
//                                                                                var namaGi = selectedObj.label;
//                                                                                document.getElementById("namaGi").value = namaGi;
//                                                                                document.getElementById("namaGiH").value = namaGi;

                                                                                return selectedObj.id;
                                                                            }
                                                                        });
                                                                        //
                                                                        //
                                                                    </script>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <%--<tr>--%>
                                                <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<tr>--%>
                                                            <%--<td>--%>
                                                                <%--<label class="control-label" for="alatDetail.namaAlatDetail">Nama Alat :</label>--%>
                                                            <%--</td>--%>

                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                    <%--<s:textfield id="namaAlatDetail" name="alatDetail.namaAlatDetail" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                        <%--</tr>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                    <label class="control-label" for="alatDetail.status">Status Alat :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:select list="#{'Active':'Active','Non-Active':'Non-Active','Gudang':'Gudang'}" id="status" name="alatDetail.status"
                                                                              headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <table>
                                            <tr>

                                                <td>
                                                    <table>

                                                        <sj:dialog id="waiting_dialog" openTopics="showDialogSearch" closeTopics="closeDialogSearch" modal="true" resizable="false"
                                                                   position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                                            Please don't close this window, server is processing your request ...
                                                            </br>
                                                            </br>
                                                            </br>
                                                            <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                        </sj:dialog>
                                                        <sj:submit type="button" cssClass="btn btn-primary" formIds="alatDetailForm" id="searchAlatDetail" name="searchAlatDetail"
                                                                   onClickTopics="showDialogSearch" onCompleteTopics="closeDialogSearch">
                                                            <i class="icon-search icon-white"></i>
                                                            Cari
                                                        </sj:submit>

                                                    </table>
                                                </td>
                                                <s:if test="isEnableBtn()">
                                                <td>
                                                    <table>

                                                        <s:url id="urlAdd" namespace="/alatdetail" action="initAdd_alatdetail" escapeAmp="false"/>
                                                        <s:url id="urlAddNotSap" namespace="/alatdetail" action="initAddNotSap_alatdetail" escapeAmp="false"/>

                                                            <div class="btn-group">
                                                                <button class="btn dropdown-toggle btn-primary" data-toggle="dropdown">
                                                                    <i class="icon-briefcase icon-white"></i>
                                                                    Tambah Alat
                                                                    <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li><s:a href="%{urlAdd}"><i class="icon-plus"></i> Tambah Alat dengan No SAP</s:a></li>
                                                                    <li><s:a href="%{urlAddNotSap}"><i class="icon-plus"></i> Tambah Alat tanpa No SAP</s:a></li>
                                                                </ul>
                                                            </div>
                                                    </table>
                                                </td>
                                                </s:if>
                                            <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<button type="button" class="btn btn-primary" onclick="window.location.href='<s:url action="initAdd_alatdetail"/>' ">--%>
                                                            <%--<i class="icon-plus icon-white"></i> Add Alat--%>
                                                        <%--</button>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>
                                                    <td>
                                                        <table>
                                                            <sj:dialog id="waiting_dialog" openTopics="showDialogSearch" closeTopics="closeDialogSearch" modal="true" resizable="false"
                                                                       position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                                                Please don't close this window, server is processing your request ...
                                                                </br>
                                                                </br>
                                                                </br>
                                                                <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                            </sj:dialog>

                                                            <td>
                                                                <table>

                                                                    <button type="button" class="btn btn-primary" onclick="window.location.href='<s:url action="printReportAlat_alatdetail"/>' ">
                                                                        <i class="icon-print icon-white"></i>
                                                                        Download PDF
                                                                    </button>

                                                                </table>
                                                            </td>

                                                        </table>

                                                    </td>

                                                    <td>
                                                        <table>
                                                                <%--<sj:dialog id="waiting_dialog" openTopics="showDialogSearch" closeTopics="closeDialogSearch" modal="true" resizable="false"--%>
                                                                <%--position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">--%>
                                                                <%--Please don't close this window, server is processing your request ...--%>
                                                                <%--</br>--%>
                                                                <%--</br>--%>
                                                                <%--</br>--%>
                                                                <%--<img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">--%>
                                                                <%--</sj:dialog>--%>

                                                            <td>
                                                                <table>

                                                                    <button type="button" class="btn btn-primary" onclick="window.location.href='<s:url action="downloadXls_alatdetail"/>' ">
                                                                        <i class="icon-print icon-white"></i>
                                                                        Download XLS
                                                                    </button>

                                                                </table>
                                                            </td>

                                                        </table>

                                                    </td>
                                                <td>
                                                    <table>

                                                        <button type="button" class="btn" onclick="window.location.href='<s:url action="initForm_alatdetail"/>' ">
                                                            <i class="icon-repeat"></i> Reset
                                                        </button>

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>



                                    <sj:dialog id="view_dialog_log" openTopics="showDialogLog" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="1000" autoOpen="false" title="History Log Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete" openTopics="showDialogDelete" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="700" width="600" autoOpen="false" title="Delete Data Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit" openTopics="showDialogEditAlatDetail" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="800" width="700" autoOpen="false" title="Edit data Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_alat_detail" openTopics="showDialogViewAlatDetail" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="700" width="600" autoOpen="false" title="View Data Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_catatan" openTopics="showDialogViewCatatan" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="1000" autoOpen="false" title="History Pemeliharaan Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="waiting_dialog_print" openTopics="showDialogPrintQR" closeTopics="closeDialogPrintQR" modal="true" resizable="false"
                                               position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                        Please don't close this window, server is processing your request ...
                                        </br>
                                        </br>
                                        </br>
                                        <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                    </sj:dialog>

                                    <table width="100%">
                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfAlatDetail" value="#session.listOfResult" scope="request" />
                                                <display:table name="listOfAlatDetail" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_alatdetail.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                    <display:column media="html" title="<small>View Log</small>" style="text-align:center;font-size:9">

                                                        <s:url var="urlView" namespace="/alatdetail" action="viewAlatDetailLog_alatdetail"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogLog" href="%{urlView}">
                                                            <img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print" width="25px">
                                                        </sj:a>

                                                    </display:column>

                                                    <display:column media="html" title="<small>View</small>" style="text-align:center;font-size:9">

                                                        <s:url var="urlView" namespace="/alatdetail" action="viewAlatDetail_alatdetail"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogViewAlatDetail" href="%{urlView}">
                                                            <img border="0" src="<s:url value="/pages/images/view_detail_project.png"/>" name="icon_print" width="25px">
                                                        </sj:a>

                                                    </display:column>

                                                    <%--<display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">--%>

                                                        <%--<s:url var="urlEdit" namespace="/alatdetail" action="initEdit_alatdetail"--%>
                                                               <%--escapeAmp="false">--%>
                                                            <%--<s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>--%>
                                                        <%--</s:url>--%>
                                                        <%--<sj:a onClickTopics="showDialogEditAlatDetail" href="%{urlEdit}">--%>
                                                            <%--<img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_print" width="25px">--%>
                                                        <%--</sj:a>--%>

                                                    <%--</display:column>--%>

                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:10">
                                                        <s:if test = "%{#attr.row.enBtn}" >
                                                            <s:url value="editAlat_alatdetail.action" var="editAlat" >
                                                                <s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>
                                                                <s:param name="kodeAlat"><s:property value="#attr.row.kodeAlat" /></s:param>
                                                            </s:url>
                                                            <s:a href="%{editAlat}"><img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_edit" width="25px"></s:a>
                                                        </s:if>
                                                        <s:else>
                                                            <img border="0" src="<s:url value="/pages/images/icon_not_edit.png"/>" name="icon_not_edit">
                                                        </s:else>


                                                    </display:column>

                                                    <display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">

                                                        <s:if test = "%{#attr.row.enBtn}" >
                                                            <s:url var="urlDelete" namespace="/alatdetail" action="initDelete_alatdetail"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>
                                                                <s:param name="kodeAlat"><s:property value="#attr.row.kodeAlat" /></s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogDelete" href="%{urlDelete}">
                                                                <img border="0" src="<s:url value="/pages/images/delete_task.png"/>" name="icon_print" width="25px">
                                                            </sj:a>
                                                        </s:if>
                                                        <s:else>
                                                            <img border="0" src="<s:url value="/pages/images/icon_not_edit.png"/>" name="icon_not_edit">
                                                        </s:else>
                                                    </display:column>

                                                    <display:column media="html" title="<small>Print Barcode</small>" style="text-align:center;font-size:9">

                                                        <s:if test = "%{#attr.row.enBtn}" >
                                                            <a href='/pmsapb/alatdetail/printQRCode_alatdetail.action?id=<s:property value="#attr.row.idAlatDetail"/>' target="_blank" >
                                                                <img border="0" src="<s:url value="/pages/images/icon_printer_new.ico"/>" name="icon_print" width="25px">
                                                            </a>
                                                        </s:if>
                                                        <s:else>
                                                            <img border="0" src="<s:url value="/pages/images/icon_not_edit.png"/>" name="icon_not_edit">
                                                        </s:else>
                                                    </display:column>


                                                    <display:column property="idAlatDetail" sortable="true" title="<small>No. Detail Alat</small>" />
                                                    <display:column property="noSap" sortable="true" title="<small>No. SAP</small>" />
                                                    <display:column property="noAktiva" sortable="true" title="<small>No. Aktiva</small>" />
                                                    <display:column property="kodeAlat" sortable="true" title="<small>Kode Alat</small>" />
                                                    <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                                    <display:column property="thumbFoto" sortable="true" title="<small>Foto Alat</small>" />
                                                    <display:column property="noSeries" sortable="true" title="<small>No. Series</small>" />
                                                    <display:column property="namaAlatDetail" sortable="true" title="<small>Jenis Alat</small>" />
                                                    <display:column property="kodeApp" sortable="true" title="<small>Kode APP</small>"  />
                                                    <display:column property="namaApp" sortable="true" title="<small>Wilayah APP</small>" />
                                                    <display:column property="noGi" sortable="true" title="<small>No. GI</small>" />
                                                    <display:column property="namaGi" sortable="true" title="<small>Gardu</small>" />
                                                    <display:column property="noGiTujuan" sortable="true" title="<small>No. Gardu Tujuan</small>" />
                                                    <display:column property="namaGiTujuan" sortable="true" title="<small>Gardu Tujuan</small>" />
                                                    <display:column property="status" sortable="true" title="<small>Status Alat</small>" />
                                                    <%--<display:column property="enDaya" sortable="true" title="<small>daya</small>" />--%>
                                                    <%--<display:column property="enFrekuensiRx" sortable="true" title="<small>Frekuensi Rx</small>" />--%>
                                                    <%--<display:column property="enFrekuensiTx" sortable="true" title="<small>Frekuensi Tx</small>" />--%>
                                                    <%--<display:column property="enTransmisi" sortable="true" title="<small>Transmisi</small>" />--%>
                                                    <%--<display:column property="enTegOprs" sortable="true" title="<small>Teg Operasi</small>" />--%>
                                                    <%--<display:column property="enJenisTp" sortable="true" title="<small>Jenis TP</small>" />--%>
                                                    <%--<display:column property="note" sortable="true" title="<small>Catatan</small>" />--%>

                                                    <display:column media="html" title="<small>View Catatan</small>" style="text-align:center;font-size:9">

                                                        <s:url var="urlViewCatatan" namespace="/alatdetail" action="initViewCatatan_alatdetail"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogViewCatatan" href="%{urlViewCatatan}">
                                                            <img border="0" src="<s:url value="/pages/images/view_project.png"/>" name="icon_print" width="25px">
                                                        </sj:a>

                                                    </display:column>

                                                    <display:setProperty name="paging.banner.item_name">Project</display:setProperty>
                                                    <display:setProperty name="paging.banner.items_name">Project</display:setProperty>
                                                    <display:setProperty name="export.excel.filename">AlatDetail.xls</display:setProperty>
                                                    <display:setProperty name="export.csv.filename">AlatDetail.csv</display:setProperty>
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                </s:form>



                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>


