<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AppAction.js"/>'></script>
    <script type="text/javascript">
        function cekAndSave(){
            var noSap = document.getElementById('noSap').value;
            var kodeAlat = document.getElementById('kodeAlat').value;

            var data = [];
            var temp = "";

            dwr.engine.setAsync(false);
            AlatDetailAction.cekByNoSap(noSap, kodeAlat, function(response){
                if (response == '00'){
                    AlatDetailAction.getListAlat(noSap, kodeAlat, function(listAlat){
                        data = listAlat;
                        $.each(data, function (i, item) {
                            temp += "no. Sap : "+item.noSap+", Jenis Alat : "+item.namaAlatDetail+", No. Series : "+item.noSeries+", APP : "+item.namaApp+", GI : "+item.namaGi+", GI Tujuan : "+item.namaGiTujuan+", status : "+item.status;
                        })
                    });
                    $("#cont").html(temp);
                    document.getElementById('alert').style.display = 'block';
                    document.getElementById('btnsave').disabled = 'true';
                } else {
                    document.addProjectForm.submit();
                }
            });
        }

        function saveData(){
            document.addProjectForm.submit();
        }

        function closeAlert(){
            document.getElementById('alert').style.display = 'none';
        }
    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="addProjectForm" method="post"  namespace="/alatdetail" action="saveAdd_alatdetail" enctype="multipart/form-data"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Add a Alat</legend>

                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>



                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Kode APP :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:textfield id="kodeApp" name="alatDetail.kodeApp" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />

                                                                    <script>
                                                                        var functions, mapped;
                                                                        $('#kodeApp').typeahead({
                                                                            minLength: 1,
                                                                            source: function (query, process) {
                                                                                functions = [];
                                                                                mapped = {};

                                                                                var data = [];
                                                                                dwr.engine.setAsync(false);
                                                                                AppAction.initComboAppSearch(query, function (listdata) {
                                                                                    data = listdata;
                                                                                });

                                                                                $.each(data, function (i, item) {
                                                                                    var labelItem = item.kodeApp+" - "+item.namaWilayah;
                                                                                    mapped[labelItem] = { id: item.kodeApp, label: labelItem };
                                                                                    functions.push(labelItem);
                                                                                });

                                                                                process(functions);
                                                                            },
                                                                            updater: function (item) {
                                                                                var selectedObj = mapped[item];
                                                                                var namaApp = selectedObj.label;
                                                                                document.getElementById("namaApp").value = namaApp;
                                                                                return selectedObj.id;
                                                                            }
                                                                        });
                                                                    </script>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Wilayah APP :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaApp" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" disabled="true" />
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <s:if test="isEnableSap()">
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label" for="alatDetail.noSap">No. SAP :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="noSap" name="alatDetail.noSap" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </s:if>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" >Gardu Induk :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noGi" name="alatDetail.noGi" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    <script>
                                                                        var functions, mapped;
                                                                        $('#noGi').typeahead({
                                                                            minLength: 1,
                                                                            source: function (query, process) {
                                                                                functions = [];
                                                                                mapped = {};

                                                                                var data = [];
                                                                                dwr.engine.setAsync(false);
                                                                                AlatDetailAction.initComboGardu(query, function (listdata) {
                                                                                    data = listdata;
                                                                                });

                                                                                $.each(data, function (i, item) {
                                                                                    var labelItem = item.namaGarduInduk;
                                                                                    mapped[labelItem] = { id: item.nomorGarduInduk, label: labelItem };
                                                                                    functions.push(labelItem);
                                                                                });

                                                                                process(functions);
                                                                            },
                                                                            updater: function (item) {
                                                                                var selectedObj = mapped[item];
                                                                                var namaGi = selectedObj.label;
                                                                                document.getElementById("namaGi").value = namaGi;
//                                                                                document.getElementById("namaGiH").value = namaGi;

                                                                                return selectedObj.id;
                                                                            }
                                                                        });
                                                                        //
                                                                        //
                                                                    </script>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <sj:dialog id="view_dialog_add" openTopics="showDialogAdd" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                               position="center" height="550" width="950" autoOpen="false" title="Tambah Gardu"
                                                                    >
                                                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                                    </sj:dialog>

                                                                    <s:url var="urlAdd" namespace="/gardu" action="add_gardu"
                                                                           escapeAmp="false">
                                                                    </s:url>
                                                                    <sj:a onClickTopics="showDialogAdd" href="%{urlAdd}">
                                                                        <img border="0" src="<s:url value="/pages/images/icon_add_desa.png"/>"
                                                                    </sj:a>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Nama Gi :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaGi" name="alatDetail.namaGi" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Bidang :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaBidang" name="alatDetail.namaBidang" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                    <s:hidden name="alatDetail.idBidang"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Sub Bidang :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaSubBidang" name="alatDetail.namaSubBidang" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                    <s:hidden name="alatDetail.idSubBidang"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Nama Peralatan :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaAlatDetail" name="alatDetail.namaAlatDetail" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">No. Peralatan :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="kodeAlat" name="alatDetail.kodeAlat" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" readonly="true"/>
                                                                    <s:hidden name="kodeAlat"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Terpasang di ruang :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="ruangTerpasang" name="alatDetail.ruangTerpasang" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Nomor Seri :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noSeries" name="alatDetail.noSeries" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Merk :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="merk" name="alatDetail.merk" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">jenis TP :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:select list="#{'digital':'DIGITAL','analog':'ANALOG'}" id="jenisTp" name="alatDetail.jenisTp"
                                                                              headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;"/>
                                                                    <%--<s:textfield id="jenisTp" name="alatDetail.jenisTp" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />--%>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="alatDetail.mode">Mode :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:select list="#{'Integrated':'Integrated','Independent':'Independent'}" id="mode" name="alatDetail.mode"
                                                                              headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <s:if test="isEnableSap()">
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <label class="control-label">No. Aktiva :</label>
                                                                </td>

                                                                <td>
                                                                    <table>
                                                                        <s:textfield id="noAktiva" name="alatDetail.noAktiva" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </s:if>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="alatDetail.status">Status Alat :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:select list="#{'Active':'Active','Non-Active':'Non-Active','Gudang':'Gudang'}" id="status" name="alatDetail.status"
                                                                              headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" >Tanggal Operasi :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <sj:datepicker id="projectStartDateForm" name="alatDetail.stTglOprs" displayFormat="dd-mm-yy"
                                                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />

                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Upload Foto :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:file id="uploadFront" name="uploadFront" cssClass="btn-mini" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Note :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textarea id="note" name="alatDetail.note" cssClass="btn-mini" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" cols="3" rows="4"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <div id="alert" class="alert" style="display: none">
                                            <button type="button" class="close" onclick="closeAlert()">&times;</button>
                                            <h4>Warning!</h4>
                                                <%--Best check yo self, you're not...--%>
                                            Data dengan nomor SAP ini telah ada.
                                            <div id="cont">

                                            </div>
                                        </div>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <div id="crud">
                                            <table>
                                                <tr>
                                                    <s:if test="isEnableSap()">
                                                        <td>
                                                            <table>

                                                                    <%--<sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"--%>
                                                                    <%--position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">--%>
                                                                    <%--Please don't close this window, server is processing your request ...--%>
                                                                    <%--</br>--%>
                                                                    <%--</br>--%>
                                                                    <%--</br>--%>
                                                                    <%--<img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">--%>
                                                                    <%--</sj:dialog>--%>
                                                                    <%--<sj:submit type="button" cssClass="btn btn-primary" formIds="addProjectForm" id="save" name="save"--%>
                                                                    <%--onClickTopics="showDialog" onCompleteTopics="closeDialog">--%>
                                                                    <%--<i class="icon-ok icon-white"></i>--%>
                                                                    <%--Save--%>
                                                                    <%--</sj:submit>--%>

                                                                    <%--<sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"--%>
                                                                    <%--position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">--%>
                                                                    <%--Please don't close this window, server is processing your request ...--%>
                                                                    <%--</br>--%>
                                                                    <%--</br>--%>
                                                                    <%--</br>--%>
                                                                    <%--<img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">--%>
                                                                    <%--</sj:dialog>--%>
                                                                    <%--<sj:submit type="button" cssClass="btn btn-primary" formIds="addProjectForm" id="search" name="search"--%>
                                                                    <%--onClickTopics="showDialog" onCompleteTopics="closeDialog">--%>
                                                                    <%--<i class="icon-search icon-white"></i>--%>
                                                                    <%--Save--%>
                                                                    <%--</sj:submit>--%>
                                                                <button type="button" id="btnsave" class="btn btn-primary" onclick="cekAndSave()">
                                                                    Save
                                                                </button>
                                                            </table>
                                                        </td>
                                                    </s:if>
                                                    <s:else>
                                                        <td>
                                                            <table>
                                                                <button type="button" id="btnsave" class="btn btn-primary" onclick="saveData()">
                                                                    Save
                                                                </button>
                                                            </table>
                                                        </td>
                                                    </s:else>


                                                    <td>
                                                        <table>
                                                            <sj:dialog id="view_dialog_add_user" openTopics="showDialogAddUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                       position="center" height="400" width="600" autoOpen="false" title="Add Channel"
                                                            >
                                                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                            </sj:dialog>

                                                            <s:url var="urlAddUser" namespace="/alatdetail" action="addTpChanel_alatdetail"
                                                                   escapeAmp="false">
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlAddUser}" cssClass="btn btn-warning">
                                                                <i class="icon-user icon-white"></i>
                                                                <small>Add Chanel</small>
                                                            </sj:a>
                                                        </table>
                                                    </td>

                                                    <td>
                                                        <table>

                                                            <button type="button" class="btn" onclick="window.location.href='<s:url action="initAdd_alatdetail"/>' ">
                                                                <i class="icon-repeat"></i> Reset
                                                            </button>

                                                        </table>
                                                    </td>


                                                    <td>
                                                        <table>

                                                            <button type="button" class="btn btn-default" onclick="window.location.href='<s:url action="initForm_alatdetail"/>' ">
                                                                <i class="icon-chevron-left icon-black"></i> Back
                                                            </button>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <sj:dialog id="view_dialog_edit_user" openTopics="showDialogEditUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Edit User">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit_issue" openTopics="showDialogEditIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Edit Issue">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_issue" openTopics="showDialogDeleteIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Delete Issue">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_user" openTopics="showDialogDeleteUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Delete User">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit_task" openTopics="showDialogEditTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Edit a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_task" openTopics="showDialogDeleteTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Delete a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_add_member_task" openTopics="showDialogAddMemberTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Add Task Team">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="20%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Channel :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfAlat" value="#session.listOfTpChanel" scope="request" />
                                                <display:table name="listOfAlat" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                    <display:column property="chanel" sortable="true" title="<small>Channel</small>" />
                                                    <display:column property="fungsiChanel" sortable="true" title="<small>fungsi Channel</small>" />
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>


                                </s:form>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>


