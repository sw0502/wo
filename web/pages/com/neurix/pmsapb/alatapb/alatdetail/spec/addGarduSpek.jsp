<%--
  Created by IntelliJ IDEA.
  User: gondok
  Date: 12/09/17
  Time: 8:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type="text/javascript">

        $.subscribe('beforeProcessSave', function (event, data) {
            var kodeapp = document.getElementById('addkodeApp').value;
            var kodeApb = document.getElementById('addkodeApb').value;
            var garduInduk = document.getElementById('addnamaGarduInduk').value;
            var kodeGardu = document.getElementById('addkodeGardu').value;
            var alamat = document.getElementById('addalamat').value;
            var tegangan = document.getElementById('addtegangan').value;
            var statusGardu = document.getElementById('addstatusGarduInduk').value;
            var jcc = document.getElementById('addjcc').value;
            var gi = document.getElementById('addnomorGarduInduk').value;


            if (kodeapp != '' && kodeApb != '' && garduInduk != '' && kodeGardu != '' && alamat != '' && tegangan != '' && statusGardu != '' && jcc != '' && gi != '') {

                if (confirm('Do you want to save this record ?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialog');

                } else {
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (kodeapp == '') {
                    msg = 'Field <strong>Kode app</strong> is required.' + '<br/>';
                }

                if (kodeApb == '') {
                    msg = 'Field <strong>Kode apb</strong> is required.' + '<br/>';
                }

                if (garduInduk == '') {
                    msg = 'Field <strong>Gardu induk</strong> is required.' + '<br/>';
                }

                if (kodeGardu == '') {
                    msg = 'Field <strong>Kode gardu</strong> is required.' + '<br/>';
                }

                if (alamat == '') {
                    msg = 'Field <strong>Alamat</strong> is required.' + '<br/>';
                }

                if (tegangan == '') {
                    msg = 'Field <strong>Tegangan</strong> is required.' + '<br/>';
                }

                if (statusGardu == '') {
                    msg = 'Field <strong>Status gardu</strong> is required.' + '<br/>';
                }

                if (jcc == '') {
                    msg = 'Field <strong>JCC</strong> is required.' + '<br/>';
                }

                if (gi == '') {
                    msg = 'Field <strong>Nomor Gardu induk</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessage').innerHTML = msg;

                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialog', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialogAdd');
            }
        });

        $.subscribe('errorDialog', function (event, data) {
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function cancelAddBtn() {
            $('#view_dialog_add').dialog('close');
        };

        function callSearchFunction() {
            $('#info_dialog_add').dialog('close');
            $('#view_dialog_add').dialog('close');
//            document.garduForm.action='search_gardu.action';
//            document.garduForm.submit();
        };



    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="addGarduForm" method="post"  namespace="/gardu" action="saveAddSpek_gardu"
                    cssClass="well form-horizontal">
                <fieldset>

                    <legend align="left">Add Gardu</legend>
                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table >
                        <tr>
                            <td>
                                <label class="control-label" for="gardu.nomorGarduInduk">Nomor Gardu induk :</label>
                            </td>

                            <td>
                                <table>
                                    <s:textfield id="addnomorGarduInduk" name="gardu.nomorGarduInduk" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="gardu.kodeApp">Kode App :</label>
                            </td>

                            <td>
                                <table>
                                    <s:textfield id="addkodeApp" name="gardu.kodeApp" required="true" placeholder="%{getText('Ketikan wilayah App')}" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                    <script>
                                        var functions, mapped;
                                        $('#addkodeApp').typeahead({
                                            minLength: 1,
                                            source: function (query, process) {
                                                functions = [];
                                                mapped = {};

                                                var data = [];
                                                dwr.engine.setAsync(false);
                                                AlatDetailAction.initComboApp(query, function (listdata) {
                                                    data = listdata;
                                                });

                                                $.each(data, function (i, item) {
                                                    var labelItem = +item.kodeApp+' - '+item.namaWilayah;
                                                    mapped[labelItem] = { id: item.kodeApp, label: labelItem };
                                                    functions.push(labelItem);
                                                });

                                                process(functions);
                                            },
                                            updater: function (item) {
                                                var selectedObj = mapped[item];
                                                return selectedObj.id;
                                            }
                                        });
                                    </script>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="gardu.kodeApb">Kode Apb :</label>
                            </td>

                            <td>
                                <table>
                                    <s:textfield id="addkodeApb" name="gardu.kodeApb" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="gardu.namaGarduInduk">Nama Gardu Induk :</label>
                            </td>

                            <td>
                                <table>
                                    <s:textfield id="addnamaGarduInduk" name="gardu.namaGarduInduk" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="gardu.kodeGardu">Kode Gardu :</label>
                            </td>

                            <td>
                                <table>
                                    <s:textfield id="addkodeGardu" name="gardu.kodeGardu" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Alamat :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textarea id="addalamat" name="gardu.alamat" required="true" disabled="false" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="gardu.tegangan">Tegangan :</label>
                            </td>

                            <td>
                                <table>
                                    <s:textfield id="addtegangan" name="gardu.tegangan" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="gardu.statusGarduInduk">Status Gardu :</label>
                            </td>

                            <td>
                                <table>
                                    <s:textfield id="addstatusGarduInduk" name="gardu.statusGarduInduk" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="gardu.jcc">JCC :</label>
                            </td>

                            <td>
                                <table>
                                    <s:textfield id="addjcc" name="gardu.jcc" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="gardu.flag">Flag :</label>
                            </td>

                            <td>
                                <table>

                                    <s:select list="#{'N':'Non-Active'}" id="flag" name="gardu.flag"
                                              headerKey="Y" headerValue="Active" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>

                                </table>
                            </td>
                        </tr>

                    </table>
                </fieldset>

                <div id="actions" class="form-actions">
                    <table>
                        <tr>
                            <td></td>
                            <td>
                                <div id="crud">
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="control-label"></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <table>
                                                    <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="addGarduForm" id="save" name="save"
                                                               onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialog,successDialog"
                                                               onSuccessTopics="successDialog" onErrorTopics="errorDialog" >
                                                        <i class="icon-ok-sign icon-white"></i>
                                                        Save
                                                    </sj:submit>

                                                    <sj:dialog id="waiting_dialog_save" openTopics="showDialog" closeTopics="closeDialog" modal="true"
                                                               resizable="false"
                                                               height="250" width="600" autoOpen="false" title="Saving ...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                    </sj:dialog>

                                                    <sj:dialog id="info_dialog_add" openTopics="showInfoDialogAdd" modal="true" resizable="false" closeOnEscape="false"
                                                               position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                               buttons="{
                                                                    'OK':function() {
                                                                               callSearchFunction();
                                                                         }
                                                                 }"
                                                    >
                                                        <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                        Record has been saved successfully.
                                                    </sj:dialog>

                                                    <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                               position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                               buttons="{
                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                    <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                               position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                               buttons="{
                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                <br/>
                                                                <center><div id="errorValidationMessage"></div></center>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>
                                                </table>
                                            </td>
                                            <td>
                                                <table>
                                                    <button type="button" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelAddBtn();">
                                                        <i class="icon-remove-circle "/> Cancel
                                                    </button>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>



            </s:form>



        </td>
    </tr>
</table>

</body>
</html>

