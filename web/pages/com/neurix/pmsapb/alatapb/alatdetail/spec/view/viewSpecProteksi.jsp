<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        function closeActivation(){
            $('#view_dialog_catatan').dialog('close');
//            document.alatDetailForm.action='search_alatdetail.action';
//            document.alatDetailForm.submit();
        }

    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="" method="post" namespace="" action=""
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">History Pemeliharaan Alat</legend>

                    <table >
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>

                    </table>


                </fieldset>

                <%--<sj:dialog id="view_dialog_View_doc" openTopics="showDialogViewDoc" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                           <%--position="center" height="600" width="1000" autoOpen="false" title="View Document">--%>
                    <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                <%--</sj:dialog>--%>


                <div style="overflow: scroll">
                    <table width="40%">
                        <tr align="center">
                            <td>
                                <h5>
                                    Setting :
                                </h5>
                            </td>
                        </tr>

                        <tr>
                            <td align="center">

                                <s:set name="listOfProteksi" value="#session.listOfProteksiSetting" scope="request" />
                                <display:table name="listOfProteksi" class="table table-condensed table-striped table-hover"
                                               requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                    <display:column property="tahap" sortable="true" title="<small>Tahap</small>" />
                                    <display:column property="arus" sortable="true" title="<small>Arus</small>" />
                                    <display:column property="waktu" sortable="true" title="<small>Waktu</small>" />
                                    <display:column property="target" sortable="true" title="<small>Target</small>" />
                                </display:table>
                            </td>
                        </tr>
                    </table>
                </div>


                <table>
                    <tr>
                        <td>

                            <button type="button" class="btn btn-default" onclick="closeActivation();">
                                <i class="icon-remove-circle icon-black"></i> Close
                            </button>
                        </td>
                    </tr>
                </table>



            </s:form>

        </td>
    </tr>
</table>

</body>
</html>


