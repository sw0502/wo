<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type='text/javascript' src='<s:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/util.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_add_user').dialog('close');
        };

        function saveButtonAdd() {
            var namaModul = document.getElementById('namaModul').value;
            var timeSlot = document.getElementById('timeSlot').value;
            var fungsi = document.getElementById('fungsi').value;
            var kapasitas = document.getElementById('kapasitas').value;
            var kapasitasId = document.getElementById('kapasitasId').value;
            var status = document.getElementById('status').value;

//            alert("klik");
//            alert(memberPosition);
            if (namaModul!='' && timeSlot!='' && fungsi !='' && kapasitas != '') {

                dwr.engine.setAsync(false);
                AlatDetailAction.saveListKapasitasMux(namaModul, timeSlot, fungsi, kapasitas, status, kapasitasId, function (response) {

                    if (response=='00') {
                        $('#view_dialog_add_user').dialog('close');


                        //reload popup add permohonan
                        document.addMuxForm.action='initAddJenisMux_alatdetail.action';
                        document.addMuxForm.submit();

                    }  else if (response=='02') {
//                        initEdit
                    document.editForm.action='initEdit_alatdetail.action';
                    document.editForm.submit();

                    }
                    else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (chanel == '') {
                    msg = 'Field <strong>Chanel</strong> is required.' + '<br/>';
                }

                if (fungsiChanel == '') {
                    msg = msg + 'Field <strong>fungsiChanel </strong> is required.' + '<br/>';
                }


                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_add_user').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <label class="control-label"><small>nama modul :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaModul" name="kapasitasMux.namaModul" cssClass="textForm" />
                                <s:hidden id="kapasitasId" name="kapasitasMux.kapasitasId"/>
                                <s:hidden id="status" name="status"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>time slot :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="timeSlot" name="kapasitasMux.timeSlot" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>fungsi :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="fungsi" name="kapasitasMux.fungsi" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>kapasitas :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="kapasitas" name="kapasitasMux.kapasitas" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <%--<sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"--%>
                                            <%--position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"--%>
                                            <%--buttons="{--%>
                                            <%--'OK':function() { okFailureButtonAddLahan(); }--%>
                                            <%--}"--%>
                                            <%-->--%>
                                            <%--<img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">--%>
                                            <%--Found failure when saving, please try again or call your admin.--%>
                                            <%--</sj:dialog>--%>

                                            <%--<sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"--%>
                                            <%--position="center" height="280" width="500" autoOpen="false" title="Warning"--%>
                                            <%--buttons="{--%>
                                            <%--'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }--%>
                                            <%--}"--%>
                                            <%-->--%>
                                            <%--<div class="alert alert-error fade in">--%>
                                            <%--<label class="control-label" align="left">--%>
                                            <%--<img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :--%>
                                            <%--<br/>--%>
                                            <%--<center><div id="errorValidationMessageAddLahan"></div></center>--%>
                                            <%--</label>--%>
                                            <%--</div>--%>
                                            <%--</sj:dialog>--%>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAdd();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

