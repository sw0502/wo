<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type='text/javascript' src='<s:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/util.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_add_user').dialog('close');
        };

        function saveButtonAdd() {
            var panjangAcdb = document.getElementById('panjangAcdb').value;
            var panjangBattery = document.getElementById('panjangBattery').value;
            var panjangDcdb = document.getElementById('panjangDcdb').value;
            var typeAcdb = document.getElementById('typeAcdb').value;
            var typeBattery = document.getElementById('typeBattery').value;
            var typeDcdb = document.getElementById('typeDcdb').value;
            var rectifierId = document.getElementById('rectifierId').value;
            var status = document.getElementById('status').value;

//            alert("klik");
//            alert(memberPosition);
            if (panjangAcdb!='' ) {

                dwr.engine.setAsync(false);
                AlatDetailAction.saveListRectifier(panjangAcdb, panjangBattery, panjangDcdb, typeAcdb, typeBattery, typeDcdb, status, rectifierId, function (response) {

                    if (response=='00') {
                        $('#view_dialog_add_user').dialog('close');


                        //reload popup add permohonan
                        document.addProjectForm.action='initAddJenisRectifier_alatdetail.action';
                        document.addProjectForm.submit();

                    } else if (response=='02') {
//                        initEdit
                        document.editForm.action='initEdit_alatdetail.action';
                        document.editForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg=""
                if (panjangAcdb == '') {
                    msg = 'Field <strong>panjangAcdb</strong> is required.' + '<br/>';
                }
                if (panjangBattery == '') {
                    msg = msg + 'Field <strong>panjangBattery</strong> is required.' + '<br/>';
                }

                if (panjangDcdb == '') {
                    msg = msg + 'Field <strong>panjangDcdb</strong> is required.' + '<br/>';
                }
                if (typeAcdb == '') {
                    msg = 'Field <strong>typeAcdb </strong> is required.' + '<br/>';
                }

                if (typeBattery == '') {
                    msg = msg + 'Field <strong>typeBattery</strong> is required.' + '<br/>';
                }
                if (typeDcdb == '') {
                    msg = 'Field <strong>typeDcdb</strong> is required.' + '<br/>';
                }



                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_add_user').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >

                    <tr>
                        <td>
                            <label class="control-label" >Panjang ACDB :</label>
                        </td>

                        <td>
                            <table>
                                <s:textfield id="panjangAcdb" name="rectifier.panjangAcdb" cssClass="textForm" />
                                <s:hidden id="rectifierId" name="rectifier.rectifierId"/>
                                <s:hidden id="status" name="status"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Type Kabel ACDB :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="typeAcdb" name="rectifier.typeAcdb" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Panjang Battery :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="panjangBattery" name="rectifier.panjangBattery" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Type Kabel Battery :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="typeBattery" name="rectifier.typeBattery" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Panjang Dcdb :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="panjangDcdb" name="rectifier.panjangDcdb" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Type Kabel Dcdb :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="typeDcdb" name="rectifier.typeDcdb" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <%--<sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"--%>
                                            <%--position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"--%>
                                            <%--buttons="{--%>
                                            <%--'OK':function() { okFailureButtonAddLahan(); }--%>
                                            <%--}"--%>
                                            <%-->--%>
                                            <%--<img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">--%>
                                            <%--Found failure when saving, please try again or call your admin.--%>
                                            <%--</sj:dialog>--%>

                                            <%--<sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"--%>
                                            <%--position="center" height="280" width="500" autoOpen="false" title="Warning"--%>
                                            <%--buttons="{--%>
                                            <%--'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }--%>
                                            <%--}"--%>
                                            <%-->--%>
                                            <%--<div class="alert alert-error fade in">--%>
                                            <%--<label class="control-label" align="left">--%>
                                            <%--<img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :--%>
                                            <%--<br/>--%>
                                            <%--<center><div id="errorValidationMessageAddLahan"></div></center>--%>
                                            <%--</label>--%>
                                            <%--</div>--%>
                                            <%--</sj:dialog>--%>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAdd();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

