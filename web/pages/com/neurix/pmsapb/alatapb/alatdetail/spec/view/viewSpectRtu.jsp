<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        function closeActivation(){
            $('#view_dialog_catatan').dialog('close');
//            document.alatDetailForm.action='search_alatdetail.action';
//            document.alatDetailForm.submit();
        }

    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="" method="post" namespace="" action=""
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">TRU Modul & bay</legend>

                    <table >
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>

                    </table>


                </fieldset>

                <%--<sj:dialog id="view_dialog_View_doc" openTopics="showDialogViewDoc" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                           <%--position="center" height="600" width="1000" autoOpen="false" title="View Document">--%>
                    <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                <%--</sj:dialog>--%>


                <div style="overflow: scroll">
                    <table width="40%" style="overflow: scroll">
                        <tr align="center">
                            <td>
                                <h5>
                                    Modul RTU :
                                </h5>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <s:set name="listOfModulRtu" value="#session.listOfModulRtu" scope="request" />
                                <display:table name="listOfModulRtu" class="table table-condensed table-striped table-hover"
                                               requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                    <display:column property="modul" sortable="true" title="<small>Modul</small>" />
                                    <display:column property="qty" sortable="true" title="<small>Qty</small>" />
                                    <display:column property="jumlahPoint" sortable="true" title="<small>Jumlah Point</small>" />
                                </display:table>
                            </td>
                        </tr>

                    </table>

                    <table width="40%" style="overflow: scroll">
                        <tr align="center">
                            <td>
                                <h5>
                                    Bay RTU :
                                </h5>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <s:set name="listOfBayRtu" value="#session.listOfBayRtu" scope="request" />
                                <display:table name="listOfBayRtu" class="table table-condensed table-striped table-hover"
                                               requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                    <display:column property="namaBay" sortable="true" title="<small>Nama Bay</small>" />
                                    <display:column property="jumlahDi" sortable="true" title="<small>Jumlah DI</small>" />
                                    <display:column property="jumlahDo" sortable="true" title="<small>Jumlah DO</small>" />
                                    <display:column property="jumlahAo" sortable="true" title="<small>Jumlah AO</small>" />
                                    <display:column property="jumlahAi" sortable="true" title="<small>Jumlah AI</small>" />
                                    <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                    <display:column property="ratioVt" sortable="true" title="<small>Ratio VT</small>" />
                                    <display:column property="ratioCt" sortable="true" title="<small>Ratio CT</small>" />
                                </display:table>
                            </td>
                        </tr>

                    </table>
                </div>


                <table>
                    <tr>
                        <td>

                            <button type="button" class="btn btn-default" onclick="closeActivation();">
                                <i class="icon-remove-circle icon-black"></i> Close
                            </button>
                        </td>
                    </tr>
                </table>



            </s:form>

        </td>
    </tr>
</table>

</body>
</html>


