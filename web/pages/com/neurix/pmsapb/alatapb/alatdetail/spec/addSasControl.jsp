<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type='text/javascript' src='<s:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/util.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_add_user').dialog('close');
        };

        function saveButtonAdd() {
            var namaControl = document.getElementById('namaControl').value;
            var tipe = document.getElementById('tipe').value;
            var tipeControl = document.getElementById('tipeControl').value;
            var merk = document.getElementById('merk1').value;
            var namaSoftware = document.getElementById('namaSoftware').value;
            var versiSoftware = document.getElementById('versiSoftware').value;
            var jumlahDi = document.getElementById('jumlahDi').value;
            var jumlahDo = document.getElementById('jumlahDo').value;
            var jumlahAi = document.getElementById('jumlahAi').value;
            var ratioVt = document.getElementById('ratioVt').value;
            var ratioCt = document.getElementById('ratioCt').value;
            var sn = document.getElementById('sn').value;
            var status = document.getElementById('status').value;
            var sasControlId = document.getElementById('sasControlId').value;

//            alert("klik");
//            alert(memberPosition);
            if (namaControl!='' && tipe!='') {

                dwr.engine.setAsync(false);
                AlatDetailAction.saveListSasControl(namaControl, tipe, tipeControl, merk, namaSoftware, versiSoftware, jumlahDi, jumlahDo, jumlahAi, ratioVt, ratioCt, sn, status, sasControlId, function (response) {

                    if (response=='00') {
                        $('#view_dialog_add_user').dialog('close');


                        //reload popup add permohonan
                        document.addSasForm.action='initAddJenisSas_alatdetail.action';
                        document.addSasForm.submit();

                    } else if (response=='02') {
//                        initEdit
                        document.editForm.action='initEdit_alatdetail.action';
                        document.editForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (tahap == '') {
                    msg = 'Field <strong>tahap</strong> is required.' + '<br/>';
                }

                if (arus == '') {
                    msg = msg + 'Field <strong>arus </strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_add_user').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <label class="control-label"><small>Jenis :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:select list="#{'BCU':'BCU' ,'IED':'IED I/O'}" id="tipeControl" name="sasControl.tipeControl"
                                          headerKey="%" headerValue="[Select One]" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Nama Control :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaControl" name="sasControl.namaControl" cssClass="textForm"/>
                                <s:hidden id="sasControlId" name="sasControl.sasControlId"/>
                                <s:hidden id="status" name="status"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Merk :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="merk1" name="sasControl.merk" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Tipe :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="tipe" name="sasControl.tipe" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Nama SoftWare :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaSoftware" name="sasControl.namaSoftWare" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Versi Software :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="versiSoftware" name="sasControl.versiSoftware" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>jumlah DI :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="jumlahDi" name="sasControl.jumlahDi" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>jumlah DO :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="jumlahDo" name="sasControl.jumlahDo" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>jumlah AI :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="jumlahAi" name="sasControl.jumlahAi" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Ratio VT :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="ratioVt" name="sasControl.ratioVt" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Ratio CT :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="ratioCt" name="sasControl.ratioCt" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td>
                            <label class="control-label"><small>Serial Number :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="sn" name="sasControl.sn" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>



                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <%--<sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"--%>
                                                       <%--position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"--%>
                                                       <%--buttons="{--%>
                                                                <%--'OK':function() { okFailureButtonAddLahan(); }--%>
                                                                <%--}"--%>
                                            <%-->--%>
                                                <%--<img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">--%>
                                                <%--Found failure when saving, please try again or call your admin.--%>
                                            <%--</sj:dialog>--%>

                                            <%--<sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"--%>
                                                       <%--position="center" height="280" width="500" autoOpen="false" title="Warning"--%>
                                                       <%--buttons="{--%>
                                                                    <%--'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }--%>
                                                                <%--}"--%>
                                            <%-->--%>
                                                <%--<div class="alert alert-error fade in">--%>
                                                    <%--<label class="control-label" align="left">--%>
                                                        <%--<img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :--%>
                                                        <%--<br/>--%>
                                                        <%--<center><div id="errorValidationMessageAddLahan"></div></center>--%>
                                                    <%--</label>--%>
                                                <%--</div>--%>
                                            <%--</sj:dialog>--%>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAdd();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

