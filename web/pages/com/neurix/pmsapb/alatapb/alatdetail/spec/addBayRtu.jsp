<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type='text/javascript' src='<s:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/util.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_add_user').dialog('close');
        };

        function saveButtonAdd() {
            var namaBay = document.getElementById('namaBay').value;
            var jumlahDi = document.getElementById('jumlahDi').value;
            var jumlahDo = document.getElementById('jumlahDo').value;
            var jumlahAo = document.getElementById('jumlahAo').value;
            var jumlahAi = document.getElementById('jumlahAi').value;
            var merk = document.getElementById('merk1').value;
            var ratioVt = document.getElementById('ratioVt').value;
            var ratioCt = document.getElementById('ratioCt').value;
            var bayId = document.getElementById('bayId').value;
            var status = document.getElementById('status').value;

            alert("klik");
//            alert(memberPosition);
            if (namaBay != '') {

                dwr.engine.setAsync(false);
                AlatDetailAction.saveListBayRtu(namaBay, jumlahDi, jumlahDo, jumlahAo, jumlahAi, merk, ratioVt, ratioCt, status, bayId, function (response) {

                    if (response=='00') {
                        $('#view_dialog_add_user').dialog('close');


                        //reload popup add permohonan
                        document.addRtuForm.action='initAddJenisRtu_alatdetail.action';
                        document.addRtuForm.submit();

                    } else if (response=='02') {
//                        initEdit
                        document.editForm.action='initEdit_alatdetail.action';
                        document.editForm.submit();

                    }
                    else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (namaBay == '') {
                    msg = 'Field <strong>namaBay</strong> is required.' + '<br/>';
                }

                if (jumlahDi == '') {
                    msg = msg + 'Field <strong>jumlahDi </strong> is required.' + '<br/>';
                }
                if (jumlahDo == '') {
                    msg = 'Field <strong>jumlahDo</strong> is required.' + '<br/>';
                }

                if (jumlahAo == '') {
                    msg = msg + 'Field <strong>jumlahAo </strong> is required.' + '<br/>';
                }
                if (jumlahAi == '') {
                    msg = 'Field <strong>jumlahAi</strong> is required.' + '<br/>';
                }

                if (merk == '') {
                    msg = msg + 'Field <strong>merk </strong> is required.' + '<br/>';
                }
                if (ratioVt == '') {
                    msg = 'Field <strong>ratioVt</strong> is required.' + '<br/>';
                }

                if (ratioCt == '') {
                    msg = msg + 'Field <strong>ratioCt </strong> is required.' + '<br/>';
                }


                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_add_user').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <label class="control-label"><small>Nama Bay :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaBay" name="bayRtu.namaBay"  cssClass="textForm" />
                                <s:hidden id="bayId" name="bayRtu.bayId"/>
                                <s:hidden id="status" name="status" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Jumlah DI :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="jumlahDi" name="bayRtu.jumlahDi" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Jumlah DO :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="jumlahDo" name="bayRtu.jumlahDo" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Jumlah AO :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="jumlahAo" name="bayRtu.jumlahAo" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Jumlah AI :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="jumlahAi" name="bayRtu.jumlahAi" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Merk :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="merk1" name="bayRtu.merk" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Ratio VT :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="ratioVt" name="bayRtu.ratioVt" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Ratio CT :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="ratioCt" name="bayRtu.ratioCt" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>



                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <%--<sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"--%>
                                            <%--position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"--%>
                                            <%--buttons="{--%>
                                            <%--'OK':function() { okFailureButtonAddLahan(); }--%>
                                            <%--}"--%>
                                            <%-->--%>
                                            <%--<img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">--%>
                                            <%--Found failure when saving, please try again or call your admin.--%>
                                            <%--</sj:dialog>--%>

                                            <%--<sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"--%>
                                            <%--position="center" height="280" width="500" autoOpen="false" title="Warning"--%>
                                            <%--buttons="{--%>
                                            <%--'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }--%>
                                            <%--}"--%>
                                            <%-->--%>
                                            <%--<div class="alert alert-error fade in">--%>
                                            <%--<label class="control-label" align="left">--%>
                                            <%--<img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :--%>
                                            <%--<br/>--%>
                                            <%--<center><div id="errorValidationMessageAddLahan"></div></center>--%>
                                            <%--</label>--%>
                                            <%--</div>--%>
                                            <%--</sj:dialog>--%>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAdd();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

