<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type='text/javascript' src='<s:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/util.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_add_user').dialog('close');
        };

        function saveButtonAdd() {
            var batteryName = document.getElementById('batteryName').value;
            var tegangan = document.getElementById('tegangan').value;
            var jumlahCell = document.getElementById('jumlahCell').value;
            var merk = document.getElementById('merk1').value;
            var type = document.getElementById('type').value;
            var kapasitas = document.getElementById('kapasitas').value;
            var fuse = document.getElementById('fuse').value;
            var fungsi = document.getElementById('fungsi').value;
            var batteryId = document.getElementById('batteryId').value;
            var status = document.getElementById('status').value;

//            alert("klik");
//            alert(memberPosition);
            if (batteryName!='' ) {

                dwr.engine.setAsync(false);
                AlatDetailAction.saveListBattery(batteryName, tegangan, jumlahCell, merk, type, kapasitas, fuse, fungsi, status, batteryId, function (response) {

                    if (response=='00') {
                        $('#view_dialog_add_user').dialog('close');


                        //reload popup add permohonan
                        document.addProjectForm.action='initAddJenisBattery_alatdetail.action';
                        document.addProjectForm.submit();

                    } else if (response=='02') {
//                        initEdit
                        document.editForm.action='initEdit_alatdetail.action';
                        document.editForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg=""
                if (batteryName == '') {
                    msg = 'Field <strong>batteryName</strong> is required.' + '<br/>';
                }
                if (tegangan == '') {
                    msg = msg + 'Field <strong>tegangan </strong> is required.' + '<br/>';
                }

                if (jumlahCell == '') {
                    msg = msg + 'Field <strong>jumlahCell </strong> is required.' + '<br/>';
                }
                if (merk == '') {
                    msg = 'Field <strong>merk</strong> is required.' + '<br/>';
                }

                if (type == '') {
                    msg = msg + 'Field <strong>type </strong> is required.' + '<br/>';
                }
                if (kapasitas == '') {
                    msg = 'Field <strong>kapasitas</strong> is required.' + '<br/>';
                }

                if (fuse == '') {
                    msg = msg + 'Field <strong>fuse </strong> is required.' + '<br/>';
                }

                if (fungsi == '') {
                    msg = msg + 'Field <strong>fungsi </strong> is required.' + '<br/>';
                }


                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_add_user').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >

                    <tr>
                        <td>
                            <label class="control-label" >Nama Battery :</label>
                        </td>

                        <td>
                            <table>
                                <s:textfield id="batteryName" name="battery.batteryName" cssClass="textForm" />
                                <s:hidden id="batteryId" name="battery.batteryId"/>
                                <s:hidden id="status" name="status"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Tegangan :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="tegangan" name="battery.tegangan" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Jumlah Cell :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="jumlahCell" name="battery.jumlahCell" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Merk :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="merk1" name="battery.merk" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Type :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="type" name="battery.type" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Kapasitas (AH):</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="kapasitas" name="battery.kapasitas" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Fuse/MCB :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="fuse" name="battery.fuse" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Fungsi :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="fungsi" name="battery.fungsi" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>



                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <%--<sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"--%>
                                            <%--position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"--%>
                                            <%--buttons="{--%>
                                            <%--'OK':function() { okFailureButtonAddLahan(); }--%>
                                            <%--}"--%>
                                            <%-->--%>
                                            <%--<img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">--%>
                                            <%--Found failure when saving, please try again or call your admin.--%>
                                            <%--</sj:dialog>--%>

                                            <%--<sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"--%>
                                            <%--position="center" height="280" width="500" autoOpen="false" title="Warning"--%>
                                            <%--buttons="{--%>
                                            <%--'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }--%>
                                            <%--}"--%>
                                            <%-->--%>
                                            <%--<div class="alert alert-error fade in">--%>
                                            <%--<label class="control-label" align="left">--%>
                                            <%--<img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :--%>
                                            <%--<br/>--%>
                                            <%--<center><div id="errorValidationMessageAddLahan"></div></center>--%>
                                            <%--</label>--%>
                                            <%--</div>--%>
                                            <%--</sj:dialog>--%>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAdd();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

