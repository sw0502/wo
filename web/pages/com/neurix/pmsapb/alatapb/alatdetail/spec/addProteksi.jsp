<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type='text/javascript' src='<s:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/util.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_add_user').dialog('close');
        };

        function saveButtonAdd() {
            var tahap = document.getElementById('tahap').value;
            var arus = document.getElementById('arus').value;
            var waktu = document.getElementById('waktu').value;
            var target = document.getElementById('target').value;
            var proteksiSettingId = document.getElementById('proteksiSettingId').value;
            var status = document.getElementById('status').value;

//            alert("klik");
//            alert(memberPosition);
            if (tahap!='' && arus!='') {

                dwr.engine.setAsync(false);
                AlatDetailAction.saveListProteksiSetting(tahap, arus, waktu, target , status, proteksiSettingId, function (response) {

                    if (response=='00') {
                        $('#view_dialog_add_user').dialog('close');


                        //reload popup add permohonan
                        document.addProteksiForm.action='initAddJenisProteksi_alatdetail.action';
                        document.addProteksiForm.submit();

                    } else if (response=='02') {
//                        initEdit
                        document.editForm.action='initEdit_alatdetail.action';
                        document.editForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (tahap == '') {
                    msg = 'Field <strong>tahap</strong> is required.' + '<br/>';
                }

                if (arus == '') {
                    msg = msg + 'Field <strong>arus </strong> is required.' + '<br/>';
                }


                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_add_user').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <label class="control-label"><small>Tahap :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="tahap" name="proteksiSetting.tahap" cssClass="textForm" />
                                <s:hidden id="proteksiSettingId" name="proteksiSetting.proteksiSettingId"/>
                                <s:hidden id="status" name="status"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Arus :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="arus" name="proteksiSetting.arus" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Waktu :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="waktu" name="proteksiSetting.waktu" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Target :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="target" name="proteksiSetting.target" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>



                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <%--<sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"--%>
                                                       <%--position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"--%>
                                                       <%--buttons="{--%>
                                                                <%--'OK':function() { okFailureButtonAddLahan(); }--%>
                                                                <%--}"--%>
                                            <%-->--%>
                                                <%--<img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">--%>
                                                <%--Found failure when saving, please try again or call your admin.--%>
                                            <%--</sj:dialog>--%>

                                            <%--<sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"--%>
                                                       <%--position="center" height="280" width="500" autoOpen="false" title="Warning"--%>
                                                       <%--buttons="{--%>
                                                                    <%--'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }--%>
                                                                <%--}"--%>
                                            <%-->--%>
                                                <%--<div class="alert alert-error fade in">--%>
                                                    <%--<label class="control-label" align="left">--%>
                                                        <%--<img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :--%>
                                                        <%--<br/>--%>
                                                        <%--<center><div id="errorValidationMessageAddLahan"></div></center>--%>
                                                    <%--</label>--%>
                                                <%--</div>--%>
                                            <%--</sj:dialog>--%>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAdd();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

