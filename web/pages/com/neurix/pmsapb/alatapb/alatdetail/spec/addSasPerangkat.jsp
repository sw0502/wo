<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type='text/javascript' src='<s:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/util.js"/>'></script>
    <script type='text/javascript' src='<s:url value="/dwr/interface/AlatDetailAction.js"/>'></script>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_add_user').dialog('close');
        };

        function changeTipe(){

            var tp = document.getElementById('tipePerangkat').value;

            if (tp == 'INVERTER'){
                document.getElementById('kap').style.visibility = 'visible';
                document.getElementById('tip').style.visibility = 'hidden';
            } else if (tp == 'SWITCH'){
                document.getElementById('kap').style.visibility = 'hidden';
                document.getElementById('tip').style.visibility = 'visible';
            } else {
                document.getElementById('kap').style.visibility = 'hidden';
                document.getElementById('tip').style.visibility = 'hidden';
            }

        }

        function saveButtonAdd() {
            var namaPerangkat = document.getElementById('namaPerangkat').value;
            var tipe = document.getElementById('tipe').value;
            var tipePerangkat = document.getElementById('tipePerangkat').value;
            var merk = document.getElementById('merk1').value;
            var jumlah = document.getElementById('jumlah').value;
            var sn = document.getElementById('sn').value;
            var status = document.getElementById('status').value;
            var sasPerangkatId = document.getElementById('sasPerangkatId').value;
            var kapasitas = document.getElementById('kapasitas').value;
            var tipeport = document.getElementById('tipeport').value;

//            alert("klik");
//            alert(memberPosition);
            if (namaPerangkat!='' && tipe!='') {

                dwr.engine.setAsync(false);
                AlatDetailAction.saveListSasPerangkat(namaPerangkat, tipe, tipePerangkat, merk, jumlah, sn, kapasitas, tipeport, status, sasPerangkatId, function (response) {

                    if (response=='00') {
                        $('#view_dialog_add_user').dialog('close');


                        //reload popup add permohonan
                        document.addSasForm.action='initAddJenisSas_alatdetail.action';
                        document.addSasForm.submit();

                    } else if (response=='02') {
//                        initEdit
                        document.editForm.action='initEdit_alatdetail.action';
                        document.editForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (tahap == '') {
                    msg = 'Field <strong>tahap</strong> is required.' + '<br/>';
                }

                if (arus == '') {
                    msg = msg + 'Field <strong>arus </strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_add_user').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <label class="control-label"><small>Jenis :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:select list="#{'GPS':'GPS' ,'SWITCH':'SWITCH', 'MONITOR HMI':'MONITOR HMI', 'PRINTER':'PRINTER','INVERTER':'INVERTER'}" id="tipePerangkat" name="sasPerangkat.tipePerangkat"
                                          headerKey="%" headerValue="[Select One]" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onchange="changeTipe()"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Nama Perangkat :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaPerangkat" name="sasPerangkat.namaPerangkat" cssClass="textForm"/>
                                <s:hidden id="sasPerangkatId" name="sasPerangkat.sasPerangkatId"/>
                                <s:hidden id="status" name="status"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Merk :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="merk1" name="sasPerangkat.merk" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Tipe :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="tipe" name="sasPerangkat.type" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>jumlah :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="jumlah" name="sasPerangkat.jumlah" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><small>Serial Number :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="sn" name="sasPerangkat.sn" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>


                <%--<s:div id="kap" cssStyle="display:none">--%>
                        <tr id="kap" style="visibility:hidden">
                            <td>
                                <label class="control-label"><small>Kapasitas :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kapasitas" name="sasPerangkat.kapasitas" cssClass="textForm" />
                                </table>
                            </td>
                        </tr>
                    <%--</s:div>--%>


                    <tr id="tip" style="visibility: hidden">
                        <td>
                            <label class="control-label"><small>Tipe Port :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="tipeport" name="sasPerangkat.tipeport" cssClass="textForm" />
                            </table>
                        </td>
                    </tr>




                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <%--<sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"--%>
                                                       <%--position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"--%>
                                                       <%--buttons="{--%>
                                                                <%--'OK':function() { okFailureButtonAddLahan(); }--%>
                                                                <%--}"--%>
                                            <%-->--%>
                                                <%--<img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">--%>
                                                <%--Found failure when saving, please try again or call your admin.--%>
                                            <%--</sj:dialog>--%>

                                            <%--<sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"--%>
                                                       <%--position="center" height="280" width="500" autoOpen="false" title="Warning"--%>
                                                       <%--buttons="{--%>
                                                                    <%--'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }--%>
                                                                <%--}"--%>
                                            <%-->--%>
                                                <%--<div class="alert alert-error fade in">--%>
                                                    <%--<label class="control-label" align="left">--%>
                                                        <%--<img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :--%>
                                                        <%--<br/>--%>
                                                        <%--<center><div id="errorValidationMessageAddLahan"></div></center>--%>
                                                    <%--</label>--%>
                                                <%--</div>--%>
                                            <%--</sj:dialog>--%>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAdd();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

