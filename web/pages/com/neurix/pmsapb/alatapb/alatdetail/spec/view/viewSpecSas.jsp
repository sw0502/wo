<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        function closeActivation(){
            $('#view_dialog_catatan').dialog('close');
//            document.alatDetailForm.action='search_alatdetail.action';
//            document.alatDetailForm.submit();
        }

    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="" method="post" namespace="" action=""
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">History Pemeliharaan Alat</legend>

                    <table >
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>

                    </table>


                </fieldset>

                <%--<sj:dialog id="view_dialog_View_doc" openTopics="showDialogViewDoc" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                           <%--position="center" height="600" width="1000" autoOpen="false" title="View Document">--%>
                    <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                <%--</sj:dialog>--%>


                <table width="60%">
                    <tr align="center">
                        <td>
                            <h5>
                                RTU Station :
                            </h5>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <s:set name="listOfSasStation" value="#session.listOfSasStation" scope="request" />
                            <display:table name="listOfSasStation" class="table table-condensed table-striped table-hover"
                                           requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                <display:column property="namaSatation" sortable="true" title="<small>Nama Satation</small>" />
                                <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                <display:column property="tipeStation" sortable="true" title="<small>Jenis Station</small>" />
                                <display:column property="tipe" sortable="true" title="<small>Tipe</small>" />
                                <display:column property="namaSoftWare" sortable="true" title="<small>Nama Software</small>" />
                                <display:column property="versiSoftware" sortable="true" title="<small>Versi Software</small>" />
                                <display:column property="jumlah" sortable="true" title="<small>Jumlah</small>" />
                                <display:column property="sn" sortable="true" title="<small>Serial Number</small>" />
                            </display:table>
                        </td>
                    </tr>
                </table>

                <table width="80%">
                    <tr align="center">
                        <td>
                            <h5>
                                RTU Control :
                            </h5>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <s:set name="listOfSasStation" value="#session.listOfSasControl" scope="request" />
                            <display:table name="listOfSasStation" class="table table-condensed table-striped table-hover"
                                           requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                <display:column property="namaControl" sortable="true" title="<small>Nama</small>" />
                                <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                <display:column property="tipeControl" sortable="true" title="<small>Jenis</small>" />
                                <display:column property="tipe" sortable="true" title="<small>Tipe</small>" />
                                <display:column property="namaSoftWare" sortable="true" title="<small>Nama Software</small>" />
                                <display:column property="versiSoftware" sortable="true" title="<small>Versi Software</small>" />
                                <display:column property="jumlahDi" sortable="true" title="<small>Jumlah DI</small>" />
                                <display:column property="jumlahDo" sortable="true" title="<small>Jumlah DO</small>" />
                                <display:column property="jumlahAi" sortable="true" title="<small>Jumlah AI</small>" />
                                <display:column property="ratioVt" sortable="true" title="<small>Ratio VT</small>" />
                                <display:column property="ratioCt" sortable="true" title="<small>RATIO CT</small>" />
                                <display:column property="sn" sortable="true" title="<small>Serial Number</small>" />
                            </display:table>
                        </td>
                    </tr>
                </table>

                <table width="60%">
                    <tr align="center">
                        <td>
                            <h5>
                                RTU Perangkat :
                            </h5>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <s:set name="listOfSasPerangkat" value="#session.listOfSasPerangkat" scope="request" />
                            <display:table name="listOfSasPerangkat" class="table table-condensed table-striped table-hover"
                                           requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                <display:column property="namaPerangkat" sortable="true" title="<small>Nama</small>" />
                                <display:column property="merk" sortable="true" title="<small>Merk</small>" />
                                <display:column property="tipePerangkat" sortable="true" title="<small>Jenis</small>" />
                                <display:column property="type" sortable="true" title="<small>Tipe</small>" />
                                <display:column property="jumlah" sortable="true" title="<small>Jumlah</small>" />
                                <display:column property="sn" sortable="true" title="<small>Serial Number</small>" />
                            </display:table>
                        </td>
                    </tr>
                </table>


                <table>
                    <tr>
                        <td>

                            <button type="button" class="btn btn-default" onclick="closeActivation();">
                                <i class="icon-remove-circle icon-black"></i> Close
                            </button>
                        </td>
                    </tr>
                </table>



            </s:form>

        </td>
    </tr>
</table>

</body>
</html>


