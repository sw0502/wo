<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        function closeActivation(){
            $('#view_dialog_catatan').dialog('close');
//            document.alatDetailForm.action='search_alatdetail.action';
//            document.alatDetailForm.submit();
        }

    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="" method="post" namespace="" action=""
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">PLC Channel</legend>

                    <table >
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>

                    </table>


                </fieldset>

                <%--<sj:dialog id="view_dialog_View_doc" openTopics="showDialogViewDoc" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                           <%--position="center" height="600" width="1000" autoOpen="false" title="View Document">--%>
                    <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                <%--</sj:dialog>--%>


                <div style="overflow: scroll">
                    <table width="40%" style="overflow: scroll">
                        <tr>
                            <td align="center">
                                <s:set name="listOfResult" value="#session.listOfPlcChanel" scope="request" />
                                <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="100">

                                    <display:column property="chanel" sortable="true" title="<small>Channel</small>" />
                                    <display:column property="fungsiChanel" sortable="true" title="<small>Fungsi Channel</small>" />
                                    <display:column property="frekuensi" sortable="true" title="<small>Frekuensi</small>" />

                                </display:table>

                            </td>
                        </tr>

                    </table>
                </div>


                <table>
                    <tr>
                        <td>

                            <button type="button" class="btn btn-default" onclick="closeActivation();">
                                <i class="icon-remove-circle icon-black"></i> Close
                            </button>
                        </td>
                    </tr>
                </table>



            </s:form>

        </td>
    </tr>
</table>

</body>
</html>


