<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        function closeActivation(){
            $('#view_dialog_list_Task').dialog('close');
        }

    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="listTaskProject" method="post" namespace="" action="viewTaskList_project"
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">List Team</legend>

                    <table >
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>

                    </table>
                </fieldset>

                <sj:dialog id="view_dialog_delete_task_member" openTopics="showDialogDeleteTaskMember" modal="true" resizable="false" cssStyle="text-align:left;"
                           position="center" height="400" width="600" autoOpen="false" title="Delete Member Team">
                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                </sj:dialog>
                <sj:dialog id="view_dialog_edit_task_member" openTopics="showDialogEditTaskMember" modal="true" resizable="false" cssStyle="text-align:left;"
                           position="center" height="400" width="600" autoOpen="false" title="Edit Member Team">
                    <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                </sj:dialog>



                <table width="100%">
                    <tr>
                        <td align="center">


                            <%--<sj:dialog id="view_dialog_edit_task" openTopics="showDialogViewEdit" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                                       <%--position="center" height="550" width="700" autoOpen="false" title="View Task">--%>
                                <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                            <%--</sj:dialog>--%>
                            <%--<sj:dialog id="view_dialog_delete_task" openTopics="showDialogViewDelete" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                                       <%--position="center" height="550" width="700" autoOpen="false" title="Delete Task">--%>
                                <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                            <%--</sj:dialog>--%>
                            <%--<sj:dialog id="view_dialog_add_doc" openTopics="showDialogAddDoc" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                                       <%--position="center" height="400" width="600" autoOpen="false" title="Add a Doc">--%>
                                <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                            <%--</sj:dialog>--%>

                            <s:set name="listOfResult" value="#session.listOfResulTaskMember" scope="request" />
                            <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                           requestURI="#" id="row" export="false" style="font-size:11" pagesize="100">

                                <display:column media="html" title="<small>edit</small>" style="text-align:center;font-size:9">
                                    <s:url var="urlViewEdit" namespace="/project" action="viewEditTaskMemberEdit_project"
                                           escapeAmp="false">
                                        <s:param name="id"><s:property value="#attr.row.idTaskMember"/></s:param>
                                    </s:url>
                                    <sj:a onClickTopics="showDialogEditTaskMember" href="%{urlViewEdit}">
                                        <img border="0" src="<s:url value="/pages/images/edit_member.png"/>" name="icon_print" width="25px">
                                    </sj:a>

                                </display:column>

                                <display:column media="html" title="<small>delete</small>" style="text-align:center;font-size:9">
                                    <s:url var="urlViewDelete" namespace="/project" action="viewDeleteTaskMemberEdit_project"
                                           escapeAmp="false">
                                        <s:param name="id"><s:property value="#attr.row.idTaskMember"/></s:param>
                                    </s:url>
                                    <sj:a onClickTopics="showDialogDeleteTaskMember" href="%{urlViewDelete}">
                                        <img border="0" src="<s:url value="/pages/images/delete_member.png"/>" name="icon_print" width="25px">
                                    </sj:a>

                                </display:column>

                                <display:column property="statusFlag" sortable="false" title="<small>Status</small>"  />
                                <display:column property="idTaskMember" sortable="false" title="<small>Id Task Member</small>"  />
                                <display:column property="idMember" sortable="false" title="<small>IdMember</small>"  />
                                <display:column property="namaMember" sortable="false" title="<small>Nama Member</small>"  />
                                <display:column property="namaTask" sortable="false" title="<small>Task</small>"  />
                                <display:column property="namaPosition" sortable="false" title="<small>Position</small>"  />
                            </display:table>

                        </td>
                    </tr>

                </table>

                <table>
                    <tr>
                        <td>

                            <button type="button" class="btn btn-danger" onclick="closeActivation();">
                                <i class="icon-remove-circle icon-white"></i> Close
                            </button>
                        </td>
                    </tr>
                </table>



            </s:form>

        </td>
    </tr>
</table>

</body>
</html>


