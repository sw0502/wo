<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>--%>
    <script type="text/javascript">

        function closeBtnTaskMemberEdit() {
            $('#view_dialog_edit_task_member').dialog('close');
            $('#view_dialog_list_Task').dialog('close');
            document.addProjectForm.action='initEditProject_project.action';
            document.addProjectForm.submit();

        };

        function saveButtonTaskMemberEDit() {
            var idMember = document.getElementById('comboMemberTask').value;
            var memberPosition = document.getElementById('memberPosition').value;
            var idTask = document.getElementById('idTask').value;
            var idTaskMember = document.getElementById('idTaskMember').value;

//            alert(memberPosition);
            if (idTaskMember!='' && idTask!='' && idMember!='' && memberPosition!='') {

                dwr.engine.setAsync(false);
                ProjectAction.saveEditTaskMemberEdit( idTaskMember, idTask, idMember, memberPosition, function (response) {

                    if (response=='0') {
                        $('#view_dialog_add_member_task').dialog('close');
                        $('#view_dialog_list_Task').dialog('close');


                        //reload popup add permohonan
                        document.addProjectForm.action='initEditProject_project.action';
                        document.addProjectForm.submit();

                    } else {
                        $('#info_dialog_edit_list_task').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (idMember == '') {
                    msg = 'Field <strong>Member Id</strong> is required.' + '<br/>';
                }

                if (idTask == '') {
                    msg = msg + 'Field <strong>Task Id</strong> is required.' + '<br/>';
                }
                if (memberPosition == ''){
                    msg = msg + 'Field <strong>Position</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessageTaskMemberEdit').innerHTML = msg;

                $.publish('showErrorValidationDialogTaskMember');

            }


        };

        function okFailureButtonTaskMemberEdit() {
            $('#info_dialog_edit_list_task').dialog('close');
            $('#view_dialog_edit_task_member').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >


                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Task ID :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="idTask" name="taskMember.idTask" cssClass="textForm" disabled="true"/>
                                <s:hidden id="idTaskMember" name="taskMember.idTaskMember" cssClass="textForm"/>
                                <s:hidden id="idTask" name="taskMember.idTask" cssClass="textForm"/>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Asign To :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:action id="comboMemberTask" namespace="/project" name="initComboMemberTask_project"/>
                                <s:select list="#comboMemberTask.listOfComboMemberTask" id="comboMemberTask" name="taskMember.idMember" required="true"
                                          listKey="idMember" listValue="namaMember" headerKey="" headerValue="[Select one]"
                                          cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Position As :</small></label>
                        </td>
                        <td>
                            <table>
                                <%--<s:textfield id="memberPosition" name="project.memberPosition" cssClass="textForm" />--%>
                                <s:select list="#{'4':'STAFF','5':'PIC'}" id="memberPosition" name="taskMember.position"
                                          headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="info_dialog_edit_list_task" openTopics="showInfoDialogEditTaskMember" modal="true" resizable="false"
                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                       buttons="{
                                                                'OK':function() { okFailureButtonTaskMemberEdit(); }
                                                                }"
                                            >
                                                <img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                Found failure when saving, please try again or call your admin.
                                            </sj:dialog>

                                            <sj:dialog id="error_dialog_edit_list_task" openTopics="showErrorValidationDialogTaskMember" modal="true" resizable="false"
                                                       position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                       buttons="{
                                                                    'OK':function() { $('#error_dialog_edit_list_task').dialog('close'); }
                                                                }"
                                            >
                                                <div class="alert alert-error fade in">
                                                    <label class="control-label" align="left">
                                                        <img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                        <br/>
                                                        <center><div id="errorValidationMessageTaskMemberEdit"></div></center>
                                                    </label>
                                                </div>
                                            </sj:dialog>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonTaskMemberEDit();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnTaskMemberEdit();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

