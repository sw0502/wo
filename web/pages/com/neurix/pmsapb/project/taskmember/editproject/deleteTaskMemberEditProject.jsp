<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>--%>
    <script type="text/javascript">

        function closeBtnTaskMemberDelete() {
            $('#view_dialog_delete_task_member').dialog('close');
            $('#view_dialog_list_Task').dialog('close');
            document.addProjectForm.action='initEditProject_project.action';
            document.addProjectForm.submit();
        };

        function saveButtonTaskMemberDelete() {
            var idTaskMember = document.getElementById('idTaskMember').value;

            if (idTask!='') {

                dwr.engine.setAsync(false);
                ProjectAction.saveDeleteTaskMemberEdit(idTaskMember, function (response) {

                    if (response=='0') {
                        $('#view_dialog_delete_task_member').dialog('close');


                        //reload popup add permohonan
                        document.addProjectForm.action='initEditProject_project.action';
                        document.addProjectForm.submit();

                    } else {
                        $('#info_dialog_task_member_delete').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (idTask == '') {
                    msg = 'Field <strong>Task Id</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessageTaskMemberDelete').innerHTML = msg;

                $.publish('showErrorValidationDialogTaskMemberDelete');

            }


        };

        function okFailureButtonTaskMemberDelete() {
            $('#info_dialog_task_member_delete').dialog('close');
            $('#view_dialog_delete_task_member').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Id Task Member :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="idTaskMember" name="taskMember.idTaskMember" cssClass="textForm" disabled="true"/>
                                <s:hidden id="idTask" name="taskMember.idTask"/>
                                <s:hidden id="idTaskMember" name="taskMember.idTaskMember"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Task :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaTask" name="taskMember.namaTask" cssClass="textForm" disabled="true"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Member :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaMember" name="taskMember.namaMember" cssClass="textForm" disabled="true"/>
                                <s:hidden id="idMember" name="taskMember.idMember"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Position :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaPosition" name="taskMember.namaPosition" cssClass="textForm" disabled="true"/>
                                <s:hidden id="position" name="taskMember.position"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="info_dialog_task_member_delete" openTopics="showInfoDialogDeleteTaskMember" modal="true" resizable="false"
                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                       buttons="{
                                                                'OK':function() { okFailureButtonTaskMemberDelete(); }
                                                                }"
                                            >
                                                <img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                Found failure when saving, please try again or call your admin.
                                            </sj:dialog>

                                            <sj:dialog id="error_validation_dialog_delete_task_member" openTopics="showErrorValidationDialogTaskMemberDelete" modal="true" resizable="false"
                                                       position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                       buttons="{
                                                                    'OK':function() { $('#error_validation_dialog_delete_task_member').dialog('close'); }
                                                                }"
                                            >
                                                <div class="alert alert-error fade in">
                                                    <label class="control-label" align="left">
                                                        <img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                        <br/>
                                                        <center><div id="errorValidationMessageTaskMemberDelete"></div></center>
                                                    </label>
                                                </div>
                                            </sj:dialog>

                                            <button type="button" id="saveabtn" class="btn btn-warning" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonTaskMemberDelete();">
                                                <i class="icon-ok-circle icon-white"/> Delete
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnTaskMemberDelete();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

