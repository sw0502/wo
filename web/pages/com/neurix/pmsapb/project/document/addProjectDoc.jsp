<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>--%>
    <script type="text/javascript">


        $.subscribe('beforeProcessSaveOperator', function (event, data) {

            var fileUploadDoc=document.getElementById("fileUploadDoc").value;
            alert(fileUploadDoc);

            if ( fileUploadDoc != '' ) {

                if (confirm('Do you want to save this record ?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialogSaveOperator');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (fileUploadDoc == '') {
                    msg = 'Field <strong>Upload File</strong> is required.' + '<br/>';
                }



                document.getElementById('errorValidationMessageSaveOperator').innerHTML = msg;

                $.publish('showErrorValidationDialogSaveOperator');

            }
        });

        $.subscribe('successDialogSaveOperator', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialogSaveOperator');
            }
        });

        $.subscribe('errorDialogSaveOperator', function (event, data) {
//            console.log(event.originalEvent.request);
//            alert(event.originalEvent.request.getResponseHeader('message'));
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function cancelSaveOperatorBtn() {

            $('#view_dialog_add_doc').dialog('close');
        };

        function callSearchOperatorFunction() {
            $('#info_dialog_saveoperator').dialog('close');
            $('#view_dialog_add_doc').dialog('close');
            document.addProjectForm.action='initAddProject_project.action';
            document.addProjectForm.submit();
        };




    </script>
</head>

<body bgcolor="#FFFFFF">


<table width="100%" align="center">
    <tr>
        <td align="center">


            <s:form id="addProjectDocForm" method="post" namespace="/project" action="saveSessionDoc_project" enctype="multipart/form-data"
                    cssClass="well form-horizontal">
                <legend align="left">Upload Project Document</legend>

                <table>

                    <%--<tr>--%>
                        <%--<td>--%>
                            <%--<label class="control-label"><small></small></label>--%>
                        <%--</td>--%>
                        <%--<td>--%>
                            <%--<table>--%>
                                <%--<s:textfield id="idProject" name="project.idProject" cssClass="textForm" disabled="true"/>--%>
                            <%--</table>--%>
                        <%--</td>--%>
                    <%--</tr>--%>

                    <tr>
                        <td>
                            <label class="control-label"><small></small></label>
                        </td>
                        <td>
                            <table>
                                <s:file id="fileUploadDoc" name="fileUploadDoc" cssClass="btn-mini" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>

                                    <tr>
                                        <td align="center">

                                            <table>

                                                <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="addProjectDocForm" id="saveoperator" name="saveoperator"
                                                           onBeforeTopics="beforeProcessSaveOperator" onCompleteTopics="closeDialogSaveOperator,successDialogSaveOperator"
                                                           onSuccessTopics="successDialogSaveOperator" onErrorTopics="errorDialogSaveOperator">
                                                    <i class="icon-ok-sign icon-white"></i>
                                                    Save
                                                </sj:submit>

                                                <sj:dialog id="waiting_dialog_saveoperator" openTopics="showDialogSaveOperator" closeTopics="closeDialogSaveOperator" modal="true"
                                                           resizable="false"
                                                           height="250" width="600" autoOpen="false" title="Saving ...">
                                                    Please don't close this window, server is processing your request ...
                                                    </br>
                                                    </br>
                                                    </br>
                                                    <img border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" id="image_indicator_write_operator" name="image_indicator_write_operator">
                                                </sj:dialog>

                                                <sj:dialog id="info_dialog_saveoperator" openTopics="showInfoDialogSaveOperator" modal="true" resizable="false" closeOnEscape="false"
                                                           position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                           buttons="{
                                                                        'OK':function() {
                                                                                   callSearchOperatorFunction();
                                                                             }
                                                                     }"
                                                >
                                                    <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                    Record has been saved successfully.
                                                </sj:dialog>

                                                <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                           position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                           buttons="{
                                                                        'OK':function() { $('#error_dialog').dialog('close'); }
                                                                    }"
                                                >
                                                    <div class="alert alert-error fade in">
                                                        <label class="control-label" align="left">
                                                            <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                        </label>
                                                    </div>
                                                </sj:dialog>

                                                <sj:dialog id="error_validation_dialog_saveoperator" openTopics="showErrorValidationDialogSaveOperator" modal="true" resizable="false"
                                                           position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                           buttons="{
                                                                        'OK':function() { $('#error_validation_dialog_saveoperator').dialog('close'); }
                                                                    }"
                                                >
                                                    <div class="alert alert-error fade in">
                                                        <label class="control-label" align="left">
                                                            <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                            <br/>
                                                            <center><div id="errorValidationMessageSaveOperator"></div></center>
                                                        </label>
                                                    </div>
                                                </sj:dialog>


                                            </table>
                                        </td>

                                        <td>
                                            <button type="button" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelSaveOperatorBtn();">
                                                <i class="icon-remove-circle "/> Cancel
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
                </div>

            </s:form>

        </td>
    </tr>
</table>

</body>
</html>

