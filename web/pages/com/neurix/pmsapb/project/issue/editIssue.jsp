<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>--%>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_edit_issue').dialog('close');
        };

        function saveButtonAddLahan() {
            var namaIssue = document.getElementById('namaIssue').value;
            var idIssue = document.getElementById('idIssue').value;
            var idMember = document.getElementById('idMember').value;
            var lastUpdateWho = document.getElementById('lastUpdateWho').value;


//            alert(memberPosition);
            if (namaIssue!='' && idIssue !='') {

                dwr.engine.setAsync(false);
                ProjectAction.saveEditProjectIssue(idIssue, namaIssue, idMember,lastUpdateWho, function (response) {

                    if (response=='0') {
                        $('#view_dialog_edit_issue').dialog('close');

                        //reload popup add permohonan
                        document.addProjectForm.action='initAddProject_project.action';
                        document.addProjectForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                if (namaIssue == '') {
                    msg = msg + 'Field <strong>Issue</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');
            }
        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_edit_issue').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <label class="control-label"><small>Namaa Issue :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaIssue" name="projectIssue.namaIssue" cssClass="textForm"/>
                                <s:hidden id="idIssue" name="projectIssue.IdIssue" />
                                <s:hidden id="idMember" name="projectIssue.idMember" />
                                <s:hidden id="lastUpdateWho" name="projectIssue.lastUpdateWho" />
                            </table>
                        </td>
                    </tr>

                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                       buttons="{
                                                                'OK':function() { okFailureButtonAddLahan(); }
                                                                }"
                                            >
                                                <img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                Found failure when saving, please try again or call your admin.
                                            </sj:dialog>

                                            <sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                       buttons="{
                                                                    'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }
                                                                }"
                                            >
                                                <div class="alert alert-error fade in">
                                                    <label class="control-label" align="left">
                                                        <img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                        <br/>
                                                        <center><div id="errorValidationMessageAddLahan"></div></center>
                                                    </label>
                                                </div>
                                            </sj:dialog>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAddLahan();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

