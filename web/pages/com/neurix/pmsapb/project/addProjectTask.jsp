<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">


        $.subscribe('beforeProcessSave', function (event, data) {

            var namaTask = document.getElementById('namaTask').value;
            var progres = document.getElementById('taskDateForm').value;
//            var projectDeadline = document.getElementById('pDeadline').value;
//            var projectStartDate = document.getElementById('pStartDate').value;
//            var startDate = document.getElementById('taskStartDateForm').value;
//            var deadline = document.getElementById('taskDateForm').value;

            if (namaTask != ''&& progres != '') {

                if (confirm('Do you want to save this record?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialog');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (document.getElementById("namaTask").value == '') {
                    msg = 'Field <strong>Task Name</strong> is required.' + '<br/>';
                }
                var msg = "";
                if (document.getElementById("taskDateForm").value == '') {
                    msg = 'Field <strong>Deadline</strong> is required.' + '<br/>';
                }

//                var msg = "";
//                if (startDate < projectStartDate) {
//                    msg = 'Field <strong>start date</strong> must after start date project.' + '<br/>';
//                }

//                var msg = "";
//                if (deadline > projectDeadline) {
//                    msg = 'Field <strong>Deadline</strong> must before Deadline project.' + '<br/>';
//                }

                document.getElementById('errorValidationMessage').innerHTML = msg;

                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialog', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialog');
//                $('#view_dialog_edit_task').dialog('close');
            }
        });

        $.subscribe('errorDialog', function (event, data) {
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function cancelBtn() {
            $('#view_dialog_add_Task').dialog('close');
            document.projectForm.action='search_project.action';
            document.projectForm.submit();
        };

        function callSearchFunction() {
            $('#info_dialog').dialog('close');
            $('#view_dialog_add_task').dialog('close');
            document.projectForm.action='search_project.action';
            document.projectForm.submit();
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="addFormTask" method="post" namespace="/project" action="saveAddTask_project" enctype="multipart/form-data"
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">Add Project Task</legend>

                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Project Id :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="idProject" name="task.idProject" cssClass="textForm" disabled="true"/>
                                    <s:hidden id="pStartDate" name="task.pStartDate"/>
                                    <s:hidden id="pDeadline" name="task.pDeadline"/>
                                    <s:hidden id="id" name="task.idProject"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Task Name :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaTask" name="task.namaTask" cssClass="textForm"/>
                                    <%--<s:hidden id="idProject" name="task.idProject"/>--%>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Asign To :</small></label>
                                </table>
                            </td>

                            <td>
                                <table>
                                    <s:action id="comboAsignTo" namespace="/project" name="initComboAsignToAddTask_project"/>
                                    <s:select list="#comboAsignTo.listOfComboAsignTo" id="comboAsignTo" name="task.asignTo" required="true"
                                              listKey="idMember" listValue="namaMember" headerKey="" headerValue="[Select one]"
                                              cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Priority :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:action id="comboPriority" namespace="/project" name="initComboPriority_project"/>
                                    <s:select list="#comboPriority.listOfComboPriority" id="comboPriority" name="task.priority" required="true"
                                              listKey="idPriority" listValue="namaPriority" headerKey="" headerValue="[Select one]"
                                              cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label" for="task.stStartDate">Start Date :</label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <sj:datepicker id="taskStartDateForm" name="task.stStartDate" displayFormat="dd-mm-yy"
                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                                </table>
                            </td>
                        </tr>
                            <tr>
                                <td>
                                    <table>
                                        <label class="control-label" for="task.stDeadline">Deadline :</label>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <sj:datepicker id="taskDateForm" name="task.stDeadline" displayFormat="dd-mm-yy"
                                                       showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                                    </table>
                                </td>
                            </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Note :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textarea id="note" name="task.note" disabled="false" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                </table>
                            </td>
                        </tr>

                    </table>
                </fieldset>

                <div id="actions" class="form-actions">
                    <table>
                        <tr>
                            <td></td>
                            <td>
                                <div id="crud">
                                    <table>

                                        <tr>
                                            <td align="center">

                                                <table>

                                                    <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="addFormTask" id="save" name="save"
                                                               onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialog,successDialog"
                                                               onSuccessTopics="successDialog" onErrorTopics="errorDialog" >
                                                        <i class="icon-ok-sign icon-white"></i>
                                                        Save
                                                    </sj:submit>

                                                    <sj:dialog id="waiting_dialog_add" openTopics="showDialog" closeTopics="closeDialogEditTask" modal="true"
                                                               resizable="false"
                                                               height="250" width="600" autoOpen="false" title="Saving ...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                    </sj:dialog>

                                                    <sj:dialog id="info_dialog" openTopics="showInfoDialog" modal="true" resizable="false" closeOnEscape="false"
                                                               position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                               buttons="{
                                                                    'OK':function() {
                                                                               callSearchFunction();
                                                                         }
                                                                 }"
                                                    >
                                                        <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                        Record has been saved successfully.
                                                    </sj:dialog>

                                                    <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                               position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                               buttons="{
                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                    <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                               position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                               buttons="{
                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                <br/>
                                                                <center><div id="errorValidationMessage"></div></center>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>


                                                </table>
                                            </td>

                                            <td>
                                                <button type="button" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelBtn();">
                                                    <i class="icon-remove-circle "/> Cancel
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>

            </s:form>


        </td>
    </tr>
</table>

</body>
</html>


