<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type="text/javascript">



    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="printReport" method="post"  namespace="/project" action="printReportSumary_project"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Print Report Summary Project</legend>

                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.idProject">No. Project :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="idProject" name="project.idProject" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.namaProject">No. WO :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noWo" name="project.noWo" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.namaProject">Nama Project :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaProject" name="project.namaProject" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Periode :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                        <%--<sj:datepicker id="bulanForm" name="project.bulan" displayFormat="m"--%>
                                                                        <%--showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" showButtonPanel="true" changeMonth="true" changeYear="false" cssClass="input-small" />--%>

                                                                    <s:select list="#{'1':'Januari','2':'Februari','3':'Maret','4':'April','5':'Mei','6':'Juni','7':'Juli','8':'Agustus','9':'September','10':'Oktober','11':'November','12':'Desember'}" id="bulanForm" name="project.bulan"
                                                                              headerKey="" headerValue="[Bulan]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>

                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <label class="control-label"></label>

                                                                </table>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                        <%--<sj:datepicker id="tahunForm" name="project.tahun" displayFormat="yy"--%>
                                                                        <%--showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" showButtonPanel="true" changeMonth="false" changeYear="true" cssClass="input-small" />--%>

                                                                    <s:select list="#{'2015':'2015','2016':'2016','2017':'2017','2018':'2018','2019':'2019','2020':'2020'}" id="tahunForm" name="project.tahun"
                                                                              headerKey="" headerValue="[Tahun]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <%--<tr>--%>
                                                <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<tr>--%>
                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                    <%--<label class="control-label" for="project.tahun">Tahun :</label>--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                    <%--&lt;%&ndash;<sj:datepicker id="tahunForm" name="project.tahun" displayFormat="yy"&ndash;%&gt;--%>
                                                                                   <%--&lt;%&ndash;showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" showButtonPanel="true" changeMonth="false" changeYear="true" cssClass="input-small" />&ndash;%&gt;--%>

                                                                    <%--<s:select list="#{'2015':'2015','2016':'2016','2017':'2017','2018':'2018','2019':'2019','2020':'2020'}" id="tahunForm" name="project.tahun"--%>
                                                                              <%--headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                        <%--&lt;%&ndash;<sj:datepicker id="bulanForm" name="project.bulan" displayFormat="m"&ndash;%&gt;--%>
                                                                        <%--&lt;%&ndash;showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" showButtonPanel="true" changeMonth="true" changeYear="false" cssClass="input-small" />&ndash;%&gt;--%>

                                                                    <%--<s:select list="#{'01':'Januari','02':'Februari','03':'Maret','04':'April','05':'Mei','06':'Juni','07':'Juli','08':'Agustus','09':'September','10':'Oktober','11':'November','12':'Desember'}" id="bulanForm" name="project.bulan"--%>
                                                                              <%--headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                        <%--</tr>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>
                                            <%--<tr>--%>
                                                <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<tr>--%>
                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                    <%--<label class="control-label" for="project.bulan">Bulan :</label>--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                    <%--&lt;%&ndash;<sj:datepicker id="bulanForm" name="project.bulan" displayFormat="m"&ndash;%&gt;--%>
                                                                                   <%--&lt;%&ndash;showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" showButtonPanel="true" changeMonth="true" changeYear="false" cssClass="input-small" />&ndash;%&gt;--%>

                                                                    <%--<s:select list="#{'01':'Januari','02':'Februari','03':'Maret','04':'April','05':'Mei','06':'Juni','07':'Juli','08':'Agustus','09':'September','10':'Oktober','11':'November','12':'Desember'}" id="bulanForm" name="project.bulan"--%>
                                                                              <%--headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                        <%--</tr>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>
                                            <%--<tr>--%>
                                                <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<tr>--%>
                                                            <%--<td>--%>
                                                                <%--<label class="control-label" for="project.idTypeProject">Type Project :</label>--%>
                                                            <%--</td>--%>
                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                    <%--<s:action id="comboTypeProject" namespace="/project" name="initComboTypeProject_project"/>--%>
                                                                    <%--<s:select list="#comboTypeProject.listOfComboTypeProject" id="comboTypeProject" name="project.idTypeProject" required="true"--%>
                                                                              <%--listKey="idTypeProject" listValue="namaTypeProject" headerKey="" headerValue="[Select one]"--%>
                                                                              <%--cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>

                                                        <%--</tr>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>
                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <table>
                                            <tr>

                                                <td>
                                                    <table>

                                                        <sj:dialog id="waiting_dialog" openTopics="showDialogSearch" closeTopics="closeDialogSearch" modal="true" resizable="false"
                                                                   position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                                            Please don't close this window, server is processing your request ...
                                                            </br>
                                                            </br>
                                                            </br>
                                                            <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                        </sj:dialog>
                                                        <sj:submit type="button" cssClass="btn btn-primary" formIds="printReport" id="print" name="print"
                                                                   onClickTopics="showDialogSearch" onCompleteTopics="closeDialogSearch">
                                                            <i class="icon-print icon-white"></i>
                                                            Print
                                                        </sj:submit>

                                                    </table>
                                                </td>


                                                <td>
                                                    <table>

                                                        <button type="button" class="btn" onclick="window.location.href='<s:url action="initReport_project"/>' ">
                                                            <i class="icon-repeat"></i> Reset
                                                        </button>

                                                    </table>
                                                </td>


                                            </tr>
                                        </table>
                                    </div>

                                    <%--<sj:dialog id="view_dialog_Issue" openTopics="showDialogIssue" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                                               <%--position="center" height="400" width="650" autoOpen="false" title="Add a Issue">--%>
                                        <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                                    <%--</sj:dialog>--%>

                                    <%--<sj:dialog id="view_dialog_Task" openTopics="showDialogTask" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                                               <%--position="center" height="600" width="1000" autoOpen="false" title="Edit Task">--%>
                                        <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                                    <%--</sj:dialog>--%>

                                    <%--<sj:dialog id="view_dialog_add_Task" openTopics="showDialogAddTask" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                                               <%--position="center" height="550" width="700" autoOpen="false" title="Add a Task">--%>
                                        <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                                    <%--</sj:dialog>--%>


                                    <%--<sj:dialog id="view_dialog_ViewProject" openTopics="showDialogViewProject" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                                               <%--position="center" height="600" width="1000" autoOpen="false" title="View Data Project">--%>
                                        <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                                    <%--</sj:dialog>--%>

                                    <%--<sj:dialog id="view_dialog_add_doc" openTopics="showDialogAddDoc" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                                               <%--position="center" height="300" width="600" autoOpen="false" title="Upload Project Document">--%>
                                        <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                                    <%--</sj:dialog>--%>
                                    <%--<sj:dialog id="waiting_dialog" openTopics="showDialogPrint" closeTopics="closeDialogPrint" modal="true" resizable="false"--%>
                                               <%--position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">--%>
                                        <%--Please don't close this window, server is processing your request ...--%>
                                        <%--</br>--%>
                                        <%--</br>--%>
                                        <%--</br>--%>
                                        <%--<img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">--%>
                                    <%--</sj:dialog>--%>
                                    <%--<table width="100%">--%>
                                        <%--<tr>--%>
                                            <%--<td align="center">--%>

                                                <%--<s:set name="listOfProject" value="#session.listOfResult" scope="request" />--%>
                                                <%--<display:table name="listOfProject" class="table table-condensed table-striped table-hover"--%>
                                                               <%--requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">--%>

                                                    <%--<display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:10">--%>

                                                        <%--<s:url value="initDelete_project.action" var="urlViewDelete">--%>
                                                            <%--<s:param name="id"><s:property value="#attr.row.idProject" /></s:param>--%>
                                                        <%--</s:url>--%>
                                                        <%--<s:a href="%{urlViewDelete}">--%>
                                                            <%--<img border="0" src="<s:url value="/pages/images/delete_task.png"/>" name="icon_print" width="25px">--%>
                                                        <%--</s:a>--%>

                                                    <%--</display:column>--%>


                                                    <%--<display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:10">--%>

                                                        <%--<s:url value="editProject_project.action" var="editProject" >--%>
                                                            <%--<s:param name="id"><s:property value="#attr.row.idProject" /></s:param>--%>
                                                        <%--</s:url>--%>
                                                        <%--<s:a href="%{editProject}"><img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_edit" width="25px"></s:a>--%>

                                                    <%--</display:column>--%>


                                                    <%--&lt;%&ndash;<display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">&ndash;%&gt;--%>

                                                        <%--&lt;%&ndash;<s:url var="urlViewEdit" namespace="/project" action="editProject_project"&ndash;%&gt;--%>
                                                               <%--&lt;%&ndash;escapeAmp="false">&ndash;%&gt;--%>
                                                            <%--&lt;%&ndash;<s:param name="id"><s:property value="#attr.row.idProject" /></s:param>&ndash;%&gt;--%>
                                                        <%--&lt;%&ndash;</s:url>&ndash;%&gt;--%>
                                                        <%--&lt;%&ndash;<sj:a onclickTopics="showDialogViewProject" href="%{urlViewEdit}">&ndash;%&gt;--%>
                                                            <%--&lt;%&ndash;<img border="0" src="<s:url value="/pages/images/edit_project.png"/>" name="icon_print" width="25px">&ndash;%&gt;--%>
                                                        <%--&lt;%&ndash;</sj:a>&ndash;%&gt;--%>

                                                    <%--&lt;%&ndash;</display:column>&ndash;%&gt;--%>

                                                    <%--<display:column media="html" title="<small>View</small>" style="text-align:center;font-size:9">--%>

                                                        <%--<s:url var="urlView" namespace="/project" action="viewProject_project"--%>
                                                               <%--escapeAmp="false">--%>
                                                            <%--<s:param name="id"><s:property value="#attr.row.idProject" /></s:param>--%>
                                                        <%--</s:url>--%>
                                                        <%--<sj:a onClickTopics="showDialogViewProject" href="%{urlView}">--%>
                                                            <%--<img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print" width="25px">--%>
                                                        <%--</sj:a>--%>

                                                    <%--</display:column>--%>
                                                    <%--<display:column property="namaTypeProject" sortable="true" title="<small>Project Type</small>" />--%>
                                                    <%--<display:column property="idProject" sortable="true" title="<small>No. Project</small>" />--%>
                                                    <%--<display:column property="noWo" sortable="true" title="<small>No. WO</small>" />--%>
                                                    <%--<display:column property="namaProject" sortable="true" title="<small>Project Name</small>"  />--%>
                                                    <%--<display:column property="namaProjectManager" sortable="true" title="<small>PIC</small>" />--%>
                                                    <%--<display:column property="startDate" sortable="true" title="<small>Start Date</small>" />--%>
                                                    <%--<display:column property="deadline" sortable="true" title="<small>Deadline</small>" />--%>
                                                    <%--<display:column property="namaStatus" sortable="true" title="<small>Status</small>" />--%>
                                                    <%--<display:column property="progresBar" sortable="true" title="<small>Progres</small>" />--%>
                                                    <%--<display:column media="html" title="<small>Add Task</small>" style="text-align:center;font-size:9">--%>
                                                        <%--<s:url var="urlAddTask" namespace="/project" action="addProjectTask_project"--%>
                                                               <%--escapeAmp="false">--%>
                                                            <%--<s:param name="id"><s:property value="#attr.row.idProject" /></s:param>--%>
                                                        <%--</s:url>--%>
                                                        <%--<sj:a onClickTopics="showDialogAddTask" href="%{urlAddTask}">--%>
                                                            <%--<img border="0" src="<s:url value="/pages/images/add_task1.png"/>" name="icon_print" width="25px">--%>
                                                        <%--</sj:a>--%>
                                                    <%--</display:column>--%>

                                                    <%--<display:column media="html" title="<small>Add Doc</small>" style="text-align:center;font-size:9">--%>
                                                        <%--<s:url var="urlAddDoc" namespace="/project" action="addDoc_project"--%>
                                                               <%--escapeAmp="false">--%>
                                                            <%--<s:param name="id"><s:property value="#attr.row.idProject" /></s:param>--%>
                                                        <%--</s:url>--%>
                                                        <%--<sj:a onClickTopics="showDialogAddDoc" href="%{urlAddDoc}">--%>
                                                            <%--<img border="0" src="<s:url value="/pages/images/add_doc.png"/>" name="icon_print" width="25px">--%>
                                                        <%--</sj:a>--%>
                                                    <%--</display:column>--%>
                                                    <%--<display:setProperty name="paging.banner.item_name">Project</display:setProperty>--%>
                                                    <%--<display:setProperty name="paging.banner.items_name">Project</display:setProperty>--%>
                                                    <%--<display:setProperty name="export.excel.filename">Project.xls</display:setProperty>--%>
                                                    <%--<display:setProperty name="export.csv.filename">Project.csv</display:setProperty>--%>
                                            <%--</display:table>--%>
                                            <%--</td>--%>
                                        <%--</tr>--%>
                                    <%--</table>--%>

                                </s:form>



                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>


