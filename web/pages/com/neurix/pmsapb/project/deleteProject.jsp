<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>
    <script type="text/javascript">


    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="addProjectForm" method="post"  namespace="/project" action="saveDeleteProject_project" enctype="multipart/form-data"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Add a Project</legend>

                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.noWo">WO Number :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noWo" name="project.noWo" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" disabled="true"/>
                                                                    <s:hidden id="noWo" name="project.noWo" />
                                                                    <s:hidden id="progres" name="project.progres" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.idProject">No. Project :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="idProject" name="project.idProject" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" disabled="true"/>
                                                                    <s:hidden id="idProject" name="project.idProject"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.namaProject">Nama Project :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaProject" name="project.namaProject" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" disabled="true" />
                                                                    <s:hidden id="namaProject" name="project.namaProject"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.stStartDate">Start Date :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <sj:datepicker id="projectStartDateForm" name="project.stStartDate" displayFormat="dd-mm-yy" disabled="true"
                                                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                                                                    <s:hidden id="projectStartDateForm" name="project.stStartDate"/>


                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>


                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.stDeadline">Deadline :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <sj:datepicker id="projectDateForm" name="project.stDeadline" displayFormat="dd-mm-yy" disabled="true"
                                                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                                                                    <s:hidden id="projectDateForm" name="project.stDeadline"/>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.status">Status :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:action id="comboStatus" namespace="/project" name="initComboStatus_project"/>
                                                                    <s:select list="#comboStatus.listOfComboStatus" id="comboStatus" name="project.status" required="true"
                                                                              listKey="idStatus" listValue="namaStatus" headerKey="" headerValue="[Select one]" disabled="true"
                                                                              cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                    <s:hidden id="comboStatus" name="project.status"/>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.idTypeProject">Type Project :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:action id="comboTypeProject" namespace="/project" name="initComboTypeProject_project"/>
                                                                    <s:select list="#comboTypeProject.listOfComboTypeProject" id="comboTypeProject" name="project.idTypeProject" required="true"
                                                                              listKey="idTypeProject" listValue="namaTypeProject" headerKey="" headerValue="[Select one]" disabled="true"
                                                                              cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                    <s:hidden id="comboTypeProject" name="project.idTypeProject"/>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>



                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <table>
                                            <tr>

                                                <td>
                                                    <table>

                                                        <sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"
                                                                   position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                                            Please don't close this window, server is processing your request ...
                                                            </br>
                                                            </br>
                                                            </br>
                                                            <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                        </sj:dialog>
                                                        <sj:submit type="button" cssClass="btn btn-warning" formIds="addProjectForm" id="save" name="save"
                                                                   onClickTopics="showDialog" onCompleteTopics="closeDialog">
                                                            <i class="icon-ok icon-white"></i>
                                                            Delete
                                                        </sj:submit>

                                                    </table>
                                                </td>

                                                <td>
                                                    <table>

                                                        <button type="button" class="btn btn-default" onclick="window.location.href='<s:url action="initForm_project"/>' ">
                                                            <i class="icon-chevron-left icon-black"></i> Back
                                                        </button>

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <sj:dialog id="view_dialog_edit_user" openTopics="showDialogEditUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Edit User">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_user" openTopics="showDialogDeleteUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Delete User">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit_task" openTopics="showDialogEditTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Edit a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_task" openTopics="showDialogDeleteTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Delete a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="40%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Alat :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfAlat" value="#session.listOfResultAlat" scope="request" />
                                                <display:table name="listOfAlat" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                    <display:column property="idAlatDetail" sortable="true" title="<small>Id Alat Detail</small>" />
                                                    <%--<display:column property="idAlat" sortable="true" title="<small>Id Alat</small>" />--%>
                                                    <display:column property="namaAlatDetail" sortable="true" title="<small>Nama Alat</small>" />
                                                    <display:column property="namaApp" sortable="true" title="<small>Wilayah APP</small>" />
                                                    <display:column property="namaGi" sortable="true" title="<small>Start date</small>"/>
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table width="50%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Member :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <s:set name="listOfResultMember" value="#session.listOfResultMember" scope="request" />
                                                <display:table name="listOfResultMember" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">
                                                    <display:column property="idProjectMember" sortable="false" title="<small>Project Member Id</small>"  />
                                                    <display:column property="idMember" sortable="false" title="<small>Member Id</small>"  />
                                                    <display:column property="namaMember" sortable="false" title="<small>Memmber Name</small>"  />
                                                    <display:column property="photoPath" sortable="false" title="<small>Photo</small>"  />
                                                    <display:column property="positionName" sortable="false" title="<small>Position</small>"  />

                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table width="50%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Document :
                                                </h5>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfResultDoc" value="#session.listOfResultDoc" scope="request" />
                                                <display:table name="listOfResultDoc" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">
                                                    <display:column property="idDoc" sortable="false" title="<small>Document Id</small>"  />
                                                    <display:column property="idTask" sortable="false" title="<small>Task</small>"  />
                                                    <display:column property="namaDoc" sortable="false" title="<small>Document Name</small>"  />
                                                    <%--<display:column property="filePath" sortable="false" title="<small>Location</small>"  />--%>
                                                    <display:column media="html" title="<small>View</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlViewDoc" namespace="/project" action="viewDoc_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.namaDoc" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogViewDoc" href="%{urlViewDoc}">
                                                            <img border="0" src="<s:url value="/pages/images/view_gallery.png"/>" name="icon_print" width="25px">
                                                        </sj:a>
                                                    </display:column>
                                                </display:table>

                                            </td>
                                        </tr>
                                    </table>

                                    <table width="30%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Issue :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <s:set name="listOfResult" value="#session.listOfResultIssue" scope="request" />
                                                <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">
                                                    <display:column property="idIssue" sortable="false" title="<small>Issue Id</small>"  />
                                                    <display:column property="namaIssue" sortable="false" title="<small>Issue Name</small>"  />
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                    <sj:dialog id="view_dialog_Task" openTopics="showDialogTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="1000" autoOpen="false" title="Edit Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="100%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Task :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <s:set name="listOfResult" value="#session.listOfResultTask" scope="request" />
                                                <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="100">
                                                    <display:column media="html" title="<small>View Log</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlLogTask" namespace="/project" action="initLogTask_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idTask" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogTask" href="%{urlLogTask}">
                                                            <img border="0" src="<s:url value="/pages/images/view_detail_project.png"/>" name="icon_print">
                                                        </sj:a>

                                                    </display:column>
                                                    <%--<display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">--%>
                                                        <%--&lt;%&ndash;<s:if test = "%{#attr.row.enabledConfirm}" >&ndash;%&gt;--%>
                                                        <%--<s:url var="urlEditTask" namespace="/project" action="initEditTask_project"--%>
                                                               <%--escapeAmp="false">--%>
                                                            <%--<s:param name="id"><s:property value="#attr.row.idTask" /></s:param>--%>
                                                        <%--</s:url>--%>
                                                        <%--<sj:a onClickTopics="showDialogEditTask" href="%{urlEditTask}">--%>
                                                            <%--<img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_print">--%>
                                                        <%--</sj:a>--%>

                                                    <%--</display:column>--%>

                                                    <%--<display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">--%>
                                                        <%--&lt;%&ndash;<s:if test = "%{#attr.row.enabledConfirm}" >&ndash;%&gt;--%>
                                                        <%--<s:url var="urlDeleteTask" namespace="/project" action="initDeleteTask_project"--%>
                                                               <%--escapeAmp="false">--%>
                                                            <%--<s:param name="id"><s:property value="#attr.row.idTask" /></s:param>--%>
                                                        <%--</s:url>--%>
                                                        <%--<sj:a onClickTopics="showDialogDeleteTask" href="%{urlDeleteTask}">--%>
                                                            <%--<img border="0" src="<s:url value="/pages/images/delete_task.png"/>" name="icon_print">--%>
                                                        <%--</sj:a>--%>
                                                    <%--</display:column>--%>
                                                    <display:column property="idTask" sortable="false" title="<small>Id Task</small>"  />
                                                    <display:column property="namaTask" sortable="false" title="<small>Task Name</small>"  />
                                                    <display:column property="namaStatus" sortable="false" title="<small>Status</small>"  />
                                                    <display:column property="asignTo" sortable="false" title="<small>Asign To</small>"  />
                                                    <display:column property="deadline" sortable="false" title="<small>Deadline</small>"  />
                                                    <display:column property="namaPriority" sortable="false" title="<small>Priority</small>"  />
                                                    <display:column property="note" sortable="false" title="<small>Note</small>"/>
                                                    <display:column property="progresBar" sortable="false" title="<small>Progres</small>"/>
                                                    <%--<display:column property="progresName" sortable="false" title="<small>Progres</small>" />--%>

                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                </s:form>



                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>


