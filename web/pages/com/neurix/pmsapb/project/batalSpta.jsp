<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/surveyevaluasiAction.js"/>'></script>--%>
    <script type="text/javascript">


        $.subscribe('beforeProcessSave', function (event, data) {

            var ketBatal = document.getElementById('keteranganBatal').value;
//             var surveyId = document.getElementById('surveyId').value;

            if (ketBatal != '') {

                if (confirm('Do you want to save this record?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialog');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (document.getElementById("keteranganBatal").value == '') {
                    msg = 'Field <strong>Keterangan Batal</strong> is required.' + '<br/>';
                }

//                 if (document.getElementById("surveyId").value == '') {
//                     msg = msg + 'Field <strong>As.Mud</strong> is required.' + '<br/>';
//                 }

                document.getElementById('errorValidationMessage').innerHTML = msg;

                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialog', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialog');
            }
        });

        $.subscribe('errorDialog', function (event, data) {

//            alert(event.originalEvent.request.getResponseHeader('message'));
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function cancelBtn() {
            $('#view_dialog_batal').dialog('close');
        };

        function callSearchFunction() {
            $('#info_dialog').dialog('close');
            $('#view_dialog_batal').dialog('close');
            document.surveySptaForm.action='search_surveyspta.action';
            document.surveySptaForm.submit();
        };



    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="batalSurveySpta" method="post"  namespace="/surveyspta" action="saveBatal_surveyspta"
                    cssClass="well form-horizontal">

                <table>
                    <tr>
                        <td width="10%" align="center">
                            <%@ include file="/pages/common/message.jsp" %>
                        </td>
                    </tr>
                </table>

                <div id="actions" class="form-actions">
                    <table >
                        <tr>
                            <td>
                                <label class="control-label"><small>No. Kontrak : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="SurveySpta.noSpta" />
                                    <s:hidden id="noKontrak" name="surveySpta.noKontrak"/>
                                    <s:hidden id="pabrikGula" name="surveySpta.pabrikGula"/>
                                    <s:hidden id="status" name="surveySpta.status"/>
                                    <s:hidden id="noSpta" name="surveySpta.noSpta"/>

                                </div>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td>
                                <label class="control-label"><small>Lokasi Lahan : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="SurveySpta.tempat" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label"><small>varitas : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="SurveySpta.namaVaritas" />
                                    <s:hidden id="namaVaritas" name="surveySpta.namaVaritas"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td>
                                <label class="control-label"><small>Kategori : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="SurveySpta.namaKategori" />
                                    <s:hidden id="namaKategori" name="surveySpta.namaKategori"/>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label"><small>Petani Id : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="SurveySpta.ketuaPetaniId" />
                                    <s:hidden id="petaniId" name="surveySpta.ketuaPetaniId"/>
                                </div>
                            </td>
                        </tr>
                        <tr>

                        <tr>


                            <td>
                                <label class="control-label"><small>Ketua Kelompok : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="SurveySpta.ketuaKelompok" />
                                    <s:hidden id="ketuaKelompok" name="surveySpta.ketuaKelompok"/>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label"><small>Petani Anggota : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="SurveySpta.namaAnggota" />
                                    <s:hidden id="namaAnggota" name="surveySpta.namaAnggota"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label"><small>Operator GPS : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="SurveySpta.namaOperatorGps" />
                                    <s:hidden id="operatorGps" name="surveySpta.oporatorGps"/>
                                    <s:hidden id="asmud" name="surveySpta.asmud"/>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label">Keterangan Batal : </label>
                            </td>

                            <td>
                                <table>
                                    <s:textarea id="keteranganBatal" name="surveySpta.keteranganBatal" disabled="false" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>
                                <div id="crud">
                                    <table>

                                        <tr>
                                            <td align="center">

                                                <table>

                                                    <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="batalSurveySpta" id="save" name="save"
                                                               onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialog,successDialog"
                                                               onSuccessTopics="successDialog" onErrorTopics="errorDialog" >
                                                        <i class="icon-ok-sign icon-white"></i>
                                                        Batal
                                                    </sj:submit>

                                                    <sj:dialog id="waiting_dialog_batal" openTopics="showDialog" closeTopics="closeDialog" modal="true"
                                                               resizable="false"
                                                               height="250" width="600" autoOpen="false" title="Saving ...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                    </sj:dialog>

                                                    <sj:dialog id="info_dialog" openTopics="showInfoDialog" modal="true" resizable="false" closeOnEscape="false"
                                                               position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                               buttons="{
                                                                    'OK':function() {
                                                                               callSearchFunction();
                                                                         }
                                                                 }"
                                                    >
                                                        <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                        Record has been saved successfully.
                                                    </sj:dialog>

                                                    <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                               position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                               buttons="{
                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                    <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                               position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                               buttons="{
                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                <br/>
                                                                <center><div id="errorValidationMessage"></div></center>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>


                                                </table>
                                            </td>
                                            <td>

                                                <button type="button" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelBtn();">
                                                    <i class="icon-remove-circle"/> Cancel
                                                </button>

                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>
            </s:form>



        </td>
    </tr>
</table>

</body>
</html>

