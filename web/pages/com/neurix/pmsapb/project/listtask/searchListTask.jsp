<%--
  Created by IntelliJ IDEA.
  User: gondok
  Date: 21/09/17
  Time: 10:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type="text/javascript">

    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="listtaskForm" method="post"  namespace="/project" action="searchTask_project"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Search List Task</legend>

                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="task.idProject">Project Id :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="idProject" name="task.idProject" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="task.noWo">No. WO :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="nomorWo" name="task.noWo" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="task.projectName">Project Name :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="projectName" name="task.projectName" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="task.statusTask">Status :</label>
                                                            </td>

                                                            <td>
                                                                <table>

                                                                    <s:select list="#{'1':'Initiate', '2':'In Progres', '3':'Done', '4':'Pending', '5':'Waiting Start'}" id="taskStatus" name="task.statusTask"
                                                                              headerKey="" headerValue="--Select--" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <%--<tr>--%>
                                                <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<tr>--%>
                                                            <%--<td>--%>
                                                                <%--<label class="control-label" for="project.flag">Flag :</label>--%>
                                                            <%--</td>--%>

                                                            <%--<td>--%>
                                                                <%--<table>--%>

                                                                    <%--<s:select list="#{'N':'Non-Active'}" id="flag" name="project.flag"--%>
                                                                              <%--headerKey="Y" headerValue="Active" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>

                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                        <%--</tr>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>
                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <table>
                                            <tr>

                                                <td>
                                                    <table>

                                                        <sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"
                                                                   position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                                            Please don't close this window, server is processing your request .....
                                                            </br>
                                                            </br>
                                                            </br>
                                                            <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                        </sj:dialog>
                                                        <sj:submit type="button" cssClass="btn btn-primary" formIds="listtaskForm" id="search" name="search"
                                                                   onClickTopics="showDialog" onCompleteTopics="closeDialog">
                                                            <i class="icon-search icon-white"></i>
                                                            Search
                                                        </sj:submit>

                                                    </table>
                                                </td>
                                                <td>
                                                    <table>

                                                        <button type="button" class="btn" onclick="window.location.href='<s:url action="initFormTask_project"/>' ">
                                                            <i class="icon-repeat"></i> Reset
                                                        </button>

                                                    </table>
                                                </td>


                                            </tr>
                                        </table>
                                    </div>

                                    <sj:dialog id="view_dialog_Issue" openTopics="showDialogIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="650" autoOpen="false" title="Add a Issue">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_Task" openTopics="showDialogTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="600" width="1000" autoOpen="false" title="Edit Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_add_doc" openTopics="showDialogAddDoc" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Add a Doc">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit_task" openTopics="showDialogEditTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Edit a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_ViewProject" openTopics="showDialogViewProject" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="600" width="1000" autoOpen="false" title="View Data Project">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="100%">
                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfProject" value="#session.listOfResult" scope="request" />

                                                <display:table name="listOfProject" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_project.action" export="true" id="row" pagesize="20" style="font-size:10">
                                                    <display:column media="html" title="<small>View Project</small>" style="text-align:center;font-size:9">

                                                        <s:url var="urlView" namespace="/project" action="viewProjectTask_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idProject" /></s:param>
                                                            <s:param name="idTask"><s:property value="#attr.row.idTask" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogViewProject" href="%{urlView}">
                                                            <img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print" width="25px">
                                                        </sj:a>

                                                    </display:column>
                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                        <s:if test = "%{#attr.row.enabledEdit}" >
                                                        <s:url var="urlViewEdit" namespace="/project" action="editListTask_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idProject" /></s:param>
                                                            <s:param name="idTask"><s:property value="#attr.row.idTask" /></s:param>
                                                            <%--<s:param name="isTas"><s:property value="#attr.row.idTask" /></s:param>--%>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogEditTask" href="%{urlViewEdit}">
                                                            <img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_edit">
                                                        </sj:a>
                                                        </s:if>
                                                        <s:else>
                                                            <img border="0" src="<s:url value="/pages/images/icon_not_edit.png"/>" name="icon_not_edit">
                                                        </s:else>
                                                    </display:column>

                                                    <display:column media="html" title="<small>Edit For PIC</small>" style="text-align:center;font-size:9">

                                                        <s:if test = "%{#attr.row.enabledEditPIC}" >
                                                            <s:url var="urlViewEdit" namespace="/project" action="editListTaskPIC_project"
                                                                   escapeAmp="false">
                                                                <s:param name="id"><s:property value="#attr.row.idProject" /></s:param>
                                                                <s:param name="idTask"><s:property value="#attr.row.idTask" /></s:param>
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogEditTask" href="%{urlViewEdit}">
                                                                <img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_edit">
                                                            </sj:a>
                                                        </s:if>
                                                        <s:else>
                                                            <img border="0" src="<s:url value="/pages/images/icon_not_edit.png"/>" name="icon_not_edit">
                                                        </s:else>

                                                        <%---------------------------------------%>

                                                    </display:column>
                                                    <display:column property="idProject" sortable="true" title="<small>Project Id</small>" />
                                                    <display:column property="noWo" sortable="true" title="<small>Nomor WO</small>" />
                                                    <display:column property="namaProject" sortable="true" title="<small>Project Name</small>" />
                                                    <display:column property="idTask" sortable="true" title="<small>Task Id</small>" />
                                                    <display:column property="namaTask" sortable="true" title="<small>Task Name</small>" />
                                                    <display:column property="startDate" sortable="true" title="<small>Start</small>" />
                                                    <display:column property="deadline" sortable="true" title="<small>Deadline</small>" />
                                                    <display:column media="html" title="<small>Issue</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlIssue" namespace="/project" action="addIssue_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idProject" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogIssue" href="%{urlIssue}">
                                                            <img border="0" src="<s:url value="/pages/images/if_issue.ico"/>" name="icon_print" width="25px">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column media="html" title="<small>Add Document</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlAddDoc" namespace="/project" action="addDocTask_project"
                                                               escapeAmp="false">
                                                            <s:param name="idTask"><s:property value="#attr.row.idTask"/></s:param>
                                                            <s:param name="id"><s:property value="#attr.row.idProject"/></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogAddDoc" href="%{urlAddDoc}">
                                                            <img border="0" src="<s:url value="/pages/images/add_doc.png"/>" name="icon_print" width="25px">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column property="taskStatusText" sortable="true" title="<small>Status</small>" />
                                                    <display:column property="priorityText" sortable="true" title="<small>Priority</small>" />
                                                    <display:column property="progresBarText" sortable="true" title="<small>Progres</small>" />
                                                    <display:column property="isTask" sortable="true" title="<small>Task Induk</small>" />
                                                    <display:setProperty name="export.excel.filename">List_project.xls</display:setProperty>
                                                    <display:setProperty name="export.csv.filename">List_project.csv</display:setProperty>

                                                    <%--<display:column property="action" sortable="true" title="CreatedWho"/>--%>

                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                </s:form>



                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>


