<%--
  Created by IntelliJ IDEA.
  User: gondok
  Date: 23/09/17
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">


        $.subscribe('beforeProcessSave', function (event, data) {

            var namaTask = document.getElementById('namaTask').value;
            var progres = document.getElementById('comboProgres').value;

            if (namaTask != ''&& progres != '') {

                if (confirm('Do you want to save this record?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialog');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (document.getElementById("namaTask").value == '') {
                    msg = 'Field <strong>Task Name</strong> is required.' + '<br/>';
                }
                var msg = "";
                if (document.getElementById("progres").value == '') {
                    msg = 'Field <strong>Progres</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessage').innerHTML = msg;

                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialogEdit', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialogEdit');
            }
        });

        $.subscribe('errorDialogEdit', function (event, data) {
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function cancelEditBtn() {
            $('#view_dialog_edit_task').dialog('close');
        };

        function callSearchFunction() {
            $('#info_dialog_edit').dialog('close');
            $('#view_dialog_edit_task').dialog('close');
            document.listtaskForm.action='searchTask_project.action';
            document.listtaskForm.submit();
        };

    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="editformPIC" method="post" namespace="/project" action="saveEditListTaskPIC_project" enctype="multipart/form-data"
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">Edit Task</legend>

                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Id Project :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="idProject" name="task.idProject" cssClass="textForm" disabled="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                    <%--<s:hidden id="deadline" name="task.stDeadline"/>--%>
                                    <%--<s:hidden id="startDate" name="task.stStartDate"/>--%>
                                    <s:hidden id="createdDate" name="task.createdDate"/>
                                    <s:hidden id="createdWho" name="task.createdWho"/>
                                    <s:hidden id="flag" name="task.flag"/>
                                    <s:hidden id="action" name="task.action"/>
                                    <s:hidden id="position" name="task.position"/>
                                    <s:hidden id="hiddenidProject" name="task.idProject"/>
                                    <s:hidden id="hiddenidTask" name="task.idTask" />
                                    <s:hidden id="hiddennamaTask" name="task.namaTask"/>
                                    <s:hidden id="hiddentask.priority" name="task.priority"/>
                                    <s:hidden id="hiddensttusLastPudate" name="task.statusBefore"/>
                                    <s:hidden id="idTaskMember" name="task.idTaskMember"/>
                                    <s:hidden id="idMember" name="task.idMember"/>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Id Task :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="idTask" name="task.idTask" cssClass="textForm" disabled="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Task Name :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaTask" name="task.namaTask" cssClass="textForm" disabled="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table>
                                    <label class="control-label" for="task.priority"><small>Priority :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:action id="comboPriority" namespace="/project" name="initComboPriority_project" />
                                    <s:select list="#comboPriority.listOfComboPriority" id="comboPriority" name="task.priority" required="true" disabled="true"
                                              listKey="idPriority" listValue="namaPriority" headerKey="" headerValue="[Select one]"
                                              cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label" for="task.stStartDate">Start Date :</label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <sj:datepicker id="taskStartDateForm" name="task.stStartDate" displayFormat="dd-mm-yy"
                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true"  changeYear="true" cssClass="input-small" />
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label" for="task.stDeadline">Deadline :</label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <sj:datepicker id="taskDateForm" name="task.stDeadline" displayFormat="dd-mm-yy"
                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Progres :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:action id="comboProgres" namespace="/project" name="initComboProgres_project"/>
                                    <s:select list="#comboProgres.listOfComboProgres" id="comboProgres" name="task.progres" required="true" disabled="false"
                                              listKey="idProgres" listValue="namProgres" headerKey="" headerValue="[Select one]"
                                              cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <label class="control-label"><small>Note :</small></label>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <s:textarea id="note" name="task.note" disabled="false" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>

                <div id="actions" class="form-actions">
                    <table>
                        <tr>
                            <td></td>
                            <td>
                                <div id="crud">
                                    <table>

                                        <tr>
                                            <td align="center">

                                                <table>

                                                    <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="editformPIC" id="save" name="save"
                                                               onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialogEdit,successDialogEdit"
                                                               onSuccessTopics="successDialogEdit" onErrorTopics="errorDialogEdit" >
                                                        <i class="icon-ok-sign icon-white"></i>
                                                        Save
                                                    </sj:submit>

                                                    <sj:dialog id="waiting_dialog_edit" openTopics="showDialogEdit" closeTopics="closeDialogEdit" modal="true"
                                                               resizable="false"
                                                               height="250" width="600" autoOpen="false" title="Saving ...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                    </sj:dialog>

                                                    <sj:dialog id="info_dialog_edit" openTopics="showInfoDialogEdit" modal="true" resizable="false" closeOnEscape="false"
                                                               position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                               buttons="{
                                                                    'OK':function() {
                                                                               callSearchFunction();
                                                                         }
                                                                 }"
                                                    >
                                                        <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                        Record has been saved successfully.
                                                    </sj:dialog>

                                                    <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                               position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                               buttons="{
                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                    <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                               position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                               buttons="{
                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                <br/>
                                                                <center><div id="errorValidationMessage"></div></center>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                </table>
                                            </td>

                                            <td>
                                                <button type="button" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelEditBtn();">
                                                    <i class="icon-remove-circle "/> Cancel
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>

            </s:form>


        </td>
    </tr>
</table>

</body>
</html>