<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>
    <script type="text/javascript">

        $.subscribe('beforeProcessSave', function (event, data) {

            var noWo = document.getElementById('noWo').value;

            if (noWo != '') {

                if (confirm('Do you want to save this record ?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialog');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (document.getElementById("noWo").value == '') {
                    msg = 'Field <strong>No Wo</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessage').innerHTML = msg;
                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialog', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialog');
            }
        });

        $.subscribe('errorDialog', function (event, data) {

//            alert(event.originalEvent.request.getResponseHeader('message'));
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function callSearchFunction() {
            $('#info_dialog').dialog('close');
            document.addProjectForm.action='initForm_project.action';
            document.addProjectForm.submit();
        };

    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="addProjectForm" method="post"  namespace="/project" action="saveProject_project" enctype="multipart/form-data"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Add a Project</legend>

                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Divisi :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:action id="comboDivisi" namespace="/project" name="initComboDivisi_project"/>
                                                                    <s:select list="#comboDivisi.listOfComboDivisi" id="comboDivisi" name="project.idDivisi" required="true"
                                                                              listKey="idDivisi" listValue="namaDivisi" headerKey="" headerValue="[Select one]"
                                                                              cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.namaProject">Nama Project :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaProject" name="project.namaProject" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.noWo">WO Number :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noWo" name="project.noWo" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.stStartDate">Start Date :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <sj:datepicker id="projectStartDateForm" name="project.stStartDate" displayFormat="dd-mm-yy"
                                                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />

                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.stDeadline">Deadline :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <sj:datepicker id="projectDateForm" name="project.stDeadline" displayFormat="dd-mm-yy"
                                                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />

                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.idTypeProject">Type Project :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:action id="comboTypeProject" namespace="/project" name="initComboTypeProject_project"/>
                                                                    <s:select list="#comboTypeProject.listOfComboTypeProject" id="comboTypeProject" name="project.idTypeProject" required="true"
                                                                              listKey="idTypeProject" listValue="namaTypeProject" headerKey="" headerValue="[Select one]"
                                                                              cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.stDeadline">Upload IMG/PDF :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:file id="fileUploadDoc" name="fileUploadDoc"  cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>



                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <div id="crud">
                                            <table>
                                                <tr>

                                                    <td>
                                                        <table>

                                                            <%--<sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"--%>
                                                                       <%--position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">--%>
                                                                <%--Please don't close this window, server is processing your request ...--%>
                                                                <%--</br>--%>
                                                                <%--</br>--%>
                                                                <%--</br>--%>
                                                                <%--<img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">--%>
                                                            <%--</sj:dialog>--%>
                                                            <%--<sj:submit type="button" cssClass="btn btn-primary" formIds="addProjectForm" id="save" name="save"--%>
                                                                       <%--onClickTopics="showDialog" onCompleteTopics="closeDialog">--%>
                                                                <%--<i class="icon-ok icon-white"></i>--%>
                                                                <%--Save--%>
                                                            <%--</sj:submit>--%>

                                                                <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="addProjectForm" id="save" name="save"
                                                                           onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialog,successDialog"
                                                                           onSuccessTopics="successDialog" onErrorTopics="errorDialog" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;">
                                                                    <i class="icon-ok-sign icon-white"></i>
                                                                    Save
                                                                </sj:submit>

                                                                <sj:dialog id="waiting_dialog_save" openTopics="showDialog" closeTopics="closeDialog" modal="true"
                                                                           resizable="false"
                                                                           height="250" width="600" autoOpen="false" title="Saving ...">
                                                                    Please don't close this window, server is processing your request ...
                                                                    </br>
                                                                    </br>
                                                                    </br>
                                                                    <img id="image_write" border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                                </sj:dialog>

                                                                <sj:dialog id="info_dialog" openTopics="showInfoDialog" modal="true" resizable="false" closeOnEscape="false"
                                                                           position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                                           buttons="{
                                                                                    'OK':function() {
                                                                                               callSearchFunction();
                                                                                         }
                                                                                 }"
                                                                >
                                                                    <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                                    Record has been saved successfully.
                                                                </sj:dialog>

                                                                <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                                           position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                                           buttons="{
                                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                                }"
                                                                >
                                                                    <div class="alert alert-error fade in">
                                                                        <label class="control-label" align="left">
                                                                            <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                                        </label>
                                                                    </div>
                                                                </sj:dialog>

                                                                <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                                           position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                                           buttons="{
                                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                                }"
                                                                >
                                                                    <div class="alert alert-error fade in">
                                                                        <label class="control-label" align="left">
                                                                            <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                            <br/>
                                                                            <center><div id="errorValidationMessage"></div></center>
                                                                        </label>
                                                                    </div>
                                                                </sj:dialog>

                                                                <sj:dialog id="info_dialog_update_session" openTopics="showInfoDialogUpdateSession" modal="true" resizable="false"
                                                                           position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                                           buttons="{
                                                                                        'OK':function() { $('#info_dialog_update_session').dialog('close'); }
                                                                                    }"
                                                                >
                                                                    <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                                    Found failure when saving, please try again or call your admin.
                                                                </sj:dialog>

                                                        </table>
                                                    </td>

                                                    <td>
                                                        <table>
                                                            <sj:dialog id="view_dialog_add_user" openTopics="showDialogAddUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                       position="center" height="400" width="600" autoOpen="false" title="Add User"
                                                            >
                                                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                            </sj:dialog>

                                                            <s:url var="urlAddUser" namespace="/project" action="addUser_project"
                                                                   escapeAmp="false">
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddUser" href="%{urlAddUser}" cssClass="btn btn-warning">
                                                                <i class="icon-user icon-white"></i>
                                                                <small>Add Member</small>
                                                            </sj:a>
                                                        </table>
                                                    </td>

                                                    <td>
                                                        <table>
                                                            <sj:dialog id="view_dialog_add_task" openTopics="showDialogAddTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                       position="center" height="500" width="600" autoOpen="false" title="Add a Task"
                                                            >
                                                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                            </sj:dialog>

                                                            <s:url var="urlAddTask" namespace="/project" action="addTask_project"
                                                                   escapeAmp="false">
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddTask" href="%{urlAddTask}" cssClass="btn btn-warning">
                                                                <i class="icon-list-alt icon-white"></i>
                                                                <small>Add Task</small>
                                                            </sj:a>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <sj:dialog id="view_dialog_add_issue" openTopics="showDialogAddIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                       position="center" height="400" width="600" autoOpen="false" title="Add Issue"
                                                            >
                                                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                            </sj:dialog>

                                                            <s:url var="urlAddIssue" namespace="/project" action="addProjectIssue_project"
                                                                   escapeAmp="false">
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddIssue" href="%{urlAddIssue}" cssClass="btn btn-warning">
                                                                <i class="icon-user icon-white"></i>
                                                                <small>Add Issue</small>
                                                            </sj:a>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <sj:dialog id="view_dialog_add_alat" openTopics="showDialogAddAlat" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                       position="center" height="400" width="600" autoOpen="false" title="Add Issue"
                                                            >
                                                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                            </sj:dialog>

                                                            <s:url var="urlAddAlat" namespace="/project" action="addAlat_project"
                                                                   escapeAmp="false">
                                                            </s:url>
                                                            <sj:a onClickTopics="showDialogAddAlat" href="%{urlAddAlat}" cssClass="btn btn-warning">
                                                                <i class="icon-user icon-white"></i>
                                                                <small>Add Alat</small>
                                                            </sj:a>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table>

                                                            <button type="button" class="btn" onclick="window.location.href='<s:url action="addProject_project"/>' ">
                                                                <i class="icon-repeat"></i> Reset
                                                            </button>

                                                        </table>
                                                    </td>


                                                    <td>
                                                        <table>

                                                            <button type="button" class="btn btn-default" onclick="window.location.href='<s:url action="initForm_project"/>' ">
                                                                <i class="icon-chevron-left icon-black"></i> Back
                                                            </button>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <sj:dialog id="view_dialog_edit_user" openTopics="showDialogEditUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Edit User">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit_issue" openTopics="showDialogEditIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Edit Issue">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_issue" openTopics="showDialogDeleteIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Delete Issue">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_user" openTopics="showDialogDeleteUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Delete User">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit_task" openTopics="showDialogEditTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Edit a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_task" openTopics="showDialogDeleteTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Delete a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_add_member_task" openTopics="showDialogAddMemberTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Add Task Team">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="40%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Alat :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfAlat" value="#session.listOfResultAlat" scope="request" />
                                                <display:table name="listOfAlat" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                    <display:column property="idAlatDetail" sortable="true" title="<small>Id Alat Detail</small>" />
                                                    <%--<display:column property="idAlat" sortable="true" title="<small>Id Alat</small>" />--%>
                                                    <display:column property="namaAlatDetail" sortable="true" title="<small>Nama Alat</small>" />
                                                    <display:column property="namaApp" sortable="true" title="<small>Wilayah APP</small>" />
                                                    <display:column property="namaGi" sortable="true" title="<small>Start date</small>"/>
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table width="50%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Member :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <s:set name="listOfResult" value="#session.listOfResultMember" scope="request" />
                                                <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">

                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlEditUser" namespace="/project" action="editMember_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idProjectMember" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogEditUser" href="%{urlEditUser}">
                                                            <img border="0" src="<s:url value="/pages/images/edit_member.png"/>" name="icon_print">
                                                        </sj:a>
                                                    </display:column>

                                                    <display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlDeleteUser" namespace="/project" action="deleteMember_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idProjectMember" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogDeleteUser" href="%{urlDeleteUser}">
                                                            <img border="0" src="<s:url value="/pages/images/delete_member.png"/>" name="icon_print">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column property="idProjectMember" title="<small>Project Member Id</small>" />
                                                    <display:column property="idMember"  title="<small>Member Id</small>" />
                                                    <display:column property="namaMember"  title="<small>Member Name</small>"  />
                                                    <display:column property="photoPath"  title="<small>Foto</small>"  />
                                                    <display:column property="roleName"  title="<small>Role</small>"  />
                                                    <display:column property="positionName" title="<small>Position</small>"  />
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table width="30%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Issue :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <s:set name="listOfResult" value="#session.listOfResultIssue" scope="request" />
                                                <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">

                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlEditIssue" namespace="/project" action="editProjectIssue_project"
                                                               escapeAmp="false">
                                                            <%--<s:param name="id"><s:property value="#attr.row.idProjectMember" /></s:param>--%>
                                                            <s:param name="id"><s:property value="#attr.row.idIssue" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogEditIssue" href="%{urlEditIssue}">
                                                            <img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_print">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlDeleteIssue" namespace="/project" action="deleteProjectIssue_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idIssue" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogDeleteIssue" href="%{urlDeleteIssue}">
                                                            <img border="0" src="<s:url value="/pages/images/delete_task.png"/>" name="icon_print">
                                                        </sj:a>
                                                    </display:column>


                                                    <display:column property="idIssue" sortable="false" title="<small>Issue Id</small>"  />
                                                    <display:column property="namaIssue" sortable="false" title="<small>Issue Name</small>"  />
                                                    <display:column property="idMember" sortable="false" title="<small>Id Member</small>"  />
                                                    <display:column property="lastUpdateWho" sortable="false" title="<small>From</small>"  />
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                    <sj:dialog id="view_dialog_list_Task" openTopics="showDialogListTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="500" autoOpen="false" title="View List Team">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="60%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Task :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfTask" value="#session.listOfResultTask" scope="request" />
                                                <display:table name="listOfTask" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlEditTask" namespace="/project" action="editSessionTask_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idTask" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogEditTask" href="%{urlEditTask}">
                                                            <img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_print">
                                                        </sj:a>

                                                    </display:column>

                                                    <display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlDeleteTask" namespace="/project" action="deleteSessionTask_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idTask" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogDeleteTask" href="%{urlDeleteTask}">
                                                            <img border="0" src="<s:url value="/pages/images/delete_task.png"/>" name="icon_print">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column property="idTask" sortable="true" title="<small>Task Id</small>" />
                                                    <display:column property="namaTask" sortable="true" title="<small>Task Name</small>" />
                                                    <display:column property="asignTo" sortable="true" title="<small>Asign To</small>" />
                                                    <display:column property="stStartDate" sortable="true" title="<small>Start date</small>"/>
                                                    <display:column property="stDeadline" sortable="true" title="<small>Deadline</small>"/>

                                                    <display:column media="html" title="<small>Add Team</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlAddMemberTask" namespace="/project" action="initAddMemberTask_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idTask" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogAddMemberTask" href="%{urlAddMemberTask}">
                                                            <img border="0" src="<s:url value="/pages/images/edit_member.png"/>" name="icon_print">
                                                        </sj:a>
                                                    </display:column>

                                                    <display:column media="html" title="<small>View Team</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlViewMemberTask" namespace="/project" action="viewListTaskMember_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idTask" /></s:param>
                                                            <s:param name="atr"><s:property value="'add'" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogListTask" href="%{urlViewMemberTask}">
                                                            <img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print">
                                                        </sj:a>

                                                    </display:column>


                                                    <display:column property="namaStatus" sortable="true" title="<small>Status</small>"/>
                                                    <display:column property="namaPriority" sortable="true" title="<small>Priority</small>"/>
                                                    <display:column property="note" sortable="true" title="<small>Note</small>"  />
                                            </display:table>
                                            </td>
                                        </tr>
                                    </table>
                                </s:form>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>


