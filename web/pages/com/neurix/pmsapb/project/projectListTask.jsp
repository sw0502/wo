<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        function closeActivation(){
            $('#view_dialog_Task').dialog('close');
            document.projectForm.action='search_project.action';
            document.projectForm.submit();
        }

    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="listTaskProject" method="post" namespace="" action="viewTaskList_project"
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">Edit Task</legend>

                    <table >
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>

                    </table>


                </fieldset>



                    <table width="100%">
                    <tr>
                        <td align="center">


                            <sj:dialog id="view_dialog_edit_task" openTopics="showDialogViewEdit" modal="true" resizable="false" cssStyle="text-align:left;"
                                       position="center" height="550" width="700" autoOpen="false" title="View Task">
                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                            </sj:dialog>
                            <sj:dialog id="view_dialog_delete_task" openTopics="showDialogViewDelete" modal="true" resizable="false" cssStyle="text-align:left;"
                                       position="center" height="550" width="700" autoOpen="false" title="Delete Task">
                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                            </sj:dialog>
                            <sj:dialog id="view_dialog_add_doc" openTopics="showDialogAddDoc" modal="true" resizable="false" cssStyle="text-align:left;"
                                       position="center" height="400" width="600" autoOpen="false" title="Add a Doc">
                                <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                            </sj:dialog>

                            <s:set name="listOfResult" value="#session.listOfResultTaskList" scope="request" />
                            <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                           requestURI="#" id="row" export="false" style="font-size:11" pagesize="100">
                                <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">

                                    <s:url var="urlView" namespace="/project" action="viewEditTask_project"
                                           escapeAmp="false">
                                        <s:param name="id"><s:property value="#attr.row.idTask"/></s:param>
                                        <s:param name="id1"><s:property value="#attr.row.idProject"/></s:param>
                                    </s:url>
                                    <sj:a onClickTopics="showDialogViewEdit" href="%{urlView}">
                                        <img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_print" width="25px">
                                    </sj:a>

                                </display:column>

                                <display:column media="html" title="<small>delete</small>" style="text-align:center;font-size:9">

                                    <s:url var="urlViewDelete" namespace="/project" action="viewDeleteTask_project"
                                           escapeAmp="false">
                                        <s:param name="id"><s:property value="#attr.row.idTask"/></s:param>
                                    </s:url>
                                    <sj:a onClickTopics="showDialogViewDelete" href="%{urlViewDelete}">
                                        <img border="0" src="<s:url value="/pages/images/delete_task.png"/>" name="icon_print" width="25px">
                                    </sj:a>

                                </display:column>

                                <display:column media="html" title="<small>Add Document</small>" style="text-align:center;font-size:9">
                                    <s:url var="urlAddDoc" namespace="/project" action="addDoc_project"
                                           escapeAmp="false">
                                        <%--<s:param name="id"><s:property value="#attr.row.idProject" /></s:param>--%>
                                        <s:param name="id"><s:property value="#attr.row.idTask"/></s:param>
                                        <%--<s:param name="id1"><s:property value="#attr.row.idProject"/></s:param>--%>
                                    </s:url>
                                    <sj:a onClickTopics="showDialogAddDoc" href="%{urlAddDoc}">
                                        <img border="0" src="<s:url value="/pages/images/add_doc.png"/>" name="icon_print" width="25px">
                                    </sj:a>
                                </display:column>

                                <display:column property="idTask" sortable="false" title="<small>Id Task</small>"  />
                                <display:column property="namaTask" sortable="false" title="<small>Task Name</small>"  />
                                <display:column property="namaStatus" sortable="false" title="<small>Status</small>"  />
                                <display:column property="asignTo" sortable="false" title="<small>Asign To</small>"  />
                                <display:column property="startDate" sortable="false" title="<small>Start Date</small>"  />
                                <display:column property="deadline" sortable="false" title="<small>Deadline</small>"  />
                                <display:column property="namaPriority" sortable="false" title="<small>Priority</small>"  />
                                <display:column property="note" sortable="false" title="<small>Note</small>"  />
                                <display:column property="progresBar" sortable="false" title="<small>Progres</small>"/>
                            </display:table>

                        </td>
                    </tr>

                </table>

                <table>
                    <tr>
                        <td>

                    <button type="button" class="btn btn-danger" onclick="closeActivation();">
                        <i class="icon-remove-circle icon-white"></i> Close
                    </button>
                        </td>
                    </tr>
                </table>



            </s:form>

        </td>
    </tr>
</table>

</body>
</html>


