<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%--<action name="tags" class="tags">--%>
<%--<result type="json">--%>
<%--<param name="root">tags</param>--%>
<%--</result>--%>
<%--</action>--%>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">


        $.subscribe('beforeProcessSave', function (event, data) {


            var noSpta  = document.getElementById('noSpta').value;
            if (noSpta != '') {

                if (confirm('Do you want to save this record ?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialog');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {
                event.originalEvent.options.submit = false;

                var msg = "";

                if (noSpta == '') {
                    msg = 'Field <strong>Nomor SPTA</strong> is required.' + '<br/>';
                }
                document.getElementById('errorValidationMessage').innerHTML = msg;

                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialog', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialog');
            }
        });

        $.subscribe('errorDialog', function (event, data) {
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function cancelBtn() {
            $('#view_dialog_confirm').dialog('close');
        };

        function callSearchFunction() {
            $('#info_dialog').dialog('close');
            $('#view_dialog_confirm').dialog('close');
            document.surveySptaForm.action='search_surveyspta.action';
            document.surveySptaForm.submit();
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">


            <s:form id="formConfirm" method="post"  namespace="/surveyspta" action="saveConfirm_surveyspta" cssClass="well form-horizontal">
                <fieldset>

                    <legend align="left">Survey Spta Form</legend>
                    <table >


                        <tr>
                            <td>
                                <label class="control-label"><small>No. Survey Spta         :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noSpta" name="SurveySpta.noSpta" disabled="true" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="SurveySpta.noSpta"/>

                                </table>

                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label"><small>No. Kontrak         :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="noKontrak" name="surveySpta.noKontrak" disabled="true" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="surveySpta.noKontrak"/>

                                </table>

                            </td>
                        </tr>


                        <tr>
                            <td>
                                <label class="control-label"><small>Kode Petak            :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="lahanId" name="surveySpta.LahanId" disabled="true" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="surveySpta.lahanId"/>

                                </table>

                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label"><small>Ketua Petani Id     :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="ketuaPetaniId" name="surveySpta.ketuaPetaniId" disabled="true" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="surveySpta.ketuaPetaniId"/>

                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label"><small>Nama Ketua Petani   :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="ketuaKelompok" name="surveySpta.ketuaKelompok" disabled="true" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="surveySpta.ketuaKelompok"/>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label"><small>Petani Anggota Id :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="anggotaPetaniId" name="surveySpta.anggotaPetaniId" disabled="true" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="surveySpta.anggotaPetaniId"/>


                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label"><small>Nama Petani Anggota :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaAnggota" name="surveySpta.namaAnggota" disabled="true" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="surveySpta.namaAnggota"/>
                                </table>

                            </td>
                        </tr>


                        <tr>
                            <td>
                                <label class="control-label"><small>Varitas Name :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="varitasName" name="surveySpta.namaVaritasLama" disabled="true"  cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="surveySpta.varitasId"/>
                                        <%--<s:hidden name="surveySpta.varitasName"/>--%>
                                </table>

                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label"><small>Kategori Name :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="kategoriName" name="surveySpta.namaKategoriLama" disabled="true"  cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="surveySpta.kategoriId"/>
                                        <%--<s:hidden name="surveySpta.kategoriName"/>--%>
                                </table>

                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label"><small>New Varitas Name :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="VaritasName" name="surveySpta.namaVaritas" disabled="true"  cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="surveySpta.newVaritasId"/>
                                </table>

                            </td>
                        </tr>



                        <tr>
                            <td>
                                <label class="control-label"><small>New Kategori Name :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="KategoriName" name="surveySpta.namaKategori" disabled="true"  cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden name="surveySpta.newKategoriId"/>
                                </table>

                            </td>
                        </tr>


                            <tr>
                            <td>
                            <label class="control-label"><small>Pol :</small></label>
                            </td>
                            <td>
                            <table>
                            <s:textfield id="pol" name="surveySpta.pol" disabled="true" cssClass="textForm"  cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            <s:hidden name="surveySpta.pol"/>
                            </table>

                            </td>
                            </tr>


                            <tr>
                            <td>
                            <label class="control-label"><small>Brix :</small></label>
                            </td>
                            <td>
                            <table>
                            <s:textfield id="brix" name="surveySpta.brix" disabled="true" cssClass="textForm" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            <s:hidden name="surveySpta.brix"/>

                            </table>

                            </td>
                            </tr>

                            <tr>
                            <td>
                            <label class="control-label"><small>Fiber :</small></label>
                            </td>
                            <td>
                            <table>
                            <s:textfield id="fiber" name="surveySpta.fiber" disabled="true" cssClass="textForm"  cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            <s:hidden name="surveySpta.fiber"/>

                            </table>

                            </td>
                            </tr>

                            <tr>
                            <td>
                            <label class="control-label"><small>Rendemen :</small></label>
                            </td>
                            <td>
                            <table>
                            <s:textfield id="Rendemen" name="surveySpta.Rendemen" disabled="true" cssClass="textForm"  cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            <s:hidden name="surveySpta.rendemen"/>

                            </table>
                            </td>
                            </tr>

                        <tr>
                            <td>
                                <label class="control-label"><small>Perkiraan Sisa Luasan   :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="perkiraanSisa" name="surveySpta.perkiraanSisa" disabled="true" cssClass="textForm" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                        <%--<s:hidden name="surveySpta.perkiraanSisa"/>--%>
                                </table>

                            </td>
                        </tr>


                        <tr>
                            <td>
                                <label class="control-label"><small>Perkiraan Sisa Ton    :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="perkiraanTon" name="surveySpta.perkiraanTon" disabled="true" cssClass="textForm" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                        <%--<s:hidden name="surveySpta.perkiraanTon"/>--%>
                                </table>

                            </td>
                        </tr>

                    </table>
                </fieldset>



                <div id="actions" class="form-actions">
                    <table>
                        <tr>
                            <td></td>
                            <td>
                                <div id="crud">
                                    <table>

                                        <tr>
                                            <td align="center">

                                                <table>

                                                    <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="formConfirm" id="save" name="save"
                                                               onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialog,successDialog"
                                                               onSuccessTopics="successDialog" onErrorTopics="errorDialog" >
                                                        <i class="icon-ok-sign icon-white"></i>
                                                        Save
                                                    </sj:submit>

                                                    <sj:dialog id="waiting_dialog_confim" openTopics="showDialog" closeTopics="closeDialog" modal="true"
                                                               resizable="false"
                                                               height="250" width="600" autoOpen="false" title="Saving ...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                    </sj:dialog>

                                                    <sj:dialog id="info_dialog" openTopics="showInfoDialog" modal="true" resizable="false" closeOnEscape="false"
                                                               position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                               buttons="{
                                                                    'OK':function() {
                                                                               callSearchFunction();
                                                                         }
                                                                 }"
                                                    >
                                                        <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                        Record has been saved successfully.
                                                    </sj:dialog>

                                                    <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                               position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                               buttons="{
                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                    <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                               position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                               buttons="{
                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                <br/>
                                                                <center><div id="errorValidationMessage"></div></center>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>


                                                </table>
                                            </td>

                                            <td>
                                                <button type="button" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelBtn();">
                                                    <i class="icon-remove-circle "/> Cancel
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>

            </s:form>

        </td>
    </tr>
</table>

</body>
</html>

