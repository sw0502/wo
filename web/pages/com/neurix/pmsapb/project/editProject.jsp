<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>
    <script type="text/javascript">

        $.subscribe('beforeProcessSave', function (event, data) {

            var noWo = document.getElementById('noWo').value;

            if (noWo != '') {

                if (confirm('Do you want to save this record ?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialog');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (document.getElementById("noWo").value == '') {
                    msg = 'Field <strong>No Wo</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessage').innerHTML = msg;
                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialog', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialog');
            }
        });

        $.subscribe('errorDialog', function (event, data) {

//            alert(event.originalEvent.request.getResponseHeader('message'));
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function callSearchFunction() {
            $('#info_dialog').dialog('close');
            document.addProjectForm.action='initForm_project.action';
            document.addProjectForm.submit();
        };


    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="addProjectForm" method="post"  namespace="/project" action="saveEditProject_project" enctype="multipart/form-data"
                                        cssClass="well form-horizontal">
                                    <fieldset>
                                        <legend align="left">Edit Project</legend>
                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.idTypeProject">Type Project :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:action id="comboTypeProject" namespace="/project" name="initComboTypeProject_project"/>
                                                                    <s:select list="#comboTypeProject.listOfComboTypeProject" id="comboTypeProject" name="project.idTypeProject" required="true"
                                                                              listKey="idTypeProject" listValue="namaTypeProject" headerKey="" headerValue="[Select one]"
                                                                              cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.noWo">WO Number :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="noWo" name="project.noWo" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" disabled="true"/>
                                                                    <%--<s:textfield id="progres" name="project.progres" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" disabled="true"/>--%>
                                                                    <s:hidden id="noWo" name="project.noWo" />
                                                                    <s:hidden id="progres" name="project.progres" />
                                                                    <s:hidden id="idDivisi" name="project.idDivisi" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.idProject">No. Project :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="idProject" name="project.idProject" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" disabled="true"/>
                                                                    <s:hidden id="idProject" name="project.idProject"/>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.namaProject">Nama Project :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaProject" name="project.namaProject" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.stStartDate">Start Date :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <sj:datepicker id="projectStartDateForm" name="project.stStartDate" displayFormat="dd-mm-yy"
                                                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />

                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>


                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.stDeadline">Deadline :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <sj:datepicker id="projectDateForm" name="project.stDeadline" displayFormat="dd-mm-yy"
                                                                                   showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />

                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.status">Status :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:action id="comboStatus" namespace="/project" name="initComboStatus_project"/>
                                                                    <s:select list="#comboStatus.listOfComboStatus" id="comboStatus" name="project.status" required="true"
                                                                              listKey="idStatus" listValue="namaStatus" headerKey="" headerValue="[Select one]"
                                                                              cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>

                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="project.stDeadline">Upload IMG/PDF :</label>
                                                            </td>
                                                            <td>
                                                                <table>
                                                                    <s:file id="fileUploadDoc" name="fileUploadDoc"  cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>


                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <div id="crud">
                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                            <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="addProjectForm" id="save" name="save"
                                                                       onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialog,successDialog"
                                                                       onSuccessTopics="successDialog" onErrorTopics="errorDialog" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;">
                                                                <i class="icon-ok-sign icon-white"></i>
                                                                Save
                                                            </sj:submit>

                                                            <sj:dialog id="waiting_dialog_save" openTopics="showDialog" closeTopics="closeDialog" modal="true"
                                                                       resizable="false"
                                                                       height="250" width="600" autoOpen="false" title="Saving ...">
                                                                Please don't close this window, server is processing your request ...
                                                                </br>
                                                                </br>
                                                                </br>
                                                                <img id="image_write" border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                            </sj:dialog>

                                                            <sj:dialog id="info_dialog" openTopics="showInfoDialog" modal="true" resizable="false" closeOnEscape="false"
                                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                                       buttons="{
                                                                                    'OK':function() {
                                                                                               callSearchFunction();
                                                                                         }
                                                                                 }"
                                                            >
                                                                <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                                Record has been saved successfully.
                                                            </sj:dialog>

                                                            <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                                       position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                                       buttons="{
                                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                                }"
                                                            >
                                                                <div class="alert alert-error fade in">
                                                                    <label class="control-label" align="left">
                                                                        <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                                    </label>
                                                                </div>
                                                            </sj:dialog>

                                                            <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                                       position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                                       buttons="{
                                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                                }"
                                                            >
                                                                <div class="alert alert-error fade in">
                                                                    <label class="control-label" align="left">
                                                                        <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                        <br/>
                                                                        <center><div id="errorValidationMessage"></div></center>
                                                                    </label>
                                                                </div>
                                                            </sj:dialog>

                                                            <sj:dialog id="info_dialog_update_session" openTopics="showInfoDialogUpdateSession" modal="true" resizable="false"
                                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                                       buttons="{
                                                                                        'OK':function() { $('#info_dialog_update_session').dialog('close'); }
                                                                                    }"
                                                            >
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                                Found failure when saving, please try again or call your admin.
                                                            </sj:dialog>

                                                        <%--<sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"--%>
                                                                   <%--position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">--%>
                                                            <%--Please don't close this window, server is processing your request ...--%>
                                                            <%--</br>--%>
                                                            <%--</br>--%>
                                                            <%--</br>--%>
                                                            <%--<img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">--%>
                                                        <%--</sj:dialog>--%>
                                                        <%--<sj:submit type="button" cssClass="btn btn-primary" formIds="addProjectForm" id="save" name="save"--%>
                                                                   <%--onClickTopics="showDialog" onCompleteTopics="closeDialog">--%>
                                                            <%--<i class="icon-ok icon-white"></i>--%>
                                                            <%--Save--%>
                                                        <%--</sj:submit>--%>
                                                    </table>
                                                </td>

                                                <td>
                                                    <table>
                                                        <sj:dialog id="view_dialog_add_user" openTopics="showDialogAddUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                   position="center" height="400" width="600" autoOpen="false" title="Add User"
                                                        >
                                                            <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                        </sj:dialog>

                                                        <s:url var="urlAddUser" namespace="/project" action="addMemberEditProject_project"
                                                               escapeAmp="false">
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogAddUser" href="%{urlAddUser}" cssClass="btn btn-warning">
                                                            <i class="icon-user icon-white"></i>
                                                            <small>Add Member</small>
                                                        </sj:a>
                                                    </table>
                                                </td>

                                                <td>
                                                    <table>
                                                        <sj:dialog id="view_dialog_Issue" openTopics="showDialogIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                   position="center" height="400" width="650" autoOpen="false" title="Add a Issue">
                                                            <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                        </sj:dialog>

                                                        <s:url var="urlAddIssue" namespace="/project" action="addIssueEditProject_project"
                                                               escapeAmp="false">
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogIssue" href="%{urlAddIssue}" cssClass="btn btn-warning">
                                                            <i class="icon-user icon-white"></i>
                                                            <small>Add Issue</small>
                                                        </sj:a>
                                                    </table>
                                                </td>

                                                <td>
                                                    <table>
                                                        <sj:dialog id="view_dialog_add_task" openTopics="showDialogAddTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                   position="center" height="500" width="600" autoOpen="false" title="Add a Task"
                                                        >
                                                            <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                        </sj:dialog>

                                                        <s:url var="urlAddTask" namespace="/project" action="addTaskEditProject_project"
                                                               escapeAmp="false">
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogAddTask" href="%{urlAddTask}" cssClass="btn btn-warning">
                                                            <i class="icon-list-alt icon-white"></i>
                                                            <small>Add Task</small>
                                                        </sj:a>
                                                    </table>
                                                </td>

                                                    <%--<td>--%>
                                                    <%--<table>--%>
                                                    <%--<sj:dialog id="view_dialog_add_doc" openTopics="showDialogAddDoc" modal="true" resizable="false" cssStyle="text-align:left;"--%>
                                                    <%--position="center" height="500" width="600" autoOpen="false" title="Add a Doc"--%>
                                                    <%-->--%>
                                                    <%--<center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>--%>
                                                    <%--</sj:dialog>--%>

                                                    <%--<s:url var="urlAddDoc" namespace="/project" action="addSessionDoc_project"--%>
                                                    <%--escapeAmp="false">--%>
                                                    <%--</s:url>--%>
                                                    <%--<sj:a onClickTopics="showDialogAddDoc" href="%{urlAddDoc}" cssClass="btn btn-warning">--%>
                                                    <%--<i class="icon-briefcase icon-white"></i>--%>
                                                    <%--<small>Add Doc</small>--%>
                                                    <%--</sj:a>--%>
                                                    <%--</table>--%>
                                                    <%--</td>--%>

                                                <%--<td>--%>
                                                    <%--<table>--%>

                                                        <%--<button type="button" class="btn" onclick="window.location.href='<s:url action="addProject_project"/>' ">--%>
                                                            <%--<i class="icon-repeat"></i> Reset--%>
                                                        <%--</button>--%>

                                                    <%--</table>--%>
                                                <%--</td>--%>


                                                <td>
                                                    <table>

                                                        <button type="button" class="btn btn-default" onclick="window.location.href='<s:url action="initForm_project"/>' ">
                                                            <i class="icon-chevron-left icon-black"></i> Back
                                                        </button>

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                                    </div>

                                    <sj:dialog id="view_dialog_edit_alat" openTopics="showDialogEditAlat" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Edit Alat">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit_user" openTopics="showDialogEditUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Edit User">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_user" openTopics="showDialogDeleteUser" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Delete User">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_edit_task" openTopics="showDialogEditTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Edit a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_task" openTopics="showDialogDeleteTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Delete a Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_View_doc" openTopics="showDialogViewDoc" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="600" width="1000" autoOpen="false" title="View Document">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_add_member_task" openTopics="showDialogAddMemberTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="500" width="600" autoOpen="false" title="Add Task Team">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="40%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Alat :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfAlat" value="#session.listOfResultAlat" scope="request" />
                                                <display:table name="listOfAlat" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlEditAlat" namespace="/project" action="editAlatEditProject_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogEditAlat" href="%{urlEditAlat}">
                                                            <img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_print">
                                                        </sj:a>
                                                    </display:column>


                                                    <display:column property="idAlatDetail" sortable="true" title="<small>Id Alat Detail</small>" />
                                                    <%--<display:column property="idAlat" sortable="true" title="<small>Id Alat</small>" />--%>
                                                    <display:column property="namaAlatDetail" sortable="true" title="<small>Nama Alat</small>" />
                                                    <display:column property="namaApp" sortable="true" title="<small>Wilayah APP</small>" />
                                                    <display:column property="namaGi" sortable="true" title="<small>Start date</small>"/>
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table width="50%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Member :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <s:set name="listOfResultMember" value="#session.listOfResultMember" scope="request" />
                                                <display:table name="listOfResultMember" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">
                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                    <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                    <s:url var="urlEditUser" namespace="/project" action="initEditMember_project"
                                                    escapeAmp="false">
                                                    <s:param name="id"><s:property value="#attr.row.idProjectMember" /></s:param>
                                                    </s:url>
                                                    <sj:a onClickTopics="showDialogEditUser" href="%{urlEditUser}">
                                                    <img border="0" src="<s:url value="/pages/images/edit_member.png"/>" name="icon_print">
                                                    </sj:a>
                                                    </display:column>

                                                    <display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">
                                                    <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                    <s:url var="urlDeleteUser" namespace="/project" action="initDeleteMember_project"
                                                    escapeAmp="false">
                                                    <s:param name="id"><s:property value="#attr.row.idProjectMember" /></s:param>
                                                    </s:url>
                                                    <sj:a onClickTopics="showDialogDeleteUser" href="%{urlDeleteUser}">
                                                    <img border="0" src="<s:url value="/pages/images/delete_member.png"/>" name="icon_print">
                                                    </sj:a>
                                                    </display:column>
                                                    <display:column property="idProjectMember" sortable="false" title="<small>Project Member Id</small>"  />
                                                    <display:column property="idMember" sortable="false" title="<small>Member Id</small>"  />
                                                    <display:column property="namaMember" sortable="false" title="<small>Memmber Name</small>"  />
                                                    <display:column property="photoPath" sortable="false" title="<small>Photo</small>"  />
                                                    <display:column property="positionName" sortable="false" title="<small>Position</small>"  />
                                                    <display:column property="statusFlag" sortable="false" title="<small>Active / Non Active</small>"  />
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table width="50%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Document :
                                                </h5>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfResultDoc" value="#session.listOfResultDoc" scope="request" />
                                                <display:table name="listOfResultDoc" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">
                                                    <display:column property="lastUpdate" sortable="false" title="<small>Upload</small>"  />
                                                    <display:column property="uploadBy" sortable="false" title="<small>Upload By</small>"  />
                                                    <display:column property="statusDocument" sortable="false" title="<small>Project / Task</small>"  />
                                                    <display:column property="idDoc" sortable="false" title="<small>Document Id</small>"  />
                                                    <display:column property="idTask" sortable="false" title="<small>Task</small>"  />
                                                    <display:column property="namaDoc" sortable="false" title="<small>Document Name</small>"  />
                                                    <%--<display:column property="filePath" sortable="false" title="<small>Location</small>"  />--%>
                                                    <display:column media="html" title="<small>View</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlViewDoc" namespace="/project" action="viewDoc_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.namaDoc" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogViewDoc" href="%{urlViewDoc}">
                                                            <img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print" width="25px">
                                                        </sj:a>
                                                    </display:column>
                                                </display:table>

                                            </td>
                                        </tr>
                                    </table>

                                    <sj:dialog id="view_dialog_edit_issue" openTopics="showDialogEdiIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Edit Issue">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_delete_issue" openTopics="showDialogDeleteIssue" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="600" autoOpen="false" title="Delete Issue">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="50%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Member Change :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <s:set name="listOfTaskMemberChange" value="#session.listOfTaskMemberChange" scope="request" />
                                                <display:table name="listOfTaskMemberChange" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">
                                                    <display:column property="idTaskMemberChange" sortable="false" title="<small>Change Id</small>"  />
                                                    <display:column property="idMemberOld" sortable="false" title="<small>Member Lama</small>"  />
                                                    <display:column property="idMemberNew" sortable="false" title="<small>Member Baru</small>"  />
                                                    <display:column property="idTask" sortable="false" title="<small>Task</small>"  />
                                                    <display:column property="progres" sortable="false" title="<small>Progres</small>"  />
                                                    <display:column property="note" sortable="false" title="<small></small>"  />
                                                    <display:column property="lastUpdate" sortable="false" title="<small>Tanggal Pergantian</small>"  />
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>


                                    <table width="50%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Issue :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <s:set name="listOfResult" value="#session.listOfResultIssue" scope="request" />
                                                <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">

                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlEditIssue" namespace="/project" action="editIssueEditProject_project"
                                                               escapeAmp="false">
                                                            <%--<s:param name="id"><s:property value="#attr.row.idProjectMember" /></s:param>--%>
                                                            <s:param name="id"><s:property value="#attr.row.idIssue" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogEdiIssue" href="%{urlEditIssue}">
                                                            <img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_print">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlDeleteIssue" namespace="/project" action="deleteIssueEditProject_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idIssue" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogDeleteIssue" href="%{urlDeleteIssue}">
                                                            <img border="0" src="<s:url value="/pages/images/delete_task.png"/>" name="icon_print">
                                                        </sj:a>
                                                    </display:column>


                                                    <display:column property="idIssue" sortable="false" title="<small>Issue ID</small>"  />
                                                    <display:column property="namaIssue" sortable="false" title="<small>Issue Name</small>"  />
                                                    <display:column property="idMember" sortable="false" title="<small>From ID</small>"  />
                                                    <display:column property="namaMember" sortable="false" title="<small>From Name</small>"  />
                                                    <display:column property="photoPath" sortable="false" title="<small>Photo</small>"  />
                                                    <display:column property="statusFlag" sortable="false" title="<small>ACTIVE / NON ACTIVE</small>"  />
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                    <sj:dialog id="view_dialog_Task" openTopics="showDialogTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="1000" autoOpen="false" title="Edit Task">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <sj:dialog id="view_dialog_list_Task" openTopics="showDialogListTask" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="400" width="500" autoOpen="false" title="View List Team">
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="100%">
                                        <tr align="center">
                                            <td>
                                                <h5>
                                                    Task :
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <s:set name="listOfResult" value="#session.listOfResultTask" scope="request" />
                                                <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                                               requestURI="#" id="row" export="false" style="font-size:11" pagesize="100">
                                                    <display:column media="html" title="<small>View Log</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlLogTask" namespace="/project" action="initLogTask_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idTask" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogTask" href="%{urlLogTask}">
                                                            <img border="0" src="<s:url value="/pages/images/view_detail_project.png"/>" name="icon_print">
                                                        </sj:a>

                                                    </display:column>
                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlEditTask" namespace="/project" action="initEditTask_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idTask" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogEditTask" href="%{urlEditTask}">
                                                            <img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_print">
                                                        </sj:a>

                                                    </display:column>

                                                    <display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlDeleteTask" namespace="/project" action="initDeleteTask_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idTask" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogDeleteTask" href="%{urlDeleteTask}">
                                                            <img border="0" src="<s:url value="/pages/images/delete_task.png"/>" name="icon_print">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column property="statusFlag" sortable="false" title="<small>Active / Non Active</small>"  />
                                                    <display:column property="idTask" sortable="false" title="<small>Id Task</small>"  />
                                                    <display:column property="namaTask" sortable="false" title="<small>Task Name</small>"  />
                                                    <display:column property="namaStatus" sortable="false" title="<small>Status</small>"  />
                                                    <display:column property="asignTo" sortable="false" title="<small>Asign To</small>"  />
                                                    <display:column property="startDate" sortable="false" title="<small>Start Date</small>"  />
                                                    <display:column property="deadline" sortable="false" title="<small>Deadline</small>"  />

                                                    <display:column media="html" title="<small>Add Team</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlAddMemberTask" namespace="/project" action="initAddMemberTaskEditProject_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idTask" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogAddMemberTask" href="%{urlAddMemberTask}">
                                                            <img border="0" src="<s:url value="/pages/images/edit_member.png"/>" name="icon_print">
                                                        </sj:a>

                                                    </display:column>

                                                    <display:column media="html" title="<small>View Team</small>" style="text-align:center;font-size:9">
                                                        <%--<s:if test = "%{#attr.row.enabledConfirm}" >--%>
                                                        <s:url var="urlViewMemberTask" namespace="/project" action="viewListTaskMember_project"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idTask" /></s:param>
                                                            <s:param name="atr"><s:property value="'edit'" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogListTask" href="%{urlViewMemberTask}">
                                                            <img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print">
                                                        </sj:a>

                                                    </display:column>
                                                    <display:column property="namaPriority" sortable="false" title="<small>Priority</small>"  />
                                                    <display:column property="note" sortable="false" title="<small>Note</small>"/>
                                                    <display:column property="progresBar" sortable="false" title="<small>Progres</small>"/>
                                                    <%--<display:column property="progresName" sortable="false" title="<small>Progres</small>" />--%>

                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                </s:form>


                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>


