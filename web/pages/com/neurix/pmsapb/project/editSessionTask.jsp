<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>--%>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_edit_task').dialog('close');
        };

        function saveButtonAddLahan() {
            var namaTask = document.getElementById('namaTask').value;
            var idTask = document.getElementById('idTask').value;
//            var asignTo = document.getElementById('asignTo').value;
//            var status = document.getElementById('comboStatus').value;
            var priority = document.getElementById('comboPriority').value;
            var note = document.getElementById('note').value;
            var deadline = document.getElementById('taskDateForm').value;
            var startDate = document.getElementById('taskStartDateForm').value;

            if (namaTask!=''  && note!='' ) {

                dwr.engine.setAsync(false);
                ProjectAction.saveUpdateTask(idTask, namaTask, note, priority,deadline,startDate, function (response) {

                    if (response=='0') {
                        $('#view_dialog_add_task').dialog('close');


                        //reload popup add permohonan
                        document.addProjectForm.action='initAddProject_project.action';
                        document.addProjectForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (idTask == '') {
                    msg = 'Field <strong>Task Id</strong> is required.' + '<br/>';
                }
                if (namaTask == '') {
                    msg = 'Field <strong>Task Name</strong> is required.' + '<br/>';
                }

//                if (asignTo == '') {
//                    msg = msg + 'Field <strong>Asign To</strong> is required.' + '<br/>';
//                }

//                if (status == '') {
//                    msg = msg + 'Field <strong>Status</strong> is required.' + '<br/>';
//                }
                if (priority == '') {
                    msg = msg + 'Field <strong>Priority</strong> is required.' + '<br/>';
                }
                if (note == '') {
                    msg = msg + 'Field <strong>Note</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_add_task').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Task Name :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaTask" name="task.namaTask" cssClass="textForm"/>
                                <s:hidden id="idTask" name="task.idTask"/>
                            </table>
                        </td>
                    </tr>

                    <%--<tr>--%>
                        <%--<td>--%>
                            <%--<table>--%>
                                <%--<label class="control-label"><small>Asign To :</small></label>--%>
                            <%--</table>--%>
                        <%--</td>--%>
                        <%--<td>--%>
                            <%--<table>--%>
                                <%--<s:textfield id="asignTo" name="project.asignTo" cssClass="textForm"/>--%>
                                <%--<script>--%>
                                    <%--var functions, mapped;--%>
                                    <%--$('#asignTo').typeahead({--%>
                                        <%--minLength: 1,--%>
                                        <%--source: function (query, process) {--%>
                                            <%--functions = [];--%>
                                            <%--mapped = {};--%>

                                            <%--var data = [];--%>
                                            <%--dwr.engine.setAsync(false);--%>
                                            <%--ProjectAction.initComboUser(query, function (listdata) {--%>
                                                <%--data = listdata;--%>
                                            <%--});--%>

                                            <%--$.each(data, function (i, item) {--%>
                                                <%--var labelItem = item.username;--%>
                                                <%--mapped[labelItem] = { id: item.userId, label: labelItem };--%>
                                                <%--functions.push(labelItem);--%>
                                            <%--});--%>

                                            <%--process(functions);--%>
                                        <%--},--%>
                                        <%--updater: function (item) {--%>
                                            <%--var selectedObj = mapped[item];--%>
<%--//                                          var namaMember = selectedObj.label;--%>
<%--//                                          document.getElementById("idMember").value = namaMember;--%>
                                            <%--return selectedObj.id;--%>
                                        <%--}--%>
                                    <%--});--%>
                                <%--</script>--%>
                            <%--</table>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                    <%--<tr>--%>
                        <%--<td>--%>
                            <%--<table>--%>
                                <%--<label class="control-label"><small>Asign To :</small></label>--%>
                            <%--</table>--%>
                        <%--</td>--%>
                        <%--<td>--%>
                            <%--<table>--%>
                                <%--<s:action id="comboAsignTo" namespace="/project" name="initComboAsignTo_project"/>--%>
                                <%--<s:select list="#comboAsignTo.listOfComboAsignTo" id="comboAsignTo" name="project.asignTo" required="true"--%>
                                          <%--listKey="idMember" listValue="namaMember" headerKey="" headerValue="[Select one]"--%>
                                          <%--cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>

                            <%--</table>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                    <%--<tr>--%>
                        <%--<td>--%>
                            <%--<table>--%>
                                <%--<label class="control-label"><small>Status :</small></label>--%>
                            <%--</table>--%>
                        <%--</td>--%>
                        <%--<td>--%>
                                <%--<table>--%>
                                    <%--<s:action id="comboStatus" namespace="/project" name="initComboStatus_project"/>--%>
                                    <%--<s:select list="#comboStatus.listOfComboStatus" id="comboStatus" name="project.taskStatus" required="true"--%>
                                              <%--listKey="idStatus" listValue="namaStatus" headerKey="" headerValue="[Select one]"--%>
                                              <%--cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>

                                <%--</table>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Priority :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:action id="comboPriority" namespace="/project" name="initComboPriority_project"/>
                                <s:select list="#comboPriority.listOfComboPriority" id="comboPriority" name="task.priority" required="true"
                                          listKey="idPriority" listValue="namaPriority" headerKey="" headerValue="[Select one]"
                                          cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label" for="task.stStartDate">Start Date :</label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <sj:datepicker id="taskStartDateForm" name="task.stStartDate" displayFormat="dd-mm-yy"
                                               showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label" for="task.stDeadline">Deadline :</label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <sj:datepicker id="taskDateForm" name="task.stDeadline" displayFormat="dd-mm-yy"
                                               showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Note :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textarea id="note" name="task.note" disabled="false" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                       buttons="{
                                                                'OK':function() { okFailureButtonAddLahan(); }
                                                                }"
                                            >
                                                <img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                Found failure when saving, please try again or call your admin.
                                            </sj:dialog>

                                            <sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                       buttons="{
                                                                    'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }
                                                                }"
                                            >
                                                <div class="alert alert-error fade in">
                                                    <label class="control-label" align="left">
                                                        <img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                        <br/>
                                                        <center><div id="errorValidationMessageAddLahan"></div></center>
                                                    </label>
                                                </div>
                                            </sj:dialog>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAddLahan();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

