<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>--%>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_delete_user').dialog('close');
        };

        function saveButtonAddLahan() {
            var idProjectMember = document.getElementById('idProjectMember').value;
            var idMember = document.getElementById('idMember').value;
            var namaMember = document.getElementById('namaMember').value;
            var memberPosition = document.getElementById('memberPosition').value;
            var idProject = document.getElementById('idProject').value;
            var idTask = document.getElementById('idTask').value;

            if (idProjectMember !=''&& idTask == '' ) {

                dwr.engine.setAsync(false);
                ProjectAction.SaveDeleteMemberEditProject( idProjectMember,  idMember,  namaMember,  memberPosition, idProject, function (response) {

                    if (response=='0') {
                        $('#view_dialog_delete_user').dialog('close');


                        //reload popup add permohonan
                        document.addProjectForm.action='initEditProject_project.action';
                        document.addProjectForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (idMember == '') {
                    msg = 'Field <strong>Member Id</strong> is required.' + '<br/>';
                }

                if (namaMember == '') {
                    msg = msg + 'Field <strong>Member Name</strong> is required.' + '<br/>';
                }
                if (idTask != '') {
                    msg = msg + '<strong>Hapus dulu Task yang di Asign ke Member</strong>' + '<br/>';
                }

                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_delete_user').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <label class="control-label"><small>Member Id :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="idMember" name="member.idMember" cssClass="textForm" disabled="true"/>
                                <s:hidden id="idProjectMember" name="member.idProjectMember"/>
                                <s:hidden id="idMember" name="member.idMember"/>
                                <script>
                                    var functions, mapped;
                                    $('#idMember').typeahead({
                                        minLength: 1,
                                        source: function (query, process) {
                                            functions = [];
                                            mapped = {};

                                            var data = [];
                                            dwr.engine.setAsync(false);
                                            ProjectAction.initComboUser(query, function (listdata) {
                                                data = listdata;
                                            });

                                            $.each(data, function (i, item) {
                                                var labelItem = item.username;
                                                mapped[labelItem] = { id: item.userId, label: labelItem };
                                                functions.push(labelItem);
                                            });

                                            process(functions);
                                        },
                                        updater: function (item) {
                                            var selectedObj = mapped[item];
                                            var namaMember = selectedObj.label;
                                            document.getElementById("namaMember").value = namaMember;

                                            return selectedObj.id;
                                        }
                                    });
                                    //
                                    //
                                </script>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Member Name :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaMember" name="member.namaMember" cssClass="textForm" disabled="true"/>
                                <s:hidden id="namaMember" name="member.namaMember"/>
                                <s:hidden id="idProject" name="member.idProject"/>
                                <s:hidden id="idTask" name="member.idTask"/>

                            </table>
                        </td>
                    </tr>
                    <%--<tr>--%>
                        <%--<td>--%>
                            <%--<label class="control-label"><small>Id Task :</small></label>--%>
                        <%--</td>--%>
                        <%--<td>--%>
                            <%--<table>--%>
                                <%--<s:textfield id="idTask" name="member.idTask" cssClass="textForm" disabled="true"/>--%>
                                <%--<s:hidden id="idTask" name="member.idTask"/>--%>
                            <%--</table>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                    <tr>
                        <td>
                            <label class="control-label"><small>Position As :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="memberPosition" name="member.stPositionId" cssClass="textForm" disabled="true"/>
                                <s:hidden id="memberPosition" name="member.stPositionId"/>
                                <%--<s:select list="#{'4':'STAFF','5':'PIC'}" id="memberPosition" name="project.memberPosition"--%>
                                <%--headerKey="" headerValue="[Select one]" cssClass="textForm" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                       buttons="{
                                                                'OK':function() { okFailureButtonAddLahan(); }
                                                                }"
                                            >
                                                <img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                Found failure when saving, please try again or call your admin.
                                            </sj:dialog>

                                            <sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                       buttons="{
                                                                    'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }
                                                                }"
                                            >
                                                <div class="alert alert-error fade in">
                                                    <label class="control-label" align="left">
                                                        <img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                        <br/>
                                                        <center><div id="errorValidationMessageAddLahan"></div></center>
                                                    </label>
                                                </div>
                                            </sj:dialog>

                                            <button type="button" id="saveabtn" class="btn btn-warning" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAddLahan();">
                                                <i class="icon-ok-circle icon-white"/> Delete
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

