<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>--%>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_delete_task').dialog('close');
        };

        function saveButtonAddLahan() {
            var namaTask = document.getElementById('namaTask').value;
            var idTask = document.getElementById('idTask').value;
//            var asignTo = document.getElementById('asignTo').value;
//            var status = document.getElementById('comboStatus').value;
            var priority = document.getElementById('comboPriority').value;
            var note = document.getElementById('note').value;
            var deadline = document.getElementById('taskDateForm').value;
            var startDate = document.getElementById('taskStartDateForm').value;
            var status = document.getElementById('status').value;
            var asignto = document.getElementById('comboMemberTask').value;
            var progres = document.getElementById('taskProgres').value;

            if (namaTask!=''  && note!='' ) {

                dwr.engine.setAsync(false);
                ProjectAction.saveDeleteTaskEditProject( idTask,  namaTask,  note,  priority,  deadline,  startDate,  status,  asignto, progres, function (response) {

                    if (response=='0') {
                        $('#view_dialog_delete_task').dialog('close');


                        //reload popup add permohonan
                        document.addProjectForm.action='initEditProject_project.action';
                        document.addProjectForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (idTask == '') {
                    msg = 'Field <strong>Task Id</strong> is required.' + '<br/>';
                }
                if (namaTask == '') {
                    msg = 'Field <strong>Task Name</strong> is required.' + '<br/>';
                }

//                if (asignTo == '') {
//                    msg = msg + 'Field <strong>Asign To</strong> is required.' + '<br/>';
//                }

//                if (status == '') {
//                    msg = msg + 'Field <strong>Status</strong> is required.' + '<br/>';
//                }
                if (priority == '') {
                    msg = msg + 'Field <strong>Priority</strong> is required.' + '<br/>';
                }
                if (note == '') {
                    msg = msg + 'Field <strong>Note</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_delete_task').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Task Name :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaTask" name="task.namaTask" cssClass="textForm" disabled="true"/>
                                <s:hidden id="namaTask" name="task.namaTask"/>
                                <s:hidden id="idTask" name="task.idTask"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Asign To :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="comboMemberTask" name="task.asignTo" cssClass="textForm" disabled="true"/>
                                <s:hidden id="comboMemberTask" name="task.asignTo"/>

                                <%--<s:action id="comboMemberTask" namespace="/project" name="initComboMemberTask_project"/>--%>
                                <%--<s:select list="#comboMemberTask.listOfComboMemberTask" id="comboMemberTask" name="task.asignTo" required="true"--%>
                                          <%--listKey="idMember" listValue="namaMember" headerKey="" headerValue="[Select one]"--%>
                                          <%--cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Priority :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="comboPriority" name="task.priority" cssClass="textForm" disabled="true"/>
                                <s:hidden id="comboPriority" name="task.priority"/>
                                <%--<s:action id="comboPriority" namespace="/project" name="initComboPriority_project"/>--%>
                                <%--<s:select list="#comboPriority.listOfComboPriority" id="comboPriority" name="task.priority" required="true"--%>
                                          <%--listKey="idPriority" listValue="namaPriority" headerKey="" headerValue="[Select one]"--%>
                                          <%--cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Status :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="status" name="task.statusTask" cssClass="textForm" disabled="true"/>
                                <s:hidden id="status" name="task.statusTask"/>
                                <%--<s:action id="status" namespace="/project" name="initComboStatus_project"/>--%>
                                <%--<s:select list="#status.listOfComboStatus" id="status" name="task.statusTask" required="true"--%>
                                          <%--listKey="idStatus" listValue="namaStatus" headerKey="" headerValue="[Select one]"--%>
                                          <%--cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label" for="task.startDate">Start Date :</label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <%--<s:textfield id="taskStartDateForm" name="task.startDate" cssClass="textForm" disabled="true"/>--%>
                                <s:hidden id="taskStartDateForm" name="task.startDate"/>
                                <sj:datepicker id="taskStartDateForm" name="task.startDate" displayFormat="dd-mm-yy" disabled="true"
                                               showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label" for="task.deadline">Deadline :</label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <%--<s:textfield id="taskDateForm" name="task.deadline" cssClass="textForm" disabled="true"/>--%>
                                <s:hidden id="taskDateForm" name="task.deadline"/>
                                <sj:datepicker id="taskDateForm" name="task.deadline" displayFormat="dd-mm-yy" disabled="true"
                                               showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Progres :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:action id="comboProgres" namespace="/project" name="initComboProgres_project"/>
                                <s:hidden id="taskProgres" name="task.progres"/>
                                <s:select list="#comboProgres.listOfComboProgres" id="comboProgres" name="task.progres" required="true"
                                          listKey="idProgres" listValue="namProgres" headerKey="" headerValue="[Select one]" disabled="true"
                                          cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Note :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textarea id="note" name="task.note" disabled="true" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                <s:hidden id="note" name="task.note"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                       buttons="{
                                                                'OK':function() { okFailureButtonAddLahan(); }
                                                                }"
                                            >
                                                <img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                Found failure when saving, please try again or call your admin.
                                            </sj:dialog>

                                            <sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                       buttons="{
                                                                    'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }
                                                                }"
                                            >
                                                <div class="alert alert-error fade in">
                                                    <label class="control-label" align="left">
                                                        <img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                        <br/>
                                                        <center><div id="errorValidationMessageAddLahan"></div></center>
                                                    </label>
                                                </div>
                                            </sj:dialog>

                                            <button type="button" id="saveabtn" class="btn btn-warning" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAddLahan();">
                                                <i class="icon-ok-circle icon-white"/> delete
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

