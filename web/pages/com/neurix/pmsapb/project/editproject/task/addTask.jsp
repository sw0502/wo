<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>--%>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_add_task').dialog('close');
        };

        function saveButtonAddLahan() {
            var namaTask = document.getElementById('namaTask').value;
            var priority = document.getElementById('comboPriority').value;
            var note = document.getElementById('note').value;
            var deadline = document.getElementById('taskDateForm').value;
            var startDate = document.getElementById('taskStartDateForm').value;
            var asignTo = document.getElementById('comboMemberTask').value;
            var projectStartDate = document.getElementById('projectStartDateForm').value;
            var taskStartDateForm = document.getElementById('taskStartDateForm').value;
            var projectDeadline = document.getElementById('projectDateForm').value;
            var taskDeadline = document.getElementById('taskDateForm').value;

            if (namaTask!=''  && note!='' && projectStartDate <= taskStartDateForm && projectDeadline >= taskDeadline) {

                dwr.engine.setAsync(false);
                ProjectAction.saveAddTaskEditProject(namaTask, note, priority,deadline,startDate,asignTo, function (response) {

                    if (response=='0') {
                        $('#view_dialog_add_task').dialog('close');


                        //reload popup add permohonan
                        document.addProjectForm.action='initEditProject_project.action';
                        document.addProjectForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (namaTask == '') {
                    msg = 'Field <strong>Task Name</strong> is required.' + '<br/>';
                }

//                if (asignTo == '') {
//                    msg = msg + 'Field <strong>Asign To</strong> is required.' + '<br/>';
//                }

//                if (status == '') {
//                    msg = msg + 'Field <strong>Status</strong> is required.' + '<br/>';
//                }
                if (taskStartDateForm <= projectStartDate){
                    msg = msg + '<strong>Start Date harus setelah Project start date</strong>' + '<br/>';
                }
                if (taskDeadline >= projectDeadline){
                    msg = msg + '<strong>Task Deadline harus sebelum Project Deadline</strong>' + '<br/>';
                }

//                if (taskDeadline > taskStartDateForm){
//                    msg = msg + '<strong>Task Start Date tidak boleh melebihi Deadline</strong>' + '<br/>';
//                }


                if (priority == '') {
                    msg = msg + 'Field <strong>Priority</strong> is required.' + '<br/>';
                }
                if (note == '') {
                    msg = msg + 'Field <strong>Note</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_add_task').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Task Name :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaTask" name="project.namaTask" cssClass="textForm"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Asign To :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:action id="comboMemberTask" namespace="/project" name="initComboMemberTask_project"/>
                                <s:select list="#comboMemberTask.listOfComboMemberTask" id="comboMemberTask" name="project.asignTo" required="true"
                                          listKey="idMember" listValue="namaMember" headerKey="" headerValue="[Select one]"
                                          cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Priority :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:action id="comboPriority" namespace="/project" name="initComboPriority_project"/>
                                <s:select list="#comboPriority.listOfComboPriority" id="comboPriority" name="project.priority" required="true"
                                          listKey="idPriority" listValue="namaPriority" headerKey="" headerValue="[Select one]"
                                          cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label" for="project.taskStartDate">Start Date :</label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <sj:datepicker id="taskStartDateForm" name="project.taskStartDate" displayFormat="dd-mm-yy"
                                               showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label" for="project.taskDeadline">Deadline :</label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <sj:datepicker id="taskDateForm" name="project.taskDeadline" displayFormat="dd-mm-yy"
                                               showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Note :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textarea id="note" name="project.taskNote" disabled="false" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                       buttons="{
                                                                'OK':function() { okFailureButtonAddLahan(); }
                                                                }"
                                            >
                                                <img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                Found failure when saving, please try again or call your admin.
                                            </sj:dialog>

                                            <sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                       buttons="{
                                                                    'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }
                                                                }"
                                            >
                                                <div class="alert alert-error fade in">
                                                    <label class="control-label" align="left">
                                                        <img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                        <br/>
                                                        <center><div id="errorValidationMessageAddLahan"></div></center>
                                                    </label>
                                                </div>
                                            </sj:dialog>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAddLahan();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

