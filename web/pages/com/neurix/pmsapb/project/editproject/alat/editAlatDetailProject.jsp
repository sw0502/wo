<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>--%>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_edit_alat').dialog('close');
        };

        function saveButtonAddLahan() {
            var idAlatDetail = document.getElementById('idalat').value;
            var namaAlat = document.getElementById('namaMember').value;
            var namaApp = document.getElementById('namaApp').value;
            var namaGi = document.getElementById('namaGi').value;

//            alert(memberPosition);
            if (idAlatDetail!='' && namaAlat!='') {

                dwr.engine.setAsync(false);
                ProjectAction.saveEditAlatEditProject(idAlatDetail, namaAlat, namaApp,namaGi, function (response) {

                    if (response=='0') {
                        $('#view_dialog_edit_alat').dialog('close');


                        //reload popup add permohonan
                        document.addProjectForm.action='initEditProject_project.action';
                        document.addProjectForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (idAlatDetail == '') {
                    msg = 'Field <strong>Alat Detail Id</strong> is required.' + '<br/>';
                }


                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_edit_alat').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <label class="control-label"><small>Alat Detail Id :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="idalat" name="alatDetail.idAlatDetail" cssClass="textForm"/>
                                <script>
                                    var functions, mapped;
                                    $('#idalat').typeahead({
                                        minLength: 1,
                                        source: function (query, process) {
                                            functions = [];
                                            mapped = {};

                                            var data = [];
                                            dwr.engine.setAsync(false);
                                            ProjectAction.initComboAlat(query, function (listdata) {
                                                data = listdata;
                                            });

                                            $.each(data, function (i, item) {
                                                var labelItem = item.idAlatDetail +" - "+ item.namaAlatDetail+" - "+item.namaApp+" - "+item.namaGi;
                                                var nama = item.namaAlatDetail;
                                                var kodeApp = item.kodeApp;
                                                var namaApp = item.namaApp;
                                                var kodeAlat = item.kodeAlat;
                                                var namaGi = item.namaGi;
//                                                var roleid = item.role;
//                                                var namaRole = item.namaRole;
                                                mapped[labelItem] = { id: item.idAlatDetail, label: nama, kp: kodeApp, np: namaApp, ka: kodeAlat, ng: namaGi};
                                                functions.push(labelItem);
                                            });

                                            process(functions);
                                        },
                                        updater: function (item) {
                                            var selectedObj = mapped[item];
                                            var namaAlatDetail = selectedObj.label;
                                            var itemKp = selectedObj.kp;
                                            var itemNp = selectedObj.np;
                                            var itemKa = selectedObj.ka;
                                            var itemNg = selectedObj.ng;

//                                            var roleMember = selectedObj.role;
//                                            var roleName = selectedObj.namarole;
                                            document.getElementById("namaMember").value = namaAlatDetail;
                                            document.getElementById("namaApp").value = itemNp;
                                            document.getElementById("namaGi").value = itemNg;
//                                            document.getElementById("kodeAlat").value = itemKa;
//                                            document.getElementById("memberRole").value =  roleMember;
//                                            document.getElementById("nameMemberRole").value = roleName;
                                            return selectedObj.id;
                                        }
                                    });
                                    //
                                    //
                                </script>
                            </table>
                        </td>
                    </tr>

                    <%--<tr>--%>
                    <%--<td>--%>
                    <%--<label class="control-label"><small>Member Role :</small></label>--%>
                    <%--</td>--%>
                    <%--<td>--%>
                    <%--<table>--%>
                    <%--<s:textfield id="memberRole" name="project.role" cssClass="textForm" disabled="true"/>--%>
                    <%--</table>--%>
                    <%--</td>--%>
                    <%--</tr>--%>

                    <%--<tr>--%>
                    <%--<td>--%>
                    <%--<label class="control-label"><small>Member Role Name:</small></label>--%>
                    <%--</td>--%>
                    <%--<td>--%>
                    <%--<table>--%>
                    <%--<s:textfield id="nameMemberRole" name="project.namaRole" cssClass="textForm" disabled="true"/>--%>
                    <%--</table>--%>
                    <%--</td>--%>
                    <%--</tr>--%>

                    <tr>
                        <td>
                            <label class="control-label"><small>Nama Alat :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaMember" name="alatDetail.namaAlatDetail" cssClass="textForm" disabled="true"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>Wilayah APP :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaApp" name="alatDetail.namaApp" cssClass="textForm" disabled="true"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label class="control-label"><small>GI :</small></label>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaGi" name="alatDetail.namaGi" cssClass="textForm" disabled="true"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                       buttons="{
                                                                'OK':function() { okFailureButtonAddLahan(); }
                                                                }"
                                            >
                                                <img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                Found failure when saving, please try again or call your admin.
                                            </sj:dialog>

                                            <sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                       buttons="{
                                                                    'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }
                                                                }"
                                            >
                                                <div class="alert alert-error fade in">
                                                    <label class="control-label" align="left">
                                                        <img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                        <br/>
                                                        <center><div id="errorValidationMessageAddLahan"></div></center>
                                                    </label>
                                                </div>
                                            </sj:dialog>

                                            <button type="button" id="saveabtn" class="btn btn-primary" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAddLahan();">
                                                <i class="icon-ok-circle icon-white"/> Save
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

