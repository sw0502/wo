<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        function closeActivation(){
            $('#view_dialog_ViewProject').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<sj:dialog id="view_dialog_alat_detail" openTopics="showDialogViewDocAlat" modal="true" resizable="false" cssStyle="text-align:left;"
           position="center" height="700" width="600" autoOpen="false" title="View Document">
    <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
</sj:dialog>
<sj:dialog id="view_dialog_View_doc" openTopics="showDialogViewDoc" modal="true" resizable="false" cssStyle="text-align:left;"
           position="center" height="600" width="1000" autoOpen="false" title="View Document">
    <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
</sj:dialog>

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="aktivasiSKArealForm" method="post" namespace="/project" action="" enctype="multipart/form-data"
                    cssClass="well form-horizontal">

                <fieldset>
                    <legend align="left">Detail Project</legend>

                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table>
                        <tr>
                            <td>
                                <label class="control-label"><small>Type Project : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="project.namaTypeProject"/>
                                        <%--<s:hidden id="status" name="SurveyEval.status"/>--%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label"><small>No. WO : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="project.noWo"/>
                                        <%--<s:hidden id="status" name="SurveyEval.status"/>--%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label"><small>No. Project : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="project.idProject"/>
                                    <s:hidden id="idProject" name="project.idProject"/>
                                        <%--<s:hidden id="status" name="SurveyEval.status"/>--%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label"><small>Nama Project : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="project.namaProject"/>
                                </div>
                            </td>
                        </tr>
                        <%--<tr>--%>
                            <%--<td>--%>
                                <%--<label class="control-label"><small>PIC : </small></label>--%>
                            <%--</td>--%>
                            <%--<td>--%>
                                <%--<div class="label">--%>
                                    <%--<s:property value="project.managerProject"/> - <s:property value="project.namaProjectManager"/>--%>
                                <%--</div>--%>
                            <%--</td>--%>
                        <%--</tr>--%>
                        <tr>
                            <td>
                                <label class="control-label"><small>Start Date : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="project.startDate"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label"><small>Deadline : </small></label>
                            </td>
                            <td>
                                <div class="label">
                                    <s:property value="project.deadline"/>
                                </div>
                            </td>
                        </tr>
                </fieldset>




                <div id="actions" class="form-actions">
                </div>

                <table width="40%">
                    <tr align="center">
                        <td>
                            <h5>
                                Alat :
                            </h5>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">

                            <s:set name="listOfAlat" value="#session.listOfResultAlat" scope="request" />
                            <display:table name="listOfAlat" class="table table-condensed table-striped table-hover"
                                           requestURI="paging_displaytag_project.action" export="false" id="row" pagesize="50" style="font-size:10">

                                <%--<display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">--%>
                                    <%--<s:url var="urlEditAlat" namespace="/project" action="editAlatEditProject_project"--%>
                                           <%--escapeAmp="false">--%>
                                        <%--<s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>--%>
                                    <%--</s:url>--%>
                                    <%--<sj:a onClickTopics="showDialogEditAlat" href="%{urlEditAlat}">--%>
                                        <%--<img border="0" src="<s:url value="/pages/images/edit_task.png"/>" name="icon_print">--%>
                                    <%--</sj:a>--%>
                                <%--</display:column>--%>


                                <display:column property="idAlatDetail" sortable="true" title="<small>Id Alat Detail</small>" />
                                <%--<display:column property="idAlat" sortable="true" title="<small>Id Alat</small>" />--%>
                                <display:column property="namaAlatDetail" sortable="true" title="<small>Nama Alat</small>" />
                                <display:column property="namaApp" sortable="true" title="<small>Wilayah APP</small>" />
                                <display:column property="namaGi" sortable="true" title="<small>Start date</small>"/>
                                <%--<display:column property="urlFoto" sortable="true" title="<small>url foto</small>"/>--%>
                                <display:column media="html" title="<small>View</small>" style="text-align:center;font-size:9">
                                    <s:url var="urlViewDoc" namespace="/project" action="viewAlatDetail_project"
                                           escapeAmp="false">
                                        <s:param name="id"><s:property value="#attr.row.idAlatDetail" /></s:param>
                                    </s:url>
                                    <sj:a onClickTopics="showDialogViewDocAlat" href="%{urlViewDoc}">
                                        <img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print" width="25px">
                                    </sj:a>
                                </display:column>
                            </display:table>
                        </td>
                    </tr>
                </table>

                <table width="60%">
                    <tr align="center">
                        <td>
                            <h5>
                                Document :
                            </h5>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">

                            <s:set name="listOfResultDoc" value="#session.listOfResultDoc" scope="request" />
                            <display:table name="listOfResultDoc" class="table table-condensed table-striped table-hover"
                                           requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">
                                <display:column property="lastUpdate" sortable="false" title="<small>Upload</small>"  />
                                <display:column property="uploadBy" sortable="false" title="<small>Upload By</small>"  />
                                <display:column property="statusDocument" sortable="false" title="<small>Project / Task</small>"  />
                                <display:column property="idDoc" sortable="false" title="<small>Document Id</small>"  />
                                <display:column property="idTask" sortable="false" title="<small>Task</small>"  />
                                <display:column property="namaDoc" sortable="false" title="<small>Document Name</small>"  />
                                <%--<display:column property="filePath" sortable="false" title="<small>Location</small>"  />--%>

                                <display:column media="html" title="<small>View</small>" style="text-align:center;font-size:9">
                                    <s:if test = "%{#attr.row.enabledView}" >
                                    <s:url var="urlViewDoc" namespace="/project" action="viewDoc_project"
                                           escapeAmp="false">
                                        <s:param name="id"><s:property value="#attr.row.namaDoc" /></s:param>
                                    </s:url>
                                    <sj:a onClickTopics="showDialogViewDoc" href="%{urlViewDoc}">
                                        <img border="0" src="<s:url value="/pages/images/view.png"/>" name="icon_print" width="25px">
                                    </sj:a>
                                    </s:if>
                                    <s:else>
                                        <img border="0" src="<s:url value="/pages/images/icon_not_edit.png"/>" name="icon_not_edit">
                                    </s:else>
                                </display:column>
                                <display:column property="downloadPath" sortable="false" title="<small>Downlaod File</small>"  />
                            </display:table>

                        </td>
                        </tr>
                    </table>

                <table width="30%">
                    <tr align="center">
                        <td>
                            <h5>
                                Member :
                            </h5>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">

                            <s:set name="listOfResult" value="#session.listOfResultMember" scope="request" />
                            <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                           requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">
                                <display:column property="idMember" sortable="false" title="<small>Member Id</small>"  />
                                <display:column property="photoPath" sortable="false" title="<small>Photo</small>"  />
                                <display:column property="namaMember" sortable="false" title="<small>Memmber Name</small>"  />
                                <display:column property="positionName" sortable="false" title="<small>Position</small>"  />
                            </display:table>

                        </td>
                    </tr>
                    </table>

                <table width="50%">
                    <tr align="center">
                        <td>
                            <h5>
                                Member Change :
                            </h5>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <s:set name="listOfTaskMemberChange" value="#session.listOfTaskMemberChange" scope="request" />
                            <display:table name="listOfTaskMemberChange" class="table table-condensed table-striped table-hover"
                                           requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">
                                <display:column property="idTaskMemberChange" sortable="false" title="<small>Change Id</small>"  />
                                <display:column property="idMemberOld" sortable="false" title="<small>Member Lama</small>"  />
                                <display:column property="idMemberNew" sortable="false" title="<small>Member Baru</small>"  />
                                <display:column property="idTask" sortable="false" title="<small>Task</small>"  />
                                <display:column property="progres" sortable="false" title="<small>Progres</small>"  />
                                <display:column property="note" sortable="false" title="<small></small>"  />
                                <display:column property="lastUpdate" sortable="false" title="<small>Tanggal Pergantian</small>"  />
                            </display:table>
                        </td>
                    </tr>
                </table>

                    <table width="80%">
                        <tr align="center">
                            <td>
                                <h5>
                                    Task :
                                </h5>
                            </td>
                        </tr>
                    <tr>
                        <td align="center">

                            <s:set name="listOfResult" value="#session.listOfResultTask" scope="request" />
                            <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                           requestURI="#" id="row" export="false" style="font-size:11" pagesize="100">
                                <display:column property="idTask" sortable="false" title="<small>Id Task</small>"  />
                                <display:column property="namaTask" sortable="false" title="<small>Task Name</small>"  />
                                <display:column property="namaStatus" sortable="false" title="<small>Status</small>"  />
                                <display:column property="asignTo" sortable="false" title="<small>Asign To</small>"  />
                                <display:column property="startDate" sortable="false" title="<small>Start Date</small>"  />
                                <display:column property="deadline" sortable="false" title="<small>Deadline</small>"  />
                                <display:column property="namaPriority" sortable="false" title="<small>Priority</small>"  />
                                <display:column property="note" sortable="false" title="<small>Note</small>"/>
                                <display:column property="progresBar" sortable="false" title="<small>Progres</small>"/>
                                <%--<display:column property="progresName" sortable="false" title="<small>Progres</small>" />--%>

                            </display:table>

                        </td>
                    </tr>
                </table>
                <table width="30%">
                    <tr align="center">
                        <td>
                            <h5>
                                Issue :
                            </h5>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">

                            <s:set name="listOfResult" value="#session.listOfResultIssue" scope="request" />
                            <display:table name="listOfResult" class="table table-condensed table-striped table-hover"
                                           requestURI="#" id="row" export="false" style="font-size:11" pagesize="60">
                                <display:column property="idIssue" sortable="false" title="<small>Issue Id</small>"  />
                                <display:column property="namaMember" sortable="false" title="<small>From</small>"  />
                                <display:column property="photoPath" sortable="false" title="<small>Photo</small>"  />
                                <display:column property="namaIssue" sortable="false" title="<small>Issue Name</small>"  />
                            </display:table>

                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <table>
                                    <%--<s:url var="urlBack" namespace="/permohonanlahan" action="initForm_permohonanlahan"/>--%>
                                    <%--<sj:submit type="button" cssClass="btn" href="%{urlBack}" id="back" name="back">--%>
                                    <%--<i class="icon-remove-circle"></i> Back--%>
                                    <%--</sj:submit>--%>

                                    <%--<button type="button" class="btn" onclick="window.location.href='<s:url action="initEditMajor_skareal"/>'">--%>
                                    <%--<i class="icon-repeat"></i> <small> Back</small>--%>
                                    <%--</button>--%>

                                <button type="button" class="btn" onclick="closeActivation();">
                                    <i class="icon-repeat"></i> <small> Close </small>
                                </button>

                            </table>
                        </td>
                    </tr>
                </table>

            </s:form>
            
            <table>
                <%--<tr>--%>
                    <%--<td>--%>
                        <%--<img src="pages/images/doc/irvean_images.jpg">--%>
                    <%--</td>--%>
                <%--</tr>--%>
            </table>

        </td>
    </tr>
</table>

</body>
</html>


