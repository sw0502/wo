<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <%--<script type='text/javascript' src='<s:url value="/dwr/interface/ProjectAction.js"/>'></script>--%>
    <script type="text/javascript">

        function closeBtnAddLahan() {
            $('#view_dialog_delete_task').dialog('close');
        };

        function saveButtonAddLahan() {
            var idTask = document.getElementById('idTask').value;

            if (idTask!='') {

                dwr.engine.setAsync(false);
                ProjectAction.saveDeleteTask(idTask, function (response) {

                    if (response=='0') {
                        $('#view_dialog_delete_task').dialog('close');


                        //reload popup add permohonan
                        document.addProjectForm.action='initAddProject_project.action';
                        document.addProjectForm.submit();

                    } else {
                        $('#info_dialog_add_lahan').dialog('open');
                    }

                });

            } else {

                var msg = "";
                if (idTask == '') {
                    msg = 'Field <strong>Task Id</strong> is required.' + '<br/>';
                }

                document.getElementById('errorValidationMessageAddLahan').innerHTML = msg;

                $.publish('showErrorValidationDialogAddLahan');

            }


        };

        function okFailureButtonAddLahan() {
            $('#info_dialog_add_lahan').dialog('close');
            $('#view_dialog_delete_task').dialog('close');
        };


    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <div id="actions" class="form-actions">
                <table >
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Task Name :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textfield id="namaTask" name="task.namaTask" cssClass="textForm" disabled="true"/>
                                <s:hidden id="idTask" name="task.idTask"/>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Priority :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:action id="comboPriority" namespace="/project" name="initComboPriority_project"/>
                                <s:select list="#comboPriority.listOfComboPriority" id="comboPriority" name="task.priority" required="true"
                                          listKey="idPriority" listValue="namaPriority" headerKey="" headerValue="[Select one]" disabled="true"
                                          cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label" for="task.stStartDate">Start Date :</label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <sj:datepicker id="taskStartDateForm" name="task.stStartDate" displayFormat="dd-mm-yy"
                                               showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small"  disabled="true"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label" for="task.stDeadline">Deadline :</label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <sj:datepicker id="taskDateForm" name="task.stDeadline" displayFormat="dd-mm-yy"
                                               disabled="true" showAnim="fadeIn" showOptions="{direction: 'up' }" duration="slow" changeMonth="true" changeYear="true" cssClass="input-small" />
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <label class="control-label"><small>Note :</small></label>
                            </table>
                        </td>
                        <td>
                            <table>
                                <s:textarea id="note" name="task.note" disabled="true"  cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div id="crud">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="control-label"></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">

                                            <sj:dialog id="info_dialog_add_lahan" openTopics="showInfoDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                       buttons="{
                                                                'OK':function() { okFailureButtonAddLahan(); }
                                                                }"
                                            >
                                                <img id="iconinfo" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error">
                                                Found failure when saving, please try again or call your admin.
                                            </sj:dialog>

                                            <sj:dialog id="error_validation_dialog_add_lahan" openTopics="showErrorValidationDialogAddLahan" modal="true" resizable="false"
                                                       position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                       buttons="{
                                                                    'OK':function() { $('#error_validation_dialog_add_lahan').dialog('close'); }
                                                                }"
                                            >
                                                <div class="alert alert-error fade in">
                                                    <label class="control-label" align="left">
                                                        <img id="iconerror" border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                        <br/>
                                                        <center><div id="errorValidationMessageAddLahan"></div></center>
                                                    </label>
                                                </div>
                                            </sj:dialog>

                                            <button type="button" id="saveabtn" class="btn btn-warning" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="saveButtonAddLahan();">
                                                <i class="icon-ok-circle icon-white"/> Delete
                                            </button>

                                        </td>
                                        <td>

                                            <button type="button" id="cancelbtn" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="closeBtnAddLahan();">
                                                <i class="icon-remove-circle"/> Cancel
                                            </button>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>

        </td>
    </tr>
</table>

</body>
</html>

