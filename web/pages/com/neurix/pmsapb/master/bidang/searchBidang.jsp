<%--
  Created by IntelliJ IDEA.
  User: gondok
  Date: 06/09/17
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type="text/javascript">

    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="bidangForm" method="post"  namespace="/bidang" action="search_bidang"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Search Bidang</legend>

                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="bidang.idBidang">Kode Bidang :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="idBidang" name="bidang.idBidang" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="bidang.namaBidang">Nama Bidang :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaBidang" name="bidang.namaBidang" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="bidang.flag">Flag :</label>
                                                            </td>

                                                            <td>
                                                                <table>

                                                                    <s:select list="#{'N':'Non-Active'}" id="flag" name="bidang.flag"
                                                                              headerKey="Y" headerValue="Active" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <table>
                                            <tr>
                                                <td>
                                                    <table>

                                                        <sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"
                                                                   position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                                            Please don't close this window, server is processing your request ...
                                                            </br>
                                                            </br>
                                                            </br>
                                                            <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                        </sj:dialog>
                                                        <sj:submit type="button" cssClass="btn btn-primary" formIds="bidangForm" id="search" name="search"
                                                                   onClickTopics="showDialog" onCompleteTopics="closeDialog">
                                                            <i class="icon-search icon-white"></i>
                                                            Search
                                                        </sj:submit>

                                                    </table>
                                                </td>

                                                <td>
                                                    <table>
                                                        <sj:dialog id="view_dialog_add" openTopics="showDialogAdd" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                   position="center" height="350" width="650" autoOpen="false" title="Tambah Bidang"
                                                        >
                                                            <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                        </sj:dialog>

                                                        <s:url var="urlAdd" namespace="/bidang" action="add_bidang"
                                                               escapeAmp="false">
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogAdd" href="%{urlAdd}" cssClass="btn btn-primary">
                                                            <i class="icon-plus-sign icon-white"></i>
                                                            Add Bidang
                                                        </sj:a>
                                                    </table>
                                                </td>

                                                <td>
                                                    <table>

                                                        <button type="button" class="btn" onclick="window.location.href='<s:url action="initForm_bidang"/>'">
                                                            <i class="icon-repeat"></i> Reset
                                                        </button>

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>


                                    <sj:dialog id="view_dialog_edit" openTopics="showDialogSearchEdit" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="450" width="650" autoOpen="false" title="Edit Bidang"
                                    >
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>
                                    <sj:dialog id="view_dialog_delete" openTopics="showDialogSearchDelete" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="450" width="650" autoOpen="false" title="Hapus Bidang"
                                    >
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="40%">
                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfBidang" value="#session.listOfResult" scope="request" />

                                                <display:table name="listOfBidang" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_bidang.action" export="true" id="row" pagesize="20" style="font-size:10">
                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlViewEdit" namespace="/bidang" action="edit_bidang"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idBidang" /></s:param>
                                                            <s:param name="flag"><s:property value="#attr.row.flag" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogSearchEdit" href="%{urlViewEdit}">
                                                            <img border="0" src="<s:url value="/pages/images/icon_edit.ico"/>" name="icon_edit">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlViewDelete" namespace="/bidang" action="delete_bidang"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idBidang" /></s:param>
                                                            <s:param name="flag"><s:property value="#attr.row.flag" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogSearchDelete" href="%{urlViewDelete}">
                                                            <img border="0" src="<s:url value="/pages/images/icon_trash.ico"/>" name="icon_trash">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column property="idBidang" sortable="true" title="<small>Kode Bidang</small>" />
                                                    <display:column property="namaBidang" sortable="true" title="<small>Nama Bidang</small>"  />
                                                    <display:column property="flag" sortable="true" title="<small>Flag</small>" />
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                </s:form>



                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>
