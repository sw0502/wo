<%--
  Created by IntelliJ IDEA.
  User: gondok
  Date: 07/09/17
  Time: 3:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<html>
<head>

    <link href="<s:url value="/pages/bootstrap/css/bootstrap.css"/>" rel="stylesheet" media="screen">
    <script type="text/javascript">


        $.subscribe('beforeProcessSave', function (event, data) {

            var namaBidang = document.getElementById('namaBidang').value;
            var idBidang = '<s:property value="bidang.idBidang"/>';

            if (idBidang != '') {

                if (confirm('Do you want to save this record ?')) {
                    event.originalEvent.options.submit = true;
                    $.publish('showDialogEdit');

                } else {
                    // Cancel Submit comes with 1.8.0
                    event.originalEvent.options.submit = false;
                }

            } else {

                event.originalEvent.options.submit = false;

                var msg = "";
                if (idBidang == '') {
                    msg = 'Field <strong>Kode Bidang</strong> is required.' + '<br/>';
                }
//
//                if (namaBidang == '') {
//                    msg = msg + 'Field <strong>Nama Bidang</strong> is required.' + '<br/>';
//                }

                document.getElementById('errorValidationMessage').innerHTML = msg;

                $.publish('showErrorValidationDialog');

            }
        });

        $.subscribe('successDialogEdit', function (event, data) {
            if (event.originalEvent.request.status == 200) {
                jQuery(".ui-dialog-titlebar-close").hide();
                $.publish('showInfoDialogEdit');
            }
        });

        $.subscribe('errorDialogEdit', function (event, data) {
            document.getElementById('errorMessage').innerHTML = "Status = " + event.originalEvent.request.status + ", \n\n" + event.originalEvent.request.getResponseHeader('message');
            $.publish('showErrorDialog');
        });

        function cancelEditBtn() {
            $('#view_dialog_edit').dialog('close');
        };

        function callSearchFunction() {
            $('#info_dialog_edit').dialog('close');
            $('#view_dialog_edit').dialog('close');
            document.bidangForm.action='search_bidang.action';
            document.bidangForm.submit();
        };



    </script>
</head>

<body bgcolor="#FFFFFF">

<table width="100%" align="center">
    <tr>
        <td align="center">

            <s:form id="editBidangForm" method="post"  namespace="/bidang" action="saveEdit_bidang"
                    cssClass="well form-horizontal">
                <fieldset>

                    <legend align="left">Edit Alat</legend>
                    <table>
                        <tr>
                            <td width="10%" align="center">
                                <%@ include file="/pages/common/message.jsp" %>
                            </td>
                        </tr>
                    </table>

                    <table >
                        <tr>
                            <td>
                                <label class="control-label"><small>Nama Bidang :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:textfield id="namaBidang" name="bidang.namaBidang" required="true"  cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>
                                    <s:hidden id="idBidang" name="bidang.idBidang"/>
                                </table>
                            </td>
                        </tr>
                        <%--<tr>--%>
                            <%--<td>--%>
                                <%--<label class="control-label"><small>Nama belajar :</small></label>--%>
                            <%--</td>--%>
                            <%--<td>--%>
                                <%--<table>--%>
                                    <%--<s:textfield id="editnamaAlat" name="belajar.namaAlat" required="true" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                                <%--</table>--%>
                            <%--</td>--%>
                        <%--</tr>--%>
                        <%--<tr>--%>
                            <%--<td>--%>
                                <%--<table>--%>
                                    <%--<label class="control-label"><small>Keterangan :</small></label>--%>
                                <%--</table>--%>
                            <%--</td>--%>
                            <%--<td>--%>
                                <%--<table>--%>
                                    <%--<s:textarea id="editKeterangan" name="belajar.keterangan" disabled="false" cssClass="textForm" rows="3" cols="3" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>--%>
                                <%--</table>--%>
                            <%--</td>--%>
                        <%--</tr>--%>
                        <tr>
                            <td>
                                <label class="control-label"><small>Flag :</small></label>
                            </td>
                            <td>
                                <table>
                                    <s:select list="#{'N':'Non-Active'}" id="editflag" name="bidang.flag"
                                              headerKey="Y" headerValue="Active" cssClass="textForm"/>
                                </table>

                            </td>
                        </tr>


                    </table>
                </fieldset>

                <div id="actions" class="form-actions">
                    <table>
                        <tr>
                            <td></td>
                            <td>
                                <div id="crud">
                                    <table>

                                        <tr>
                                            <td align="center">

                                                <table>

                                                    <sj:submit targets="crud" type="button" cssClass="btn btn-primary" formIds="editBidangForm" id="save" name="save"
                                                               onBeforeTopics="beforeProcessSave" onCompleteTopics="closeDialogEdit,successDialogEdit"
                                                               onSuccessTopics="successDialogEdit" onErrorTopics="errorDialogEdit" >
                                                        <i class="icon-ok-sign icon-white"></i>
                                                        Save
                                                    </sj:submit>

                                                    <sj:dialog id="waiting_dialog_edit" openTopics="showDialogEdit" closeTopics="closeDialogEdit" modal="true"
                                                               resizable="false"
                                                               height="250" width="600" autoOpen="false" title="Saving ...">
                                                        Please don't close this window, server is processing your request ...
                                                        </br>
                                                        </br>
                                                        </br>
                                                        <img border="0" src="<s:url value="/pages/images/indicator-write.gif"/>" name="image_indicator_write">
                                                    </sj:dialog>

                                                    <sj:dialog id="info_dialog_edit" openTopics="showInfoDialogEdit" modal="true" resizable="false" closeOnEscape="false"
                                                               position="center" height="200" width="400" autoOpen="false" title="Infomation Dialog"
                                                               buttons="{
                                                                    'OK':function() {
                                                                               callSearchFunction();
                                                                         }
                                                                 }"
                                                    >
                                                        <img border="0" src="<s:url value="/pages/images/icon_success.png"/>" name="icon_success">
                                                        Record has been saved successfully.
                                                    </sj:dialog>

                                                    <sj:dialog id="error_dialog" openTopics="showErrorDialog" modal="true" resizable="false"
                                                               position="center" height="250" width="600" autoOpen="false" title="Error Dialog"
                                                               buttons="{
                                                                    'OK':function() { $('#error_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> System Found : <p id="errorMessage"></p>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                    <sj:dialog id="error_validation_dialog" openTopics="showErrorValidationDialog" modal="true" resizable="false"
                                                               position="center" height="280" width="500" autoOpen="false" title="Warning"
                                                               buttons="{
                                                                    'OK':function() { $('#error_validation_dialog').dialog('close'); }
                                                                }"
                                                    >
                                                        <div class="alert alert-error fade in">
                                                            <label class="control-label" align="left">
                                                                <img border="0" src="<s:url value="/pages/images/icon_error.png"/>" name="icon_error"> Please check this field :
                                                                <br/>
                                                                <center><div id="errorValidationMessage"></div></center>
                                                            </label>
                                                        </div>
                                                    </sj:dialog>

                                                </table>
                                            </td>

                                            <td>
                                                <button type="button" class="btn" style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" onclick="cancelEditBtn();">
                                                    <i class="icon-remove-circle "/> Cancel
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                    </table>
                </div>
            </s:form>
        </td>
    </tr>
</table>

</body>
</html>
