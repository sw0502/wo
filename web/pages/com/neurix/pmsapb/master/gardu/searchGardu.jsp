<%--
  Created by IntelliJ IDEA.
  User: gondok
  Date: 12/09/17
  Time: 6:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type="text/javascript">

    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="garduForm" method="post"  namespace="/gardu" action="search_gardu"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Search Gardu</legend>

                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="gardu.nomorGarduInduk">No Gardu Induk :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="nomorGarduInduk" name="gardu.nomorGarduInduk" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="gardu.kodeApp">Kode App :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="kodeApp" name="gardu.kodeApp" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="gardu.kodeApb">Kode Apb :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="kodeApb" name="gardu.kodeApb" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="gardu.namaGarduInduk">Nama Gardu Induk :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaGarduInduk" name="gardu.namaGarduInduk" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <%--<tr>--%>
                                                <%--<td>--%>
                                                    <%--<table>--%>
                                                        <%--<tr>--%>
                                                            <%--<td>--%>
                                                                <%--<label class="control-label" for="gardu.kodeGardu">Kode Gardu :</label>--%>
                                                            <%--</td>--%>

                                                            <%--<td>--%>
                                                                <%--<table>--%>
                                                                    <%--<s:textfield id="kodeGardu" name="gardu.kodeGardu" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />--%>
                                                                <%--</table>--%>
                                                            <%--</td>--%>
                                                        <%--</tr>--%>
                                                    <%--</table>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="gardu.tegangan">Tegangan :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="tegangan" name="gardu.tegangan" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="gardu.statusGarduInduk">Status Gardu :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="statusGarduInduk" name="gardu.statusGarduInduk" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="gardu.jcc">JCC :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="jcc" name="gardu.jcc" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="gardu.flag">Flag :</label>
                                                            </td>

                                                            <td>
                                                                <table>

                                                                    <s:select list="#{'N':'Non-Active'}" id="flag" name="gardu.flag"
                                                                              headerKey="Y" headerValue="Active" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <table>
                                            <tr>
                                                <td>
                                                    <table>

                                                        <sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"
                                                                   position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                                            Please don't close this window, server is processing your request ...
                                                            </br>
                                                            </br>
                                                            </br>
                                                            <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                        </sj:dialog>
                                                        <sj:submit type="button" cssClass="btn btn-primary" formIds="garduForm" id="search" name="search"
                                                                   onClickTopics="showDialog" onCompleteTopics="closeDialog">
                                                            <i class="icon-search icon-white"></i>
                                                            Search
                                                        </sj:submit>

                                                    </table>
                                                </td>

                                                <td>
                                                    <table>
                                                        <sj:dialog id="view_dialog_add" openTopics="showDialogAdd" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                   position="center" height="550" width="950" autoOpen="false" title="Tambah Gardu"
                                                        >
                                                            <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                        </sj:dialog>

                                                        <s:url var="urlAdd" namespace="/gardu" action="add_gardu"
                                                               escapeAmp="false">
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogAdd" href="%{urlAdd}" cssClass="btn btn-primary">
                                                            <i class="icon-plus-sign icon-white"></i>
                                                            Add Gardu
                                                        </sj:a>
                                                    </table>
                                                </td>

                                                <td>
                                                    <table>

                                                        <button type="button" class="btn" onclick="window.location.href='<s:url action="initForm_gardu"/>'">
                                                            <i class="icon-repeat"></i> Reset
                                                        </button>

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>


                                    <sj:dialog id="view_dialog_edit" openTopics="showDialogSearchEdit" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="550" width="950" autoOpen="false" title="Edit Gardu"
                                    >
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>
                                    <sj:dialog id="view_dialog_delete" openTopics="showDialogSearchDelete" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="550" width="950" autoOpen="false" title="Hapus Gardu"
                                    >
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="80%">
                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfGardu" value="#session.listOfResult" scope="request" />

                                                <display:table name="listOfGardu" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_gardu.action" export="true" id="row" pagesize="20" style="font-size:10">
                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlViewEdit" namespace="/gardu" action="edit_gardu"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.nomorGarduInduk" /></s:param>
                                                            <s:param name="flag"><s:property value="#attr.row.flag" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogSearchEdit" href="%{urlViewEdit}">
                                                            <img border="0" src="<s:url value="/pages/images/icon_edit.ico"/>" name="icon_edit">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlViewDelete" namespace="/gardu" action="delete_gardu"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.nomorGarduInduk" /></s:param>
                                                            <s:param name="flag"><s:property value="#attr.row.flag" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogSearchDelete" href="%{urlViewDelete}">
                                                            <img border="0" src="<s:url value="/pages/images/icon_trash.ico"/>" name="icon_trash">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column property="nomorGarduInduk" sortable="true" title="<small>No Gardu</small>" />
                                                    <display:column property="kodeApp" sortable="true" title="<small>Kode App</small>" />
                                                    <display:column property="kodeApb" sortable="true" title="<small>Kode Apb</small>" />
                                                    <display:column property="namaGarduInduk" sortable="true" title="<small>Nama Gardu</small>" />
                                                    <%--<display:column property="kodeGardu" sortable="true" title="<small>Kode Gardu</small>" />--%>
                                                    <display:column property="alamat" sortable="true" title="<small>Alamat</small>" />
                                                    <display:column property="tegangan" sortable="true" title="<small>Tegangan</small>" />
                                                    <display:column property="statusGarduInduk" sortable="true" title="<small>Status Gardu</small>" />
                                                    <display:column property="jcc" sortable="true" title="<small>JCC</small>" />
                                                    <display:column property="flag" sortable="true" title="<small>Flag</small>" />
                                                    <display:setProperty name="export.excel.filename">Gardu_induk.xls</display:setProperty>
                                                    <display:setProperty name="export.csv.filename">Gardu_induk.csv</display:setProperty>

                                                    <%--<display:column property="action" sortable="true" title="CreatedWho"/>--%>

                                                </display:table>

                                            </td>
                                        </tr>
                                    </table>

                                </s:form>



                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>
