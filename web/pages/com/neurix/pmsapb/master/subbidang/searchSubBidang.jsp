<%--
  Created by IntelliJ IDEA.
  User: gondok
  Date: 06/09/17
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
    <%@ include file="/pages/common/header.jsp" %>
    <script type="text/javascript">

    </script>

</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <s:form id="subbidangForm" method="post"  namespace="/subbidang" action="search_subbidang"
                                        cssClass="well form-horizontal">

                                    <fieldset>
                                        <legend align="left">Search Sub Bidang</legend>

                                        <table >
                                            <tr>
                                                <td width="10%" align="center">
                                                    <%@ include file="/pages/common/message.jsp" %>
                                                </td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="subBidang.idSubBidang">Kode Sub Bidang :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="idSubBidang" name="subBidang.idSubBidang" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="subBidang.namaSubBidang">Nama Sub Bidang :</label>
                                                            </td>

                                                            <td>
                                                                <table>
                                                                    <s:textfield id="namaSubBidang" name="subBidang.namaSubBidang" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;" />
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <label class="control-label" for="subBidang.flag">Flag :</label>
                                                            </td>

                                                            <td>
                                                                <table>

                                                                    <s:select list="#{'N':'Non-Active'}" id="flag" name="subBidang.flag"
                                                                              headerKey="Y" headerValue="Active" cssStyle="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;"/>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                        </table>

                                    </fieldset>


                                    <div id="actions" class="form-actions">
                                        <table>
                                            <tr>
                                                <td>
                                                    <table>

                                                        <sj:dialog id="waiting_dialog" openTopics="showDialog" closeTopics="closeDialog" modal="true" resizable="false"
                                                                   position="center" height="250" width="600" autoOpen="false" title="Searching..." cssClass="dialogFont">
                                                            Please don't close this window, server is processing your request ...
                                                            </br>
                                                            </br>
                                                            </br>
                                                            <img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" name="image_indicator_read">
                                                        </sj:dialog>
                                                        <sj:submit type="button" cssClass="btn btn-primary" formIds="subbidangForm" id="search" name="search"
                                                                   onClickTopics="showDialog" onCompleteTopics="closeDialog">
                                                            <i class="icon-search icon-white"></i>
                                                            Search
                                                        </sj:submit>

                                                    </table>
                                                </td>

                                                <td>
                                                    <table>
                                                        <sj:dialog id="view_dialog_add" openTopics="showDialogAdd" modal="true" resizable="false" cssStyle="text-align:left;"
                                                                   position="center" height="350" width="650" autoOpen="false" title="Tambah Sub Bidang"
                                                        >
                                                            <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                                        </sj:dialog>

                                                        <s:url var="urlAdd" namespace="/subbidang" action="add_subbidang"
                                                               escapeAmp="false">
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogAdd" href="%{urlAdd}" cssClass="btn btn-primary">
                                                            <i class="icon-plus-sign icon-white"></i>
                                                            Add Sub Bidang
                                                        </sj:a>
                                                    </table>
                                                </td>

                                                <td>
                                                    <table>

                                                        <button type="button" class="btn" onclick="window.location.href='<s:url action="initForm_subbidang"/>'">
                                                            <i class="icon-repeat"></i> Reset
                                                        </button>

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>


                                    <sj:dialog id="view_dialog_edit" openTopics="showDialogSearchEdit" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="450" width="650" autoOpen="false" title="Edit Sub Bidang"
                                    >
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>
                                    <sj:dialog id="view_dialog_delete" openTopics="showDialogSearchDelete" modal="true" resizable="false" cssStyle="text-align:left;"
                                               position="center" height="450" width="650" autoOpen="false" title="Hapus Sub Bidang"
                                    >
                                        <center><img border="0" src="<s:url value="/pages/images/indicator-read.gif"/>" alt="Loading..."/></center>
                                    </sj:dialog>

                                    <table width="40%">
                                        <tr>
                                            <td align="center">

                                                <s:set name="listOfSubBidang" value="#session.listOfResult" scope="request" />

                                                <display:table name="listOfSubBidang" class="table table-condensed table-striped table-hover"
                                                               requestURI="paging_displaytag_subbidang.action" export="true" id="row" pagesize="20" style="font-size:10">
                                                    <display:column media="html" title="<small>Edit</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlViewEdit" namespace="/subbidang" action="edit_subbidang"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idSubBidang" /></s:param>
                                                            <s:param name="flag"><s:property value="#attr.row.flag" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogSearchEdit" href="%{urlViewEdit}">
                                                            <img border="0" src="<s:url value="/pages/images/icon_edit.ico"/>" name="icon_edit">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column media="html" title="<small>Delete</small>" style="text-align:center;font-size:9">
                                                        <s:url var="urlViewDelete" namespace="/subbidang" action="delete_subbidang"
                                                               escapeAmp="false">
                                                            <s:param name="id"><s:property value="#attr.row.idSubBidang" /></s:param>
                                                            <s:param name="flag"><s:property value="#attr.row.flag" /></s:param>
                                                        </s:url>
                                                        <sj:a onClickTopics="showDialogSearchDelete" href="%{urlViewDelete}">
                                                            <img border="0" src="<s:url value="/pages/images/icon_trash.ico"/>" name="icon_trash">
                                                        </sj:a>
                                                    </display:column>
                                                    <display:column property="idSubBidang" sortable="true" title="<small>Kode Sub Bidang</small>" />
                                                    <display:column property="namaSubBidang" sortable="true" title="<small>Nama Sub Bidang</small>"  />
                                                    <%--<display:column property="idBidang" sortable="true" title="<small>Nama Bidang</small>"  />--%>
                                                    <display:column property="flag" sortable="true" title="<small>Flag</small>" />
                                                </display:table>
                                            </td>
                                        </tr>
                                    </table>

                                </s:form>



                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>
</body>
</html>
