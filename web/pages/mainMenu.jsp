<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ivelincloud" uri="/WEB-INF/tld/mainmenu.tld" %>
<%@ taglib prefix="display" uri="/WEB-INF/tld/displaytag-el.tld" %>

<html>
<head>
<%@ include file="/pages/common/header.jsp" %>
</head>

<body class="sidebar-push  sticky-footer">

<%@ include file="/pages/common/headerNav.jsp" %>

<ivelincloud:mainMenu/>

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <label class="control-label" style="font-size: 28;font-family: Calibri, Arial, sans-serif" ><strong>Welcome to Project Management System PLN-APB Jatim</strong></label>
                                <br/>
                                <p><label class="control-label"><small>You in Cloud now and simplify your work</small></label></p>

                            </div>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@ include file="/pages/common/footer.jsp" %>
</div>

<%@ include file="/pages/common/lastScript.jsp" %>

</body>
</html>


