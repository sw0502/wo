<div class="overlay-disabled"></div>

<script type='text/javascript' src='<s:url value="/pages/script/bootstrap.min.js"/>'></script>

<!-- Plugins -->

<script type='text/javascript' src='<s:url value="/pages/script/plugins.min.js"/>'></script>

<!-- App Scripts -->

<script type='text/javascript' src='<s:url value="/pages/script/scripts.js"/>'></script>
