<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>

    <TITLE>E-Farming</TITLE>
    <link rel="shortcut icon" href="../pages/images/E-Farming.png"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link href="../pages/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <link type="text/css" href="../pages/css/initial-form.css" rel="stylesheet"/>
    <link type="text/css" href="../pages/css/style_2.css" rel="stylesheet"/>
    <link type="text/css" href="../pages/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>

    <script type='text/javascript' src='../pages/bootstrap/js/bootstrap.js'></script>
</head>

<body class="sidebar-push  sticky-footer">
<!-- Navbar -->

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <%--<div class="navbar-header">--%>
        <%--<ul class="nav navbar-nav navbar-left">--%>
        <%--<li>--%>
        <table>
            <tr>
                <td>
                    <ul class="nav navbar-nav navbar-left">
                        <li>

                            <a class="navbar-brand" href="#">

                                <img style="width: 75px" src="../pages/images/E-Farming.png" name="image_efarming">

                            </a>
                        </li>


                    </ul>

                </td>
                <td>
                    <a class="navbar-brand" href="#" style="font-size: 28;font-family: Calibri, Arial, sans-serif">E-Farming </a>
                </td>
                <td>
                    <img style="width: 70px" border="0" src="../pages/images/logo-PTPN-10.png" name="image_ptpn">

                </td>

            </tr>
        </table>

    </div>
</nav>
<!-- // Navbar -->

<div class="container-fluid">
    <div id="main">
        <div class="media">
            <div class="media-body">
                <table width="100%" align="center" align="center">
                    <tr>
                        <td align="center">
                            <div id="box">

                                <label class="control-label" style="font-size: 28;font-family: Calibri, Arial, sans-serif" ><strong>Access Denied</strong></label>
                                <br/>
                                <p><label class="control-label"><small>Your access is invalid. <a href="../mainMenu.action">Please check your address again.</a></small></label></p>

                            </div>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
<div class="footer">
    <ul>

        <li><a href="http://www.ptpn10.co.id/QC"><strong>Created by Tim QC-PTPN10 &copy; 2016 </strong></a></li>
        <li><a href="http://www.ptpn10.co.id/contact_us">Contact us</a></li>
        <li><a href="http://www.ptpn10.co.id/support">Support</a></li>
    </ul>
</div>

</div>
<div class="overlay-disabled"></div>

<script type='text/javascript' src='../pages/script/bootstrap.min.js'></script>

<!-- Plugins -->

<script type='text/javascript' src='../pages/script/plugins.min.js'></script>

<!-- App Scripts -->

<script type='text/javascript' src='../pages/script/scripts.js'></script>

</body>
</html>


