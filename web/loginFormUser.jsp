<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PMS APB</title>
    <link rel="shortcut icon" href="<s:url value="/pages/images/logo-pln.png"/>"/>
    <link rel="stylesheet" type="text/css" href="<s:url value="/pages/css/style-login.css"/>"/>

    <script src="<s:url value="/pages/script/login.custom.js"/>"></script>
    <!--[if lte IE 7]>
    <style>.main {
        display: none;
    }

    .support-note .note-ie {
        display: block;
    }


    </style>
    <![endif]-->
    <sj:head jqueryui="true" jquerytheme="pepper-grinder"/>
</head>
<script type="text/javascript">
    function validationSubmit() {
        var username=window.document.getElementsByName('j_username')[0].value;
        var password=window.document.getElementsByName('j_password')[0].value;

        if (username== '' || password == '') {
            window.document.getElementById('messageError').innerHTML='';
            window.document.getElementById('validationSubmitError').innerHTML='Your username or password is empty. Please enter correctly.';
            return false;
        } else  {
            window.document.getElementById('validationSubmitError').innerHTML='';
            return true;
        }
    }
</script>
<body>

<div class="container">

    <header>

        </br>
        </br>

        </br>
        </br>

        <h1 style="font-size:40px;font-family: Arial, Helvetica, sans-serif;text-align : center;color : #3a86ff;"><strong><b>Management System APB Jatim</b></strong></h1>

        <h2></h2>

        </br>
        </br>

        <p>
            <img width="10%" height="10%" border="0" src="<s:url value="/pages/images/logo-pln.png"/>" name="image_cloud">
        </p>

        <div class="support-note">
            <span class="note-ie">Sorry, only modern browsers.</span>
        </div>

    </header>

    <s:if test="flagError">
        <div class="errorblock">
	            <span id="messageError" class="note-errorblock">
		            Your login attempt was not successful, try again. Caused :
                    </br>
                    <s:property value="%{messageError}"/>
                    </br>
                        <%--<s:if test="flagSignUp">--%>
                        <%--</br>--%>
                        <%--<s:a action="signUpUser">Sign up for Neurix (e-payment) apps</s:a>--%>
                    <%--</s:if>--%>
                    <%--<s:else>--%>
                        <%--</br>--%>
                        <%--<img border="0" src="<s:url value="%{userPhotoUrl}"/>" name="image_user">--%>
                    <%--</s:else>--%>
	             </span>
        </div>
    </s:if>
    <s:if test="hasActionErrors()">
        <div class="errorblock">
                <span class="note-errorblock">
                    <s:actionerror/>
                </span>
        </div>
    </s:if>

    <div  class="errorblock" >
        <span id="validationSubmitError" class="note-errorblock"></span>
    </div>


    <section class="main">
        <form id="formlogin" class="form-1" action="<s:url value='j_spring_security_check'/>" method='POST' onsubmit="return validationSubmit();">
            <p class="field">
                <s:textfield name="j_username" value="%{userName}" placeholder="Userid or email"/>
                <%--<input type="text" name="j_username" value="%{userName}" placeholder="Userid or email">--%>
                <i class="icon-user icon-large"></i>
            </p>

            <p class="field">
                <s:password name="j_password" placeholder="Password"/>
                <%--<input type="password" name="j_password" placeholder="Password">--%>
                <i class="icon-lock icon-large"></i>
            </p>
            </p>
            <p class="field">
                <input type="checkbox" name="_spring_security_remember_me">
                <label for="remember" class="label">Remember Me</label>
            </p>

            <div id="waiting_dialog" style="font-size:14px;font-family: Arial, Helvetica, sans-serif;text-align : center;">
                Please wait a moment, server is loging in your account ...
                </br>
                </br>
                </br>
                <img border="0" src="<s:url value="/pages/images/indicator-loading-login.GIF"/>" name="image_indicator_read">
            </div>
            <script type='text/javascript'>
                jQuery(document).ready(function () {
                    var options_waiting_dialog = {};
                    options_waiting_dialog.height = 200;
                    options_waiting_dialog.width = 500;
                    options_waiting_dialog.title = "Loging in...";
                    options_waiting_dialog.position = "center";
                    options_waiting_dialog.resizable = false;
                    options_waiting_dialog.autoOpen = false;
                    options_waiting_dialog.modal = true;
                    options_waiting_dialog.opentopics = "showDialog";
                    options_waiting_dialog.closetopics = "closeDialog";
                    options_waiting_dialog.jqueryaction = "dialog";
                    options_waiting_dialog.id = "waiting_dialog";
                    options_waiting_dialog.href = "#";

                    jQuery.struts2_jquery_ui.bind(jQuery('#waiting_dialog'),options_waiting_dialog);
                    jQuery(".ui-dialog-titlebar-close").hide();
                });
            </script>

            <script type='text/javascript'>
                jQuery(document).ready(function () {
                    var options_search = {};
                    options_search.jqueryaction = "button";
                    options_search.id = "login";
                    options_search.name = "login";
                    options_search.oncom = "closeDialog";
                    options_search.href = "#";
                    options_search.formids = "formlogin";
                    options_search.onclick = "showDialog";

                    jQuery.struts2_jquery.bind(jQuery('#login'),options_search);
                });
            </script>

            <p class="submit">
                <button type="submit" id="login" name="login"><i class="icon-arrow-right icon-large"></i></button>
            </p>
        </form>

    </section>
</div>

</body>
</html>