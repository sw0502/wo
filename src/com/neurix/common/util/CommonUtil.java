package com.neurix.common.util;

import com.neurix.authorization.role.model.Roles;
import com.neurix.authorization.user.model.UserDetailsLogin;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.servlet.http.HttpSession;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: Thunderbird
 * Date: 19/02/13
 * Time: 21:18
 * To change this template use File | Settings | File Templates.
 */
public class CommonUtil {

    protected static transient Logger logger = Logger.getLogger(CommonUtil.class);

    public static String userLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
//                String username=userDetailsLogin.getUsername();

                String username=userDetailsLogin.getUserNameDetail();

                return username;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String userIdLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
                String username=userDetailsLogin.getUsername();

                return username;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String roleAsLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
//                String username=userDetailsLogin.getUsername();

                String roleName=((Roles)userDetailsLogin.getRoles().get(0)).getRoleName();

                return roleName;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String userBranchLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
//                String username=userDetailsLogin.getUsername();

                String branchId=userDetailsLogin.getBranchId();

                return branchId;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String userBranchNameLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();

                String branchName=userDetailsLogin.getBranchName();

                return branchName;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String companyNameLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
                String companyName=userDetailsLogin.getCompanyName();

                return companyName;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String companyIdLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
                String companyId=userDetailsLogin.getCompanyId();

                return companyId;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }


    //for payment gateway customer login
    public static String customerIdLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
                String customerId=userDetailsLogin.getCustomerId();

                return customerId;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String customerNameLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
                String customerName=userDetailsLogin.getCustomerName();

                return customerName;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String customerAddressLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
                String customerAddress=userDetailsLogin.getCustomerAddress();

                return customerAddress;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String customerEmailLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
                String customerEmail=userDetailsLogin.getCustomerEmail();

                return customerEmail;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String customerNPWPLogin() throws UsernameNotFoundException {
        HttpSession session = ServletActionContext.getRequest().getSession();
        if (session.getAttribute("SPRING_SECURITY_CONTEXT") != null) {
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
            if (securityContextImpl.getAuthentication() != null) {
                UserDetailsLogin userDetailsLogin=(UserDetailsLogin)securityContextImpl.getAuthentication().getPrincipal();
                String customerNPWP=userDetailsLogin.getCustomerNPWP();

                return customerNPWP;
            } else {
                throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
            }

        } else {
            throw new UsernameNotFoundException("User Not Found, session may be time out. Please login again.");
        }
    }

    public static String numbericFormat(BigDecimal number,String pattern) {
//        NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMANY); //for indo money format
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US); //for international money format
        DecimalFormat df = (DecimalFormat)nf;
        df.applyPattern(pattern);
        return df.format(number);
    }

    public static String longDateFormat(Timestamp timestamp) {
        if (timestamp!=null) {
            FastDateFormat dateFormat = FastDateFormat.getInstance("dd/MM/yyyy HH:mm:ss");
            return dateFormat.format(timestamp.getTime());
        } else {
            return "";
        }
    }

    public static int elapsed(Calendar before, Calendar after, int field) {
        Calendar clone = (Calendar) before.clone(); // Otherwise changes are been reflected.
        int elapsed = -1;
        while (!clone.after(after)) {
            clone.add(field, 1);
            elapsed++;
        }
        return elapsed;
    }

    public static String waitingTime(Calendar start, Calendar end) {

        Integer[] elapsed = new Integer[6];
        Calendar clone = (Calendar) start.clone(); // Otherwise changes are been reflected.
        elapsed[0] = elapsed(clone, end, Calendar.YEAR);
        clone.add(Calendar.YEAR, elapsed[0]);
        elapsed[1] = elapsed(clone, end, Calendar.MONTH);
        clone.add(Calendar.MONTH, elapsed[1]);
        elapsed[2] = elapsed(clone, end, Calendar.DATE);
        clone.add(Calendar.DATE, elapsed[2]);
        elapsed[3] = (int) (end.getTimeInMillis() - clone.getTimeInMillis()) / 3600000;
        clone.add(Calendar.HOUR, elapsed[3]);
        elapsed[4] = (int) (end.getTimeInMillis() - clone.getTimeInMillis()) / 60000;
        clone.add(Calendar.MINUTE, elapsed[4]);
        elapsed[5] = (int) (end.getTimeInMillis() - clone.getTimeInMillis()) / 1000;

        String selisih = String.format("%d days - %d hours - %d minutes - %d seconds", elapsed[2], elapsed[3], elapsed[4], elapsed[5]);

        return selisih;
    }

    public static String getUploadFolderValue() {

        Properties prop = new Properties();
        InputStream input = null;

        String value = "null";
        try {

            input = CommonUtil.class.getClassLoader().getResourceAsStream("pms-apb.properties");

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            value = prop.getProperty("upload.folder");
            input.close();
            logger.info("success to load pms-apb.properties");
        } catch (IOException ex) {
            logger.error("Found error," + ex.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    logger.error("Found error," + e.getMessage());
                }
            }
        }

        return value;

    }

    public static String simpleDateFormat(Date date) {
        FastDateFormat dateFormat = FastDateFormat.getInstance("dd-MM-yyyy");
        String stDate = dateFormat.format(date.getTime());
        return stDate;
    }

    public static Date convertToDate(String stDate) {
        if (stDate != null && !"".equalsIgnoreCase(stDate)) {
            String sDay;
            String sMonth;
            String sYear;
            int day;
            int month;
            int year;
            sDay = stDate.substring(0, 2);
            day = Integer.parseInt(sDay);
            sMonth = stDate.substring(3, 5);
            month = Integer.parseInt(sMonth);
            sYear = stDate.substring(6);
            year = Integer.parseInt(sYear);
            return new Date(year - 1900, month - 1, day);
        } else {
            return null;
        }

    }

    public static Timestamp convertToTimestamp(String stDate) {
        if (stDate != null && !"".equalsIgnoreCase(stDate)) {
            String sDay;
            String sMonth;
            String sYear;
            int day;
            int month;
            int year;
            sDay = stDate.substring(0, 2);
            day = Integer.parseInt(sDay);
            sMonth = stDate.substring(3, 5);
            month = Integer.parseInt(sMonth);
            sYear = stDate.substring(6);
            year = Integer.parseInt(sYear);
            return new Timestamp(new Date(year - 1900, month - 1, day).getTime());
        } else {
            return null;
        }

    }


}
