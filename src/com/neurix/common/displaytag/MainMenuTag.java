package com.neurix.common.displaytag;

import com.neurix.authorization.user.model.UserDetailsLogin;
import com.neurix.common.constant.CommonConstant;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Thunderbird
 * Date: 06/02/13
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */
public class MainMenuTag extends TagSupport {

    protected static transient Logger logger = Logger.getLogger(MainMenuTag.class);

    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        initParameters();
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTag() throws JspException {
        logger.debug("begin execute method doEndTag ");

        JspWriter out = pageContext.getOut();
        try {
            out.write(this.getScriptCodes());
        } catch (IOException e) {
            logger.error("[doEndTag] error renderer tag popup, ", e);
            throw new JspException();
        }

        logger.debug("end execute method doEndTag ");

        return EVAL_BODY_INCLUDE;
    }

    private String getScriptCodes() {
        logger.debug("begin execute method getScriptCodes ");

        StringBuilder addedScript = new StringBuilder();

        SimpleDateFormat format = new SimpleDateFormat("EEEEEE, d MMM yyyy");
        String formatedDate = format.format(new Date());
        String userId = "";
        String companyName = "";
        String areaName = "";
        String branchName = "";
        String photoUserUrl = "";

        //for payment gateway
        String customerId = "";
        String customerName = "";
        String customerAddress = "";
        String customerNPWP = "";

        //tambahan posisi user
        String positionName = "";
        List<String> listOfMenuString = new ArrayList<String>();
        String userName = "";

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        HttpSession session = ServletActionContext.getRequest().getSession();
        String contextPath = ServletActionContext.getRequest().getContextPath();
        String logoutUrl = contextPath + CommonConstant.LOGOUT_URL;
        String sessionMonitoringUrl = contextPath + CommonConstant.SESSION_URL;
        String notificationUrl = contextPath + CommonConstant.NOTIFICATION_URL;
        SessionRegistryImpl sessionRegistry = (SessionRegistryImpl) ctx.getBean("sessionRegistry");
        SessionInformation sessionInfo = (sessionRegistry != null ? sessionRegistry.getSessionInformation(session.getId()) : null);
        UserDetailsLogin userDetailsLogin;

        if (sessionInfo != null) {
            userDetailsLogin = (UserDetailsLogin) sessionInfo.getPrincipal();

            if (userDetailsLogin != null) {
                userId = userDetailsLogin.getUsername();
                userName = userDetailsLogin.getUserNameDetail();
                companyName = userDetailsLogin.getCompanyName();
                areaName = userDetailsLogin.getAreaName();
                branchName = userDetailsLogin.getBranchName();
                positionName = userDetailsLogin.getPositionName();
                listOfMenuString = userDetailsLogin.getMenus();

                //for payment-gateway
//                customerId = userDetailsLogin.getCustomerId();
//                customerName = userDetailsLogin.getCustomerName();
//                customerAddress = userDetailsLogin.getCustomerAddress();
//                customerNPWP = userDetailsLogin.getCustomerNPWP();


                if (userDetailsLogin.getPhotoUserUrl()!=null) {

                    //set windows or linux path first
//                    photoUserUrl = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY  + contextPath  + userDetailsLogin.getPhotoUserUrl();

                    photoUserUrl = contextPath  + userDetailsLogin.getPhotoUserUrl();

                    File filePhoto = new File(photoUserUrl);
                    if (filePhoto!=null) {
                        if (filePhoto.length() == 0) {
                            photoUserUrl = contextPath  + CommonConstant.RESOURCE_PATH_USER_PHOTO + CommonConstant.RESOURCE_PATH_UNKNOWN_PHOTO;
//                            photoUserUrl =  CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY  + contextPath + CommonConstant.RESOURCE_PATH_USER_PHOTO + CommonConstant.RESOURCE_PATH_UNKNOWN_PHOTO;
                        } else {
                            photoUserUrl = contextPath  + userDetailsLogin.getPhotoUserUrl();
//                            photoUserUrl =  CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY  + contextPath + userDetailsLogin.getPhotoUserUrl();
                        }
                    } else {
                        photoUserUrl = contextPath  + CommonConstant.RESOURCE_PATH_USER_PHOTO + CommonConstant.RESOURCE_PATH_UNKNOWN_PHOTO;
//                        photoUserUrl =  CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY   + contextPath + CommonConstant.RESOURCE_PATH_USER_PHOTO + CommonConstant.RESOURCE_PATH_UNKNOWN_PHOTO;
                    }

                } else {
                    photoUserUrl = contextPath  + CommonConstant.RESOURCE_PATH_USER_PHOTO + CommonConstant.RESOURCE_PATH_UNKNOWN_PHOTO;
//                    photoUserUrl =  CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY   + contextPath + CommonConstant.RESOURCE_PATH_USER_PHOTO + CommonConstant.RESOURCE_PATH_UNKNOWN_PHOTO;
                }

            }
        }

        //<!-- Sidebar Left -->

        addedScript.append("  <div class=\"sidebar left-side\" id=\"sidebar-left\">\n" +
                "\t<div class=\"sidebar-user\">\n" +
                "\t\t<div class=\"media sidebar-padding\">\n" +
                "\t\t\t\n" +
                "\t\t\t<img src=\"").append(photoUserUrl).append("\" alt=\"person\" style=\"width: 60px\" class=\"img-circle\"/>\n" +
                "\t\t\t\n" +
                "\t\t\t<div class=\"media-body media-middle\">\n" +
                "\t\t\t\t<a href=\"#\" class=\"h4 margin-none\" id=\"userInfo\" class=\"user\" data-toggle=\"popover\" data-placement=\"bottom\" data-content=\"<strong><small>User Id : ").append(userId).append("</br>Branch : ").append(branchName).append("</br>Area : ").append(areaName).append("</br>Jabatan : ").append(positionName).append("</strong>\" data-original-title=\"<strong>User Information</strong>\">").append(userName).append("</a>\n" +
                "\t\t\t\t<ul class=\"list-unstyled list-inline margin-none\">\n" +
                "\t\t\t\t\t<li>\n" +
                "\t\t\t\t\t<a href=\"#\"><i class=\"md-person-outline\"></i></a>\n" +
                "\t\t\t\t\t<script>\n" +
                "\t\t\t                \t$(function ()\n" +
                "\t\t\t                \t{ \n" +
                "\t\t\t                \t$(\"#userInfo\").popover({\n" +
                "\t\t\t                            html:true            \n" +
                "\t\t\t                        });\n" +
                "\t\t\t                \t});\n" +
                "\t\t\t                </script>\n" +
                "\t\t\t\t\t</li>\n" +
                "\t\t\t\t\t<li><a href=\"").append(notificationUrl).append("\"><i class=\"md-email\"></i></a></li>\n" +
                "\t\t\t\t\t<li><a href=\"").append(sessionMonitoringUrl).append("\"><i class=\"md-settings\"></i></a></li>\n" +
                "\t\t\t\t\t<li><a href=\"").append(logoutUrl).append("\"><i class=\"md-exit-to-app\"></i></a></li>\n" +
                "\t\t\t\t</ul>\n" +
                "\t\t\t</div>\n" +
                "\t\t</div>\n" +
                "\t</div>\n");

        //<!-- Wrapper Reqired by Nicescroll (start scroll from here) -->

        addedScript.append("<div class=\"nicescroll \">\n" +
                "\t\t<div class=\"wrapper\" style=\"margin-bottom:90px\">\n" +
                "\t\t\t<ul class=\"nav nav-sidebar\" id=\"sidebar-menu\">");

        //looping menu
        for (String menuString : listOfMenuString) {
            addedScript.append(menuString);
        }

        addedScript.append("</ul></div>\n" +
                "\t\t\n" +
                "\t</div>\n" +
                "</div>");

        //comment cause use new menu, if used old menu open this comment
//        addedScript.append("<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n" +
//                "    <tr>\n" +
//                "        <td width=\"100%\" align=\"center\" style=\"background:#ffffff url('").append(contextPath).append("/pages/images/background-head.jpg') repeat-x top left\">\n" +
//                "            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
//                "                <tr>\n" +
//                "                    <td>\n" +
//                "\n" +
//                "                        <table>\n" +
//                "                            <tr>\n" +
//                "                            <td></td>\n" +
//                "                            <td></td>\n" +
//                "                            <td></td>\n" +
//                "                            <td class=\"userTime\" align=\"center\" width=\"120\"> ").append(formatedDate).append("</td>\n" +
//                "                            <td align=\"center\"><img class=\"fitImg centerImg\" src=\"").append(contextPath).append("/pages/images/airnav-logo.jpg").append("\" name=\"image_logo\"></td>\n" +
//                "                            </tr>\n" +
//                "                        </table>\n" +
//                "                    </td>\n" +
//                "\n" +
//                "                    <td width=\"900px\" valign=\"top\" height=\"56px\" align=\"center\"\n" +
//                "                        style=\"background:#1E4262 url('").append(contextPath).append("/pages/mozilla/header-background.png') no-repeat top left\">\n" +
//                "\n" +
//                "\n" +
//                "                        <script type=\"text/javascript\" src=\"").append(contextPath).append("/pages/mozilla/data.js\"></script>\n" +
//                "\n" +
//                "                        <script type=\"text/javascript\">\n" +
//                "\n" +
//                "                            var menuItems = [];\n" +
//                "                            dwr.engine.setAsync(false);\n" +
//                "                            UserLoginAction.getMenuUser(function(listdata) {\n" +
//                "                                if (listdata[0].indexOf('ERROR') >= 0) {\n" +
//                "                                    alert('Main menu fail to display, please inform to admin.');\n" +
//                "                                } else {\n" +
//                "                                    menuItems = listdata;\n" +
//                "\n" +
//                "                                }\n" +
//                "                            });\n" +
//                "\n" +
//                "                            dm_init();\n" +
//                "\n" +
//                "                        </script>\n" +
//                "\n" +
//                "                    </td>\n" +
//                "\n" +
//                "\n" +
//                "                    <td>\n" +
//                "                        <table>\n" +
//                "                            <tr>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                               <td></td>\n" +
//                "                                <td align=\"center\"><img border=\"0\" class=\"circular centerImg\" src=\"").append(photoUserUrl).append("\" name=\"image_user\"></td>\n" +
//                "                                <td></td>\n"+
//                "                                <td></td>\n"+
//                "                                <td></td>\n"+
//                "                                <td align=\"center\" class=\"userDetail\">" +
//                "<a href=\"#\" id=\"userInfo\" class=\"user\" data-toggle=\"popover\" data-placement=\"bottom\" data-content=\"<strong><small>User Id :</strong> ").append(userName).append(" </br><strong>Client Id :</strong>  ").append(customerId).append(" </br><strong>Address :</strong>  ").append(customerAddress).append(" </br><strong>NPWP :</strong> ").append(customerNPWP).append("</small>").append("\" data-original-title=\"<strong>User Information</strong>\"><div class=\"label label-info\">").append(customerName).append("</div></a>" +
//                "<script>\n" +
//                "$(function ()\n" +
//                "{ \n" +
//                "$(\"#userInfo\").popover({\n" +
//                "            html:true            \n" +
//                "        });\n" +
//                "});\n" +
//                "</script>                                " +
//                "</td>\n" +
//                "                                <td></td>\n" +
//                "                                <td></td>\n" +
//                "                                <td></td>\n" +
//                "                                <td align=\"center\" class=\"user\" >\n" +
//                "                                <a href=\"").append(logoutUrl).append("\" id=\"userInfo\" class=\"label label-info\"><i class=\"icon-off\"></i></a>                                \n" +
//                "                                </td>\n" +
//                "                            </tr>\n" +
//                "                        </table>\n" +
//                "                    </td>\n" +
//                "\n" +
//                "                </tr>\n" +
//                "\n" +
//                "            </table>\n" +
//                "        </td>\n" +
//                "    </tr>\n" +
//                "</table>");

        logger.debug("end execute method getScriptCodes ");

        return addedScript.toString();
    }

    public void release() {
        super.release();
    }

    protected void initParameters() {

    }
}
