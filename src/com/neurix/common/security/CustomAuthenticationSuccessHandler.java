package com.neurix.common.security;

import com.neurix.authorization.user.bo.UserBo;
import com.neurix.authorization.user.model.UserDetailsLogin;
import com.neurix.common.constant.CommonConstant;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.context.ContextLoader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by User on 7/22/2017.
 */
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    protected static transient Logger logger = Logger.getLogger(CustomAuthenticationSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        logger.info("[CustomAuthenticationSuccessHandler.onAuthenticationSuccess] start process >>>");

        String sessionId = request.getSession()!=null ? request.getSession().getId() : null;
        String urlRedirect;

        if (sessionId!=null && !"".equalsIgnoreCase(sessionId)) {
            UserDetailsLogin userDetailsLogin = (UserDetailsLogin) authentication.getPrincipal();
            if (userDetailsLogin!=null) {

                ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
                UserBo userBo = (UserBo) ctx.getBean("userBoProxy");

                String pathImage = "";
                String pathName = CommonConstant.RESOURCE_PATH_USER_PHOTO;

                UserDetailsLogin readUserDetailsLogin = null;
                String username=userDetailsLogin.getUsername();

                //updated by ferdi, 07-07-2017, checking tokenId from mobile apps, if found then throw error message

                boolean isFound = false;
                try {
                    isFound = userBo.isFoundOtherSessionActiveUserSessionLog(username);
                } catch (HibernateException e) {
                    logger.error("[CustomAuthenticationSuccessHandler.onAuthenticationSuccess] Error while finding user session log,", e);
                }

                if (isFound) {

                    urlRedirect = request.getContextPath() + "/loginUser.action";
                    request.getSession().setAttribute("ErrorMessageLogin","Found other device using your id, please call your admin.");

                } else {

                    try {
                        readUserDetailsLogin = userBo.retrievePhotoUser(pathName, username);
                    } catch (HibernateException e) {
                        logger.error("[CustomAuthenticationSuccessHandler.onAuthenticationSuccess] Error while retrive user photo,", e);
                    } catch (IOException e) {
                        logger.error("[CustomAuthenticationSuccessHandler.onAuthenticationSuccess] Error while retrive user photo, ", e);

                    }

                    if (readUserDetailsLogin != null) {
                        pathImage = readUserDetailsLogin.getPhotoUserUrl();
                        userDetailsLogin.setPhotoUserUrl(pathImage);
                    }

                    WebAuthenticationDetails webAuthenticationDetails=(WebAuthenticationDetails)authentication.getDetails();
                    String ipLogin=webAuthenticationDetails.getRemoteAddress();

                    try {
                        userBo.insertUserSessionLog(sessionId,userDetailsLogin,ipLogin);
                    } catch (HibernateException e) {
                        logger.error("[CustomAuthenticationSuccessHandler.onAuthenticationSuccess] Error while updating session log." ,e );
                    }

                    urlRedirect = request.getContextPath() + "/mainMenu.action";
                }

            } else {

                urlRedirect = request.getContextPath() + "/loginUser.action";

            }
        } else {

            urlRedirect = request.getContextPath() + "/loginUser.action";

        }

        response.sendRedirect(urlRedirect);

        logger.info("[CustomAuthenticationSuccessHandler.onAuthenticationSuccess] end process <<<");

    }
}
