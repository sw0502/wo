package com.neurix.authorization.user.action;

import com.neurix.authorization.company.bo.AreaBo;
import com.neurix.authorization.company.bo.BranchBo;
import com.neurix.authorization.company.model.Area;
import com.neurix.authorization.company.model.Branch;
import com.neurix.authorization.position.bo.PositionBo;
import com.neurix.authorization.position.model.Position;
import com.neurix.authorization.role.bo.RoleBo;
import com.neurix.authorization.role.model.Roles;
import com.neurix.authorization.user.bo.UserBo;
import com.neurix.authorization.user.model.User;
import com.neurix.common.action.BaseMasterAction;
import com.neurix.common.constant.CommonConstant;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Ferdi on 02/10/2014.
 */
public class UserAction extends BaseMasterAction {

    protected static transient Logger logger = Logger.getLogger(UserAction.class);

    private UserBo userBoProxy;
    private RoleBo roleBoProxy;
    private AreaBo areaBoProxy;
    private BranchBo branchBoProxy;
    private PositionBo positionBoProxy;

    private User users;
    private List<Roles> listOfComboRoles = new ArrayList<Roles>();
    private List<Area> listOfComboAreas = new ArrayList<Area>();
    private List<Branch> listOfComboBranches = new ArrayList<Branch>();
    private List<Position> listOfComboPositions = new ArrayList<Position>();
    private File fileUpload;
    private String userId;

    public void setAreaBoProxy(AreaBo areaBoProxy) {
        this.areaBoProxy = areaBoProxy;
    }

    public void setBranchBoProxy(BranchBo branchBoProxy) {
        this.branchBoProxy = branchBoProxy;
    }

    public List<Area> getListOfComboAreas() {
        return listOfComboAreas;
    }

    public void setListOfComboAreas(List<Area> listOfComboAreas) {
        this.listOfComboAreas = listOfComboAreas;
    }

    public List<Branch> getListOfComboBranches() {
        return listOfComboBranches;
    }

    public void setListOfComboBranches(List<Branch> listOfComboBranches) {
        this.listOfComboBranches = listOfComboBranches;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public void setUserBoProxy(UserBo userBoProxy) {
        this.userBoProxy = userBoProxy;
    }

    public void setRoleBoProxy(RoleBo roleBoProxy) {
        this.roleBoProxy = roleBoProxy;
    }

    public void setPositionBoProxy(PositionBo positionBoProxy) {
        this.positionBoProxy = positionBoProxy;
    }

    public User getUsers() {
        return users;
    }

    public void setUsers(User users) {
        this.users = users;
    }

    public List<Roles> getListOfComboRoles() {
        return listOfComboRoles;
    }

    public void setListOfComboRoles(List<Roles> listOfComboRoles) {
        this.listOfComboRoles = listOfComboRoles;
    }

    public List<Position> getListOfComboPositions() {
        return listOfComboPositions;
    }

    public void setListOfComboPositions(List<Position> listOfComboPositions) {
        this.listOfComboPositions = listOfComboPositions;
    }


    public String initComboRole() {

        Roles roles = new Roles();
        roles.setFlag("Y");

        List<Roles> listOfRoles = new ArrayList<Roles>();
        try {
            listOfRoles = roleBoProxy.getByCriteria(roles);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = roleBoProxy.saveErrorMessage(e.getMessage(), "RoleBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[UserAction.initComboRole] Error when saving error,", e1);
            }
            logger.error("[UserAction.initComboRole] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        listOfComboRoles.addAll(listOfRoles);

        return "init_combo_role";
    }

    public String initComboArea() {

        Area area = new Area();
        area.setFlag("Y");

        List<Area> listOfAreas = new ArrayList<Area>();
        try {
            listOfAreas = areaBoProxy.getByCriteria(area);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = areaBoProxy.saveErrorMessage(e.getMessage(), "AreaBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[UserAction.initComboArea] Error when saving error,", e1);
            }
            logger.error("[UserAction.initComboArea] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        listOfComboAreas.addAll(listOfAreas);

        return "init_combo_area";
    }

    public String initComboBranch() {

        Branch braches = new Branch();
        braches.setFlag("Y");

        List<Branch> listOfBranches = new ArrayList<Branch>();
        try {
            listOfBranches = branchBoProxy.getByCriteria(braches);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = branchBoProxy.saveErrorMessage(e.getMessage(), "BranchBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[UserAction.initComboBranch] Error when saving error,", e1);
            }
            logger.error("[UserAction.initComboBranch] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        listOfComboBranches.addAll(listOfBranches);

        return "init_combo_branch";
    }

    public String initComboPosition() {

        Position position = new Position();
        position.setFlag("Y");

        List<Position> listOfPosition = new ArrayList<Position>();
        try {
            listOfPosition = positionBoProxy.getByCriteria(position);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = positionBoProxy.saveErrorMessage(e.getMessage(), "PositionBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[UserAction.initComboPosition] Error when saving error,", e1);
            }
            logger.error("[UserAction.initComboPosition] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        listOfComboPositions.addAll(listOfPosition);

        return "init_combo_position";
    }

    @Override
    public String edit() {
        logger.info("[UserAction.edit] start process >>>");

        String itemId = getId();
        String itemFlag = getFlag();
        User editUser = new User();

        if (itemFlag != null && "Y".equalsIgnoreCase(itemFlag)) {

            HttpSession session = ServletActionContext.getRequest().getSession();
            List<User> listOfResult = (List<User>) session.getAttribute("listOfResult");

            for (User itemUser : listOfResult) {
                if (itemUser.getUserId().equalsIgnoreCase(itemId)) {
                    editUser.setUserId(itemUser.getUserId());
                    editUser.setUsername(itemUser.getUsername());
                    editUser.setPassword("");
                    editUser.setConfirmPassword("");
                    editUser.setEmail(itemUser.getEmail());
                    editUser.setPositionId(itemUser.getPositionId());
                    editUser.setRoleId(itemUser.getRoleId());
                    editUser.setBranchId(itemUser.getBranchId());
                    editUser.setAreaId(itemUser.getAreaId());
                    setUserId(itemUser.getUserId());
                    break;
                }
            }

        }

        setUsers(editUser);
        setAddOrEdit(true);
        setAdd(true);

        logger.info("[UserAction.edit] end process <<<");

        return "view_detail";
    }

    @Override
    public String add() {
        logger.info("[UserAction.add] start process >>>");

        User addUsers = new User();
        setUsers(addUsers);
        setAddOrEdit(true);
        setAdd(true);

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[UserAction.add] end process <<<");

        return "view_detail_add";
    }

    @Override
    public String delete() {
        logger.info("[UserAction.delete] start process >>>");

        String itemId = getId();
        String itemFlag = getFlag();
        User deleteUsers = new User();

        if (itemFlag != null && "Y".equalsIgnoreCase(itemFlag)) {

            HttpSession session = ServletActionContext.getRequest().getSession();
            List<User> listOfResult = (List<User>) session.getAttribute("listOfResult");

            for (User itemUser : listOfResult) {
                if (itemUser.getUserId().equalsIgnoreCase(itemId)) {
                    deleteUsers.setUserId(itemUser.getUserId());
                    deleteUsers.setUsername(itemUser.getUsername());
                    deleteUsers.setPassword(itemUser.getPassword());
                    deleteUsers.setConfirmPassword(itemUser.getPassword());
                    deleteUsers.setEmail(itemUser.getEmail());
                    deleteUsers.setPositionId(itemUser.getPositionId());
                    deleteUsers.setRoleId(itemUser.getRoleId());
                    deleteUsers.setBranchId(itemUser.getBranchId());
                    deleteUsers.setAreaId(itemUser.getAreaId());
                    setUserId(itemUser.getUserId());
                    break;
                }
            }

        }

        setUsers(deleteUsers);
        setDelete(true);

        logger.info("[UserAction.delete] end process <<<");

        return "view_detail";
    }

    @Override
    public String view() {
        logger.info("[UserAction.view] start process >>>");

        //get data from session
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<User> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (User userDetailsLogin : listOfResult) {
                    if (id.equalsIgnoreCase(userDetailsLogin.getUserId())) {
                        setUsers(userDetailsLogin);
                        break;
                    }
                }

            } else {
                setUsers(new User());
            }
        } else {
            setUsers(new User());
        }

        logger.info("[UserAction.view] end process <<<");

        return "view_detail";
    }

    @Override
    public String save() {
        logger.info("[UserAction.save] start process >>>");

        if (isAddOrEdit()) {

            if (!isAdd()) { //edit

                User editUser = getUsers();
                String rawPassword = editUser.getPassword();
//                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//                String hashedPassword = passwordEncoder.encode(rawPassword);

                ShaPasswordEncoder passwordEncoder = new ShaPasswordEncoder();
                String hashedPassword = passwordEncoder.encodePassword(rawPassword,null);

                editUser.setPassword(hashedPassword);

                String itemId = editUser.getUserId()!=null ? editUser.getUserId() : getUserId();
                if (itemId != null && !"".equalsIgnoreCase(itemId)) {

                    if (this.fileUpload!=null) {

                        //note : for linux directory
                        //String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD;

                        //note : for windows directory
                        String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD;
                        String fileName = itemId + ".jpg";
                        File fileToCreate = new File(filePath, fileName);

                        //create file to save to folder '/upload'
                        byte[] contentFile = null;
                        try {
                            FileUtils.copyFile(this.fileUpload, fileToCreate);
                            contentFile = FileUtils.readFileToByteArray(this.fileUpload);
                        } catch (IOException e) {
                            Long logId = null;
                            try {
                                logId = userBoProxy.saveErrorMessage(e.getMessage(), "UserAction.save");
                            } catch (GeneralBOException e1) {
                                logger.error("[UserAction.save] Error when saving error,", e1);
                            }
                            logger.error("[UserAction.save] Error when uploading and saving user," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
                            addActionError("Error, " + "[code=" + logId + "] Found problem when uploading and saving user, please inform to your admin. Cause : " + e.getMessage());
                            return ERROR;
                        }

                        if (contentFile!=null) {
                            editUser.setContentFile(contentFile);
                            editUser.setPhotoUserUrl(fileName);
                        }
                    }

                    try {

                        String userLogin = CommonUtil.userLogin();
                        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

                        editUser.setUserId(getUserId());
                        editUser.setLastUpdate(updateTime);
                        editUser.setLastUpdateWho(userLogin);
                        editUser.setAction("U");

                        userBoProxy.saveEdit(editUser);

                    } catch (UsernameNotFoundException e) {
                        logger.error("[UserAction.save] Error when editing item user,", e);
                        addActionError("Error, " + e.getMessage());
                        return ERROR;
                    } catch (GeneralBOException e) {
                        Long logId = null;
                        try {
                            logId = userBoProxy.saveErrorMessage(e.getMessage(), "UserBO.save");
                        } catch (GeneralBOException e1) {
                            logger.error("[UserAction.save] Error when saving error,", e1);
                        }
                        logger.error("[UserAction.save] Error when editing item user," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
                        addActionError("Error, " + "[code=" + logId + "] Found problem when saving edit data, please inform to your admin.");
                        return ERROR;
                    }
                }

                HttpSession session = ServletActionContext.getRequest().getSession();
                session.removeAttribute("listOfResult");

                return "success_save_edit";


            } else { //add

                User addUser = getUsers();
                String rawPassword = addUser.getPassword();
//                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//                String hashedPassword = passwordEncoder.encode(rawPassword);

                ShaPasswordEncoder passwordEncoder = new ShaPasswordEncoder();
                String hashedPassword = passwordEncoder.encodePassword(rawPassword,null);

                if (rawPassword.equalsIgnoreCase(addUser.getConfirmPassword())) {

                    addUser.setPassword(hashedPassword);

                    String itemId = addUser.getUserId();
                    if (itemId != null && !"".equalsIgnoreCase(itemId)) {

                        if (this.fileUpload!=null) {

                            //note : for linux directory
                            //String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD;

                            //note : for windows directory
                            String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD;
                            String fileName = itemId + ".jpg";
                            File fileToCreate = new File(filePath, fileName);

                            //create file to save to folder '/upload'
                            byte[] contentFile = null;
                            try {
                                FileUtils.copyFile(this.fileUpload, fileToCreate);
                                contentFile = FileUtils.readFileToByteArray(this.fileUpload);
                            } catch (IOException e) {
                                Long logId = null;
                                try {
                                    logId = userBoProxy.saveErrorMessage(e.getMessage(), "UserAction.save");
                                } catch (GeneralBOException e1) {
                                    logger.error("[UserAction.save] Error when saving error,", e1);
                                }
                                logger.error("[UserAction.save] Error when uploading and saving user," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
                                addActionError("Error, " + "[code=" + logId + "] Found problem when uploading and saving user, please inform to your admin. Cause : " + e.getMessage());
                                return ERROR;
                            }

                            if (contentFile!=null) {
                                addUser.setContentFile(contentFile);
                                addUser.setPhotoUserUrl(fileName);
                            }
                        }

                        try {

                            String userLogin = CommonUtil.userLogin();
                            Timestamp createTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

                            addUser.setCreatedDate(createTime);
                            addUser.setCreatedWho(userLogin);
                            addUser.setLastUpdate(createTime);
                            addUser.setLastUpdateWho(userLogin);
                            addUser.setAction("C");

                            userBoProxy.saveAdd(addUser);

                        } catch (UsernameNotFoundException e) {
                            logger.error("[UserAction.save] Error when adding item user,", e);
                            addActionError("Error, " + e.getMessage());
                            return ERROR;
                        } catch (GeneralBOException e) {
                            Long logId = null;
                            try {
                                logId = userBoProxy.saveErrorMessage(e.getMessage(), "UserBO.saveAdd");
                            } catch (GeneralBOException e1) {
                                logger.error("[UserAction.save] Error when saving error,", e1);
                            }
                            logger.error("[UserAction.save] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
                            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.");
                            return ERROR;
                        }
                    }

                } else {

                    addActionError("Please checked your password not same with confirm password.");
                    return ERROR;

                }

                HttpSession session = ServletActionContext.getRequest().getSession();
                session.removeAttribute("listOfResult");

                return "success_save_add";
            }

        } else if (isDelete()) {

            try {
                String userLogin = CommonUtil.userLogin();
                Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

                User deleteUsers = getUsers()!=null ? getUsers() : new User();

                if (getUsers()==null) deleteUsers.setUserId(getUserId());
                deleteUsers.setLastUpdate(updateTime);
                deleteUsers.setLastUpdateWho(userLogin);
                deleteUsers.setAction("D");

                userBoProxy.saveDelete(deleteUsers);
                addActionMessage("Save Delete is success.");

            } catch (UsernameNotFoundException e) {
                logger.error("[UserAction.save] Error when deleting item,", e);
                addActionError("Error, " + e.getMessage());
                return ERROR;
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = userBoProxy.saveErrorMessage(e.getMessage(), "UserBO.saveDelete");
                } catch (GeneralBOException e1) {
                    logger.error("[UserAction.save] Error when saving error,", e1);
                }
                logger.error("[UserAction.save] Error when deleting item ," + "[" + logId + "] Found problem when saving delete data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when saving delete data, please inform to your admin.");
                return ERROR;
            }

            HttpSession session = ServletActionContext.getRequest().getSession();
            session.removeAttribute("listOfResult");

            return "success_save_delete";

        }

        logger.info("[UserAction.save] end process <<<");

        return SUCCESS;
    }

    @Override
    public String initForm() {

        clearMessages();
        clearActionErrors();
        User initUsers = new User();
        setUsers(initUsers);
        setDelete(false);
        setAddOrEdit(false);
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        return INPUT;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }

    @Override
    public String search() {
        logger.info("[UserAction.search] start process >>>");

        User searchUsers = getUsers();
        List<User> listOfSearchUsers = new ArrayList();
        try {
            listOfSearchUsers = userBoProxy.getByCriteria(searchUsers);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = userBoProxy.saveErrorMessage(e.getMessage(), "UserBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[UserAction.search] Error when saving error,", e1);
            }
            logger.error("[UserAction.search] Error when searching user by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfSearchUsers);

        logger.info("[UserAction.search] end process <<<");

        return SUCCESS;
    }

    public List initComboUser(String query) {
        logger.info("[UserAction.initComboUser] start process >>>");

        List<User> listOfUser = new ArrayList();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        UserBo userBo = (UserBo) ctx.getBean("userBoProxy");

        try {
            listOfUser = userBo.getComboUserWithCriteria(query);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = userBo.saveErrorMessage(e.getMessage(), "UserBO.getComboUserWithCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[UserAction.initComboUser] Error when saving error,", e1);
            }
            logger.error("[UserAction.initComboUser] Error when get combo User," + "[" + logId + "] Found problem when retrieving combo User data, please inform to your admin.", e);
        }

        logger.info("[UserAction.initComboUser] end process <<<");

        return listOfUser;
    }

    public String initChangeNewPasswordForm() {

        clearMessages();
        clearActionErrors();
        User initUsers = new User();
        initUsers.setUserId(CommonUtil.userIdLogin());
        initUsers.setUsername(CommonUtil.userLogin());
        setUsers(initUsers);
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        return "init_change_password";
    }

    public String saveNewPassword() {

        logger.info("[UserAction.saveNewPassword] end process <<<");

        User addUser = getUsers();
        String rawPassword = addUser.getPassword();
//        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//        String hashedPassword = passwordEncoder.encode(rawPassword);

        ShaPasswordEncoder passwordEncoder = new ShaPasswordEncoder();
        String hashedPassword = passwordEncoder.encodePassword(rawPassword,null);


        if (rawPassword.equalsIgnoreCase(addUser.getConfirmPassword())) {

            addUser.setPassword(hashedPassword);

            String itemId = addUser.getUserId() != null ? addUser.getUserId() : CommonUtil.userIdLogin();

            if (itemId != null && !"".equalsIgnoreCase(itemId)) {

                if (this.fileUpload!=null) {

                    //note : for linux directory
                    //String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD;

                    //note : for windows directory
                    String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD;
                    String fileName = itemId + ".jpg";
                    File fileToCreate = new File(filePath, fileName);

                    //create file to save to folder '/upload'
                    byte[] contentFile = null;
                    try {
                        FileUtils.copyFile(this.fileUpload, fileToCreate);
                        contentFile = FileUtils.readFileToByteArray(this.fileUpload);
                    } catch (IOException e) {
                        Long logId = null;
                        try {
                            logId = userBoProxy.saveErrorMessage(e.getMessage(), "UserAction.saveNewPassword");
                        } catch (GeneralBOException e1) {
                            logger.error("[UserAction.saveNewPassword] Error when saving error,", e1);
                        }
                        logger.error("[UserAction.saveNewPassword] Error when uploading and saving user," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
                        addActionError("Error, " + "[code=" + logId + "] Found problem when uploading and saving user, please inform to your admin. Cause : " + e.getMessage());
                        return ERROR;
                    }

                    if (contentFile!=null) {
                        addUser.setContentFile(contentFile);
                        addUser.setPhotoUserUrl(fileName);
                    }
                }

                try {

                    String userLogin = CommonUtil.userLogin();
                    Timestamp createTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

                    if (addUser.getUserId()==null) addUser.setUserId(itemId);
                    addUser.setCreatedDate(createTime);
                    addUser.setCreatedWho(userLogin);
                    addUser.setLastUpdate(createTime);
                    addUser.setLastUpdateWho(userLogin);
                    addUser.setAction("U");

                    userBoProxy.saveEditPassword(addUser);

                } catch (UsernameNotFoundException e) {
                    logger.error("[UserAction.saveNewPassword] Error when adding item user,", e);
                    addActionError("Error, " + e.getMessage());
                    return ERROR;
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = userBoProxy.saveErrorMessage(e.getMessage(), "UserBO.saveNewPassword");
                    } catch (GeneralBOException e1) {
                        logger.error("[UserAction.saveNewPassword] Error when saving error,", e1);
                    }
                    logger.error("[UserAction.saveNewPassword] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.");
                    return ERROR;
                }
            }

        } else {
            addActionError("Please checked your password not same with confirm password.");
            return ERROR;
        }

        logger.info("[UserAction.saveNewPassword] end process <<<");

        return "success_save_newpassword";
    }
}