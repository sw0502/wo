package com.neurix.authorization.position.model;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;

/**
 * Created by Ferdi on 02/02/2015.
 */
public class Position extends BaseModel implements Serializable, Comparable<Position> {
    private Long positionId;
    private String positionName;
    private String stPositionId;

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getStPositionId() {
        return stPositionId;
    }

    public void setStPositionId(String stPositionId) {
        this.stPositionId = stPositionId;
    }

    @Override
    public int compareTo(Position o) {
        return this.positionName.toLowerCase().compareTo(o.getPositionName().toLowerCase());
    }
}
