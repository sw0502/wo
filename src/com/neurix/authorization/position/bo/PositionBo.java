package com.neurix.authorization.position.bo;

import com.neurix.authorization.position.model.Position;
import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;

import java.util.List;

/**
 * Created by Ferdi on 02/02/2015.
 */
public interface PositionBo extends BaseMasterBo<Position> {

    public Position getPositionById(long positionId, String flag) throws GeneralBOException;
    public List<Position> getComboPositionWithCriteria(String query) throws GeneralBOException;

}