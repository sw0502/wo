package com.neurix.authorization.position.bo.impl;

import com.neurix.authorization.position.bo.PositionBo;
import com.neurix.authorization.position.dao.PositionDao;
import com.neurix.authorization.position.model.ImPosition;
import com.neurix.authorization.position.model.ImPositionHistory;
import com.neurix.authorization.position.model.Position;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.exception.GenerateBoLog;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Created by Ferdi on 02/02/2015.
 */
public class PositionBoImpl implements PositionBo {

    protected static transient Logger logger = Logger.getLogger(PositionBoImpl.class);
    private PositionDao positionDao;

    public void setPositionDao(PositionDao positionDao) {
        this.positionDao = positionDao;
    }

    public List<Position> getAll() throws GeneralBOException {

        logger.info("[PositionBoImpl.getAll] start process >>>");

        List<Position> listOfResultPosition = new ArrayList();
        List<ImPosition> listOfPosition = null;
        try {
            listOfPosition = positionDao.getAll();
        } catch (HibernateException e) {
            logger.error("[PositionBoImpl.getAll] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting all data, please info to your admin..." + e.getMessage());
        }

        if ( listOfPosition != null) {
            Position resultPosition;
            for (ImPosition imPosition : listOfPosition) {
                resultPosition = new Position();

                try {
                    BeanUtils.copyProperties(resultPosition, imPosition);
                } catch (IllegalAccessException e) {
                    logger.error("[PositionBoImpl.getAll] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when coping saving ImPosition to resultPositions at getting all data, please info to your admin..." + e.getMessage());
                } catch (InvocationTargetException e) {
                    logger.error("[PositionBoImpl.getAll] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when coping saving ImPosition to resultPositions at getting all data, please info to your admin..." + e.getMessage());
                }

                resultPosition.setFlag(imPosition.getFlag());
                listOfResultPosition.add(resultPosition);
            }
        }

        logger.info("[PositionBoImpl.getAll] end process <<<");

        return listOfResultPosition;
    }

    public List<Position> getByCriteria(Position searchPosition) throws GeneralBOException {

        logger.info("[PositionBoImpl.getByCriteria] start process >>>");

        List<Position> listOfResultPosition = new ArrayList();

        if (searchPosition != null) {
            Map hsCriteria = new HashMap();
            if (searchPosition.getStPositionId() != null && !"".equalsIgnoreCase(searchPosition.getStPositionId())) {
                hsCriteria.put("position_id", Long.parseLong(searchPosition.getStPositionId()));
            }

            if (searchPosition.getPositionName() != null && !"".equalsIgnoreCase(searchPosition.getPositionName())) {
                hsCriteria.put("position_name", searchPosition.getPositionName());
            }

            if (searchPosition.getFlag() != null && !"".equalsIgnoreCase(searchPosition.getFlag())) {
                if ("N".equalsIgnoreCase(searchPosition.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchPosition.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            List<ImPosition> listOfPosition = null;
            try {
                listOfPosition = positionDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[PositionBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (listOfPosition != null) {
                Position resultPosition;
                for (ImPosition imPosition : listOfPosition) {
                    resultPosition = new Position();

                    resultPosition.setPositionId(imPosition.getPositionId());
                    resultPosition.setStPositionId(imPosition.getPositionId().toString());
                    resultPosition.setPositionName(imPosition.getPositionName());
                    resultPosition.setAction(imPosition.getAction());
                    resultPosition.setCreatedDate(imPosition.getCreatedDate());
                    resultPosition.setCreatedWho(imPosition.getCreatedWho());
                    resultPosition.setLastUpdate(imPosition.getLastUpdate());
                    resultPosition.setLastUpdateWho(imPosition.getLastUpdateWho());
                    resultPosition.setFlag(imPosition.getFlag());

                    listOfResultPosition.add(resultPosition);
                }
            }

        }

        logger.info("[PositionBoImpl.getByCriteria] end process <<<");

        return listOfResultPosition;
    }

    public Position saveAdd(Position position) throws GeneralBOException {

        logger.info("[PositionBoImpl.saveAdd] start process >>>");

        if (position != null) {
            ImPosition imPosition = new ImPosition();

            try {
                BeanUtils.copyProperties(imPosition, position);
            } catch (IllegalAccessException e) {
                logger.error("[PositionBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when coping addposition to ImPosition, please info to your admin..." + e.getMessage());
            } catch (InvocationTargetException e) {
                logger.error("[PositionBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when coping addposition to ImPosition, please info to your admin..." + e.getMessage());
            }

            imPosition.setFlag("Y");

            try {
                positionDao.addAndSave(imPosition);
            } catch (HibernateException e) {
                logger.error("[PositionBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data position, please info to your admin..." + e.getMessage());
            }

        }

        logger.info("[PositionBoImpl.saveAdd] end process <<<");

        return position;
    }

    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {

        Long result = GenerateBoLog.generateBoLog(positionDao, message, moduleMethod);

        return result;
    }

    public void saveEdit(Position positionNew) throws GeneralBOException {

        logger.info("[PositionBoImpl.saveEdit] start process >>>");

        if (positionNew != null) {

            //copy new data to model tabel
            ImPosition imPositionNew = new ImPosition();
            try {
                BeanUtils.copyProperties(imPositionNew, positionNew);
            } catch (IllegalAccessException e) {
                logger.error("[PositionBoImpl.saveEdit] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when coping data object positionNew to ImPositionNew, please info to your admin..." + e.getMessage());
            } catch (InvocationTargetException e) {
                logger.error("[PositionBoImpl.saveEdit] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when coping data object positionNew to ImPositionNew, please info to your admin..." + e.getMessage());
            }

            //retrieve last data by id
            long positionId = positionNew.getPositionId();

            ImPosition imPositionOld = null;
            try {
                imPositionOld = positionDao.getById("positionId",positionId, "Y");
            } catch (HibernateException e) {
                logger.error("[PositionBoImpl.saveEdit] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data position by id, please inform to your admin...," + e.getMessage());
            }

            if (imPositionOld != null) {

                // move last data to table history
                ImPositionHistory imPositionDeactive = new ImPositionHistory();
                try {
                    BeanUtils.copyProperties(imPositionDeactive, imPositionOld);
                } catch (IllegalAccessException e) {
                    logger.error("[PositionBoImpl.saveEdit] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when coping data object positionOld to ImPositionBeforeDeactive, please info to your admin..." + e.getMessage());
                } catch (InvocationTargetException e) {
                    logger.error("[PositionBoImpl.saveEdit] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when coping data object positionOld to ImPositionBeforeDeactive, please info to your admin..." + e.getMessage());
                }

                try {
                    positionDao.addAndSaveHistory(imPositionDeactive);
                } catch (HibernateException e) {
                    logger.error("[PositionBoImpl.saveEdit] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving deactive data role, please info to your admin..." + e.getMessage());
                }

                //update some of last data become new data
                imPositionNew.setPositionId(positionId);
                imPositionNew.setCreatedDate(imPositionOld.getCreatedDate());
                imPositionNew.setCreatedWho(imPositionOld.getCreatedWho());
                imPositionNew.setFlag(imPositionOld.getFlag());

                ImPosition imPositionActive = (ImPosition) positionDao.getSessionFactory().getCurrentSession().merge(imPositionNew);

                try {
                    positionDao.updateAndSave(imPositionActive);
                } catch (HibernateException e) {
                    logger.error("[PositionBoImpl.saveEdit] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving updated data position, please inform to your admin...," + e.getMessage());
                }

            } else {
                logger.error("[PositionBoImpl.saveEdit] Unable to save edit cause no found role key.");
                throw new GeneralBOException("Found problem when saving edit data position cause no found role key., please info to your admin...");
            }
        }

        logger.info("[PositionBoImpl.saveEdit] end process <<<");
    }

    public void saveDelete(Position position) throws GeneralBOException {

        logger.info("[PositionBoImpl.saveDelete] start process >>>");

        if (position != null) {

            long positionId = position.getPositionId();

            ImPosition imPositionOld = null;
            try {
                imPositionOld = positionDao.getById("positionId",positionId, "Y");
            } catch (HibernateException e) {
                logger.error("[PositionBoImpl.saveDelete] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving delete data position, please info to your admin..." + e.getMessage());
            }

            if (imPositionOld != null) {

                Set listOfImUsers = imPositionOld.getImUserses();

                if (listOfImUsers.size() == 0) {

                    ImPosition imPositionToDeactive = new ImPosition();

                    try {
                        BeanUtils.copyProperties(imPositionToDeactive, imPositionOld);
                    } catch (IllegalAccessException e) {
                        logger.error("[PositionBoImpl.saveDelete] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when coping data object position Will be Delete to ImPositionBeforeDeactive, please info to your admin..." + e.getMessage());
                    } catch (InvocationTargetException e) {
                        logger.error("[PositionBoImpl.saveDelete] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when coping data object position  Will be Delete to ImPositionBeforeDeactive, please info to your admin..." + e.getMessage());
                    }

                    //update data with flag=N
                    imPositionToDeactive.setFlag("N");
                    imPositionToDeactive.setAction(position.getAction());
                    imPositionToDeactive.setLastUpdate(position.getLastUpdate());
                    imPositionToDeactive.setLastUpdateWho(position.getLastUpdateWho());

                    ImPosition imPositionDeactive = (ImPosition) positionDao.getSessionFactory().getCurrentSession().merge(imPositionToDeactive);

                    try {
                        positionDao.updateAndSave(imPositionDeactive);
                    } catch (HibernateException e) {
                        logger.error("[PositionBoImpl.saveDelete] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when saving delete data position, please info to your admin..." + e.getMessage());
                    }

                } else {
                    logger.error("[PositionBoImpl.saveDelete] Unable to delete cause have reference data exist in user table.");
                    throw new GeneralBOException("Found problem when saving delete data role cause have reference data exist in user table, please info to your admin...");
                }
            } else {
                logger.error("[PositionBoImpl.saveDelete] Unable to delete cause no found position key.");
                throw new GeneralBOException("Found problem when saving delete data role cause no found position key., please info to your admin...");
            }
        }

        logger.info("[PositionBoImpl.saveDelete] end process <<<");
    }

    public Position getPositionById(long positionId, String flag) throws GeneralBOException {

        logger.info("[PositionBoImpl.getPositionById] start process >>>");

        String getFlag = "";
        if (flag != null && !"".equalsIgnoreCase(flag)) {
            if (flag.equalsIgnoreCase("")) getFlag = "Y";
            else getFlag = flag;
        } else {
            getFlag = "Y";
        }

        ImPosition imPosition = null;
        try {
            imPosition = positionDao.getById("positionId",(Long)positionId,getFlag);
        } catch (HibernateException e) {
            logger.error("[PositionBoImpl.getPositionById] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when retrieving position based on id and flag, please info to your admin..." + e.getMessage());
        }

        Position resultPosition = new Position();
        if (imPosition != null) {

            try {
                BeanUtils.copyProperties(resultPosition, imPosition);
            } catch (IllegalAccessException e) {
                logger.error("[PositionBoImpl.getPositionById] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when coping search ImPosition to result position to display , please info to your admin..." + e.getMessage());
            } catch (InvocationTargetException e) {
                logger.error("[PositionBoImpl.getPositionById] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when coping search ImPosition to result position to display , please info to your admin..." + e.getMessage());
            }

            resultPosition.setStPositionId(imPosition.getPositionId().toString());
            resultPosition.setFlag(imPosition.getFlag());
        }

        logger.info("[PositionBoImpl.getPositionById] end process <<<");

        return resultPosition;
    }

    public List<Position> getComboPositionWithCriteria(String query) throws GeneralBOException {
        logger.info("[PositionBoImpl.getComboPositionWithCriteria] start process >>>");

        List<Position> listComboPosition = new ArrayList();
        String criteria = "%" + query + "%";

        List<ImPosition> listPosition = null;
        try {
            listPosition = positionDao.getListPosition(criteria);
        } catch (HibernateException e) {
            logger.error("[PositionBoImpl.getComboPositionWithCriteria] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when retieving list position with criteria, please info to your admin..." + e.getMessage());
        }

        if (listPosition != null) {
            for (ImPosition imPosition : listPosition) {
                Position itemComboRoles = new Position();
                itemComboRoles.setStPositionId(imPosition.getPositionId().toString());
                itemComboRoles.setPositionName(imPosition.getPositionName());
                listComboPosition.add(itemComboRoles);
            }
        }
        logger.info("[PositionBoImpl.getComboPositionWithCriteria] end process <<<");
        return listComboPosition;
    }
}
