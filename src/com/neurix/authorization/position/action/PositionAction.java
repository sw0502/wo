package com.neurix.authorization.position.action;

import com.neurix.authorization.position.bo.PositionBo;
import com.neurix.authorization.position.model.Position;
import com.neurix.common.action.BaseMasterAction;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Ferdi on 02/02/2015.
 */
public class PositionAction extends BaseMasterAction {

    protected static transient Logger logger = Logger.getLogger(PositionAction.class);

    private PositionBo positionBoProxy;
    private List<Position> positionList;
    private Position position;

    public void setPositionBoProxy(PositionBo positionBoProxy) {
        this.positionBoProxy = positionBoProxy;
    }

    public List<Position> getPositionList() {
        return positionList;
    }

    public void setPositionList(List<Position> positionList) {
        this.positionList = positionList;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    private Position init(String id, String flag) throws NumberFormatException, GeneralBOException {

        logger.info("[PositionAction.init] start process >>>");

        Position initPosition = new Position();
        if (id != null && !"".equalsIgnoreCase(id)) {
            long lId = Long.parseLong(id);
            initPosition = positionBoProxy.getPositionById(lId, flag);
        }

        logger.info("[PositionAction.init] end process <<<");

        return initPosition;
    }

    @Override
    public String edit() {

        logger.info("[PositionAction.edit] start process >>>");

        String itemId = getId();
        String itemFlag = getFlag();
        Position editPosition = new Position();

        if (itemFlag != null && "Y".equalsIgnoreCase(itemFlag)) {

            try {
                editPosition = init(itemId, itemFlag);
            } catch (NumberFormatException e) {
                logger.error("[PositionAction.edit] error parsing long.", e);
                addFieldError("Position.positionId", "Position Id must number.");
                return "failure";
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = positionBoProxy.saveErrorMessage(e.getMessage(), "PositionBO.getPositionById");
                } catch (GeneralBOException e1) {
                    logger.error("[PositionAction.edit] Error when retrieving edit data,", e1);
                }
                logger.error("[PositionAction.edit] Error when retrieving item," + "[" + logId + "] Found problem when retrieving data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when retrieving data for edit, please inform to your admin.");
                return "failure";
            }

            if (editPosition != null) {
                if (editPosition.getFlag().equalsIgnoreCase("Y")) {
                    setAddOrEdit(true);
                    setPosition(editPosition);

                    HttpSession session = ServletActionContext.getRequest().getSession();
                    session.removeAttribute("listOfResult");

                } else {
                    editPosition.setStPositionId(itemId);
                    editPosition.setFlag(itemFlag);
                    setPosition(editPosition);
                    addActionError("Error, Unable to edit again with flag = N.");
                    return "failure";
                }
            } else {
                editPosition.setStPositionId(itemId);
                editPosition.setFlag(itemFlag);
                setPosition(editPosition);
                addActionError("Error, Unable to find data with id = " + itemId);
                return "failure";
            }
        } else {
            editPosition.setStPositionId(itemId);
            editPosition.setFlag(itemFlag);
            setPosition(editPosition);
            addActionError("Error, Unable to edit again with flag = N.");
            return "failure";
        }

        logger.info("[PositionAction.edit] end process <<<");

        return INPUT;
    }

    @Override
    public String add() {
        logger.info("[PositionAction.add] start process >>>");
        Position addPosition = new Position();
        setPosition(addPosition);
        setAddOrEdit(true);
        setAdd(true);

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[PositionAction.add] end process <<<");
        return INPUT;
    }

    @Override
    public String delete() {
        logger.info("[PositionAction.delete] start process >>>");

        String itemId = getId();
        String itemFlag = getFlag();
        Position deletePosition = new Position();

        if (itemFlag != null && "Y".equalsIgnoreCase(itemFlag) ) {

            try {
                deletePosition = init(itemId, itemFlag);
            } catch (NumberFormatException e) {
                logger.error("[PositionAction.delete] error parsing long.", e);
                addFieldError("Position.positionId", "Position Id must number.");
                return "failure";
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = positionBoProxy.saveErrorMessage(e.getMessage(), "PositionBO.getPositionById");
                } catch (GeneralBOException e1) {
                    logger.error("[PositionAction.delete] Error when retrieving delete data,", e1);
                }
                logger.error("[PositionAction.delete] Error when retrieving item," + "[" + logId + "] Found problem when retrieving data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when retrieving data for delete, please inform to your admin.");
                return "failure";
            }

            if (deletePosition != null) {
                if (deletePosition.getFlag().equalsIgnoreCase("Y")) {
                    setDelete(true);
                    setPosition(deletePosition);

                    HttpSession session = ServletActionContext.getRequest().getSession();
                    session.removeAttribute("listOfResult");

                } else {
                    deletePosition.setStPositionId(itemId);
                    deletePosition.setFlag(itemFlag);
                    setPosition(deletePosition);
                    addActionError("Error, Unable to delete again with flag = N.");
                    return "failure";
                }
            } else {
                deletePosition.setStPositionId(itemId);
                deletePosition.setFlag(itemFlag);
                setPosition(deletePosition);
                addActionError("Error, Unable to find data with id = " + itemId);
                return "failure";
            }
        } else {
            deletePosition.setStPositionId(itemId);
            deletePosition.setFlag(itemFlag);
            setPosition(deletePosition);
            addActionError("Error, Unable to delete again with flag = N.");
            return "failure";
        }

        logger.info("[PositionAction.delete] end process <<<");

        return INPUT;
    }

    @Override
    public String view() {

        return INPUT;
    }

    @Override
    public String save() {
        logger.info("[PositionAction.save] start process >>>");

        if (isAddOrEdit()) {

            if (!isAdd()) {
                String itemId = getPosition().getStPositionId();
                if (itemId != null && !"".equalsIgnoreCase(itemId)) {

                    //edit
                    try {

                        String userLogin = CommonUtil.userLogin();
                        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

                        Position editPosition = getPosition();
                        Long lItemId = Long.parseLong(itemId);
                        editPosition.setPositionId(lItemId);
                        editPosition.setCreatedDate(updateTime);
                        editPosition.setCreatedWho(userLogin);
                        editPosition.setLastUpdate(updateTime);
                        editPosition.setLastUpdateWho(userLogin);
                        editPosition.setAction("U");

                        positionBoProxy.saveEdit(editPosition);

                    } catch (NumberFormatException e) {
                        logger.error("[PositionAction.save] Error when editing item position,", e);
                        addActionError("Error, " + "Position Id must number.");
                        return ERROR;
                    } catch (UsernameNotFoundException e) {
                        logger.error("[PositionAction.save] Error when editing item position,", e);
                        addActionError("Error, " + e.getMessage());
                        return ERROR;
                    } catch (GeneralBOException e) {
                        Long logId = null;
                        try {
                            logId = positionBoProxy.saveErrorMessage(e.getMessage(), "PositionBO.save");
                        } catch (GeneralBOException e1) {
                            logger.error("[PositionAction.save] Error when saving error,", e1);
                        }
                        logger.error("[PositionAction.save] Error when editing item position," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
                        addActionError("Error, " + "[code=" + logId + "] Found problem when saving edit data, please inform to your admin.\n" + e.getMessage());
                        return ERROR;
                    }
                }
            } else {
                //add
                try {
                    String userLogin = CommonUtil.userLogin();
                    Timestamp createTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

                    Position entryPosition = getPosition();

                    entryPosition.setCreatedDate(createTime);
                    entryPosition.setCreatedWho(userLogin);
                    entryPosition.setLastUpdate(createTime);
                    entryPosition.setLastUpdateWho(userLogin);
                    entryPosition.setAction("C");

                    positionBoProxy.saveAdd(entryPosition);

                } catch (UsernameNotFoundException e) {
                    logger.error("[PositionAction.save] Error when adding item position,", e);
                    addActionError("Error, " + e.getMessage());
                    return ERROR;
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = positionBoProxy.saveErrorMessage(e.getMessage(), "PositionBO.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[PositionAction.save] Error when saving error,", e1);
                    }
                    logger.error("[PositionAction.save] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
                    return ERROR;
                }

            }

        } else if (isDelete()) {

            try {
                String userLogin = CommonUtil.userLogin();
                Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

                Position deletePosition = getPosition();

                if (deletePosition.getStPositionId() != null && !"".equalsIgnoreCase(deletePosition.getStPositionId()))
                    deletePosition.setPositionId(Long.parseLong(deletePosition.getStPositionId()));

                deletePosition.setLastUpdate(updateTime);
                deletePosition.setLastUpdateWho(userLogin);
                deletePosition.setAction("D");

                positionBoProxy.saveDelete(deletePosition);

            } catch (UsernameNotFoundException e) {
                logger.error("[PositionAction.save] Error when deleting item position,", e);
                addActionError("Error, " + e.getMessage());
                return ERROR;
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = positionBoProxy.saveErrorMessage(e.getMessage(), "PositionBO.saveDelete");
                } catch (GeneralBOException e1) {
                    logger.error("[PositionAction.save] Error when saving error,", e1);
                }
                logger.error("[PositionAction.save] Error when deleting item ," + "[" + logId + "] Found problem when saving delete data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when saving delete data, please inform to your admin.\n" + e.getMessage());
                return ERROR;
            }

        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[PositionAction.save] end process <<<");

        return "success_save";
    }

    @Override
    public String initForm() {

        clearMessages();
        clearActionErrors();
        Position initPosition = new Position();
        setPosition(initPosition);
        setDelete(false);
        setAdd(false);
        setAddOrEdit(false);
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        return INPUT;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }

    @Override
    public String search() {
        logger.info("[PositionAction.search] start process >>>");

        Position searchPosition = getPosition();
        List<Position> listOfSearchPosition = new ArrayList();
        try {
            listOfSearchPosition = positionBoProxy.getByCriteria(searchPosition);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = positionBoProxy.saveErrorMessage(e.getMessage(), "PositionBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PositionAction.search] Error when saving error,", e1);
            }
            logger.error("[PositionAction.save] Error when searching position by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin" );
            return "failure";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfSearchPosition);

        logger.info("[PositionAction.search] end process <<<");

        return SUCCESS;
    }

    public List initComboPosition(String query) {
        logger.info("[PositionAction.initComboPosition] start process >>>");

        List<Position> listOfPosition = new ArrayList();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        PositionBo positionBo = (PositionBo) ctx.getBean("roleBoProxy");

        try {
            listOfPosition = positionBo.getComboPositionWithCriteria(query);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = positionBo.saveErrorMessage(e.getMessage(), "PositionBO.getComboPositionWithCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PositionAction.initComboPosition] Error when saving error,", e1);
            }
            logger.error("[PositionAction.initComboPosition] Error when get combo position," + "[" + logId + "] Found problem when retrieving combo position data, please inform to your admin.", e);
        }

        logger.info("[PositionAction.initComboPosition] end process <<<");

        return listOfPosition;
    }
}
