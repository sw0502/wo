package com.neurix.authorization.position.dao;

import com.neurix.authorization.position.model.ImPosition;
import com.neurix.authorization.position.model.ImPositionHistory;
import com.neurix.common.dao.GenericDao;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Thunderbird
 * Date: 20/01/13
 * Time: 0:19
 * To change this template use File | Settings | File Templates.
 */
public class PositionDao extends GenericDao<ImPosition,Long> {

    @Override
    protected Class getEntityClass() {
        return ImPosition.class;
    }

    @Override
    public List<ImPosition> getByCriteria(Map mapCriteria) throws HibernateException {

        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImPosition.class);

        if (mapCriteria!=null) {
            if (mapCriteria.get("position_id")!=null) {
                criteria.add(Restrictions.ilike("positionId", (Long) mapCriteria.get("position_id")));
            }
            if (mapCriteria.get("position_name")!=null) {
                criteria.add(Restrictions.ilike("positionName", "%" + (String)mapCriteria.get("position_name") + "%" ));
            }
        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.asc("positionId"));

        List<ImPosition> results = criteria.list();

        return results;
    }

    public List<ImPosition> getListPosition(String term) throws HibernateException {

        List<ImPosition> results = this.sessionFactory.getCurrentSession().createCriteria(ImPosition.class)
                .add(Restrictions.ilike("positionName",term))
                .add(Restrictions.eq("flag", "Y"))
                .addOrder(Order.asc("positionId"))
                .list();

        return results;
    }


    public void addAndSaveHistory(ImPositionHistory entity) throws HibernateException {
        this.sessionFactory.getCurrentSession().save(entity);

    }
}