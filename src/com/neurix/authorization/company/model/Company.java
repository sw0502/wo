package com.neurix.authorization.company.model;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Ferdi on 05/02/2015.
 */
public class Company extends BaseModel implements Serializable {
    private String companyId;
    private String companyName;
    private String address;
    private String npwp;
    private String serviceOnOff;
    private String mailServer;
    private String portServer;
    private String userNameServer;
    private String passwordServer;
    private String defaultEmailSender;
    private String defaultEmailSubject;
    private String defaultEmailContent;
    private BigDecimal minimumLuasan;
    private String stMinimumLuasan;

    public String getStMinimumLuasan() {
        return stMinimumLuasan;
    }

    public void setStMinimumLuasan(String stMinimumLuasan) {
        this.stMinimumLuasan = stMinimumLuasan;
    }

    public BigDecimal getMinimumLuasan() {
        return minimumLuasan;
    }

    public void setMinimumLuasan(BigDecimal minimumLuasan) {
        this.minimumLuasan = minimumLuasan;
    }

    public String getCompanyId() {

        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getServiceOnOff() {
        return serviceOnOff;
    }

    public void setServiceOnOff(String serviceOnOff) {
        this.serviceOnOff = serviceOnOff;
    }

    public String getMailServer() {
        return mailServer;
    }

    public void setMailServer(String mailServer) {
        this.mailServer = mailServer;
    }

    public String getPortServer() {
        return portServer;
    }

    public void setPortServer(String portServer) {
        this.portServer = portServer;
    }

    public String getUserNameServer() {
        return userNameServer;
    }

    public void setUserNameServer(String userNameServer) {
        this.userNameServer = userNameServer;
    }

    public String getPasswordServer() {
        return passwordServer;
    }

    public void setPasswordServer(String passwordServer) {
        this.passwordServer = passwordServer;
    }

    public String getDefaultEmailSender() {
        return defaultEmailSender;
    }

    public void setDefaultEmailSender(String defaultEmailSender) {
        this.defaultEmailSender = defaultEmailSender;
    }

    public String getDefaultEmailSubject() {
        return defaultEmailSubject;
    }

    public void setDefaultEmailSubject(String defaultEmailSubject) {
        this.defaultEmailSubject = defaultEmailSubject;
    }

    public String getDefaultEmailContent() {
        return defaultEmailContent;
    }

    public void setDefaultEmailContent(String defaultEmailContent) {
        this.defaultEmailContent = defaultEmailContent;
    }
}
