package com.neurix.pmsapb.alatapb.alatdoc.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetail;
import com.neurix.pmsapb.alatapb.alatdoc.model.AlatDoc;
import com.neurix.pmsapb.alatapb.alatdoc.model.ItApbAlatDocEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class AlatDocDao extends GenericDao<ItApbAlatDocEntity, String> {

    @Override
    protected Class<ItApbAlatDocEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ItApbAlatDocEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbAlatDocEntity.class);

        if (mapCriteria!=null) {
            if (mapCriteria.get("kode_alat_detail")!=null) {
                criteria.add(Restrictions.ilike("kodeAlatDetail", "%" + (String) mapCriteria.get("kode_alat_detail") + "%"));
            }
            if (mapCriteria.get("alat_doc_id")!=null) {
                criteria.add(Restrictions.eq("idAlatDoc", (String)mapCriteria.get("alat_doc_id")));
            }
            if (mapCriteria.get("position")!=null) {
                criteria.add(Restrictions.ilike("position", "%" + (String) mapCriteria.get("position") + "%"));
            }
        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.asc("idAlatDoc"));
//        criteria.setMaxResults(4);
        List<ItApbAlatDocEntity> results = criteria.list();
        return results;
    }

    public List<AlatDoc> getListData(String kodeAlatDetail){
        List<AlatDoc> listOfResult = new ArrayList<AlatDoc>();
        List<Object[]> result = new ArrayList<Object[]>();

        if (kodeAlatDetail == null){
          kodeAlatDetail = "%";
        }
        String query = "SELECT alat_doc_id, kode_alat_detail, position, create_date, created_date_who FROM it_apb_alat_doc WHERE kode_alat_detail LIKE :kodeAlatDetail";
        result = this.sessionFactory.getCurrentSession().createSQLQuery(query)
                .setParameter("kodeAlatDetail", kodeAlatDetail)
                .list();

        AlatDoc alatDoc;
        for (Object[] row: result){
            alatDoc = new AlatDoc();
            alatDoc.setIdAlatDoc((String) row[0]);
            alatDoc.setKodeAlatDetail((String) row[1]);
            alatDoc.setPosition((String) row[2]);
            alatDoc.setCreatedDate((Timestamp) row[3]);
            alatDoc.setCreatedDateWho((String) row[4]);
            listOfResult.add(alatDoc);
        }
        return listOfResult;
    }

    public String getNextAlatDocId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_alat_doc" + "')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }
}
