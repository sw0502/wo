package com.neurix.pmsapb.alatapb.alatdoc.model;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class AlatDoc extends BaseModel implements Serializable {
    private String idAlatDoc;
    private String kodeAlatDetail;
    private String urlPhoto;
    private String position;
    private Timestamp createdDate;
    private String createdDateWho;
    private Timestamp lastUpdate;
    private String lastUpdateWho;
    private String flag;
    private String action;
    private String type;

    private String imgFront;
    private String imgBack;
    private String imgLeft;
    private String imgRight;

    private String filePath;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getImgFront() {
        return imgFront;
    }

    public void setImgFront(String imgFront) {
        this.imgFront = imgFront;
    }

    public String getImgBack() {
        return imgBack;
    }

    public void setImgBack(String imgBack) {
        this.imgBack = imgBack;
    }

    public String getImgLeft() {
        return imgLeft;
    }

    public void setImgLeft(String imgLeft) {
        this.imgLeft = imgLeft;
    }

    public String getImgRight() {
        return imgRight;
    }

    public void setImgRight(String imgRight) {
        this.imgRight = imgRight;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getIdAlatDoc() {
        return idAlatDoc;
    }

    public void setIdAlatDoc(String idAlatDoc) {
        this.idAlatDoc = idAlatDoc;
    }

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDateWho() {
        return createdDateWho;
    }

    public void setCreatedDateWho(String createdDateWho) {
        this.createdDateWho = createdDateWho;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
