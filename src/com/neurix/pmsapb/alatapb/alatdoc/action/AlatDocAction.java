package com.neurix.pmsapb.alatapb.alatdoc.action;

import com.neurix.common.action.BaseTransactionAction;
import com.neurix.common.constant.CommonConstant;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.alatapb.alatdoc.bo.AlatDocBo;
import com.neurix.pmsapb.master.alat.bo.AlatBo;
import com.neurix.pmsapb.master.alat.model.Alat;
import com.neurix.pmsapb.master.app.bo.AppBo;
import com.neurix.pmsapb.master.app.model.App;
import com.neurix.pmsapb.master.gardu.bo.GarduBo;
import com.neurix.pmsapb.master.gardu.model.Gardu;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class AlatDocAction extends BaseTransactionAction{
    protected static transient Logger logger = Logger.getLogger(AlatDocAction.class);
    private AlatDocBo alatDocBoProxy;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        AlatDocAction.logger = logger;
    }

    public AlatDocBo getAlatDocBoProxy() {
        return alatDocBoProxy;
    }

    public void setAlatDocBoProxy(AlatDocBo alatDocBoProxy) {
        this.alatDocBoProxy = alatDocBoProxy;
    }

    public String paging(){
        return SUCCESS;
    }

    @Override
    public String search() {
        return null;
    }

    @Override
    public String initForm() {
        return null;
    }
}
