package com.neurix.pmsapb.alatapb.alatdoc.bo.impl;

import com.neurix.common.constant.CommonConstant;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.alatapb.alatdoc.bo.AlatDocBo;

import com.neurix.pmsapb.alatapb.alatdoc.dao.AlatDocDao;
import com.neurix.pmsapb.alatapb.alatdoc.model.AlatDoc;

import com.neurix.pmsapb.alatapb.alatdoc.model.ItApbAlatDocEntity;
import com.neurix.pmsapb.master.app.dao.AppDao;
import com.neurix.pmsapb.master.app.model.ImApbAppEntity;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class AlatDocBoImpl implements AlatDocBo {
    protected static transient Logger logger = Logger.getLogger(AlatDocBoImpl.class);
    private AlatDocDao alatDocDao;

    public AlatDocDao getAlatDocDao() {
        return alatDocDao;
    }

    public void setAlatDocDao(AlatDocDao alatDocDao) {
        this.alatDocDao = alatDocDao;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        AlatDocBoImpl.logger = logger;
    }

    @Override
    public String getSeqAlatDetailId() throws GeneralBOException {
        return null;
    }

    @Override
    public void saveDelete(AlatDoc bean) throws GeneralBOException {

    }

    @Override
    public void saveEdit(AlatDoc bean) throws GeneralBOException {

    }

    @Override
    public AlatDoc saveAdd(AlatDoc bean) throws GeneralBOException {
        return null;
    }

    @Override
    public List<AlatDoc> getByCriteria(AlatDoc searchBean) throws GeneralBOException {

        List<AlatDoc> listOfResult = new ArrayList<AlatDoc>();
        if (searchBean != null){
            Map hsCriteria = new HashMap();
            if (searchBean.getKodeAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getKodeAlatDetail())){
                hsCriteria.put("kode_alat_detail", searchBean.getKodeAlatDetail());
            }
            if (searchBean.getIdAlatDoc() != null && !"".equalsIgnoreCase(searchBean.getIdAlatDoc())){
                hsCriteria.put("alat_doc_id",searchBean.getIdAlatDoc());
            }
            hsCriteria.put("flag", "Y");

            List<ItApbAlatDocEntity> itApbAlatDocEntity = null;

            try {
                itApbAlatDocEntity = alatDocDao.getByCriteria(hsCriteria);
            } catch (GeneralBOException e){
                logger.error("[AlatDocBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
            if (itApbAlatDocEntity != null){
                AlatDoc alatDoc;
                for (ItApbAlatDocEntity listData : itApbAlatDocEntity){
                    alatDoc = new AlatDoc();
                    alatDoc.setIdAlatDoc(listData.getIdAlatDoc());
                    alatDoc.setKodeAlatDetail(listData.getKodeAlatDetail());
                    alatDoc.setUrlPhoto(listData.getUrlPhoto());
                    alatDoc.setCreatedDate(listData.getCreatedDate());
                    alatDoc.setCreatedDateWho(listData.getCreatedDateWho());
                    alatDoc.setLastUpdate(listData.getLastUpdate());
                    alatDoc.setLastUpdateWho(listData.getLastUpdateWho());
                    alatDoc.setFlag(listData.getFlag());
                    alatDoc.setAction(listData.getAction());
                    alatDoc.setPosition(listData.getPosition());
                    alatDoc.setType(listData.getType());

                    String folderPath = "/pmsapb" + CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT + listData.getUrlPhoto();

                    if (listData.getPosition() == "1"){
                        alatDoc.setImgFront(folderPath);
                    }if (listData.getPosition() == "2"){
                        alatDoc.setImgBack(folderPath);
                    }if (listData.getPosition() == "3"){
                        alatDoc.setImgLeft(folderPath);
                    }if (listData.getPosition() == "4"){
                        alatDoc.setImgRight(folderPath);
                    }
                    listOfResult.add(alatDoc);
                }
            }
        }
        return listOfResult;
    }

    @Override
    public List<AlatDoc> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }
}
