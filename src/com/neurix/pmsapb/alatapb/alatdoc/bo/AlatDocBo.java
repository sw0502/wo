package com.neurix.pmsapb.alatapb.alatdoc.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.alatapb.alatdoc.model.AlatDoc;

import java.util.List;

/**
 * Created by thinkpad on 06/09/2017.
 */
public interface AlatDocBo extends BaseMasterBo<AlatDoc> {
    public String getSeqAlatDetailId() throws GeneralBOException;
}
