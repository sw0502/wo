package com.neurix.pmsapb.alatapb.pemeliharaandoc.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.alatapb.pemeliharaan.model.ItApbCatatanPemeliharaan;
import com.neurix.pmsapb.alatapb.pemeliharaandoc.model.ItApbPemeliharaanDoc;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 30/01/2018.
 */
public class PemeliharaanDocDao extends GenericDao<ItApbPemeliharaanDoc,String> {

    @Override
    protected Class<ItApbPemeliharaanDoc> getEntityClass() {
        return null;
    }

    @Override
    public List<ItApbPemeliharaanDoc> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbPemeliharaanDoc.class);

        if (mapCriteria!=null) {
            if (mapCriteria.get("doc_pemeliharaan_id")!=null) {
                criteria.add(Restrictions.eq("idPemeliharaanDoc", (String) mapCriteria.get("doc_pemeliharaan_id")));
            }
            if (mapCriteria.get("catatan_pemeliharaan_id")!=null) {
                criteria.add(Restrictions.ilike("idPemeliharaan", "%" + (String) mapCriteria.get("catatan_pemeliharaan_id") + "%" ));
            }
        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
//        criteria.addOrder(Order.desc("doc_pemeliharaan_id"));

        List<ItApbPemeliharaanDoc> results = criteria.list();
        return results;
    }

    public String getNextPemeliharaanDocId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_doc_pemeliharaan" + "')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }
}
