package com.neurix.pmsapb.alatapb.pemeliharaandoc.model;

import com.neurix.common.model.BaseModel;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 30/01/2018.
 */
public class PemeliharaanDoc extends BaseModel implements Serializable {

    public String idPemeliharaanDoc;
    public String idPemeliharaan;
    public String urlDoc;
    public Timestamp lastUpdate;
    public String lastUpdateWho;
    public Timestamp createdDate;
    public String createdDateWho;
    public String flag;
    public String action;

    public String getIdPemeliharaanDoc() {
        return idPemeliharaanDoc;
    }

    public void setIdPemeliharaanDoc(String idPemeliharaanDoc) {
        this.idPemeliharaanDoc = idPemeliharaanDoc;
    }

    public String getIdPemeliharaan() {
        return idPemeliharaan;
    }

    public void setIdPemeliharaan(String idPemeliharaan) {
        this.idPemeliharaan = idPemeliharaan;
    }

    public String getUrlDoc() {
        return urlDoc;
    }

    public void setUrlDoc(String urlDoc) {
        this.urlDoc = urlDoc;
    }

    @Override
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    @Override
    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    @Override
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDateWho() {
        return createdDateWho;
    }

    public void setCreatedDateWho(String createdDateWho) {
        this.createdDateWho = createdDateWho;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }
}
