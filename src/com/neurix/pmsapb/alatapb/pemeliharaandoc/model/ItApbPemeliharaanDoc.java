package com.neurix.pmsapb.alatapb.pemeliharaandoc.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 30/01/2018.
 */
public class ItApbPemeliharaanDoc implements Serializable {
    public String idPemeliharaanDoc;
    public String idPemeliharaan;
    public String urlDoc;
    public Timestamp lastUpdate;
    public String lastUpdateWho;
    public Timestamp createdDate;
    public String createdDateWho;
    public String flag;
    public String action;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIdPemeliharaanDoc() {
        return idPemeliharaanDoc;
    }

    public void setIdPemeliharaanDoc(String idPemeliharaanDoc) {
        this.idPemeliharaanDoc = idPemeliharaanDoc;
    }

    public String getIdPemeliharaan() {
        return idPemeliharaan;
    }

    public void setIdPemeliharaan(String idPemeliharaan) {
        this.idPemeliharaan = idPemeliharaan;
    }

    public String getUrlDoc() {
        return urlDoc;
    }

    public void setUrlDoc(String urlDoc) {
        this.urlDoc = urlDoc;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDateWho() {
        return createdDateWho;
    }

    public void setCreatedDateWho(String createdDateWho) {
        this.createdDateWho = createdDateWho;
    }
}
