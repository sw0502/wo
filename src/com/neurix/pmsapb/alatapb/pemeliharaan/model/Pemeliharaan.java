package com.neurix.pmsapb.alatapb.pemeliharaan.model;

import com.neurix.common.model.BaseModel;

import java.io.File;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 30/01/2018.
 */
public class Pemeliharaan extends BaseModel implements Serializable {
    public String idPemeliharaan;
    public String idAlatDetail;
    public String catatan;
    public Timestamp createdDate;
    public String createdDateWho;
    public String noSap;
    public String noApp;
    public String kodeAlat;
    public String namaAlatDetail;
    public File doc;

    public String idPemeliharaanDoc;
    public String urlDoc;
    public String filePath;
    public String downloadPath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public String getIdPemeliharaanDoc() {
        return idPemeliharaanDoc;
    }

    public void setIdPemeliharaanDoc(String idPemeliharaanDoc) {
        this.idPemeliharaanDoc = idPemeliharaanDoc;
    }

    public String getUrlDoc() {
        return urlDoc;
    }

    public void setUrlDoc(String urlDoc) {
        this.urlDoc = urlDoc;
    }

    public File getDoc() {
        return doc;
    }

    public void setDoc(File doc) {
        this.doc = doc;
    }

    public String getNoApp() {
        return noApp;
    }

    public void setNoApp(String noApp) {
        this.noApp = noApp;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getNamaAlatDetail() {
        return namaAlatDetail;
    }

    public void setNamaAlatDetail(String namaAlatDetail) {
        this.namaAlatDetail = namaAlatDetail;
    }

    public String getNoSap() {
        return noSap;
    }

    public void setNoSap(String noSap) {
        this.noSap = noSap;
    }

    public String getIdPemeliharaan() {
        return idPemeliharaan;
    }

    public void setIdPemeliharaan(String idPemeliharaan) {
        this.idPemeliharaan = idPemeliharaan;
    }

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDateWho() {
        return createdDateWho;
    }

    public void setCreatedDateWho(String createdDateWho) {
        this.createdDateWho = createdDateWho;
    }
}
