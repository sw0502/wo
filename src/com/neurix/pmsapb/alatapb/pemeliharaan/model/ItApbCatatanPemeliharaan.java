package com.neurix.pmsapb.alatapb.pemeliharaan.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 30/01/2018.
 */
public class ItApbCatatanPemeliharaan implements Serializable {
    public String idPemeliharaan;
    public String idAlatDetail;
    public String catatan;
    public String noSap;
    public Timestamp createdDate;
    public String createdDateWho;

    public String flag;
    public String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getNoSap() {
        return noSap;
    }

    public void setNoSap(String noSap) {
        this.noSap = noSap;
    }

    public String getIdPemeliharaan() {
        return idPemeliharaan;
    }

    public void setIdPemeliharaan(String idPemeliharaan) {
        this.idPemeliharaan = idPemeliharaan;
    }

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDateWho() {
        return createdDateWho;
    }

    public void setCreatedDateWho(String createdDateWho) {
        this.createdDateWho = createdDateWho;
    }
}
