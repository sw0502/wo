package com.neurix.pmsapb.alatapb.pemeliharaan.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.alatapb.alatdetail.model.MtApbAlatDetailEntity;
import com.neurix.pmsapb.alatapb.pemeliharaan.model.ItApbCatatanPemeliharaan;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 30/01/2018.
 */
public class PemeliharaanDao extends GenericDao<ItApbCatatanPemeliharaan,String> {

    @Override
    protected Class<ItApbCatatanPemeliharaan> getEntityClass() {
        return null;
    }

    @Override
    public List<ItApbCatatanPemeliharaan> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbCatatanPemeliharaan.class);

        if (mapCriteria!=null) {
            if (mapCriteria.get("catatan_pemeliharaan_id")!=null) {
                criteria.add(Restrictions.eq("idPemeliharaan", (String) mapCriteria.get("catatan_pemeliharaan_id")));
            }
            if (mapCriteria.get("kode_alat_detail")!=null) {
                criteria.add(Restrictions.ilike("idAlatDetail", "%" + (String) mapCriteria.get("kode_alat_detail") + "%" ));
            }
            if (mapCriteria.get("no_sap")!=null) {
                criteria.add(Restrictions.ilike("noSap", "%" + (String)mapCriteria.get("no_sap") + "%" ));
            }
        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.desc("idPemeliharaan"));

        List<ItApbCatatanPemeliharaan> results = criteria.list();

        return results;
    }

    public String getNextPemeliharaanId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_pemeliharaan" + "')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }
}
