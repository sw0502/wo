package com.neurix.pmsapb.alatapb.pemeliharaan.action;

import com.neurix.common.action.BaseTransactionAction;
import com.neurix.common.constant.CommonConstant;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetail;
import com.neurix.pmsapb.alatapb.pemeliharaan.bo.PemeliharaanBo;
import com.neurix.pmsapb.alatapb.pemeliharaan.model.Pemeliharaan;
import com.neurix.pmsapb.alatapb.pemeliharaandoc.model.PemeliharaanDoc;
import com.neurix.pmsapb.project.bo.ProjectBo;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by thinkpad on 30/01/2018.
 */
public class PemeliharaanAction extends BaseTransactionAction {
    protected static transient Logger logger = Logger.getLogger(PemeliharaanAction.class);

    private Pemeliharaan pemeliharaan;
    private AlatDetail alatDetail;
    private PemeliharaanBo pemeliharaanBoProxy;
    private String id;
    private File upload;
    private String uploadContentType;
    private String uploadFileName;

    public File getUpload() {
        return upload;
    }

    public void setUpload(File upload) {
        this.upload = upload;
    }

    public String getUploadContentType() {
        return uploadContentType;
    }

    public void setUploadContentType(String uploadContentType) {
        this.uploadContentType = uploadContentType;
    }

    public String getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PemeliharaanBo getPemeliharaanBoProxy() {
        return pemeliharaanBoProxy;
    }

    public void setPemeliharaanBoProxy(PemeliharaanBo pemeliharaanBoProxy){
        this.pemeliharaanBoProxy = pemeliharaanBoProxy;
    }

    public AlatDetail getAlatDetail() {
        return alatDetail;
    }

    public void setAlatDetail(AlatDetail alatDetail) {
        this.alatDetail = alatDetail;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        PemeliharaanAction.logger = logger;
    }

    public Pemeliharaan getPemeliharaan() {
        return pemeliharaan;
    }

    public void setPemeliharaan(Pemeliharaan pemeliharaan) {
        this.pemeliharaan = pemeliharaan;
    }

    @Override
    public String search() {
        logger.info("[AlatDetailAction.search] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfResult = new ArrayList<AlatDetail>();

        AlatDetail alatDetailData = new AlatDetail();
        alatDetailData.setNoSap(pemeliharaan.getNoSap());
        alatDetailData.setIdAlatDetail(pemeliharaan.getIdAlatDetail());
        alatDetailData.setKodeAlat(pemeliharaan.getKodeAlat());
        alatDetailData.setNamaAlatDetail(pemeliharaan.getNamaAlatDetail());

        try{
            listOfResult = pemeliharaanBoProxy.getAlatDetail(alatDetailData);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = pemeliharaanBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
            }
            logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult",listOfResult);
        return SUCCESS;
    }


    @Override
    public String initForm() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        return INPUT;
    }

    public String initEdit(){
        logger.info("[AlatDetailAction.initEdit] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (AlatDetail alatDetailOfList : listOfResult) {
                    if (id.equalsIgnoreCase(alatDetailOfList.getIdAlatDetail())) {
                        setAlatDetail(alatDetailOfList);
                        break;
                    }
                }
            } else {
                setAlatDetail(new AlatDetail());
            }
        } else {
            setAlatDetail(new AlatDetail());
        }
        return "init_edit";
    }

    public String saveEdit(){
        HttpSession session = ServletActionContext.getRequest().getSession();

        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

//        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
//        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

        String seqId = null;
        seqId = pemeliharaanBoProxy.getSeqPemeliharaanId();

        AlatDetail alatDetail = getAlatDetail();
        Pemeliharaan pemeliharaanNew = new Pemeliharaan();
        pemeliharaanNew.setIdPemeliharaan("CP"+seqId);
        pemeliharaanNew.setIdAlatDetail(alatDetail.getIdAlatDetail());
        pemeliharaanNew.setNoSap(alatDetail.getNoSap());
        pemeliharaanNew.setCatatan(pemeliharaan.getCatatan());
        pemeliharaanNew.setCreatedDate(updateTime);
        pemeliharaanNew.setCreatedDateWho(userLogin);

        try{
            pemeliharaanBoProxy.saveAdd(pemeliharaanNew);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = pemeliharaanBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
            }
            logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        if (upload != null){
            boolean copied = false;
            PemeliharaanDoc docPemeliharaan = new PemeliharaanDoc();
            File fileToCreate = null;
            //note : for windows directory
            String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT;

            if (this.upload != null){
                if (this.upload.length() > 0 && this.upload.length() <= 15728640){
                    if ("image/jpeg".equalsIgnoreCase(this.uploadContentType)) {
                        String docId = null;
                        docId = pemeliharaanBoProxy.getSeqPemeliharaanDocId();

                        String fileDocName = "IMG_" + "CP_" + docId + "_" + this.uploadFileName;
                        fileToCreate = new File(filePath, fileDocName);
                        try {
                            FileUtils.copyFile(this.upload, fileToCreate);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        docPemeliharaan.setIdPemeliharaanDoc("DC" + docId);
                        docPemeliharaan.setIdPemeliharaan(pemeliharaanNew.getIdPemeliharaan());
                        docPemeliharaan.setUrlDoc(fileDocName);
                        docPemeliharaan.setCreatedDate(updateTime);
                        docPemeliharaan.setCreatedDateWho(userLogin);
                        docPemeliharaan.setLastUpdate(updateTime);
                        docPemeliharaan.setLastUpdateWho(userLogin);
                    } else if ("application/pdf".equalsIgnoreCase(this.uploadContentType)){
                        String docId = null;
                        docId = pemeliharaanBoProxy.getSeqPemeliharaanDocId();

                        String fileDocName = "PDF_" + "CP_" + docId + "_" + this.uploadFileName;
                        fileToCreate = new File(filePath, fileDocName);
                        try {
                            FileUtils.copyFile(this.upload, fileToCreate);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        docPemeliharaan.setIdPemeliharaanDoc("DC" + docId);
                        docPemeliharaan.setIdPemeliharaan(pemeliharaanNew.getIdPemeliharaan());
                        docPemeliharaan.setUrlDoc(fileDocName);
                        docPemeliharaan.setCreatedDate(updateTime);
                        docPemeliharaan.setCreatedDateWho(userLogin);
                        docPemeliharaan.setLastUpdate(updateTime);
                        docPemeliharaan.setLastUpdateWho(userLogin);
                    }
                    else {
                            logger.error("[PemeliharaanAction.saveDocUpload] Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                            addActionError("Error,  Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        return ERROR;
                    }

                    try{
                            pemeliharaanBoProxy.saveAddDoc(docPemeliharaan);
                        }catch (GeneralBOException e){
                            Long logId = null;
                            try {
                                logId = pemeliharaanBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.search");
                            } catch (GeneralBOException e1) {
                                logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                            }
                            logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                            return "failure_search";
                        }
                    }
                }
            }

        session.removeAttribute("listOfResult");
        return "save_edit";
    }

    public String initViewCatatan(){
        logger.info("[AlatDetailAction.search] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Pemeliharaan> listOfResult = new ArrayList<Pemeliharaan>();

        Pemeliharaan pemeliharaanData = new Pemeliharaan();
        pemeliharaanData.setIdAlatDetail(id);
        try{
            listOfResult = pemeliharaanBoProxy.getByCriteria(pemeliharaanData);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = pemeliharaanBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
            }
            logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        session.removeAttribute("listOfResultCatatan");
        session.setAttribute("listOfResultCatatan",listOfResult);

        return "view_catatan";
    }

    public String viewDoc(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Pemeliharaan> listOfResult = (List) session.getAttribute("listOfResultCatatan");

//        boolean flagPdf;

        String gId = getId();
        String subWord = gId.substring(0,3);
        if("PDF".equalsIgnoreCase(subWord)){
            if (gId != null && !"".equalsIgnoreCase(gId)) {
                if (listOfResult != null) {
                    for (Pemeliharaan projectOfList : listOfResult) {
                        if (gId.equalsIgnoreCase(projectOfList.getUrlDoc())) {
//                            flagPdf = true;
                            setPemeliharaan(projectOfList);
                            break;
                        }
                    }
                } else {
                    setPemeliharaan(new Pemeliharaan());
                }
            }
            return "view_preview_pdf";
        }

        else if("IMG".equalsIgnoreCase(subWord)){
            if (gId != null && !"".equalsIgnoreCase(gId)) {
                if (listOfResult != null) {
                    for (Pemeliharaan projectOfList : listOfResult) {
                        if (gId.equalsIgnoreCase(projectOfList.getUrlDoc())) {
//                            flagPdf = true;
                            setPemeliharaan(projectOfList);
                            break;
                        }
                    }
                } else {
                    setPemeliharaan(new Pemeliharaan());
                }
            }
            return "view_preview_doc";
        }
        else {
            setPemeliharaan(new Pemeliharaan());
            return "view_preview_doc";
        }
    }

}
