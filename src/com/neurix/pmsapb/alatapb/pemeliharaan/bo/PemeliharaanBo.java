package com.neurix.pmsapb.alatapb.pemeliharaan.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetail;
import com.neurix.pmsapb.alatapb.pemeliharaan.model.Pemeliharaan;
import com.neurix.pmsapb.alatapb.pemeliharaandoc.model.PemeliharaanDoc;

import java.util.List;

/**
 * Created by thinkpad on 30/01/2018.
 */
public interface PemeliharaanBo extends BaseMasterBo<Pemeliharaan> {
    public List<AlatDetail> getAlatDetail(AlatDetail searchBean) throws GeneralBOException;
    public String getSeqPemeliharaanId() throws GeneralBOException;
    public String getSeqPemeliharaanDocId() throws GeneralBOException;
    public PemeliharaanDoc saveAddDoc(PemeliharaanDoc bean) throws GeneralBOException;
}
