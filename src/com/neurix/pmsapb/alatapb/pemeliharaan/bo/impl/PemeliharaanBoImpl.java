package com.neurix.pmsapb.alatapb.pemeliharaan.bo.impl;

import com.neurix.common.constant.CommonConstant;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.alatapb.alatdetail.dao.AlatDetailDao;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetail;
import com.neurix.pmsapb.alatapb.alatdetail.model.MtApbAlatDetailEntity;
import com.neurix.pmsapb.alatapb.alatdoc.dao.AlatDocDao;
import com.neurix.pmsapb.alatapb.alatdoc.model.ItApbAlatDocEntity;
import com.neurix.pmsapb.alatapb.pemeliharaan.bo.PemeliharaanBo;
import com.neurix.pmsapb.alatapb.pemeliharaan.dao.PemeliharaanDao;
import com.neurix.pmsapb.alatapb.pemeliharaan.model.ItApbCatatanPemeliharaan;
import com.neurix.pmsapb.alatapb.pemeliharaan.model.Pemeliharaan;
import com.neurix.pmsapb.alatapb.pemeliharaandoc.dao.PemeliharaanDocDao;
import com.neurix.pmsapb.alatapb.pemeliharaandoc.model.ItApbPemeliharaanDoc;
import com.neurix.pmsapb.alatapb.pemeliharaandoc.model.PemeliharaanDoc;
import com.neurix.pmsapb.master.alat.dao.AlatFeatureDao;
import com.neurix.pmsapb.master.alat.model.AlatFeature;
import com.neurix.pmsapb.master.app.dao.AppDao;
import com.neurix.pmsapb.master.app.model.ImApbAppEntity;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 30/01/2018.
 */
public class PemeliharaanBoImpl implements PemeliharaanBo {
    protected static transient Logger logger = Logger.getLogger(PemeliharaanBoImpl.class);

    private PemeliharaanDao pemeliharaanDao;
    private AlatDetailDao alatDetailDao;
    private AlatDocDao alatDocDao;
    private AppDao appDao;
    private PemeliharaanDocDao pemeliharaanDocDao;
    private AlatFeatureDao alatFeatureDao;

    public AlatFeatureDao getAlatFeatureDao() {
        return alatFeatureDao;
    }

    public void setAlatFeatureDao(AlatFeatureDao alatFeatureDao) {
        this.alatFeatureDao = alatFeatureDao;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        PemeliharaanBoImpl.logger = logger;
    }

    public PemeliharaanDocDao getPemeliharaanDocDao() {
        return pemeliharaanDocDao;
    }

    public void setPemeliharaanDocDao(PemeliharaanDocDao pemeliharaanDocDao) {
        this.pemeliharaanDocDao = pemeliharaanDocDao;
    }

    public AlatDocDao getAlatDocDao() {
        return alatDocDao;
    }

    public void setAlatDocDao(AlatDocDao alatDocDao) {
        this.alatDocDao = alatDocDao;
    }

    public AppDao getAppDao() {
        return appDao;
    }

    public void setAppDao(AppDao appDao) {
        this.appDao = appDao;
    }

    public PemeliharaanDao getPemeliharaanDao() {
        return pemeliharaanDao;
    }

    public void setPemeliharaanDao(PemeliharaanDao pemeliharaanDao) {
        this.pemeliharaanDao = pemeliharaanDao;
    }

    public AlatDetailDao getAlatDetailDao() {
        return alatDetailDao;
    }

    public void setAlatDetailDao(AlatDetailDao alatDetailDao) {
        this.alatDetailDao = alatDetailDao;
    }

    @Override
    public void saveDelete(Pemeliharaan bean) throws GeneralBOException {

    }

    @Override
    public void saveEdit(Pemeliharaan bean) throws GeneralBOException {

    }

    @Override
    public Pemeliharaan saveAdd(Pemeliharaan bean) throws GeneralBOException {
        boolean isSave = false;

        if (bean != null){
            ItApbCatatanPemeliharaan entityData = new ItApbCatatanPemeliharaan();

            if (bean.getIdPemeliharaan() != null){
                entityData.setIdPemeliharaan(bean.getIdPemeliharaan());
                entityData.setNoSap(bean.getNoSap());
                entityData.setIdAlatDetail(bean.getIdAlatDetail());
                entityData.setCatatan(bean.getCatatan());
                entityData.setCreatedDate(bean.getCreatedDate());
                entityData.setCreatedDateWho(bean.getCreatedDateWho());
                entityData.setFlag("Y");
                entityData.setAction("C");
                try {
                    pemeliharaanDao.addAndSave(entityData);
                    isSave = true;
                } catch (GeneralBOException e) {
                    logger.error("[PemeliharaanBoImpl.saveAdd] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                }

            }

        }
        return null;
    }

    @Override
    public List<Pemeliharaan> getByCriteria(Pemeliharaan searchBean) throws GeneralBOException {
        List<Pemeliharaan> listOfResultPemeliharaan = new ArrayList<Pemeliharaan>();

        if (searchBean != null){
            Map hsCriteria = new HashMap();
            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            if (searchBean.getIdPemeliharaan() != null && !"".equalsIgnoreCase(searchBean.getIdPemeliharaan())) {
                hsCriteria.put("catatan_pemeliharaan_id", searchBean.getIdPemeliharaan());
            }

            if (searchBean.getIdAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getIdAlatDetail())) {
                hsCriteria.put("kode_alat_detail", searchBean.getIdAlatDetail());
            }
//            if (searchBean.getKodeAlat() != null && !"".equalsIgnoreCase(searchBean.getKodeAlat())) {
//                hsCriteria.put("kode_alat", searchBean.getKodeAlat());
//            }
//            if (searchBean.getNamaAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getNamaAlatDetail())) {
//                hsCriteria.put("nama_alat_detail", searchBean.getNamaAlatDetail());
//            }
            if (searchBean.getNoSap() != null && !"".equalsIgnoreCase(searchBean.getNoSap())) {
                hsCriteria.put("no_sap", searchBean.getNoSap());
            }
            List<ItApbCatatanPemeliharaan> listOfPemeliharaan = null;
            try {
                listOfPemeliharaan = pemeliharaanDao.getByCriteria(hsCriteria);
            } catch (GeneralBOException e) {
                logger.error("[PemeliharaanBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
            if (listOfPemeliharaan != null){
                Pemeliharaan dataPemeliharaan;
                for (ItApbCatatanPemeliharaan catatanEntity : listOfPemeliharaan){
                    dataPemeliharaan = new Pemeliharaan();
                    dataPemeliharaan.setIdPemeliharaan(catatanEntity.getIdPemeliharaan());
                    dataPemeliharaan.setCatatan(catatanEntity.getCatatan());
                    dataPemeliharaan.setIdAlatDetail(catatanEntity.getIdAlatDetail());
                    dataPemeliharaan.setNoSap(catatanEntity.getNoSap());
                    dataPemeliharaan.setCreatedDate(catatanEntity.getCreatedDate());
                    dataPemeliharaan.setCreatedDateWho(catatanEntity.getCreatedDateWho());

                    if (dataPemeliharaan.getIdPemeliharaan() != null){
                        hsCriteria.put("catatan_pemeliharaan_id", dataPemeliharaan.getIdPemeliharaan());
                        List<ItApbPemeliharaanDoc> listOfDoc = null;
                        listOfDoc = pemeliharaanDocDao.getByCriteria(hsCriteria);
                        if (listOfDoc != null){
                            for (ItApbPemeliharaanDoc docEntity : listOfDoc){
                                dataPemeliharaan.setIdPemeliharaanDoc(docEntity.getIdPemeliharaanDoc());
                                dataPemeliharaan.setUrlDoc(docEntity.getUrlDoc());
                                String folderPath = CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT + docEntity.getUrlDoc();
                                dataPemeliharaan.setFilePath("/pmsapb"+folderPath);
                            }
                            String linkDownload = "<a href='"+dataPemeliharaan.getFilePath()+"' download><img src='/pmsapb/pages/images/icon_download.png' width='23px'/></a>";
                            dataPemeliharaan.setDownloadPath(linkDownload);
                        }
                    }
                    listOfResultPemeliharaan.add(dataPemeliharaan);
                }
            }

        }
        return listOfResultPemeliharaan;
    }

    @Override
    public List<Pemeliharaan> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }

    @Override
    public List<AlatDetail> getAlatDetail(AlatDetail searchBean) throws GeneralBOException {
        List<AlatDetail> listOfResultAlatDetail = new ArrayList<AlatDetail>();

        if (searchBean != null) {
            String flag = searchBean.getFlag();
            Map hsCriteria = new HashMap();

            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            if (searchBean.getIdAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getIdAlatDetail())) {
                hsCriteria.put("kode_alat_detail", searchBean.getIdAlatDetail());
            }
            if (searchBean.getKodeAlat() != null && !"".equalsIgnoreCase(searchBean.getKodeAlat())) {
                hsCriteria.put("kode_alat", searchBean.getKodeAlat());
            }
            if (searchBean.getNamaAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getNamaAlatDetail())) {
                hsCriteria.put("nama_alat_detail", searchBean.getNamaAlatDetail());
            }
            if (searchBean.getNoSap() != null && !"".equalsIgnoreCase(searchBean.getNoSap())) {
                hsCriteria.put("no_sap", searchBean.getNoSap());
            }
            List<MtApbAlatDetailEntity> listOfAlatDetail = null;
            try {
                listOfAlatDetail = alatDetailDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
            if (listOfAlatDetail != null) {
                AlatDetail alatDetailData;
                for (MtApbAlatDetailEntity alatDetailEntity : listOfAlatDetail) {
                    alatDetailData = new AlatDetail();
                    alatDetailData.setIdAlatDetail(alatDetailEntity.getIdAlatDetail());
                    alatDetailData.setKodeAlat(alatDetailEntity.getKodeAlat());
                    alatDetailData.setKodeApp(alatDetailEntity.getKodeApp());
                    alatDetailData.setUrlFoto(alatDetailEntity.getUrlFoto());

                    String folderPath = CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT + alatDetailData.getUrlFoto();
                    alatDetailData.setFilePath("/pmsapb" + folderPath);

                    if (alatDetailData.getKodeApp() != null && !"".equalsIgnoreCase(alatDetailData.getKodeApp())) {
                        hsCriteria.put("kode_app", alatDetailData.getKodeApp());
                    }
                    List<ImApbAppEntity> listApp = new ArrayList<ImApbAppEntity>();
                    listApp = appDao.getByCriteria(hsCriteria);

                    if (listApp != null) {
                        for (ImApbAppEntity getApp : listApp) {
                            alatDetailData.setNamaApp(getApp.getNamaWilayah());
                        }
                    }


                    alatDetailData.setNoGi(alatDetailEntity.getNoGi());
                    alatDetailData.setNamaGi(alatDetailEntity.getNamaGi());
                    alatDetailData.setNoGiTujuan(alatDetailEntity.getNoGiTujuan());
                    alatDetailData.setNamaGiTujuan(alatDetailEntity.getNamaGiTujuan());
                    alatDetailData.setNamaAlatDetail(alatDetailEntity.getNamaAlatDetail());
                    alatDetailData.setMerk(alatDetailEntity.getMerk());
                    alatDetailData.setTegangan(alatDetailEntity.getTegangan());
                    alatDetailData.setTypeId(alatDetailEntity.getTypeId());
                    alatDetailData.setKdStatus(alatDetailEntity.getKdStatus());
                    alatDetailData.setNoSeries(alatDetailEntity.getNoSeries());
                    alatDetailData.setTglOprs(alatDetailEntity.getTglOprs());
                    alatDetailData.setTglPasang(alatDetailEntity.getTglPasang());
                    alatDetailData.setKeterangan(alatDetailEntity.getKeterangan());
                    alatDetailData.setJenisTp(alatDetailEntity.getJenisTp());
                    alatDetailData.setFrekuensiTx(alatDetailEntity.getFrekuensiTx());
                    alatDetailData.setFrekuensiRx(alatDetailEntity.getFrekuensiRx());
                    alatDetailData.setDaya(alatDetailEntity.getDaya());
                    alatDetailData.setTransmisi(alatDetailEntity.getTransmisi());
                    alatDetailData.setNote(alatDetailEntity.getNote());
                    alatDetailData.setFlag(alatDetailEntity.getFlag());
                    alatDetailData.setAction(alatDetailEntity.getAction());
                    alatDetailData.setNoSap(alatDetailEntity.getNoSap());
                    alatDetailData.setNoAktiva(alatDetailEntity.getNoAktiva());

                    alatDetailData.setEnDaya("N");
                    alatDetailData.setEnFrekuensiRx("N");
                    alatDetailData.setEnFrekuensiTx("N");
                    alatDetailData.setEnTransmisi("N");
                    alatDetailData.setEnTegOprs("N");
                    alatDetailData.setEnJenisTp("N");

                    if (alatDetailData.getKodeAlat() != null){
//                        hsCriteria.put("kode_alat", alatDetailData.getKodeAlat());
                        List<AlatFeature> listOfAlatFeature = null;
                        AlatFeature alatFeature = new AlatFeature();
                        alatFeature.setKodeAlat(alatDetailData.getKodeAlat());
                        listOfAlatFeature = alatFeatureDao.getDataAlatFeature(alatFeature);

                        if (listOfAlatFeature != null){
                            for (AlatFeature listAlatFeatureData : listOfAlatFeature){
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getDaya())){
                                    alatDetailData.setEnDaya("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getFrekuensiRx())){
                                    alatDetailData.setEnFrekuensiRx("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getFrekuensiTx())){
                                    alatDetailData.setEnFrekuensiTx("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getTransmisi())){
                                    alatDetailData.setEnTransmisi("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getTegOprs())){
                                    alatDetailData.setEnTegOprs("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getJenisTp())){
                                    alatDetailData.setEnJenisTp("Y");
                                }
                            }
                        }
                    }

                    Map alatCriteria = new HashMap();
                    alatCriteria.put("kode_alat_detail", alatDetailEntity.getIdAlatDetail());
                    alatCriteria.put("flag", "Y");
                    List<ItApbAlatDocEntity> itApbAlatDocEntities = null;
                    try {
                        itApbAlatDocEntities = alatDocDao.getByCriteria(alatCriteria);
                    } catch (GeneralBOException e) {
                        logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                    }

                    if (itApbAlatDocEntities != null) {
                        for (ItApbAlatDocEntity listData : itApbAlatDocEntities) {
                            String folderPathAlat = "/pmsapb" + CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT + listData.getUrlPhoto();
                            if ("1".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgFront(folderPathAlat);
                            }
                            if ("2".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgBack(folderPathAlat);
                            }
                            if ("3".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgLeft(folderPathAlat);
                            }
                            if ("4".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgRight(folderPathAlat);
                            }
                            if ("5".equalsIgnoreCase(listData.getPosition())){
                                alatDetailData.setDocName(listData.getUrlPhoto());
                                alatDetailData.setFilePath(folderPathAlat);
                            }
                        }
                    }


//                    String kdalat = alatDetailData.getKodeAlat();
//                    String activeFlag = alatDetailData.getFlag();
//                    String col = "kode_alat";
//                    int count;
//                    count = alatDetailDao.getCount(col, kdalat, activeFlag);

                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    if (alatDetailData.getTglOprs() != null) {
                        alatDetailData.setStTglOprs(df.format(alatDetailData.getTglOprs()));
                    }
                    if (alatDetailData.getTglPasang() != null) {
                        alatDetailData.setStTglPasang(df.format(alatDetailData.getTglPasang()));
                    }
                    listOfResultAlatDetail.add(alatDetailData);
                }
            }
        }
        return listOfResultAlatDetail;
    }

    public String getSeqPemeliharaanId() throws GeneralBOException {
        String id = "";
        try {
            id= pemeliharaanDao.getNextPemeliharaanId();
        } catch (HibernateException e) {
            logger.error("[PermohonanLahanBoImpl.getSeqNumLahanId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }

        logger.info("[PermohonanLahanBoImpl.getSeqNumLahanId] end process <<<");

        return id;
    }

    public String getSeqPemeliharaanDocId() throws GeneralBOException {
        String id = "";
        try {
            id= pemeliharaanDocDao.getNextPemeliharaanDocId();
        } catch (HibernateException e) {
            logger.error("[PermohonanLahanBoImpl.getSeqNumLahanId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }

        logger.info("[PermohonanLahanBoImpl.getSeqNumLahanId] end process <<<");

        return id;
    }

    @Override
    public PemeliharaanDoc saveAddDoc(PemeliharaanDoc bean) throws GeneralBOException {
        if (bean!=null){
            if (bean.getIdPemeliharaanDoc() != null){
                ItApbPemeliharaanDoc entityData = new ItApbPemeliharaanDoc();
                entityData.setIdPemeliharaanDoc("DC"+bean.getIdPemeliharaanDoc());
                entityData.setIdPemeliharaan(bean.getIdPemeliharaan());
                entityData.setUrlDoc(bean.getUrlDoc());
                entityData.setCreatedDate(bean.getCreatedDate());
                entityData.setCreatedDateWho(bean.getCreatedDateWho());
                entityData.setLastUpdate(bean.getLastUpdate());
                entityData.setLastUpdateWho(bean.getLastUpdateWho());
                entityData.setFlag("Y");
                entityData.setAction("C");
                try {
                    pemeliharaanDocDao.addAndSave(entityData);
                } catch (GeneralBOException e) {
                    logger.error("[PemeliharaanBoImpl.saveAddDoc] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
        return null;
    }
}
