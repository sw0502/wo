package com.neurix.pmsapb.alatapb.alatdetail.action;

import com.neurix.authorization.function.model.Functions;
import com.neurix.common.action.BaseMasterAction;
import com.neurix.common.action.BaseTransactionAction;
import com.neurix.common.constant.CommonConstant;
import com.neurix.common.download.excel.CellDetail;
import com.neurix.common.download.excel.DownloadUtil;
import com.neurix.common.download.excel.RowData;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.alatapb.alatdetail.bo.AlatDetailBo;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetail;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetailLog;
import com.neurix.pmsapb.alatapb.alatdetail.model.TpChanel;
import com.neurix.pmsapb.alatapb.alatdetail.model.spek.*;
import com.neurix.pmsapb.alatapb.pemeliharaan.bo.PemeliharaanBo;
import com.neurix.pmsapb.alatapb.pemeliharaan.model.Pemeliharaan;
import com.neurix.pmsapb.master.alat.bo.AlatBo;
import com.neurix.pmsapb.master.alat.model.Alat;
import com.neurix.pmsapb.master.app.bo.AppBo;
import com.neurix.pmsapb.master.app.model.App;
import com.neurix.pmsapb.master.bidang.bo.BidangBo;
import com.neurix.pmsapb.master.bidang.model.Bidang;
import com.neurix.pmsapb.master.gardu.bo.GarduBo;
import com.neurix.pmsapb.master.gardu.model.Gardu;
import com.neurix.pmsapb.master.progres.model.Progres;
import com.neurix.pmsapb.master.subbidang.bo.SubBidangBo;
import com.neurix.pmsapb.master.subbidang.model.SubBidang;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.prepost.PreInvocationAuthorizationAdviceVoter;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.KeyPair;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class AlatDetailAction extends BaseMasterAction {
    protected static transient Logger logger = Logger.getLogger(AlatDetailAction.class);
    private AlatDetail alatDetail;
    private TpChanel tpChanel;
    private PlcChanel plcChanel;
    private KapasitasMux kapasitasMux;
    private SwitchPort switchPort;
    private ModulRtu modulRtu;
    private BayRtu bayRtu;
    private Battery battery;
    private ProteksiSetting proteksiSetting;
    private SasStation sasStation;
    private SasControl sasControl;
    private SasPerangkat sasPerangkat;
    private Rectifier rectifier;

    private AlatDetailBo alatDetailBoProxy;
    private AlatBo alatBoProxy;
    private AppBo appBoProxy;
    private GarduBo garduBoProxy;
    private PemeliharaanBo pemeliharaanBoProxy;
    private Bidang bidang;
    private SubBidang subBidang;
    private BidangBo bidangBoProxy;
    private SubBidangBo subbidangBoProxy;

    private List<Alat> initComboAlat;
    private List<App> initComboApp;
    private List<Gardu> initComboGardu;

    private List<App> initComboAppSearch;
    private List<Gardu> initComboGarduSearch;

    private File uploadDoc;
    private String uploadDocContentType;
    private String uploadDocFileName;

    private File uploadBack;
    private String uploadBackContentType;
    private String uploadBackFileName;

    private File uploadFront;
    private String uploadFrontContentType;
    private String uploadFrontFileName;

    private File uploadLeft;
    private String uploadLeftContentType;
    private String uploadLeftFileName;

    private File uploadRight;
    private String uploadRightContentType;
    private String uploadRightFileName;

    private File fileUploadDoc;
    private String fileUploadDocContentType;
    private String fileUploadDocFileName;

    private String kodeAlat;
    private String spec;
    private String status;
    private String tipe;
    private String id;


    private boolean enableTp = false;
    private boolean enablePlc = false;
    private boolean enableRtu = false;
    private boolean enableMux = false;
    private boolean enableSwitch = false;
    private boolean enableProteksi = false;
    private boolean enableBattery = false;
    private boolean enableSas = false;
    private boolean enableRectifier = false;
    private boolean enableSap = true;
    private boolean enableBtn = false;

    public boolean isEnableBtn() {
        return enableBtn;
    }

    public void setEnableBtn(boolean enableBtn) {
        this.enableBtn = enableBtn;
    }

    public boolean isEnableSap() {
        return enableSap;
    }

    public void setEnableSap(boolean enableSap) {
        this.enableSap = enableSap;
    }

    public Rectifier getRectifier() {
        return rectifier;
    }

    public void setRectifier(Rectifier rectifier) {
        this.rectifier = rectifier;
    }

    public boolean isEnableRectifier() {
        return enableRectifier;
    }

    public void setEnableRectifier(boolean enableRectifier) {
        this.enableRectifier = enableRectifier;
    }

    public boolean isEnableSas() {
        return enableSas;
    }

    public void setEnableSas(boolean enableSas) {
        this.enableSas = enableSas;
    }

    public Battery getBattery() {
        return battery;
    }

    public void setBattery(Battery battery) {
        this.battery = battery;
    }

    public boolean isEnableBattery() {
        return enableBattery;
    }

    public void setEnableBattery(boolean enableBattery) {
        this.enableBattery = enableBattery;
    }


    public SasStation getSasStation() {
        return sasStation;
    }

    public void setSasStation(SasStation sasStation) {
        this.sasStation = sasStation;
    }

    public SasControl getSasControl() {
        return sasControl;
    }

    public void setSasControl(SasControl sasControl) {
        this.sasControl = sasControl;
    }

    public SasPerangkat getSasPerangkat() {
        return sasPerangkat;
    }

    public void setSasPerangkat(SasPerangkat sasPerangkat) {
        this.sasPerangkat = sasPerangkat;
    }

    public boolean isEnableTp() {
        return enableTp;
    }

    public void setEnableTp(boolean enableTp) {
        this.enableTp = enableTp;
    }

    public boolean isEnablePlc() {
        return enablePlc;
    }

    public void setEnablePlc(boolean enablePlc) {
        this.enablePlc = enablePlc;
    }

    public boolean isEnableRtu() {
        return enableRtu;
    }

    public void setEnableRtu(boolean enableRtu) {
        this.enableRtu = enableRtu;
    }

    public boolean isEnableMux() {
        return enableMux;
    }

    public void setEnableMux(boolean enableMux) {
        this.enableMux = enableMux;
    }

    public boolean isEnableSwitch() {
        return enableSwitch;
    }

    public void setEnableSwitch(boolean enableSwitch) {
        this.enableSwitch = enableSwitch;
    }

    public boolean isEnableProteksi() {
        return enableProteksi;
    }

    public void setEnableProteksi(boolean enableProteksi) {
        this.enableProteksi = enableProteksi;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public TpChanel getTpChanel() {
        return tpChanel;
    }

    public void setTpChanel(TpChanel tpChanel) {
        this.tpChanel = tpChanel;
    }

    public PlcChanel getPlcChanel() {
        return plcChanel;
    }

    public void setPlcChanel(PlcChanel plcChanel) {
        this.plcChanel = plcChanel;
    }

    public KapasitasMux getKapasitasMux() {
        return kapasitasMux;
    }

    public void setKapasitasMux(KapasitasMux kapasitasMux) {
        this.kapasitasMux = kapasitasMux;
    }

    public SwitchPort getSwitchPort() {
        return switchPort;
    }

    public void setSwitchPort(SwitchPort switchPort) {
        this.switchPort = switchPort;
    }

    public ModulRtu getModulRtu() {
        return modulRtu;
    }

    public void setModulRtu(ModulRtu modulRtu) {
        this.modulRtu = modulRtu;
    }

    public BayRtu getBayRtu() {
        return bayRtu;
    }

    public void setBayRtu(BayRtu bayRtu) {
        this.bayRtu = bayRtu;
    }

    public ProteksiSetting getProteksiSetting() {
        return proteksiSetting;
    }

    public void setProteksiSetting(ProteksiSetting proteksiSetting) {
        this.proteksiSetting = proteksiSetting;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public SubBidang getSubBidang() {
        return subBidang;
    }

    public void setSubBidang(SubBidang subBidang) {
        this.subBidang = subBidang;
    }

    public Bidang getBidang() {
        return bidang;
    }

    public void setBidang(Bidang bidang) {
        this.bidang = bidang;
    }

    public BidangBo getBidangBoProxy() {
        return bidangBoProxy;
    }

    public void setBidangBoProxy(BidangBo bidangBoProxy) {
        this.bidangBoProxy = bidangBoProxy;
    }

    public SubBidangBo getSubbidangBoProxy() {
        return subbidangBoProxy;
    }

    public void setSubbidangBoProxy(SubBidangBo subbidangBoProxy) {
        this.subbidangBoProxy = subbidangBoProxy;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public List<App> getInitComboAppSearch() {
        return initComboAppSearch;
    }

    public void setInitComboAppSearch(List<App> initComboAppSearch) {
        this.initComboAppSearch = initComboAppSearch;
    }

    public List<Gardu> getInitComboGarduSearch() {
        return initComboGarduSearch;
    }

    public void setInitComboGarduSearch(List<Gardu> initComboGarduSearch) {
        this.initComboGarduSearch = initComboGarduSearch;
    }

    public PemeliharaanBo getPemeliharaanBoProxy() {
        return pemeliharaanBoProxy;
    }

    public void setPemeliharaanBoProxy(PemeliharaanBo pemeliharaanBoProxy) {
        this.pemeliharaanBoProxy = pemeliharaanBoProxy;
    }

    public File getUploadDoc() {
        return uploadDoc;
    }

    public void setUploadDoc(File uploadDoc) {
        this.uploadDoc = uploadDoc;
    }

    public String getUploadDocContentType() {
        return uploadDocContentType;
    }

    public void setUploadDocContentType(String uploadDocContentType) {
        this.uploadDocContentType = uploadDocContentType;
    }

    public String getUploadDocFileName() {
        return uploadDocFileName;
    }

    public void setUploadDocFileName(String uploadDocFileName) {
        this.uploadDocFileName = uploadDocFileName;
    }

    public File getUploadBack() {
        return uploadBack;
    }

    public void setUploadBack(File uploadBack) {
        this.uploadBack = uploadBack;
    }

    public String getUploadBackContentType() {
        return uploadBackContentType;
    }

    public void setUploadBackContentType(String uploadBackContentType) {
        this.uploadBackContentType = uploadBackContentType;
    }

    public String getUploadBackFileName() {
        return uploadBackFileName;
    }

    public void setUploadBackFileName(String uploadBackFileName) {
        this.uploadBackFileName = uploadBackFileName;
    }

    public File getUploadFront() {
        return uploadFront;
    }

    public void setUploadFront(File uploadFront) {
        this.uploadFront = uploadFront;
    }

    public String getUploadFrontContentType() {
        return uploadFrontContentType;
    }

    public void setUploadFrontContentType(String uploadFrontContentType) {
        this.uploadFrontContentType = uploadFrontContentType;
    }

    public String getUploadFrontFileName() {
        return uploadFrontFileName;
    }

    public void setUploadFrontFileName(String uploadFrontFileName) {
        this.uploadFrontFileName = uploadFrontFileName;
    }

    public File getUploadLeft() {
        return uploadLeft;
    }

    public void setUploadLeft(File uploadLeft) {
        this.uploadLeft = uploadLeft;
    }

    public String getUploadLeftContentType() {
        return uploadLeftContentType;
    }

    public void setUploadLeftContentType(String uploadLeftContentType) {
        this.uploadLeftContentType = uploadLeftContentType;
    }

    public String getUploadLeftFileName() {
        return uploadLeftFileName;
    }

    public void setUploadLeftFileName(String uploadLeftFileName) {
        this.uploadLeftFileName = uploadLeftFileName;
    }

    public File getUploadRight() {
        return uploadRight;
    }

    public void setUploadRight(File uploadRight) {
        this.uploadRight = uploadRight;
    }

    public String getUploadRightContentType() {
        return uploadRightContentType;
    }

    public void setUploadRightContentType(String uploadRightContentType) {
        this.uploadRightContentType = uploadRightContentType;
    }

    public String getUploadRightFileName() {
        return uploadRightFileName;
    }

    public void setUploadRightFileName(String uploadRightFileName) {
        this.uploadRightFileName = uploadRightFileName;
    }


    public File getFileUploadDoc() {
        return fileUploadDoc;
    }

    public void setFileUploadDoc(File fileUploadDoc) {
        this.fileUploadDoc = fileUploadDoc;
    }

    public String getFileUploadDocContentType() {
        return fileUploadDocContentType;
    }

    public void setFileUploadDocContentType(String fileUploadDocContentType) {
        this.fileUploadDocContentType = fileUploadDocContentType;
    }

    public String getFileUploadDocFileName() {
        return fileUploadDocFileName;
    }

    public void setFileUploadDocFileName(String fileUploadDocFileName) {
        this.fileUploadDocFileName = fileUploadDocFileName;
    }

    public AlatDetail getAlatDetail() {
        return alatDetail;
    }

    public void setAlatDetail(AlatDetail alatDetail) {
        this.alatDetail = alatDetail;
    }

    public AlatDetailBo getAlatDetailBoProxy() {
        return alatDetailBoProxy;
    }

    public void setAlatDetailBoProxy(AlatDetailBo alatDetailBoProxy) {
        this.alatDetailBoProxy = alatDetailBoProxy;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        AlatDetailAction.logger = logger;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Alat> getInitComboAlat() {
        return initComboAlat;
    }

    public void setInitComboAlat(List<Alat> initComboAlat) {
        this.initComboAlat = initComboAlat;
    }

    public List<App> getInitComboApp() {
        return initComboApp;
    }

    public void setInitComboApp(List<App> initComboApp) {
        this.initComboApp = initComboApp;
    }

    public List<Gardu> getInitComboGardu() {
        return initComboGardu;
    }

    public void setInitComboGardu(List<Gardu> initComboGardu) {
        this.initComboGardu = initComboGardu;
    }

    public AlatBo getAlatBoProxy() {
        return alatBoProxy;
    }

    public void setAlatBoProxy(AlatBo alatBoProxy) {
        this.alatBoProxy = alatBoProxy;
    }

    public AppBo getAppBoProxy() {
        return appBoProxy;
    }

    public void setAppBoProxy(AppBo appBoProxy) {
        this.appBoProxy = appBoProxy;
    }

    public GarduBo getGarduBoProxy() {
        return garduBoProxy;
    }

    public void setGarduBoProxy(GarduBo garduBoProxy) {
        this.garduBoProxy = garduBoProxy;
    }

    @Override
    public String search() {
        logger.info("[AlatDetailAction.search] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfResult = new ArrayList<AlatDetail>();

        AlatDetail alatDetailData = getAlatDetail();

        try {
            listOfResult = alatDetailBoProxy.getByCriteria(alatDetailData);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.search] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        String role = CommonUtil.roleAsLogin();

        if (role.equalsIgnoreCase("ADMIN ASSET")){
            setEnableBtn(true);
        }

        if (role.equalsIgnoreCase("ADMIN")){
            setEnableBtn(true);
        }

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfResult);
        return SUCCESS;
    }


    @Override
    public String initForm() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        session.removeAttribute("listOfProject");
        session.removeAttribute("listOfSwitchPort");
        session.removeAttribute("listOfTpChanel");
        session.removeAttribute("listOfPlcChanel");
        session.removeAttribute("listOfKapasitasMux");
        session.removeAttribute("listOfModulRtu");
        session.removeAttribute("listOfBayRtu");
        session.removeAttribute("listOfProteksiSetting");
        session.removeAttribute("listOfBattery");
        session.removeAttribute("listOfSasStation");
        session.removeAttribute("listOfSasControl");
        session.removeAttribute("listOfSasPerangkat");
        session.removeAttribute("listOfRectifier");

        String role = CommonUtil.roleAsLogin();

        if (role.equalsIgnoreCase("ADMIN ASSET")){
            setEnableBtn(true);
        }

        if (role.equalsIgnoreCase("ADMIN")){
            setEnableBtn(true);
        }

        return INPUT;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    public String viewAlatDetail() {
        logger.info("[AlatDetailAction.viewAlatDetail] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfResult = (List) session.getAttribute("listOfResult");

//        String id = getId();
//        String kodeAlat = getKodeAlat();
//
//        boolean enBattery = false;
//
//        if (kodeAlat.equalsIgnoreCase("24")) {
//            Battery battery = new Battery();
//            battery.setKodeAlatDetail(id);
//            enBattery = true;
//        }

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (AlatDetail alatDetailOfList : listOfResult) {
                    if (id.equalsIgnoreCase(alatDetailOfList.getIdAlatDetail())) {
                        setAlatDetail(alatDetailOfList);
                        break;
                    }
                }
            } else {
                setAlatDetail(new AlatDetail());
            }
        } else {
            setAlatDetail(new AlatDetail());
        }
        return "view_alat_detail";
    }


    public String initAdd() {
        logger.info("[AlatDetailAction.initAdd] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        session.removeAttribute("listOfSwitchPort");
        session.removeAttribute("listOfTpChanel");
        session.removeAttribute("listOfPlcChanel");
        session.removeAttribute("listOfKapasitasMux");
        session.removeAttribute("listOfModulRtu");
        session.removeAttribute("listOfBayRtu");
        session.removeAttribute("listOfProteksiSetting");
        session.removeAttribute("listOfBattery");
        session.removeAttribute("listOfRectifier");
        logger.info("[AlatDetailAction.initAdd] end process >>>");
        return "init_add";
    }

    public String initAddNotSap() {
        logger.info("[AlatDetailAction.initAdd] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        session.removeAttribute("listOfSwitchPort");
        session.removeAttribute("listOfTpChanel");
        session.removeAttribute("listOfPlcChanel");
        session.removeAttribute("listOfKapasitasMux");
        session.removeAttribute("listOfModulRtu");
        session.removeAttribute("listOfBayRtu");
        session.removeAttribute("listOfProteksiSetting");
        session.removeAttribute("listOfBattery");
        session.removeAttribute("listOfRectifier");
        logger.info("[AlatDetailAction.initAdd] end process >>>");
        return "init_add_not_sap";
    }

    public String saveAdd() {
        logger.info("[AlatDetailAction.saveAdd] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        List<TpChanel> listOfTpChanel = (List) session.getAttribute("listOfTpChanel");
        List<PlcChanel> listOfPlcChanel = (List<PlcChanel>) session.getAttribute("listOfPlcChanel");
        List<KapasitasMux> listOfKapasitasMux = (List<KapasitasMux>) session.getAttribute("listOfKapasitasMux");
        List<SwitchPort> listOfSwitchPort = (List) session.getAttribute("listOfSwitchPort");
        List<ModulRtu> listOfModulRtu = (List<ModulRtu>) session.getAttribute("listOfModulRtu");
        List<BayRtu> listOfBayRtu = (List<BayRtu>) session.getAttribute("listOfBayRtu");
        List<ProteksiSetting> listOfProteksiSetting = (List<ProteksiSetting>) session.getAttribute("listOfProteksiSetting");
        List<Battery> listOfBattery = (List<Battery>) session.getAttribute("listOfBattery");
        List<SasStation> listOfSasStation = (List<SasStation>) session.getAttribute("listOfSasStation");
        List<SasControl> listOfSasControl = (List<SasControl>) session.getAttribute("listOfSasControl");
        List<SasPerangkat> listOfSasPerangkat = (List<SasPerangkat>) session.getAttribute("listOfSasPerangkat");
        List<Rectifier> listOfRectifier = (List<Rectifier>) session.getAttribute("listOfRectifier");

        AlatDetail alatDetailData = getAlatDetail();
        String kodeAlat = alatDetailData.getKodeAlat();

        String getSeqId = null;
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AlatDetailBo alatDetailBo = (AlatDetailBo) ctx.getBean("alatDetailBoProxy");

        try {
            getSeqId = alatDetailBo.getSeqAlatDetailId();
        } catch (GeneralBOException e) {
            logger.error("Error when generate sequence id" + e);
        }

        if (alatDetailData.getNoAktiva() != null){
            if (getSeqId != null) {
                alatDetailData.setIdAlatDetail(kodeAlat + getSeqId + "-" + alatDetailData.getNoAktiva());
            }
        } else {
            if (getSeqId != null) {
                alatDetailData.setIdAlatDetail(kodeAlat + getSeqId);
            }
        }



        File fileToCreate = null;
        //note : for windows directory
        String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT;

        if (this.uploadFront != null) {
            if (this.uploadFront.length() > 0 && this.uploadFront.length() <= 15728640) {
                if ("image/jpeg".equalsIgnoreCase(this.uploadFrontContentType)) {
                    String fileDocName = "IMG_" + alatDetailData.getIdAlatDetail() + "_" + "FRONT" + "_" + this.uploadFrontFileName;
                    fileToCreate = new File(filePath, fileDocName);
                    try {
                        FileUtils.copyFile(this.uploadFront, fileToCreate);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    alatDetailData.setUploadFront(fileDocName);
                }
            }
        }

        if (this.uploadDoc != null) {
            if (this.uploadDoc.length() > 0 && this.uploadDoc.length() <= 15728640) {
                if ("application/pdf".equalsIgnoreCase(this.uploadDocContentType)) {
                    String fileDocName = "DOC_" + alatDetailData.getIdAlatDetail() + "_" + "FRONT" + "_" + this.uploadDocFileName;
                    fileToCreate = new File(filePath, fileDocName);
                    try {
                        FileUtils.copyFile(this.uploadDoc, fileToCreate);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    alatDetailData.setUploadDoc(fileDocName);
                }
            }
        }

//        if (this.uploadBack != null){
//            if (this.uploadBack.length() > 0 && this.uploadBack.length() <= 15728640){
//                if ("image/jpeg".equalsIgnoreCase(this.uploadBackContentType)) {
//                    String fileDocName = "IMG_"+kodeAlat+"_"+"BACK"+"_"+ this.uploadBackFileName;
//                    fileToCreate = new File(filePath, fileDocName);
//                    try {
//                        FileUtils.copyFile(this.uploadBack, fileToCreate);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    alatDetailData.setUploadBack(fileDocName);
//                }
//            }
//        }
//        if (this.uploadLeft != null){
//            if (this.uploadLeft.length() > 0 && this.uploadLeft.length() <= 15728640){
//                if ("image/jpeg".equalsIgnoreCase(this.uploadLeftContentType)) {
//                    String fileDocName = "IMG_"+kodeAlat+"_"+"LEFT"+"_"+ this.uploadLeftFileName;
//                    fileToCreate = new File(filePath, fileDocName);
//                    try {
//                        FileUtils.copyFile(this.uploadLeft, fileToCreate);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    alatDetailData.setUploadLeft(fileDocName);
//                }
//            }
//        }
//        if (this.uploadRight != null){
//            if (this.uploadRight.length() > 0 && this.uploadRight.length() <= 15728640){
//                if ("image/jpeg".equalsIgnoreCase(this.uploadRightContentType)) {
//                    String fileDocName = "IMG_"+kodeAlat+"_"+"RIGHT"+"_"+ this.uploadRightFileName;
//                    fileToCreate = new File(filePath, fileDocName);
//                    try {
//                        FileUtils.copyFile(this.uploadRight, fileToCreate);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    alatDetailData.setUploadRight(fileDocName);
//                }
//            }
//        }

        alatDetailData.setLastUpdate(updateTime);
        alatDetailData.setLastUpdateWho(userLogin);
        try {
            alatDetailBoProxy.saveAdd(alatDetailData);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_add";
        }

        //save list of plc channel
        if (listOfTpChanel != null) {
            for (TpChanel tpChanel : listOfTpChanel) {
                tpChanel.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                tpChanel.setKodeAlat(alatDetailData.getKodeAlat());
                tpChanel.setCreatedDate(updateTime);
                tpChanel.setCreatedWho(userLogin);
                tpChanel.setLastUpdate(updateTime);
                tpChanel.setLastUpdateWho(userLogin);
                tpChanel.setFlag("Y");
                tpChanel.setAction("C");
                tpChanel.setAdd(true);
                try {
                    alatDetailBoProxy.saveTpChanel(tpChanel);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }

            }
        }

        // save list of tp channel
        if (listOfPlcChanel != null) {
            for (PlcChanel plcChanel : listOfPlcChanel) {
                plcChanel.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                plcChanel.setKodeAlat(alatDetailData.getKodeAlat());
                plcChanel.setCreatedDate(updateTime);
                plcChanel.setCreatedWho(userLogin);
                plcChanel.setLastUpdate(updateTime);
                plcChanel.setLastUpdateWho(userLogin);
                plcChanel.setFlag("Y");
                plcChanel.setAction("C");
                plcChanel.setAdd(true);
                try {
                    alatDetailBoProxy.savePlcChanel(plcChanel);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }

            }
        }
        // save list switch port
        if (listOfSwitchPort != null) {
            for (SwitchPort switchPort : listOfSwitchPort) {
                switchPort.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                switchPort.setKodeAlat(alatDetailData.getKodeAlat());
                switchPort.setCreatedDate(updateTime);
                switchPort.setCreatedWho(userLogin);
                switchPort.setLastUpdate(updateTime);
                switchPort.setLastUpdateWho(userLogin);
                switchPort.setFlag("Y");
                switchPort.setAction("C");
                switchPort.setAdd(true);
                try {
                    alatDetailBoProxy.saveSwitchPort(switchPort);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }

            }
        }

        //save list of kapasitas mux
        if (listOfKapasitasMux != null) {
            for (KapasitasMux kapasitasMux : listOfKapasitasMux) {
                kapasitasMux.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                kapasitasMux.setKodeAlat(alatDetailData.getKodeAlat());
                kapasitasMux.setCreatedDate(updateTime);
                kapasitasMux.setCreatedWho(userLogin);
                kapasitasMux.setLastUpdate(updateTime);
                kapasitasMux.setLastUpdateWho(userLogin);
                kapasitasMux.setFlag("Y");
                kapasitasMux.setAction("C");
                kapasitasMux.setAdd(true);
                try {
                    alatDetailBoProxy.saveKapasitasMux(kapasitasMux);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        //save list of modul rtu
        if (listOfModulRtu != null) {
            for (ModulRtu modulRtu : listOfModulRtu) {
                modulRtu.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                modulRtu.setKodeAlat(alatDetailData.getKodeAlat());
                modulRtu.setCreatedDate(updateTime);
                modulRtu.setCreatedWho(userLogin);
                modulRtu.setLastUpdate(updateTime);
                modulRtu.setLastUpdateWho(userLogin);
                modulRtu.setFlag("Y");
                modulRtu.setAction("C");
                modulRtu.setAdd(true);
                try {
                    alatDetailBoProxy.saveModulRtu(modulRtu);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        // save list of bay rtu
        if (listOfBayRtu != null) {
            for (BayRtu bayRtu : listOfBayRtu) {
                bayRtu.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                bayRtu.setKodeAlat(alatDetailData.getKodeAlat());
                bayRtu.setCreatedDate(updateTime);
                bayRtu.setCreatedWho(userLogin);
                bayRtu.setLastUpdate(updateTime);
                bayRtu.setLastUpdateWho(userLogin);
                bayRtu.setFlag("Y");
                bayRtu.setAction("C");
                bayRtu.setAdd(true);
                try {
                    alatDetailBoProxy.saveBayRtu(bayRtu);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        //save List of Proteksi Setting
        if (listOfProteksiSetting != null) {
            for (ProteksiSetting proteksiSetting : listOfProteksiSetting) {
                proteksiSetting.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                proteksiSetting.setKodeAlat(alatDetailData.getKodeAlat());
                proteksiSetting.setCreatedDate(updateTime);
                proteksiSetting.setCreatedWho(userLogin);
                proteksiSetting.setLastUpdate(updateTime);
                proteksiSetting.setLastUpdateWho(userLogin);
                proteksiSetting.setFlag("Y");
                proteksiSetting.setAction("C");
                proteksiSetting.setAdd(true);
                try {
                    alatDetailBoProxy.saveProteksiSetting(proteksiSetting);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }
        //save List of Catu daya
//        if (listOfBattery != null) {
//            for (Battery battery : listOfBattery) {
//                battery.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
//                battery.setKodeAlat(alatDetailData.getKodeAlat());
//                battery.setCreatedDate(updateTime);
//                battery.setCreatedWho(userLogin);
//                battery.setLastUpdate(updateTime);
//                battery.setLastUpdateWho(userLogin);
//                battery.setFlag("Y");
//                battery.setAction("C");
//                battery.setAdd(true);
//                try {
//                    alatDetailBoProxy.saveBattery(battery);
//                } catch (GeneralBOException e) {
//                    Long logId = null;
//                    try {
//                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
//                    } catch (GeneralBOException e1) {
//                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
//                    }
//                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
//                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
//                    return "failure_save_add";
//                }
//
//            }
//        }

        //save List of Catu daya
        if (listOfRectifier != null) {
            for (Rectifier  rectifier : listOfRectifier) {
                rectifier.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                rectifier.setKodeAlat(alatDetailData.getKodeAlat());
                rectifier.setCreatedDate(updateTime);
                rectifier.setCreatedWho(userLogin);
                rectifier.setLastUpdate(updateTime);
                rectifier.setLastUpdateWho(userLogin);
                rectifier.setFlag("Y");
                rectifier.setAction("C");
                rectifier.setAdd(true);
                try {
                    alatDetailBoProxy.saveRectifier(rectifier);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }

            }
        }

        //save List of SAS Station
        if (listOfSasStation != null){
            for (SasStation sasStation :listOfSasStation){
                sasStation.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                sasStation.setKodeAlat(alatDetailData.getKodeAlat());
                sasStation.setCreatedDate(updateTime);
                sasStation.setCreatedWho(userLogin);
                sasStation.setLastUpdate(updateTime);
                sasStation.setLastUpdateWho(userLogin);
                sasStation.setFlag("Y");
                sasStation.setAction("C");
                sasStation.setAdd(true);
                try{
                    alatDetailBoProxy.saveSasStation(sasStation);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        //save List of SAS Control
        if (listOfSasControl != null){
            for (SasControl sasControl :listOfSasControl){
                sasControl.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                sasControl.setKodeAlat(alatDetailData.getKodeAlat());
                sasControl.setCreatedDate(updateTime);
                sasControl.setCreatedWho(userLogin);
                sasControl.setLastUpdate(updateTime);
                sasControl.setLastUpdateWho(userLogin);
                sasControl.setFlag("Y");
                sasControl.setAction("C");
                sasControl.setAdd(true);
                try{
                    alatDetailBoProxy.saveSasControl(sasControl);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        //save List of SAS Perangkat
        if (listOfSasPerangkat != null){
            for (SasPerangkat sasPerangkat :listOfSasPerangkat){
                sasPerangkat.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                sasPerangkat.setKodeAlat(alatDetailData.getKodeAlat());
                sasPerangkat.setCreatedDate(updateTime);
                sasPerangkat.setCreatedWho(userLogin);
                sasPerangkat.setLastUpdate(updateTime);
                sasPerangkat.setLastUpdateWho(userLogin);
                sasPerangkat.setFlag("Y");
                sasPerangkat.setAction("C");
                sasPerangkat.setAdd(true);
                try{
                    alatDetailBoProxy.saveSasPerangkat(sasPerangkat);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        logger.info("[AlatDetailAction.saveAdd] end process >>>");
        session.removeAttribute("listOfResult");

        return "save_add";
    }

    public String initEdit() {
        logger.info("[AlatDetailAction.initEdit] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();

        List<TpChanel> listOfTpChanel = (List) session.getAttribute("listOfTpChanel");
        List<PlcChanel> listOfPlcChanel = (List<PlcChanel>) session.getAttribute("listOfPlcChanel");
        List<KapasitasMux> listOfKapasitasMux = (List<KapasitasMux>) session.getAttribute("listOfKapasitasMux");
        List<SwitchPort> listOfSwitchPort = (List) session.getAttribute("listOfSwitchPort");
        List<ModulRtu> listOfModulRtu = (List<ModulRtu>) session.getAttribute("listOfModulRtu");
        List<BayRtu> listOfBayRtu = (List<BayRtu>) session.getAttribute("listOfBayRtu");
        List<ProteksiSetting> listOfProteksiSetting = (List<ProteksiSetting>) session.getAttribute("listOfProteksiSetting");
        List<Battery> listOfBattery = (List<Battery>) session.getAttribute("listOfBattery");
        List<SasStation> listOfSasStation = (List<SasStation>) session.getAttribute("listOfSasStation");
        List<SasControl> listOfSasControl = (List<SasControl>) session.getAttribute("listOfSasControl");
        List<SasPerangkat> listOfSasPerangkat = (List<SasPerangkat>) session.getAttribute("listOfSasPerangkat");
        List<Rectifier> listOfRectifier = (List<Rectifier>) session.getAttribute("listOfRectifier");


        if (listOfTpChanel != null) {
            setEnableTp(true);
        }
        if (listOfPlcChanel != null) {
            setEnablePlc(true);
        }
        if (listOfKapasitasMux != null) {
            setEnableMux(true);
        }
        if (listOfModulRtu != null || listOfBayRtu != null) {
            setEnableRtu(true);
        }
        if (listOfSwitchPort != null) {
            setEnableSwitch(true);
        }
        if (listOfBattery != null) {
            setEnableBattery(true);
        }
        if (listOfRectifier != null) {
            setEnableRectifier(true);
        }
        if (listOfProteksiSetting != null){
            setEnableProteksi(true);
        }

        if (listOfSasStation != null || listOfSasControl != null || listOfSasPerangkat != null){
            setEnableSas(true);
        }

        return "edit_alat";
    }

    public String saveEdit() {
        logger.info("[AlatDetailAction.saveEdit] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

        List<TpChanel> listOfTpChanel = (List) session.getAttribute("listOfTpChanel");
        List<PlcChanel> listOfPlcChanel = (List<PlcChanel>) session.getAttribute("listOfPlcChanel");
        List<KapasitasMux> listOfKapasitasMux = (List<KapasitasMux>) session.getAttribute("listOfKapasitasMux");
        List<SwitchPort> listOfSwitchPort = (List) session.getAttribute("listOfSwitchPort");
        List<ModulRtu> listOfModulRtu = (List<ModulRtu>) session.getAttribute("listOfModulRtu");
        List<BayRtu> listOfBayRtu = (List<BayRtu>) session.getAttribute("listOfBayRtu");
        List<ProteksiSetting> listOfProteksiSetting = (List<ProteksiSetting>) session.getAttribute("listOfProteksiSetting");
        List<Battery> listOfBattery = (List<Battery>) session.getAttribute("listOfBattery");
        List<SasStation> listOfSasStation = (List<SasStation>) session.getAttribute("listOfSasStation");
        List<SasControl> listOfSasControl = (List<SasControl>) session.getAttribute("listOfSasControl");
        List<SasPerangkat> listOfSasPerangkat = (List<SasPerangkat>) session.getAttribute("listOfSasPerangkat");
        List<Rectifier> listOfRectifier = (List<Rectifier>) session.getAttribute("listOfRectifier") ;

        AlatDetail alatDetailData = getAlatDetail();
        String kodeAlat = alatDetailData.getKodeAlat();
        alatDetailData.setLastUpdate(updateTime);
        alatDetailData.setLastUpdateWho(userLogin);

        File fileToCreate = null;
        //note : for windows directory
        String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT;

        if (this.uploadFront != null) {
            if (this.uploadFront.length() > 0 && this.uploadFront.length() <= 15728640) {
                if ("image/jpeg".equalsIgnoreCase(this.uploadFrontContentType)) {
                    String fileDocName = "IMG_" + kodeAlat + "_" + "FRONT" + "_" + this.uploadFrontFileName;
                    fileToCreate = new File(filePath, fileDocName);
                    try {
                        FileUtils.copyFile(this.uploadFront, fileToCreate);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    alatDetailData.setUploadFront(fileDocName);
                }
            }
        }

        if (this.uploadDoc != null) {
            if (this.uploadDoc.length() > 0 && this.uploadDoc.length() <= 15728640) {
                if ("application/pdf".equalsIgnoreCase(this.uploadDocContentType)) {
                    String fileDocName = "DOC_" + kodeAlat + "_" + "FRONT" + "_" + this.uploadDocFileName;
                    fileToCreate = new File(filePath, fileDocName);
                    try {
                        FileUtils.copyFile(this.uploadDoc, fileToCreate);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    alatDetailData.setUploadDoc(fileDocName);
                }
            }
        }

//        if (this.uploadBack != null){
//            if (this.uploadBack.length() > 0 && this.uploadBack.length() <= 15728640){
//                if ("image/jpeg".equalsIgnoreCase(this.uploadBackContentType)) {
//                    String fileDocName = "IMG_"+kodeAlat+"_"+"BACK"+"_"+ this.uploadBackFileName;
//                    fileToCreate = new File(filePath, fileDocName);
//                    try {
//                        FileUtils.copyFile(this.uploadBack, fileToCreate);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    alatDetailData.setUploadBack(fileDocName);
//                }
//            }
//        }
//        if (this.uploadLeft != null){
//            if (this.uploadLeft.length() > 0 && this.uploadLeft.length() <= 15728640){
//                if ("image/jpeg".equalsIgnoreCase(this.uploadLeftContentType)) {
//                    String fileDocName = "IMG_"+kodeAlat+"_"+"LEFT"+"_"+ this.uploadLeftFileName;
//                    fileToCreate = new File(filePath, fileDocName);
//                    try {
//                        FileUtils.copyFile(this.uploadLeft, fileToCreate);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    alatDetailData.setUploadLeft(fileDocName);
//                }
//            }
//        }
//        if (this.uploadRight != null){
//            if (this.uploadRight.length() > 0 && this.uploadRight.length() <= 15728640){
//                if ("image/jpeg".equalsIgnoreCase(this.uploadRightContentType)) {
//                    String fileDocName = "IMG_"+kodeAlat+"_"+"RIGHT"+"_"+ this.uploadRightFileName;
//                    fileToCreate = new File(filePath, fileDocName);
//                    try {
//                        FileUtils.copyFile(this.uploadRight, fileToCreate);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    alatDetailData.setUploadRight(fileDocName);
//                }
//            }
//        }

        try {
            alatDetailBoProxy.saveEdit(alatDetailData);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveEdit");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveEdit] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveEdit] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_edit";
        }

        //save list of plc channel
        if (listOfTpChanel != null) {
            for (TpChanel tpChanel : listOfTpChanel) {
                tpChanel.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                tpChanel.setKodeAlat(alatDetailData.getKodeAlat());
                tpChanel.setLastUpdate(updateTime);
                tpChanel.setLastUpdateWho(userLogin);
                tpChanel.setFlag("Y");
                tpChanel.setAction("U");
                try {
                    alatDetailBoProxy.saveTpChanel(tpChanel);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveEdit");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }

            }
        }

        // save list of tp channel
        if (listOfPlcChanel != null) {
            for (PlcChanel plcChanel : listOfPlcChanel) {
                plcChanel.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                plcChanel.setKodeAlat(alatDetailData.getKodeAlat());
                plcChanel.setLastUpdate(updateTime);
                plcChanel.setLastUpdateWho(userLogin);
                plcChanel.setFlag("Y");
                plcChanel.setAction("U");
                try {
                    alatDetailBoProxy.savePlcChanel(plcChanel);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveEdit");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }

            }
        }
        // save list switch port
        if (listOfSwitchPort != null) {
            for (SwitchPort switchPort : listOfSwitchPort) {
                switchPort.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                switchPort.setKodeAlat(alatDetailData.getKodeAlat());
                switchPort.setLastUpdate(updateTime);
                switchPort.setLastUpdateWho(userLogin);
                switchPort.setFlag("Y");
                switchPort.setAction("U");
                try {
                    alatDetailBoProxy.saveSwitchPort(switchPort);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveEdit");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }

            }
        }

        //save list of kapasitas mux
        if (listOfKapasitasMux != null) {
            for (KapasitasMux kapasitasMux : listOfKapasitasMux) {
                kapasitasMux.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                kapasitasMux.setKodeAlat(alatDetailData.getKodeAlat());
                kapasitasMux.setLastUpdate(updateTime);
                kapasitasMux.setLastUpdateWho(userLogin);
                kapasitasMux.setFlag("Y");
                kapasitasMux.setAction("U");
                try {
                    alatDetailBoProxy.saveKapasitasMux(kapasitasMux);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveEdit");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        //save list of modul rtu
        if (listOfModulRtu != null) {
            for (ModulRtu modulRtu : listOfModulRtu) {
                modulRtu.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                modulRtu.setKodeAlat(alatDetailData.getKodeAlat());
                modulRtu.setLastUpdate(updateTime);
                modulRtu.setLastUpdateWho(userLogin);
                modulRtu.setFlag("Y");
                modulRtu.setAction("U");
                try {
                    alatDetailBoProxy.saveModulRtu(modulRtu);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveEdit");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        // save list of bay rtu
        if (listOfBayRtu != null) {
            for (BayRtu bayRtu : listOfBayRtu) {
                bayRtu.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                bayRtu.setKodeAlat(alatDetailData.getKodeAlat());
                bayRtu.setLastUpdate(updateTime);
                bayRtu.setLastUpdateWho(userLogin);
                bayRtu.setFlag("Y");
                bayRtu.setAction("U");
                try {
                    alatDetailBoProxy.saveBayRtu(bayRtu);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveEdit");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        //save List of Proteksi Setting
        if (listOfProteksiSetting != null) {
            for (ProteksiSetting proteksiSetting : listOfProteksiSetting) {
                proteksiSetting.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                proteksiSetting.setKodeAlat(alatDetailData.getKodeAlat());
                proteksiSetting.setLastUpdate(updateTime);
                proteksiSetting.setLastUpdateWho(userLogin);
                proteksiSetting.setFlag("Y");
                proteksiSetting.setAction("U");
                try {
                    alatDetailBoProxy.saveProteksiSetting(proteksiSetting);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveEdit");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        if (listOfBattery != null) {
            for (Battery battery : listOfBattery) {
                battery.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                battery.setKodeAlat(alatDetailData.getKodeAlat());
                battery.setLastUpdate(updateTime);
                battery.setLastUpdateWho(userLogin);
                battery.setFlag("Y");
                battery.setAction("U");
                try {
                    alatDetailBoProxy.saveBattery(battery);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveEdit");
                    }catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        if (listOfRectifier != null) {
            for (Rectifier rectifier : listOfRectifier) {
                rectifier.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                rectifier.setKodeAlat(alatDetailData.getKodeAlat());
                rectifier.setLastUpdate(updateTime);
                rectifier.setLastUpdateWho(userLogin);
                rectifier.setFlag("Y");
                rectifier.setAction("U");
                try {
                    alatDetailBoProxy.saveRectifier(rectifier);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveEdit");
                    }catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        //save List of SAS Station
        if (listOfSasStation != null){
            for (SasStation sasStation :listOfSasStation){
                sasStation.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                sasStation.setKodeAlat(alatDetailData.getKodeAlat());
                sasStation.setLastUpdate(updateTime);
                sasStation.setLastUpdateWho(userLogin);
                sasStation.setFlag("Y");
                sasStation.setAction("U");
                try{
                    alatDetailBoProxy.saveSasStation(sasStation);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        //save List of SAS Control
        if (listOfSasControl != null){
            for (SasControl sasControl :listOfSasControl){
                sasControl.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                sasControl.setKodeAlat(alatDetailData.getKodeAlat());
                sasControl.setLastUpdate(updateTime);
                sasControl.setLastUpdateWho(userLogin);
                sasControl.setFlag("Y");
                sasControl.setAction("U");
                try{
                    alatDetailBoProxy.saveSasControl(sasControl);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        //save List of SAS Perangkat
        if (listOfSasPerangkat != null){
            for (SasPerangkat sasPerangkat :listOfSasPerangkat){
                sasPerangkat.setKodeAlatDetail(alatDetailData.getIdAlatDetail());
                sasPerangkat.setKodeAlat(alatDetailData.getKodeAlat());
                sasPerangkat.setLastUpdate(updateTime);
                sasPerangkat.setLastUpdateWho(userLogin);
                sasPerangkat.setFlag("Y");
                sasPerangkat.setAction("U");
                try{
                    alatDetailBoProxy.saveSasPerangkat(sasPerangkat);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveAdd");
                    } catch (GeneralBOException e1) {
                        logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
                    }
                    logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_save_add";
                }
            }
        }

        logger.info("[AlatDetailAction.saveEdit] end process >>>");
        session.removeAttribute("listOfResult");
        return "save_edit";
    }

    public String saveDelete() {
        logger.info("[AlatDetailAction.saveDelete] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

        AlatDetail alatDetailData = getAlatDetail();
        alatDetailData.setLastUpdate(updateTime);
        alatDetailData.setLastUpdateWho(userLogin);

        try {
            alatDetailBoProxy.saveDelete(alatDetailData);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.saveDelete");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveDelete] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveDelete] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_delete";
        }
        logger.info("[AlatDetailAction.saveEdit] end process >>>");
        session.removeAttribute("listOfResult");
        return "save_delete";
    }

    public String initDelete() {
        logger.info("[AlatDetailAction.initDelete] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (AlatDetail alatDetailOfList : listOfResult) {
                    if (id.equalsIgnoreCase(alatDetailOfList.getIdAlatDetail())) {
                        setAlatDetail(alatDetailOfList);
                        break;
                    }
                }
            } else {
                setAlatDetail(new AlatDetail());
            }
        } else {
            setAlatDetail(new AlatDetail());
        }
        return "init_delete";
    }

    public List initComboAlat(String query) {
        logger.info("[PermohonanLahanAction.initComboLokasiKebun] start process >>>");

        List<Alat> listOfAlat = new ArrayList();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AlatBo alatBo = (AlatBo) ctx.getBean("alatBoProxy");
//        HttpSession session = WebContextFactory.get().getSession();


        try {
            listOfAlat = alatBo.getComboAlatWithCriteria(query);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatBo.saveErrorMessage(e.getMessage(), "DesaBo.getComboDesaWithCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when get combo lokasi kebun," + "[" + logId + "] Found problem when retrieving combo lokasi kebun data, please inform to your admin.", e);
        }

        logger.info("[PermohonanLahanAction.initComboLokasiKebun] end process <<<");

        return listOfAlat;
    }

    public List initComboApp(String query) {
        logger.info("[PermohonanLahanAction.initComboLokasiKebun] start process >>>");
        List<App> listOfApp = new ArrayList<App>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AppBo appBo = (AppBo) ctx.getBean("appBoProxy");

        try {
            listOfApp = appBo.getComboAppWithCriteria(query);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = appBo.saveErrorMessage(e.getMessage(), "DesaBo.getComboDesaWithCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when get combo lokasi kebun," + "[" + logId + "] Found problem when retrieving combo lokasi kebun data, please inform to your admin.", e);
        }
        logger.info("[PermohonanLahanAction.initComboLokasiKebu" +
                "" +
                "n] end process <<<");
        return listOfApp;
    }

    public List initComboAppSearch(String query) {
        logger.info("[PermohonanLahanAction.initComboLokasiKebun] start process >>>");
        List<App> listOfApp = new ArrayList<App>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AppBo appBo = (AppBo) ctx.getBean("appBoProxy");

        try {
            listOfApp = appBo.getComboAppWithCriteria(query);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = appBo.saveErrorMessage(e.getMessage(), "DesaBo.getComboDesaWithCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when get combo lokasi kebun," + "[" + logId + "] Found problem when retrieving combo lokasi kebun data, please inform to your admin.", e);
        }
        logger.info("[PermohonanLahanAction.initComboLokasiKebu" +
                "" +
                "n] end process <<<");
        return listOfApp;
    }

    public List initComboGardu(String query) {
        logger.info("[PermohonanLahanAction.initComboLokasiKebun] start process >>>");
        List<Gardu> listOfGardu = new ArrayList<Gardu>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        GarduBo garduBo = (GarduBo) ctx.getBean("garduBoProxy");

        try {
            listOfGardu = garduBo.getComboGarduByCriteria(query);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = garduBo.saveErrorMessage(e.getMessage(), "DesaBo.getComboDesaWithCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when get combo lokasi kebun," + "[" + logId + "] Found problem when retrieving combo lokasi kebun data, please inform to your admin.", e);
        }
        logger.info("[PermohonanLahanAction.initComboLokasiKebu" + "" + "n] end process <<<");
        return listOfGardu;
    }

    public List initComboGarduSearch(String query) {
        logger.info("[PermohonanLahanAction.initComboLokasiKebun] start process >>>");
        List<Gardu> listOfGardu = new ArrayList<Gardu>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        GarduBo garduBo = (GarduBo) ctx.getBean("garduBoProxy");

        try {
            listOfGardu = garduBo.getComboGarduByCriteria(query);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = garduBo.saveErrorMessage(e.getMessage(), "DesaBo.getComboDesaWithCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when get combo lokasi kebun," + "[" + logId + "] Found problem when retrieving combo lokasi kebun data, please inform to your admin.", e);
        }
        logger.info("[PermohonanLahanAction.initComboLokasiKebu" + "" + "n] end process <<<");
        return listOfGardu;
    }

    public String viewAlatDetailLog() {
        logger.info("[AlatDetailAction.search] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetailLog> listOfResult = new ArrayList<AlatDetailLog>();

        String idAlatDetail = getId();
        AlatDetailLog alatData = new AlatDetailLog();
        alatData.setIdAlatDetail(idAlatDetail);
        alatData.setFlag("Y");

        try {
            listOfResult = alatDetailBoProxy.getLogByCriteria(alatData);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.search] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        session.removeAttribute("listOfResultLogAlat");
        session.setAttribute("listOfResultLogAlat", listOfResult);
        return "view_log";
    }

    public String printQRCode() {
        String kodeAlatDetail = getId();

        if (kodeAlatDetail != null) {
            reportParams.put("urlLogo", CommonConstant.URL_IMAGE_LOGO_REPORT);
            reportParams.put("titleReport", "Print QR Code");
            reportParams.put("kodeAlatDetail", kodeAlatDetail);

            try {
                preDownload();
            } catch (SQLException e) {
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "printQRCode");
                } catch (GeneralBOException e1) {
                    logger.error("[AlatDetailAction.printQRCode] Error when downloading ,", e1);
                }
                logger.error("[AlatDetailAction.printQRCode] Error when print berita acara ," + "[" + logId + "] Found problem when downloading data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when downloading data, please inform to your admin.");
                return "failure_print";

            }

        }
        logger.info("[AlatDetailAction.printQRCode] end process <<<");

//        return "print_qr";
        return "success_print";
    }

    public String printReportAlat() {
        String kodeAlat = getId();
        reportParams.put("kodeAlat", kodeAlat);
//        if(kodeAlat != null){
        reportParams.put("urlLogo1", CommonConstant.URL_IMAGE_LOGO_REPORT);
        reportParams.put("urlLogo", CommonConstant.URL_IMAGE_LOGO_REPORT2);
//            reportParams.put("titleReport", "Print Report Alat");
//            reportParams.put("kodeAlat", kodeAlat);

        try {
            preDownload();
        } catch (SQLException e) {
            Long logId = null;
            try {
                logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "printReportAlat");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.printReportAlat] Error when downloading ,", e1);
            }
            logger.error("[AlatDetailAction.printReportAlat] Error when print berita acara ," + "[" + logId + "] Found problem when downloading data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when downloading data, please inform to your admin.");
            return "failure_print";

        }

        logger.info("[AlatDetailAction.printReportAlat] end process <<<");

//        return "print_qr";
        return "success_print_report_alat";
    }

    public String initViewCatatan() {
        logger.info("[AlatDetailAction.search] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Pemeliharaan> listOfResult = new ArrayList<Pemeliharaan>();

        Pemeliharaan pemeliharaanData = new Pemeliharaan();
        pemeliharaanData.setIdAlatDetail(id);
        try {
            listOfResult = pemeliharaanBoProxy.getByCriteria(pemeliharaanData);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = pemeliharaanBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
            }
            logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        session.removeAttribute("listOfResultCatatan");
        session.setAttribute("listOfResultCatatan", listOfResult);

        return "view_catatan";
    }

    public String viewDoc() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfResult = (List) session.getAttribute("listOfResult");

//        boolean flagPdf;

        String gId = getId();
        String subWord = gId.substring(0, 3);
        if ("DOC".equalsIgnoreCase(subWord)) {
            if (gId != null && !"".equalsIgnoreCase(gId)) {
                if (listOfResult != null) {
                    for (AlatDetail projectOfList : listOfResult) {
                        if (gId.equalsIgnoreCase(projectOfList.getDocName())) {
//                            flagPdf = true;
                            setAlatDetail(projectOfList);
                            break;
                        }
                    }
                } else {
                    setAlatDetail(new AlatDetail());
                }
            }
        }

//        else if("IMG".equalsIgnoreCase(subWord)){
//            if (gId != null && !"".equalsIgnoreCase(gId)) {
//                if (listOfResult != null) {
//                    for (Pemeliharaan projectOfList : listOfResult) {
//                        if (gId.equalsIgnoreCase(projectOfList.getUrlDoc())) {
////                            flagPdf = true;
//                            setPemeliharaan(projectOfList);
//                            break;
//                        }
//                    }
//                } else {
//                    setPemeliharaan(new Pemeliharaan());
//                }
//            }
//            return "view_preview_doc";
//        }
//        else {
//            setPemeliharaan(new Pemeliharaan());
//            return "view_preview_doc";
//        }
        return "view_preview_pdf";
    }

    public String addAlat() {
        logger.info("[AlatDetailAction.addAlat] start process >>>");
        Alat alat = new Alat();
//        AlatDetail alatdetail = new AlatDetail();
        String idSubBidang = "";
        String namaSubBidang = "";
        String idBidang = "";
        String namaBidang = "";

        List<Alat> listofResultAlat = new ArrayList<Alat>();

        AlatDetail alatDetail = getAlatDetail();
        alat.setKodeAlat(alatDetail.getKodeAlat());

        try {
            listofResultAlat = alatBoProxy.getByCriteria(alat);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatBoProxy.saveErrorMessage(e.getMessage(), "AlatAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[AlatAction.search] Error when saving error,", e1);
            }
            logger.error("[AlatAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        if (listofResultAlat != null) {
            for (Alat listalat : listofResultAlat) {
                idSubBidang = listalat.getIdSubBidang();
                namaSubBidang = listalat.getNamaSubBidang();
            }
        }
        List<SubBidang> listofResultSubBidang = new ArrayList<SubBidang>();
        SubBidang searchSubBidang = new SubBidang();
        searchSubBidang.setIdBidang(idBidang);
        searchSubBidang.setIdSubBidang(idSubBidang);
        searchSubBidang.setNamaSubBidang(namaSubBidang);

        try {
            listofResultSubBidang = subbidangBoProxy.getByCriteria(searchSubBidang);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = subbidangBoProxy.saveErrorMessage(e.getMessage(), "SubBidangAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[SubBidangAction.search] Error when saving error,", e1);
            }
            logger.error("[SubBidangAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        if (listofResultSubBidang != null) {
            for (SubBidang listsubbidang : listofResultSubBidang) {
                idSubBidang = listsubbidang.getIdSubBidang();
                namaSubBidang = listsubbidang.getNamaSubBidang();
                idBidang = listsubbidang.getIdBidang();

            }
        }

        List<Bidang> listofResultBidang = new ArrayList<Bidang>();
        Bidang searchBidang = new Bidang();
        searchBidang.setIdBidang(idBidang);
        searchBidang.setNamaBidang(namaBidang);

        try {
            listofResultBidang = bidangBoProxy.getByCriteria(searchBidang);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = bidangBoProxy.saveErrorMessage(e.getMessage(), "BidangAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[BidangAction.search] Error when saving error,", e1);
            }
            logger.error("[BidangAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        if (listofResultBidang != null) {
            for (Bidang listbidang : listofResultBidang) {
                idBidang = listbidang.getIdBidang();
                namaBidang = listbidang.getNamaBidang();
            }
        }

        if ("Y".equalsIgnoreCase(tipe)){
            setEnableSap(true);
        }

        if ("63".equalsIgnoreCase(alatDetail.getKodeAlat())) {
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            alatDetail.setNamaAlatDetail("PLC");
            alatDetail.setIdBidang(idBidang);
            alatDetail.setNamaBidang(namaBidang);
            alatDetail.setIdSubBidang(idSubBidang);
            alatDetail.setNamaSubBidang(namaSubBidang);
            setAlatDetail(alatDetail);
            return "add_plc";
        } else if ("64".equalsIgnoreCase(alatDetail.getKodeAlat())) {
            alatDetail.setNamaAlatDetail("TELEPROTEKSI");
            alatDetail.setIdBidang(idBidang);
            alatDetail.setNamaBidang(namaBidang);
            alatDetail.setIdSubBidang(idSubBidang);
            alatDetail.setNamaSubBidang(namaSubBidang);
            setAlatDetail(alatDetail);
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            return "addTp_alat";
        } else if ("65".equalsIgnoreCase(alatDetail.getKodeAlat())) {
            alatDetail.setNamaAlatDetail("PAX");
            alatDetail.setIdBidang(idBidang);
            alatDetail.setNamaBidang(namaBidang);
            alatDetail.setIdSubBidang(idSubBidang);
            alatDetail.setNamaSubBidang(namaSubBidang);
            setAlatDetail(alatDetail);
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            return "addPax_alat";
        } else if ("68".equalsIgnoreCase(alatDetail.getKodeAlat())) {
            alatDetail.setNamaAlatDetail("MULTIPLEXER");
            alatDetail.setIdBidang(idBidang);
            alatDetail.setNamaBidang(namaBidang);
            alatDetail.setIdSubBidang(idSubBidang);
            alatDetail.setNamaSubBidang(namaSubBidang);
            setAlatDetail(alatDetail);
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            return "add_mux";
        } else if ("76".equalsIgnoreCase(alatDetail.getKodeAlat())) {
            alatDetail.setNamaAlatDetail("SWITCH");
            alatDetail.setIdBidang(idBidang);
            alatDetail.setNamaBidang(namaBidang);
            alatDetail.setIdSubBidang(idSubBidang);
            alatDetail.setNamaSubBidang(namaSubBidang);
            setAlatDetail(alatDetail);
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            return "add_switch";
        } else if ("62".equalsIgnoreCase(alatDetail.getKodeAlat())) {
            alatDetail.setNamaAlatDetail("RTU");
            alatDetail.setIdBidang(idBidang);
            alatDetail.setNamaBidang(namaBidang);
            alatDetail.setIdSubBidang(idSubBidang);
            alatDetail.setNamaSubBidang(namaSubBidang);
            setAlatDetail(alatDetail);
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            return "add_rtu";
        } else if ("24".equalsIgnoreCase(alatDetail.getKodeAlat())) {
            alatDetail.setNamaAlatDetail("BATTERY");
            alatDetail.setIdBidang(idBidang);
            alatDetail.setNamaBidang(namaBidang);
            alatDetail.setIdSubBidang(idSubBidang);
            alatDetail.setNamaSubBidang(namaSubBidang);
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            return "addBattery_alat";
        } else if ("25".equalsIgnoreCase(alatDetail.getKodeAlat())) {
            alatDetail.setNamaAlatDetail("RECTIFIER");
            alatDetail.setIdBidang(idBidang);
            alatDetail.setNamaBidang(namaBidang);
            alatDetail.setIdSubBidang(idSubBidang);
            alatDetail.setNamaSubBidang(namaSubBidang);
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            return "addRectifier_alat";
        } else if ("21".equalsIgnoreCase(alatDetail.getKodeAlat())){
            alatDetail.setNamaAlatDetail("RELAY");
            alatDetail.setIdBidang(idBidang);
            alatDetail.setNamaBidang(namaBidang);
            alatDetail.setIdSubBidang(idSubBidang);
            alatDetail.setNamaSubBidang(namaSubBidang);
            setAlatDetail(alatDetail);
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            return "add_proteksi";
        } else if ("95".equalsIgnoreCase(alatDetail.getKodeAlat())){
            alatDetail.setNamaAlatDetail("SAS");
            alatDetail.setIdBidang(idBidang);
            alatDetail.setNamaBidang(namaBidang);
            alatDetail.setIdSubBidang(idSubBidang);
            alatDetail.setNamaSubBidang(namaSubBidang);
            setAlatDetail(alatDetail);
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            return "add_sas";
        } else {
            logger.info("[AlatDetailAction.addAlat] end process >>>");
            return "input";
        }
    }



    // access to add alat specs start >>>
    public String addTpChanel() {
        return "add_tp";
    }

    public String addPlcChanel() {
        return "add_plc_chenel";
    }

    public String addKapasitasMux() {
        return "add_kapasitas_mux";
    }

    public String addSwitchPort() {
        return "add_switch_port";
    }

    public String addBattery() {
        return "add_battery";
    }
    public String addRectifier(){
        return "add_rectifier";
    }

    public String addSpec() {
        String spec = getSpec();
        if ("modulrtu".equalsIgnoreCase(spec)) {
            return "add_modul_rtu";
        } else if ("bayrtu".equalsIgnoreCase(spec)) {
            return "add_bay_rtu";
        } else if ("proteksi".equalsIgnoreCase(spec)){
            return "add_proteksi_setting";
        } else if ("station".equalsIgnoreCase(spec)){
            return "add_sas_station";
        } else if ("control".equalsIgnoreCase(spec)){
            return "add_sas_control";
        } else if ("perangkat".equalsIgnoreCase(spec)){
            return "add_sas_perangkat";
        }
        else {
            return "failure";
        }
    }
    // acces to add alat specs end >>>

    // save into session list start >>>
    public String saveListTpChanel(String chanel, String fungsiChanel, String status, String chanelId) {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<TpChanel> listOfTpChanel = (List) session.getAttribute("listOfTpChanel");
        List<TpChanel> listOfTpChanelNew = new ArrayList<TpChanel>();

        TpChanel tpChanel = new TpChanel();

        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)) {
            for (TpChanel listData : listOfTpChanel) {
                if (chanelId.equalsIgnoreCase(listData.getChanelId())) {
                    listData.setChanel(chanel);
                    listData.setFungsiChanel(fungsiChanel);
                    listData.setEdit(true);
                    listOfTpChanel.remove(listData);
                    listOfTpChanel.add(listData);
                    break;
                }
            }
        } else {
            tpChanel.setChanel(chanel);
            tpChanel.setFungsiChanel(fungsiChanel);
        }

        if (listOfTpChanel != null) {
            if ("edit".equalsIgnoreCase(status)) {
                session.removeAttribute("listOfTpChanel");
                session.setAttribute("listOfTpChanel", listOfTpChanel);
            } else {
                listOfTpChanel.add(tpChanel);
                session.removeAttribute("listOfTpChanel");
                session.setAttribute("listOfTpChanel", listOfTpChanel);
            }
            flag = true;
        } else {
            listOfTpChanelNew.add(tpChanel);
            session.removeAttribute("listOfTpChanel");
            session.setAttribute("listOfTpChanel", listOfTpChanelNew);
            flag = true;
        }

        if (flag) {
            if ("edit".equalsIgnoreCase(status)) {
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    // save into session list start >>>
    public String saveListSwitchPort(String port, String type, String fungsiPort, String status, String switchId) {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<SwitchPort> listOfSwitchPort = (List) session.getAttribute("listOfSwitchPort");
        List<SwitchPort> listOfSwitchPortNew = new ArrayList<SwitchPort>();

        SwitchPort switchPort = new SwitchPort();


        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)) {
            for (SwitchPort listData : listOfSwitchPort) {
                if (switchId.equalsIgnoreCase(listData.getSwitchPortId())) {
                    listData.setType(type);
                    listData.setPort(port);
                    listData.setFungsiPort(fungsiPort);
                    listData.setEdit(true);
                    listOfSwitchPort.remove(listData);
                    listOfSwitchPort.add(listData);
                    break;
                }
            }
        } else {
            switchPort.setType(type);
            switchPort.setPort(port);
            switchPort.setFungsiPort(fungsiPort);
            switchPort.setAdd(true);
        }

        if (listOfSwitchPort != null) {
            if ("edit".equalsIgnoreCase(status)) {
                session.removeAttribute("listOfSwitchPort");
                session.setAttribute("listOfSwitchPort", listOfSwitchPort);
            } else {
                listOfSwitchPort.add(switchPort);
                session.removeAttribute("listOfSwitchPort");
                session.setAttribute("listOfSwitchPort", listOfSwitchPort);
            }
            flag = true;
        } else {
            listOfSwitchPortNew.add(switchPort);
            session.removeAttribute("listOfSwitchPort");
            session.setAttribute("listOfSwitchPort", listOfSwitchPortNew);
            flag = true;
        }

        if (flag) {
            if ("edit".equalsIgnoreCase(status)) {
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    public String saveListPlcChanel(String chanel, String fungsiChanel, String frekuensi, String status, String chanelId) {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<PlcChanel> listOfPlcChanel = (List) session.getAttribute("listOfPlcChanel");
        List<PlcChanel> listOfPlcChanelNew = new ArrayList<PlcChanel>();

        PlcChanel plcChanel = new PlcChanel();


        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)) {
            for (PlcChanel listData : listOfPlcChanel) {
                if (chanelId.equalsIgnoreCase(listData.getChanelId())) {
                    listData.setChanel(chanel);
                    listData.setFungsiChanel(fungsiChanel);
                    listData.setFrekuensi(frekuensi);
                    listData.setEdit(true);
                    listOfPlcChanel.remove(listData);
                    listOfPlcChanel.add(listData);
                    break;
                }
            }
        } else {
            plcChanel.setChanel(chanel);
            plcChanel.setFungsiChanel(fungsiChanel);
            plcChanel.setFrekuensi(frekuensi);
        }

        if (listOfPlcChanel != null) {
            if ("edit".equalsIgnoreCase(status)) {
                session.removeAttribute("listOfPlcChanel");
                session.setAttribute("listOfPlcChanel", listOfPlcChanel);
            } else {
                listOfPlcChanel.add(plcChanel);
                session.removeAttribute("listOfPlcChanel");
                session.setAttribute("listOfPlcChanel", listOfPlcChanel);
            }
            flag = true;
        } else {
            listOfPlcChanelNew.add(plcChanel);
            session.removeAttribute("listOfPlcChanel");
            session.setAttribute("listOfPlcChanel", listOfPlcChanelNew);
            flag = true;
        }

        if (flag) {
            if ("edit".equalsIgnoreCase(status)) {
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    public String saveListKapasitasMux(String namaModul, String timeSlot, String fungsi, String kapasitas, String status, String kapasitasId) {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<KapasitasMux> listOfKapasitasMux = (List) session.getAttribute("listOfKapasitasMux");
        List<KapasitasMux> listOfKapasitasMuxNew = new ArrayList<KapasitasMux>();

        KapasitasMux kapasitasMux = new KapasitasMux();

        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)) {
            for (KapasitasMux listData : listOfKapasitasMux) {
                if (kapasitasId.equalsIgnoreCase(listData.getKapasitasId())) {
                    listData.setNamaModul(namaModul);
                    listData.setTimeSlot(timeSlot);
                    listData.setFungsi(fungsi);
                    listData.setKapasitas(kapasitas);
                    listData.setEdit(true);
                    listOfKapasitasMux.remove(listData);
                    listOfKapasitasMux.add(listData);
                    break;
                }
            }
        } else {
            kapasitasMux.setNamaModul(namaModul);
            kapasitasMux.setTimeSlot(timeSlot);
            kapasitasMux.setFungsi(fungsi);
            kapasitasMux.setKapasitas(kapasitas);
            kapasitasMux.setAdd(true);
        }

        if (listOfKapasitasMux != null) {
            if ("edit".equalsIgnoreCase(status)) {
                session.removeAttribute("listOfKapasitasMux");
                session.setAttribute("listOfKapasitasMux", listOfKapasitasMux);
            } else {
                listOfKapasitasMux.add(kapasitasMux);
                session.removeAttribute("listOfKapasitasMux");
                session.setAttribute("listOfKapasitasMux", listOfKapasitasMux);
            }
            flag = true;
        } else {
            listOfKapasitasMuxNew.add(kapasitasMux);
            session.removeAttribute("listOfKapasitasMux");
            session.setAttribute("listOfKapasitasMux", listOfKapasitasMuxNew);
            flag = true;
        }

        if (flag) {
            if ("edit".equalsIgnoreCase(status)) {
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    public String saveListModulRtu(String modul, String qty, String jumlahPoint, String status, String modulId) {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<ModulRtu> listOfModulRtu = (List) session.getAttribute("listOfModulRtu");
        List<ModulRtu> listOfModulRtuNew = new ArrayList<ModulRtu>();

        ModulRtu modulRtu = new ModulRtu();


        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)) {
            for (ModulRtu listData : listOfModulRtu) {
                if (modulId.equalsIgnoreCase(listData.getModulId())) {
                    listData.setModul(modul);
                    listData.setQty(qty);
                    listData.setJumlahPoint(jumlahPoint);
                    listData.setEdit(true);
                    listOfModulRtu.remove(listData);
                    listOfModulRtu.add(listData);
                    break;
                }
            }
        } else {
            modulRtu.setModul(modul);
            modulRtu.setQty(qty);
            modulRtu.setJumlahPoint(jumlahPoint);
            modulRtu.setAdd(true);
        }

        if (listOfModulRtu != null) {
            if ("edit".equalsIgnoreCase(status)) {
                session.removeAttribute("listOfModulRtu");
                session.setAttribute("listOfModulRtu", listOfModulRtu);
            } else {
                listOfModulRtu.add(modulRtu);
                session.removeAttribute("listOfModulRtu");
                session.setAttribute("listOfModulRtu", listOfModulRtu);
            }
            flag = true;
        } else {
            listOfModulRtuNew.add(modulRtu);
            session.removeAttribute("listOfModulRtu");
            session.setAttribute("listOfModulRtu", listOfModulRtuNew);
            flag = true;
        }

        if (flag) {
            if ("edit".equalsIgnoreCase(status)) {
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    public String saveListBayRtu(String namaBay, String jumlahDi, String jumlahDo, String jumlahAo, String jumlahAi, String merk, String ratioVt, String ratioCt, String status, String bayId) {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<BayRtu> listOfBayRtu = (List) session.getAttribute("listOfBayRtu");
        List<BayRtu> listOfBayRtuNew = new ArrayList<BayRtu>();

        BayRtu bayRtu = new BayRtu();

        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)) {
            for (BayRtu listData : listOfBayRtu) {
                if (bayId.equalsIgnoreCase(listData.getBayId())) {
                    listData.setNamaBay(namaBay);
                    listData.setJumlahDi(jumlahDi);
                    listData.setJumlahDo(jumlahDo);
                    listData.setJumlahAo(jumlahAo);
                    listData.setJumlahAi(jumlahAi);
                    listData.setMerk(merk);
                    listData.setRatioVt(ratioVt);
                    listData.setRatioCt(ratioCt);
                    listData.setEdit(true);
                    listOfBayRtu.remove(listData);
                    listOfBayRtu.add(listData);
                    break;
                }
            }
        } else {
            bayRtu.setNamaBay(namaBay);
            bayRtu.setJumlahDi(jumlahDi);
            bayRtu.setJumlahDo(jumlahDo);
            bayRtu.setJumlahAo(jumlahAo);
            bayRtu.setJumlahAi(jumlahAi);
            bayRtu.setMerk(merk);
            bayRtu.setRatioVt(ratioVt);
            bayRtu.setRatioCt(ratioCt);
            bayRtu.setAdd(true);
        }

        if (listOfBayRtu != null) {
            if ("edit".equalsIgnoreCase(status)) {
                session.removeAttribute("listOfBayRtu");
                session.setAttribute("listOfBayRtu", listOfBayRtu);
            } else {
                listOfBayRtu.add(bayRtu);
                session.removeAttribute("listOfBayRtu");
                session.setAttribute("listOfBayRtu", listOfBayRtu);
            }
            flag = true;
        } else {
            listOfBayRtuNew.add(bayRtu);
            session.removeAttribute("listOfBayRtu");
            session.setAttribute("listOfBayRtu", listOfBayRtuNew);
            flag = true;
        }

        if (flag) {
            if ("edit".equalsIgnoreCase(status)) {
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    public String saveListProteksiSetting(String tahap, String arus, String waktu, String target, String status, String proteksiId) {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<ProteksiSetting> listOfProteksiSetting = (List) session.getAttribute("listOfProteksiSetting");
        List<ProteksiSetting> listOfProteksiSettingNew = new ArrayList<ProteksiSetting>();

        ProteksiSetting proteksiSetting = new ProteksiSetting();

        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)) {
            for (ProteksiSetting listData : listOfProteksiSetting) {
                if (proteksiId.equalsIgnoreCase(listData.getProteksiSettingId())) {
                    listData.setTahap(tahap);
                    listData.setArus(arus);
                    listData.setWaktu(waktu);
                    listData.setTarget(target);
                    listData.setEdit(true);
                    listOfProteksiSetting.remove(listData);
                    listOfProteksiSetting.add(listData);
                    break;
                }
            }
        } else {
            proteksiSetting.setTahap(tahap);
            proteksiSetting.setArus(arus);
            proteksiSetting.setWaktu(waktu);
            proteksiSetting.setTarget(target);
            proteksiSetting.setAdd(true);
        }

        if (listOfProteksiSetting != null) {
            if ("edit".equalsIgnoreCase(status)) {
                session.removeAttribute("listOfProteksiSetting");
                session.setAttribute("listOfProteksiSetting", listOfProteksiSetting);
            } else {
                listOfProteksiSetting.add(proteksiSetting);
                session.removeAttribute("listOfProteksiSetting");
                session.setAttribute("listOfProteksiSetting", listOfProteksiSetting);
            }
            flag = true;
        } else {
            listOfProteksiSettingNew.add(proteksiSetting);
            session.removeAttribute("listOfProteksiSetting");
            session.setAttribute("listOfProteksiSetting", listOfProteksiSettingNew);
            flag = true;
        }

        if (flag) {
            if ("edit".equalsIgnoreCase(status)) {
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    // save into session list start >>>
    public String saveListBattery(String batteryName, String tegangan, String jumlahCell, String merk, String type, String kapasitas, String fuse, String fungsi, String status, String batteryId) {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Battery> listOfBattery = (List) session.getAttribute("listOfBattery");
        List<Battery> listOfBatteryNew = new ArrayList<Battery>();

        Battery battery = new Battery();

        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)) {
            for (Battery listData : listOfBattery) {
                if (batteryId.equalsIgnoreCase(listData.getBatteryId())) {
                    listData.setBatteryName(batteryName);
                    listData.setTegangan(tegangan);
                    listData.setJumlahCell(jumlahCell);
                    listData.setMerk(merk);
                    listData.setType(type);
                    listData.setKapasitas(kapasitas);
                    listData.setFuse(fuse);
                    listData.setFungsi(fungsi);
                    listData.setEdit(true);
                    listOfBattery.remove(listData);
                    listOfBattery.add(listData);
                    break;
                }
            }
        } else {
            battery.setBatteryName(batteryName);
            battery.setTegangan(tegangan);
            battery.setJumlahCell(jumlahCell);
            battery.setMerk(merk);
            battery.setType(type);
            battery.setKapasitas(kapasitas);
            battery.setFuse(fuse);
            battery.setFungsi(fungsi);
            battery.setAdd(true);
        }

        if (listOfBattery != null) {
            if ("edit".equalsIgnoreCase(status)) {
                session.removeAttribute("listOfBattery");
                session.setAttribute("listOfBattery", listOfBattery);
            } else {
                listOfBattery.add(battery);
                session.removeAttribute("listOfBattery");
                session.setAttribute("listOfBattery", listOfBattery);
            }
            flag = true;
        } else {
            listOfBatteryNew.add(battery);
            session.removeAttribute("listOfBattery");
            session.setAttribute("listOfBattery", listOfBatteryNew);
            flag = true;
        }

        if (flag) {
            if ("edit".equalsIgnoreCase(status)) {
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    // save into session list start >>>
    public String saveListRectifier(String panjangAcdb, String panjangBattery, String panjangDcdb, String typeAcdb, String typeBattery, String typeDcdb, String status, String rectifierId) {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Rectifier> listOfRectifier = (List) session.getAttribute("listOfRectifier");
        List<Rectifier> listOfRectifierNew = new ArrayList<Rectifier>();

        Rectifier rectifier = new Rectifier();

        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)) {
            for (Rectifier listData : listOfRectifier) {
                if (rectifierId.equalsIgnoreCase(listData.getRectifierId())) {
                    listData.setPanjangAcdb(panjangAcdb);
                    listData.setPanjangBattery(panjangBattery);
                    listData.setPanjangDcdb(panjangDcdb);
                    listData.setTypeAcdb(typeAcdb);
                    listData.setTypeBattery(typeBattery);
                    listData.setTypeDcdb(typeDcdb);
                    listData.setEdit(true);
                    listOfRectifier.remove(listData);
                    listOfRectifier.add(listData);
                    break;
                }
            }
        } else {
            rectifier.setPanjangAcdb(panjangAcdb);
            rectifier.setPanjangBattery(panjangBattery);
            rectifier.setPanjangDcdb(panjangDcdb);
            rectifier.setTypeAcdb(typeAcdb);
            rectifier.setTypeBattery(typeBattery);
            rectifier.setTypeDcdb(typeDcdb);
            rectifier.setAdd(true);
        }

        if (listOfRectifier != null) {
            if ("edit".equalsIgnoreCase(status)) {
                session.removeAttribute("listOfRectifier");
                session.setAttribute("listOfRectifier", listOfRectifier);
            } else {
                listOfRectifier.add(rectifier);
                session.removeAttribute("listOfRectifier");
                session.setAttribute("listOfRectifier", listOfRectifier);
            }
            flag = true;
        } else {
            listOfRectifierNew.add(rectifier);
            session.removeAttribute("listOfRectifier");
            session.setAttribute("listOfRectifier", listOfRectifierNew);
            flag = true;
        }

        if (flag) {
            if ("edit".equalsIgnoreCase(status)) {
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    public String saveLisSasStation(String namaStation, String tipe, String tipeStation, String merk, String namaSoftware, String versiSoftware, String jumlah, String sn, String status, String sasStationId){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<SasStation> listOfSasStation = (List) session.getAttribute("listOfSasStation");
        List<SasStation> listOfSasStationNew = new ArrayList<SasStation>();

        SasStation sasStation = new SasStation();

        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)){
            for (SasStation listData : listOfSasStation){
                if (sasStationId.equalsIgnoreCase(listData.getSasStationId())){
                    listData.setNamaSatation(namaStation);
                    listData.setTipe(tipe);
                    listData.setTipeStation(tipeStation);
                    listData.setNamaSoftWare(namaSoftware);
                    listData.setVersiSoftware(versiSoftware);
                    listData.setMerk(merk);
                    listData.setJumlah(jumlah);
                    listData.setSn(sn);
                    listData.setEdit(true);
                    listOfSasStation.remove(listData);
                    listOfSasStation.add(listData);
                    break;
                }
            }
        } else {
            sasStation.setNamaSatation(namaStation);
            sasStation.setTipe(tipe);
            sasStation.setTipeStation(tipeStation);
            sasStation.setNamaSoftWare(namaSoftware);
            sasStation.setVersiSoftware(versiSoftware);
            sasStation.setMerk(merk);
            sasStation.setJumlah(jumlah);
            sasStation.setSn(sn);
            sasStation.setAdd(true);
        }

        if (listOfSasStation != null){
            if ("edit".equalsIgnoreCase(status)){
                session.removeAttribute("listOfSasStation");
                session.setAttribute("listOfSasStation", listOfSasStation);
            } else {
                listOfSasStation.add(sasStation);
                session.removeAttribute("listOfSasStation");
                session.setAttribute("listOfSasStation", listOfSasStation);
            }
            flag = true;
        } else {
            listOfSasStationNew.add(sasStation);
            session.removeAttribute("listOfSasStation");
            session.setAttribute("listOfSasStation", listOfSasStationNew);
            flag = true;
        }

        if (flag){
            if ("edit".equalsIgnoreCase(status)){
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    public String saveListSasControl(String namaControl, String tipe, String tipeControl, String merk, String namaSoftware, String versiSoftware, String jumlahDi, String jumlahDo, String jumlahAi, String ratioVt, String ratioCt, String sn, String status, String sasControlId){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<SasControl> listOfSasControl = (List) session.getAttribute("listOfSasControl");
        List<SasControl> listOfSasControlNew = new ArrayList<SasControl>();

        SasControl sasControl = new SasControl();

        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)){
            for (SasControl listData : listOfSasControl){
                if (sasControlId.equalsIgnoreCase(listData.getSasControlId())){
                    listData.setNamaControl(namaControl);
                    listData.setTipe(tipe);
                    listData.setTipeControl(tipeControl);
                    listData.setNamaSoftWare(namaSoftware);
                    listData.setVersiSoftware(versiSoftware);
                    listData.setMerk(merk);
                    listData.setJumlahDi(jumlahDi);
                    listData.setJumlahDo(jumlahDo);
                    listData.setJumlahAi(jumlahAi);
                    listData.setRatioVt(ratioVt);
                    listData.setRatioCt(ratioCt);
                    listData.setSn(sn);
                    listData.setEdit(true);
                    listOfSasControl.remove(listData);
                    listOfSasControl.add(listData);
                    break;
                }
            }
        } else {
            sasControl.setNamaControl(namaControl);
            sasControl.setTipe(tipe);
            sasControl.setTipeControl(tipeControl);
            sasControl.setNamaSoftWare(namaSoftware);
            sasControl.setVersiSoftware(versiSoftware);
            sasControl.setMerk(merk);
            sasControl.setJumlahDi(jumlahDi);
            sasControl.setJumlahDo(jumlahDo);
            sasControl.setJumlahAi(jumlahAi);
            sasControl.setRatioVt(ratioVt);
            sasControl.setRatioCt(ratioCt);
            sasControl.setSn(sn);
            sasControl.setAdd(true);
        }

        if (listOfSasControl != null){
            if ("edit".equalsIgnoreCase(status)){
                session.removeAttribute("listOfSasControl");
                session.setAttribute("listOfSasControl", listOfSasControl);
            } else {
                listOfSasControl.add(sasControl);
                session.removeAttribute("listOfSasControl");
                session.setAttribute("listOfSasControl", listOfSasControl);
            }
            flag = true;
        } else {
            listOfSasControlNew.add(sasControl);
            session.removeAttribute("listOfSasControl");
            session.setAttribute("listOfSasControl", listOfSasControlNew);
            flag = true;
        }

        if (flag){
            if ("edit".equalsIgnoreCase(status)){
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }

    public String saveListSasPerangkat(String namaPerangkat, String tipe, String tipePerangkat, String merk, String jumlah, String sn, String kapasitas, String tipeport, String status, String sasPerangkatId){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<SasPerangkat> listOfSasPerangkat = (List) session.getAttribute("listOfSasPerangkat");
        List<SasPerangkat> listOfSasPerangkatNew = new ArrayList<SasPerangkat>();

        SasPerangkat sasPerangkat = new SasPerangkat();

        boolean flag = false;

        if ("edit".equalsIgnoreCase(status)){
            for (SasPerangkat listData : listOfSasPerangkat){
                if (sasPerangkatId.equalsIgnoreCase(listData.getSasPerangkatId())){
                    listData.setNamaPerangkat(namaPerangkat);
                    listData.setType(tipe);
                    listData.setTipePerangkat(tipePerangkat);
                    listData.setMerk(merk);
                    listData.setJumlah(jumlah);
                    listData.setMerk(merk);
                    listData.setSn(sn);
                    listData.setKapasitas(kapasitas);
                    listData.setTipeport(tipeport);
                    listData.setEdit(true);
                    listOfSasPerangkat.remove(listData);
                    listOfSasPerangkat.add(listData);
                    break;
                }
            }
        } else {
            sasPerangkat.setNamaPerangkat(namaPerangkat);
            sasPerangkat.setType(tipe);
            sasPerangkat.setTipePerangkat(tipePerangkat);
            sasPerangkat.setMerk(merk);
            sasPerangkat.setJumlah(jumlah);
            sasPerangkat.setMerk(merk);
            sasPerangkat.setKapasitas(kapasitas);
            sasPerangkat.setTipeport(tipeport);
            sasPerangkat.setSn(sn);
            sasPerangkat.setAdd(true);
        }

        if (listOfSasPerangkat != null){
            if ("edit".equalsIgnoreCase(status)){
                session.removeAttribute("listOfSasPerangkat");
                session.setAttribute("listOfSasPerangkat", listOfSasPerangkat);
            } else {
                listOfSasPerangkat.add(sasPerangkat);
                session.removeAttribute("listOfSasPerangkat");
                session.setAttribute("listOfSasPerangkat", listOfSasPerangkat);
            }
            flag = true;
        } else {
            listOfSasPerangkatNew.add(sasPerangkat);
            session.removeAttribute("listOfSasPerangkat");
            session.setAttribute("listOfSasPerangkat", listOfSasPerangkatNew);
            flag = true;
        }

        if (flag){
            if ("edit".equalsIgnoreCase(status)){
                return "02";
            } else {
                return "00";
            }
        } else {
            return "01";
        }
    }





    // save list into session end >>>

    // init form for add alat start
    public String initAddJenisTp() {
        return "addTp_alat";
    }

    public String initAddJenisPlc() {
        return "add_plc";
    }

    public String initAddJenisMux() {
        return "add_mux";
    }

    public String initAddJenisRtu() {
        return "add_rtu";
    }

    public String initAddJenisSwitch() {
        return "add_switch";
    }

    public String initAddJenisBattery() {
        return "addBattery_alat";
    }
    public String initAddJenisRectifier(){
        return "addRectifier_alat";
    }
    public String initAddJenisProteksi(){
        return "add_proteksi";
    }
    public String initAddJenisSas(){
        return "add_sas";
    }
    // init form for add alat end

    public String viewSpesifikasi() {
        String msg = "";
        if (kodeAlat.equalsIgnoreCase("64")) {
                if (id != null) {
                msg = specTp(id);
            }
        }
        if (kodeAlat.equalsIgnoreCase("63")) {
            if (id != null) {
                msg = specPlc(id);
            }
        }
        if (kodeAlat.equalsIgnoreCase("68")) {
            if (id != null) {
                msg = specMux(id);
            }
        }
        if (kodeAlat.equalsIgnoreCase("62")) {
            if (id != null) {
                msg = specRtu(id);
            }
        }
        if (kodeAlat.equalsIgnoreCase("76")) {
            if (id != null) {
                msg = specSwitch(id);
            }
        }
        if (kodeAlat.equalsIgnoreCase("24")) {
            if (id != null) {
                msg = specBattery(id);
            }
        }
        if (kodeAlat.equalsIgnoreCase("25")) {
            if (id != null) {
                msg = specRectifier(id);
            }
        }

        if (kodeAlat.equalsIgnoreCase("21")) {
            if (id != null) {
                msg = specProteksi(id);
            }
        }
        if (kodeAlat.equalsIgnoreCase("95")) {
            if (id != null) {
                msg = specSas(id);
            }
        }


        if ("tp".equalsIgnoreCase(msg)) {
            return "view_spec_tp";
        } else if ("plc".equalsIgnoreCase(msg)) {
            return "view_spec_plc";
        } else if ("mux".equalsIgnoreCase(msg)) {
            return "view_spec_mux";
        } else if ("rtu".equalsIgnoreCase(msg)) {
            return "view_spec_rtu";
        } else if ("switch".equalsIgnoreCase(msg)) {
            return "view_spec_switch";
        } else if ("battery".equalsIgnoreCase(msg)) {
            return "view_spec_battery";
        } else if ("rectifier".equalsIgnoreCase(msg)) {
            return "view_spec_rectifier";
        } else if ("pro".equalsIgnoreCase(msg)) {
            return "view_spec_proteksi";
        } else if ("sas".equalsIgnoreCase(msg)) {
            return "view_spec_sas";
        } else {
            return "failure";
        }
    }


    public String specTp(String kodeAlatDetail) {
        HttpSession session = ServletActionContext.getRequest().getSession();

        List<TpChanel> listOfTpChanel = new ArrayList<TpChanel>();
        if (kodeAlatDetail != null) {
            TpChanel tpChanel = new TpChanel();
            tpChanel.setKodeAlatDetail(kodeAlatDetail);

            try {
                listOfTpChanel = alatDetailBoProxy.getSpecTpChanel(tpChanel);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }
        }

        session.removeAttribute("listOfTpChanel");
        session.setAttribute("listOfTpChanel", listOfTpChanel);

        return "tp";
    }

    public String specPlc(String kodeAlatDetail) {
        HttpSession session = ServletActionContext.getRequest().getSession();

        List<PlcChanel> listOfPlcChanel = new ArrayList<PlcChanel>();
        if (kodeAlatDetail != null) {
            PlcChanel plcChanel = new PlcChanel();
            plcChanel.setKodeAlatDetail(kodeAlatDetail);

            try {
                listOfPlcChanel = alatDetailBoProxy.getSpecPlcChanel(plcChanel);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }
        }

        session.removeAttribute("listOfPlcChanel");
        session.setAttribute("listOfPlcChanel", listOfPlcChanel);
        return "plc";
    }

    public String specMux(String kodeAlatDetail) {
        HttpSession session = ServletActionContext.getRequest().getSession();

        List<KapasitasMux> kapasitasMuxList = new ArrayList<KapasitasMux>();
        if (kodeAlatDetail != null) {
            KapasitasMux kapasitasMux = new KapasitasMux();
            kapasitasMux.setKodeAlatDetail(kodeAlatDetail);

            try {
                kapasitasMuxList = alatDetailBoProxy.getSpecKapasitasMux(kapasitasMux);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }
        }

        session.removeAttribute("listOfKapasitasMux");
        session.setAttribute("listOfKapasitasMux", kapasitasMuxList);
        return "mux";
    }

    public String specRtu(String kodeAlatDetail) {
        HttpSession session = ServletActionContext.getRequest().getSession();

        List<ModulRtu> modulRtus = new ArrayList<ModulRtu>();
        List<BayRtu> bayRtus = new ArrayList<BayRtu>();

        if (kodeAlatDetail != null) {
            ModulRtu modulRtu = new ModulRtu();
            BayRtu bayRtu = new BayRtu();

            modulRtu.setKodeAlatDetail(kodeAlatDetail);
            bayRtu.setKodeAlatDetail(kodeAlatDetail);

            try {
                modulRtus = alatDetailBoProxy.getSpecModulRtu(modulRtu);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }

            try {
                bayRtus = alatDetailBoProxy.getSpecBayRtu(bayRtu);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }
        }

        session.removeAttribute("listOfModulRtu");
        session.setAttribute("listOfModulRtu", modulRtus);
        session.removeAttribute("listOfBayRtu");
        session.setAttribute("listOfBayRtu", bayRtus);
        return "rtu";
    }

    public String specSwitch(String kodeAlatDetail) {
        HttpSession session = ServletActionContext.getRequest().getSession();

        List<SwitchPort> listOfSwitchPort = new ArrayList<SwitchPort>();
        if (kodeAlatDetail != null) {
            SwitchPort switchPort = new SwitchPort();
            switchPort.setKodeAlatDetail(kodeAlatDetail);

            try {
                listOfSwitchPort = alatDetailBoProxy.getSpecSwitchPort(switchPort);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }
        }

        session.removeAttribute("listOfSwitchPort");
        session.setAttribute("listOfSwitchPort", listOfSwitchPort);
        return "switch";
    }

    public String specBattery(String kodeAlatDetail) {
        HttpSession session = ServletActionContext.getRequest().getSession();

        List<Battery> listOfBattery = new ArrayList<Battery>();
        if (kodeAlatDetail != null) {
            Battery battery = new Battery();
            battery.setKodeAlatDetail(kodeAlatDetail);

            try {
                listOfBattery = alatDetailBoProxy.getSpecBattery(battery);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specBattery");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            }
        }

        session.removeAttribute("listOfBattery");
        session.setAttribute("listOfBattery", listOfBattery);
        return "battery";

        }

    public String specRectifier(String kodeAlatDetail) {
        HttpSession session = ServletActionContext.getRequest().getSession();

        List<Rectifier> listOfRectifier = new ArrayList<Rectifier>();
        if (kodeAlatDetail != null) {
            Rectifier rectifier = new Rectifier();
            rectifier.setKodeAlatDetail(kodeAlatDetail);

            try {
                listOfRectifier = alatDetailBoProxy.getSpecRectifier(rectifier);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specRectifier");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            }
        }

        session.removeAttribute("listOfRectifier");
        session.setAttribute("listOfRectifier", listOfRectifier);
        return "rectifier";

    }

            public String specSas(String kodeAlatDetail) {
                HttpSession session = ServletActionContext.getRequest().getSession();

                List<SasStation> listOfSasStation = new ArrayList<SasStation>();
                if (kodeAlatDetail != null){
                    SasStation sasStation = new SasStation();
                    sasStation.setKodeAlatDetail(kodeAlatDetail);

                    try{
                        listOfSasStation = alatDetailBoProxy.getSpecSasStation(sasStation);
                    }catch (GeneralBOException e){
                        Long logId = null;
                        try {
                            logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                        } catch (GeneralBOException e1) {
                            logger.error("[PemeliharaanAction.specSas] Error when saving error,", e1);
                        }
                        logger.error("[PemeliharaanAction.specSas] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                        addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                        return "failure_search";
                    }
                }

        List<SasControl> listOfSasControl = new ArrayList<SasControl>();
        if (kodeAlatDetail != null){
            SasControl sasControl = new SasControl();
            sasControl.setKodeAlatDetail(kodeAlatDetail);

            try{
                listOfSasControl = alatDetailBoProxy.getSpecSasControl(sasControl);
            }catch (GeneralBOException e){
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.specSas] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.specSas] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }
        }

        List<SasPerangkat> listOfSasPerangkat = new ArrayList<SasPerangkat>();
        if (kodeAlatDetail != null){
            SasPerangkat sasPerangkat = new SasPerangkat();
            sasPerangkat.setKodeAlatDetail(kodeAlatDetail);

            try{
                listOfSasPerangkat = alatDetailBoProxy.getSpecSasPerangkat(sasPerangkat);
            }catch (GeneralBOException e){
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.specSas] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.specSas] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }
        }

        session.removeAttribute("listOfSasStation");
        session.setAttribute("listOfSasStation", listOfSasStation);
        session.removeAttribute("listOfSasControl");
        session.setAttribute("listOfSasControl", listOfSasControl);
        session.removeAttribute("listOfSasPerangkat");
        session.setAttribute("listOfSasPerangkat", listOfSasPerangkat);

        return "sas";
    }

    public String specProteksi(String kodeAlatDetail){
        HttpSession session = ServletActionContext.getRequest().getSession();

        List<ProteksiSetting> listOfProteksi = new ArrayList<ProteksiSetting>();
        if (kodeAlatDetail != null){
            ProteksiSetting proteksiSetting = new ProteksiSetting();
            proteksiSetting.setKodeAlatDetail(kodeAlatDetail);

            try{
                listOfProteksi = alatDetailBoProxy.getSpecProteksi(proteksiSetting);
            }catch (GeneralBOException e){
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                } catch (GeneralBOException e1) {
                    logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                }
                logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }
        }

        session.removeAttribute("listOfProteksiSetting");
        session.setAttribute("listOfProteksiSetting", listOfProteksi);
        return "pro";
    }

    public String editAlat() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfResult = (List) session.getAttribute("listOfResult");

        String id = getId();
        String kodeAlat = getKodeAlat();

        List<TpChanel> tpChanels = new ArrayList<TpChanel>();
        List<PlcChanel> plcChanels = new ArrayList<PlcChanel>();
        List<KapasitasMux> kapasitasMuxList = new ArrayList<KapasitasMux>();
        List<ModulRtu> modulRtus = new ArrayList<ModulRtu>();
        List<BayRtu> bayRtus = new ArrayList<BayRtu>();
        List<SwitchPort> switchPorts = new ArrayList<SwitchPort>();
        List<Battery> batteries = new ArrayList<Battery>();
        List<Rectifier> rectifiers = new ArrayList<Rectifier>();
        List<ProteksiSetting> proteksiSettings = new ArrayList<ProteksiSetting>();
        List<SasStation> sasStations = new ArrayList<SasStation>();
        List<SasControl> sasControls = new ArrayList<SasControl>();
        List<SasPerangkat> sasPerangkats = new ArrayList<SasPerangkat>();

        boolean enTp = false;
        boolean enPlc = false;
        boolean enMux = false;
        boolean enRtu = false;
        boolean enSwitch = false;
        boolean enBattery = false;
        boolean enProteksi = false;
        boolean enSas = false;
        boolean enRectifier = false;


        if (id != null && kodeAlat != null) {

            if (kodeAlat.equalsIgnoreCase("64")) {
                TpChanel tpChanel = new TpChanel();
                tpChanel.setKodeAlatDetail(id);

                try {
                    tpChanels = alatDetailBoProxy.getSpecTpChanel(tpChanel);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }
            }

            if (kodeAlat.equalsIgnoreCase("63")) {
                PlcChanel plcChanel = new PlcChanel();
                plcChanel.setKodeAlatDetail(id);

                try {
                    plcChanels = alatDetailBoProxy.getSpecPlcChanel(plcChanel);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }
            }

            if (kodeAlat.equalsIgnoreCase("68")) {
                KapasitasMux kapasitasMux = new KapasitasMux();
                kapasitasMux.setKodeAlatDetail(id);

                try {
                    kapasitasMuxList = alatDetailBoProxy.getSpecKapasitasMux(kapasitasMux);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }
            }

            if (kodeAlat.equalsIgnoreCase("62")) {
                ModulRtu modulRtu = new ModulRtu();
                BayRtu bayRtu = new BayRtu();

                modulRtu.setKodeAlatDetail(id);
                bayRtu.setKodeAlatDetail(id);

                try {
                    modulRtus = alatDetailBoProxy.getSpecModulRtu(modulRtu);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }

                try {
                    bayRtus = alatDetailBoProxy.getSpecBayRtu(bayRtu);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specTp");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }
            }
            if (kodeAlat.equalsIgnoreCase("76")) {
                SwitchPort switchPort = new SwitchPort();
                switchPort.setKodeAlatDetail(id);

                try {
                    switchPorts = alatDetailBoProxy.getSpecSwitchPort(switchPort);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specSwitch");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }
            }

            if (kodeAlat.equalsIgnoreCase("21")) {
                ProteksiSetting proteksiSetting = new ProteksiSetting();
                proteksiSetting.setKodeAlatDetail(id);

                try {
                    proteksiSettings = alatDetailBoProxy.getSpecProteksi(proteksiSetting);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specSwitch");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }
            }

            if (kodeAlat.equalsIgnoreCase("24")) {
                Battery battery = new Battery();
                battery.setKodeAlatDetail(id);

                try {
                    batteries = alatDetailBoProxy.getSpecBattery(battery);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specBattery");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }
            }

            if (kodeAlat.equalsIgnoreCase("25")) {
                Rectifier rectifier = new Rectifier();
                rectifier.setKodeAlatDetail(id);

                try {
                    rectifiers = alatDetailBoProxy.getSpecRectifier(rectifier);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specRectifier");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }
            }

            if (kodeAlat.equalsIgnoreCase("95")){
                SasStation sasStation = new SasStation();
                sasStation.setKodeAlatDetail(id);

                try {
                    sasStations = alatDetailBoProxy.getSpecSasStation(sasStation);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specBattery");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }

                SasControl sasControl = new SasControl();
                sasControl.setKodeAlatDetail(id);

                try {
                    sasControls = alatDetailBoProxy.getSpecSasControl(sasControl);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specBattery");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }

                SasPerangkat sasPerangkat = new SasPerangkat();
                sasPerangkat.setKodeAlatDetail(id);

                try {
                    sasPerangkats = alatDetailBoProxy.getSpecSasPerangkat(sasPerangkat);
                } catch (GeneralBOException e) {
                    Long logId = null;
                    try {
                        logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "PemeliharaanAction.specBattery");
                    } catch (GeneralBOException e1) {
                        logger.error("[PemeliharaanAction.search] Error when saving error,", e1);
                    }
                    logger.error("[PemeliharaanAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                    addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                    return "failure_search";
                }
            }
        }
            if (kodeAlat.equalsIgnoreCase("24")) {
                Battery battery = new Battery();
                battery.setKodeAlatDetail(id);
                enBattery = true;
            }

            if (!tpChanels.isEmpty()) {

                session.removeAttribute("listOfTpChanel");
                session.setAttribute("listOfTpChanel", tpChanels);
                enTp = true;

            }
            if (!plcChanels.isEmpty()) {

                session.removeAttribute("listOfPlcChanel");
                session.setAttribute("listOfPlcChanel", plcChanels);
                enPlc = true;

            }
            if (!kapasitasMuxList.isEmpty()) {

                session.removeAttribute("listOfKapasitasMux");
                session.setAttribute("listOfKapasitasMux", kapasitasMuxList);
                enMux = true;

            }
            if (!modulRtus.isEmpty()) {

                session.removeAttribute("listOfModulRtu");
                session.setAttribute("listOfModulRtu", modulRtus);
                enRtu = true;

            }
            if (!bayRtus.isEmpty()) {

                session.removeAttribute("listOfBayRtu");
                session.setAttribute("listOfBayRtu", bayRtus);
                enRtu = true;

            }
            if (!switchPorts.isEmpty()) {

                session.removeAttribute("listOfSwitchPort");
                session.setAttribute("listOfSwitchPort", switchPorts);
                enSwitch = true;
            }
            if (!rectifiers.isEmpty()){

                session.removeAttribute("listOfRectifier");
                session.setAttribute("listOfRectifier", rectifiers);
                enRectifier = true;
            }

            if (!proteksiSettings.isEmpty()){
                session.removeAttribute("listOfProteksiSetting");
                session.setAttribute("listOfProteksiSetting", proteksiSettings);
                enProteksi = true;
            }

            if(!sasStations.isEmpty()){
                session.removeAttribute("listOfSasStation");
                session.setAttribute("listOfSasStation", sasStations);
                enSas = true;
            }

            if(!sasControls.isEmpty()){
                session.removeAttribute("listOfSasControl");
                session.setAttribute("listOfSasControl", sasControls);
                enSas = true;
            }

            if(!sasPerangkats.isEmpty()){
                session.removeAttribute("listOfSasPerangkat");
                session.setAttribute("listOfSasPerangkat", sasPerangkats);
                enSas = true;
            }

            if (listOfResult != null) {
                for (AlatDetail listData : listOfResult) {
                    if (id.equalsIgnoreCase(listData.getIdAlatDetail())) {
                        if (enTp) {
                            setEnableTp(true);
                        }
                        if (enPlc) {
                            setEnablePlc(true);
                        }
                        if (enMux) {
                            setEnableMux(true);
                        }
                        if (enRtu) {
                            setEnableRtu(true);
                        }
                        if (enSwitch) {
                            setEnableSwitch(true);
                        }
                        if (enBattery) {
                            setEnableBattery(true);
                        }
                        if (enRectifier) {
                            setEnableRectifier(true);
                        }
                        if (enProteksi) {
                            setEnableProteksi(true);
                        }

                        if (enSas) {
                            setEnableSas(true);
                        }
                        setAlatDetail(listData);
                        break;
                    }
                }
            }
        return "edit_alat";
    }



    public String initSpecEdit(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<TpChanel> listOfTpChanel = (List) session.getAttribute("listOfTpChanel");
        List<PlcChanel> listOfPlcChanel = (List<PlcChanel>) session.getAttribute("listOfPlcChanel");
        List<KapasitasMux> listOfKapasitasMux = (List<KapasitasMux>) session.getAttribute("listOfKapasitasMux");
        List<SwitchPort> listOfSwitchPort = (List) session.getAttribute("listOfSwitchPort");
        List<ModulRtu> listOfModulRtu = (List<ModulRtu>) session.getAttribute("listOfModulRtu");
        List<BayRtu> listOfBayRtu = (List<BayRtu>) session.getAttribute("listOfBayRtu");
        List<ProteksiSetting> listOfProteksiSetting = (List<ProteksiSetting>) session.getAttribute("listOfProteksiSetting");
        List<Battery> listOfBattery = (List<Battery>) session.getAttribute("listOfBattery");
        List<SasStation> listOfSasStation = (List<SasStation>) session.getAttribute("listOfSasStation");
        List<SasControl> listOfSasControl = (List<SasControl>) session.getAttribute("listOfSasControl");
        List<SasPerangkat> listOfSasPerangkat = (List<SasPerangkat>) session.getAttribute("listOfSasPerangkat");
        List<Rectifier> listOfRectifier =(List<Rectifier>) session.getAttribute("listOfRectifier");

        String id = getId();
        String tipe = getTipe();

        if (listOfTpChanel != null && tipe.equalsIgnoreCase("tpchanel")){
            for (TpChanel list :listOfTpChanel){
                if (id.equalsIgnoreCase(list.getChanelId())){
                    setStatus("edit");
                    setTpChanel(list);
                    break;
                }
            }
            return "add_tp";
        }
         else if (listOfPlcChanel != null && tipe.equalsIgnoreCase("plcchanel")){
            for (PlcChanel list :listOfPlcChanel){
                if (id.equalsIgnoreCase(list.getChanelId())){
                    setStatus("edit");
                    setPlcChanel(list);
                    break;
                }
            }
            return "add_plc_chenel";
        }
         else if (listOfKapasitasMux != null && tipe.equalsIgnoreCase("kapasitas")){
            for (KapasitasMux list :listOfKapasitasMux){
                if (id.equalsIgnoreCase(list.getKapasitasId())){
                    setStatus("edit");
                    setKapasitasMux(list);
                    break;
                }
            }
            return "add_kapasitas_mux";
        }
        else if (listOfModulRtu != null && tipe.equalsIgnoreCase("modulrtu")){
            for (ModulRtu list :listOfModulRtu){
                if (id.equalsIgnoreCase(list.getModulId())){
                    setStatus("edit");
                    setModulRtu(list);
                    break;
                }
            }
            return "add_modul_rtu";
        }
        else if (listOfBattery != null && tipe.equalsIgnoreCase("battery")){
            for (Battery list :listOfBattery){
                if (id.equalsIgnoreCase(list.getBatteryId())){
                    setStatus("edit");
                    setBattery(list);
                    break;
                }
            }
            return "add_battery";
        }
        else if (listOfRectifier != null && tipe.equalsIgnoreCase("rectifier")){
            for (Rectifier list :listOfRectifier){
                if (id.equalsIgnoreCase(list.getRectifierId())){
                    setStatus("edit");
                    setRectifier(list);
                    break;
                }
            }
            return "add_rectifier";
        }
        else if (listOfSwitchPort != null && tipe.equalsIgnoreCase("switchport")){
            for (SwitchPort list :listOfSwitchPort){
                if (id.equalsIgnoreCase(list.getSwitchPortId())){
                    setStatus("edit");
                    setSwitchPort(list);
                    break;
                }
            }
            return "add_switch_port";
        }
        else if (listOfBayRtu != null && tipe.equalsIgnoreCase("bayrtu")){
            for (BayRtu list :listOfBayRtu){
                if (id.equalsIgnoreCase(list.getBayId())){
                    setStatus("edit");
                    setBayRtu(list);
                    break;
                }
            }
            return "add_bay_rtu";
        }
        else if (listOfProteksiSetting != null && tipe.equalsIgnoreCase("proteksi")){
            for (ProteksiSetting list :listOfProteksiSetting){
                if (id.equalsIgnoreCase(list.getProteksiSettingId())){
                    setStatus("edit");
                    setProteksiSetting(list);
                    break;
                }
            }
            return "add_proteksi_setting";
        }
        else if (listOfSasStation != null && tipe.equalsIgnoreCase("station")){
            for (SasStation list :listOfSasStation){
                if (id.equalsIgnoreCase(list.getSasStationId())){
                    setStatus("edit");
                    setSasStation(list);
                    break;
                }
            }
            return "add_sas_station";
        }

        else if (listOfSasControl != null && tipe.equalsIgnoreCase("control")){
            for (SasControl list :listOfSasControl){
                if (id.equalsIgnoreCase(list.getSasControlId())){
                    setStatus("edit");
                    setSasControl(list);
                    break;
                }
            }
            return "add_sas_control";
        }

        else if (listOfSasPerangkat != null && tipe.equalsIgnoreCase("perangkat")){
            for (SasPerangkat list :listOfSasPerangkat){
                if (id.equalsIgnoreCase(list.getSasPerangkatId())){
                    setStatus("edit");
                    setSasPerangkat(list);
                    break;
                }
            }
            return "add_sas_perangkat";
        }

        else {
            return "failure";
        }

    }

    public String cekTersedia() {
        logger.info("[AlatDetailAction.search] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfResult = new ArrayList<AlatDetail>();

        String noSap = getId();

        AlatDetail alatDetail = new AlatDetail();
        alatDetail.setNoSap(noSap);

            try {
                listOfResult = alatDetailBoProxy.getByCriteria(alatDetail);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.search");
                } catch (GeneralBOException e1) {
                    logger.error("[AlatDetailAction.search] Error when saving error,", e1);
                }
                logger.error("[AlatDetailAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }

            session.removeAttribute("listOfResult");
            session.setAttribute("listOfResult", listOfResult);

            return "init_add";
        }


    public String cekByNoSap(String noSap, String kodeAlat){
        List<AlatDetail> alatDetails = new ArrayList<AlatDetail>();
        boolean flag = false;
        AlatDetail alatDetail = new AlatDetail();
        alatDetail.setNoSap(noSap);
        alatDetail.setKodeAlat(kodeAlat);

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AlatDetailBo alatDetailBo = (AlatDetailBo) ctx.getBean("alatDetailBoProxy");

        try {
            alatDetails = alatDetailBo.getByCriteria(alatDetail);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.search] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
        }

        if (!alatDetails.isEmpty()){
            flag = true;
        }

        if (flag){
            return "00";
        } else {
            return "01";
        }
    }

    public List<AlatDetail> getListAlat(String noSap, String kodeAlat){
        List<AlatDetail> alatDetails = new ArrayList<AlatDetail>();
        AlatDetail alatDetail = new AlatDetail();
        alatDetail.setNoSap(noSap);
        alatDetail.setKodeAlat(kodeAlat);

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AlatDetailBo alatDetailBo = (AlatDetailBo) ctx.getBean("alatDetailBoProxy");

        try {
            alatDetails = alatDetailBo.getByCriteria(alatDetail);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "AlatDetailAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.search] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
        }

       return alatDetails;
    }

    public String downloadXls() {
        logger.info("[FunctionAction.downloadXls] start process >>>");

        List<AlatDetail> listOfAllAlatDetail = new ArrayList();
        AlatDetail alatDetail = new AlatDetail();
        alatDetail.setFlag("Y");

        try {
            listOfAllAlatDetail = alatDetailBoProxy.getByCriteria(alatDetail);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "alatDetailBo.downloadXls");
            } catch (GeneralBOException e1) {
                logger.error("[FunctionAction.downloadXls] Error when retreiving data for download,", e1);
            }
            logger.error("[FunctionAction.downloadXls] Error when retreiving data for download," + "[" + logId + "] Found problem when retreiving data for download, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when retreiving data for download, please inform to your admin.");
            return ERROR;
        }

        CellDetail cellDetail;
        RowData rowData;
        List listOfData = new ArrayList();
        List listOfCell;
        List listOfColumn = new ArrayList();
        String titleReport = "Report Asset";

        listOfColumn.add("JENIS");
        listOfColumn.add("MERK");
        listOfColumn.add("TYPE");
        listOfColumn.add("NO_SERIES");
        listOfColumn.add("NO_SAP");
        listOfColumn.add("LOKASI_APP");
        listOfColumn.add("LOKASI_GI");
        listOfColumn.add("ARAH_GI");
        listOfColumn.add("KETERANGAN");


        for (AlatDetail data : listOfAllAlatDetail) {
            rowData = new RowData();
            listOfCell = new ArrayList();

            //Jenis
            cellDetail = new CellDetail();
            cellDetail.setCellID(0);
            cellDetail.setValueCell(data.getNamaAlatDetail());
            cellDetail.setAlignmentCell(CellDetail.ALIGN_LEFT);
            listOfCell.add(cellDetail);

            //Merk
            cellDetail = new CellDetail();
            cellDetail.setCellID(1);
            cellDetail.setValueCell(data.getMerk());
            cellDetail.setAlignmentCell(CellDetail.ALIGN_LEFT);
            listOfCell.add(cellDetail);

            //Type
            cellDetail = new CellDetail();
            cellDetail.setCellID(2);
            cellDetail.setValueCell(data.getTypeId());
            cellDetail.setAlignmentCell(CellDetail.ALIGN_LEFT);
            listOfCell.add(cellDetail);

            //No Series
            cellDetail = new CellDetail();
            cellDetail.setCellID(3);
            cellDetail.setValueCell(data.getNoSeries());
            cellDetail.setAlignmentCell(CellDetail.ALIGN_LEFT);
            listOfCell.add(cellDetail);

            //No Sap
            cellDetail = new CellDetail();
            cellDetail.setCellID(4);
            cellDetail.setValueCell(data.getNoSap());
            cellDetail.setAlignmentCell(CellDetail.ALIGN_LEFT);
            listOfCell.add(cellDetail);

            //lokasi APP
            cellDetail = new CellDetail();
            cellDetail.setCellID(5);
            cellDetail.setValueCell(data.getNamaApp());
            cellDetail.setAlignmentCell(CellDetail.ALIGN_LEFT);
            listOfCell.add(cellDetail);

            //lokasi GI
            cellDetail = new CellDetail();
            cellDetail.setCellID(6);
            cellDetail.setValueCell(data.getNamaGi());
            cellDetail.setAlignmentCell(CellDetail.ALIGN_LEFT);
            listOfCell.add(cellDetail);

            //Arah GI
            cellDetail = new CellDetail();
            cellDetail.setCellID(7);
            cellDetail.setValueCell(data.getNamaGiTujuan());
            cellDetail.setAlignmentCell(CellDetail.ALIGN_LEFT);
            listOfCell.add(cellDetail);

            //Keterangan
            cellDetail = new CellDetail();
            cellDetail.setCellID(8);
            cellDetail.setValueCell(data.getNote());
            cellDetail.setAlignmentCell(CellDetail.ALIGN_LEFT);
            listOfCell.add(cellDetail);

            //Mode
//            cellDetail = new CellDetail();
//            cellDetail.setCellID(8);
//            cellDetail.setValueCell(data.getMode());
//            cellDetail.setAlignmentCell(CellDetail.ALIGN_LEFT);
//            listOfCell.add(cellDetail);

            rowData.setListOfCell(listOfCell);
            listOfData.add(rowData);
        }


        HSSFWorkbook wb = DownloadUtil.generateExcelOutput(titleReport, null, listOfColumn, listOfData, null);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            wb.write(baos);
        } catch (IOException e) {
            Long logId = null;
            try {
                logId = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "FunctionBO.downloadXls");
            } catch (GeneralBOException e1) {
                logger.error("[FunctionAction.downloadXls] Error when downloading,", e1);
            }
            logger.error("[FunctionAction.downloadXls] Error when downloading data of function," + "[" + logId + "] Found problem when downloading data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when downloding data, please inform to your admin.");
            return ERROR;
        }

        setExcelStream(new ByteArrayInputStream(baos.toByteArray()));
        setContentDisposition("filename=\"" + "ReportAsset.${documentFormat}\"");

        logger.info("[FunctionAction.downloadXls] end process <<<");

        return "downloadXls";
    }

//    public String addGardu() {
//        logger.info("[GarduAction.add] start process >>>");
////        Gardu addGardu = new Gardu();
////        setGardu(addGardu);
//
//        HttpSession session = ServletActionContext.getRequest().getSession();
//        session.removeAttribute("listOfResult");
//
//        logger.info("[GarduAction.add] stop process <<<");
//        return "init_add_gardu";
//    }

    public String paging(){
        return SUCCESS;
    }

    @Override
    public String add() {
        return null;
    }

    @Override
    public String edit() {
        return null;
    }

    @Override
    public String delete() {
        return null;
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        return null;
    }
}
