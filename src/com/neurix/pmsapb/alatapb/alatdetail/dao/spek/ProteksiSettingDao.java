package com.neurix.pmsapb.alatapb.alatdetail.dao.spek;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.alatapb.alatdetail.model.spek.ImApbBayRtuEntity;
import com.neurix.pmsapb.alatapb.alatdetail.model.spek.ImApbProteksiSettingEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class ProteksiSettingDao extends GenericDao<ImApbProteksiSettingEntity, String> {

    @Override
    protected Class<ImApbProteksiSettingEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ImApbProteksiSettingEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImApbProteksiSettingEntity.class);

        if (mapCriteria!=null) {
            if (mapCriteria.get("kode_alat_detail")!=null) {
                criteria.add(Restrictions.ilike("kodeAlatDetail", "%" + (String)mapCriteria.get("kode_alat_detail")  + "%"));
            }
            if (mapCriteria.get("kode_alat")!=null) {
                criteria.add(Restrictions.ilike("kodeAlat", "%" + (String)mapCriteria.get("kode_alat") + "%" ));
            }
//            if (mapCriteria.get("chanel_id")!=null) {
//                criteria.add(Restrictions.eq("chanelId", (String)mapCriteria.get("chanel_id")));
//            }

        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.asc("proteksiSettingId"));

        List<ImApbProteksiSettingEntity> results = criteria.list();

        return results;
    }

    public String getNextId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_proteksi" + "')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }

//    public List<MtApbAlatDetailEntity> getListAlatDetail(String term) throws HibernateException {
//
//        List<MtApbAlatDetailEntity> results = this.sessionFactory.getCurrentSession().createCriteria(MtApbAlatDetailEntity.class)
//                .add(Restrictions.ilike("namaAlatDetail", term))
//                .add(Restrictions.eq("flag", "Y"))
//                .addOrder(Order.asc("idAlatDetail"))
////                .addOrder(Order.asc("primaryKey.id"))
//                .list();
//        return results;
//    }
}
