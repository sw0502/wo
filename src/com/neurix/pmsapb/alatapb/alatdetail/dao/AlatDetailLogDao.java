package com.neurix.pmsapb.alatapb.alatdetail.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.alatapb.alatdetail.model.MtApbAlatDetailLogEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 18/01/2018.
 */
public class AlatDetailLogDao extends GenericDao<MtApbAlatDetailLogEntity, String> {
    @Override
    protected Class<MtApbAlatDetailLogEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<MtApbAlatDetailLogEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(MtApbAlatDetailLogEntity.class);

        if (mapCriteria!=null) {
            if (mapCriteria.get("kode_log_alat_detail")!=null) {
                criteria.add(Restrictions.ilike("idLogAlatDetail", "%" + (String) mapCriteria.get("kode_log_alat_detail")));
            }
            if (mapCriteria.get("kode_alat_detail")!=null) {
                criteria.add(Restrictions.eq("idAlatDetail", (String) mapCriteria.get("kode_alat_detail")));
            }
            if (mapCriteria.get("kode_alat")!=null) {
                criteria.add(Restrictions.ilike("kodeAlat", "%" + (String)mapCriteria.get("kode_alat") + "%" ));
            }
            if (mapCriteria.get("nama_alat_detail")!=null) {
                criteria.add(Restrictions.ilike("namaAlatDetail", "%" + (String)mapCriteria.get("nama_alat_detail") + "%" ));
            }
        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.desc("idLogAlatDetail"));

        List<MtApbAlatDetailLogEntity> results = criteria.list();

        return results;
    }

    public String getNextAlatDetailLogId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_alat_detail_log" + "')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }
}
