package com.neurix.pmsapb.alatapb.alatdetail.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.alatapb.alatdetail.model.MtApbAlatDetailEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class AlatDetailDao extends GenericDao<MtApbAlatDetailEntity, String> {

    @Override
    protected Class<MtApbAlatDetailEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<MtApbAlatDetailEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(MtApbAlatDetailEntity.class);

        if (mapCriteria!=null) {
            if (mapCriteria.get("kode_alat_detail")!=null) {
                criteria.add(Restrictions.eq("idAlatDetail", (String) mapCriteria.get("kode_alat_detail")));
            }
            if (mapCriteria.get("kode_alat")!=null) {
                criteria.add(Restrictions.ilike("kodeAlat", "%" + (String)mapCriteria.get("kode_alat") + "%" ));
            }
            if (mapCriteria.get("kode_app")!=null) {
                criteria.add(Restrictions.ilike("kodeApp", "%" + (String)mapCriteria.get("kode_app") + "%" ));
            }
            if (mapCriteria.get("no_gi")!=null) {
                criteria.add(Restrictions.ilike("noGi", "%" + (String)mapCriteria.get("no_gi") + "%" ));
            }
            if (mapCriteria.get("nama_alat_detail")!=null) {
                criteria.add(Restrictions.ilike("namaAlatDetail", "%" + (String)mapCriteria.get("nama_alat_detail") + "%" ));
            }
            if (mapCriteria.get("no_sap")!=null) {
                criteria.add(Restrictions.ilike("noSap", "%" + (String)mapCriteria.get("no_sap") + "%" ));
            }
            if (mapCriteria.get("status")!=null) {
                criteria.add(Restrictions.eq("status",  (String)mapCriteria.get("status")));
            }
            if (mapCriteria.get("no_aktiva")!=null) {
                criteria.add(Restrictions.eq("noAktiva",  (String)mapCriteria.get("no_aktiva")));
            }

        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.asc("idAlatDetail"));

        List<MtApbAlatDetailEntity> results = criteria.list();

        return results;
    }

    public String getNextAlatDetailId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_alat_detail" + "')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }

    public List<MtApbAlatDetailEntity> getListAlatDetail(String term) throws HibernateException {

        List<MtApbAlatDetailEntity> results = this.sessionFactory.getCurrentSession().createCriteria(MtApbAlatDetailEntity.class)
                .add(Restrictions.ilike("namaAlatDetail", term))
                .add(Restrictions.eq("flag", "Y"))
                .addOrder(Order.asc("idAlatDetail"))
//                .addOrder(Order.asc("primaryKey.id"))
                .list();
        return results;
    }
}
