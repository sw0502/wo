package com.neurix.pmsapb.alatapb.alatdetail.model;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class TpChanel extends BaseModel implements Serializable {
    private String chanelId;
    private String chanel;
    private String kodeAlatDetail;
    private String kodeAlat;
    private String fungsiChanel;

    private boolean edit = false;
    private boolean add = false;
    private boolean delete = false;

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public String getChanelId() {
        return chanelId;
    }

    public void setChanelId(String chanelId) {
        this.chanelId = chanelId;
    }

    public String getChanel() {
        return chanel;
    }

    public void setChanel(String chanel) {
        this.chanel = chanel;
    }

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getFungsiChanel() {
        return fungsiChanel;
    }

    public void setFungsiChanel(String fungsiChanel) {
        this.fungsiChanel = fungsiChanel;
    }
}
