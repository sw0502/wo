package com.neurix.pmsapb.alatapb.alatdetail.model;

import com.neurix.common.model.BaseModel;

import javax.xml.soap.SAAJResult;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class AlatDetail extends BaseModel implements Serializable {
    private String idAlatDetail;
    private String kodeAlat;
    private String kodeApp;
    private String noGi;
    private String namaGi;
    private String noGiTujuan;
    private String namaGiTujuan;
    private String namaAlatDetail;
    private String merk;
    private String tegangan;
    private String typeId;
    private String noSeries;
    private String kdStatus;
    private Date tglOprs;
    private Date tglPasang;
    private String keterangan;
    private String jenisTp;
    private String frekuensiTx;
    private String frekuensiRx;
    private String daya;
    private String transmisi;
    private String flag;
    private String action;
    private Timestamp lastUpdate;
    private String lastUpdateWho;
    private String noSap;
    private String noAktiva;
    private String status;
    private String pax;
    private String typePax;
    private String powerSupply;
    private String ruangTerpasang;
    private String typeSwitch;
    private String longt;
    private String lat;
    private String techidentno;
    private String typePlc;
    private String bandwith;
    private String namaRtu;
    private String idRtu;
    private String idBidang;
    private String namaBidang;
    private String idSubBidang;
    private String namaSubBidang;
    private String batteryName;
    private String jumlahCell;
    private String kapasitas;
    private String fuse;
    private String fungsi;
    private String teganganInput;
    private String teganganOutput;
    private String mcbInput;
    private String mcbOutput;
    private String rectifierName;
    private String phase;
    private String mode;


    private String enJenisTp;
    private String enFrekuensiTx;
    private String enFrekuensiRx;
    private String enDaya;
    private String enTransmisi;
    private String enTegOprs;
    private String enBatteryName;
    private String enJumlahCell;
    private String enKapasitas;
    private String enFuse;
    private String enFungsi;
    private String enTeganganInput;
    private String enTeganganOutput;
    private String enMcbInput;
    private String enMcbOutput;
    private String enRectifierName;
    private String enPhase;

    private String stTglOprs;
    private String stTglPasang;

    private String namaApp;

    private String urlFoto;
    private String note;
    private String filePath;

    private String imgFront;
    private String imgBack;
    private String imgLeft;
    private String imgRight;
    private String docName;

    private String uploadFront;
    private String uploadBack;
    private String uploadLeft;
    private String uploadRight;
    private String uploadDoc;

    private String idLogAlatDetail;

    private boolean enTpChanel = false;
    private boolean enPlcChanel = false;
    private boolean enKapasitasMux = false;
    private boolean enRtu = false;
    private boolean enSwitchPort = false;
    private boolean enBattery = false;
    private boolean enRectifier = false;
    private boolean enBtn = false;

    public boolean isEnBtn() {
        return enBtn;
    }

    public void setEnBtn(boolean enBtn) {
        this.enBtn = enBtn;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getEnBatteryName() {
        return enBatteryName;
    }

    public void setEnBatteryName(String enBatteryName) {
        this.enBatteryName = enBatteryName;
    }

    public String getEnJumlahCell() {
        return enJumlahCell;
    }

    public void setEnJumlahCell(String enJumlahCell) {
        this.enJumlahCell = enJumlahCell;
    }

    public String getEnKapasitas() {
        return enKapasitas;
    }

    public void setEnKapasitas(String enKapasitas) {
        this.enKapasitas = enKapasitas;
    }

    public String getEnFuse() {
        return enFuse;
    }

    public void setEnFuse(String enFuse) {
        this.enFuse = enFuse;
    }

    public String getEnFungsi() {
        return enFungsi;
    }

    public void setEnFungsi(String enFungsi) {
        this.enFungsi = enFungsi;
    }

    public String getEnTeganganInput() {
        return enTeganganInput;
    }

    public void setEnTeganganInput(String enTeganganInput) {
        this.enTeganganInput = enTeganganInput;
    }

    public String getEnTeganganOutput() {
        return enTeganganOutput;
    }

    public void setEnTeganganOutput(String enTeganganOutput) {
        this.enTeganganOutput = enTeganganOutput;
    }

    public String getEnMcbInput() {
        return enMcbInput;
    }

    public void setEnMcbInput(String enMcbInput) {
        this.enMcbInput = enMcbInput;
    }

    public String getEnMcbOutput() {
        return enMcbOutput;
    }

    public void setEnMcbOutput(String enMcbOutput) {
        this.enMcbOutput = enMcbOutput;
    }

    public String getEnRectifierName() {
        return enRectifierName;
    }

    public void setEnRectifierName(String enRectifierName) {
        this.enRectifierName = enRectifierName;
    }

    public String getEnPhase() {
        return enPhase;
    }

    public void setEnPhase(String enPhase) {
        this.enPhase = enPhase;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getRectifierName() {
        return rectifierName;
    }

    public void setRectifierName(String rectifierName) {
        this.rectifierName = rectifierName;
    }

    public String getTeganganInput() {
        return teganganInput;
    }

    public void setTeganganInput(String teganganInput) {
        this.teganganInput = teganganInput;
    }

    public String getTeganganOutput() {
        return teganganOutput;
    }

    public void setTeganganOutput(String teganganOutput) {
        this.teganganOutput = teganganOutput;
    }

    public String getMcbInput() {
        return mcbInput;
    }

    public void setMcbInput(String mcbInput) {
        this.mcbInput = mcbInput;
    }

    public String getMcbOutput() {
        return mcbOutput;
    }

    public void setMcbOutput(String mcbOutput) {
        this.mcbOutput = mcbOutput;
    }

    public boolean isEnRectifier() {
        return enRectifier;
    }

    public void setEnRectifier(boolean enRectifier) {
        this.enRectifier = enRectifier;
    }

    public String getBatteryName() {
        return batteryName;
    }

    public void setBatteryName(String batteryName) {
        this.batteryName = batteryName;
    }

    public String getJumlahCell() {
        return jumlahCell;
    }

    public void setJumlahCell(String jumlahCell) {
        this.jumlahCell = jumlahCell;
    }

    public String getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(String kapasitas) {
        this.kapasitas = kapasitas;
    }

    public String getFuse() {
        return fuse;
    }

    public void setFuse(String fuse) {
        this.fuse = fuse;
    }

    public String getFungsi() {
        return fungsi;
    }

    public void setFungsi(String fungsi) {
        this.fungsi = fungsi;
    }

    public boolean isEnBattery() {
        return enBattery;
    }

    public void setEnBattery(boolean enBattery) {
        this.enBattery = enBattery;
    }

    public boolean isEnSwitchPort() {
        return enSwitchPort;
    }

    public void setEnSwitchPort(boolean enSwitchPort) {
        this.enSwitchPort = enSwitchPort;
    }

    public boolean isEnTpChanel() {
        return enTpChanel;
    }

    public void setEnTpChanel(boolean enTpChanel) {
        this.enTpChanel = enTpChanel;
    }

    public boolean isEnPlcChanel() {
        return enPlcChanel;
    }

    public void setEnPlcChanel(boolean enPlcChanel) {
        this.enPlcChanel = enPlcChanel;
    }

    public boolean isEnKapasitasMux() {
        return enKapasitasMux;
    }

    public void setEnKapasitasMux(boolean enKapasitasMux) {
        this.enKapasitasMux = enKapasitasMux;
    }

    public boolean isEnRtu() {
        return enRtu;
    }

    public void setEnRtu(boolean enRtu) {
        this.enRtu = enRtu;
    }

    public String getIdBidang() {
        return idBidang;
    }

    public void setIdBidang(String idBidang) {
        this.idBidang = idBidang;
    }

    public String getNamaBidang() {
        return namaBidang;
    }

    public void setNamaBidang(String namaBidang) {
        this.namaBidang = namaBidang;
    }

    public String getIdSubBidang() {
        return idSubBidang;
    }

    public void setIdSubBidang(String idSubBidang) {
        this.idSubBidang = idSubBidang;
    }

    public String getNamaSubBidang() {
        return namaSubBidang;
    }

    public void setNamaSubBidang(String namaSubBidang) {
        this.namaSubBidang = namaSubBidang;
    }

    public String getTypePlc() {
        return typePlc;
    }

    public void setTypePlc(String typePlc) {
        this.typePlc = typePlc;
    }

    public String getBandwith() {
        return bandwith;
    }

    public void setBandwith(String bandwith) {
        this.bandwith = bandwith;
    }

    public String getNamaRtu() {
        return namaRtu;
    }

    public void setNamaRtu(String namaRtu) {
        this.namaRtu = namaRtu;
    }

    public String getIdRtu() {
        return idRtu;
    }

    public void setIdRtu(String idRtu) {
        this.idRtu = idRtu;
    }

    public String getTechidentno() {
        return techidentno;
    }

    public void setTechidentno(String techidentno) {
        this.techidentno = techidentno;
    }

    private String thumbFoto;

    public String getPax() {
        return pax;
    }

    public void setPax(String pax) {
        this.pax = pax;
    }

    public String getTypePax() {
        return typePax;
    }

    public void setTypePax(String typePax) {
        this.typePax = typePax;
    }

    public String getPowerSupply() {
        return powerSupply;
    }

    public void setPowerSupply(String powerSupply) {
        this.powerSupply = powerSupply;
    }

    public String getRuangTerpasang() {
        return ruangTerpasang;
    }

    public void setRuangTerpasang(String ruangTerpasang) {
        this.ruangTerpasang = ruangTerpasang;
    }

    public String getTypeSwitch() {
        return typeSwitch;
    }

    public void setTypeSwitch(String typeSwitch) {
        this.typeSwitch = typeSwitch;
    }

    public String getLongt() {
        return longt;
    }

    public void setLongt(String longt) {
        this.longt = longt;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getThumbFoto() {
        return thumbFoto;
    }

    public void setThumbFoto(String thumbFoto) {
        this.thumbFoto = thumbFoto;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getNoAktiva() {
        return noAktiva;
    }

    public void setNoAktiva(String noAktiva) {
        this.noAktiva = noAktiva;
    }

    public String getEnJenisTp() {
        return enJenisTp;
    }

    public void setEnJenisTp(String enJenisTp) {
        this.enJenisTp = enJenisTp;
    }

    public String getEnFrekuensiTx() {
        return enFrekuensiTx;
    }

    public void setEnFrekuensiTx(String enFrekuensiTx) {
        this.enFrekuensiTx = enFrekuensiTx;
    }

    public String getEnFrekuensiRx() {
        return enFrekuensiRx;
    }

    public void setEnFrekuensiRx(String enFrekuensiRx) {
        this.enFrekuensiRx = enFrekuensiRx;
    }

    public String getEnDaya() {
        return enDaya;
    }

    public void setEnDaya(String enDaya) {
        this.enDaya = enDaya;
    }

    public String getEnTransmisi() {
        return enTransmisi;
    }

    public void setEnTransmisi(String enTransmisi) {
        this.enTransmisi = enTransmisi;
    }

    public String getEnTegOprs() {
        return enTegOprs;
    }

    public void setEnTegOprs(String enTegOprs) {
        this.enTegOprs = enTegOprs;
    }

    public String getNoSap() {
        return noSap;
    }

    public void setNoSap(String noSap) {
        this.noSap = noSap;
    }

    public String getIdLogAlatDetail() {
        return idLogAlatDetail;
    }

    public void setIdLogAlatDetail(String idLogAlatDetail) {
        this.idLogAlatDetail = idLogAlatDetail;
    }

    public String getUploadFront() {
        return uploadFront;
    }

    public void setUploadFront(String uploadFront) {
        this.uploadFront = uploadFront;
    }

    public String getUploadBack() {
        return uploadBack;
    }

    public void setUploadBack(String uploadBack) {
        this.uploadBack = uploadBack;
    }

    public String getUploadLeft() {
        return uploadLeft;
    }

    public void setUploadLeft(String uploadLeft) {
        this.uploadLeft = uploadLeft;
    }

    public String getUploadRight() {
        return uploadRight;
    }

    public void setUploadRight(String uploadRight) {
        this.uploadRight = uploadRight;
    }

    public String getImgFront() {
        return imgFront;
    }

    public void setImgFront(String imgFront) {
        this.imgFront = imgFront;
    }

    public String getImgBack() {
        return imgBack;
    }

    public void setImgBack(String imgBack) {
        this.imgBack = imgBack;
    }

    public String getImgLeft() {
        return imgLeft;
    }

    public void setImgLeft(String imgLeft) {
        this.imgLeft = imgLeft;
    }

    public String getImgRight() {
        return imgRight;
    }

    public void setImgRight(String imgRight) {
        this.imgRight = imgRight;
    }

    private boolean deleted = false;
    private String statusDelete;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatusDelete() {
        return statusDelete;
    }

    public void setStatusDelete(String statusDelete) {
        this.statusDelete = statusDelete;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getNamaApp() {
        return namaApp;
    }

    public void setNamaApp(String namaApp) {
        this.namaApp = namaApp;
    }

    public String getStTglOprs() {
        return stTglOprs;
    }

    public void setStTglOprs(String stTglOprs) {
        this.stTglOprs = stTglOprs;
    }

    public String getStTglPasang() {
        return stTglPasang;
    }

    public void setStTglPasang(String stTglPasang) {
        this.stTglPasang = stTglPasang;
    }

    @Override
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getKodeApp() {
        return kodeApp;
    }

    public void setKodeApp(String kodeApp) {
        this.kodeApp = kodeApp;
    }

    public String getNoGi() {
        return noGi;
    }

    public void setNoGi(String noGi) {
        this.noGi = noGi;
    }

    public String getNamaGi() {
        return namaGi;
    }

    public void setNamaGi(String namaGi) {
        this.namaGi = namaGi;
    }

    public String getNoGiTujuan() {
        return noGiTujuan;
    }

    public void setNoGiTujuan(String noGiTujuan) {
        this.noGiTujuan = noGiTujuan;
    }

    public String getNamaGiTujuan() {
        return namaGiTujuan;
    }

    public void setNamaGiTujuan(String namaGiTujuan) {
        this.namaGiTujuan = namaGiTujuan;
    }

    public String getNamaAlatDetail() {
        return namaAlatDetail;
    }

    public void setNamaAlatDetail(String namaAlatDetail) {
        this.namaAlatDetail = namaAlatDetail;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTegangan() {
        return tegangan;
    }

    public void setTegangan(String tegangan) {
        this.tegangan = tegangan;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getNoSeries() {
        return noSeries;
    }

    public void setNoSeries(String noSeries) {
        this.noSeries = noSeries;
    }

    public String getKdStatus() {
        return kdStatus;
    }

    public void setKdStatus(String kdStatus) {
        this.kdStatus = kdStatus;
    }

    public Date getTglOprs() {
        return tglOprs;
    }

    public void setTglOprs(Date tglOprs) {
        this.tglOprs = tglOprs;
    }

    public Date getTglPasang() {
        return tglPasang;
    }

    public void setTglPasang(Date tglPasang) {
        this.tglPasang = tglPasang;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getJenisTp() {
        return jenisTp;
    }

    public void setJenisTp(String jenisTp) {
        this.jenisTp = jenisTp;
    }

    public String getFrekuensiTx() {
        return frekuensiTx;
    }

    public void setFrekuensiTx(String frekuensiTx) {
        this.frekuensiTx = frekuensiTx;
    }

    public String getFrekuensiRx() {
        return frekuensiRx;
    }

    public void setFrekuensiRx(String frekuensiRx) {
        this.frekuensiRx = frekuensiRx;
    }

    public String getDaya() {
        return daya;
    }

    public void setDaya(String daya) {
        this.daya = daya;
    }

    public String getTransmisi() {
        return transmisi;
    }

    public void setTransmisi(String transmisi) {
        this.transmisi = transmisi;
    }

    public String getUploadDoc() {
        return uploadDoc;
    }

    public void setUploadDoc(String uploadDoc) {
        this.uploadDoc = uploadDoc;
    }

}
