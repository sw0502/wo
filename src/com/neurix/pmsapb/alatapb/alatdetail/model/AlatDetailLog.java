package com.neurix.pmsapb.alatapb.alatdetail.model;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 19/01/2018.
 */
public class AlatDetailLog extends BaseModel implements Serializable {
    private String idLogAlatDetail;
    private String idAlatDetail;
    private String kodeAlat;
    private String kodeApp;
    private String noGi;
    private String namaGi;
    private String noGiTujuan;
    private String namaGiTujuan;
    private String namaAlatDetail;
    private String merk;
    private String tegangan;
    private String typeId;
    private String noSeries;
    private String kdStatus;
    private Date tglOprs;
    private Date tglPasang;
    private String keterangan;
    private String jenisTp;
    private String frekuensiTx;
    private String frekuensiRx;
    private String daya;
    private String transmisi;
    private String flag;
    private String action;
    private Timestamp lastUpdate;
    private String lastUpdateWho;
    private String status;

    private String stTglOprs;
    private String stTglPasang;

    private String namaApp;

    private String urlFoto;
    private String note;
    private String filePath;

    private String imgFront;
    private String imgBack;
    private String imgLeft;
    private String imgRight;

    private String uploadFront;
    private String uploadBack;
    private String uploadLeft;
    private String uploadRight;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdLogAlatDetail() {
        return idLogAlatDetail;
    }

    public void setIdLogAlatDetail(String idLogAlatDetail) {
        this.idLogAlatDetail = idLogAlatDetail;
    }

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getKodeApp() {
        return kodeApp;
    }

    public void setKodeApp(String kodeApp) {
        this.kodeApp = kodeApp;
    }

    public String getNoGi() {
        return noGi;
    }

    public void setNoGi(String noGi) {
        this.noGi = noGi;
    }

    public String getNamaGi() {
        return namaGi;
    }

    public void setNamaGi(String namaGi) {
        this.namaGi = namaGi;
    }

    public String getNoGiTujuan() {
        return noGiTujuan;
    }

    public void setNoGiTujuan(String noGiTujuan) {
        this.noGiTujuan = noGiTujuan;
    }

    public String getNamaGiTujuan() {
        return namaGiTujuan;
    }

    public void setNamaGiTujuan(String namaGiTujuan) {
        this.namaGiTujuan = namaGiTujuan;
    }

    public String getNamaAlatDetail() {
        return namaAlatDetail;
    }

    public void setNamaAlatDetail(String namaAlatDetail) {
        this.namaAlatDetail = namaAlatDetail;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTegangan() {
        return tegangan;
    }

    public void setTegangan(String tegangan) {
        this.tegangan = tegangan;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getNoSeries() {
        return noSeries;
    }

    public void setNoSeries(String noSeries) {
        this.noSeries = noSeries;
    }

    public String getKdStatus() {
        return kdStatus;
    }

    public void setKdStatus(String kdStatus) {
        this.kdStatus = kdStatus;
    }

    public Date getTglOprs() {
        return tglOprs;
    }

    public void setTglOprs(Date tglOprs) {
        this.tglOprs = tglOprs;
    }

    public Date getTglPasang() {
        return tglPasang;
    }

    public void setTglPasang(Date tglPasang) {
        this.tglPasang = tglPasang;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getJenisTp() {
        return jenisTp;
    }

    public void setJenisTp(String jenisTp) {
        this.jenisTp = jenisTp;
    }

    public String getFrekuensiTx() {
        return frekuensiTx;
    }

    public void setFrekuensiTx(String frekuensiTx) {
        this.frekuensiTx = frekuensiTx;
    }

    public String getFrekuensiRx() {
        return frekuensiRx;
    }

    public void setFrekuensiRx(String frekuensiRx) {
        this.frekuensiRx = frekuensiRx;
    }

    public String getDaya() {
        return daya;
    }

    public void setDaya(String daya) {
        this.daya = daya;
    }

    public String getTransmisi() {
        return transmisi;
    }

    public void setTransmisi(String transmisi) {
        this.transmisi = transmisi;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    @Override
    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public String getStTglOprs() {
        return stTglOprs;
    }

    public void setStTglOprs(String stTglOprs) {
        this.stTglOprs = stTglOprs;
    }

    public String getStTglPasang() {
        return stTglPasang;
    }

    public void setStTglPasang(String stTglPasang) {
        this.stTglPasang = stTglPasang;
    }

    public String getNamaApp() {
        return namaApp;
    }

    public void setNamaApp(String namaApp) {
        this.namaApp = namaApp;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getImgFront() {
        return imgFront;
    }

    public void setImgFront(String imgFront) {
        this.imgFront = imgFront;
    }

    public String getImgBack() {
        return imgBack;
    }

    public void setImgBack(String imgBack) {
        this.imgBack = imgBack;
    }

    public String getImgLeft() {
        return imgLeft;
    }

    public void setImgLeft(String imgLeft) {
        this.imgLeft = imgLeft;
    }

    public String getImgRight() {
        return imgRight;
    }

    public void setImgRight(String imgRight) {
        this.imgRight = imgRight;
    }

    public String getUploadFront() {
        return uploadFront;
    }

    public void setUploadFront(String uploadFront) {
        this.uploadFront = uploadFront;
    }

    public String getUploadBack() {
        return uploadBack;
    }

    public void setUploadBack(String uploadBack) {
        this.uploadBack = uploadBack;
    }

    public String getUploadLeft() {
        return uploadLeft;
    }

    public void setUploadLeft(String uploadLeft) {
        this.uploadLeft = uploadLeft;
    }

    public String getUploadRight() {
        return uploadRight;
    }

    public void setUploadRight(String uploadRight) {
        this.uploadRight = uploadRight;
    }
}
