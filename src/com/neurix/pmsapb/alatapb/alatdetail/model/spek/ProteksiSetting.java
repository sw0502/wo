package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class ProteksiSetting extends BaseModel implements Serializable {
    private String proteksiSettingId;
    private String tahap;
    private String arus;
    private String target;
    private String waktu;
    private String kodeAlatDetail;
    private String kodeAlat;

    private boolean edit = false;
    private boolean add = false;
    private boolean delete = false;

    public String getProteksiSettingId() {
        return proteksiSettingId;
    }

    public void setProteksiSettingId(String proteksiSettingId) {
        this.proteksiSettingId = proteksiSettingId;
    }

    public String getTahap() {
        return tahap;
    }

    public void setTahap(String tahap) {
        this.tahap = tahap;
    }

    public String getArus() {
        return arus;
    }

    public void setArus(String arus) {
        this.arus = arus;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
