package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class SasStation extends BaseModel implements Serializable {
    private String sasStationId;
    private String namaSatation;
    private String merk;
    private String tipeStation;
    private String tipe;
    private String namaSoftWare;
    private String versiSoftware;
    private String jumlah;
    private String sn;
    private String kodeAlatDetail;
    private String kodeAlat;

    private boolean edit = false;
    private boolean add = false;
    private boolean delete = false;

    public String getSasStationId() {
        return sasStationId;
    }

    public void setSasStationId(String sasStationId) {
        this.sasStationId = sasStationId;
    }

    public String getNamaSatation() {
        return namaSatation;
    }

    public void setNamaSatation(String namaSatation) {
        this.namaSatation = namaSatation;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTipeStation() {
        return tipeStation;
    }

    public void setTipeStation(String tipeStation) {
        this.tipeStation = tipeStation;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getNamaSoftWare() {
        return namaSoftWare;
    }

    public void setNamaSoftWare(String namaSoftWare) {
        this.namaSoftWare = namaSoftWare;
    }

    public String getVersiSoftware() {
        return versiSoftware;
    }

    public void setVersiSoftware(String versiSoftware) {
        this.versiSoftware = versiSoftware;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
