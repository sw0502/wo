package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class ImApbSasStationEntity implements Serializable {

    private String sasStationId;
    private String namaStation;
    private String merk;
    private String tipeStation;
    private String tipe;
    private String namaSoftware;
    private String versiSoftware;
    private String jumlah;
    private String sn;
    private String kodeAlatDetail;
    private String kodeAlat;
    private String flag;
    private String action;
    private Timestamp createdDate;
    private String createdWho;
    private Timestamp lastUpdate;
    private String lastUpdateWho;


    public String getSasStationId() {
        return sasStationId;
    }

    public void setSasStationId(String sasStationId) {
        this.sasStationId = sasStationId;
    }

    public String getNamaStation() {
        return namaStation;
    }

    public void setNamaStation(String namaStation) {
        this.namaStation = namaStation;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTipeStation() {
        return tipeStation;
    }

    public void setTipeStation(String tipeStation) {
        this.tipeStation = tipeStation;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getNamaSoftware() {
        return namaSoftware;
    }

    public void setNamaSoftware(String namaSoftware) {
        this.namaSoftware = namaSoftware;
    }

    public String getVersiSoftware() {
        return versiSoftware;
    }

    public void setVersiSoftware(String versiSoftware) {
        this.versiSoftware = versiSoftware;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedWho() {
        return createdWho;
    }

    public void setCreatedWho(String createdWho) {
        this.createdWho = createdWho;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }
}
