package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class SwitchPort extends BaseModel implements Serializable {
    private String switchPortId;
    private String port;
    private String kodeAlatDetail;
    private String kodeAlat;
    private String fungsiPort;
    private String type;

    private boolean edit = false;
    private boolean add = false;
    private boolean delete = false;

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public String getSwitchPortId() {
        return switchPortId;
    }

    public void setSwitchPortId(String switchPortId) {
        this.switchPortId = switchPortId;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getFungsiPort() {
        return fungsiPort;
    }

    public void setFungsiPort(String fungsiPort) {
        this.fungsiPort = fungsiPort;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
