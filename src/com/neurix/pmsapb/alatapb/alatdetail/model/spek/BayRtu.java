package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class BayRtu extends BaseModel implements Serializable {
    private String bayId;
    private String namaBay;
    private String jumlahDi;
    private String jumlahDo;
    private String jumlahAo;
    private String jumlahAi;
    private String merk;
    private String ratioVt;
    private String ratioCt;
    private String kodeAlatDetail;
    private String kodeAlat;

    private boolean edit = false;
    private boolean add = false;
    private boolean delete = false;

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public String getNamaBay() {
        return namaBay;
    }

    public void setNamaBay(String namaBay) {
        this.namaBay = namaBay;
    }

    public String getBayId() {
        return bayId;
    }

    public void setBayId(String bayId) {
        this.bayId = bayId;
    }

    public String getJumlahDi() {
        return jumlahDi;
    }

    public void setJumlahDi(String jumlahDi) {
        this.jumlahDi = jumlahDi;
    }

    public String getJumlahDo() {
        return jumlahDo;
    }

    public void setJumlahDo(String jumlahDo) {
        this.jumlahDo = jumlahDo;
    }

    public String getJumlahAo() {
        return jumlahAo;
    }

    public void setJumlahAo(String jumlahAo) {
        this.jumlahAo = jumlahAo;
    }

    public String getJumlahAi() {
        return jumlahAi;
    }

    public void setJumlahAi(String jumlahAi) {
        this.jumlahAi = jumlahAi;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getRatioVt() {
        return ratioVt;
    }

    public void setRatioVt(String ratioVt) {
        this.ratioVt = ratioVt;
    }

    public String getRatioCt() {
        return ratioCt;
    }

    public void setRatioCt(String ratioCt) {
        this.ratioCt = ratioCt;
    }

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }
}
