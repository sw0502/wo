package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class SasControl extends BaseModel implements Serializable {
    private String sasControlId;
    private String namaControl;
    private String merk;
    private String tipeControl;
    private String tipe;
    private String namaSoftWare;
    private String versiSoftware;
    private String jumlahDi;
    private String jumlahDo;
    private String jumlahAi;
    private String ratioVt;
    private String ratioCt;
    private String sn;
    private String kodeAlatDetail;
    private String kodeAlat;

    private boolean edit = false;
    private boolean add = false;
    private boolean delete = false;

    public String getSasControlId() {
        return sasControlId;
    }

    public void setSasControlId(String sasControlId) {
        this.sasControlId = sasControlId;
    }

    public String getNamaControl() {
        return namaControl;
    }

    public void setNamaControl(String namaControl) {
        this.namaControl = namaControl;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTipeControl() {
        return tipeControl;
    }

    public void setTipeControl(String tipeControl) {
        this.tipeControl = tipeControl;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getNamaSoftWare() {
        return namaSoftWare;
    }

    public void setNamaSoftWare(String namaSoftWare) {
        this.namaSoftWare = namaSoftWare;
    }

    public String getVersiSoftware() {
        return versiSoftware;
    }

    public void setVersiSoftware(String versiSoftware) {
        this.versiSoftware = versiSoftware;
    }

    public String getJumlahDi() {
        return jumlahDi;
    }

    public void setJumlahDi(String jumlahDi) {
        this.jumlahDi = jumlahDi;
    }

    public String getJumlahDo() {
        return jumlahDo;
    }

    public void setJumlahDo(String jumlahDo) {
        this.jumlahDo = jumlahDo;
    }

    public String getJumlahAi() {
        return jumlahAi;
    }

    public void setJumlahAi(String jumlahAi) {
        this.jumlahAi = jumlahAi;
    }

    public String getRatioVt() {
        return ratioVt;
    }

    public void setRatioVt(String ratioVt) {
        this.ratioVt = ratioVt;
    }

    public String getRatioCt() {
        return ratioCt;
    }

    public void setRatioCt(String ratioCt) {
        this.ratioCt = ratioCt;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
