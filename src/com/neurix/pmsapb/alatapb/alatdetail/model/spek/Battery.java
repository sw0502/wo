package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class Battery extends BaseModel implements Serializable {
    private String batteryId;
    private String batteryName;
    private String tegangan;
    private String jumlahCell;
    private String merk;
    private String type;
    private String kapasitas;
    private String fuse;
    private String fungsi;
    private String kodeAlatDetail;
    private String kodeAlat;

    private boolean edit = false;
    private boolean add = false;
    private boolean delete = false;

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public String getBatteryId() {
        return batteryId;
    }

    public void setBatteryId(String batteryId) {
        this.batteryId = batteryId;
    }

    public String getBatteryName() {
        return batteryName;
    }

    public void setBatteryName(String batteryName) {
        this.batteryName = batteryName;
    }

    public String getTegangan() {
        return tegangan;
    }

    public void setTegangan(String tegangan) {
        this.tegangan = tegangan;
    }

    public String getJumlahCell() {
        return jumlahCell;
    }

    public void setJumlahCell(String jumlahCell) {
        this.jumlahCell = jumlahCell;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFuse() {
        return fuse;
    }

    public void setFuse(String fuse) {
        this.fuse = fuse;
    }

    public String getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(String kapasitas) {
        this.kapasitas = kapasitas;
    }

    public String getFungsi() {
        return fungsi;
    }

    public void setFungsi(String fungsi) {
        this.fungsi = fungsi;
    }
}
