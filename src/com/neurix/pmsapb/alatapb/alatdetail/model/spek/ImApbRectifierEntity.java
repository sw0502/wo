package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class ImApbRectifierEntity implements Serializable {
    private String rectifierId;
    private String rectifierName;
    private String kodeAlatDetail;
    private String kodeAlat;
    private String panjangAcdb;
    private String panjangBattery;
    private String panjangDcdb;
    private String typeAcdb;
    private String typeBattery;
    private String typeDcdb;
    private String flag;
    private String action;
    private Timestamp createdDate;
    private String createdWho;
    private Timestamp lastUpdate;
    private String lastUpdateWho;

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedWho() {
        return createdWho;
    }

    public void setCreatedWho(String createdWho) {
        this.createdWho = createdWho;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public String getRectifierId() {
        return rectifierId;
    }

    public void setRectifierId(String rectifierId) {
        this.rectifierId = rectifierId;
    }

    public String getRectifierName() {
        return rectifierName;
    }

    public void setRectifierName(String rectifierName) {
        this.rectifierName = rectifierName;
    }

    public String getPanjangAcdb() {
        return panjangAcdb;
    }

    public void setPanjangAcdb(String panjangAcdb) {
        this.panjangAcdb = panjangAcdb;
    }

    public String getPanjangBattery() {
        return panjangBattery;
    }

    public void setPanjangBattery(String panjangBattery) {
        this.panjangBattery = panjangBattery;
    }

    public String getPanjangDcdb() {
        return panjangDcdb;
    }

    public void setPanjangDcdb(String panjangDcdb) {
        this.panjangDcdb = panjangDcdb;
    }

    public String getTypeAcdb() {
        return typeAcdb;
    }

    public void setTypeAcdb(String typeAcdb) {
        this.typeAcdb = typeAcdb;
    }

    public String getTypeBattery() {
        return typeBattery;
    }

    public void setTypeBattery(String typeBattery) {
        this.typeBattery = typeBattery;
    }

    public String getTypeDcdb() {
        return typeDcdb;
    }

    public void setTypeDcdb(String typeDcdb) {
        this.typeDcdb = typeDcdb;
    }
}
