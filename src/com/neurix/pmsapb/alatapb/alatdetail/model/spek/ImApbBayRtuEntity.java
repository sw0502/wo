package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class ImApbBayRtuEntity implements Serializable {
    private String bayId;
    private String namaBay;
    private String jumlahDi;
    private String jumlahDo;
    private String jumlahAo;
    private String jumlahAi;
    private String merk;
    private String ratioVt;
    private String ratioCt;
    private String kodeAlatDetail;
    private String kodeAlat;
    private String flag;
    private String action;
    private Timestamp createdDate;
    private String createdWho;
    private Timestamp lastUpdate;
    private String lastUpdateWho;

    public String getNamaBay() {
        return namaBay;
    }

    public void setNamaBay(String namaBay) {
        this.namaBay = namaBay;
    }

    public String getBayId() {
        return bayId;
    }

    public void setBayId(String bayId) {
        this.bayId = bayId;
    }

    public String getJumlahDi() {
        return jumlahDi;
    }

    public void setJumlahDi(String jumlahDi) {
        this.jumlahDi = jumlahDi;
    }

    public String getJumlahDo() {
        return jumlahDo;
    }

    public void setJumlahDo(String jumlahDo) {
        this.jumlahDo = jumlahDo;
    }

    public String getJumlahAo() {
        return jumlahAo;
    }

    public void setJumlahAo(String jumlahAo) {
        this.jumlahAo = jumlahAo;
    }

    public String getJumlahAi() {
        return jumlahAi;
    }

    public void setJumlahAi(String jumlahAi) {
        this.jumlahAi = jumlahAi;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getRatioVt() {
        return ratioVt;
    }

    public void setRatioVt(String ratioVt) {
        this.ratioVt = ratioVt;
    }

    public String getRatioCt() {
        return ratioCt;
    }

    public void setRatioCt(String ratioCt) {
        this.ratioCt = ratioCt;
    }

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedWho() {
        return createdWho;
    }

    public void setCreatedWho(String createdWho) {
        this.createdWho = createdWho;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }
}
