package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class Rectifier extends BaseModel implements Serializable {
    private String rectifierId;
    private String rectifierName;
    private String panjangAcdb;
    private String panjangBattery;
    private String panjangDcdb;
    private String typeAcdb;
    private String typeBattery;
    private String typeDcdb;
    private String kodeAlatDetail;
    private String kodeAlat;

    private boolean edit = false;
    private boolean add = false;
    private boolean delete = false;

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public String getRectifierId() {
        return rectifierId;
    }

    public void setRectifierId(String rectifierId) {
        this.rectifierId = rectifierId;
    }

    public String getRectifierName() {
        return rectifierName;
    }

    public void setRectifierName(String rectifierName) {
        this.rectifierName = rectifierName;
    }

    public String getPanjangAcdb() {
        return panjangAcdb;
    }

    public void setPanjangAcdb(String panjangAcdb) {
        this.panjangAcdb = panjangAcdb;
    }

    public String getPanjangBattery() {
        return panjangBattery;
    }

    public void setPanjangBattery(String panjangBattery) {
        this.panjangBattery = panjangBattery;
    }

    public String getTypeAcdb() {
        return typeAcdb;
    }

    public void setTypeAcdb(String typeAcdb) {
        this.typeAcdb = typeAcdb;
    }

    public String getPanjangDcdb() {
        return panjangDcdb;
    }

    public void setPanjangDcdb(String panjangDcdb) {
        this.panjangDcdb = panjangDcdb;
    }

    public String getTypeBattery() {
        return typeBattery;
    }

    public void setTypeBattery(String typeBattery) {
        this.typeBattery = typeBattery;
    }

    public String getTypeDcdb() {
        return typeDcdb;
    }

    public void setTypeDcdb(String typeDcdb) {
        this.typeDcdb = typeDcdb;
    }
}
