package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class SasPerangkat extends BaseModel implements Serializable {
    private String sasPerangkatId;
    private String namaPerangkat;
    private String merk;
    private String tipePerangkat;
    private String type;
    private String jumlah;
    private String sn;
    private String kodeAlatDetail;
    private String kodeAlat;
    private String kapasitas;
    private String tipeport;

    private boolean edit = false;
    private boolean add = false;
    private boolean delete = false;

    public String getTipeport() {
        return tipeport;
    }

    public void setTipeport(String tipeport) {
        this.tipeport = tipeport;
    }

    public String getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(String kapasitas) {
        this.kapasitas = kapasitas;
    }

    public String getSasPerangkatId() {
        return sasPerangkatId;
    }

    public void setSasPerangkatId(String sasPerangkatId) {
        this.sasPerangkatId = sasPerangkatId;
    }

    public String getNamaPerangkat() {
        return namaPerangkat;
    }

    public void setNamaPerangkat(String namaPerangkat) {
        this.namaPerangkat = namaPerangkat;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTipePerangkat() {
        return tipePerangkat;
    }

    public void setTipePerangkat(String tipePerangkat) {
        this.tipePerangkat = tipePerangkat;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
