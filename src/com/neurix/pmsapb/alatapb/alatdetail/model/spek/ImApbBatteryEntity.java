package com.neurix.pmsapb.alatapb.alatdetail.model.spek;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class ImApbBatteryEntity implements Serializable {
    private String batteryId;
    private String batteryName;
    private String kodeAlatDetail;
    private String kodeAlat;
    private String tegangan;
    private String jumlahCell;
    private String merk;
    private String type;
    private String kapasitas;
    private String fuse;
    private String fungsi;
    private String flag;
    private String action;
    private Timestamp createdDate;
    private String createdWho;
    private Timestamp lastUpdate;
    private String lastUpdateWho;

    public String getKodeAlatDetail() {
        return kodeAlatDetail;
    }

    public void setKodeAlatDetail(String kodeAlatDetail) {
        this.kodeAlatDetail = kodeAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedWho() {
        return createdWho;
    }

    public void setCreatedWho(String createdWho) {
        this.createdWho = createdWho;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public String getBatteryId() {
        return batteryId;
    }

    public void setBatteryId(String batteryId) {
        this.batteryId = batteryId;
    }

    public String getBatteryName() {
        return batteryName;
    }

    public void setBatteryName(String batteryName) {
        this.batteryName = batteryName;
    }

    public String getTegangan() {
        return tegangan;
    }

    public void setTegangan(String tegangan) {
        this.tegangan = tegangan;
    }

    public String getJumlahCell() {
        return jumlahCell;
    }

    public void setJumlahCell(String jumlahCell) {
        this.jumlahCell = jumlahCell;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFuse() {
        return fuse;
    }

    public void setFuse(String fuse) {
        this.fuse = fuse;
    }

    public String getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(String kapasitas) {
        this.kapasitas = kapasitas;
    }

    public String getFungsi() {
        return fungsi;
    }

    public void setFungsi(String fungsi) {
        this.fungsi = fungsi;
    }
}
