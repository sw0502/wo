package com.neurix.pmsapb.alatapb.alatdetail.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class MtApbAlatDetailEntity implements Serializable {
    private String idAlatDetail;
    private String kodeAlat;
    private String kodeApp;
    private String noGi;
    private String namaGi;
    private String noGiTujuan;
    private String namaGiTujuan;
    private String namaAlatDetail;
    private String merk;
    private String tegangan;
    private String typeId;
    private String noSeries;
    private String kdStatus;
    private Date tglOprs;
    private Date tglPasang;
    private String keterangan;
    private String jenisTp;
    private String frekuensiTx;
    private String frekuensiRx;
    private String daya;
    private String transmisi;
    private String flag;
    private String action;
    private Timestamp lastUpdate;
    private String lastUpdateWho;
    private String note;
    private String urlFoto;
    private String noSap;
    private String noAktiva;
    private String status;
    private String pax;
    private String typePax;
    private String powerSupply;
    private String ruangTerpasang;
    private String typeSwitch;
    private String longt;
    private String lat;
    private String techidentno;
    private String typePlc;
    private String bandwith;
    private String namaRtu;
    private String idRtu;
    private String idBidang;
    private String namaBidang;
    private String idSubBidang;
    private String namaSubBidang;
    private String batteryName;
    private String jumlahCell;
    private String kapasitas;
    private String fuse;
    private String fungsi;
    private String teganganInput;
    private String teganganOutput;
    private String mcbInput;
    private String mcbOutput;
    private String rectifierName;
    private String phase;
    private String mode;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getRectifierName() {
        return rectifierName;
    }

    public void setRectifierName(String rectifierName) {
        this.rectifierName = rectifierName;
    }

    public String getTeganganInput() {
        return teganganInput;
    }

    public void setTeganganInput(String teganganInput) {
        this.teganganInput = teganganInput;
    }

    public String getTeganganOutput() {
        return teganganOutput;
    }

    public void setTeganganOutput(String teganganOutput) {
        this.teganganOutput = teganganOutput;
    }

    public String getMcbInput() {
        return mcbInput;
    }

    public void setMcbInput(String mcbInput) {
        this.mcbInput = mcbInput;
    }

    public String getMcbOutput() {
        return mcbOutput;
    }

    public void setMcbOutput(String mcbOutput) {
        this.mcbOutput = mcbOutput;
    }

    public String getBatteryName() {
        return batteryName;
    }

    public void setBatteryName(String batteryName) {
        this.batteryName = batteryName;
    }

    public String getJumlahCell() {
        return jumlahCell;
    }

    public void setJumlahCell(String jumlahCell) {
        this.jumlahCell = jumlahCell;
    }

    public String getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(String kapasitas) {
        this.kapasitas = kapasitas;
    }

    public String getFuse() {
        return fuse;
    }

    public void setFuse(String fuse) {
        this.fuse = fuse;
    }

    public String getFungsi() {
        return fungsi;
    }

    public void setFungsi(String fungsi) {
        this.fungsi = fungsi;
    }

    public String getIdBidang() {
        return idBidang;
    }

    public void setIdBidang(String idBidang) {
        this.idBidang = idBidang;
    }

    public String getNamaBidang() {
        return namaBidang;
    }

    public void setNamaBidang(String namaBidang) {
        this.namaBidang = namaBidang;
    }

    public String getIdSubBidang() {
        return idSubBidang;
    }

    public void setIdSubBidang(String idSubBidang) {
        this.idSubBidang = idSubBidang;
    }

    public String getNamaSubBidang() {
        return namaSubBidang;
    }

    public void setNamaSubBidang(String namaSubBidang) {
        this.namaSubBidang = namaSubBidang;
    }

    public String getTypePlc() {
        return typePlc;
    }

    public void setTypePlc(String typePlc) {
        this.typePlc = typePlc;
    }

    public String getBandwith() {
        return bandwith;
    }

    public void setBandwith(String bandwith) {
        this.bandwith = bandwith;
    }

    public String getNamaRtu() {
        return namaRtu;
    }

    public void setNamaRtu(String namaRtu) {
        this.namaRtu = namaRtu;
    }

    public String getIdRtu() {
        return idRtu;
    }

    public void setIdRtu(String idRtu) {
        this.idRtu = idRtu;
    }

    public String getTechidentno() {
        return techidentno;
    }

    public void setTechidentno(String techidentno) {
        this.techidentno = techidentno;
    }

    public String getPax() {
        return pax;
    }

    public void setPax(String pax) {
        this.pax = pax;
    }

    public String getTypePax() {
        return typePax;
    }

    public void setTypePax(String typePax) {
        this.typePax = typePax;
    }

    public String getPowerSupply() {
        return powerSupply;
    }

    public void setPowerSupply(String powerSupply) {
        this.powerSupply = powerSupply;
    }

    public String getRuangTerpasang() {
        return ruangTerpasang;
    }

    public void setRuangTerpasang(String ruangTerpasang) {
        this.ruangTerpasang = ruangTerpasang;
    }

    public String getTypeSwitch() {
        return typeSwitch;
    }

    public void setTypeSwitch(String typeSwitch) {
        this.typeSwitch = typeSwitch;
    }

    public String getLongt() {
        return longt;
    }

    public void setLongt(String longt) {
        this.longt = longt;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNoAktiva() {
        return noAktiva;
    }

    public void setNoAktiva(String noAktiva) {
        this.noAktiva = noAktiva;
    }

    public String getNoSap() {
        return noSap;
    }

    public void setNoSap(String noSap) {
        this.noSap = noSap;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getKodeApp() {
        return kodeApp;
    }

    public void setKodeApp(String kodeApp) {
        this.kodeApp = kodeApp;
    }

    public String getNoGi() {
        return noGi;
    }

    public void setNoGi(String noGi) {
        this.noGi = noGi;
    }

    public String getNamaGi() {
        return namaGi;
    }

    public void setNamaGi(String namaGi) {
        this.namaGi = namaGi;
    }

    public String getNoGiTujuan() {
        return noGiTujuan;
    }

    public void setNoGiTujuan(String noGiTujuan) {
        this.noGiTujuan = noGiTujuan;
    }

    public String getNamaGiTujuan() {
        return namaGiTujuan;
    }

    public void setNamaGiTujuan(String namaGiTujuan) {
        this.namaGiTujuan = namaGiTujuan;
    }

    public String getNamaAlatDetail() {
        return namaAlatDetail;
    }

    public void setNamaAlatDetail(String namaAlatDetail) {
        this.namaAlatDetail = namaAlatDetail;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTegangan() {
        return tegangan;
    }

    public void setTegangan(String tegangan) {
        this.tegangan = tegangan;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getNoSeries() {
        return noSeries;
    }

    public void setNoSeries(String noSeries) {
        this.noSeries = noSeries;
    }

    public String getKdStatus() {
        return kdStatus;
    }

    public void setKdStatus(String kdStatus) {
        this.kdStatus = kdStatus;
    }

    public Date getTglOprs() {
        return tglOprs;
    }

    public void setTglOprs(Date tglOprs) {
        this.tglOprs = tglOprs;
    }

    public Date getTglPasang() {
        return tglPasang;
    }

    public void setTglPasang(Date tglPasang) {
        this.tglPasang = tglPasang;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getJenisTp() {
        return jenisTp;
    }

    public void setJenisTp(String jenisTp) {
        this.jenisTp = jenisTp;
    }

    public String getFrekuensiTx() {
        return frekuensiTx;
    }

    public void setFrekuensiTx(String frekuensiTx) {
        this.frekuensiTx = frekuensiTx;
    }

    public String getFrekuensiRx() {
        return frekuensiRx;
    }

    public void setFrekuensiRx(String frekuensiRx) {
        this.frekuensiRx = frekuensiRx;
    }

    public String getDaya() {
        return daya;
    }

    public void setDaya(String daya) {
        this.daya = daya;
    }

    public String getTransmisi() {
        return transmisi;
    }

    public void setTransmisi(String transmisi) {
        this.transmisi = transmisi;
    }
}
