package com.neurix.pmsapb.alatapb.alatdetail.bo.impl;

import com.neurix.common.constant.CommonConstant;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.alatapb.alatdetail.bo.AlatDetailBo;
import com.neurix.pmsapb.alatapb.alatdetail.dao.AlatDetailDao;
import com.neurix.pmsapb.alatapb.alatdetail.dao.AlatDetailLogDao;
import com.neurix.pmsapb.alatapb.alatdetail.dao.TpChanelDao;
import com.neurix.pmsapb.alatapb.alatdetail.dao.spek.*;
import com.neurix.pmsapb.alatapb.alatdetail.model.*;
import com.neurix.pmsapb.alatapb.alatdetail.model.spek.*;
import com.neurix.pmsapb.alatapb.alatdoc.dao.AlatDocDao;
import com.neurix.pmsapb.alatapb.alatdoc.model.AlatDoc;
import com.neurix.pmsapb.alatapb.alatdoc.model.ItApbAlatDocEntity;
import com.neurix.pmsapb.master.alat.dao.AlatFeatureDao;
import com.neurix.pmsapb.master.alat.model.AlatFeature;
import com.neurix.pmsapb.master.alat.model.ImApbAlatEntity;
import com.neurix.pmsapb.master.alat.model.ImApbAlatFeatureEntity;
import com.neurix.pmsapb.master.app.bo.AppBo;
import com.neurix.pmsapb.master.app.dao.AppDao;
import com.neurix.pmsapb.master.app.model.App;
import com.neurix.pmsapb.master.app.model.ImApbAppEntity;
import com.neurix.pmsapb.master.bidang.dao.BidangDao;
import com.neurix.pmsapb.master.bidang.model.Bidang;
import com.neurix.pmsapb.master.bidang.model.ImApbBidangEntity;
import com.neurix.pmsapb.master.subbidang.dao.SubBidangDao;
import com.neurix.pmsapb.master.subbidang.model.ImSubBidangEntity;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 06/09/2017.
 */
public class AlatDetailBoImpl implements AlatDetailBo {
    protected static transient Logger logger = Logger.getLogger(AlatDetailBoImpl.class);

    private AlatDetailDao alatDetailDao;
    private AppDao appDao;
    private AlatDocDao alatDocDao;
    private AlatDetailLogDao alatDetailLogDao;
    private AlatFeatureDao alatFeatureDao;
    private TpChanelDao tpChanelDao;
    private PlcChanelDao plcChanelDao;
    private KapasitasMuxDao kapasitasMuxDao;
    private ModulRtuDao modulRtuDao;
    private BayRtuDao bayRtuDao;
    private SwitchPortDao switchPortDao;
    private BatteryDao batteryDao;
    private BidangDao bidangDao;
    private SubBidangDao subBidangDao;
    private ProteksiSettingDao proteksiSettingDao;
    private SasControlDao sasControlDao;
    private SasStationDao sasStationDao;
    private SasPerangkatDao sasPerangkatDao;
    private RectifierDao rectifierDao;

    public RectifierDao getRectifierDao() {
        return rectifierDao;
    }

    public void setRectifierDao(RectifierDao rectifierDao) {
        this.rectifierDao = rectifierDao;
    }

    public void setSasStationDao(SasStationDao sasStationDao) {
        this.sasStationDao = sasStationDao;
    }

    public void setSasControlDao(SasControlDao sasControlDao) {
        this.sasControlDao = sasControlDao;
    }

    public void setSasPerangkatDao(SasPerangkatDao sasPerangkatDao) {
        this.sasPerangkatDao = sasPerangkatDao;
    }

    public BatteryDao getBatteryDao() {
        return batteryDao;
    }

    public void setBatteryDao(BatteryDao batteryDao) {
        this.batteryDao = batteryDao;
    }

    public void setProteksiSettingDao(ProteksiSettingDao proteksiSettingDao) {
        this.proteksiSettingDao = proteksiSettingDao;
    }

    public SwitchPortDao getSwitchPortDao() {
        return switchPortDao;
    }

    public void setSwitchPortDao(SwitchPortDao switchPortDao) {
        this.switchPortDao = switchPortDao;
    }

    public void setModulRtuDao(ModulRtuDao modulRtuDao) {
        this.modulRtuDao = modulRtuDao;
    }

    public void setBayRtuDao(BayRtuDao bayRtuDao) {
        this.bayRtuDao = bayRtuDao;
    }
    public BidangDao getBidangDao() {
        return bidangDao;
    }

    public void setBidangDao(BidangDao bidangDao) {
        this.bidangDao = bidangDao;
    }

    public SubBidangDao getSubBidangDao() {
        return subBidangDao;
    }

    public void setSubBidangDao(SubBidangDao subBidangDao) {
        this.subBidangDao = subBidangDao;
    }

    public KapasitasMuxDao getKapasitasMuxDao() {
        return kapasitasMuxDao;
    }

    public void setKapasitasMuxDao(KapasitasMuxDao kapasitasMuxDao) {
        this.kapasitasMuxDao = kapasitasMuxDao;
    }

    public PlcChanelDao getPlcChanelDao() {
        return plcChanelDao;
    }

    public void setPlcChanelDao(PlcChanelDao plcChanelDao) {
        this.plcChanelDao = plcChanelDao;
    }

    public TpChanelDao getTpChanelDao() {
        return tpChanelDao;
    }

    public void setTpChanelDao(TpChanelDao tpChanelDao) {
        this.tpChanelDao = tpChanelDao;
    }

    public AlatFeatureDao getAlatFeatureDao() {
        return alatFeatureDao;
    }

    public void setAlatFeatureDao(AlatFeatureDao alatFeatureDao) {
        this.alatFeatureDao = alatFeatureDao;
    }

    public AlatDetailLogDao getAlatDetailLogDao() {
        return alatDetailLogDao;
    }

    public void setAlatDetailLogDao(AlatDetailLogDao alatDetailLogDao) {
        this.alatDetailLogDao = alatDetailLogDao;
    }

    public AlatDocDao getAlatDocDao() {
        return alatDocDao;
    }

    public void setAlatDocDao(AlatDocDao alatDocDao) {
        this.alatDocDao = alatDocDao;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        AlatDetailBoImpl.logger = logger;
    }

    public AlatDetailDao getAlatDetailDao() {
        return alatDetailDao;
    }

    public void setAlatDetailDao(AlatDetailDao alatDetailDao) {
        this.alatDetailDao = alatDetailDao;
    }

    public AppDao getAppDao() {
        return appDao;
    }

    public void setAppDao(AppDao appDao) {
        this.appDao = appDao;
    }

    @Override
    public void saveDelete(AlatDetail bean) throws GeneralBOException {
        logger.info("[AlatDetailBoImpl.saveDelete] start process >>>");
        if (bean != null) {
            MtApbAlatDetailEntity mtApbAlatDetailEntityNew = new MtApbAlatDetailEntity();
            mtApbAlatDetailEntityNew.setIdAlatDetail(bean.getIdAlatDetail());
            mtApbAlatDetailEntityNew.setKodeAlat(bean.getKodeAlat());
            mtApbAlatDetailEntityNew.setKodeApp(bean.getKodeApp());
            mtApbAlatDetailEntityNew.setNoGi(bean.getNoGi());
            mtApbAlatDetailEntityNew.setNamaGi(bean.getNamaGi());
            mtApbAlatDetailEntityNew.setNoGiTujuan(bean.getNoGiTujuan());
            mtApbAlatDetailEntityNew.setNamaGiTujuan(bean.getNamaGiTujuan());
            mtApbAlatDetailEntityNew.setNamaAlatDetail(bean.getNamaAlatDetail());
            mtApbAlatDetailEntityNew.setMerk(bean.getMerk());
            mtApbAlatDetailEntityNew.setTegangan(bean.getTegangan());
            mtApbAlatDetailEntityNew.setTypeId(bean.getTypeId());
            mtApbAlatDetailEntityNew.setNoSeries(bean.getNoSeries());
            mtApbAlatDetailEntityNew.setKdStatus(bean.getKdStatus());
            mtApbAlatDetailEntityNew.setTglOprs(CommonUtil.convertToDate(bean.getStTglOprs()));
            mtApbAlatDetailEntityNew.setTglPasang(CommonUtil.convertToDate(bean.getStTglPasang()));
            mtApbAlatDetailEntityNew.setKeterangan(bean.getKeterangan());
            mtApbAlatDetailEntityNew.setJenisTp(bean.getJenisTp());
            mtApbAlatDetailEntityNew.setFrekuensiTx(bean.getFrekuensiTx());
            mtApbAlatDetailEntityNew.setFrekuensiRx(bean.getFrekuensiRx());
            mtApbAlatDetailEntityNew.setDaya(bean.getDaya());
            mtApbAlatDetailEntityNew.setTransmisi(bean.getTransmisi());
            mtApbAlatDetailEntityNew.setLastUpdate(bean.getLastUpdate());
            mtApbAlatDetailEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
            mtApbAlatDetailEntityNew.setFlag("N");
            mtApbAlatDetailEntityNew.setAction("U");
            mtApbAlatDetailEntityNew.setUrlFoto(bean.getUrlFoto());
            mtApbAlatDetailEntityNew.setNote(bean.getNote());
            mtApbAlatDetailEntityNew.setNoSap(bean.getNoSap());
            mtApbAlatDetailEntityNew.setNoAktiva(bean.getNoAktiva());
            mtApbAlatDetailEntityNew.setStatus(bean.getStatus());
            mtApbAlatDetailEntityNew.setPax(bean.getPax());
            mtApbAlatDetailEntityNew.setTypePax(bean.getTypePax());
            mtApbAlatDetailEntityNew.setPowerSupply(bean.getPowerSupply());
            mtApbAlatDetailEntityNew.setRuangTerpasang(bean.getRuangTerpasang());
            mtApbAlatDetailEntityNew.setTypeSwitch(bean.getTypeSwitch());
            mtApbAlatDetailEntityNew.setLat(bean.getLat());
            mtApbAlatDetailEntityNew.setLongt(bean.getLongt());
            mtApbAlatDetailEntityNew.setTechidentno(bean.getTechidentno());
            mtApbAlatDetailEntityNew.setTypePlc(bean.getTypePlc());
            mtApbAlatDetailEntityNew.setBandwith(bean.getBandwith());
            mtApbAlatDetailEntityNew.setNamaRtu(bean.getNamaRtu());
            mtApbAlatDetailEntityNew.setIdRtu(bean.getIdRtu());
            mtApbAlatDetailEntityNew.setBatteryName(bean.getBatteryName());
            mtApbAlatDetailEntityNew.setJumlahCell(bean.getJumlahCell());
            mtApbAlatDetailEntityNew.setKapasitas(bean.getKapasitas());
            mtApbAlatDetailEntityNew.setFuse(bean.getFuse());
            mtApbAlatDetailEntityNew.setFungsi(bean.getFungsi());
            mtApbAlatDetailEntityNew.setRectifierName(bean.getRectifierName());
            mtApbAlatDetailEntityNew.setTeganganInput(bean.getTeganganInput());
            mtApbAlatDetailEntityNew.setTeganganOutput(bean.getTeganganOutput());
            mtApbAlatDetailEntityNew.setMcbInput(bean.getMcbInput());
            mtApbAlatDetailEntityNew.setMcbOutput(bean.getMcbOutput());
            mtApbAlatDetailEntityNew.setPhase(bean.getPhase());
            mtApbAlatDetailEntityNew.setMode(bean.getMode());


            try {
                alatDetailDao.updateAndSave(mtApbAlatDetailEntityNew);
            } catch (GeneralBOException e) {
                logger.error("[AlatDetailBoImpl.saveDelete] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when delete data Alat Detail, please info to your admin..." + e.getMessage());
            }
        }
    }

    @Override
    public void saveEdit(AlatDetail bean) throws GeneralBOException {
        logger.info("[AlatDetailBoImpl.saveEdit] start process >>>");

        if (bean != null) {
            MtApbAlatDetailEntity mtApbAlatDetailEntityNew = new MtApbAlatDetailEntity();
            mtApbAlatDetailEntityNew.setIdAlatDetail(bean.getIdAlatDetail());
            mtApbAlatDetailEntityNew.setKodeAlat(bean.getKodeAlat());
            mtApbAlatDetailEntityNew.setKodeApp(bean.getKodeApp());
            mtApbAlatDetailEntityNew.setNoGi(bean.getNoGi());
            mtApbAlatDetailEntityNew.setNamaGi(bean.getNamaGi());
            mtApbAlatDetailEntityNew.setNoGiTujuan(bean.getNoGiTujuan());
            mtApbAlatDetailEntityNew.setNamaGiTujuan(bean.getNamaGiTujuan());
            mtApbAlatDetailEntityNew.setNamaAlatDetail(bean.getNamaAlatDetail());
            mtApbAlatDetailEntityNew.setMerk(bean.getMerk());
            mtApbAlatDetailEntityNew.setTegangan(bean.getTegangan());
            mtApbAlatDetailEntityNew.setTypeId(bean.getTypeId());
            mtApbAlatDetailEntityNew.setNoSeries(bean.getNoSeries());
            mtApbAlatDetailEntityNew.setKdStatus(bean.getKdStatus());
            mtApbAlatDetailEntityNew.setTglOprs(CommonUtil.convertToDate(bean.getStTglOprs()));
            mtApbAlatDetailEntityNew.setTglPasang(CommonUtil.convertToDate(bean.getStTglPasang()));
            mtApbAlatDetailEntityNew.setKeterangan(bean.getKeterangan());
            mtApbAlatDetailEntityNew.setJenisTp(bean.getJenisTp());
            mtApbAlatDetailEntityNew.setFrekuensiTx(bean.getFrekuensiTx());
            mtApbAlatDetailEntityNew.setFrekuensiRx(bean.getFrekuensiRx());
            mtApbAlatDetailEntityNew.setDaya(bean.getDaya());
            mtApbAlatDetailEntityNew.setTransmisi(bean.getTransmisi());
            mtApbAlatDetailEntityNew.setLastUpdate(bean.getLastUpdate());
            mtApbAlatDetailEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
            mtApbAlatDetailEntityNew.setNoSap(bean.getNoSap());
            mtApbAlatDetailEntityNew.setNoAktiva(bean.getNoAktiva());
            mtApbAlatDetailEntityNew.setPax(bean.getPax());
            mtApbAlatDetailEntityNew.setTypePax(bean.getTypePax());
            mtApbAlatDetailEntityNew.setPowerSupply(bean.getPowerSupply());
            mtApbAlatDetailEntityNew.setRuangTerpasang(bean.getRuangTerpasang());
            mtApbAlatDetailEntityNew.setTypeSwitch(bean.getTypeSwitch());
            mtApbAlatDetailEntityNew.setLat(bean.getLat());
            mtApbAlatDetailEntityNew.setLongt(bean.getLongt());
            mtApbAlatDetailEntityNew.setTechidentno(bean.getTechidentno());
            mtApbAlatDetailEntityNew.setTypePlc(bean.getTypePlc());
            mtApbAlatDetailEntityNew.setBandwith(bean.getBandwith());
            mtApbAlatDetailEntityNew.setNamaRtu(bean.getNamaRtu());
            mtApbAlatDetailEntityNew.setIdRtu(bean.getIdRtu());
            mtApbAlatDetailEntityNew.setIdBidang(bean.getIdBidang());
            mtApbAlatDetailEntityNew.setIdSubBidang(bean.getIdSubBidang());
            mtApbAlatDetailEntityNew.setBatteryName(bean.getBatteryName());
            mtApbAlatDetailEntityNew.setJumlahCell(bean.getJumlahCell());
            mtApbAlatDetailEntityNew.setKapasitas(bean.getKapasitas());
            mtApbAlatDetailEntityNew.setFuse(bean.getFuse());
            mtApbAlatDetailEntityNew.setFungsi(bean.getFungsi());
            mtApbAlatDetailEntityNew.setRectifierName(bean.getRectifierName());
            mtApbAlatDetailEntityNew.setTeganganInput(bean.getTeganganInput());
            mtApbAlatDetailEntityNew.setTeganganOutput(bean.getTeganganOutput());
            mtApbAlatDetailEntityNew.setMcbInput(bean.getMcbInput());
            mtApbAlatDetailEntityNew.setMcbOutput(bean.getMcbOutput());
            mtApbAlatDetailEntityNew.setPhase(bean.getPhase());
            mtApbAlatDetailEntityNew.setMode(bean.getMode());


            if (bean.getUrlFoto() != null) {
                mtApbAlatDetailEntityNew.setUrlFoto(bean.getUrlFoto());
            }
            mtApbAlatDetailEntityNew.setNote(bean.getNote());
            mtApbAlatDetailEntityNew.setFlag("Y");
            mtApbAlatDetailEntityNew.setAction("U");
            mtApbAlatDetailEntityNew.setStatus(bean.getStatus());
            try {
                alatDetailDao.updateAndSave(mtApbAlatDetailEntityNew);
            } catch (GeneralBOException e) {
                logger.error("[AlatDetailBoImpl.saveEdit] Error, " + e.getMessage());
                throw new GeneralBOException("Found p" +
                        "roblem when update data Alat Detail, please info to your admin..." + e.getMessage());
            }
            if (mtApbAlatDetailEntityNew != null) {
                MtApbAlatDetailLogEntity logAlatEntity = new MtApbAlatDetailLogEntity();
                if (logAlatEntity.getIdLogAlatDetail() == null) {
                    String id;
                    id = alatDetailLogDao.getNextAlatDetailLogId();
                    logAlatEntity.setIdLogAlatDetail("LOG" + id);
                }
                logAlatEntity.setIdAlatDetail(bean.getIdAlatDetail());
                logAlatEntity.setKodeAlat(bean.getKodeAlat());
                logAlatEntity.setKodeApp(bean.getKodeApp());
                logAlatEntity.setNoGi(bean.getNoGi());
                logAlatEntity.setNamaGi(bean.getNamaGi());
                logAlatEntity.setNoGiTujuan(bean.getNoGiTujuan());
                logAlatEntity.setNamaGiTujuan(bean.getNamaGiTujuan());
                logAlatEntity.setNamaAlatDetail(bean.getNamaAlatDetail());
                logAlatEntity.setMerk(bean.getMerk());
                logAlatEntity.setTegangan(bean.getTegangan());
                logAlatEntity.setTypeId(bean.getTypeId());
                logAlatEntity.setNoSeries(bean.getNoSeries());
                logAlatEntity.setKdStatus(bean.getKdStatus());
                logAlatEntity.setTglOprs(CommonUtil.convertToDate(bean.getStTglOprs()));
                logAlatEntity.setTglPasang(CommonUtil.convertToDate(bean.getStTglPasang()));
                logAlatEntity.setKeterangan(bean.getKeterangan());
                logAlatEntity.setJenisTp(bean.getJenisTp());
                logAlatEntity.setFrekuensiTx(bean.getFrekuensiTx());
                logAlatEntity.setFrekuensiRx(bean.getFrekuensiRx());
                logAlatEntity.setDaya(bean.getDaya());
                logAlatEntity.setTransmisi(bean.getTransmisi());
                logAlatEntity.setLastUpdate(bean.getLastUpdate());
                logAlatEntity.setLastUpdateWho(bean.getLastUpdateWho());
                if (bean.getUrlFoto() != null) {
                    logAlatEntity.setUrlFoto(bean.getUrlFoto());
                }
                logAlatEntity.setNote(bean.getNote());
                logAlatEntity.setStatus(bean.getStatus());
                logAlatEntity.setFlag("Y");
                logAlatEntity.setAction("U");
                try {
                    alatDetailLogDao.addAndSave(logAlatEntity);
                } catch (GeneralBOException e) {
                    logger.error("[AlatDetailBoImpl.saveEdit] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when update data Alat Detail, please info to your admin..." + e.getMessage());
                }
            }

            List<AlatDoc> listOfAlatDoc = new ArrayList<AlatDoc>();
            listOfAlatDoc = alatDocDao.getListData(bean.getIdAlatDetail());

//            insert data uploaded documment into table

            if (bean.getUploadDoc() != null) {
                ItApbAlatDocEntity itApbAlatDocEntityNew = new ItApbAlatDocEntity();
                if (listOfAlatDoc != null) {
                    for (AlatDoc listDoc : listOfAlatDoc) {
                        if ("5".equalsIgnoreCase(listDoc.getPosition())) {
                            itApbAlatDocEntityNew.setIdAlatDoc(listDoc.getIdAlatDoc());
                            itApbAlatDocEntityNew.setKodeAlatDetail(listDoc.getKodeAlatDetail());
                            itApbAlatDocEntityNew.setCreatedDate(listDoc.getCreatedDate());
                            itApbAlatDocEntityNew.setCreatedDateWho(listDoc.getCreatedDateWho());
                            itApbAlatDocEntityNew.setType(listDoc.getType());
                        }
                    }
                }
                itApbAlatDocEntityNew.setUrlPhoto(bean.getUploadDoc());
                itApbAlatDocEntityNew.setPosition("5");
                itApbAlatDocEntityNew.setLastUpdate(bean.getLastUpdate());
                itApbAlatDocEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
                itApbAlatDocEntityNew.setFlag("Y");
                if (itApbAlatDocEntityNew.getIdAlatDoc() != null) {
                    itApbAlatDocEntityNew.setAction("U");
                    try {
                        alatDocDao.updateAndSave(itApbAlatDocEntityNew);
                    } catch (GeneralBOException e) {
                        logger.error("[AlatDetailBoImpl.saveEdit] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when update data Alat Detail, please info to your admin..." + e.getMessage());
                    }
                } else {
                    itApbAlatDocEntityNew.setAction("C");
                    itApbAlatDocEntityNew.setKodeAlatDetail(bean.getIdAlatDetail());
                    itApbAlatDocEntityNew.setCreatedDate(bean.getLastUpdate());
                    itApbAlatDocEntityNew.setCreatedDateWho(bean.getLastUpdateWho());
                    itApbAlatDocEntityNew.setType("DOC");
                    String id;
                    id = alatDocDao.getNextAlatDocId();
                    if (id != null) {
                        itApbAlatDocEntityNew.setIdAlatDoc("ALD" + id);
                    }
                    alatDocDao.addAndSave(itApbAlatDocEntityNew);
                }
            }

//            insert data image into table

            if (bean.getUploadFront() != null) {
                ItApbAlatDocEntity itApbAlatDocEntityNew = new ItApbAlatDocEntity();
                if (listOfAlatDoc != null) {
                    for (AlatDoc listDoc : listOfAlatDoc) {
                        if ("1".equalsIgnoreCase(listDoc.getPosition())) {
                            itApbAlatDocEntityNew.setIdAlatDoc(listDoc.getIdAlatDoc());
                            itApbAlatDocEntityNew.setKodeAlatDetail(listDoc.getKodeAlatDetail());
                            itApbAlatDocEntityNew.setCreatedDate(listDoc.getCreatedDate());
                            itApbAlatDocEntityNew.setCreatedDateWho(listDoc.getCreatedDateWho());
                            itApbAlatDocEntityNew.setType(listDoc.getType());
                        }
                    }
                }
                itApbAlatDocEntityNew.setUrlPhoto(bean.getUploadFront());
                itApbAlatDocEntityNew.setPosition("1");
                itApbAlatDocEntityNew.setLastUpdate(bean.getLastUpdate());
                itApbAlatDocEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
                itApbAlatDocEntityNew.setFlag("Y");
                if (itApbAlatDocEntityNew.getIdAlatDoc() != null) {
                    itApbAlatDocEntityNew.setAction("U");
                    try {
                        alatDocDao.updateAndSave(itApbAlatDocEntityNew);
                    } catch (GeneralBOException e) {
                        logger.error("[AlatDetailBoImpl.saveEdit] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when update data Alat Detail, please info to your admin..." + e.getMessage());
                    }
                } else {
                    itApbAlatDocEntityNew.setAction("C");
                    itApbAlatDocEntityNew.setKodeAlatDetail(bean.getIdAlatDetail());
                    itApbAlatDocEntityNew.setCreatedDate(bean.getLastUpdate());
                    itApbAlatDocEntityNew.setCreatedDateWho(bean.getLastUpdateWho());
                    itApbAlatDocEntityNew.setType("IMG");
                    String id;
                    id = alatDocDao.getNextAlatDocId();
                    if (id != null) {
                        itApbAlatDocEntityNew.setIdAlatDoc("ALD" + id);
                    }
                    alatDocDao.addAndSave(itApbAlatDocEntityNew);
                }
            }

//            if (bean.getUploadBack() != null) {
//                ItApbAlatDocEntity itApbAlatDocEntityNew = new ItApbAlatDocEntity();
//                if (listOfAlatDoc != null) {
//                    for (AlatDoc listDoc : listOfAlatDoc) {
//                        if ("2".equalsIgnoreCase(listDoc.getPosition())) {
//                            itApbAlatDocEntityNew.setIdAlatDoc(listDoc.getIdAlatDoc());
//                            itApbAlatDocEntityNew.setKodeAlatDetail(listDoc.getKodeAlatDetail());
//                            itApbAlatDocEntityNew.setCreatedDate(listDoc.getCreatedDate());
//                            itApbAlatDocEntityNew.setCreatedDateWho(listDoc.getCreatedDateWho());
//                        }
//                    }
//                }
//                itApbAlatDocEntityNew.setUrlPhoto(bean.getUploadBack());
//                itApbAlatDocEntityNew.setPosition("2");
//                itApbAlatDocEntityNew.setLastUpdate(bean.getLastUpdate());
//                itApbAlatDocEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
//                itApbAlatDocEntityNew.setFlag("Y");
//                if (itApbAlatDocEntityNew.getIdAlatDoc() != null) {
//                    itApbAlatDocEntityNew.setAction("U");
//                    try {
//                        alatDocDao.updateAndSave(itApbAlatDocEntityNew);
//                    } catch (GeneralBOException e) {
//                        logger.error("[AlatDetailBoImpl.saveEdit] Error, " + e.getMessage());
//                        throw new GeneralBOException("Found problem when update data Alat Detail, please info to your admin..." + e.getMessage());
//                    }
//                } else {
//                    itApbAlatDocEntityNew.setAction("C");
//                    itApbAlatDocEntityNew.setKodeAlatDetail(bean.getIdAlatDetail());
//                    itApbAlatDocEntityNew.setCreatedDate(bean.getLastUpdate());
//                    itApbAlatDocEntityNew.setCreatedDateWho(bean.getLastUpdateWho());
//                    String id;
//                    id = alatDocDao.getNextAlatDocId();
//                    if (id != null) {
//                        itApbAlatDocEntityNew.setIdAlatDoc("ALD" + id);
//                    }
//                    alatDocDao.addAndSave(itApbAlatDocEntityNew);
//                }
//            }
//            if (bean.getUploadLeft() != null) {
//                ItApbAlatDocEntity itApbAlatDocEntityNew = new ItApbAlatDocEntity();
//                if (listOfAlatDoc != null) {
//                    for (AlatDoc listDoc : listOfAlatDoc) {
//                        if ("3".equalsIgnoreCase(listDoc.getPosition())) {
//                            itApbAlatDocEntityNew.setIdAlatDoc(listDoc.getIdAlatDoc());
//                            itApbAlatDocEntityNew.setKodeAlatDetail(listDoc.getKodeAlatDetail());
//                            itApbAlatDocEntityNew.setCreatedDate(listDoc.getCreatedDate());
//                            itApbAlatDocEntityNew.setCreatedDateWho(listDoc.getCreatedDateWho());
//                        }
//                    }
//                }
//                itApbAlatDocEntityNew.setUrlPhoto(bean.getUploadLeft());
//                itApbAlatDocEntityNew.setPosition("3");
//                itApbAlatDocEntityNew.setLastUpdate(bean.getLastUpdate());
//                itApbAlatDocEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
//                itApbAlatDocEntityNew.setFlag("Y");
//                if (itApbAlatDocEntityNew.getIdAlatDoc() != null) {
//                    itApbAlatDocEntityNew.setAction("U");
//                    try {
//                        alatDocDao.updateAndSave(itApbAlatDocEntityNew);
//                    } catch (GeneralBOException e) {
//                        logger.error("[AlatDetailBoImpl.saveEdit] Error, " + e.getMessage());
//                        throw new GeneralBOException("Found problem when update data Alat Detail, please info to your admin..." + e.getMessage());
//                    }
//                } else {
//                    itApbAlatDocEntityNew.setAction("C");
//                    itApbAlatDocEntityNew.setKodeAlatDetail(bean.getIdAlatDetail());
//                    itApbAlatDocEntityNew.setCreatedDate(bean.getLastUpdate());
//                    itApbAlatDocEntityNew.setCreatedDateWho(bean.getLastUpdateWho());
//                    String id;
//                    id = alatDocDao.getNextAlatDocId();
//                    if (id != null) {
//                        itApbAlatDocEntityNew.setIdAlatDoc("ALD" + id);
//                    }
//                    alatDocDao.addAndSave(itApbAlatDocEntityNew);
//                }
//            }
//            if (bean.getUploadRight() != null) {
//                ItApbAlatDocEntity itApbAlatDocEntityNew = new ItApbAlatDocEntity();
//                if (listOfAlatDoc != null) {
//                    for (AlatDoc listDoc : listOfAlatDoc) {
//                        if ("4".equalsIgnoreCase(listDoc.getPosition())) {
//                            itApbAlatDocEntityNew.setIdAlatDoc(listDoc.getIdAlatDoc());
//                            itApbAlatDocEntityNew.setKodeAlatDetail(listDoc.getKodeAlatDetail());
//                            itApbAlatDocEntityNew.setCreatedDate(listDoc.getCreatedDate());
//                            itApbAlatDocEntityNew.setCreatedDateWho(listDoc.getCreatedDateWho());
//                        }
//                    }
//                }
//                itApbAlatDocEntityNew.setUrlPhoto(bean.getUploadRight());
//                itApbAlatDocEntityNew.setPosition("4");
//                itApbAlatDocEntityNew.setLastUpdate(bean.getLastUpdate());
//                itApbAlatDocEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
//                itApbAlatDocEntityNew.setFlag("Y");
//                if (itApbAlatDocEntityNew.getIdAlatDoc() != null) {
//                    itApbAlatDocEntityNew.setAction("U");
//                    try {
//                        alatDocDao.updateAndSave(itApbAlatDocEntityNew);
//                    } catch (GeneralBOException e) {
//                        logger.error("[AlatDetailBoImpl.saveEdit] Error, " + e.getMessage());
//                        throw new GeneralBOException("Found problem when update data Alat Detail, please info to your admin..." + e.getMessage());
//                    }
//                } else {
//                    itApbAlatDocEntityNew.setAction("C");
//                    itApbAlatDocEntityNew.setKodeAlatDetail(bean.getIdAlatDetail());
//                    itApbAlatDocEntityNew.setCreatedDate(bean.getLastUpdate());
//                    itApbAlatDocEntityNew.setCreatedDateWho(bean.getLastUpdateWho());
//                    String id;
//                    id = alatDocDao.getNextAlatDocId();
//                    if (id != null) {
//                        itApbAlatDocEntityNew.setIdAlatDoc("ALD" + id);
//                    }
//                    alatDocDao.addAndSave(itApbAlatDocEntityNew);
//                }
//            }

        }
        logger.info("[AlatDetailBoImpl.saveEdit] end process >>>");
    }

    @Override
    public AlatDetail saveAdd(AlatDetail bean) throws GeneralBOException {
        logger.info("[AlatDetailBoImpl.saveAdd] start process >>>");
        if (bean != null) {
            MtApbAlatDetailEntity mtApbAlatDetailEntityNew = new MtApbAlatDetailEntity();
            mtApbAlatDetailEntityNew.setIdAlatDetail(bean.getIdAlatDetail());
            mtApbAlatDetailEntityNew.setKodeAlat(bean.getKodeAlat());
            mtApbAlatDetailEntityNew.setKodeApp(bean.getKodeApp());
            mtApbAlatDetailEntityNew.setNoGi(bean.getNoGi());
            mtApbAlatDetailEntityNew.setNamaGi(bean.getNamaGi());
            mtApbAlatDetailEntityNew.setNoGiTujuan(bean.getNoGiTujuan());
            mtApbAlatDetailEntityNew.setNamaGiTujuan(bean.getNamaGiTujuan());
            mtApbAlatDetailEntityNew.setNamaAlatDetail(bean.getNamaAlatDetail());
            mtApbAlatDetailEntityNew.setMerk(bean.getMerk());
            mtApbAlatDetailEntityNew.setTegangan(bean.getTegangan());
            mtApbAlatDetailEntityNew.setTypeId(bean.getTypeId());
            mtApbAlatDetailEntityNew.setNoSeries(bean.getNoSeries());
            mtApbAlatDetailEntityNew.setKdStatus(bean.getKdStatus());
            mtApbAlatDetailEntityNew.setTglOprs(CommonUtil.convertToDate(bean.getStTglOprs()));
            mtApbAlatDetailEntityNew.setTglPasang(CommonUtil.convertToDate(bean.getStTglPasang()));
            mtApbAlatDetailEntityNew.setKeterangan(bean.getKeterangan());
            mtApbAlatDetailEntityNew.setJenisTp(bean.getJenisTp());
            mtApbAlatDetailEntityNew.setFrekuensiTx(bean.getFrekuensiTx());
            mtApbAlatDetailEntityNew.setFrekuensiRx(bean.getFrekuensiRx());
            mtApbAlatDetailEntityNew.setDaya(bean.getDaya());
            mtApbAlatDetailEntityNew.setTransmisi(bean.getTransmisi());
            mtApbAlatDetailEntityNew.setLastUpdate(bean.getLastUpdate());
            mtApbAlatDetailEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
            mtApbAlatDetailEntityNew.setNoSap(bean.getNoSap());
            mtApbAlatDetailEntityNew.setNoAktiva(bean.getNoAktiva());
            mtApbAlatDetailEntityNew.setPax(bean.getPax());
            mtApbAlatDetailEntityNew.setTypePax(bean.getTypePax());
            mtApbAlatDetailEntityNew.setPowerSupply(bean.getPowerSupply());
            mtApbAlatDetailEntityNew.setRuangTerpasang(bean.getRuangTerpasang());
            mtApbAlatDetailEntityNew.setTypeSwitch(bean.getTypeSwitch());
            mtApbAlatDetailEntityNew.setLat(bean.getLat());
            mtApbAlatDetailEntityNew.setLongt(bean.getLongt());
            mtApbAlatDetailEntityNew.setTechidentno(bean.getTechidentno());
            mtApbAlatDetailEntityNew.setTypePlc(bean.getTypePlc());
            mtApbAlatDetailEntityNew.setBandwith(bean.getBandwith());
            mtApbAlatDetailEntityNew.setNamaRtu(bean.getNamaRtu());
            mtApbAlatDetailEntityNew.setIdRtu(bean.getIdRtu());
            mtApbAlatDetailEntityNew.setIdBidang(bean.getIdBidang());
            mtApbAlatDetailEntityNew.setIdSubBidang(bean.getIdSubBidang());
            mtApbAlatDetailEntityNew.setBatteryName(bean.getBatteryName());
            mtApbAlatDetailEntityNew.setJumlahCell(bean.getJumlahCell());
            mtApbAlatDetailEntityNew.setKapasitas(bean.getKapasitas());
            mtApbAlatDetailEntityNew.setFuse(bean.getFuse());
            mtApbAlatDetailEntityNew.setFungsi(bean.getFungsi());
            mtApbAlatDetailEntityNew.setRectifierName(bean.getRectifierName());
            mtApbAlatDetailEntityNew.setTeganganInput(bean.getTeganganInput());
            mtApbAlatDetailEntityNew.setTeganganOutput(bean.getTeganganOutput());
            mtApbAlatDetailEntityNew.setMcbInput(bean.getMcbInput());
            mtApbAlatDetailEntityNew.setMcbOutput(bean.getMcbOutput());
            mtApbAlatDetailEntityNew.setPhase(bean.getPhase());
            mtApbAlatDetailEntityNew.setMode(bean.getMode());

            if (bean.getUrlFoto() != null) {
                mtApbAlatDetailEntityNew.setUrlFoto(bean.getUrlFoto());
            }
            mtApbAlatDetailEntityNew.setNote(bean.getNote());
            mtApbAlatDetailEntityNew.setFlag("Y");
            mtApbAlatDetailEntityNew.setAction("C");
            mtApbAlatDetailEntityNew.setStatus(bean.getStatus());
            try {
                alatDetailDao.addAndSave(mtApbAlatDetailEntityNew);
            } catch (GeneralBOException e) {
                logger.error("[AlatDetailBoImpl.saveEdit] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when update data Alat Detail, please info to your admin..." + e.getMessage());
            }

//            insert img data into table

            if (bean.getUploadFront() != null) {
                ItApbAlatDocEntity itApbAlatDocEntityNew = new ItApbAlatDocEntity();
                String id;
                id = alatDocDao.getNextAlatDocId();
                if (id != null) {
                    itApbAlatDocEntityNew.setIdAlatDoc("ALD" + id);
                }
                itApbAlatDocEntityNew.setKodeAlatDetail(bean.getIdAlatDetail());
                itApbAlatDocEntityNew.setUrlPhoto(bean.getUploadFront());
                itApbAlatDocEntityNew.setPosition("1");
                itApbAlatDocEntityNew.setCreatedDate(bean.getLastUpdate());
                itApbAlatDocEntityNew.setCreatedDateWho(bean.getLastUpdateWho());
                itApbAlatDocEntityNew.setLastUpdate(bean.getLastUpdate());
                itApbAlatDocEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
                itApbAlatDocEntityNew.setFlag("Y");
                itApbAlatDocEntityNew.setAction("C");
                itApbAlatDocEntityNew.setType("IMG");
                alatDocDao.addAndSave(itApbAlatDocEntityNew);
            }

//            insert into documment data into table

            if (bean.getUploadDoc() != null) {
                ItApbAlatDocEntity itApbAlatDocEntityNew = new ItApbAlatDocEntity();
                String id;
                id = alatDocDao.getNextAlatDocId();
                if (id != null) {
                    itApbAlatDocEntityNew.setIdAlatDoc("ALD" + id);
                }
                itApbAlatDocEntityNew.setKodeAlatDetail(bean.getIdAlatDetail());
                itApbAlatDocEntityNew.setUrlPhoto(bean.getUploadDoc());
                itApbAlatDocEntityNew.setPosition("5");
                itApbAlatDocEntityNew.setCreatedDate(bean.getLastUpdate());
                itApbAlatDocEntityNew.setCreatedDateWho(bean.getLastUpdateWho());
                itApbAlatDocEntityNew.setLastUpdate(bean.getLastUpdate());
                itApbAlatDocEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
                itApbAlatDocEntityNew.setFlag("Y");
                itApbAlatDocEntityNew.setAction("C");
                itApbAlatDocEntityNew.setType("DOC");
                alatDocDao.addAndSave(itApbAlatDocEntityNew);
            }

//            if (bean.getUploadBack() != null) {
//                ItApbAlatDocEntity itApbAlatDocEntityNew = new ItApbAlatDocEntity();
//                String id;
//                id = alatDocDao.getNextAlatDocId();
//                if (id != null) {
//                    itApbAlatDocEntityNew.setIdAlatDoc("ALD" + id);
//                }
//                itApbAlatDocEntityNew.setKodeAlatDetail(bean.getIdAlatDetail());
//                itApbAlatDocEntityNew.setUrlPhoto(bean.getUploadBack());
//                itApbAlatDocEntityNew.setPosition("2");
//                itApbAlatDocEntityNew.setCreatedDate(bean.getLastUpdate());
//                itApbAlatDocEntityNew.setCreatedDateWho(bean.getLastUpdateWho());
//                itApbAlatDocEntityNew.setLastUpdate(bean.getLastUpdate());
//                itApbAlatDocEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
//                itApbAlatDocEntityNew.setFlag("Y");
//                itApbAlatDocEntityNew.setAction("C");
//                alatDocDao.addAndSave(itApbAlatDocEntityNew);
//            }
//            if (bean.getUploadLeft() != null) {
//                ItApbAlatDocEntity itApbAlatDocEntityNew = new ItApbAlatDocEntity();
//                String id;
//                id = alatDocDao.getNextAlatDocId();
//                if (id != null) {
//                    itApbAlatDocEntityNew.setIdAlatDoc("ALD" + id);
//                }
//                itApbAlatDocEntityNew.setKodeAlatDetail(bean.getIdAlatDetail());
//                itApbAlatDocEntityNew.setUrlPhoto(bean.getUploadLeft());
//                itApbAlatDocEntityNew.setPosition("3");
//                itApbAlatDocEntityNew.setCreatedDate(bean.getLastUpdate());
//                itApbAlatDocEntityNew.setCreatedDateWho(bean.getLastUpdateWho());
//                itApbAlatDocEntityNew.setLastUpdate(bean.getLastUpdate());
//                itApbAlatDocEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
//                itApbAlatDocEntityNew.setFlag("Y");
//                itApbAlatDocEntityNew.setAction("C");
//                alatDocDao.addAndSave(itApbAlatDocEntityNew);
//            }
//            if (bean.getUploadRight() != null) {
//                ItApbAlatDocEntity itApbAlatDocEntityNew = new ItApbAlatDocEntity();
//                String id;
//                id = alatDocDao.getNextAlatDocId();
//                if (id != null) {
//                    itApbAlatDocEntityNew.setIdAlatDoc("ALD" + id);
//                }
//                itApbAlatDocEntityNew.setKodeAlatDetail(bean.getIdAlatDetail());
//                itApbAlatDocEntityNew.setUrlPhoto(bean.getUploadRight());
//                itApbAlatDocEntityNew.setPosition("4");
//                itApbAlatDocEntityNew.setCreatedDate(bean.getLastUpdate());
//                itApbAlatDocEntityNew.setCreatedDateWho(bean.getLastUpdateWho());
//                itApbAlatDocEntityNew.setLastUpdate(bean.getLastUpdate());
//                itApbAlatDocEntityNew.setLastUpdateWho(bean.getLastUpdateWho());
//                itApbAlatDocEntityNew.setFlag("Y");
//                itApbAlatDocEntityNew.setAction("C");
//                alatDocDao.addAndSave(itApbAlatDocEntityNew);
//            }

            logger.info("[AlatDetailBoImpl.saveAdd] end process >>>");
        }
        return null;
    }

    @Override
    public List<AlatDetail> getByCriteria(AlatDetail searchBean) throws GeneralBOException {

        List<AlatDetail> listOfResultAlatDetail = new ArrayList<AlatDetail>();

        String role = CommonUtil.roleAsLogin();

        if (searchBean != null) {
            String flag = searchBean.getFlag();
            Map hsCriteria = new HashMap();

            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            if (searchBean.getIdAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getIdAlatDetail())) {
                hsCriteria.put("kode_alat_detail", searchBean.getIdAlatDetail());
            }
            if (searchBean.getKodeAlat() != null && !"".equalsIgnoreCase(searchBean.getKodeAlat())) {
                hsCriteria.put("kode_alat", searchBean.getKodeAlat());
            }
            if (searchBean.getKodeApp() != null && !"".equalsIgnoreCase(searchBean.getKodeApp())) {
                hsCriteria.put("kode_app", searchBean.getKodeApp());
            }
            if (searchBean.getNoGi() != null && !"".equalsIgnoreCase(searchBean.getNoGi())) {
                hsCriteria.put("no_gi", searchBean.getNoGi());
            }
            if (searchBean.getNamaAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getNamaAlatDetail())) {
                hsCriteria.put("nama_alat_detail", searchBean.getNamaAlatDetail());
            }
            if (searchBean.getNoSap() != null && !"".equalsIgnoreCase(searchBean.getNoSap())) {
                hsCriteria.put("no_sap", searchBean.getNoSap());
            }
            if (searchBean.getStatus() != null && !"".equalsIgnoreCase(searchBean.getStatus())) {
                hsCriteria.put("status", searchBean.getStatus());
            }
            if (searchBean.getNoAktiva() != null && !"".equalsIgnoreCase(searchBean.getNoAktiva())) {
                hsCriteria.put("no_aktiva", searchBean.getNoAktiva());
            }
            List<MtApbAlatDetailEntity> listOfAlatDetail = null;
            try {
                listOfAlatDetail = alatDetailDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
            if (listOfAlatDetail != null) {
                AlatDetail alatDetailData;
                for (MtApbAlatDetailEntity alatDetailEntity : listOfAlatDetail) {
                    alatDetailData = new AlatDetail();
                    alatDetailData.setIdAlatDetail(alatDetailEntity.getIdAlatDetail());
                    alatDetailData.setKodeAlat(alatDetailEntity.getKodeAlat());
                    alatDetailData.setKodeApp(alatDetailEntity.getKodeApp());
                    alatDetailData.setUrlFoto(alatDetailEntity.getUrlFoto());

                    String folderPath = CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT + alatDetailData.getUrlFoto();
                    alatDetailData.setFilePath("/pmsapb" + folderPath);

                    if (alatDetailData.getKodeApp() != null && !"".equalsIgnoreCase(alatDetailData.getKodeApp())) {
                        hsCriteria.put("kode_app", alatDetailData.getKodeApp());
                    }
                    List<ImApbAppEntity> listApp = new ArrayList<ImApbAppEntity>();
                    listApp = appDao.getByCriteria(hsCriteria);

                    if (listApp != null) {
                        for (ImApbAppEntity getApp : listApp) {
                            alatDetailData.setNamaApp(getApp.getNamaWilayah());
                        }
                    }

                    alatDetailData.setNoGi(alatDetailEntity.getNoGi());
                    alatDetailData.setNamaGi(alatDetailEntity.getNamaGi());
                    alatDetailData.setNoGiTujuan(alatDetailEntity.getNoGiTujuan());
                    alatDetailData.setNamaGiTujuan(alatDetailEntity.getNamaGiTujuan());
                    alatDetailData.setNamaAlatDetail(alatDetailEntity.getNamaAlatDetail());
                    alatDetailData.setMerk(alatDetailEntity.getMerk());
                    alatDetailData.setTegangan(alatDetailEntity.getTegangan());
                    alatDetailData.setTypeId(alatDetailEntity.getTypeId());
                    alatDetailData.setKdStatus(alatDetailEntity.getKdStatus());
                    alatDetailData.setNoSeries(alatDetailEntity.getNoSeries());
                    alatDetailData.setTglOprs(alatDetailEntity.getTglOprs());
                    alatDetailData.setTglPasang(alatDetailEntity.getTglPasang());
                    alatDetailData.setKeterangan(alatDetailEntity.getKeterangan());
                    alatDetailData.setJenisTp(alatDetailEntity.getJenisTp());
                    alatDetailData.setFrekuensiTx(alatDetailEntity.getFrekuensiTx());
                    alatDetailData.setFrekuensiRx(alatDetailEntity.getFrekuensiRx());
                    alatDetailData.setDaya(alatDetailEntity.getDaya());
                    alatDetailData.setTransmisi(alatDetailEntity.getTransmisi());
                    alatDetailData.setNote(alatDetailEntity.getNote());
                    alatDetailData.setFlag(alatDetailEntity.getFlag());
                    alatDetailData.setAction(alatDetailEntity.getAction());
                    alatDetailData.setNoSap(alatDetailEntity.getNoSap());
                    alatDetailData.setNoAktiva(alatDetailEntity.getNoAktiva());
                    alatDetailData.setStatus(alatDetailEntity.getStatus());
                    alatDetailData.setPax(alatDetailEntity.getPax());
                    alatDetailData.setTypePax(alatDetailEntity.getTypePax());
                    alatDetailData.setPowerSupply(alatDetailEntity.getPowerSupply());
                    alatDetailData.setRuangTerpasang(alatDetailEntity.getRuangTerpasang());
                    alatDetailData.setTypeSwitch(alatDetailEntity.getTypeSwitch());
                    alatDetailData.setLat(alatDetailEntity.getLat());
                    alatDetailData.setLongt(alatDetailEntity.getLongt());
                    alatDetailData.setTechidentno(alatDetailEntity.getTechidentno());
                    alatDetailData.setTypePlc(alatDetailEntity.getTypePlc());
                    alatDetailData.setBandwith(alatDetailEntity.getBandwith());
                    alatDetailData.setNamaRtu(alatDetailEntity.getNamaRtu());
                    alatDetailData.setIdRtu(alatDetailEntity.getIdRtu());
                    alatDetailData.setIdSubBidang(alatDetailEntity.getIdSubBidang());
                    alatDetailData.setIdBidang(alatDetailEntity.getIdBidang());
                    alatDetailData.setBatteryName(alatDetailEntity.getBatteryName());
                    alatDetailData.setJumlahCell(alatDetailEntity.getJumlahCell());
                    alatDetailData.setKapasitas(alatDetailEntity.getKapasitas());
                    alatDetailData.setFuse(alatDetailEntity.getFuse());
                    alatDetailData.setFungsi(alatDetailEntity.getFungsi());
                    alatDetailData.setRectifierName(alatDetailEntity.getRectifierName());
                    alatDetailData.setTeganganInput(alatDetailEntity.getTeganganInput());
                    alatDetailData.setTeganganOutput(alatDetailEntity.getTeganganOutput());
                    alatDetailData.setMcbInput(alatDetailEntity.getMcbInput());
                    alatDetailData.setMcbOutput(alatDetailEntity.getMcbOutput());
                    alatDetailData.setPhase(alatDetailEntity.getPhase());
                    alatDetailData.setMode(alatDetailEntity.getMode());

                    if (role.equalsIgnoreCase("ADMIN ASSET")){
                        alatDetailData.setEnBtn(true);
                    }

                    if (role.equalsIgnoreCase("ADMIN")){
                        alatDetailData.setEnBtn(true);
                    }

                    if (alatDetailEntity.getIdSubBidang() != null){
                        hsCriteria = new HashMap();
                        hsCriteria.put("sub_bidang_id", alatDetailData.getIdSubBidang());
                        hsCriteria.put("flag","Y");

                        List<ImSubBidangEntity> imSubBidangEntityList = null;

                        try {
                            imSubBidangEntityList = subBidangDao.getByCriteria(hsCriteria);
                        } catch (HibernateException e) {
                            logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                        }

                        if (imSubBidangEntityList != null){
                            for (ImSubBidangEntity listSub : imSubBidangEntityList){
                                alatDetailData.setNamaSubBidang(listSub.getNamaSubBidang());
                            }
                        }
                    }


                    if (alatDetailEntity.getIdBidang()!= null){
                        hsCriteria = new HashMap();
                        hsCriteria.put("bidang_id", alatDetailEntity.getIdBidang());
                        hsCriteria.put("flag", "Y");

                        List<ImApbBidangEntity> bidangEntities = null;

                        try {
                            bidangEntities = bidangDao.getByCriteria(hsCriteria);
                        } catch (HibernateException e) {
                            logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                        }

                        if (bidangEntities != null){
                            for (ImApbBidangEntity listSub : bidangEntities){
                                alatDetailData.setNamaBidang(listSub.getNamaBidang());
                            }
                        }
                    }

                    alatDetailData.setEnDaya("N");
                    alatDetailData.setEnFrekuensiRx("N");
                    alatDetailData.setEnFrekuensiTx("N");
                    alatDetailData.setEnTransmisi("N");
                    alatDetailData.setEnTegOprs("N");
                    alatDetailData.setEnJenisTp("N");

                    if (alatDetailData.getKodeAlat() != null){
//                        hsCriteria.put("kode_alat", alatDetailData.getKodeAlat());
                        List<AlatFeature> listOfAlatFeature = null;
                        AlatFeature alatFeature = new AlatFeature();
                        alatFeature.setKodeAlat(alatDetailData.getKodeAlat());
                        listOfAlatFeature = alatFeatureDao.getDataAlatFeature(alatFeature);

                        if (listOfAlatFeature != null){
                            for (AlatFeature listAlatFeatureData : listOfAlatFeature){
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getDaya())){
                                    alatDetailData.setEnDaya("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getFrekuensiRx())){
                                    alatDetailData.setEnFrekuensiRx("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getFrekuensiTx())){
                                    alatDetailData.setEnFrekuensiTx("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getTransmisi())){
                                    alatDetailData.setEnTransmisi("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getTegOprs())){
                                    alatDetailData.setEnTegOprs("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getJenisTp())){
                                    alatDetailData.setEnJenisTp("Y");
                                }

                            }
                        }
                    }


                    Map alatCriteria = new HashMap();
                    alatCriteria.put("kode_alat_detail", alatDetailEntity.getIdAlatDetail());
                    alatCriteria.put("flag", "Y");
                    List<ItApbAlatDocEntity> itApbAlatDocEntities = null;
                    try {
                        itApbAlatDocEntities = alatDocDao.getByCriteria(alatCriteria);
                    } catch (GeneralBOException e) {
                        logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                    }

                    if (itApbAlatDocEntities != null) {
                        for (ItApbAlatDocEntity listData : itApbAlatDocEntities) {
                            String folderPathAlat = "/pmsapb" + CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT + listData.getUrlPhoto();
                            if ("1".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgFront(folderPathAlat);
                                alatDetailData.setThumbFoto("<img src = "+folderPathAlat+" height='25px' width='25px'/>");
                            }
                            if ("2".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgBack(folderPathAlat);
                            }
                            if ("3".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgLeft(folderPathAlat);
                            }
                            if ("4".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgRight(folderPathAlat);
                            }
                            if ("5".equalsIgnoreCase(listData.getPosition())){
                                alatDetailData.setDocName(listData.getUrlPhoto());
                                alatDetailData.setFilePath(folderPathAlat);
                            }
                        }
                    }


//                    String kdalat = alatDetailData.getKodeAlat();
//                    String activeFlag = alatDetailData.getFlag();
//                    String col = "kode_alat";
//                    int count;
//                    count = alatDetailDao.getCount(col, kdalat, activeFlag);

                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    if (alatDetailData.getTglOprs() != null) {
                        alatDetailData.setStTglOprs(df.format(alatDetailData.getTglOprs()));
                    }
                    if (alatDetailData.getTglPasang() != null) {
                        alatDetailData.setStTglPasang(df.format(alatDetailData.getTglPasang()));
                    }
                    listOfResultAlatDetail.add(alatDetailData);
                }
            }
        }
        return listOfResultAlatDetail;
    }

    @Override
    public List<AlatDetail> getByAlat (AlatDetail searchBean) throws GeneralBOException {

        List<AlatDetail> listOfResultAlatDetail = new ArrayList<AlatDetail>();

        if (searchBean != null) {
            String flag = searchBean.getFlag();
            Map hsCriteria = new HashMap();

            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            if (searchBean.getIdAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getIdAlatDetail())) {
                hsCriteria.put("kode_alat_detail", searchBean.getIdAlatDetail());
            }
            if (searchBean.getKodeAlat() != null && !"".equalsIgnoreCase(searchBean.getKodeAlat())) {
                hsCriteria.put("kode_alat", searchBean.getKodeAlat());
            }
            if (searchBean.getKodeApp() != null && !"".equalsIgnoreCase(searchBean.getKodeApp())) {
                hsCriteria.put("kode_app", searchBean.getKodeApp());
            }
            if (searchBean.getNoGi() != null && !"".equalsIgnoreCase(searchBean.getNoGi())) {
                hsCriteria.put("no_gi", searchBean.getNoGi());
            }
            if (searchBean.getNamaAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getNamaAlatDetail())) {
                hsCriteria.put("nama_alat_detail", searchBean.getNamaAlatDetail());
            }
            if (searchBean.getNoSap() != null && !"".equalsIgnoreCase(searchBean.getNoSap())) {
                hsCriteria.put("no_sap", searchBean.getNoSap());
            }
            if (searchBean.getStatus() != null && !"".equalsIgnoreCase(searchBean.getStatus())) {
                hsCriteria.put("status", searchBean.getStatus());
            }
            if (searchBean.getNoAktiva() != null && !"".equalsIgnoreCase(searchBean.getNoAktiva())) {
                hsCriteria.put("no_aktiva", searchBean.getNoAktiva());
            }
            List<MtApbAlatDetailEntity> listOfAlatDetail = null;
            try {
                listOfAlatDetail = alatDetailDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
            if (listOfAlatDetail != null) {
                AlatDetail alatDetailData;
                for (MtApbAlatDetailEntity alatDetailEntity : listOfAlatDetail) {
                    alatDetailData = new AlatDetail();
                    alatDetailData.setIdAlatDetail(alatDetailEntity.getIdAlatDetail());
                    alatDetailData.setKodeAlat(alatDetailEntity.getKodeAlat());
                    alatDetailData.setKodeApp(alatDetailEntity.getKodeApp());
                    alatDetailData.setUrlFoto(alatDetailEntity.getUrlFoto());

                    String folderPath = CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT + alatDetailData.getUrlFoto();
                    alatDetailData.setFilePath("/pmsapb" + folderPath);

                    if (alatDetailData.getKodeApp() != null && !"".equalsIgnoreCase(alatDetailData.getKodeApp())) {
                        hsCriteria.put("kode_app", alatDetailData.getKodeApp());
                    }
                    List<ImApbAppEntity> listApp = new ArrayList<ImApbAppEntity>();
                    listApp = appDao.getByCriteria(hsCriteria);

                    if (listApp != null) {
                        for (ImApbAppEntity getApp : listApp) {
                            alatDetailData.setNamaApp(getApp.getNamaWilayah());
                        }
                    }

                    alatDetailData.setNoGi(alatDetailEntity.getNoGi());
                    alatDetailData.setNamaGi(alatDetailEntity.getNamaGi());
                    alatDetailData.setNoGiTujuan(alatDetailEntity.getNoGiTujuan());
                    alatDetailData.setNamaGiTujuan(alatDetailEntity.getNamaGiTujuan());
                    alatDetailData.setNamaAlatDetail(alatDetailEntity.getNamaAlatDetail());
                    alatDetailData.setMerk(alatDetailEntity.getMerk());
                    alatDetailData.setTegangan(alatDetailEntity.getTegangan());
                    alatDetailData.setTypeId(alatDetailEntity.getTypeId());
                    alatDetailData.setKdStatus(alatDetailEntity.getKdStatus());
                    alatDetailData.setNoSeries(alatDetailEntity.getNoSeries());
                    alatDetailData.setTglOprs(alatDetailEntity.getTglOprs());
                    alatDetailData.setTglPasang(alatDetailEntity.getTglPasang());
                    alatDetailData.setKeterangan(alatDetailEntity.getKeterangan());
                    alatDetailData.setJenisTp(alatDetailEntity.getJenisTp());
                    alatDetailData.setFrekuensiTx(alatDetailEntity.getFrekuensiTx());
                    alatDetailData.setFrekuensiRx(alatDetailEntity.getFrekuensiRx());
                    alatDetailData.setDaya(alatDetailEntity.getDaya());
                    alatDetailData.setTransmisi(alatDetailEntity.getTransmisi());
                    alatDetailData.setNote(alatDetailEntity.getNote());
                    alatDetailData.setFlag(alatDetailEntity.getFlag());
                    alatDetailData.setAction(alatDetailEntity.getAction());
                    alatDetailData.setNoSap(alatDetailEntity.getNoSap());
                    alatDetailData.setNoAktiva(alatDetailEntity.getNoAktiva());
                    alatDetailData.setStatus(alatDetailEntity.getStatus());
                    alatDetailData.setPax(alatDetailEntity.getPax());
                    alatDetailData.setTypePax(alatDetailEntity.getTypePax());
                    alatDetailData.setPowerSupply(alatDetailEntity.getPowerSupply());
                    alatDetailData.setRuangTerpasang(alatDetailEntity.getRuangTerpasang());
                    alatDetailData.setTypeSwitch(alatDetailEntity.getTypeSwitch());
                    alatDetailData.setLat(alatDetailEntity.getLat());
                    alatDetailData.setLongt(alatDetailEntity.getLongt());
                    alatDetailData.setTechidentno(alatDetailEntity.getTechidentno());
                    alatDetailData.setTypePlc(alatDetailEntity.getTypePlc());
                    alatDetailData.setBandwith(alatDetailEntity.getBandwith());
                    alatDetailData.setNamaRtu(alatDetailEntity.getNamaRtu());
                    alatDetailData.setIdRtu(alatDetailEntity.getIdRtu());
                    alatDetailData.setIdSubBidang(alatDetailEntity.getIdSubBidang());
                    alatDetailData.setIdBidang(alatDetailEntity.getIdBidang());
                    alatDetailData.setBatteryName(alatDetailEntity.getBatteryName());
                    alatDetailData.setJumlahCell(alatDetailEntity.getJumlahCell());
                    alatDetailData.setKapasitas(alatDetailEntity.getKapasitas());
                    alatDetailData.setFuse(alatDetailEntity.getFuse());
                    alatDetailData.setFungsi(alatDetailEntity.getFungsi());
                    alatDetailData.setRectifierName(alatDetailEntity.getRectifierName());
                    alatDetailData.setTeganganInput(alatDetailEntity.getTeganganInput());
                    alatDetailData.setTeganganOutput(alatDetailEntity.getTeganganOutput());
                    alatDetailData.setMcbInput(alatDetailEntity.getMcbInput());
                    alatDetailData.setMcbOutput(alatDetailEntity.getMcbOutput());
                    alatDetailData.setPhase(alatDetailEntity.getPhase());
                    alatDetailData.setMode(alatDetailEntity.getMode());

                    if (alatDetailEntity.getIdSubBidang() != null){
                        hsCriteria = new HashMap();
                        hsCriteria.put("sub_bidang_id", alatDetailData.getIdSubBidang());
                        hsCriteria.put("flag","Y");

                        List<ImSubBidangEntity> imSubBidangEntityList = null;

                        try {
                            imSubBidangEntityList = subBidangDao.getByCriteria(hsCriteria);
                        } catch (HibernateException e) {
                            logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                        }

                        if (imSubBidangEntityList != null){
                            for (ImSubBidangEntity listSub : imSubBidangEntityList){
                                alatDetailData.setNamaSubBidang(listSub.getNamaSubBidang());
                            }
                        }
                    }


                    if (alatDetailEntity.getIdBidang()!= null){
                        hsCriteria = new HashMap();
                        hsCriteria.put("bidang_id", alatDetailEntity.getIdBidang());
                        hsCriteria.put("flag", "Y");

                        List<ImApbBidangEntity> bidangEntities = null;

                        try {
                            bidangEntities = bidangDao.getByCriteria(hsCriteria);
                        } catch (HibernateException e) {
                            logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                        }

                        if (bidangEntities != null){
                            for (ImApbBidangEntity listSub : bidangEntities){
                                alatDetailData.setNamaBidang(listSub.getNamaBidang());
                            }
                        }
                    }

                    alatDetailData.setEnDaya("N");
                    alatDetailData.setEnFrekuensiRx("N");
                    alatDetailData.setEnFrekuensiTx("N");
                    alatDetailData.setEnTransmisi("N");
                    alatDetailData.setEnTegOprs("N");
                    alatDetailData.setEnJenisTp("N");

                    if (alatDetailData.getKodeAlat() != null){
//                        hsCriteria.put("kode_alat", alatDetailData.getKodeAlat());
                        List<AlatFeature> listOfAlatFeature = null;
                        AlatFeature alatFeature = new AlatFeature();
                        alatFeature.setKodeAlat(alatDetailData.getKodeAlat());
                        listOfAlatFeature = alatFeatureDao.getDataAlatFeature(alatFeature);

                        if (listOfAlatFeature != null){
                            for (AlatFeature listAlatFeatureData : listOfAlatFeature){
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getDaya())){
                                    alatDetailData.setEnDaya("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getFrekuensiRx())){
                                    alatDetailData.setEnFrekuensiRx("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getFrekuensiTx())){
                                    alatDetailData.setEnFrekuensiTx("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getTransmisi())){
                                    alatDetailData.setEnTransmisi("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getTegOprs())){
                                    alatDetailData.setEnTegOprs("Y");
                                }
                                if ("Y".equalsIgnoreCase(listAlatFeatureData.getJenisTp())){
                                    alatDetailData.setEnJenisTp("Y");
                                }

                            }
                        }
                    }


                    Map alatCriteria = new HashMap();
                    alatCriteria.put("kode_alat_detail", alatDetailEntity.getIdAlatDetail());
                    alatCriteria.put("flag", "Y");
                    List<ItApbAlatDocEntity> itApbAlatDocEntities = null;
                    try {
                        itApbAlatDocEntities = alatDocDao.getByCriteria(alatCriteria);
                    } catch (GeneralBOException e) {
                        logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                    }

                    if (itApbAlatDocEntities != null) {
                        for (ItApbAlatDocEntity listData : itApbAlatDocEntities) {
                            String folderPathAlat = "/pmsapb" + CommonConstant.RESOURCE_PATH_USER_UPLOAD_ALAT + listData.getUrlPhoto();
                            if ("1".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgFront(folderPathAlat);
                                alatDetailData.setThumbFoto("<img src = "+folderPathAlat+" height='25px' width='25px'/>");
                            }
                            if ("2".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgBack(folderPathAlat);
                            }
                            if ("3".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgLeft(folderPathAlat);
                            }
                            if ("4".equalsIgnoreCase(listData.getPosition())) {
                                alatDetailData.setImgRight(folderPathAlat);
                            }
                            if ("5".equalsIgnoreCase(listData.getPosition())){
                                alatDetailData.setDocName(listData.getUrlPhoto());
                                alatDetailData.setFilePath(folderPathAlat);
                            }
                        }
                    }

                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    if (alatDetailData.getTglOprs() != null) {
                        alatDetailData.setStTglOprs(df.format(alatDetailData.getTglOprs()));
                    }
                    if (alatDetailData.getTglPasang() != null) {
                        alatDetailData.setStTglPasang(df.format(alatDetailData.getTglPasang()));
                    }
                    listOfResultAlatDetail.add(alatDetailData);
                }
            }
        }
        return listOfResultAlatDetail;
    }


    @Override
    public List<AlatDetail> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }

    @Override
    public String getSeqAlatDetailId() throws GeneralBOException {

        String idAlatDetail = null;
        try {
            idAlatDetail = alatDetailDao.getNextAlatDetailId();
        } catch (HibernateException e) {
            logger.error("[PermohonanLahanBoImpl.getSeqNumLahanId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }
        return idAlatDetail;
    }

    @Override
    public List<AlatDetail> getComboAlatByCriteria(String query) throws GeneralBOException {
        logger.info("[UserBoImpl.getComboUserWithCriteria] start process >>>");

        List<AlatDetail> listOfComboAlat = new ArrayList();
        String criteria = "%" + query + "%";

        List<MtApbAlatDetailEntity> listAlat = null;
        try {
            listAlat = alatDetailDao.getListAlatDetail(criteria);
        } catch (HibernateException e) {
            logger.error("[UserBoImpl.getComboUserWithCriteria] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when retieving list user with criteria, please info to your admin..." + e.getMessage());
        }
        List<ImApbAppEntity> listApp = null;

        if (listAlat != null) {
            for (MtApbAlatDetailEntity mtAlat : listAlat) {
                AlatDetail itemComboAlat = new AlatDetail();
                itemComboAlat.setIdAlatDetail(mtAlat.getIdAlatDetail());
                itemComboAlat.setKodeApp(mtAlat.getKodeApp());
                itemComboAlat.setNamaAlatDetail(mtAlat.getNamaAlatDetail());
                itemComboAlat.setNamaGi(mtAlat.getNamaGi());
                Map hsCriteria = new HashMap();
                if (mtAlat.getKodeApp() != null && !"".equalsIgnoreCase(mtAlat.getKodeApp())) {
                    hsCriteria.put("kode_alat_detail", mtAlat.getKodeApp());
                    hsCriteria.put("flag", "Y");
                }
                listApp = appDao.getByCriteria(hsCriteria);

                if (listApp != null) {
                    for (ImApbAppEntity listitem : listApp) {
                        itemComboAlat.setNamaApp(listitem.getNamaWilayah());
                    }
                }

                listOfComboAlat.add(itemComboAlat);
            }
        }
        logger.info("[UserBoImpl.getComboUserWithCriteria] end process <<<");
        return listOfComboAlat;
    }

    @Override
    public List<AlatDetailLog> getLogByCriteria(AlatDetailLog searchBean) throws GeneralBOException {
        List<AlatDetailLog> listOfResultLogAlatDetail = new ArrayList<AlatDetailLog>();

        if (searchBean != null) {
            String flag = searchBean.getFlag();
            Map hsCriteria = new HashMap();

            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            if (searchBean.getIdLogAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getIdLogAlatDetail())) {
                hsCriteria.put("kode_log_alat_detail", searchBean.getIdLogAlatDetail());
            }

            if (searchBean.getIdAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getIdAlatDetail())) {
                hsCriteria.put("kode_alat_detail", searchBean.getIdAlatDetail());
            }
            if (searchBean.getKodeAlat() != null && !"".equalsIgnoreCase(searchBean.getKodeAlat())) {
                hsCriteria.put("kode_alat", searchBean.getKodeAlat());
            }
            if (searchBean.getNamaAlatDetail() != null && !"".equalsIgnoreCase(searchBean.getNamaAlatDetail())) {
                hsCriteria.put("nama_alat_detail", searchBean.getNamaAlatDetail());
            }
            List<MtApbAlatDetailLogEntity> listOfLogAlatDetail = null;
            try {
                listOfLogAlatDetail = alatDetailLogDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
            if (listOfLogAlatDetail != null){
                AlatDetailLog logData;
                for (MtApbAlatDetailLogEntity dataLogEntity : listOfLogAlatDetail){
                    logData = new AlatDetailLog();
                    logData.setIdLogAlatDetail(dataLogEntity.getIdLogAlatDetail());
                    logData.setIdAlatDetail(dataLogEntity.getIdAlatDetail());
                    logData.setKodeAlat(dataLogEntity.getKodeAlat());
                    logData.setKodeApp(dataLogEntity.getKodeApp());

                    hsCriteria.put("kode_app", dataLogEntity.getKodeApp());

                    List<ImApbAppEntity> imApbAppEntity = null;
                    try {
                        imApbAppEntity = appDao.getByCriteria(hsCriteria);
                    } catch (HibernateException e) {
                        logger.error("[AlatDetailBoImpl.getByCriteria] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                    }

                    if (imApbAppEntity != null){
                        for (ImApbAppEntity dataApp : imApbAppEntity){
                            logData.setNamaApp(dataApp.getNamaWilayah());
                        }
                    }

                    logData.setNoGi(dataLogEntity.getNoGi());
                    logData.setNamaGi(dataLogEntity.getNamaGi());
                    logData.setNoGiTujuan(dataLogEntity.getNoGiTujuan());
                    logData.setNamaAlatDetail(dataLogEntity.getNamaAlatDetail());
                    logData.setMerk(dataLogEntity.getMerk());
                    logData.setTegangan(dataLogEntity.getTegangan());
                    logData.setTypeId(dataLogEntity.getTypeId());
                    logData.setNoSeries(dataLogEntity.getNoSeries());
                    logData.setKdStatus(dataLogEntity.getKdStatus());
                    logData.setTglOprs(dataLogEntity.getTglOprs());
                    logData.setTglPasang(dataLogEntity.getTglPasang());
                    logData.setTglPasang(dataLogEntity.getTglPasang());
                    logData.setKeterangan(dataLogEntity.getKeterangan());
                    logData.setJenisTp(dataLogEntity.getJenisTp());
                    logData.setFrekuensiTx(dataLogEntity.getFrekuensiTx());
                    logData.setFrekuensiRx(dataLogEntity.getFrekuensiRx());
                    logData.setDaya(dataLogEntity.getDaya());
                    logData.setTransmisi(dataLogEntity.getTransmisi());
                    logData.setFlag(dataLogEntity.getFlag());
                    logData.setAction(dataLogEntity.getAction());
                    logData.setLastUpdateWho(dataLogEntity.getLastUpdateWho());
                    logData.setNamaGiTujuan(dataLogEntity.getNamaGiTujuan());
                    logData.setLastUpdate(dataLogEntity.getLastUpdate());
                    logData.setUrlFoto(dataLogEntity.getUrlFoto());
                    logData.setNote(dataLogEntity.getNote());
                    logData.setStatus(dataLogEntity.getStatus());
                    listOfResultLogAlatDetail.add(logData);
                }
            }
        }
        return listOfResultLogAlatDetail;
    }

    @Override
    public void saveTpChanel(TpChanel tpChanel) throws GeneralBOException {
        if (tpChanel != null){
            ImApbTpChanelEntity add = new ImApbTpChanelEntity();

            boolean save = false;

            if (tpChanel.isAdd() == true){
                String id = "";
                id = tpChanelDao.getNextTpId();
                add.setChanelId("TP"+id);
                save = true;
            }

            if (tpChanel.isEdit() == true){
                add.setChanelId(tpChanel.getChanelId());
                save = true;
            }


            add.setChanel(tpChanel.getChanel());
            add.setKodeAlatDetail(tpChanel.getKodeAlatDetail());
            add.setKodeAlat(tpChanel.getKodeAlat());
            add.setChanel(tpChanel.getChanel());
            add.setFungsiChanel(tpChanel.getFungsiChanel());
            add.setCreatedDate(tpChanel.getCreatedDate());
            add.setCreatedWho(tpChanel.getCreatedWho());
            add.setLastUpdate(tpChanel.getLastUpdate());
            add.setLastUpdateWho(tpChanel.getLastUpdateWho());
            add.setFlag(tpChanel.getFlag());
            add.setAction(tpChanel.getAction());

            if (save){
                try {
                    if (tpChanel.isAdd() == true){
                        tpChanelDao.addAndSave(add);
                    }
                    if (tpChanel.isEdit() == true){
                        tpChanelDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveTpChanel] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }


    public void saveSwitchPort(SwitchPort bean) throws GeneralBOException {
        if (bean != null){

            ImApbSwitchPortEntity add = new ImApbSwitchPortEntity();

            boolean save = false;
            if (bean.isAdd() == true){
                String id = "";
                id = switchPortDao.getNextSwitchId();
                add.setSwitchPortId("SW"+id);
                save = true;
            }

            if (bean.isEdit() == true){
                add.setSwitchPortId(bean.getSwitchPortId());
                save = true;
            }

            add.setPort(bean.getPort());
            add.setKodeAlatDetail(bean.getKodeAlatDetail());
            add.setKodeAlat(bean.getKodeAlat());
            add.setFungsiPort(bean.getFungsiPort());
            add.setCreatedDate(bean.getCreatedDate());
            add.setCreatedWho(bean.getCreatedWho());
            add.setLastUpdate(bean.getLastUpdate());
            add.setLastUpdateWho(bean.getLastUpdateWho());
            add.setFlag(bean.getFlag());
            add.setAction(bean.getAction());
            add.setType(bean.getType());

            if (save){
                try {
                    if (bean.isAdd() == true){
                        switchPortDao.addAndSave(add);
                    }
                    if (bean.isEdit() == true){
                        switchPortDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveSwitchPort] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void savePlcChanel(PlcChanel bean) throws GeneralBOException {
        if (bean != null){

            ImApbPlcChanelEntity add = new ImApbPlcChanelEntity();
            boolean save = false;

            if (bean.isAdd() == true){
                String id = "";
                id = plcChanelDao.getNextId();
                add.setChanelId("TPC"+id);
                save = true;
            }

            if (bean.isEdit() == true){
                add.setChanelId(bean.getChanelId());
                save = true;
            }

            add.setChanel(bean.getChanel());
            add.setKodeAlatDetail(bean.getKodeAlatDetail());
            add.setKodeAlat(bean.getKodeAlat());
            add.setChanel(bean.getChanel());
            add.setFungsiChanel(bean.getFungsiChanel());
            add.setFrekuensi(bean.getFrekuensi());
            add.setCreatedDate(bean.getCreatedDate());
            add.setCreatedWho(bean.getCreatedWho());
            add.setLastUpdate(bean.getLastUpdate());
            add.setLastUpdateWho(bean.getLastUpdateWho());
            add.setFlag(bean.getFlag());
            add.setAction(bean.getAction());

            if (save){
                try {
                    if (bean.isAdd() == true){
                        plcChanelDao.addAndSave(add);
                    }
                    if (bean.isEdit() == true){
                        plcChanelDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.savePlcChanel] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void saveKapasitasMux(KapasitasMux bean) throws GeneralBOException {
        if (bean != null){


            ImApbKapsitasMuxEntity add = new ImApbKapsitasMuxEntity();
            boolean save = false;

            if (bean.isAdd() == true){
                String id = "";
                id = kapasitasMuxDao.getNextId();
                add.setKapasitasId("KPM"+id);
                save = true;
            }
            if (bean.isEdit() == true){
                add.setKapasitasId(bean.getKapasitasId());
                save = true;
            }

            add.setNamaModul(bean.getNamaModul());
            add.setKodeAlatDetail(bean.getKodeAlatDetail());
            add.setKodeAlat(bean.getKodeAlat());
            add.setKapasitas(bean.getKapasitas());
            add.setTimeSlot(bean.getTimeSlot());
            add.setFungsi(bean.getFungsi());
            add.setCreatedDate(bean.getCreatedDate());
            add.setCreatedWho(bean.getCreatedWho());
            add.setLastUpdate(bean.getLastUpdate());
            add.setLastUpdateWho(bean.getLastUpdateWho());
            add.setFlag(bean.getFlag());
            add.setAction(bean.getAction());

            if (save){
                try {
                    if (bean.isAdd() == true){
                        kapasitasMuxDao.addAndSave(add);
                    }
                    if (bean.isEdit() == true){
                        kapasitasMuxDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveKapasitasMux] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void saveModulRtu(ModulRtu bean) throws GeneralBOException {
        if (bean != null){

            ImApbModulRtuEntity add = new ImApbModulRtuEntity();
            boolean save = false;

            if (bean.isAdd() == true){
                String id = "";
                id = modulRtuDao.getNextId();
                add.setModulId("MRU"+id);
                save = true;
            }
            if (bean.isEdit() == true){
                add.setModulId(bean.getModulId());
                save = true;
            }

            add.setModul(bean.getModul());
            add.setKodeAlatDetail(bean.getKodeAlatDetail());
            add.setKodeAlat(bean.getKodeAlat());
            add.setQty(bean.getQty());
            add.setJumlahPoint(bean.getJumlahPoint());
            add.setCreatedDate(bean.getCreatedDate());
            add.setCreatedWho(bean.getCreatedWho());
            add.setLastUpdate(bean.getLastUpdate());
            add.setLastUpdateWho(bean.getLastUpdateWho());
            add.setFlag(bean.getFlag());
            add.setAction(bean.getAction());

            if (save){
                try {
                    if (bean.isAdd() == true){
                        modulRtuDao.addAndSave(add);
                    }
                    if (bean.isEdit() == true){
                        modulRtuDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveModulRtu] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void saveBayRtu(BayRtu bean) throws GeneralBOException {
        if (bean != null){

            ImApbBayRtuEntity add = new ImApbBayRtuEntity();
            boolean save = false;

            if (bean.isAdd() == true){
                String id = "";
                id = bayRtuDao.getNextId();
                add.setBayId("BYU"+id);
                save = true;
            }
            if (bean.isEdit() == true){
                add.setBayId(bean.getBayId());
                save = true;
            }

            add.setNamaBay(bean.getNamaBay());
            add.setJumlahDi(bean.getJumlahDi());
            add.setJumlahDo(bean.getJumlahDo());
            add.setJumlahAo(bean.getJumlahAo());
            add.setJumlahAi(bean.getJumlahAi());
            add.setMerk(bean.getMerk());
            add.setRatioVt(bean.getRatioVt());
            add.setRatioCt(bean.getRatioCt());
            add.setKodeAlatDetail(bean.getKodeAlatDetail());
            add.setKodeAlat(bean.getKodeAlat());
            add.setCreatedDate(bean.getCreatedDate());
            add.setCreatedWho(bean.getCreatedWho());
            add.setLastUpdate(bean.getLastUpdate());
            add.setLastUpdateWho(bean.getLastUpdateWho());
            add.setFlag(bean.getFlag());
            add.setAction(bean.getAction());

            if (save){
                try {
                    if (bean.isAdd() == true){
                        bayRtuDao.addAndSave(add);
                    }
                    if (bean.isEdit() == true){
                        bayRtuDao.updateAndSave(add);
                    }

                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveBayRtu] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void saveProteksiSetting(ProteksiSetting bean) throws GeneralBOException {
        if (bean != null){

            ImApbProteksiSettingEntity add = new ImApbProteksiSettingEntity();
            boolean save = false;

            if (bean.isAdd() == true){
                String id = "";
                id = proteksiSettingDao.getNextId();
                add.setProteksiSettingId("PRT"+id);
                save = true;
            }
            if (bean.isEdit() == true){
                add.setProteksiSettingId(bean.getProteksiSettingId());
                save = true;
            }

            add.setTahap(bean.getTahap());
            add.setArus(bean.getArus());
            add.setTarget(bean.getTarget());
            add.setWaktu(bean.getWaktu());
            add.setKodeAlatDetail(bean.getKodeAlatDetail());
            add.setKodeAlat(bean.getKodeAlat());
            add.setCreatedDate(bean.getCreatedDate());
            add.setCreatedWho(bean.getCreatedWho());
            add.setLastUpdate(bean.getLastUpdate());
            add.setLastUpdateWho(bean.getLastUpdateWho());
            add.setFlag(bean.getFlag());
            add.setAction(bean.getAction());

            if (save){
                try {
                    if (bean.isAdd() == true){
                        proteksiSettingDao.addAndSave(add);
                    }
                    if (bean.isEdit() == true){
                        proteksiSettingDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveProteksiSetting] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }
    @Override
    public void saveBattery(Battery battery) throws GeneralBOException {
        if (battery != null){
            ImApbBatteryEntity add = new ImApbBatteryEntity();

            boolean save = false;

            if (battery.isAdd() == true){
                String id = "";
                id = batteryDao.getNextBatId();
                add.setBatteryId("RMS"+id);
                save = true;
            }

            if (battery.isEdit() == true){
                add.setBatteryId(battery.getBatteryId());
                save = true;
            }

            add.setBatteryName(battery.getBatteryName());
            add.setKodeAlatDetail(battery.getKodeAlatDetail());
            add.setKodeAlat(battery.getKodeAlat());
            add.setTegangan(battery.getTegangan());
            add.setJumlahCell(battery.getJumlahCell());
            add.setMerk(battery.getMerk());
            add.setType(battery.getType());
            add.setKapasitas(battery.getKapasitas());
            add.setFuse(battery.getFuse());
            add.setFungsi(battery.getFungsi());
            add.setCreatedDate(battery.getCreatedDate());
            add.setCreatedWho(battery.getCreatedWho());
            add.setLastUpdate(battery.getLastUpdate());
            add.setLastUpdateWho(battery.getLastUpdateWho());
            add.setFlag(battery.getFlag());
            add.setAction(battery.getAction());

            if (save){
                try {
                    if (battery.isAdd() == true){
                        batteryDao.addAndSave(add);
                    }
                    if (battery.isEdit() == true){
                        batteryDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveTpChanel] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void saveRectifier(Rectifier rectifier) throws GeneralBOException {
        if (rectifier != null){
            ImApbRectifierEntity add = new ImApbRectifierEntity();

            boolean save = false;

            if (rectifier.isAdd() == true){
                String id = "";
                id = rectifierDao.getNextRecId();
                add.setRectifierId("RECT"+id);
                save = true;
            }

            if (rectifier.isEdit() == true){
                add.setRectifierId(rectifier.getRectifierId());
                save = true;
            }

            add.setKodeAlatDetail(rectifier.getKodeAlatDetail());
            add.setKodeAlat(rectifier.getKodeAlat());
            add.setPanjangAcdb(rectifier.getPanjangAcdb());
            add.setPanjangBattery(rectifier.getPanjangBattery());
            add.setPanjangDcdb(rectifier.getPanjangDcdb());
            add.setTypeAcdb(rectifier.getTypeAcdb());
            add.setTypeBattery(rectifier.getTypeBattery());
            add.setTypeDcdb(rectifier.getTypeDcdb());
            add.setCreatedDate(rectifier.getCreatedDate());
            add.setCreatedWho(rectifier.getCreatedWho());
            add.setLastUpdate(rectifier.getLastUpdate());
            add.setLastUpdateWho(rectifier.getLastUpdateWho());
            add.setFlag(rectifier.getFlag());
            add.setAction(rectifier.getAction());

            if (save){
                try {
                    if (rectifier.isAdd() == true){
                        rectifierDao.addAndSave(add);
                    }
                    if (rectifier.isEdit() == true){
                        rectifierDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveTpChanel] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }


    @Override
    public void saveSasStation(SasStation bean) throws GeneralBOException {
        if (bean != null){

            ImApbSasStationEntity add = new ImApbSasStationEntity();
            boolean save = false;

            if (bean.isAdd() == true){
                String id = "";
                id = sasStationDao.getNextId();
                add.setSasStationId("STS"+id);
                save = true;
            }
            if (bean.isEdit() == true){
                add.setSasStationId(bean.getSasStationId());
                save = true;
            }

            add.setNamaStation(bean.getNamaSatation());
            add.setTipeStation(bean.getTipeStation());
            add.setNamaSoftware(bean.getNamaSoftWare());
            add.setVersiSoftware(bean.getVersiSoftware());
            add.setTipe(bean.getTipe());
            add.setMerk(bean.getMerk());
            add.setJumlah(bean.getJumlah());
            add.setSn(bean.getSn());
            add.setKodeAlatDetail(bean.getKodeAlatDetail());
            add.setKodeAlat(bean.getKodeAlat());
            add.setCreatedDate(bean.getCreatedDate());
            add.setCreatedWho(bean.getCreatedWho());
            add.setLastUpdate(bean.getLastUpdate());
            add.setLastUpdateWho(bean.getLastUpdateWho());
            add.setFlag(bean.getFlag());
            add.setAction(bean.getAction());

            if (save){
                try {
                    if (bean.isAdd() == true){
                        sasStationDao.addAndSave(add);
                    }
                    if (bean.isEdit() == true){
                        sasStationDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveProteksiSetting] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void saveSasControl(SasControl bean) throws GeneralBOException {
        if (bean != null){

            ImApbSasControlEntity add = new ImApbSasControlEntity();
            boolean save = false;

            if (bean.isAdd() == true){
                String id = "";
                id = sasControlDao.getNextId();
                add.setSasControlId("SCR"+id);
                save = true;
            }
            if (bean.isEdit() == true){
                add.setSasControlId(bean.getSasControlId());
                save = true;
            }

            add.setNamaControl(bean.getNamaControl());
            add.setTipeControl(bean.getTipeControl());
            add.setNamaSoftware(bean.getNamaSoftWare());
            add.setVersiSoftware(bean.getVersiSoftware());
            add.setTipe(bean.getTipe());
            add.setMerk(bean.getMerk());
            add.setJumlahDi(bean.getJumlahDi());
            add.setJumlahDo(bean.getJumlahDo());
            add.setJumlahAi(bean.getJumlahAi());
            add.setRatioVt(bean.getRatioVt());
            add.setRatioCt(bean.getRatioCt());
            add.setSn(bean.getSn());
            add.setKodeAlatDetail(bean.getKodeAlatDetail());
            add.setKodeAlat(bean.getKodeAlat());
            add.setCreatedDate(bean.getCreatedDate());
            add.setCreatedWho(bean.getCreatedWho());
            add.setLastUpdate(bean.getLastUpdate());
            add.setLastUpdateWho(bean.getLastUpdateWho());
            add.setFlag(bean.getFlag());
            add.setAction(bean.getAction());

            if (save){
                try {
                    if (bean.isAdd() == true){
                        sasControlDao.addAndSave(add);
                    }
                    if (bean.isEdit() == true){
                        sasControlDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveProteksiSetting] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void saveSasPerangkat(SasPerangkat bean) throws GeneralBOException {
        if (bean != null){

            ImApbSasPerangkatEntity add = new ImApbSasPerangkatEntity();
            boolean save = false;

            if (bean.isAdd() == true){
                String id = "";
                id = sasPerangkatDao.getNextId();
                add.setSasPerangkatId("SPR"+id);
                save = true;
            }
            if (bean.isEdit() == true){
                add.setSasPerangkatId(bean.getSasPerangkatId());
                save = true;
            }

            add.setNamaPerangkat(bean.getNamaPerangkat());
            add.setTipePerangkat(bean.getTipePerangkat());
            add.setType(bean.getType());
            add.setMerk(bean.getMerk());
            add.setJumlah(bean.getJumlah());
            add.setSn(bean.getSn());
            add.setKodeAlatDetail(bean.getKodeAlatDetail());
            add.setKodeAlat(bean.getKodeAlat());
            add.setCreatedDate(bean.getCreatedDate());
            add.setCreatedWho(bean.getCreatedWho());
            add.setLastUpdate(bean.getLastUpdate());
            add.setLastUpdateWho(bean.getLastUpdateWho());
            add.setFlag(bean.getFlag());
            add.setAction(bean.getAction());
            add.setKapasitas(bean.getKapasitas());
            add.setTipeport(bean.getTipeport());

            if (save){
                try {
                    if (bean.isAdd() == true){
                        sasPerangkatDao.addAndSave(add);
                    }
                    if (bean.isEdit() == true){
                        sasPerangkatDao.updateAndSave(add);
                    }
                } catch (HibernateException e) {
                    logger.error("[AlatDetailBoImpl.saveProteksiSetting] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public List<TpChanel> getSpecTpChanel(TpChanel bean) throws GeneralBOException {
        List<TpChanel> result = new ArrayList<TpChanel>();

        if (bean != null){
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag","Y");

            List<ImApbTpChanelEntity> imApbTpChanelEntities = null;

            try {
                imApbTpChanelEntities = tpChanelDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecTpChanel] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (imApbTpChanelEntities != null){
                TpChanel dataTp;
                for (ImApbTpChanelEntity listTpChanel : imApbTpChanelEntities){
                    dataTp = new TpChanel();
                    dataTp.setChanelId(listTpChanel.getChanelId());
                    dataTp.setChanel(listTpChanel.getChanel());
                    dataTp.setFungsiChanel(listTpChanel.getFungsiChanel());
                    dataTp.setCreatedDate(listTpChanel.getCreatedDate());
                    dataTp.setCreatedWho(listTpChanel.getCreatedWho());
                    dataTp.setLastUpdate(listTpChanel.getLastUpdate());
                    dataTp.setLastUpdateWho(listTpChanel.getLastUpdateWho());
                    dataTp.setFlag(listTpChanel.getFlag());
                    dataTp.setAction(listTpChanel.getAction());
                    result.add(dataTp);
                }
            }
        }

        return result;
    }



    @Override
    public List<PlcChanel> getSpecPlcChanel(PlcChanel bean) throws GeneralBOException {
        List<PlcChanel> result = new ArrayList<PlcChanel>();

        if (bean != null) {
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag", "Y");

            List<ImApbPlcChanelEntity> imApbPlcChanelEntities = null;

            try {
                imApbPlcChanelEntities = plcChanelDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecTpChanel] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (imApbPlcChanelEntities != null) {
                PlcChanel add;
                for (ImApbPlcChanelEntity listAdd : imApbPlcChanelEntities) {
                    add = new PlcChanel();
                    add.setChanelId(listAdd.getChanelId());
                    add.setChanel(listAdd.getChanel());
                    add.setKodeAlatDetail(listAdd.getKodeAlatDetail());
                    add.setKodeAlat(listAdd.getKodeAlat());
                    add.setFungsiChanel(listAdd.getFungsiChanel());
                    add.setFrekuensi(listAdd.getFrekuensi());
                    add.setFlag(listAdd.getFlag());
                    add.setAction(listAdd.getAction());
                    add.setLastUpdate(listAdd.getLastUpdate());
                    add.setLastUpdateWho(listAdd.getLastUpdateWho());
                    add.setCreatedDate(listAdd.getCreatedDate());
                    add.setCreatedWho(listAdd.getCreatedWho());
                    result.add(add);

                }
            }
        }
        return result;
    }


        public List<SwitchPort> getSpecSwitchPort(SwitchPort bean) throws GeneralBOException {
        List<SwitchPort> result = new ArrayList<SwitchPort>();

        if (bean != null){
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag","Y");

            List<ImApbSwitchPortEntity> imApbSwitchPortEntities = null;

            try {
                imApbSwitchPortEntities = switchPortDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecSwitchPort] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (imApbSwitchPortEntities != null){
                SwitchPort dataSwitch;
                for (ImApbSwitchPortEntity listSwitchPort : imApbSwitchPortEntities){
                    dataSwitch = new SwitchPort();
                    dataSwitch.setSwitchPortId(listSwitchPort.getSwitchPortId());
                    dataSwitch.setPort(listSwitchPort.getPort());
                    dataSwitch.setFungsiPort(listSwitchPort.getFungsiPort());
                    dataSwitch.setCreatedDate(listSwitchPort.getCreatedDate());
                    dataSwitch.setCreatedWho(listSwitchPort.getCreatedWho());
                    dataSwitch.setLastUpdate(listSwitchPort.getLastUpdate());
                    dataSwitch.setLastUpdateWho(listSwitchPort.getLastUpdateWho());
                    dataSwitch.setFlag(listSwitchPort.getFlag());
                    dataSwitch.setAction(listSwitchPort.getAction());
                    dataSwitch.setType(listSwitchPort.getType());
                    result.add(dataSwitch);
                }
            }
        }

        return result;
    }
    @Override
    public List<Battery> getSpecBattery(Battery bean) throws GeneralBOException {
        List<Battery> result = new ArrayList<Battery>();

        if (bean != null) {
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag", "Y");

            List<ImApbBatteryEntity> imApbBatteryEntities = null;

            try {
                imApbBatteryEntities = batteryDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecTpChanel] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (imApbBatteryEntities != null) {
                Battery add;
                for (ImApbBatteryEntity listAdd : imApbBatteryEntities) {
                    add = new Battery();
                    add.setBatteryId(listAdd.getBatteryId());
                    add.setBatteryName(listAdd.getBatteryName());
                    add.setTegangan(listAdd.getTegangan());
                    add.setJumlahCell(listAdd.getJumlahCell());
                    add.setMerk(listAdd.getMerk());
                    add.setType(listAdd.getType());
                    add.setKapasitas(listAdd.getKapasitas());
                    add.setFuse(listAdd.getFuse());
                    add.setFungsi(listAdd.getFungsi());
                    add.setKodeAlatDetail(listAdd.getKodeAlatDetail());
                    add.setKodeAlat(listAdd.getKodeAlat());
                    add.setFlag(listAdd.getFlag());
                    add.setAction(listAdd.getAction());
                    add.setLastUpdate(listAdd.getLastUpdate());
                    add.setLastUpdateWho(listAdd.getLastUpdateWho());
                    add.setCreatedDate(listAdd.getCreatedDate());
                    add.setCreatedWho(listAdd.getCreatedWho());
                    result.add(add);

                }
            }
        }
        return result;
    }
    @Override
    public List<Rectifier> getSpecRectifier(Rectifier bean) throws GeneralBOException {
        List<Rectifier> result = new ArrayList<Rectifier>();

        if (bean != null) {
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag", "Y");

            List<ImApbRectifierEntity> imApbRectifierEntities= null;

            try {
                imApbRectifierEntities = rectifierDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecTpChanel] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (imApbRectifierEntities != null) {
                Rectifier add;
                for (ImApbRectifierEntity listAdd : imApbRectifierEntities) {
                    add = new Rectifier();
                    add.setRectifierId(listAdd.getRectifierId());
                    add.setPanjangAcdb(listAdd.getPanjangAcdb());
                    add.setPanjangBattery(listAdd.getPanjangBattery());
                    add.setPanjangDcdb(listAdd.getPanjangDcdb());
                    add.setTypeAcdb(listAdd.getTypeAcdb());
                    add.setTypeBattery(listAdd.getTypeBattery());
                    add.setTypeDcdb(listAdd.getTypeDcdb());
                    add.setKodeAlatDetail(listAdd.getKodeAlatDetail());
                    add.setKodeAlat(listAdd.getKodeAlat());
                    add.setFlag(listAdd.getFlag());
                    add.setAction(listAdd.getAction());
                    add.setLastUpdate(listAdd.getLastUpdate());
                    add.setLastUpdateWho(listAdd.getLastUpdateWho());
                    add.setCreatedDate(listAdd.getCreatedDate());
                    add.setCreatedWho(listAdd.getCreatedWho());
                    result.add(add);

                }
            }
        }
        return result;
    }

    @Override
    public List<KapasitasMux> getSpecKapasitasMux(KapasitasMux bean) throws GeneralBOException {
        List<KapasitasMux> result = new ArrayList<KapasitasMux>();

        if (bean != null){
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag","Y");

            List<ImApbKapsitasMuxEntity> imApbKapsitasMuxEntities = null;

            try {
                imApbKapsitasMuxEntities = kapasitasMuxDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecTpChanel] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (imApbKapsitasMuxEntities != null){
                KapasitasMux add;
                for (ImApbKapsitasMuxEntity listAdd : imApbKapsitasMuxEntities){
                    add = new KapasitasMux();
                    add.setKapasitasId(listAdd.getKapasitasId());
                    add.setKapasitas(listAdd.getKapasitas());
                    add.setKodeAlatDetail(listAdd.getKodeAlatDetail());
                    add.setKodeAlat(listAdd.getKodeAlat());
                    add.setTimeSlot(listAdd.getTimeSlot());
                    add.setFungsi(listAdd.getFungsi());
                    add.setNamaModul(listAdd.getNamaModul());
                    add.setFlag(listAdd.getFlag());
                    add.setAction(listAdd.getAction());
                    add.setLastUpdate(listAdd.getLastUpdate());
                    add.setLastUpdateWho(listAdd.getLastUpdateWho());
                    add.setCreatedDate(listAdd.getCreatedDate());
                    add.setCreatedWho(listAdd.getCreatedWho());
                    result.add(add);
                }
            }
        }

        return result;
    }

    @Override
    public List<ModulRtu> getSpecModulRtu(ModulRtu bean) throws GeneralBOException {
        List<ModulRtu> result = new ArrayList<ModulRtu>();

        if (bean != null){
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag","Y");

            List<ImApbModulRtuEntity> imApbModulRtuEntities = null;

            try {
                imApbModulRtuEntities = modulRtuDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecTpChanel] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (imApbModulRtuEntities != null){
                ModulRtu add;
                for (ImApbModulRtuEntity listAdd : imApbModulRtuEntities){
                    add = new ModulRtu();
                    add.setModulId(listAdd.getModulId());
                    add.setModul(listAdd.getModul());
                    add.setKodeAlatDetail(listAdd.getKodeAlatDetail());
                    add.setKodeAlat(listAdd.getKodeAlat());
                    add.setQty(listAdd.getQty());
                    add.setJumlahPoint(listAdd.getJumlahPoint());
                    add.setFlag(listAdd.getFlag());
                    add.setAction(listAdd.getAction());
                    add.setLastUpdate(listAdd.getLastUpdate());
                    add.setLastUpdateWho(listAdd.getLastUpdateWho());
                    add.setCreatedDate(listAdd.getCreatedDate());
                    add.setCreatedWho(listAdd.getCreatedWho());
                    result.add(add);
                }
            }
        }

        return result;    }

    @Override
    public List<BayRtu> getSpecBayRtu(BayRtu bean) throws GeneralBOException {
        List<BayRtu> result = new ArrayList<BayRtu>();

        if (bean != null){
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag","Y");

            List<ImApbBayRtuEntity> imApbBayRtuEntities = null;

            try {
                imApbBayRtuEntities = bayRtuDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecTpChanel] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (imApbBayRtuEntities != null){
                BayRtu add;
                for (ImApbBayRtuEntity listAdd : imApbBayRtuEntities){
                    add = new BayRtu();
                    add.setBayId(listAdd.getBayId());
                    add.setNamaBay(listAdd.getNamaBay());
                    add.setJumlahDi(listAdd.getJumlahDi());
                    add.setJumlahDo(listAdd.getJumlahDo());
                    add.setJumlahAo(listAdd.getJumlahAo());
                    add.setJumlahAi(listAdd.getJumlahAi());
                    add.setKodeAlatDetail(listAdd.getKodeAlatDetail());
                    add.setKodeAlat(listAdd.getKodeAlat());
                    add.setMerk(listAdd.getMerk());
                    add.setRatioVt(listAdd.getRatioVt());
                    add.setRatioCt(listAdd.getRatioCt());
                    add.setFlag(listAdd.getFlag());
                    add.setAction(listAdd.getAction());
                    add.setLastUpdate(listAdd.getLastUpdate());
                    add.setLastUpdateWho(listAdd.getLastUpdateWho());
                    add.setCreatedDate(listAdd.getCreatedDate());
                    add.setCreatedWho(listAdd.getCreatedWho());
                    result.add(add);
                }
            }
        }
        return result;
    }

    @Override
    public List<ProteksiSetting> getSpecProteksi(ProteksiSetting bean) throws GeneralBOException {
        List<ProteksiSetting> result = new ArrayList<ProteksiSetting>();

        if (bean != null){
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag","Y");

            List<ImApbProteksiSettingEntity> proteksiSettingEntities = null;

            try {
                proteksiSettingEntities = proteksiSettingDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecProteksi] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (proteksiSettingEntities != null){
                ProteksiSetting add;
                for (ImApbProteksiSettingEntity listAdd : proteksiSettingEntities){
                    add = new ProteksiSetting();
                    add.setProteksiSettingId(listAdd.getProteksiSettingId());
                    add.setTahap(listAdd.getTahap());
                    add.setWaktu(listAdd.getWaktu());
                    add.setArus(listAdd.getArus());
                    add.setTarget(listAdd.getTarget());
                    add.setKodeAlatDetail(listAdd.getKodeAlatDetail());
                    add.setKodeAlat(listAdd.getKodeAlat());
                    add.setFlag(listAdd.getFlag());
                    add.setAction(listAdd.getAction());
                    add.setLastUpdate(listAdd.getLastUpdate());
                    add.setLastUpdateWho(listAdd.getLastUpdateWho());
                    add.setCreatedDate(listAdd.getCreatedDate());
                    add.setCreatedWho(listAdd.getCreatedWho());
                    result.add(add);
                }
            }
        }
        return result;
    }

    @Override
    public List<SasStation> getSpecSasStation(SasStation bean) throws GeneralBOException {
        List<SasStation> result = new ArrayList<SasStation>();

        if (bean != null){
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag","Y");

            List<ImApbSasStationEntity> sasStationEntities = null;

            try {
                sasStationEntities = sasStationDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecSasStation] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (sasStationEntities != null){
                SasStation add;
                for (ImApbSasStationEntity listAdd : sasStationEntities){
                    add = new SasStation();
                    add.setSasStationId(listAdd.getSasStationId());
                    add.setNamaSatation(listAdd.getNamaStation());
                    add.setNamaSoftWare(listAdd.getNamaSoftware());
                    add.setTipeStation(listAdd.getTipeStation());
                    add.setMerk(listAdd.getMerk());
                    add.setTipe(listAdd.getTipe());
                    add.setKodeAlatDetail(listAdd.getKodeAlatDetail());
                    add.setJumlah(listAdd.getJumlah());
                    add.setVersiSoftware(listAdd.getVersiSoftware());
                    add.setSn(listAdd.getSn());
                    add.setKodeAlat(listAdd.getKodeAlat());
                    add.setFlag(listAdd.getFlag());
                    add.setAction(listAdd.getAction());
                    add.setLastUpdate(listAdd.getLastUpdate());
                    add.setLastUpdateWho(listAdd.getLastUpdateWho());
                    add.setCreatedDate(listAdd.getCreatedDate());
                    add.setCreatedWho(listAdd.getCreatedWho());
                    result.add(add);
                }
            }
        }
        return result;
    }

    @Override
    public List<SasControl> getSpecSasControl(SasControl bean) throws GeneralBOException {
        List<SasControl> result = new ArrayList<SasControl>();

        if (bean != null){
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag","Y");

            List<ImApbSasControlEntity> sasControlEntities = null;

            try {
                sasControlEntities = sasControlDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecSasControl] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (sasControlEntities != null){
                SasControl add;
                for (ImApbSasControlEntity listAdd : sasControlEntities){
                    add = new SasControl();
                    add.setSasControlId(listAdd.getSasControlId());
                    add.setNamaControl(listAdd.getNamaControl());
                    add.setNamaSoftWare(listAdd.getNamaSoftware());
                    add.setVersiSoftware(listAdd.getVersiSoftware());
                    add.setMerk(listAdd.getMerk());
                    add.setTipe(listAdd.getTipe());
                    add.setTipeControl(listAdd.getTipeControl());
                    add.setJumlahDi(listAdd.getJumlahDi());
                    add.setJumlahDo(listAdd.getJumlahDo());
                    add.setJumlahAi(listAdd.getJumlahAi());
                    add.setRatioVt(listAdd.getRatioVt());
                    add.setRatioCt(listAdd.getRatioCt());
                    add.setSn(listAdd.getSn());
                    add.setKodeAlatDetail(listAdd.getKodeAlatDetail());
                    add.setKodeAlat(listAdd.getKodeAlat());
                    add.setFlag(listAdd.getFlag());
                    add.setAction(listAdd.getAction());
                    add.setLastUpdate(listAdd.getLastUpdate());
                    add.setLastUpdateWho(listAdd.getLastUpdateWho());
                    add.setCreatedDate(listAdd.getCreatedDate());
                    add.setCreatedWho(listAdd.getCreatedWho());
                    result.add(add);
                }
            }
        }
        return result;
    }

    @Override
    public List<SasPerangkat> getSpecSasPerangkat(SasPerangkat bean) throws GeneralBOException {
        List<SasPerangkat> result = new ArrayList<SasPerangkat>();

        if (bean != null){
            Map hsCriteria = new HashMap();
            hsCriteria.put("kode_alat_detail", bean.getKodeAlatDetail());
            hsCriteria.put("flag","Y");

            List<ImApbSasPerangkatEntity> sasPerangkatEntities = null;

            try {
                sasPerangkatEntities = sasPerangkatDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatDetailBoImpl.getSpecSasPerangkat] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (sasPerangkatEntities != null){
                SasPerangkat add;
                for (ImApbSasPerangkatEntity listAdd : sasPerangkatEntities){
                    add = new SasPerangkat();
                    add.setSasPerangkatId(listAdd.getSasPerangkatId());
                    add.setNamaPerangkat(listAdd.getNamaPerangkat());
                    add.setTipePerangkat(listAdd.getTipePerangkat());
                    add.setType(listAdd.getType());
                    add.setMerk(listAdd.getMerk());
                    add.setJumlah(listAdd.getJumlah());
                    add.setSn(listAdd.getSn());
                    add.setKodeAlatDetail(listAdd.getKodeAlatDetail());
                    add.setKodeAlat(listAdd.getKodeAlat());
                    add.setFlag(listAdd.getFlag());
                    add.setAction(listAdd.getAction());
                    add.setLastUpdate(listAdd.getLastUpdate());
                    add.setLastUpdateWho(listAdd.getLastUpdateWho());
                    add.setCreatedDate(listAdd.getCreatedDate());
                    add.setCreatedWho(listAdd.getCreatedWho());
                    result.add(add);
                }
            }
        }
        return result;
    }
}
