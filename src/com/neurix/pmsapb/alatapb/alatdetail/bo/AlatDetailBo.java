package com.neurix.pmsapb.alatapb.alatdetail.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetail;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetailLog;
import com.neurix.pmsapb.alatapb.alatdetail.model.TpChanel;
import com.neurix.pmsapb.alatapb.alatdetail.model.spek.*;
import org.hibernate.HibernateException;

import java.util.List;

/**
 * Created by thinkpad on 06/09/2017.
 */
public interface AlatDetailBo extends BaseMasterBo<AlatDetail> {
    public String getSeqAlatDetailId() throws GeneralBOException;
    public List<AlatDetail> getComboAlatByCriteria(String query) throws GeneralBOException;
    public List<AlatDetailLog> getLogByCriteria(AlatDetailLog searchBean) throws GeneralBOException;

    //save spesifikasi masing - masing alat
    public void saveTpChanel(TpChanel tpChanel) throws GeneralBOException;
    public void savePlcChanel(PlcChanel bean) throws GeneralBOException;
    public void saveSwitchPort(SwitchPort bean) throws GeneralBOException;
    public void saveKapasitasMux(KapasitasMux bean) throws GeneralBOException;
    public void saveModulRtu(ModulRtu bean) throws GeneralBOException;
    public void saveBayRtu(BayRtu bean) throws GeneralBOException;
    public void saveProteksiSetting(ProteksiSetting bean) throws GeneralBOException;
    public void saveBattery(Battery battery) throws GeneralBOException;
    public void saveSasStation(SasStation bean) throws GeneralBOException;
    public void saveSasControl(SasControl bean) throws GeneralBOException;
    public void saveSasPerangkat(SasPerangkat bean) throws GeneralBOException;
    public void saveRectifier (Rectifier rectifier) throws GeneralBOException;


    //get data spesifikasi masing - masing alat
    public List<TpChanel> getSpecTpChanel(TpChanel bean) throws GeneralBOException;
    public List<PlcChanel> getSpecPlcChanel(PlcChanel bean) throws GeneralBOException;
    public List<KapasitasMux> getSpecKapasitasMux(KapasitasMux bean) throws GeneralBOException;
    public List<ModulRtu> getSpecModulRtu(ModulRtu bean) throws GeneralBOException;
    public List<BayRtu> getSpecBayRtu(BayRtu bean) throws GeneralBOException;
    public List<SwitchPort> getSpecSwitchPort(SwitchPort bean) throws GeneralBOException;
    public List<ProteksiSetting> getSpecProteksi(ProteksiSetting bean) throws GeneralBOException;
    public List<Battery> getSpecBattery(Battery bean) throws GeneralBOException;
    public List<SasStation> getSpecSasStation(SasStation bean) throws GeneralBOException;
    public List<SasControl> getSpecSasControl(SasControl bean) throws GeneralBOException;
    public List<SasPerangkat> getSpecSasPerangkat(SasPerangkat bean) throws GeneralBOException;
    public List<Rectifier> getSpecRectifier (Rectifier bean) throws GeneralBOException;
    public List<AlatDetail> getByAlat (AlatDetail searchBean) throws GeneralBOException;
}
