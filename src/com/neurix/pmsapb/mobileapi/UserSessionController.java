package com.neurix.pmsapb.mobileapi;

import com.neurix.authorization.user.bo.UserBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.mobileapi.model.Task;
import com.neurix.pmsapb.mobileapi.model.UserSession;
import com.neurix.pmsapb.project.bo.ProjectBo;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import org.hibernate.HibernateException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by thinkpad on 15/05/2018.
 */
public class UserSessionController implements ModelDriven<Object> {
    protected static transient Logger logger = Logger.getLogger(String.valueOf(UserSessionController.class));

    private UserSession model = new UserSession();
    private String idMember;
    private String id;
    private String idTaskMember;
    private String deadline;
    private String startDate;
    private String progres;
    private String note;
    private String idTask;

    private Collection<UserSession> listOfSession = new ArrayList<UserSession>();

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    private Collection<Task> listOfListTask = new ArrayList<Task>();

    public String getIdTaskMember() {
        return idTaskMember;
    }

    public void setIdTaskMember(String idTaskMember) {
        this.idTaskMember = idTaskMember;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getProgres() {
        return progres;
    }

    public void setProgres(String progres) {
        this.progres = progres;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        UserSessionController.logger = logger;
    }


    public HttpHeaders index(){
        logger.info("[ListTaskController.index] Statt process GET /User Sesion <<<");

        HttpServletRequest request = ServletActionContext.getRequest();
        String token = request.getHeader("tokenId");

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        UserBo userBo = (UserBo) ctx.getBean("userBoProxy");


        boolean isActive = false;

        try {
            isActive = userBo.isActiveUserSessionLog(token);
        } catch (HibernateException e) {
//            logger.error("[ExceptionHandlerInterceptor.checkingSession] Error while checking session log." ,e );
            throw new GeneralBOException(e);
        }


        UserSession session = new UserSession();

        if (isActive){
            session.setStatus("active");
        } else {
            session.setStatus("nonactive");
        }

        logger.info("[ListTaskController.index] Statt process GET /User Sesion <<<");

        listOfSession.add(session);
        return new DefaultHttpHeaders("index").disableCaching();

    }


    public String getIdMember() {
        return idMember;
    }

    public void setIdMember(String idMember) {
        this.idMember = idMember;
    }

    public void setModel(UserSession model) {
        this.model = model;
    }

    @Override
    public Object getModel() {
//        return model;
        return (listOfSession != null ? listOfSession : model);
    }
}
