package com.neurix.pmsapb.mobileapi;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.mobileapi.model.CatatanPemeliharaan;
import com.neurix.pmsapb.alatapb.pemeliharaan.bo.PemeliharaanBo;
import com.neurix.pmsapb.alatapb.pemeliharaan.model.Pemeliharaan;
import com.opensymphony.xwork2.ModelDriven;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.logging.Logger;

/**
 * Created by thinkpad on 15/05/2018.
 */
public class AddCatatanPemeliharaanController implements ModelDriven<Object> {
    protected static transient Logger logger = Logger.getLogger(String.valueOf(AddCatatanPemeliharaanController.class));
    private CatatanPemeliharaan model = new CatatanPemeliharaan();
    private PemeliharaanBo pemeliharaanBoProxy;
    private Collection<CatatanPemeliharaan> listOfCatatanPemeliharaan = new ArrayList<CatatanPemeliharaan>();
    private String idPemeliharaan;
    public String idAlatDetail;
    public String catatan;
    public String createdDate;
    public String createdWho;
    public String noSap;

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public String getCreatedWho() {
        return createdWho;
    }

    public void setCreatedWho(String createdWho) {
        this.createdWho = createdWho;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getNoSap() {
        return noSap;
    }

    public void setNoSap(String noSap) {
        this.noSap = noSap;
    }

    public String getIdPemeliharaan() {
        return idPemeliharaan;
    }

    public void setIdPemeliharaan(String idPemeliharaan) {
        this.idPemeliharaan = idPemeliharaan;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        AddCatatanPemeliharaanController.logger = logger;
    }

    public void setModel(CatatanPemeliharaan model) {
        this.model = model;
    }

    public PemeliharaanBo getPemeliharaanBoProxy() {
        return pemeliharaanBoProxy;
    }

    public void setPemeliharaanBoProxy(PemeliharaanBo pemeliharaanBoProxy) {
        this.pemeliharaanBoProxy = pemeliharaanBoProxy;
    }

    public Collection<CatatanPemeliharaan> getListOfListTask() {
        return listOfCatatanPemeliharaan;
    }

    public void setListOfListTask(Collection<CatatanPemeliharaan> listOfListTask) {
        this.listOfCatatanPemeliharaan = listOfListTask;
    }

    public String create(){
        logger.info("[AddCatatanPemeliharaanController.create] Start process POST /List Task <<<");

        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();

        PemeliharaanBo pemeliharaanBo = (PemeliharaanBo) ctx.getBean("pemeliharaanBoProxy");

        try{
            Pemeliharaan entryCatatan = new Pemeliharaan();

            entryCatatan.setIdPemeliharaan(idPemeliharaan);
            entryCatatan.setIdAlatDetail(idAlatDetail);
            entryCatatan.setCatatan(catatan);
            entryCatatan.setCreatedDate(updateTime);
            entryCatatan.setCreatedWho(createdWho);
            entryCatatan.setNoSap(noSap);
            entryCatatan.setFlag("Y");
            entryCatatan.setAction("C");

            pemeliharaanBo.saveAdd(entryCatatan);

        }  catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = pemeliharaanBo.saveErrorMessage(e.getMessage(), "pemeliharaanAction.saveIssue");
            } catch (GeneralBOException e1) {
//                logger.error("[projectAction.saveIssue] Error when saving error,", e1);
            }
//            logger.error("[projectAction.saveIssue] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
//            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
//            return ERROR;
        }
        logger.info("[AddCatatanPemeliharaanController.create] End process POST /List Task <<<");
        return "success";
    }

    @Override
    public Object getModel() {
//        return model;
        return (listOfCatatanPemeliharaan != null ? listOfCatatanPemeliharaan : model);
    }
}
