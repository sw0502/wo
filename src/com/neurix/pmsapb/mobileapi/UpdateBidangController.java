package com.neurix.pmsapb.mobileapi;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.master.bidang.model.Bidang;
import com.neurix.pmsapb.mobileapi.model.BidangMobile;
import com.neurix.pmsapb.mobileapi.model.Task;
import com.neurix.pmsapb.master.bidang.bo.BidangBo;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by thinkpad on 15/05/2018.
 */
public class UpdateBidangController implements ModelDriven<Object> {
    protected static transient Logger logger = Logger.getLogger(String.valueOf(UpdateBidangController.class));

    private BidangMobile model = new BidangMobile();
    private Collection<BidangMobile> listOfBidang = new ArrayList<BidangMobile>();
    private BidangBo bidangBoProxy;
    private String idBidang;
    private String namaBidang;
    private String flag;
    private String Action;

    public BidangBo getBidangBoProxy() {
        return bidangBoProxy;
    }

    public void setBidangBoProxy(BidangBo bidangBoProxy) {
        this.bidangBoProxy = bidangBoProxy;
    }

    public String getIdBidang() {
        return idBidang;
    }

    public void setIdBidang(String idBidang) {
        this.idBidang = idBidang;
    }

    public String getNamaBidang() {
        return namaBidang;
    }

    public void setNamaBidang(String namaBidang) {
        this.namaBidang = namaBidang;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        UpdateBidangController.logger = logger;
    }

    //    POST /listtask/{id}
    public String create(){
        logger.info("[ListTaskController.update] start process PUT /taskasmud/{id} <<<");

        if (idBidang != null){

            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            Date utilDate = new Date();
            java.sql.Date today = new java.sql.Date(utilDate.getTime());

            com.neurix.pmsapb.master.bidang.model.Bidang entryBidang = new com.neurix.pmsapb.master.bidang.model.Bidang();

            entryBidang.setIdBidang(getIdBidang());
            entryBidang.setNamaBidang(getNamaBidang());
            entryBidang.setFlag("Y");
            entryBidang.setAction("U");


            try {
                bidangBoProxy.saveEdit(entryBidang);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = bidangBoProxy.saveErrorMessage(e.getMessage(), "EvalusiLahanAction.saveBatalAsmudSurvey");
                } catch (GeneralBOException e1) {
//                    logger.error("[EvalusiLahanAction.saveAsmudSurvey] Error when saving error,", e1);
                }
//                logger.error("[EvalusiLahanAction.saveAsmudSurvey] Error when saving batal asmud survey lahan," + "[" + logId + "] Found problem when saving batal asmud survey data, please inform to your admin.", e);
                throw new GeneralBOException(e);
            }
        }

        logger.info("[ListBidangController.update] end process PUT /taskasmud/{id} <<<");
        return "success";
    }


    public void setModel(BidangMobile model) {
        this.model = model;
    }

    public Collection<BidangMobile> getListOfBidang() {
        return listOfBidang;
    }

    public void setListOfBidang(Collection<BidangMobile> listOfBidang) {
        this.listOfBidang = listOfBidang;
    }

    @Override
    public Object getModel() {
//        return model;
        return (listOfBidang != null ? listOfBidang : model);
    }
}
