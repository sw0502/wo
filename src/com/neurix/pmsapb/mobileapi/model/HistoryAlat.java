package com.neurix.pmsapb.mobileapi.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by lenovo on 5/15/2018.
 */
public class HistoryAlat implements Serializable {
    private String idLogAlatDetail;
    private String idAlatDetail;
    private String kodeAlat;
    private String kodeApp;
    private String noGi;
    private String namaGi;
    private String noGiTujuan;
    private String namaGiTujuan;
    private String namaAlatDetail;
    private String merk;
    private String typeId;
    private String noSeries;
    private String kdStatus;
    private String tglOprs;
    private String tglPasang;
    private String flag;
    private String action;
    private String lastUpdate;
    private String lastUpdateWho;
    private String note;
    private String urlFoto;
    private String status;

    public String getIdLogAlatDetail() {
        return idLogAlatDetail;
    }

    public void setIdLogAlatDetail(String idLogAlatDetail) {
        this.idLogAlatDetail = idLogAlatDetail;
    }

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getKodeApp() {
        return kodeApp;
    }

    public void setKodeApp(String kodeApp) {
        this.kodeApp = kodeApp;
    }

    public String getNoGi() {
        return noGi;
    }

    public void setNoGi(String noGi) {
        this.noGi = noGi;
    }

    public String getNamaGi() {
        return namaGi;
    }

    public void setNamaGi(String namaGi) {
        this.namaGi = namaGi;
    }

    public String getNoGiTujuan() {
        return noGiTujuan;
    }

    public void setNoGiTujuan(String noGiTujuan) {
        this.noGiTujuan = noGiTujuan;
    }

    public String getNamaGiTujuan() {
        return namaGiTujuan;
    }

    public void setNamaGiTujuan(String namaGiTujuan) {
        this.namaGiTujuan = namaGiTujuan;
    }

    public String getNamaAlatDetail() {
        return namaAlatDetail;
    }

    public void setNamaAlatDetail(String namaAlatDetail) {
        this.namaAlatDetail = namaAlatDetail;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getNoSeries() {
        return noSeries;
    }

    public void setNoSeries(String noSeries) {
        this.noSeries = noSeries;
    }

    public String getKdStatus() {
        return kdStatus;
    }

    public void setKdStatus(String kdStatus) {
        this.kdStatus = kdStatus;
    }

    public String getTglPasang() {
        return tglPasang;
    }

    public void setTglPasang(String tglPasang) {
        this.tglPasang = tglPasang;
    }

    public String getTglOprs() {
        return tglOprs;
    }

    public void setTglOprs(String tglOprs) {
        this.tglOprs = tglOprs;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
