package com.neurix.pmsapb.mobileapi.model;

import java.io.Serializable;

/**
 * Created by thinkpad on 16/05/2018.
 */
public class UserSession implements Serializable {

    private String userId;
    private String userName;
    private String status;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
