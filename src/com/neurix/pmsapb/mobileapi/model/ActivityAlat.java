package com.neurix.pmsapb.mobileapi.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by lenovo on 5/15/2018.
 */
public class ActivityAlat implements Serializable {
    private String idAlatDetail;
    private String noSap;
    private String kodeAlat;
    private String merk;
    private String noSeries;
    private String namaAlatDetail;
    private String kodeApp;
    private String namaApp;
    private String noGi;
    private String namaGi;
    private String noGiTujuan;
    private String namaGiTujuan;
    private String urlFoto;

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }

    public String getNoSap() {
        return noSap;
    }

    public void setNoSap(String noSap) {
        this.noSap = noSap;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getNoSeries() {
        return noSeries;
    }

    public void setNoSeries(String noSeries) {
        this.noSeries = noSeries;
    }

    public String getNamaAlatDetail() {
        return namaAlatDetail;
    }

    public void setNamaAlatDetail(String namaAlatDetail) {
        this.namaAlatDetail = namaAlatDetail;
    }

    public String getKodeApp() {
        return kodeApp;
    }

    public void setKodeApp(String kodeApp) {
        this.kodeApp = kodeApp;
    }

    public String getNamaApp() {
        return namaApp;
    }

    public void setNamaApp(String namaApp) {
        this.namaApp = namaApp;
    }

    public String getNoGi() {
        return noGi;
    }

    public void setNoGi(String noGi) {
        this.noGi = noGi;
    }

    public String getNamaGi() {
        return namaGi;
    }

    public void setNamaGi(String namaGi) {
        this.namaGi = namaGi;
    }

    public String getNoGiTujuan() {
        return noGiTujuan;
    }

    public void setNoGiTujuan(String noGiTujuan) {
        this.noGiTujuan = noGiTujuan;
    }

    public String getNamaGiTujuan() {
        return namaGiTujuan;
    }

    public void setNamaGiTujuan(String namaGiTujuan) {
        this.namaGiTujuan = namaGiTujuan;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }
}
