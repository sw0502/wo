package com.neurix.pmsapb.mobileapi.model;

import java.io.Serializable;

/**
 * Created by thinkpad on 16/05/2018.
 */
public class BidangMobile implements Serializable {

    private String idBidang;
    private String namaBidang;

    public String getIdBidang() {
        return idBidang;
    }

    public void setIdBidang(String idBidang) {
        this.idBidang = idBidang;
    }

    public String getNamaBidang() {
        return namaBidang;
    }

    public void setNamaBidang(String namaBidang) {
        this.namaBidang = namaBidang;
    }
}
