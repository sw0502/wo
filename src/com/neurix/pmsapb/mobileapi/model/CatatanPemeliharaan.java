package com.neurix.pmsapb.mobileapi.model;

import java.io.File;
import java.io.Serializable;

/**
 * Created by lenovo on 5/15/2018.
 */
public class CatatanPemeliharaan implements Serializable {
    public String idPemeliharaan;
    public String idAlatDetail;
    public String catatan;
    public String createdDate;
    public String createdWho;
    public String noSap;
    public String flag;
    public String action;

    public String getIdPemeliharaan() {
        return idPemeliharaan;
    }

    public void setIdPemeliharaan(String idPemeliharaan) {
        this.idPemeliharaan = idPemeliharaan;
    }

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedWho() {
        return createdWho;
    }

    public void setCreatedWho(String createdWho) {
        this.createdWho = createdWho;
    }

    public String getNoSap() {
        return noSap;
    }

    public void setNoSap(String noSap) {
        this.noSap = noSap;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
