package com.neurix.pmsapb.mobileapi;

/**
 * Created by lenovo on 5/14/2018.
 */
import java.util.Map;
import java.util.logging.Logger;

import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import com.opensymphony.xwork2.ModelDriven;
public class EmployeeController implements ModelDriven<Object>{
    private static final long serialVersionUID = 1L;
      //  protected static transient Logger logger = Logger.getLogger(EmployeeController.class);
    //protected static transient Logger logger = Logger.getLogger(String.valueOf(EmployeeController.class));
  //  logger.info("[ActivityPetaniController.create] start process POST /activity >>>");
    private String id;
    private Object model;
    private EmployeeRepository employeeRepository = new EmployeeRepository();
    private static Map<String,Employee> map;
    {
      //  logger.info("[ExceptionHandlerInterceptor.checkingApi] skiping token");
        map = employeeRepository.findAllEmployee();
    }
    public HttpHeaders index() {
        model = map;

        return new DefaultHttpHeaders("index").disableCaching();
    }
    public String add(){
        Integer empId = Integer.parseInt(id);
        Employee emp = new Employee(empId,"Ramesh", "PQR");
        model = emp;
        return "SUCCESS";
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        model = employeeRepository.getemployeeById(id);
        this.id = id;
    }
    @Override
    public Object getModel() {
        return model;
    }
}