package com.neurix.pmsapb.mobileapi;

import com.neurix.common.constant.CommonConstant;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.doc.model.Document;
import com.neurix.pmsapb.mobileapi.model.Task;
import com.neurix.pmsapb.project.bo.ProjectBo;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by thinkpad on 15/05/2018.
 */
public class UploadDocTaskController implements ModelDriven<Object> {
    protected static transient Logger logger = Logger.getLogger(String.valueOf(UploadDocTaskController.class));

    private Task model = new Task();
    private ProjectBo projectBoProxy;
    private String id;
    private String idProject;
    private String idTask;
    private String uploadPerson;

    private File fileUploadDoc;
    private String fileUploadDocContentType;
    private String fileUploadDocFileName;
    private Collection<Task> listOfListTask = new ArrayList<Task>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public File getFileUploadDoc() {
        return fileUploadDoc;
    }

    public void setFileUploadDoc(File fileUploadDoc) {
        this.fileUploadDoc = fileUploadDoc;
    }

    public String getFileUploadDocContentType() {
        return fileUploadDocContentType;
    }

    public void setFileUploadDocContentType(String fileUploadDocContentType) {
        this.fileUploadDocContentType = fileUploadDocContentType;
    }

    public String getFileUploadDocFileName() {
        return fileUploadDocFileName;
    }

    public void setFileUploadDocFileName(String fileUploadDocFileName) {
        this.fileUploadDocFileName = fileUploadDocFileName;
    }

    public void setProjectBoProxy(ProjectBo projectBoProxy) {
        this.projectBoProxy = projectBoProxy;
    }


    public String create(){
        logger.info("[UploadDocTaskController.create] Start process POST /List Task <<<");

        String idProject = getIdProject();
        String idTask = getIdTask();
        String userLogin = getUploadPerson();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

//        String idProject = dataProject.getIdProject();
        if(idProject !=null && idTask != null){
            Document addDoc = new Document();
            addDoc.setLastUpdate(updateTime);
            addDoc.setLastUpdateWho(userLogin);
            addDoc.setUploadBy(userLogin);
            ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
            ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
            String idDoc = null;
            try{
                idDoc = "DOC"+projectBo.getSeqDocId();
                File fileToCreate = null;
                //note : for windows directory
                String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                if (this.fileUploadDoc != null) {
                    if (this.fileUploadDoc.length() > 0 && this.fileUploadDoc.length() <= 5242880) {
                        if ("image/jpeg".equalsIgnoreCase(this.fileUploadDocContentType)) {
                            String fileDocName = "IMG_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");
                        }
                        else if ("application/pdf".equalsIgnoreCase(this.fileUploadDocContentType)){
                            String filePathPdf = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                            String fileDocName = "PDF_" +idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePathPdf, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");
                        }
                        else {
                            String fileDocName = "FLE_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("file");
                        }
                    } else {
//                        logger.error("[projectAction.saveProject] Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
//                        addActionError("Error,  Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
//                        return ERROR;
                    }
                }
                addDoc.setIdProject(idProject);
                addDoc.setIdTask(idTask);
                try{
                    projectBoProxy.saveDocument(addDoc);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = projectBo.saveErrorMessage(e.getMessage(), "projectBo.saveDocument");
                    } catch (GeneralBOException e1) {
//                        logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                    }
//                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
                }

            }catch (GeneralBOException e ){
                Long logId = null;
                try {
                    logId = projectBo.saveErrorMessage(e.getMessage(), "projectBo.saveDocument");
                } catch (GeneralBOException e1) {
//                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                }
//                logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
            }
        }
        logger.info("[UploadDocTaskController.create] End process POST /List Task <<<");
        return "success";
    }


    public String getUploadPerson() {
        return uploadPerson;
    }

    public void setUploadPerson(String uploadPerson) {
        this.uploadPerson = uploadPerson;
    }

    public void setModel(Task model) {
        this.model = model;
    }

    @Override
    public Object getModel() {
//        return model;
        return (listOfListTask != null ? listOfListTask : model);
    }
}
