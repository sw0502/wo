package com.neurix.pmsapb.mobileapi;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.alatapb.alatdetail.bo.AlatDetailBo;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetail;
import com.neurix.pmsapb.mobileapi.model.ActivityAlat;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by lenovo on 5/15/2018.
 */
public class ActivityAlatController implements ModelDriven<Object> {
    protected static transient Logger logger = Logger.getLogger(String.valueOf(ActivityAlatController.class));
    private AlatDetailBo alatDetailBoProxy;
    private Collection<ActivityAlat> listOfAlat = new ArrayList<ActivityAlat>();
    private ActivityAlat Alat = new ActivityAlat();
    private String query;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        ActivityAlatController.logger = logger;
    }

    public AlatDetailBo getAlatDetailBoProxy() {
        return alatDetailBoProxy;
    }

    public void setAlatDetailBoProxy(AlatDetailBo alatDetailBoProxy) {
        this.alatDetailBoProxy = alatDetailBoProxy;
    }

    public void setListOfAlat(Collection<ActivityAlat> listOfAlat) {
        this.listOfAlat = listOfAlat;
    }

    public ActivityAlat getAlat() {
        return Alat;
    }

    public void setAlat(ActivityAlat alat) {
        Alat = alat;
    }

    public HttpHeaders index() {
        List<com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetail> listAlat = new ArrayList<AlatDetail>();


        AlatDetail alatDetail = new AlatDetail();
        alatDetail.setFlag("Y");

        try {
            listAlat = alatDetailBoProxy.getByAlat(alatDetail);
        } catch (GeneralBOException e) {
            Long logid = null;
            try {
                logid = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "alat controller index");
            } catch (GeneralBOException el) {
//                 logger.error("[DesaController.index] Error when get combo lokasi kebun," + "[" + logId + "] Found problem when retrieving combo lokasi kebun data, please inform to your admin.", e);
            }
        }
        if (listAlat.size() != 0) {
            for (com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetail alatList : listAlat) {
                ActivityAlat alat = new ActivityAlat();
                alat.setIdAlatDetail(alatList.getIdAlatDetail());
                alat.setNoSap(alatList.getNoSap());
                alat.setKodeAlat(alatList.getKodeAlat());
                alat.setMerk(alatList.getMerk());
                alat.setNoSeries(alatList.getNoSeries());
                alat.setNamaAlatDetail(alatList.getNamaAlatDetail());
                alat.setKodeApp(alatList.getKodeApp());
                alat.setNamaApp(alatList.getNamaApp());
                alat.setNoGi(alatDetail.getNoGi());
                alat.setNamaGi(alatList.getNamaGi());
                alat.setNoGiTujuan(alatList.getNoGiTujuan());
                alat.setNamaGiTujuan(alatList.getNamaGiTujuan());
                listOfAlat.add(alat);
            }
        }
        logger.info("[DesaController.index] end process <<<");

        return new DefaultHttpHeaders("index").disableCaching();

    }


    @Override
    public Object getModel() {
        return (listOfAlat != null ? listOfAlat : Alat);


    }

    public String getQuery() {
        return query;
    }

    public Collection<ActivityAlat> getListOfAlat() {
        return listOfAlat;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
