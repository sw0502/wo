package com.neurix.pmsapb.mobileapi;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.mobileapi.model.Task;
import com.neurix.pmsapb.project.bo.ProjectBo;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by thinkpad on 15/05/2018.
 */
public class ListTaskController implements ModelDriven<Object> {
    protected static transient Logger logger = Logger.getLogger(String.valueOf(ListTaskController.class));

    private Task model = new Task();
    private ProjectBo projectBoProxy;
    private String idMember;
    private String id;
    private String idTaskMember;
    private String deadline;
    private String startDate;
    private String progres;
    private String note;
    private Collection<Task> listOfListTask = new ArrayList<Task>();

    public String getIdTaskMember() {
        return idTaskMember;
    }

    public void setIdTaskMember(String idTaskMember) {
        this.idTaskMember = idTaskMember;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getProgres() {
        return progres;
    }

    public void setProgres(String progres) {
        this.progres = progres;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        ListTaskController.logger = logger;
    }

    public void setProjectBoProxy(ProjectBo projectBoProxy) {
        this.projectBoProxy = projectBoProxy;
    }

    public HttpHeaders index(){
        logger.info("[ListTaskController.index] Start process GET /List Task <<<");

        com.neurix.pmsapb.project.model.Task searchTask = new com.neurix.pmsapb.project.model.Task();
        searchTask.setFlag("Y");
        String userLogin = "";
        String userRole = "";

        userLogin = model.getIdMember();
        userRole = "STAFF";

        List<com.neurix.pmsapb.project.model.Task> taskList = new ArrayList<com.neurix.pmsapb.project.model.Task>();
        try {
            taskList = projectBoProxy.getSearchListTask(searchTask, userRole, userLogin);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "projectBo.getSearchMasaTanamByCriteria");
            } catch (GeneralBOException e1) {
//                logger.error("[ListTaskController.search] Error when saving error,", e1);
            }
//            logger.error("[ListTaskController.search] Error when searching masa tanam by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);

            throw new GeneralBOException(e);
        }

        if (taskList.size() != 0){
            for (com.neurix.pmsapb.project.model.Task listTask : taskList){
                Task task = new Task();
                task.setIdTask(listTask.getIdTask());
                task.setIdProject(listTask.getIdProject());
                task.setNamaTask(listTask.getNamaTask());
//                task.setDeadline(listTask.getDeadline());
//            task.setAsignTo(listTask.getAsignTo());
                task.setPriority(listTask.getPriority());
                task.setNote(listTask.getNote());
//            task.setProgres(listTask.getProgres());
//                task.setStartDate(listTask.getStartDate());
//            task.setLastUpdate(listTask.getLastUpdate());
                task.setLastUpdateWho(listTask.getLastUpdateWho());
//            task.setLastStatusUpdate(listTask.getLastStatusUpdate());task.setLastStatusUpdate(listTask.getLastStatusUpdate());task.setLastStatusUpdate(listTask.getLastStatusUpdate());
                task.setIdMember(listTask.getIdMember());
                task.setPosition(listTask.getPosition());
                task.setUserName(listTask.getUserName());
                task.setIsTask(listTask.getIsTask());
                task.setNamaProject(listTask.getNamaProject());
                task.setNoWo(listTask.getNoWo());
                task.setIdTaskMember(listTask.getIdTaskMember());
                listOfListTask.add(task);
            }
        }

        logger.info("[ListTaskController.index] End process GET /List Task <<<");
        return new DefaultHttpHeaders("index").disableCaching();
    }

    public HttpHeaders create(){
        logger.info("[ListTaskController.index] Start process GET /List Task <<<");

        com.neurix.pmsapb.project.model.Task searchTask = new com.neurix.pmsapb.project.model.Task();
        searchTask.setFlag("Y");
        String userLogin = "";
        String userRole = "";

        userLogin = idMember;
        userRole = "STAFF";

        List<com.neurix.pmsapb.project.model.Task> taskList = new ArrayList<com.neurix.pmsapb.project.model.Task>();
        try {
            taskList = projectBoProxy.getSearchListTask(searchTask, userRole, userLogin);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "projectBo.getSearchMasaTanamByCriteria");
            } catch (GeneralBOException e1) {
//                logger.error("[ListTaskController.search] Error when saving error,", e1);
            }
//            logger.error("[ListTaskController.search] Error when searching masa tanam by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);

            throw new GeneralBOException(e);
        }

        if (taskList.size() != 0){
            for (com.neurix.pmsapb.project.model.Task listTask : taskList){
                Task task = new Task();
                task.setIdTask(listTask.getIdTask());
                task.setIdProject(listTask.getIdProject());
                task.setNamaTask(listTask.getNamaTask());
                task.setPriority(listTask.getPriority());
                task.setNote(listTask.getNote());
                task.setLastUpdateWho(listTask.getLastUpdateWho());
                task.setIdMember(listTask.getIdMember());
                task.setPosition(listTask.getPosition());
                task.setUserName(listTask.getUserName());
                task.setIsTask(listTask.getIsTask());
                task.setNamaProject(listTask.getNamaProject());
                task.setNoWo(listTask.getNoWo());
                task.setIdTaskMember(listTask.getIdTaskMember());

                // Create an instance of SimpleDateFormat used for formatting
                // the string representation of date (month/day/year)
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

                // Get the date today using Calendar object.
                Date sd = listTask.getStartDate();
                Date dl = listTask.getDeadline();

                // Using DateFormat format method we can create a string
                // representation of a date with the defined format.
                String stStartdate = df.format(sd);
                String stDeadline = df.format(dl);

                String stProgress = String.valueOf(listTask.getProgres());

                task.setStartDate(stStartdate);
                task.setDeadline(stDeadline);
                task.setProgres(stProgress);

                listOfListTask.add(task);
            }
        }

        logger.info("[ListTaskController.index] End process GET /List Task <<<");
        return new DefaultHttpHeaders("index").disableCaching();
    }

//    PUT /listtask/{id}
    public String update(){
        logger.info("[ListTaskController.update] start process PUT /taskasmud/{id} <<<");

        if (id != null){

//            String userLogin = CommonUtil.userLogin();
//            String userLoginId = CommonUtil.userIdLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date today = new java.sql.Date(utilDate.getTime());

            com.neurix.pmsapb.project.model.Task entryTask = new com.neurix.pmsapb.project.model.Task();

            int intProgres = 0;
            if (progres != null){
                String stProgres = progres;
                intProgres = Integer.parseInt(stProgres);
            }

            entryTask.setIdTaskMember(getIdTaskMember());
            entryTask.setIdTask(id);
            entryTask.setIdMember(idMember);
            entryTask.setProgres(intProgres);
            entryTask.setFlag("Y");
            entryTask.setAction("U");
            entryTask.setNote(note);

//            java.sql.Date sd = CommonUtil.convertToDate(startDate);
//            java.sql.Date dl = CommonUtil.convertToDate(deadline);

            String s = startDate;
            String d = deadline;

            String stDay = s.substring(0,2);
            String stMonth = s.substring(3,5);
            String stYear = s.substring(6,10);

            String dDay = d.substring(0,2);
            String dMonth = d.substring(3,5);
            String dYear = d.substring(6,10);

            String stStartDate = stDay + "-" + stMonth + "-" +stYear;
            String stDeadline = dDay + "-" + dMonth + "-" +dYear;

            entryTask.setLastUpdate(updateTime);
            entryTask.setLastUpdateWho(idMember);
            entryTask.setStartDate(CommonUtil.convertToDate(stStartDate));
            entryTask.setDeadline(CommonUtil.convertToDate(stDeadline));

            if (today.after(entryTask.getStartDate()) && today.before(entryTask.getDeadline()) && entryTask.getProgres() < 100){
                entryTask.setStatusTask("1");
            } else if (today.after(entryTask.getDeadline()) && entryTask.getProgres() < 100){
                entryTask.setStatusTask("3");
            } else if (entryTask.getProgres() == 100){
                entryTask.setStatusTask("2");
            } else if(today.before(entryTask.getStartDate())){
                entryTask.setStatusTask("5");
            }

            try {
                projectBoProxy.saveEditTask(entryTask);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = projectBoProxy.saveErrorMessage(e.getMessage(), "EvalusiLahanAction.saveBatalAsmudSurvey");
                } catch (GeneralBOException e1) {
//                    logger.error("[EvalusiLahanAction.saveAsmudSurvey] Error when saving error,", e1);
                }
//                logger.error("[EvalusiLahanAction.saveAsmudSurvey] Error when saving batal asmud survey lahan," + "[" + logId + "] Found problem when saving batal asmud survey data, please inform to your admin.", e);
                throw new GeneralBOException(e);
            }
        }

        logger.info("[ListTaskController.update] end process PUT /taskasmud/{id} <<<");
        return "success";
    }



    public Collection<Task> getListOfListTask() {
        return listOfListTask;
    }

    public void setListOfListTask(Collection<Task> listOfListTask) {
        this.listOfListTask = listOfListTask;
    }

    public String getIdMember() {
        return idMember;
    }

    public void setIdMember(String idMember) {
        this.idMember = idMember;
    }

    public void setModel(Task model) {
        this.model = model;
    }

    @Override
    public Object getModel() {
//        return model;
        return (listOfListTask != null ? listOfListTask : model);
    }
}
