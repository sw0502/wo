package com.neurix.pmsapb.mobileapi;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.bidang.bo.BidangBo;
import com.neurix.pmsapb.master.bidang.model.Bidang;
import com.neurix.pmsapb.mobileapi.model.BidangMobile;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by lenovo on 5/15/2018.
 */
public class BidangMobileController implements ModelDriven<Object> {
    protected static transient Logger logger = Logger.getLogger(String.valueOf(BidangMobileController.class));
    private BidangBo bidangBoProxy;
    private Collection<BidangMobile> listOfBidang = new ArrayList<BidangMobile>();
    private BidangMobile Bidang = new BidangMobile();
    private String query;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        BidangMobileController.logger = logger;
    }

    public BidangBo getBidangBoProxy() {
        return bidangBoProxy;
    }

    public void setBidangBoProxy(BidangBo bidangBoProxy) {
        this.bidangBoProxy = bidangBoProxy;
    }

    public void setListOfBidang(Collection<BidangMobile> listOfBidang) {
        this.listOfBidang = listOfBidang;
    }

    public BidangMobile getBidang() {
        return Bidang;
    }

    public void setBidang(BidangMobile bidang) {
        Bidang = bidang;
    }


    public HttpHeaders index() {
        List<Bidang> listBidang = new ArrayList<Bidang>();


        Bidang aBidang = new Bidang();
        aBidang.setFlag("Y");

        try {
            listBidang = bidangBoProxy.getByCriteria(aBidang);
        } catch (GeneralBOException e) {
            Long logid = null;
            try {
                logid = bidangBoProxy.saveErrorMessage(e.getMessage(), "bidang controller index");
            } catch (GeneralBOException el) {
//                 logger.error("[DesaController.index] Error when get combo lokasi kebun," + "[" + logId + "] Found problem when retrieving combo lokasi kebun data, please inform to your admin.", e);
            }
        }
        if (listBidang.size() != 0) {
            for (Bidang aList : listBidang) {
                BidangMobile bidang = new BidangMobile();
                bidang.setIdBidang(aList.getIdBidang());
                bidang.setNamaBidang(aList.getNamaBidang());

                listOfBidang.add(bidang);
            }
        }
        logger.info("[DesaController.index] end process <<<");

        return new DefaultHttpHeaders("index").disableCaching();

    }


    @Override
    public Object getModel() {
        return (listOfBidang != null ? listOfBidang : Bidang);


    }

    public String getQuery() {
        return query;
    }

    public Collection<BidangMobile> getListOfBidang() {
        return listOfBidang;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
