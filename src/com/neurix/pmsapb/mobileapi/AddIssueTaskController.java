package com.neurix.pmsapb.mobileapi;

import com.neurix.common.constant.CommonConstant;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.master.doc.model.Document;
import com.neurix.pmsapb.mobileapi.model.Task;
import com.neurix.pmsapb.project.bo.ProjectBo;
import com.neurix.pmsapb.project.model.Project;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.logging.Logger;

/**
 * Created by thinkpad on 15/05/2018.
 */
public class AddIssueTaskController implements ModelDriven<Object> {
    protected static transient Logger logger = Logger.getLogger(String.valueOf(AddIssueTaskController.class));

    private Task model = new Task();
    private ProjectBo projectBoProxy;
    private String id;
    private String idProject;
    private String idTask;
    private String idMember;
    private String namaMember;
    private String namaIssue;

    private File fileUploadDoc;
    private String fileUploadDocContentType;
    private String fileUploadDocFileName;
    private Collection<Task> listOfListTask = new ArrayList<Task>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public File getFileUploadDoc() {
        return fileUploadDoc;
    }

    public void setFileUploadDoc(File fileUploadDoc) {
        this.fileUploadDoc = fileUploadDoc;
    }

    public String getFileUploadDocContentType() {
        return fileUploadDocContentType;
    }

    public void setFileUploadDocContentType(String fileUploadDocContentType) {
        this.fileUploadDocContentType = fileUploadDocContentType;
    }

    public String getFileUploadDocFileName() {
        return fileUploadDocFileName;
    }

    public void setFileUploadDocFileName(String fileUploadDocFileName) {
        this.fileUploadDocFileName = fileUploadDocFileName;
    }

    public void setProjectBoProxy(ProjectBo projectBoProxy) {
        this.projectBoProxy = projectBoProxy;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        AddIssueTaskController.logger = logger;
    }

    public String getNamaIssue() {
        return namaIssue;
    }

    public void setNamaIssue(String namaIssue) {
        this.namaIssue = namaIssue;
    }

    public String getNamaMember() {
        return namaMember;
    }

    public void setNamaMember(String namaMember) {
        this.namaMember = namaMember;
    }

    public String create(){
        logger.info("[UploadDocTaskController.create] Start process POST /List Task <<<");

        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

        try{
            Project entryProjectIssue = new Project();

            entryProjectIssue.setIdMember(idMember);
            entryProjectIssue.setNamaIssue(namaIssue);
            entryProjectIssue.setIdTask(idTask);
            entryProjectIssue.setIdProject(idProject);
            entryProjectIssue.setLastUpdate(updateTime);
            entryProjectIssue.setLastUpdateWho(namaMember);
            entryProjectIssue.setFlag("Y");
            entryProjectIssue.setAction("C");

            projectBo.saveIssue(entryProjectIssue);

        }  catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBo.saveErrorMessage(e.getMessage(), "projectAction.saveIssue");
            } catch (GeneralBOException e1) {
//                logger.error("[projectAction.saveIssue] Error when saving error,", e1);
            }
//            logger.error("[projectAction.saveIssue] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
//            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
//            return ERROR;
        }
        logger.info("[UploadDocTaskController.create] End process POST /List Task <<<");
        return "success";
    }

    public String getIdMember() {
        return idMember;
    }

    public void setIdMember(String idMember) {
        this.idMember = idMember;
    }

    public void setModel(Task model) {
        this.model = model;
    }

    @Override
    public Object getModel() {
//        return model;
        return (listOfListTask != null ? listOfListTask : model);
    }
}
