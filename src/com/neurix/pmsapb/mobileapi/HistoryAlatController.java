package com.neurix.pmsapb.mobileapi;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.alatapb.alatdetail.bo.AlatDetailBo;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetailLog;
import com.neurix.pmsapb.mobileapi.model.HistoryAlat;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by lenovo on 5/15/2018.
 */
public class HistoryAlatController implements ModelDriven<Object> {
    protected static transient Logger logger = Logger.getLogger(String.valueOf(HistoryAlatController.class));
    private AlatDetailBo alatDetailBoProxy;
    private Collection<HistoryAlat> listOfAlatLog = new ArrayList<HistoryAlat>();
    private HistoryAlat AlatLog = new HistoryAlat();
    private String query;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        HistoryAlatController.logger = logger;
    }

    public AlatDetailBo getAlatDetailBoProxy() {
        return alatDetailBoProxy;
    }

    public void setAlatDetailBoProxy(AlatDetailBo alatDetailBoProxy) {
        this.alatDetailBoProxy = alatDetailBoProxy;
    }

    public Collection<HistoryAlat> getListOfAlatLog() {
        return listOfAlatLog;
    }

    public void setListOfAlatLog(Collection<HistoryAlat> listOfAlatLog) {
        this.listOfAlatLog = listOfAlatLog;
    }

    public HistoryAlat getAlatLog() {
        return AlatLog;
    }

    public void setAlatLog(HistoryAlat alatLog) {
        AlatLog = alatLog;
    }

    public HttpHeaders index() {
        List<AlatDetailLog> listAlatLog = new ArrayList<AlatDetailLog>();

        AlatDetailLog alatDetailLog = new AlatDetailLog();
        alatDetailLog.setFlag("Y");

        try {
            listAlatLog = alatDetailBoProxy.getLogByCriteria(alatDetailLog);
        } catch (GeneralBOException e) {
            Long logid = null;
            try {
                logid = alatDetailBoProxy.saveErrorMessage(e.getMessage(), "alatLog controller index");
            } catch (GeneralBOException el) {
//                 logger.error("[HistoryAlatController.index] Error when get combo history alat," + "[" + logId + "] Found problem when retrieving combo history alat, please inform to your admin.", e);
            }
        }
        if (listAlatLog.size() != 0) {
            for (AlatDetailLog alatListLog : listAlatLog) {
                HistoryAlat alatLog = new HistoryAlat();
                alatLog.setIdLogAlatDetail(alatListLog.getIdLogAlatDetail());
                alatLog.setIdAlatDetail(alatListLog.getIdAlatDetail());
                alatLog.setKodeAlat(alatListLog.getKodeAlat());
                alatLog.setKodeApp(alatListLog.getKodeApp());
                alatLog.setNoGi(alatListLog.getNoGi());
                alatLog.setNamaGi(alatListLog.getNamaGi());
                alatLog.setNoGiTujuan(alatListLog.getNoGiTujuan());
                alatLog.setNamaGiTujuan(alatListLog.getNamaGiTujuan());
                alatLog.setNamaAlatDetail(alatListLog.getNamaAlatDetail());
                alatLog.setMerk(alatListLog.getMerk());
                alatLog.setTypeId(alatListLog.getTypeId());
                alatLog.setNoSeries(alatListLog.getNoSeries());
                alatLog.setKdStatus(alatListLog.getKdStatus());
//                alatLog.setTglOprs(alatListLog.getTglOprs());
//                alatLog.setTglPasang(alatListLog.getTglPasang());
                alatLog.setFlag(alatListLog.getFlag());
                alatLog.setAction(alatListLog.getAction());
                //alatLog.setLastUpdate(alatListLog.getLastUpdate());
                alatLog.setLastUpdateWho(alatListLog.getLastUpdateWho());
                alatLog.setNote(alatListLog.getNote());
                alatLog.setUrlFoto(alatListLog.getUrlFoto());
                alatLog.setStatus(alatListLog.getStatus());


                // Create an instance of SimpleDateFormat used for formatting
                // the string representation of date (month/day/year)
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

                // Get the date today using Calendar object.
                Date to = alatListLog.getTglOprs();
                Date tp = alatListLog.getTglPasang();

                String stLastUpdate = String.valueOf(alatListLog.getLastUpdate());

                // Using DateFormat format method we can create a string
                // representation of a date with the defined format.
                String stTglOprs = df.format(to);
//                String stTglPasang = df.format(tp);


                alatLog.setTglOprs(stTglOprs);
//                alatLog.setTglPasang(stTglPasang);
                alatLog.setLastUpdate(stLastUpdate);
             ;

                listOfAlatLog.add(alatLog);
            }
        }
        logger.info("[HistoryController.index] end process <<<");

        return new DefaultHttpHeaders("index").disableCaching();

    }


    @Override
    public Object getModel() {
        return (listOfAlatLog != null ? listOfAlatLog : AlatLog);


    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
