package com.neurix.pmsapb.project.model;

import com.neurix.common.displaytag.DisplayObject;
import com.neurix.common.model.BaseModel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

//import sun.text.normalizer.UCharacter;

/**
 * Created by User on 3/1/2017.
 */
public class Member extends BaseModel implements Serializable {

    private String idProject;
    private String idProjectMember;
    private String idMember;
    private String namaMember;
    private String flag;
    private String action;
    private Long positionId;
    private String stPositionId;
    private String positionName;
    private BigInteger bPositionId;
    private String statusFlag;
    private String roleId;
    private String roleName;
    private String idTask;

    private String photoUrl;
    private String photoPath;
    private boolean added = false;

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public boolean isAdded() {
        return added;
    }

    public void setAdded(boolean added) {
        this.added = added;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    public BigInteger getbPositionId() {
        return bPositionId;
    }

    public void setbPositionId(BigInteger bPositionId) {
        this.bPositionId = bPositionId;
    }

    public String getStPositionId() {
        return stPositionId;
    }

    public void setStPositionId(String stPositionId) {
        this.stPositionId = stPositionId;
    }

    private List<Member> listOfMember;

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public List<Member> getListOfMember() {
        return listOfMember;
    }

    public void setListOfMember(List<Member> listOfMember) {
        this.listOfMember = listOfMember;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getIdMember() {
        return idMember;
    }

    public void setIdMember(String idMember) {
        this.idMember = idMember;
    }

    public String getNamaMember() {
        return namaMember;
    }

    public void setNamaMember(String namaMember) {
        this.namaMember = namaMember;
    }

    public String getIdProjectMember() {
        return idProjectMember;
    }

    public void setIdProjectMember(String idProjectMember) {
        this.idProjectMember = idProjectMember;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }
}
