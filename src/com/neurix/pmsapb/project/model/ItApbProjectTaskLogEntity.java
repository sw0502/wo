package com.neurix.pmsapb.project.model;

import com.neurix.pmsapb.master.status.model.Status;

import javax.persistence.JoinColumn;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Hendra on 3/1/2017.
 */
public class ItApbProjectTaskLogEntity implements Serializable {

    private String idTaskLog;
    private String idTask;
    private String namaTask;
    private Date deadline;
    private Date startDate;
    private String asignTo;
    private String priority;
    private String note;
    private String statusTask;
    private String flag;
    private String action;
    private int progres;
    private String lastUpdateWho;
    private Timestamp lastUpdate;
    private Timestamp lastStatusUpdate;

    public Timestamp getLastStatusUpdate() {
        return lastStatusUpdate;
    }

    public void setLastStatusUpdate(Timestamp lastStatusUpdate) {
        this.lastStatusUpdate = lastStatusUpdate;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    //    @JoinColumn(name = "statusTask", referencedColumnName = "status", insertable = false, updatable = false)
//    private Status status;


    //    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }

    public int getProgres() {
        return progres;
    }

    public void setProgres(int progres) {
        this.progres = progres;
    }

    public String getIdTaskLog() {
        return idTaskLog;
    }

    public void setIdTaskLog(String idTaskLog) {
        this.idTaskLog = idTaskLog;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public String getNamaTask() {
        return namaTask;
    }

    public void setNamaTask(String namaTask) {
        this.namaTask = namaTask;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getAsignTo() {
        return asignTo;
    }

    public void setAsignTo(String asignTo) {
        this.asignTo = asignTo;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatusTask() {
        return statusTask;
    }

    public void setStatusTask(String statusTask) {
        this.statusTask = statusTask;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
