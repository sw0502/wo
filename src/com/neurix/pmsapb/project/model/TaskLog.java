package com.neurix.pmsapb.project.model;

import com.neurix.common.displaytag.DisplayObject;
import com.neurix.common.model.BaseModel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

//import sun.text.normalizer.UCharacter;

/**
 * Created by User on 3/1/2017.
 */
public class TaskLog extends BaseModel implements Serializable {

    private String idTaskLog;
    private String idTask;
    private String namaTask;
    private Date deadline;
    private Date startDate;
    private String statusTask;
    private String asignTo;
    private String priority;
    private String note;
    private String flag;
    private String action;
    private String progresName;
    private String namaStatus;
    private String namaPriority;
    private String userAsignTo;
    private String stDeadline;
    private String stStartDate;
    private String progresBar;
    private String bar;
    private String lastUpdateWho;
    private Timestamp lastUpdate;
    private String statusBefore;
    private Timestamp lastStatusUpdate;

    public String getStatusBefore() {
        return statusBefore;
    }

    public void setStatusBefore(String statusBefore) {
        this.statusBefore = statusBefore;
    }

    public Timestamp getLastStatusUpdate() {
        return lastStatusUpdate;
    }

    public void setLastStatusUpdate(Timestamp lastStatusUpdate) {
        this.lastStatusUpdate = lastStatusUpdate;
    }

    @Override
    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    @Override
    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    @Override
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    private List<Task> listOfTask;


    private int progres;
    private BigInteger sumProgres;
    private BigInteger countOfRow;

    private String statusTaskText;
    private String priorityText;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getProgresName() {
        return progresName;
    }

    public void setProgresName(String progresName) {
        this.progresName = progresName;
    }

    public String getNamaStatus() {
        return namaStatus;
    }

    public void setNamaStatus(String namaStatus) {
        this.namaStatus = namaStatus;
    }

    public String getNamaPriority() {
        return namaPriority;
    }

    public void setNamaPriority(String namaPriority) {
        this.namaPriority = namaPriority;
    }

    public List<Task> getListOfTask() {
        return listOfTask;
    }

    public void setListOfTask(List<Task> listOfTask) {
        this.listOfTask = listOfTask;
    }

    public int getProgres() {
        return progres;
    }

    public void setProgres(int progres) {
        this.progres = progres;
    }

    public String getStatusTaskText() {
        return statusTaskText;
    }

    public void setStatusTaskText(String statusTaskText) {
        this.statusTaskText = statusTaskText;
    }

    public String getPriorityText() {
        return priorityText;
    }

    public void setPriorityText(String priorityText) {
        this.priorityText = priorityText;
    }

    public String getProgresBarText() {
        return progresBarText;
    }

    public void setProgresBarText(String progresBarText) {
        this.progresBarText = progresBarText;
    }

    private String progresBarText;

    public String getIdTaskLog() {
        return idTaskLog;
    }

    public void setIdTaskLog(String idTaskLog) {
        this.idTaskLog = idTaskLog;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public String getNamaTask() {
        return namaTask;
    }

    public void setNamaTask(String namaTask) {
        this.namaTask = namaTask;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getStatusTask() {
        return statusTask;
    }

    public void setStatusTask(String statusTask) {
        this.statusTask = statusTask;
    }

    public String getAsignTo() {
        return asignTo;
    }

    public void setAsignTo(String asignTo) {
        this.asignTo = asignTo;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public BigInteger getSumProgres() {
        return sumProgres;
    }

    public void setSumProgres(BigInteger sumProgres) {
        this.sumProgres = sumProgres;
    }

    public BigInteger getCountOfRow() {
        return countOfRow;
    }

    public void setCountOfRow(BigInteger countOfRow) {
        this.countOfRow = countOfRow;
    }

    public String getUserAsignTo() {
        return userAsignTo;
    }

    public void setUserAsignTo(String userAsignTo) {
        this.userAsignTo = userAsignTo;
    }

    public String getStDeadline() {
        return stDeadline;
    }

    public void setStDeadline(String stDeadline) {
        this.stDeadline = stDeadline;
    }

    public String getProgresBar() {
        return progresBar;
    }

    public void setProgresBar(String progresBar) {
        this.progresBar = progresBar;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStStartDate() {
        return stStartDate;
    }

    public void setStStartDate(String stStartDate) {
        this.stStartDate = stStartDate;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }
}
