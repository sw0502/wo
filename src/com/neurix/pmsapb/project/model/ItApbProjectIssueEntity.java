package com.neurix.pmsapb.project.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Hendra on 3/1/2017.
 */
public class ItApbProjectIssueEntity implements Serializable {

    private String idProject;
    private String idIssue;
    private String namaIssue;
    private String idTask;
    private String idMember;
    private String flag;
    private String action;
    private Timestamp lastUpdate;
    private String lastUpdateWho;

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public String getIdMember() {
        return idMember;
    }

    public void setIdMember(String idMember) {
        this.idMember = idMember;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getIdIssue() {
        return idIssue;
    }

    public void setIdIssue(String idIssue) {
        this.idIssue = idIssue;
    }

    public String getNamaIssue() {
        return namaIssue;
    }

    public void setNamaIssue(String namaIssue) {
        this.namaIssue = namaIssue;
    }
}
