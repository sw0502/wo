package com.neurix.pmsapb.project.model;

import com.neurix.common.displaytag.DisplayObject;
import com.neurix.common.model.BaseModel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

//import sun.text.normalizer.UCharacter;

/**
 * Created by User on 3/1/2017.
 */
public class Task extends BaseModel implements Serializable {

    private String idProject;
    private String idTask;
    private String namaTask;
    private Date deadline;
    private Date startDate;
    private String statusTask;
    private String asignTo;
    private String priority;
    private String note;
    private String flag;
    private String action;
    private String progresName;
    private String namaStatus;
    private String namaPriority;
    private String userAsignTo;
    private String stDeadline;
    private String stStartDate;
    private String progresBar;
    private String bar;
    private String statusFlag;
    private Boolean edited = false;
    private String statusBefore;
    private int progresBefore;
    private Timestamp lastStatusUpdate;

    private String pStartDate;
    private String pDeadline;
    private String noWo;
    private String namaProject;
    private String idMember;
    private String position;
    private String userName;
    private String isTask;
    private String taskStatusText;
    private String priorityText;
    private String progresBarText;
    private String idTaskMember;

    private List<Task> listOfTask;

    private int progres;
    private BigInteger sumProgres;
    private BigInteger countOfRow;

    private boolean added = false;
    private boolean enabledEdit;
    private boolean enabledEditPIC;

    public int getProgresBefore() {
        return progresBefore;
    }

    public void setProgresBefore(int progresBefore) {
        this.progresBefore = progresBefore;
    }

    public String getIdTaskMember() {
        return idTaskMember;
    }

    public void setIdTaskMember(String idTaskMember) {
        this.idTaskMember = idTaskMember;
    }

    public boolean isEnabledEditPIC() {
        return enabledEditPIC;
    }

    public void setEnabledEditPIC(boolean enabledEditPIC) {
        this.enabledEditPIC = enabledEditPIC;
    }

    public boolean isEnabledEdit() {
        return enabledEdit;
    }

    public void setEnabledEdit(boolean enabledEdit) {
        this.enabledEdit = enabledEdit;
    }

    public String getProgresBarText() {
        return progresBarText;
    }

    public void setProgresBarText(String progresBarText) {
        this.progresBarText = progresBarText;
    }

    public String getTaskStatusText() {
        return taskStatusText;
    }

    public void setTaskStatusText(String taskStatusText) {
        this.taskStatusText = taskStatusText;
    }

    public String getPriorityText() {
        return priorityText;
    }

    public void setPriorityText(String priorityText) {
        this.priorityText = priorityText;
    }

    public String getIdMember() {
        return idMember;
    }

    public void setIdMember(String idMember) {
        this.idMember = idMember;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIsTask() {
        return isTask;
    }

    public void setIsTask(String isTask) {
        this.isTask = isTask;
    }

    public String getNamaProject() {
        return namaProject;
    }

    public void setNamaProject(String namaProject) {
        this.namaProject = namaProject;
    }

    public String getNoWo() {
        return noWo;
    }

    public void setNoWo(String noWo) {
        this.noWo = noWo;
    }

    public String getStatusBefore() {
        return statusBefore;
    }

    public void setStatusBefore(String statusBefore) {
        this.statusBefore = statusBefore;
    }

    public Timestamp getLastStatusUpdate() {
        return lastStatusUpdate;
    }

    public void setLastStatusUpdate(Timestamp lastStatusUpdate) {
        this.lastStatusUpdate = lastStatusUpdate;
    }

    public String getpStartDate() {
        return pStartDate;
    }

    public void setpStartDate(String pStartDate) {
        this.pStartDate = pStartDate;
    }

    public String getpDeadline() {
        return pDeadline;
    }

    public void setpDeadline(String pDeadline) {
        this.pDeadline = pDeadline;
    }

    public boolean isAdded() {
        return added;
    }

    public void setAdded(boolean added) {
        this.added = added;
    }

    public Boolean getEdited() {
        return edited;
    }

    public void setEdited(Boolean edited) {
        this.edited = edited;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    public List<Task> getListOfTask() {
        return listOfTask;
    }

    public void setListOfTask(List<Task> listOfTask) {
        this.listOfTask = listOfTask;
    }

    public int getProgres() {
        return progres;
    }

    public void setProgres(int progres) {
        this.progres = progres;
    }

    public String getProgresName() {
        return progresName;
    }

    public void setProgresName(String progresName) {
        this.progresName = progresName;
    }

    public String getNamaStatus() {
        return namaStatus;
    }

    public void setNamaStatus(String namaStatus) {
        this.namaStatus = namaStatus;
    }

    public String getNamaPriority() {
        return namaPriority;
    }

    public void setNamaPriority(String namaPriority) {
        this.namaPriority = namaPriority;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public String getNamaTask() {
        return namaTask;
    }

    public void setNamaTask(String namaTask) {
        this.namaTask = namaTask;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getStatusTask() {
        return statusTask;
    }

    public void setStatusTask(String statusTask) {
        this.statusTask = statusTask;
    }

    public String getAsignTo() {
        return asignTo;
    }

    public void setAsignTo(String asignTo) {
        this.asignTo = asignTo;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public BigInteger getSumProgres() {
        return sumProgres;
    }

    public void setSumProgres(BigInteger sumProgres) {
        this.sumProgres = sumProgres;
    }

    public BigInteger getCountOfRow() {
        return countOfRow;
    }

    public void setCountOfRow(BigInteger countOfRow) {
        this.countOfRow = countOfRow;
    }

    public String getUserAsignTo() {
        return userAsignTo;
    }

    public void setUserAsignTo(String userAsignTo) {
        this.userAsignTo = userAsignTo;
    }

    public String getStDeadline() {
        return stDeadline;
    }

    public void setStDeadline(String stDeadline) {
        this.stDeadline = stDeadline;
    }

    public String getProgresBar() {
        return progresBar;
    }

    public void setProgresBar(String progresBar) {
        this.progresBar = progresBar;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStStartDate() {
        return stStartDate;
    }

    public void setStStartDate(String stStartDate) {
        this.stStartDate = stStartDate;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }
}
