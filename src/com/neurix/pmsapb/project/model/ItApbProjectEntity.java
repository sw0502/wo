package com.neurix.pmsapb.project.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Hendra on 3/1/2017.
 */
public class ItApbProjectEntity implements Serializable {

    private String idProject;
    private String namaProject;
//    private String managerProject;
    private Date deadline;
    private Date startDate;
    private String status;
    private String idTypeProject;
//    private String docUpload;
    private String flag;
    private String action;
    private String noWo;
    private String lastUpdateWho;
    private Timestamp lastUpdate;
    private String idAlatDetail;
    private Integer progres;
    private String divisi;

    public String getDivisi() {
        return divisi;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }

    public Integer getProgres() {
        return progres;
    }

    public void setProgres(Integer progres) {
        this.progres = progres;
    }

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getNamaProject() {
        return namaProject;
    }

    public void setNamaProject(String namaProject) {
        this.namaProject = namaProject;
    }

//    public String getManagerProject() {
//        return managerProject;
//    }
//
//    public void setManagerProject(String managerProject) {
//        this.managerProject = managerProject;
//    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

//    public String getDocUpload() {
//        return docUpload;
//    }
//
//    public void setDocUpload(String docUpload) {
//        this.docUpload = docUpload;
//    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getNoWo() {
        return noWo;
    }

    public void setNoWo(String noWo) {
        this.noWo = noWo;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getIdTypeProject() {
        return idTypeProject;
    }

    public void setIdTypeProject(String idTypeProject) {
        this.idTypeProject = idTypeProject;
    }
}
