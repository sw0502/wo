package com.neurix.pmsapb.project.model;

import com.neurix.common.displaytag.DisplayObject;
import com.neurix.common.model.BaseModel;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

//import sun.text.normalizer.UCharacter;

/**
 * Created by User on 3/1/2017.
 */
public class Project extends BaseModel implements Serializable {

    private String idProject;
    private String namaProject;
    private String managerProject;
    private Date deadline;
    private Date startDate;
    private String status;
    private String namaStatus;
    private boolean enabledPdf;
    private String namaProjectManager;
    private String flag;
    private String action;
    private String noWo;
    private String bulan;
    private String tahun;
    private String noProject;
    private String idTypeProject;
    private String namaTypeProject;
    private Integer progres;
    private String idDivisi;
    private String namaDivisi;

    private Timestamp lastUpdate;
    private String lastUpdateWho;

    private String docUpload;
    private String pdfUpload;

    private String stDeadline;
    private String stStartDate;
    private String pathfile;

    private String idMember;
    private String namaMember;
    private String idProjectMember;
    private String memberPosition;
    private String memberPositionName;
    private String memberRole;

    private String idTask;
    private String namaTask;
    private String asignTo;

    public String getIdDivisi() {
        return idDivisi;
    }

    public void setIdDivisi(String idDivisi) {
        this.idDivisi = idDivisi;
    }

    public String getNamaDivisi() {
        return namaDivisi;
    }

    public void setNamaDivisi(String namaDivisi) {
        this.namaDivisi = namaDivisi;
    }

    @Override
    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    @Override
    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    @Override
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    private String taskStatus;
    private String taskStatusBf;
    private String taskStatusText;
    private String progresBarText;
    private String priorityText;
    private String taskDeadline;
    private String taskStartDate;
    private String priority;
    private String taskNote;
    private int taskProgres;
    private String taskStatusName;
    private String taskPriorityName;

    private String idIssue;
    private String namaIssue;

    private List<Project> listOfProjectMember;
    private List<Project> listOfProjectTask;
    private List<Member> listOfMember;
    private List<Task> lisOfTask;
    private String bar;
    private String progresBar;

    public String getTaskStatusBf() {
        return taskStatusBf;
    }

    public void setTaskStatusBf(String taskStatusBf) {
        this.taskStatusBf = taskStatusBf;
    }

    public String getProgresBar() {
        return progresBar;
    }

    public void setProgresBar(String progresBar) {
        this.progresBar = progresBar;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }

    public Integer getProgres() {
        return progres;
    }

    public void setProgres(Integer progres) {
        this.progres = progres;
    }

    private String idAlatDetail;

    public String getIdAlatDetail() {
        return idAlatDetail;
    }

    public void setIdAlatDetail(String idAlatDetail) {
        this.idAlatDetail = idAlatDetail;
    }



    public List<Member> getListOfMember() {
        return listOfMember;
    }

    public void setListOfMember(List<Member> listOfMember) {
        this.listOfMember = listOfMember;
    }

    public List<Task> getLisOfTask() {
        return lisOfTask;
    }

    public void setLisOfTask(List<Task> lisOfTask) {
        this.lisOfTask = lisOfTask;
    }

    public String getMemberRole() {
        return memberRole;
    }

    public void setMemberRole(String memberRole) {
        this.memberRole = memberRole;
    }

    public String getMemberPositionName() {
        return memberPositionName;
    }

    public void setMemberPositionName(String memberPositionName) {
        this.memberPositionName = memberPositionName;
    }

    public String getMemberPosition() {
        return memberPosition;
    }

    public void setMemberPosition(String memberPosition) {
        this.memberPosition = memberPosition;
    }

    public String getNamaTypeProject() {
        return namaTypeProject;
    }

    public void setNamaTypeProject(String namaTypeProject) {
        this.namaTypeProject = namaTypeProject;
    }

    public String getIdTypeProject() {
        return idTypeProject;
    }

    public void setIdTypeProject(String idTypeProject) {
        this.idTypeProject = idTypeProject;
    }

    public String getTaskStartDate() {
        return taskStartDate;
    }

    public void setTaskStartDate(String taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    public int getTaskProgres() {
        return taskProgres;
    }

    public void setTaskProgres(int taskProgres) {
        this.taskProgres = taskProgres;
    }

    public String getPathfile() {
        return pathfile;
    }

    public void setPathfile(String pathfile) {
        this.pathfile = pathfile;
    }

    public List<Project> getListOfProjectTask() {
        return listOfProjectTask;
    }

    public void setListOfProjectTask(List<Project> listOfProjectTask) {
        this.listOfProjectTask = listOfProjectTask;
    }

    public List<Project> getListOfProjectMember() {
        return listOfProjectMember;
    }

    public void setListOfProjectMember(List<Project> listOfProjectMember) {
        this.listOfProjectMember = listOfProjectMember;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskDeadline() {
        return taskDeadline;
    }

    public void setTaskDeadline(String taskDeadline) {
        this.taskDeadline = taskDeadline;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getTaskNote() {
        return taskNote;
    }

    public void setTaskNote(String taskNote) {
        this.taskNote = taskNote;
    }

    public String getIdIssue() {
        return idIssue;
    }

    public void setIdIssue(String idIssue) {
        this.idIssue = idIssue;
    }

    public String getNamaIssue() {
        return namaIssue;
    }

    public void setNamaIssue(String namaIssue) {
        this.namaIssue = namaIssue;
    }

    public String getNamaStatus() {
        return namaStatus;
    }

    public void setNamaStatus(String namaStatus) {
        this.namaStatus = namaStatus;
    }

    private List<Project> ListOfProjectDetail;

    public List<Project> getListOfProjectDetail() {
        return ListOfProjectDetail;
    }

    public void setListOfProjectDetail(List<Project> listOfProjectDetail) {
        ListOfProjectDetail = listOfProjectDetail;
    }

    public String getAsignTo() {
        return asignTo;
    }

    public void setAsignTo(String asignTo) {
        this.asignTo = asignTo;
    }

    public String getIdMember() {
        return idMember;
    }

    public void setIdMember(String idMember) {
        this.idMember = idMember;
    }

    public String getNamaMember() {
        return namaMember;
    }

    public void setNamaMember(String namaMember) {
        this.namaMember = namaMember;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public String getNamaTask() {
        return namaTask;
    }

    public void setNamaTask(String namaTask) {
        this.namaTask = namaTask;
    }

    public String getStDeadline() {
        return stDeadline;
    }

    public void setStDeadline(String stDeadline) {
        this.stDeadline = stDeadline;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getNamaProject() {
        return namaProject;
    }

    public void setNamaProject(String namaProject) {
        this.namaProject = namaProject;
    }

    public String getManagerProject() {
        return managerProject;
    }

    public void setManagerProject(String managerProject) {
        this.managerProject = managerProject;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDocUpload() {
        return docUpload;
    }

    public void setDocUpload(String docUpload) {
        this.docUpload = docUpload;
    }

    public String getPdfUpload() {
        return pdfUpload;
    }

    public void setPdfUpload(String pdfUpload) {
        this.pdfUpload = pdfUpload;
    }

    public boolean isEnabledPdf() {
        return enabledPdf;
    }

    public void setEnabledPdf(boolean enabledPdf) {
        this.enabledPdf = enabledPdf;
    }

    public String getNamaProjectManager() {
        return namaProjectManager;
    }

    public void setNamaProjectManager(String namaProjectManager) {
        this.namaProjectManager = namaProjectManager;
    }

    public String getIdProjectMember() {
        return idProjectMember;
    }

    public void setIdProjectMember(String idProjectMember) {
        this.idProjectMember = idProjectMember;
    }

    public String getTaskStatusName() {
        return taskStatusName;
    }

    public void setTaskStatusName(String taskStatusName) {
        this.taskStatusName = taskStatusName;
    }

    public String getTaskPriorityName() {
        return taskPriorityName;
    }

    public void setTaskPriorityName(String taskPriorityName) {
        this.taskPriorityName = taskPriorityName;
    }

    public String getNoWo() {
        return noWo;
    }

    public void setNoWo(String noWo) {
        this.noWo = noWo;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getNoProject() {
        return noProject;
    }

    public void setNoProject(String noProject) {
        this.noProject = noProject;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStStartDate() {
        return stStartDate;
    }

    public String getProgresBarText() {
        return progresBarText;
    }

    public void setProgresBarText(String progresBarText) {
        this.progresBarText = progresBarText;
    }

    public String getPriorityText() {
        return priorityText;
    }

    public void setPriorityText(String priorityText) {
        this.priorityText = priorityText;
    }

    public void setStStartDate(String stStartDate) {
        this.stStartDate = stStartDate;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getTaskStatusText() {
        return taskStatusText;
    }

    public void setTaskStatusText(String taskStatusText) {
        this.taskStatusText = taskStatusText;
    }
}
