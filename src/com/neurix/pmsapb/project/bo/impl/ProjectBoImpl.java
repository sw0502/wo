package com.neurix.pmsapb.project.bo.impl;


import com.neurix.authorization.user.dao.UserDao;
import com.neurix.authorization.user.dao.UserRoleDao;
import com.neurix.authorization.user.model.ImUsers;
import com.neurix.authorization.user.model.ImUsersRoles;
import com.neurix.authorization.user.model.User;
import com.neurix.authorization.user.model.UserRoles;
import com.neurix.common.constant.CommonConstant;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.master.divisi.dao.DivisiDao;
import com.neurix.pmsapb.master.divisi.model.Divisi;
import com.neurix.pmsapb.master.divisi.model.ImApbDivisiEntity;
import com.neurix.pmsapb.master.doc.dao.DocDao;
import com.neurix.pmsapb.master.doc.model.Document;
import com.neurix.pmsapb.master.doc.model.ItApbProjectDocEntity;
import com.neurix.pmsapb.master.typeproject.dao.TypeProjectDao;
import com.neurix.pmsapb.master.typeproject.model.ImApbTypeProjectEntity;
import com.neurix.pmsapb.master.typeproject.model.TypeProject;
import com.neurix.pmsapb.project.bo.ProjectBo;
import com.neurix.pmsapb.project.dao.*;
import com.neurix.pmsapb.project.model.*;
import com.neurix.pmsapb.project.taskmember.dao.TaskMemberDao;
import com.neurix.pmsapb.project.taskmember.model.ItApbTaskMemberEntity;
import com.neurix.pmsapb.project.taskmember.model.TaskMember;
import com.neurix.pmsapb.project.taskmemberchange.dao.TaskMemberChangeDao;
import com.neurix.pmsapb.project.taskmemberchange.model.ItApbTaskMemberChangeEntity;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.math.BigInteger;
import java.sql.Date;
import java.util.*;

/**
 * Created by User on 3/1/2017.
 */
public class ProjectBoImpl implements ProjectBo {
    protected static transient Logger logger = Logger.getLogger(ProjectBoImpl.class);
    private ProjectDao projectDao;
    private ProjectIssueDao projectIssueDao;
    private ProjectMemberDao projectMemberDao;
    private ProjectTaskDao projectTaskDao;
    private DocDao docDao;
    private TypeProjectDao typeProjectDao;
    private UserRoleDao userRoleDao;
    private ProjectTaskLogDao projectTaskLogDao;
    private UserDao userDao;
    private TaskMemberDao taskMemberDao;
    private TaskMemberChangeDao taskMemberChangeDao;

    private DivisiDao divisiDao;

    private Task task;
    private Project project;
    String ronGenName;

    public DivisiDao getDivisiDao() {
        return divisiDao;
    }

    public void setDivisiDao(DivisiDao divisiDao) {
        this.divisiDao = divisiDao;
    }

    public TaskMemberChangeDao getTaskMemberChangeDao() {
        return taskMemberChangeDao;
    }

    public void setTaskMemberChangeDao(TaskMemberChangeDao taskMemberChangeDao) {
        this.taskMemberChangeDao = taskMemberChangeDao;
    }

    public TaskMemberDao getTaskMemberDao() {
        return taskMemberDao;
    }

    public void setTaskMemberDao(TaskMemberDao taskMemberDao) {
        this.taskMemberDao = taskMemberDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public ProjectTaskLogDao getProjectTaskLogDao() {
        return projectTaskLogDao;
    }

    public void setProjectTaskLogDao(ProjectTaskLogDao projectTaskLogDao) {
        this.projectTaskLogDao = projectTaskLogDao;
    }

    public UserRoleDao getUserRoleDao() {
        return userRoleDao;
    }

    public void setUserRoleDao(UserRoleDao userRoleDao) {
        this.userRoleDao = userRoleDao;
    }

    public ProjectMemberDao getProjectMemberDao() {
        return projectMemberDao;

    }

    public TypeProjectDao getTypeProjectDao() {
        return typeProjectDao;
    }

    public void setTypeProjectDao(TypeProjectDao typeProjectDao) {
        this.typeProjectDao = typeProjectDao;
    }

    public void setProjectMemberDao(ProjectMemberDao projectMemberDao) {
        this.projectMemberDao = projectMemberDao;
    }

    public ProjectTaskDao getProjectTaskDao() {
        return projectTaskDao;
    }

    public void setProjectTaskDao(ProjectTaskDao projectTaskDao) {
        this.projectTaskDao = projectTaskDao;
    }

    public ProjectIssueDao getProjectIssueDao() {
        return projectIssueDao;
    }

    public void setProjectIssueDao(ProjectIssueDao projectIssueDao) {
        this.projectIssueDao = projectIssueDao;
    }

    public ProjectDao getProjectDao() {
        return projectDao;
    }

    public void setProjectDao(ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

    public String getRonGenName() {
        return ronGenName;
    }

    public void setRonGenName(String ronGenName) {
        this.ronGenName = ronGenName;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        ProjectBoImpl.logger = logger;
    }

    public DocDao getDocDao() {
        return docDao;
    }

    public void setDocDao(DocDao docDao) {
        this.docDao = docDao;
    }

    @Override
    public List<Project> getSearchByCriteria(Project searchBean, String roleAs, String LoginId) throws GeneralBOException {
        logger.info("[ProjectBoImpl.getSearchByCriteria] start process >>>");

        List<Project> listOfResult = new ArrayList<Project>();
//        List<Project> listOfResultProject = new ArrayList<Project>();

        String bulan = searchBean.getBulan();
        String tahun = searchBean.getTahun();

//        if("".equalsIgnoreCase(searchBean.getIdProject())){
//            searchBean.setIdProject("%");
//        } if("".equalsIgnoreCase(searchBean.getNamaProject())){
//            searchBean.setNamaProject("%");
//        } if("".equalsIgnoreCase(searchBean.getStatus())){
//            searchBean.setStatus("%");
//        } if("".equalsIgnoreCase(searchBean.getIdTypeProject())){
//            searchBean.setIdTypeProject("%");
//        }

        String idProject = searchBean.getIdProject();
        String namaProject  = searchBean.getNamaProject();
        String status = searchBean.getStatus();
        String idTypeProject = searchBean.getIdTypeProject();

        if(!"".equalsIgnoreCase(bulan) && !"".equalsIgnoreCase(tahun)){
            Project searchProject = new Project();
            searchProject.setTahun(tahun);
            searchProject.setBulan(bulan);
            searchProject.setIdProject(idProject);
            searchProject.setNamaProject(namaProject);
            searchProject.setStatus(status);
            searchProject.setIdTypeProject(idTypeProject);

            try{
                listOfResult = projectDao.getSearchByDate(searchProject);
            }catch (GeneralBOException e){
                logger.info(e);
            }
        } else if(!"".equalsIgnoreCase(tahun) && "".equalsIgnoreCase(bulan)){
            Project searchProject = new Project();
            searchProject.setTahun(tahun);
            searchProject.setIdProject(idProject);
            searchProject.setNamaProject(namaProject);
            searchProject.setStatus(status);
            searchProject.setIdTypeProject(idTypeProject);

            try{
                listOfResult = projectDao.getSearchByDate(searchProject);
            }catch (GeneralBOException e){
                logger.info(e);
            }
        }else if("".equalsIgnoreCase(tahun) && !"".equalsIgnoreCase(bulan)){
            Project searchProject = new Project();
            searchProject.setBulan(bulan);
            searchProject.setIdProject(idProject);
            searchProject.setNamaProject(namaProject);
            searchProject.setStatus(status);
            searchProject.setIdTypeProject(idTypeProject);
            try{
                listOfResult = projectDao.getSearchByDate(searchProject);
            }catch (GeneralBOException e){
                logger.info(e);
            }

        } else {
            if (searchBean != null) {
                Map hsCriteria = new HashMap();

                if (searchBean.getIdProject() != null && !"".equalsIgnoreCase(searchBean.getIdProject())) {
                    hsCriteria.put("project_id", searchBean.getIdProject());
                }
                if (searchBean.getNamaProject() != null && !"".equalsIgnoreCase(searchBean.getNamaProject())) {
                    hsCriteria.put("project_name", searchBean.getNamaProject());
                }
                if (searchBean.getStatus() != null && !"".equalsIgnoreCase(searchBean.getStatus())) {
                    hsCriteria.put("status", searchBean.getStatus());
                }
                if (searchBean.getIdTypeProject() != null && !"".equalsIgnoreCase(searchBean.getIdTypeProject())) {
                    hsCriteria.put("type_project_id", searchBean.getIdTypeProject());
                }
                if (searchBean.getIdDivisi() != null && !"".equalsIgnoreCase(searchBean.getIdDivisi())) {
                    hsCriteria.put("divisi", searchBean.getIdDivisi());
                }


                List<ItApbProjectEntity> itApbProjectEntities = null;

                try {
                    listOfResult = projectDao.getSearchByCriteria(hsCriteria);
                } catch (HibernateException e) {
                    logger.error("[ProjectBoImpl.getSearchByCriteria] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                }
            }
        }
        // menambahkan divisi di search list
        if (listOfResult != null){
            for (Project listProject : listOfResult){
                Map hsCriteria = new HashMap();
                if (listProject.getIdDivisi() != null ){
                    hsCriteria.put("divisi_id", listProject.getIdDivisi());
                    hsCriteria.put("flag","Y");

                    List<ImApbDivisiEntity> imApbDivisiEntityList = null;

                    try {
                        imApbDivisiEntityList = divisiDao.getByCriteria(hsCriteria);
                    } catch (HibernateException e) {
                        logger.error("[ProjectBoImpl.getSearchByCriteria] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
                    }

                    if (imApbDivisiEntityList != null){
                        for (ImApbDivisiEntity listDivisi : imApbDivisiEntityList){
                            listProject.setNamaDivisi(listDivisi.getNamaDivisi());
                        }
                    }
                }
            }
        }

//        Project searchProject;
//        for(Project listOfProject : listOfResult){
//            searchProject = new Project();
//            searchProject.setIdProject(listOfProject.getIdProject());
//        }
        logger.info("[SurveyEvalBoImpl.getSearchByCriteria] end process <<<");
        return listOfResult;
    }

    @Override
    public void saveProject(Project bean) throws GeneralBOException {

        logger.info("[ProjectBoImpl.saveProject] start process >>>");

        if (bean!=null) {
            ItApbProjectEntity itApbProjectEntityNew = new ItApbProjectEntity();
            itApbProjectEntityNew.setIdProject(bean.getIdProject());
            itApbProjectEntityNew.setNamaProject(bean.getNamaProject());
//            itApbProjectEntityNew.setManagerProject(bean.getManagerProject());
            itApbProjectEntityNew.setDeadline(CommonUtil.convertToDate(bean.getStDeadline()));
            itApbProjectEntityNew.setStartDate(CommonUtil.convertToDate(bean.getStStartDate()));
            itApbProjectEntityNew.setNoWo(bean.getNoWo());
            itApbProjectEntityNew.setIdTypeProject(bean.getIdTypeProject());
            itApbProjectEntityNew.setFlag("Y");
            itApbProjectEntityNew.setAction("C");

            Date projectDeadline = CommonUtil.convertToDate(bean.getStDeadline());
            Date projectStartDate = CommonUtil.convertToDate(bean.getStStartDate());
            java.util.Date utilDate = new java.util.Date();
            Date today = new Date(utilDate.getTime());

            if(today.after(projectDeadline)){
                itApbProjectEntityNew.setStatus("3");
            } else if (today.after(projectStartDate) && today.before(projectDeadline)){
                itApbProjectEntityNew.setStatus("1");
            }
            else if(today.before(projectStartDate)){
                itApbProjectEntityNew.setStatus("4");
            }

            try {
                projectDao.addAndSave(itApbProjectEntityNew);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveProject] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }
        }
        logger.info("[ProjectBoImpl.saveProject] end process <<<");
    }

    @Override
    public List<Project> viewProject(String idProject) throws GeneralBOException {

        logger.info("[ProjectBoImpl.viewProject] start process >>>");

//        Project project = new Project();
        List<Project> listOfResult = new ArrayList();

        Map hsCriteria = new HashMap();

        if (idProject != null && !"".equalsIgnoreCase(idProject)) {
            hsCriteria.put("project_id",idProject);
        }
            List<Project> listOfProjectDetail = null;

            try {
                listOfResult = projectDao.getViewProject(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.viewProject] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
        logger.info("[ProjectBoImpl.viewProject] end process <<<");

        return listOfResult;
    }

    public List<Task> getSearchTaskByCriteria (String idProject) throws GeneralBOException {
        logger.info("[ProjectBoImpl.viewProject] start process >>>");

//        Project project = new Project();
        List<Task> listOfResult = new ArrayList();

        Map hsCriteria = new HashMap();

        if (idProject != null && !"".equalsIgnoreCase(idProject)) {
            hsCriteria.put("project_id",idProject);
        }

//        List<Project> listOfProjectDetail = null;
        try {
            listOfResult = projectDao.getTaskByCriteria(hsCriteria);
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.getSearchTaskByCriteria] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
        }
        logger.info("[ProjectBoImpl.getSearchTaskByCriteria] end process <<<");

        return listOfResult;

    }

    @Override
    public List<Project> getProjectTaskByCriteria(Project task, String userRole, String idLogin) throws GeneralBOException {
        logger.info("[ProjectBoImpl.getSearchTaskByCriteria] start process >>>");

        List<Project> listOfResult = new ArrayList<Project>();

        if (task != null) {
            Map hsCriteria = new HashMap();

            if (task.getIdProject() != null && !"".equalsIgnoreCase(task.getIdProject())) {
                hsCriteria.put("project_id", task.getIdProject());
            }
            if (idLogin != null && !"".equalsIgnoreCase(idLogin)) {
                hsCriteria.put("asign_to", idLogin);
            }
            if (task.getNoWo() != null && !"".equalsIgnoreCase(task.getNoWo())) {
                hsCriteria.put("no_wo", task.getNoWo());
            }
            if (task.getNamaProject() != null && !"".equalsIgnoreCase(task.getNamaProject())) {
                hsCriteria.put("project_name", task.getNamaProject());
            }
            if (task.getTaskStatus() != null && !"".equalsIgnoreCase(task.getTaskStatus())) {
                hsCriteria.put("status_task", task.getTaskStatus());
            }
            if (userRole == "ADMIN"){
                hsCriteria.put("member_id", "admin");
            } else {
                hsCriteria.put("member_id", idLogin);
            }
            if (task.getFlag() != null && !"".equalsIgnoreCase(task.getFlag())) {
                if ("N".equalsIgnoreCase(task.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", task.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            try {

                listOfResult = projectDao.getProjectByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.getSearchTaskByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

        }
        logger.info("[ProjectBoImpl.getSearchTaskByCriteria] end process <<<");

        return listOfResult;
    }

    @Override
    public List<TaskLog> getSearchTaskLogByCriteria(String itamId) throws GeneralBOException {
        List<TaskLog> listOfResult = new ArrayList();

        Map hsCriteria = new HashMap();

        if (itamId != null && !"".equalsIgnoreCase(itamId)) {
            hsCriteria.put("task_id",itamId);
        }

        List<ItApbProjectTaskLogEntity> listOfProjectTaskLog = null;

        try {
            listOfProjectTaskLog = projectTaskLogDao.getByCriteria(hsCriteria);
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.viewProject] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
        }
        if (listOfProjectTaskLog != null){
            TaskLog entryTaskLog;
            for(ItApbProjectTaskLogEntity dataTaskLog : listOfProjectTaskLog){
                entryTaskLog = new TaskLog();
                entryTaskLog.setIdTaskLog(dataTaskLog.getIdTaskLog());
                entryTaskLog.setIdTask(dataTaskLog.getIdTask());
                entryTaskLog.setNamaTask(dataTaskLog.getNamaTask());
                entryTaskLog.setAsignTo(dataTaskLog.getAsignTo());
                entryTaskLog.setStartDate(dataTaskLog.getStartDate());
                entryTaskLog.setDeadline(dataTaskLog.getDeadline());
                entryTaskLog.setPriority(dataTaskLog.getPriority());
                entryTaskLog.setProgres(dataTaskLog.getProgres());
                entryTaskLog.setStatusTask(dataTaskLog.getStatusTask());
                entryTaskLog.setNote(dataTaskLog.getNote());
                entryTaskLog.setLastUpdate(dataTaskLog.getLastUpdate());
//                entryTaskLog.setLastStatusUpdate(dataTaskLog.get);
                entryTaskLog.setLastUpdateWho(dataTaskLog.getLastUpdateWho());


                java.util.Date utilDate = new java.util.Date();
                Date today = new Date(utilDate.getTime());
                Date startDateTask = entryTaskLog.getStartDate();

                if ("1".equalsIgnoreCase(entryTaskLog.getStatusTask())){
                    entryTaskLog.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
                } else if ("2".equalsIgnoreCase(entryTaskLog.getStatusTask())) {
                    entryTaskLog.setNamaStatus("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
                }else if("3".equalsIgnoreCase(entryTaskLog.getStatusTask())) {
                    entryTaskLog.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Pending</div>");
                } else if("4".equalsIgnoreCase(entryTaskLog.getStatusTask())){
                    entryTaskLog.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Initiate</div>");
                } else {
                    entryTaskLog.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                }


                if("1".equalsIgnoreCase(entryTaskLog.getPriority())){
                    entryTaskLog.setNamaPriority("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>High</div>");
                } else if("2".equalsIgnoreCase(entryTaskLog.getPriority())){
                    entryTaskLog.setNamaPriority("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Normal</div>");
                } else{
                    entryTaskLog.setNamaPriority("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>Low</div>");
                }

                int hasil = entryTaskLog.getProgres() + 50;
                entryTaskLog.setBar(String.valueOf(hasil));

                String presentase = String.valueOf(entryTaskLog.getProgres());
                String barWidth = entryTaskLog.getBar();

                entryTaskLog.setProgresBar("<div style=\"width:150px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                        "<div align=\"center\" style=\"width:"+barWidth+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+presentase+"%</div>");

                listOfResult.add(entryTaskLog);
            }
        }
        logger.info("[ProjectBoImpl.viewProject] end process <<<");
        return listOfResult;
    }

    @Override
    public List<ProjectIssue> getSearchIssueByCriteria(String idProject) throws GeneralBOException {

//        Project project = new Project();
        List<ProjectIssue> listOfResult = new ArrayList();

        Map hsCriteria = new HashMap();

        if (idProject != null && !"".equalsIgnoreCase(idProject)) {
            hsCriteria.put("project_id",idProject);
        }
        List<ItApbProjectIssueEntity> listOfIssue = null;
        try {
            listOfIssue = projectIssueDao.getByCriteria(hsCriteria);
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.getSearchTaskByCriteria] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
        }
        logger.info("[ProjectBoImpl.getSearchTaskByCriteria] end process <<<");
        if(listOfIssue != null){
            ProjectIssue resultIssue;
            for(ItApbProjectIssueEntity itApbIssue : listOfIssue){
                resultIssue = new ProjectIssue();
                resultIssue.setIdProject(itApbIssue.getIdProject());
                resultIssue.setIdIssue(itApbIssue.getIdIssue());
                resultIssue.setNamaIssue(itApbIssue.getNamaIssue());
                resultIssue.setFlag(itApbIssue.getFlag());
                resultIssue.setAction(itApbIssue.getAction());
                resultIssue.setLastUpdate(itApbIssue.getLastUpdate());
                resultIssue.setLastUpdateWho(itApbIssue.getLastUpdateWho());
                String idMember = itApbIssue.getIdMember();
                resultIssue.setIdMember(idMember);



                if (idMember != null && !"".equalsIgnoreCase(idMember)) {
                    hsCriteria.put("user_id",idMember);
                    hsCriteria.put("flag","Y");
                }


//                if (idMember != null && !"".equalsIgnoreCase(idMember)) {
//                    hsCriteria.put("member_id",idMember);
//                }
                List<ImUsers> listOfMember = null;
                User dataUser = new User();
                dataUser.setUserId(idMember);
                dataUser.setFlag("Y");
                listOfMember = userDao.getByCriteria(hsCriteria);
                if(listOfMember != null){
                    for (ImUsers memberData :listOfMember){
                        if (idMember.equalsIgnoreCase(memberData.getPrimaryKey().getId())){
                            String filePath = "/pmsapb"+CommonConstant.RESOURCE_PATH_USER_UPLOAD + memberData.getPhotoUrl();
                            resultIssue.setPhotoPath("<img src=\""+filePath+"\" style=\"border-radius:15px;width:30px;height:30px;\" width=\"30px\" height=\"30px\" />\n");
//                            resultIssue.setPhotoPath(memberData.getPhotoUserUrl());
                            resultIssue.setPhotoUrl(memberData.getPhotoUrl());
                            resultIssue.setNamaMember(memberData.getUserName());
                        }
                    }
                }

                if ("Y".equalsIgnoreCase(itApbIssue.getFlag())){
                    resultIssue.setStatusFlag("ACTIVE");
                } else {
                    resultIssue.setStatusFlag("NON ACTIVE");
                }
                listOfResult.add(resultIssue);
            }
        }
        return listOfResult;
    }

    @Override
    public List<Document> getSearchDocumentByCriteria(String idProject) throws GeneralBOException {
        List<Document> listOfResult = new ArrayList();

        Map hsCriteria = new HashMap();

        if (idProject != null && !"".equalsIgnoreCase(idProject)) {
            hsCriteria.put("project_id",idProject);
        }
        List<ItApbProjectDocEntity> listOfDoc = null;
        try {
            listOfDoc = docDao.getByCriteria(hsCriteria);
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.getSearchTaskByCriteria] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
        }
        logger.info("[ProjectBoImpl.getSearchTaskByCriteria] end process <<<");

        if(listOfDoc != null){
            Document resultDoc;
            for(ItApbProjectDocEntity itApbDoc : listOfDoc){
                resultDoc = new Document();
                resultDoc.setIdProject(itApbDoc.getIdProject());
                resultDoc.setIdTask(itApbDoc.getIdTask());
                resultDoc.setIdDoc(itApbDoc.getIdDoc());
                resultDoc.setNamaDoc(itApbDoc.getNamaDoc());
                resultDoc.setLastUpdate(itApbDoc.getLastUpdate());
                resultDoc.setUploadBy(itApbDoc.getUploadBy());
                resultDoc.setDocType(itApbDoc.getDocType());

                if (itApbDoc.getIdTask() == null){
                    resultDoc.setStatusDocument("Doc Project");
                } else {
                    resultDoc.setStatusDocument("Doc Task");
                }
                if(resultDoc.getNamaDoc()!=null){
                    String folderPath = CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC + resultDoc.getNamaDoc();
                    resultDoc.setFilePath("/pmsapb"+folderPath);
                }

                String linkDownload = "<a href='"+resultDoc.getFilePath()+"' download><img src='/pmsapb/pages/images/icon_download.png' width='23px'/></a>";

                if ("file".equals(itApbDoc.getDocType())){
                    resultDoc.setEnabledView(false);
                    resultDoc.setDownloadPath(linkDownload);
                } else if ("doc".equals(itApbDoc.getDocType())){
                    resultDoc.setEnabledView(true);
                    resultDoc.setDownloadPath(linkDownload);
                }else {
                    resultDoc.setEnabledView(false);
                }

                listOfResult.add(resultDoc);
            }
        }
        return listOfResult;

    }

    @Override
    public List<Task> getSearchTaskCombo(Task dataTask) throws GeneralBOException {
        return null;
    }

    @Override
    public List<UserRoles> getUsersRolesByCriteria(String itemId) throws GeneralBOException {
        List<UserRoles> listOfUserRoles = new ArrayList<UserRoles>();
        if(itemId != null){
            String userid = itemId;
            Map hsCriteria = new HashMap();
            if (userid != null && !"".equalsIgnoreCase(userid)) {
                hsCriteria.put("user_id",userid);
            }
            List<ImUsersRoles> imUserRolesEntity = null;

            imUserRolesEntity = userRoleDao.getByCriteria(hsCriteria);

            if(imUserRolesEntity != null){
                UserRoles userRolesItem;
                for(ImUsersRoles imUserRolesItem : imUserRolesEntity){
                    userRolesItem = new UserRoles();
                    userRolesItem.setRoleId(imUserRolesItem.getPrimaryKey().getRoleId());
                    userRolesItem.setUserId(imUserRolesItem.getPrimaryKey().getUserId());
                    listOfUserRoles.add(userRolesItem);
                }
            }
        }
        return listOfUserRoles;
    }

    @Override
    public void saveProjectMember(Project project, List<Project> listOfProjectMember) {
        String projectId = project.getIdProject();
        if(projectId != null){
            for(Project addProjectmember : listOfProjectMember){
                ItApbProjectMemberEntity itApbProjectMemberEntityNew = new ItApbProjectMemberEntity();
                itApbProjectMemberEntityNew.setIdProject(projectId);
                itApbProjectMemberEntityNew.setIdMember(addProjectmember.getIdMember());
                try{
                    projectMemberDao.addAndSave(itApbProjectMemberEntityNew);
                }catch (HibernateException e){
                    logger.error("[ProjectBoImpl.saveProjectMember] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void saveProjectMemberTask(Project project, List<Member> listOfProjectMember, List<Task> listOfProjectTask, List<ProjectIssue> listOfissue, List<TaskMember> listOfTaskMember) {
        String projectId = project.getIdProject();

        if(project != null){
            ItApbProjectEntity itApbProjectEntityNew = new ItApbProjectEntity();
            itApbProjectEntityNew.setIdProject(project.getIdProject());
            itApbProjectEntityNew.setNamaProject(project.getNamaProject());
//            itApbProjectEntityNew.setManagerProject(project.getManagerProject());
            itApbProjectEntityNew.setDeadline(CommonUtil.convertToDate(project.getStDeadline()));
            itApbProjectEntityNew.setStartDate(CommonUtil.convertToDate(project.getStStartDate()));
            itApbProjectEntityNew.setNoWo(project.getNoWo());
            itApbProjectEntityNew.setIdTypeProject(project.getIdTypeProject());
            itApbProjectEntityNew.setFlag("Y");
            itApbProjectEntityNew.setAction("C");
            itApbProjectEntityNew.setLastUpdate(project.getLastUpdate());
            itApbProjectEntityNew.setLastUpdateWho(project.getLastUpdateWho());
            itApbProjectEntityNew.setProgres(10);
            itApbProjectEntityNew.setDivisi(project.getIdDivisi());
            if(!"".equalsIgnoreCase(project.getIdProject())){
                itApbProjectEntityNew.setIdAlatDetail(project.getIdAlatDetail());

            }
            Date projectDeadline = CommonUtil.convertToDate(project.getStDeadline());
            Date projectStartDate = CommonUtil.convertToDate(project.getStStartDate());
            java.util.Date utilDate = new java.util.Date();
            Date today = new Date(utilDate.getTime());

            if(today.after(projectDeadline)){
                itApbProjectEntityNew.setStatus("3");
            } else if (today.after(projectStartDate) && today.before(projectDeadline)){
                itApbProjectEntityNew.setStatus("1");
            }
            else if(today.before(projectStartDate)){
                itApbProjectEntityNew.setStatus("4");
            }

            try {
                projectDao.addAndSave(itApbProjectEntityNew);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveProject] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }
        }
        if (listOfProjectMember != null){
            for (Member addProjectMember : listOfProjectMember){
                ItApbProjectMemberEntity itApbProjectmemberEntityNew = new ItApbProjectMemberEntity();
                itApbProjectmemberEntityNew.setIdProjectMember(addProjectMember.getIdProjectMember());
                itApbProjectmemberEntityNew.setIdProject(projectId);
                itApbProjectmemberEntityNew.setIdMember(addProjectMember.getIdMember());
                itApbProjectmemberEntityNew.setPositionId(addProjectMember.getPositionId());
                itApbProjectmemberEntityNew.setFlag("Y");
                itApbProjectmemberEntityNew.setAction("C");
                itApbProjectmemberEntityNew.setLastUpdate(project.getLastUpdate());
                itApbProjectmemberEntityNew.setLastUpdateWho(project.getLastUpdateWho());
                try{
                    projectMemberDao.addAndSave(itApbProjectmemberEntityNew);
                } catch (HibernateException e){
                    logger.error("[ProjectBoImpl.saveProjectMemberTask] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                }
            }
        }if (listOfProjectTask != null){
            for (Task addProjectTask : listOfProjectTask){
                ItApbProjectTaskEntity itApbProjectTaskEntityNew = new ItApbProjectTaskEntity();
                itApbProjectTaskEntityNew.setIdProject(projectId);
                itApbProjectTaskEntityNew.setIdTask(addProjectTask.getIdTask());
                itApbProjectTaskEntityNew.setNamaTask(addProjectTask.getNamaTask());
                itApbProjectTaskEntityNew.setStatusTask(addProjectTask.getStatusTask());
                itApbProjectTaskEntityNew.setPriority(addProjectTask.getPriority());
                itApbProjectTaskEntityNew.setAsignTo(addProjectTask.getAsignTo());
                itApbProjectTaskEntityNew.setDeadline(CommonUtil.convertToDate(addProjectTask.getStDeadline()));
                itApbProjectTaskEntityNew.setStartDate(CommonUtil.convertToDate(addProjectTask.getStStartDate()));
                itApbProjectTaskEntityNew.setNote(addProjectTask.getNote());
                itApbProjectTaskEntityNew.setProgres(10);
                itApbProjectTaskEntityNew.setFlag("Y");
                itApbProjectTaskEntityNew.setAction("C");
                itApbProjectTaskEntityNew.setLastUpdate(project.getLastUpdate());
                itApbProjectTaskEntityNew.setLastUpdateWho(project.getLastUpdateWho());
                try{
                    projectTaskDao.addAndSave(itApbProjectTaskEntityNew);
                } catch (HibernateException e){
                    logger.error("[ProjectBoImpl.saveProjectTask] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                }

                String idTaskLog = "";
                try {
                    idTaskLog=projectTaskLogDao.getNextTaskLogId();
                } catch (HibernateException e) {
                    logger.error("[PermohonanLahanBoImpl.getSeqNumLahanId] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
                }
                ItApbProjectTaskLogEntity entryTasklog = new ItApbProjectTaskLogEntity();
                entryTasklog.setIdTaskLog("TL"+idTaskLog);
                entryTasklog.setIdTask(addProjectTask.getIdTask());
                entryTasklog.setNamaTask(addProjectTask.getNamaTask());
                entryTasklog.setStatusTask(addProjectTask.getStatusTask());
                entryTasklog.setPriority(addProjectTask.getPriority());
                entryTasklog.setAsignTo(addProjectTask.getAsignTo());
                entryTasklog.setDeadline(CommonUtil.convertToDate(addProjectTask.getStDeadline()));
                entryTasklog.setStartDate(CommonUtil.convertToDate(addProjectTask.getStStartDate()));
                entryTasklog.setNote(addProjectTask.getNote());
                entryTasklog.setProgres(10);
                entryTasklog.setFlag("Y");
                entryTasklog.setAction("C");
                entryTasklog.setLastUpdate(project.getLastUpdate());
                entryTasklog.setLastUpdateWho(project.getLastUpdateWho());
                entryTasklog.setLastStatusUpdate(project.getLastUpdate());
                try{
                    projectTaskLogDao.addAndSave(entryTasklog);
                } catch (HibernateException e){
                    logger.error("[ProjectBoImpl.saveProjectTask] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                }
            }
        }
        if (listOfissue!=null){
            for(ProjectIssue issueData : listOfissue){
                ItApbProjectIssueEntity entryIssue = new ItApbProjectIssueEntity();
                entryIssue.setIdProject(projectId);
                entryIssue.setIdIssue(issueData.getIdIssue());

                entryIssue.setNamaIssue(issueData.getNamaIssue());
                entryIssue.setIdTask(issueData.getIdTask());
                entryIssue.setIdMember(issueData.getIdMember());
                entryIssue.setLastUpdate(issueData.getLastUpdate());
                entryIssue.setLastUpdateWho(issueData.getLastUpdateWho());
                entryIssue.setFlag("Y");
                entryIssue.setAction("C");
                try{
                    projectIssueDao.addAndSave(entryIssue);
                } catch (HibernateException e){
                    logger.error("[ProjectBoImpl.saveProjectTask] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                }
            }
        } if (listOfTaskMember != null){
            for (TaskMember dataTaskMember : listOfTaskMember){
                ItApbTaskMemberEntity taskMember = new ItApbTaskMemberEntity();
                taskMember.setIdTaskMember(dataTaskMember.getIdTaskMember());
                taskMember.setIdMember(dataTaskMember.getIdMember());
                taskMember.setIdTask(dataTaskMember.getIdTask());
                taskMember.setPosition(dataTaskMember.getPosition());
                taskMember.setCreatedDate(project.getLastUpdate());
                taskMember.setCreatedWho(project.getLastUpdateWho());
                taskMember.setLastUpdate(project.getLastUpdate());
                taskMember.setLastUpdateWho(project.getLastUpdateWho());
                taskMember.setStatus(dataTaskMember.getStatus());
                taskMember.setProgres(10);
                taskMember.getNote();
                taskMember.setFlag("Y");
                taskMember.setAction("C");
                try{
                    taskMemberDao.addAndSave(taskMember);
                }catch (HibernateException e){
                    logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                }
            }
        }

    }

    @Override
    public void saveEditProject(Project project, List<Member> listOfProjectMember, List<Task> listOfProjectTask, List<ProjectIssue> listOfProjectIssue, List<Document> listOfDocument,List<TaskMember> listOfTaskMember) {
        if(project != null){
            ItApbProjectEntity updateProject = new ItApbProjectEntity();
            updateProject.setIdProject(project.getIdProject());
            updateProject.setNamaProject(project.getNamaProject());
            updateProject.setNoWo(project.getNoWo());
            updateProject.setStatus(project.getStatus());
            updateProject.setStartDate(CommonUtil.convertToDate(project.getStStartDate()));
            updateProject.setDeadline(CommonUtil.convertToDate(project.getStDeadline()));
            updateProject.setFlag("Y");
            updateProject.setAction("U");
            updateProject.setIdTypeProject(project.getIdTypeProject());
            updateProject.setProgres(project.getProgres());
            updateProject.setDivisi(project.getIdDivisi());
            if(project.getIdAlatDetail() != null){
                updateProject.setIdAlatDetail(project.getIdAlatDetail());
            }
            updateProject.setLastUpdate(project.getLastUpdate());
            updateProject.setLastUpdateWho(project.getLastUpdateWho());
            try{
                projectDao.updateAndSave(updateProject);
            }catch (HibernateException e){
                logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
            }
        }
        if(listOfProjectMember != null){
            for(Member memberData: listOfProjectMember){
                ItApbProjectMemberEntity updateMember = new ItApbProjectMemberEntity();
                updateMember.setIdProject(project.getIdProject());
                updateMember.setIdProjectMember(memberData.getIdProjectMember());
                updateMember.setIdMember(memberData.getIdMember());
                updateMember.setPositionId(Long.parseLong(memberData.getStPositionId()));
                updateMember.setLastUpdate(project.getLastUpdate());
                updateMember.setLastUpdateWho(project.getLastUpdateWho());
                updateMember.setFlag(memberData.getFlag());
                updateMember.setAction(memberData.getAction());
//                updateMember.setAction("U");
                if(memberData.getRoleId() != null){
                    updateMember.setRoleId(memberData.getRoleId());
                }
                if(memberData.isAdded()!=true){
                    try{
                        projectMemberDao.updateAndSave(updateMember);
                    }catch (HibernateException e){
                        logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                    }
                } else {
                    try{
                        projectMemberDao.addAndSave(updateMember);
                    }catch (HibernateException e){
                        logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                    }
                }

            }
        }

        if(listOfProjectTask != null){
            for(Task taskData : listOfProjectTask){
                if(taskData.getEdited() == true){
                    ItApbProjectTaskEntity itApbProjectTaskEntityNew = new ItApbProjectTaskEntity();
                    itApbProjectTaskEntityNew.setIdProject(project.getIdProject());
                    itApbProjectTaskEntityNew.setIdTask(taskData.getIdTask());
                    itApbProjectTaskEntityNew.setNamaTask(taskData.getNamaTask());
                    itApbProjectTaskEntityNew.setAsignTo(taskData.getAsignTo());
                    itApbProjectTaskEntityNew.setDeadline(CommonUtil.convertToDate(taskData.getStDeadline()));
                    itApbProjectTaskEntityNew.setStartDate(CommonUtil.convertToDate(taskData.getStStartDate()));
                    itApbProjectTaskEntityNew.setStatusTask(taskData.getStatusTask());
                    itApbProjectTaskEntityNew.setPriority(taskData.getPriority());
                    itApbProjectTaskEntityNew.setProgres(taskData.getProgres());
                    itApbProjectTaskEntityNew.setNote(taskData.getNote());
                    itApbProjectTaskEntityNew.setFlag(taskData.getFlag());
                    itApbProjectTaskEntityNew.setAction(taskData.getAction());
                    itApbProjectTaskEntityNew.setLastUpdate(project.getLastUpdate());
                    itApbProjectTaskEntityNew.setLastUpdateWho(project.getLastUpdateWho());
                    if (taskData.isAdded() != true){
                        try {
                            projectTaskDao.updateAndSave(itApbProjectTaskEntityNew);
                        } catch (HibernateException e) {
                            logger.error("[ProjectBoImpl.saveTask] Error, " + e.getMessage());
                            throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                        }

                        if (listOfTaskMember!=null){
                            for (TaskMember dataTaskMember : listOfTaskMember){
                                if (dataTaskMember.getIdTask().equals(taskData.getIdTask())){
                                    ItApbTaskMemberEntity taskMember = new ItApbTaskMemberEntity();
                                    taskMember.setIdTaskMember(dataTaskMember.getIdTaskMember());
                                    taskMember.setIdMember(dataTaskMember.getIdMember());
                                    taskMember.setIdTask(dataTaskMember.getIdTask());
                                    taskMember.setPosition(dataTaskMember.getPosition());

                                    taskMember.setLastUpdate(project.getLastUpdate());
                                    taskMember.setLastUpdateWho(project.getLastUpdateWho());
                                    taskMember.setStatus(itApbProjectTaskEntityNew.getStatusTask());
                                    if (taskData.getProgres() != taskData.getProgresBefore()){
                                        taskMember.setProgres(taskData.getProgres());
                                    }else {
                                        taskMember.setProgres(taskMember.getProgres());
                                    }
                                    if (dataTaskMember.isAdded() == true){
                                        taskMember.setCreatedDate(project.getCreatedDate());
                                        taskMember.setCreatedWho(project.getCreatedWho());
                                        taskMember.setFlag("Y");
                                        taskMember.setAction("C");
                                        try{
                                            taskMemberDao.addAndSave(taskMember);
                                        }catch (HibernateException e){
                                            logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                                            throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                                        }
                                    } else {
                                        taskMember.setCreatedDate(dataTaskMember.getCreatedDate());
                                        taskMember.setCreatedWho(dataTaskMember.getCreatedWho());
                                        taskMember.setFlag(dataTaskMember.getFlag());
                                        taskMember.setAction("U");
                                        try{
                                            taskMemberDao.updateAndSave(taskMember);
                                        }catch (HibernateException e){
                                            logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                                            throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                                        }
                                    }
                                    if (dataTaskMember.getMemberBf() != null){
                                        if (dataTaskMember.getIdMember() != dataTaskMember.getMemberBf() ){
                                            String idChange;
                                            idChange = taskMemberChangeDao.getChangeMemberId();
                                            ItApbTaskMemberChangeEntity changeEntity = new ItApbTaskMemberChangeEntity();
                                            changeEntity.setIdTaskMemberChange("CNG"+idChange);
                                            changeEntity.setIdTaskMember(dataTaskMember.getIdTaskMember());
                                            changeEntity.setIdTask(dataTaskMember.getIdTask());
                                            changeEntity.setIdProject(project.getIdProject());
                                            changeEntity.setIdMemberOld(dataTaskMember.getMemberBf());
                                            changeEntity.setIdMemberNew(dataTaskMember.getIdMember());
                                            changeEntity.setProgres(dataTaskMember.getProgres());
                                            changeEntity.setNote("Member Telah Diganti");
                                            changeEntity.setFlag("Y");
                                            changeEntity.setAction("C");
                                            changeEntity.setCreatedDate(dataTaskMember.getLastUpdate());
                                            changeEntity.setCreatedWho(dataTaskMember.getLastUpdateWho());
                                            changeEntity.setLastUpdate(dataTaskMember.getLastUpdate());
                                            changeEntity.setLastUpdateWho(dataTaskMember.getLastUpdateWho());
                                            try {
                                                taskMemberChangeDao.addAndSave(changeEntity);
                                            } catch (HibernateException e) {
                                                logger.error("[ProjectBoImpl.saveTask] Error, " + e.getMessage());
                                                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                                            }
                                        }
                                    }

                                }
                            }
                        }

                    } else {
                        try {
                            projectTaskDao.addAndSave(itApbProjectTaskEntityNew);
                        } catch (HibernateException e) {
                            logger.error("[ProjectBoImpl.saveTask] Error, " + e.getMessage());
                            throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                        }

                        if (listOfTaskMember!=null){
                            for (TaskMember dataTaskMember : listOfTaskMember) {
                                if (dataTaskMember.isAdded() == true){
                                    if (dataTaskMember.getIdTask().equals(taskData.getIdTask())){
                                        ItApbTaskMemberEntity taskMember = new ItApbTaskMemberEntity();
                                        taskMember.setIdTaskMember(dataTaskMember.getIdTaskMember());
                                        taskMember.setIdMember(dataTaskMember.getIdMember());
                                        taskMember.setIdTask(taskData.getIdTask());
                                        taskMember.setPosition(dataTaskMember.getPosition());
                                        taskMember.setCreatedDate(project.getLastUpdate());
                                        taskMember.setCreatedWho(project.getLastUpdateWho());
                                        taskMember.setLastUpdate(project.getLastUpdate());
                                        taskMember.setLastUpdateWho(project.getLastUpdateWho());
                                        taskMember.setStatus(taskData.getStatusTask());
                                        taskMember.setProgres(taskData.getProgres());
                                        taskMember.setNote(dataTaskMember.getNote());
                                        taskMember.setFlag("Y");
                                        taskMember.setAction("C");
                                        try {
                                            taskMemberDao.addAndSave(taskMember);
                                            dataTaskMember.setSaved(true);
                                        } catch (HibernateException e) {
                                            logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                                            throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                                        }
                                        if (dataTaskMember.getMemberBf() != null){
                                            if (dataTaskMember.getIdMember() != dataTaskMember.getMemberBf() && dataTaskMember.getMemberBf() != null){
                                                String idChange;
                                                idChange = taskMemberChangeDao.getChangeMemberId();
                                                ItApbTaskMemberChangeEntity changeEntity = new ItApbTaskMemberChangeEntity();
                                                changeEntity.setIdTaskMemberChange("CNG"+idChange);
                                                changeEntity.setIdTaskMember(dataTaskMember.getIdTaskMember());
                                                changeEntity.setIdTask(dataTaskMember.getIdTask());
                                                changeEntity.setIdProject(project.getIdProject());
                                                changeEntity.setIdMemberOld(dataTaskMember.getMemberBf());
                                                changeEntity.setIdMemberNew(dataTaskMember.getIdMember());
                                                changeEntity.setProgres(dataTaskMember.getProgres());
                                                changeEntity.setNote("Member Telah Diganti");
                                                changeEntity.setFlag("Y");
                                                changeEntity.setAction("C");
                                                changeEntity.setCreatedDate(dataTaskMember.getLastUpdate());
                                                changeEntity.setCreatedWho(dataTaskMember.getLastUpdateWho());
                                                changeEntity.setLastUpdate(dataTaskMember.getLastUpdate());
                                                changeEntity.setLastUpdateWho(dataTaskMember.getLastUpdateWho());
                                                try {
                                                    taskMemberChangeDao.addAndSave(changeEntity);
                                                } catch (HibernateException e) {
                                                    logger.error("[ProjectBoImpl.saveTask] Error, " + e.getMessage());
                                                    throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                    ItApbProjectTaskLogEntity itApbProjectTaskLogEntityNew  = new ItApbProjectTaskLogEntity();

                    String seqId;
                    seqId = projectTaskLogDao.getNextTaskLogId();
                    itApbProjectTaskLogEntityNew.setIdTaskLog("TL"+seqId);
                    itApbProjectTaskLogEntityNew.setIdTask(itApbProjectTaskEntityNew.getIdTask());
                    itApbProjectTaskLogEntityNew.setNamaTask(itApbProjectTaskEntityNew.getNamaTask());
                    itApbProjectTaskLogEntityNew.setAsignTo(itApbProjectTaskEntityNew.getAsignTo());
                    itApbProjectTaskLogEntityNew.setDeadline(itApbProjectTaskEntityNew.getDeadline());
                    itApbProjectTaskLogEntityNew.setStartDate(itApbProjectTaskEntityNew.getStartDate());
                    itApbProjectTaskLogEntityNew.setPriority(itApbProjectTaskEntityNew.getPriority());
                    itApbProjectTaskLogEntityNew.setProgres(itApbProjectTaskEntityNew.getProgres());
                    itApbProjectTaskLogEntityNew.setNote(itApbProjectTaskEntityNew.getNote());
                    itApbProjectTaskLogEntityNew.setStatusTask(itApbProjectTaskEntityNew.getStatusTask());
                    itApbProjectTaskLogEntityNew.setLastUpdate(itApbProjectTaskEntityNew.getLastUpdate());
                    itApbProjectTaskLogEntityNew.setLastUpdateWho(itApbProjectTaskEntityNew.getLastUpdateWho());
                    itApbProjectTaskLogEntityNew.setProgres(itApbProjectTaskEntityNew.getProgres());

                    String status = taskData.getStatusTask();
                    String statusBf = taskData.getStatusBefore();
                    String taskId = taskData.getIdTask();
                    List<TaskLog> taskLogList = new ArrayList<TaskLog>();
                    taskLogList = projectTaskLogDao.getLastStatusUpdate(taskId);
                    if (!status.equals(statusBf) ){
                        itApbProjectTaskLogEntityNew.setLastStatusUpdate(itApbProjectTaskEntityNew.getLastUpdate());
                    } else {
                        if (taskLogList != null){
                            for (TaskLog dataTaskLog : taskLogList){
                                itApbProjectTaskLogEntityNew.setLastStatusUpdate(dataTaskLog.getLastStatusUpdate());
                            }
                        }
                    }

                    if (itApbProjectTaskLogEntityNew.getLastStatusUpdate() == null){
                        itApbProjectTaskLogEntityNew.setLastStatusUpdate(itApbProjectTaskEntityNew.getLastUpdate());
                    }

                    itApbProjectTaskLogEntityNew.setFlag("Y");
                    itApbProjectTaskLogEntityNew.setAction("C");

                    try {
                        projectTaskLogDao.addAndSave(itApbProjectTaskLogEntityNew);
                    } catch (HibernateException e) {
                        logger.error("[ProjectBoImpl.saveTask] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                    }

                }
                else {
                    if (listOfTaskMember!=null){
                        for (TaskMember dataTaskMember : listOfTaskMember) {
                            if (dataTaskMember.getIdTask().equals(taskData.getIdTask())){
                                ItApbTaskMemberEntity taskMember = new ItApbTaskMemberEntity();
                                taskMember.setIdTaskMember(dataTaskMember.getIdTaskMember());
                                taskMember.setIdMember(dataTaskMember.getIdMember());
                                taskMember.setIdTask(dataTaskMember.getIdTask());
                                taskMember.setPosition(dataTaskMember.getPosition());
                                taskMember.setCreatedDate(dataTaskMember.getCreatedDate());
                                taskMember.setCreatedWho(dataTaskMember.getCreatedWho());
                                taskMember.setLastUpdate(project.getLastUpdate());
                                taskMember.setLastUpdateWho(project.getLastUpdateWho());
                                taskMember.setStatus(dataTaskMember.getStatus());
                                taskMember.setProgres(dataTaskMember.getProgres());
                                taskMember.setNote(dataTaskMember.getNote());
                                    if (dataTaskMember.isAdded() == true){
                                        taskMember.setFlag("Y");
                                        taskMember.setAction("C");
                                        try{
                                            taskMemberDao.addAndSave(taskMember);
                                        }catch (HibernateException e){
                                            logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                                            throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                                        }
                                    } else {
                                        taskMember.setFlag(dataTaskMember.getFlag());
                                        taskMember.setAction("U");
                                        try{
                                            taskMemberDao.updateAndSave(taskMember);
                                        }catch (HibernateException e){
                                            logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                                            throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                                        }
                                    }
                                if (dataTaskMember.getMemberBf() != null){
                                    if (dataTaskMember.getIdMember() != dataTaskMember.getMemberBf()){
                                        String idChange;
                                        idChange = taskMemberChangeDao.getChangeMemberId();
                                        ItApbTaskMemberChangeEntity changeEntity = new ItApbTaskMemberChangeEntity();
                                        changeEntity.setIdTaskMemberChange("CNG"+idChange);
                                        changeEntity.setIdTaskMember(dataTaskMember.getIdTaskMember());
                                        changeEntity.setIdTask(dataTaskMember.getIdTask());
                                        changeEntity.setIdProject(project.getIdProject());
                                        changeEntity.setIdMemberOld(dataTaskMember.getMemberBf());
                                        changeEntity.setIdMemberNew(dataTaskMember.getIdMember());
                                        changeEntity.setProgres(dataTaskMember.getProgres());
                                        changeEntity.setNote("Member Telah Diganti");
                                        changeEntity.setFlag("Y");
                                        changeEntity.setAction("C");
                                        changeEntity.setCreatedDate(dataTaskMember.getLastUpdate());
                                        changeEntity.setCreatedWho(dataTaskMember.getLastUpdateWho());
                                        changeEntity.setLastUpdate(dataTaskMember.getLastUpdate());
                                        changeEntity.setLastUpdateWho(dataTaskMember.getLastUpdateWho());
                                        try {
                                            taskMemberChangeDao.addAndSave(changeEntity);
                                        } catch (HibernateException e) {
                                            logger.error("[ProjectBoImpl.saveTask] Error, " + e.getMessage());
                                            throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
//        updateProgresListTask();
        if(listOfProjectIssue != null){
            for(ProjectIssue dataIssue : listOfProjectIssue){
                ItApbProjectIssueEntity updateIssue = new ItApbProjectIssueEntity();
                updateIssue.setIdIssue(dataIssue.getIdIssue());
                updateIssue.setNamaIssue(dataIssue.getNamaIssue());
                updateIssue.setIdProject(project.getIdProject());
                updateIssue.setIdTask(dataIssue.getIdTask());
                updateIssue.setIdMember(dataIssue.getIdMember());
                updateIssue.setLastUpdate(dataIssue.getLastUpdate());
                updateIssue.setLastUpdateWho(dataIssue.getLastUpdateWho());
                updateIssue.setFlag(dataIssue.getFlag());
                updateIssue.setAction(dataIssue.getAction());

                if (dataIssue.isAdded() != true){
                    try{
                        projectIssueDao.updateAndSave(updateIssue);
                    }catch (HibernateException e){
                        logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                    }
                } else {
                    try{
                        projectIssueDao.addAndSave(updateIssue);
                    }catch (HibernateException e){
                        logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                    }
                }
            }
        }

//        if(listOfDocument != null){
//            for(Document dataDoc :listOfDocument){
//                ItApbProjectDocEntity updateDocument = new ItApbProjectDocEntity();
//
//                    updateDocument.setIdProject(dataDoc.getIdProject());
//                    updateDocument.setIdDoc(dataDoc.getIdDoc());
//                    updateDocument.setNamaDoc(dataDoc.getNamaDoc());
//                    updateDocument.setLastUpdate(dataDoc.getLastUpdate());
//                    updateDocument.setUploadBy(dataDoc.getUploadBy());
//                    if(dataDoc.getIdTask() != null) {
//                        updateDocument.setIdTask(dataDoc.getIdTask());
//                    }
//                    updateDocument.setFlag("Y");
//                    updateDocument.setAction("U");
//                    try{
//                        docDao.updateAndSave(updateDocument);
//                    }catch (HibernateException e){
//                        logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
//                        throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
//                    }
//                }
//            }
    }

    @Override
    public void updateStatusProject(Project dataTask) {

        List<Task> listSumAndCountTask = new ArrayList<Task>();
        listSumAndCountTask = projectDao.getSumAndCountTask(dataTask.getIdProject());
        BigInteger countRow = null;
        BigInteger sumProgres = null;
        if (listSumAndCountTask != null){
            for (Task sumCount : listSumAndCountTask){
                countRow = sumCount.getCountOfRow();
                sumProgres = sumCount.getSumProgres();
            }
        }

        Double dCount = countRow.doubleValue();
        Double dSum = sumProgres.doubleValue();
        List<Project> listOfProject = new ArrayList<Project>();

        Map hsCriteria = new HashMap();
        if (dataTask.getIdProject() != null && !"".equalsIgnoreCase(dataTask.getIdProject())) {
            hsCriteria.put("project_id", dataTask.getIdProject());
        }

        listOfProject = projectDao.getSearchByCriteria(hsCriteria);
        Double jml = dSum/dCount;

        if (listOfProject != null){
            for (Project dataProject : listOfProject){
                ItApbProjectEntity entityProject = new ItApbProjectEntity();
                entityProject.setIdProject(dataProject.getIdProject());
                entityProject.setNamaProject(dataProject.getNamaProject());

                Date projectDeadline = dataProject.getDeadline();
                Date startDate = dataProject.getStartDate();
                java.util.Date utilDate = new java.util.Date();
                Date today = new Date(utilDate.getTime());

                if(today.before(startDate) && jml < 100){
                    dataProject.setStatus("4");
                }else if(today.after(projectDeadline) && jml < 100){
                    dataProject.setStatus("3");
                } else if(today.after(startDate) && today.before(projectDeadline) && "1".equalsIgnoreCase(dataProject.getStatus()) && jml < 100) {
                    dataProject.setStatus("1");
                } else if(jml == 100){
                    dataProject.setStatus("2");
                }
                entityProject.setProgres(jml.intValue());
                entityProject.setStartDate(dataProject.getStartDate());
                entityProject.setDeadline(dataProject.getDeadline());
                entityProject.setFlag("Y");
                entityProject.setAction("U");
                entityProject.setLastUpdate(dataProject.getLastUpdate());
                entityProject.setLastUpdateWho(dataProject.getLastUpdateWho());
                entityProject.setIdTypeProject(dataProject.getIdTypeProject());
                entityProject.setIdAlatDetail(dataProject.getIdAlatDetail());
                entityProject.setNoWo(dataProject.getNoWo());
                entityProject.setProgres(jml.intValue());
                entityProject.setDivisi(dataProject.getIdDivisi());
                try {
                    projectDao.updateAndSave(entityProject);
                } catch (HibernateException e) {
                    logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                }
            }
        }
    }


    @Override
    public void saveDeleteProject(Project project, List<Member> listOfProjectMember, List<Task> listOfProjectTask, List<ProjectIssue> listOfProjectIssue, List<Document> listOfDocument) {
        if(project != null){
            ItApbProjectEntity updateProject = new ItApbProjectEntity();
            updateProject.setIdProject(project.getIdProject());
            updateProject.setNamaProject(project.getNamaProject());
            updateProject.setNoWo(project.getNoWo());
            updateProject.setStatus(project.getStatus());
            updateProject.setStartDate(CommonUtil.convertToDate(project.getStStartDate()));
            updateProject.setDeadline(CommonUtil.convertToDate(project.getStDeadline()));
            updateProject.setProgres(updateProject.getProgres());
            updateProject.setFlag("N");
            updateProject.setAction("U");
            updateProject.setIdTypeProject(project.getIdTypeProject());
            try{
                projectDao.updateAndSave(updateProject);
            }catch (HibernateException e){
                logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
            }
        }
        if(listOfProjectIssue != null){
            for(ProjectIssue dataIssue : listOfProjectIssue){
                ItApbProjectIssueEntity updateIssue = new ItApbProjectIssueEntity();
                    updateIssue.setIdIssue(dataIssue.getIdIssue());
                    updateIssue.setNamaIssue(dataIssue.getNamaIssue());
                    updateIssue.setIdProject(dataIssue.getIdProject());
                    updateIssue.setIdTask(dataIssue.getIdTask());
                    updateIssue.setIdMember(dataIssue.getIdMember());
                    updateIssue.setFlag("N");
                    updateIssue.setAction("U");
                    try{
                        projectIssueDao.updateAndSave(updateIssue);
                    }catch (HibernateException e){
                        logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                    }
                }
            }
        if(listOfDocument != null){
            for(Document dataDoc :listOfDocument){
                ItApbProjectDocEntity updateDocument = new ItApbProjectDocEntity();
                    updateDocument.setIdProject(dataDoc.getIdProject());
                    updateDocument.setIdDoc(dataDoc.getIdDoc());
                    updateDocument.setNamaDoc(dataDoc.getNamaDoc());
                    updateDocument.setIdTask(dataDoc.getIdTask());
                    updateDocument.setFlag("N");
                    updateDocument.setAction("U");
                    try{
                        docDao.updateAndSave(updateDocument);
                    }catch (HibernateException e){
                        logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                    }
                }
            }
        if(listOfProjectMember != null){
            for(Member dataMember : listOfProjectMember){
                ItApbProjectMemberEntity updateMember = new ItApbProjectMemberEntity();
                updateMember.setIdProjectMember(dataMember.getIdProjectMember());
                updateMember.setIdMember(dataMember.getIdMember());
                updateMember.setIdProject(dataMember.getIdProject());
                updateMember.setPositionId(dataMember.getPositionId());
                updateMember.setFlag("N");
                updateMember.setAction("U");
                try{
                    projectMemberDao.updateAndSave(updateMember);
                }catch (HibernateException e){
                    logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                }
            }
        }
        if(listOfProjectTask != null){
            for(Task dataTask : listOfProjectTask){
                ItApbProjectTaskEntity updateTask = new ItApbProjectTaskEntity();
                updateTask.setIdProject(dataTask.getIdProject());
                updateTask.setIdTask(dataTask.getIdTask());
                updateTask.setNamaTask(dataTask.getNamaTask());
                updateTask.setAsignTo(dataTask.getAsignTo());
                updateTask.setStartDate(CommonUtil.convertToDate(dataTask.getStStartDate()));
                updateTask.setDeadline(CommonUtil.convertToDate(dataTask.getStDeadline()));
                updateTask.setStatusTask(dataTask.getStatusTask());
                updateTask.setNote(dataTask.getNote());
                updateTask.setPriority(dataTask.getPriority());
                updateTask.setProgres(dataTask.getProgres());
                updateTask.setFlag("N");
                updateTask.setAction("U");
                try{
                    projectTaskDao.updateAndSave(updateTask);
                }catch (HibernateException e){
                    logger.error("[ProjectBoImpl.saveEditProject] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void saveProjectTask(Project project, List<Project> listOfProjectTask) {
        String projectId = project.getIdProject();
        if(projectId!=null){
            for (Project addProjectTask : listOfProjectTask){
                ItApbProjectTaskEntity itApbProjectTaskEntityNew = new ItApbProjectTaskEntity();
                itApbProjectTaskEntityNew.setIdProject(projectId);
                itApbProjectTaskEntityNew.setIdTask(addProjectTask.getIdTask());
                itApbProjectTaskEntityNew.setNamaTask(addProjectTask.getNamaTask());
                itApbProjectTaskEntityNew.setAsignTo(addProjectTask.getAsignTo());
                itApbProjectTaskEntityNew.setStatusTask(addProjectTask.getTaskStatus());
                itApbProjectTaskEntityNew.setDeadline(CommonUtil.convertToDate(addProjectTask.getTaskDeadline()));
                itApbProjectTaskEntityNew.setPriority(addProjectTask.getPriority());
                itApbProjectTaskEntityNew.setNote(addProjectTask.getTaskNote());
                itApbProjectTaskEntityNew.setProgres(addProjectTask.getTaskProgres());
                try{
                    projectTaskDao.addAndSave(itApbProjectTaskEntityNew);
                }catch (HibernateException e){
                    logger.error("[ProjectBoImpl.saveProjectTask] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data permohonan lahan petani, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    public List<Member> getSearchMemberByCriteria (String idProject) throws GeneralBOException {
        logger.info("[ProjectBoImpl.getSearchMemberByCriteria] start process >>>");

        List<Member> listOfResult = new ArrayList();

        Map hsCriteria = new HashMap();

        if (idProject != null && !"".equalsIgnoreCase(idProject)) {
            hsCriteria.put("project_id",idProject);
        }


        List<Project> listOfProjectDetail = null;

        try {
            listOfResult = projectDao.getMemberByCriteria(hsCriteria);
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.getSearchMemberByCriteria] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
        }

        logger.info("[ProjectBoImpl.getSearchMemberByCriteria] end process <<<");

        return listOfResult;

    }


        @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }

    @Override
    public void saveDelete(Project bean) throws GeneralBOException {

    }
    @Override
    public void saveEdit(Project bean) throws GeneralBOException {

    }

    @Override
    public Project saveAdd(Project bean) throws GeneralBOException {
        return null;
    }

    @Override
    public List<Project> getByCriteria(Project searchBean) throws GeneralBOException {
        return null;
    }

    @Override
    public List<Project> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public void saveIssue(Project issueBean) throws GeneralBOException {

        logger.info("[ProjectBoImpl.saveIssue] start process >>>");

        if (issueBean !=null) {

            String issueId;

            try {
                issueId = projectIssueDao.getNextIssueId();
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveIssue] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when getting sequence status id, please info to your admin..." + e.getMessage());
            }

//            ItApbProjectEntity itApbProjectEntityNew = new ItApbProjectEntity();
            ItApbProjectIssueEntity itApbProjectIssueEntityNew = new ItApbProjectIssueEntity();

            //insert table it_apb_issue

            itApbProjectIssueEntityNew.setIdProject(issueBean.getIdProject());
            itApbProjectIssueEntityNew.setIdIssue("ISS"+issueId);
            itApbProjectIssueEntityNew.setNamaIssue(issueBean.getNamaIssue());
            itApbProjectIssueEntityNew.setIdMember(issueBean.getIdMember());
            itApbProjectIssueEntityNew.setIdTask(issueBean.getIdTask());
            itApbProjectIssueEntityNew.setLastUpdate(issueBean.getLastUpdate());
            itApbProjectIssueEntityNew.setLastUpdateWho(issueBean.getLastUpdateWho());
            itApbProjectIssueEntityNew.setFlag(issueBean.getFlag());
            itApbProjectIssueEntityNew.setAction(issueBean.getAction());

            try {
                projectIssueDao.addAndSave(itApbProjectIssueEntityNew);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveIssue] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }

        }

        logger.info("[ProjectBoImpl.saveIssue] end process <<<");

    }

    @Override
    public void updateStatusByDate() throws GeneralBOException {
        List<ItApbProjectEntity> listOfResult = new ArrayList<ItApbProjectEntity>();
        List<Project> lisOfProject = new ArrayList<Project>();
        Map hsCriteria = new HashMap();
        List<ItApbProjectEntity> itApbProjectEntities = null;
        lisOfProject = projectDao.getSearchByCriteria(hsCriteria);
        if(lisOfProject != null){
            ItApbProjectEntity projectEntity;
            for(Project listProject : lisOfProject){
                projectEntity = new ItApbProjectEntity();
                projectEntity.setIdProject(listProject.getIdProject());

                List<Task> tasks = new ArrayList<Task>();
                tasks = projectDao.getSumAndCountTask(listProject.getIdProject());
                if (tasks != null){
                    BigInteger bcount = null;
                    BigInteger bsum = null;
                    for (Task dataTask : tasks){
                         bcount = dataTask.getCountOfRow();
                         bsum = dataTask.getSumProgres();
                    }
                    Double dcount = bcount.doubleValue();
                    Double dsum = bsum.doubleValue();
                    Double sum = dsum/dcount;
                    projectEntity.setProgres(sum.intValue());
                } else {
                    projectEntity.setProgres(listProject.getProgres());
                }


                projectEntity.setNamaProject(listProject.getNamaProject());
                projectEntity.setStartDate(listProject.getStartDate());
                projectEntity.setDeadline(listProject.getDeadline());
                projectEntity.setNoWo(listProject.getNoWo());
                projectEntity.setIdTypeProject(listProject.getIdTypeProject());
                projectEntity.setFlag(listProject.getFlag());
                projectEntity.setAction(listProject.getAction());
                projectEntity.setLastUpdate(listProject.getLastUpdate());
                projectEntity.setLastUpdateWho(listProject.getLastUpdateWho());
                projectEntity.setIdAlatDetail(listProject.getIdAlatDetail());
                projectEntity.setDivisi(listProject.getIdDivisi());

                Date projectDeadline = listProject.getDeadline();
                Date startDate = listProject.getStartDate();
                java.util.Date utilDate = new java.util.Date();
                Date today = new Date(utilDate.getTime());

                Integer jml = listProject.getProgres();

                if(today.before(startDate) && jml < 100){
                    projectEntity.setStatus("4");
                }else if(today.after(projectDeadline)&& jml < 100){
                    projectEntity.setStatus("3");
                } else if(today.after(startDate) && today.before(projectDeadline) && jml < 100) {
                    projectEntity.setStatus("1");
                } else if(jml == 100){
                    projectEntity.setStatus("2");
                } else {
                    projectEntity.setStatus("4");
                }
                projectDao.updateAndSave(projectEntity);
            }
        }
    }

    @Override
    public void updateProgresListTask(String idProject) throws GeneralBOException {
        logger.info("[ProjectBoImpl.getSearchTaskByCriteria] start process >>>");

        List<Task> listOfResult = new ArrayList<Task>();


            Map hsCriteria = new HashMap();
//            hsCriteria.put("flag", "Y");
            hsCriteria.put("project_id", idProject);
            try {

                listOfResult = projectDao.getTaskByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.getSearchTaskByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if (listOfResult != null) {
                for (Task dataTask : listOfResult) {
                    if ("Y".equals(dataTask.getIsTask())) {
                        TaskMember taskMemberData = new TaskMember();
                        taskMemberData.setIdTask(dataTask.getIdTask());

                        List<TaskMember> listOfTaskMember = new ArrayList<TaskMember>();
                        listOfTaskMember = taskMemberDao.getCountAndSumTask(taskMemberData);
                        if (listOfTaskMember != null) {
                            for (TaskMember dataList : listOfTaskMember) {
                                dataTask.setSumProgres(dataList.getSumProgres());
                                dataTask.setCountOfRow(dataList.getCountOfRow());
                            }
                        }

                        java.util.Date utilDate = new java.util.Date();
                        Date today = new Date(utilDate.getTime());

                        if (dataTask.getSumProgres() != null && dataTask.getCountOfRow() != null) {
                            Double sum = dataTask.getSumProgres().doubleValue();
                            Double count = dataTask.getCountOfRow().doubleValue();
                            Double jmlh = sum / count;
                            Integer intJumlah = jmlh.intValue();

                            if (intJumlah == 100) {
                                dataTask.setStatusTask("2");
                            } else if (intJumlah > 100 && today.before(dataTask.getDeadline())) {
                                dataTask.setStatusTask("1");
                            } else if (intJumlah > 100 && today.after(dataTask.getDeadline())) {
                                dataTask.setStatusTask("3");
                            }

                            ItApbProjectTaskEntity taskEntity = new ItApbProjectTaskEntity();
                            taskEntity.setIdTask(dataTask.getIdTask());
                            taskEntity.setIdProject(dataTask.getIdProject());
                            taskEntity.setNamaTask(dataTask.getNamaTask());
                            taskEntity.setProgres(intJumlah);
                            taskEntity.setPriority(dataTask.getPriority());
                            taskEntity.setStatusTask(dataTask.getStatusTask());
                            taskEntity.setNote(dataTask.getNote());
                            taskEntity.setStartDate(dataTask.getStartDate());
                            taskEntity.setDeadline(dataTask.getDeadline());
                            taskEntity.setLastUpdate(dataTask.getLastUpdate());
                            taskEntity.setLastUpdateWho(dataTask.getLastUpdateWho());
                            taskEntity.setFlag(dataTask.getFlag());
                            taskEntity.setNote(dataTask.getNote());
                            taskEntity.setAction("U");
                            try {
                                projectTaskDao.updateAndSave(taskEntity);
                            } catch (HibernateException e) {
                                logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
                                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                            }
                        }
                    }
                }
            }
        logger.info("[ProjectBoImpl.getSearchTaskByCriteria] end process <<<");
    }

    @Override
    public String getSeqNumTaskId() throws GeneralBOException {
        logger.info("[PermohonanLahanBoImpl.getSeqNumLahanId] start process >>>");

        String idTask = "";
        try {
            idTask=projectTaskDao.getNextTaskId();
        } catch (HibernateException e) {
            logger.error("[PermohonanLahanBoImpl.getSeqNumLahanId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }

        logger.info("[PermohonanLahanBoImpl.getSeqNumLahanId] end process <<<");

        return idTask;
    }

    @Override
    public String getSeqNumTaskLogId() throws GeneralBOException {
        logger.info("[PermohonanLahanBoImpl.getSeqNumLahanId] start process >>>");

        String idTask = "";
        try {
            idTask=projectTaskLogDao.getNextTaskLogId();
        } catch (HibernateException e) {
            logger.error("[PermohonanLahanBoImpl.getSeqNumLahanId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }

        logger.info("[PermohonanLahanBoImpl.getSeqNumLahanId] end process <<<");

        return idTask;
    }

    @Override
    public String getSeqNumProjectId() throws GeneralBOException {
        String idProject = "";
        try {
            idProject=projectDao.getNextProjectId();
        } catch (HibernateException e) {
            logger.error("[PermohonanLahanBoImpl.getSeqNumLahanId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }

        logger.info("[PermohonanLahanBoImpl.getSeqNumLahanId] end process <<<");

        return idProject;
    }

    @Override
    public String getSeqMemberId() throws GeneralBOException {
        String idMember = "";
        try {
            idMember=projectMemberDao.getNextMemberId();
        } catch (HibernateException e) {
            logger.error("[PermohonanLahanBoImpl.getSeqNumLahanId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }

        logger.info("[PermohonanLahanBoImpl.getSeqNumLahanId] end process <<<");

        return idMember;
    }

    @Override
    public String getSeqDocId() throws GeneralBOException {
        String idDoc = "";
        try {
            idDoc=docDao.getNextDocId();
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.getSeqDocId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }

        logger.info("[ProjectBoImpl.getSeqDocId] end process <<<");

        return idDoc;
    }

    @Override
    public String getSeqIssueId() throws GeneralBOException {
        String idIssue = "";
        try {
            idIssue=projectIssueDao.getNextIssueId();
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.getSeqIssueId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }
        logger.info("[ProjectBoImpl.getSeqIssueId] end process <<<");
        return idIssue;
    }

    @Override
    public String getSeqMemberTaskId() throws GeneralBOException {
        String idMemberTask = "";
        try {
            idMemberTask=taskMemberDao.getMemberTaskId();
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.getSeqIssueId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }
        logger.info("[ProjectBoImpl.getSeqIssueId] end process <<<");
        return idMemberTask;
//        return null;
    }


    @Override
    public void saveTask(Task task, TaskLog taskLog) throws GeneralBOException {
        if(task != null && taskLog != null){
         ItApbProjectTaskEntity itApbProjectTaskEntityNew = new ItApbProjectTaskEntity();
            itApbProjectTaskEntityNew.setIdProject(task.getIdProject());
            itApbProjectTaskEntityNew.setIdTask(task.getIdTask());
            itApbProjectTaskEntityNew.setNamaTask(task.getNamaTask());
            itApbProjectTaskEntityNew.setAsignTo(task.getAsignTo());
            itApbProjectTaskEntityNew.setDeadline(CommonUtil.convertToDate(task.getStDeadline()));
            itApbProjectTaskEntityNew.setStartDate(CommonUtil.convertToDate(task.getStStartDate()));
            if("".equalsIgnoreCase(task.getAsignTo())){
                itApbProjectTaskEntityNew.setStatusTask("4");

            }else {
                itApbProjectTaskEntityNew.setStatusTask("1");
            }
            itApbProjectTaskEntityNew.setPriority(task.getPriority());
            itApbProjectTaskEntityNew.setProgres(10);
            itApbProjectTaskEntityNew.setNote(task.getNote());
            itApbProjectTaskEntityNew.setFlag("Y");
            itApbProjectTaskEntityNew.setAction("C");
            try {
                projectTaskDao.addAndSave(itApbProjectTaskEntityNew);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveTask] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }

            ItApbProjectTaskLogEntity itApbProjectTaskLogEntityNew  = new ItApbProjectTaskLogEntity();
            itApbProjectTaskLogEntityNew.setIdTaskLog(taskLog.getIdTaskLog());
            itApbProjectTaskLogEntityNew.setIdTask(itApbProjectTaskEntityNew.getIdTask());
            itApbProjectTaskLogEntityNew.setNamaTask(itApbProjectTaskEntityNew.getNamaTask());
            itApbProjectTaskLogEntityNew.setAsignTo(itApbProjectTaskEntityNew.getAsignTo());
            itApbProjectTaskLogEntityNew.setDeadline(itApbProjectTaskEntityNew.getDeadline());
            itApbProjectTaskLogEntityNew.setStartDate(itApbProjectTaskEntityNew.getStartDate());
            itApbProjectTaskLogEntityNew.setPriority(itApbProjectTaskEntityNew.getPriority());
            itApbProjectTaskLogEntityNew.setProgres(itApbProjectTaskEntityNew.getProgres());
            itApbProjectTaskLogEntityNew.setNote(itApbProjectTaskEntityNew.getNote());
            itApbProjectTaskLogEntityNew.setStatusTask(itApbProjectTaskEntityNew.getStatusTask());
            itApbProjectTaskLogEntityNew.setFlag("Y");
            itApbProjectTaskLogEntityNew.setAction("C");

            try {
                projectTaskLogDao.addAndSave(itApbProjectTaskLogEntityNew);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveTask] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }
        }
    }

    @Override
    public void deleteTask(Task task) throws GeneralBOException {
        if(task != null){
            ItApbProjectTaskEntity itApbProjectTaskEntityNew = new ItApbProjectTaskEntity();
            itApbProjectTaskEntityNew.setIdProject(task.getIdProject());
            itApbProjectTaskEntityNew.setIdTask(task.getIdTask());
            itApbProjectTaskEntityNew.setNamaTask(task.getNamaTask());
            itApbProjectTaskEntityNew.setAsignTo(task.getAsignTo());
            itApbProjectTaskEntityNew.setDeadline(CommonUtil.convertToDate(task.getStDeadline()));
            itApbProjectTaskEntityNew.setStatusTask(task.getStatusTask());
            itApbProjectTaskEntityNew.setPriority(task.getPriority());
            itApbProjectTaskEntityNew.setProgres(task.getProgres());
            itApbProjectTaskEntityNew.setNote(task.getNote());
            itApbProjectTaskEntityNew.setFlag("N");
            itApbProjectTaskEntityNew.setAction("D");
            try {
                projectTaskDao.updateAndSave(itApbProjectTaskEntityNew);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.deleteTask] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }
        }

    }


    @Override
    public void saveEditTask(Task dataTask) throws GeneralBOException {
        logger.info("[ProjectBoImpl.saveEditTask] start process >>>");
        if (dataTask != null){

            ItApbTaskMemberEntity taskMemberEntity = new ItApbTaskMemberEntity();
            taskMemberEntity.setIdTaskMember(dataTask.getIdTaskMember());
            taskMemberEntity.setIdTask(dataTask.getIdTask());
            taskMemberEntity.setIdMember(dataTask.getIdMember());
            taskMemberEntity.setProgres(dataTask.getProgres());
            taskMemberEntity.setFlag(dataTask.getFlag());
            taskMemberEntity.setAction(dataTask.getAction());
            taskMemberEntity.setCreatedDate(dataTask.getLastUpdate());
            taskMemberEntity.setCreatedWho(dataTask.getLastUpdateWho());
            taskMemberEntity.setStatus(dataTask.getStatusTask());
            taskMemberEntity.setLastUpdate(dataTask.getLastUpdate());
            taskMemberEntity.setLastUpdateWho(dataTask.getLastUpdateWho());
            taskMemberEntity.setPosition(dataTask.getPosition());
            taskMemberEntity.setNote(dataTask.getNote());
            try {
                taskMemberDao.updateAndSave(taskMemberEntity);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }

        }
        logger.info("[ProjectBoImpl.saveEditTask] end process <<<");
    }

    @Override
    public void updateTaskLastProgres(Task dataTask) throws GeneralBOException {

        TaskMember dataTaskMember = new TaskMember();

        dataTaskMember.setIdTask(dataTask.getIdTask());
        List<TaskMember> listOfTaskMember = new ArrayList<TaskMember>();
        listOfTaskMember = taskMemberDao.getCountAndSumTask(dataTaskMember);
        if (listOfTaskMember != null){
            for (TaskMember dataList : listOfTaskMember){
                dataTask.setSumProgres(dataList.getSumProgres());
                dataTask.setCountOfRow(dataList.getCountOfRow());
            }
        }

        java.util.Date utilDate = new java.util.Date();
        Date today = new Date(utilDate.getTime());

        if (dataTask.getSumProgres() != null && dataTask.getCountOfRow() != null){
            Double sum = dataTask.getSumProgres().doubleValue();
            Double count = dataTask.getCountOfRow().doubleValue();
            Double jmlh = sum/count;
            Integer intJumlah = jmlh.intValue();

            if (intJumlah == 100){
                dataTask.setStatusTask("2");
            }else if (intJumlah > 100 && today.before(dataTask.getDeadline())){
                dataTask.setStatusTask("1");
            }else if (intJumlah > 100 && today.after(dataTask.getDeadline())){
                dataTask.setStatusTask("3");
            }

            ItApbProjectTaskEntity taskEntity = new ItApbProjectTaskEntity();
            taskEntity.setIdTask(dataTask.getIdTask());
            taskEntity.setIdProject(dataTask.getIdProject());
            taskEntity.setNamaTask(dataTask.getNamaTask());
            taskEntity.setProgres(intJumlah);
            taskEntity.setPriority(dataTask.getPriority());
            taskEntity.setStatusTask(dataTask.getStatusTask());
            taskEntity.setNote(dataTask.getNote());
            taskEntity.setStartDate(dataTask.getStartDate());
            taskEntity.setDeadline(dataTask.getDeadline());
            taskEntity.setLastUpdate(dataTask.getLastUpdate());
            taskEntity.setLastUpdateWho(dataTask.getLastUpdateWho());
            taskEntity.setFlag(dataTask.getFlag());
            taskEntity.setNote(dataTask.getNote());
            taskEntity.setAction("U");
            try {
                projectTaskDao.updateAndSave(taskEntity);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }
        }

        String idLog;
        try {
            idLog = projectTaskLogDao.getNextTaskLogId();
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
        }

        ItApbProjectTaskLogEntity logEntity = new ItApbProjectTaskLogEntity();
        logEntity.setIdTaskLog("TL"+idLog);
        logEntity.setIdTask(dataTask.getIdTask());
        logEntity.setAsignTo(dataTask.getIdMember());
        logEntity.setDeadline(dataTask.getDeadline());
        logEntity.setAction("C");
        logEntity.setFlag("Y");
        logEntity.setNamaTask(dataTask.getNamaTask());
        logEntity.setNote(dataTask.getNote());
        logEntity.setPriority(dataTask.getPriority());
        logEntity.setProgres(dataTask.getProgres());
        logEntity.setStartDate(dataTask.getStartDate());
        logEntity.setStatusTask(dataTask.getStatusTask());
        logEntity.setLastUpdate(dataTask.getLastUpdate());
        logEntity.setLastUpdateWho(dataTask.getLastUpdateWho());

        String status = dataTask.getStatusTask();
        String statusBf = dataTask.getStatusBefore();
        String taskId = dataTask.getIdTask();
        List<TaskLog> taskLogList = new ArrayList<TaskLog>();
        taskLogList = projectTaskLogDao.getLastStatusUpdate(taskId);
        if (!status.equals(statusBf) ){
            logEntity.setLastStatusUpdate(dataTask.getLastUpdate());
        } else {
            if (taskLogList != null){
                for (TaskLog dataTaskLog : taskLogList){
                    logEntity.setLastStatusUpdate(dataTaskLog.getLastStatusUpdate());
                }
            }
        }

        if (logEntity.getLastStatusUpdate() == null){
            logEntity.setLastStatusUpdate(dataTask.getLastUpdate());
        }

        try {
            projectTaskLogDao.addAndSave(logEntity);
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
        }



        List<Task> listSumAndCountTask = new ArrayList<Task>();
        listSumAndCountTask = projectDao.getSumAndCountTask(dataTask.getIdProject());
        BigInteger countRow = null;
        BigInteger sumProgres = null;
        if (listSumAndCountTask != null){
            for (Task sumCount : listSumAndCountTask){
                countRow = sumCount.getCountOfRow();
                sumProgres = sumCount.getSumProgres();
            }
        }

        Double dCount = countRow.doubleValue();
        Double dSum = sumProgres.doubleValue();
        Double jml = dSum/dCount;
        List<Project> listOfProject = new ArrayList<Project>();

        Map hsCriteria = new HashMap();
        if (dataTask.getIdProject() != null && !"".equalsIgnoreCase(dataTask.getIdProject())) {
            hsCriteria.put("project_id", dataTask.getIdProject());
        }

        listOfProject = projectDao.getSearchByCriteria(hsCriteria);

        if (listOfProject != null){
            for (Project dataProject : listOfProject){
                ItApbProjectEntity entityProject = new ItApbProjectEntity();
                entityProject.setIdProject(dataProject.getIdProject());
                entityProject.setNamaProject(dataProject.getNamaProject());
                if (jml == 100){
                    entityProject.setProgres(jml.intValue());
                    entityProject.setStatus("2");
                }else {
                    if(today.before(dataProject.getStartDate())){
                        entityProject.setStatus("4");
                    }else if(today.after(dataProject.getDeadline())){
                        entityProject.setStatus("3");
                    } else if(today.after(dataProject.getStartDate()) && today.before(dataProject.getDeadline())) {
                        entityProject.setStatus("1");
                    }
                    entityProject.setProgres(jml.intValue());
                }

                entityProject.setStartDate(dataProject.getStartDate());
                entityProject.setDeadline(dataProject.getDeadline());
                entityProject.setFlag("Y");
                entityProject.setAction("U");
                entityProject.setLastUpdate(dataProject.getLastUpdate());
                entityProject.setLastUpdateWho(dataProject.getLastUpdateWho());
                entityProject.setIdTypeProject(dataProject.getIdTypeProject());
                entityProject.setIdAlatDetail(dataProject.getIdAlatDetail());
                entityProject.setNoWo(dataProject.getNoWo());
                entityProject.setDivisi(dataProject.getIdDivisi());
                try {
                    projectDao.updateAndSave(entityProject);
                } catch (HibernateException e) {
                    logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                }
            }
        }
    }

    @Override
    public void saveEditTaskPIC(Task dataTask) throws GeneralBOException {
        if (dataTask!=null){

            ItApbProjectTaskEntity taskEntity = new ItApbProjectTaskEntity();
            taskEntity.setIdTask(dataTask.getIdTask());
            taskEntity.setIdProject(dataTask.getIdProject());
            taskEntity.setNamaTask(dataTask.getNamaTask());
            taskEntity.setProgres(dataTask.getProgres());
            taskEntity.setPriority(dataTask.getPriority());
            taskEntity.setStatusTask(dataTask.getStatusTask());
            taskEntity.setNote(dataTask.getNote());
            taskEntity.setStartDate(dataTask.getStartDate());
            taskEntity.setDeadline(dataTask.getDeadline());
            taskEntity.setLastUpdate(dataTask.getLastUpdate());
            taskEntity.setLastUpdateWho(dataTask.getLastUpdateWho());
            taskEntity.setFlag(dataTask.getFlag());
            taskEntity.setAction("U");
            try {
                projectTaskDao.updateAndSave(taskEntity);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }


            TaskMember dataTaskMember = new TaskMember();
            dataTaskMember.setIdTask(dataTask.getIdTask());
            List<TaskMember> listOfTaskMember = new ArrayList<TaskMember>();
            listOfTaskMember = taskMemberDao.getCountAndSumTask(dataTaskMember);
            if (listOfTaskMember != null){
                for (TaskMember dataList : listOfTaskMember){
                    dataTask.setSumProgres(dataList.getSumProgres());
                    dataTask.setCountOfRow(dataList.getCountOfRow());
                }
            }

            java.util.Date utilDate = new java.util.Date();
            Date today = new Date(utilDate.getTime());

            if (dataTask.getSumProgres() != null && dataTask.getCountOfRow() != null){
                Double sum = dataTask.getSumProgres().doubleValue();
                Double count = dataTask.getCountOfRow().doubleValue();
                Double jmlh = sum/count;
                Integer intJumlah = jmlh.intValue();

                if (intJumlah != dataTask.getProgres()){

                    TaskMember taskMemberData = new TaskMember();
                    dataTaskMember.setIdTask(dataTask.getIdTask());
                    List<TaskMember> listTaskMemberData = new ArrayList<TaskMember>();
                    listTaskMemberData = taskMemberDao.getTaskMemberData(taskMemberData);

                    for (TaskMember listData :listTaskMemberData){
                        ItApbTaskMemberEntity taskMemberEntity = new ItApbTaskMemberEntity();
                        taskMemberEntity.setIdTaskMember(listData.getIdTaskMember());
                        taskMemberEntity.setIdTask(listData.getIdTask());
                        taskMemberEntity.setIdMember(listData.getIdMember());
                        taskMemberEntity.setProgres(dataTask.getProgres());
                        taskMemberEntity.setPosition(listData.getPosition());
                        taskMemberEntity.setCreatedWho(listData.getLastUpdateWho());
                        taskMemberEntity.setCreatedDate(listData.getCreatedDate());
                        taskMemberEntity.setLastUpdate(dataTask.getLastUpdate());
                        taskMemberEntity.setLastUpdateWho(dataTask.getLastUpdateWho());
                        taskMemberEntity.setStatus(dataTask.getStatusTask());
                        taskMemberEntity.setNote(listData.getNote());
                        taskMemberEntity.setFlag(dataTask.getFlag());
                        taskMemberEntity.setAction("U");
                        try {
                            taskMemberDao.updateAndSave(taskMemberEntity);
                        } catch (HibernateException e) {
                            logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
                            throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                        }
                    }
                }

            }

            String idLog;
            try {
                idLog = projectTaskLogDao.getNextTaskLogId();
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }

            ItApbProjectTaskLogEntity logEntity = new ItApbProjectTaskLogEntity();
            logEntity.setIdTaskLog("TL"+idLog);
            logEntity.setIdTask(dataTask.getIdTask());
            logEntity.setAsignTo(dataTask.getIdMember());
            logEntity.setDeadline(dataTask.getDeadline());
            logEntity.setAction("C");
            logEntity.setFlag("Y");
            logEntity.setNamaTask(dataTask.getNamaTask());
            logEntity.setNote(dataTask.getNote());
            logEntity.setPriority(dataTask.getPriority());
            logEntity.setProgres(dataTask.getProgres());
            logEntity.setStartDate(dataTask.getStartDate());
            logEntity.setStatusTask(dataTask.getStatusTask());
            logEntity.setLastUpdate(dataTask.getLastUpdate());
            logEntity.setLastUpdateWho(dataTask.getLastUpdateWho());

            String status = dataTask.getStatusTask();
            String statusBf = dataTask.getStatusBefore();
            String taskId = dataTask.getIdTask();
            List<TaskLog> taskLogList = new ArrayList<TaskLog>();
            taskLogList = projectTaskLogDao.getLastStatusUpdate(taskId);
            if (!status.equals(statusBf) ){
                logEntity.setLastStatusUpdate(dataTask.getLastUpdate());
            } else {
                if (taskLogList != null){
                    for (TaskLog dataTaskLog : taskLogList){
                        logEntity.setLastStatusUpdate(dataTaskLog.getLastStatusUpdate());
                    }
                }
            }

            if (logEntity.getLastStatusUpdate() == null){
                logEntity.setLastStatusUpdate(dataTask.getLastUpdate());
            }

            try {
                projectTaskLogDao.addAndSave(logEntity);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
            }

            List<Task> listSumAndCountTask = new ArrayList<Task>();
            listSumAndCountTask = projectDao.getSumAndCountTask(dataTask.getIdProject());
            BigInteger countRow = null;
            BigInteger sumProgres = null;
            if (listSumAndCountTask != null){
                for (Task sumCount : listSumAndCountTask){
                    countRow = sumCount.getCountOfRow();
                    sumProgres = sumCount.getSumProgres();
                }
            }

            Double dCount = countRow.doubleValue();
            Double dSum = sumProgres.doubleValue();
            Double jml = dSum/dCount;
            List<Project> listOfProject = new ArrayList<Project>();

            Map hsCriteria = new HashMap();
            if (dataTask.getIdProject() != null && !"".equalsIgnoreCase(dataTask.getIdProject())) {
                hsCriteria.put("project_id", dataTask.getIdProject());
            }

            listOfProject = projectDao.getSearchByCriteria(hsCriteria);

            if (listOfProject != null){
                for (Project dataProject : listOfProject){
                    ItApbProjectEntity entityProject = new ItApbProjectEntity();
                    entityProject.setIdProject(dataProject.getIdProject());
                    entityProject.setNamaProject(dataProject.getNamaProject());
                    if (jml == 100){
                        entityProject.setProgres(jml.intValue());
                        entityProject.setStatus("2");
                    }else {
                        if(today.before(dataProject.getStartDate())){
                            entityProject.setStatus("4");
                        }else if(today.after(dataProject.getDeadline())){
                            entityProject.setStatus("3");
                        } else if(today.after(dataProject.getStartDate()) && today.before(dataProject.getDeadline())) {
                            entityProject.setStatus("1");
                        }
                        entityProject.setProgres(jml.intValue());
                    }

                    entityProject.setStartDate(dataProject.getStartDate());
                    entityProject.setDeadline(dataProject.getDeadline());
                    entityProject.setFlag("Y");
                    entityProject.setAction("U");
                    entityProject.setLastUpdate(dataProject.getLastUpdate());
                    entityProject.setLastUpdateWho(dataProject.getLastUpdateWho());
                    entityProject.setIdTypeProject(dataProject.getIdTypeProject());
                    entityProject.setIdAlatDetail(dataProject.getIdAlatDetail());
                    entityProject.setNoWo(dataProject.getNoWo());
                    entityProject.setDivisi(dataProject.getIdDivisi());
                    try {
                        projectDao.updateAndSave(entityProject);
                    } catch (HibernateException e) {
                        logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
                        throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
                    }
                }
            }
        }
    }

    @Override
    public void saveDocument(Document dataDoc) throws GeneralBOException {
        if(dataDoc !=null){
            ItApbProjectDocEntity itApbProjectDocEntityNew = new ItApbProjectDocEntity();
            itApbProjectDocEntityNew.setIdProject(dataDoc.getIdProject());
            if(dataDoc.getIdTask() != null){
                itApbProjectDocEntityNew.setIdTask(dataDoc.getIdTask());
            }
            itApbProjectDocEntityNew.setIdDoc(dataDoc.getIdDoc());
            itApbProjectDocEntityNew.setNamaDoc(dataDoc.getNamaDoc());
            itApbProjectDocEntityNew.setFlag("Y");
            itApbProjectDocEntityNew.setAction("C");
            itApbProjectDocEntityNew.setLastUpdate(dataDoc.getLastUpdate());
            itApbProjectDocEntityNew.setUploadBy(dataDoc.getUploadBy());
            itApbProjectDocEntityNew.setDocType(dataDoc.getDocType());
            try {
                docDao.addAndSave(itApbProjectDocEntityNew);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.saveDocument] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data Document, please info to your admin..." + e.getMessage());
            }
        }
    }

    @Override
    public List<TypeProject> getTypeProjectByCriteria(TypeProject typeProject) throws GeneralBOException {
        logger.info("[ProjectBoImpl.getTypeProjectByCriteria] start process >>>");
        List<TypeProject> listOfResultTypeProject = new ArrayList<TypeProject>();

        if(typeProject != null){
            String flag = typeProject.getFlag();
            Map hsCriteria = new HashMap();

            if (typeProject.getFlag() != null && !"".equalsIgnoreCase(typeProject.getFlag())) {
                if ("N".equalsIgnoreCase(typeProject.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", typeProject.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }
            List<ImApbTypeProjectEntity> listOfTypeProject = null;
            try{
                listOfTypeProject = typeProjectDao.getByCriteria(hsCriteria);
            }catch (HibernateException e){
                logger.error("[ProjectBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if(listOfTypeProject != null){
                TypeProject addTypeProject;
                for(ImApbTypeProjectEntity imApbTypeProjectEntity : listOfTypeProject){
                    addTypeProject = new TypeProject();
                    addTypeProject.setIdTypeProject(imApbTypeProjectEntity.getIdTypeProject());
                    addTypeProject.setNamaTypeProject(imApbTypeProjectEntity.getNamaTypeProject());
                    listOfResultTypeProject.add(addTypeProject);

                }
            }
        }
        logger.info("[ProjectBoImpl.getTypeProjectByCriteria] start process >>>");
        return listOfResultTypeProject;
    }

    @Override
    public Task getTaskById(String idProject, String idTask) throws GeneralBOException {
        Task task;
        try {
            task = projectTaskDao.getByIdAndProjectId(idProject, idTask);
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.getTaskById] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when saving new data Document, please info to your admin..." + e.getMessage());
        }
        return task;
    }

    @Override
    public Project getProjectById(String idProject, String idTask) throws GeneralBOException {
        ItApbProjectEntity entity = null;
        try {
            entity = projectDao.getById("idProject", idProject);
        } catch (HibernateException e) {
            logger.error("[ProjectBoImpl.getProjectById] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when saving new data Document, please info to your admin..." + e.getMessage());
        }

        Project p = null;
        if( entity != null) {

            p = new Project();
            p.setIdProject(entity.getIdProject());
            p.setNamaProject(entity.getNamaProject());
//            p.setManagerProject(entity.getManagerProject());
            p.setDeadline(entity.getDeadline());
            p.setStartDate(entity.getStartDate());
            p.setStatus(entity.getStatus());
            p.setNoWo(entity.getNoWo());
            p.setIdAlatDetail(entity.getIdAlatDetail());

//            try {
//                ImUsers u = userDao.getById("primaryKey.id", p.getManagerProject());
//                p.setNamaProjectManager(u.getUserName());
//
//                ItApbProjectTaskEntity t = projectTaskDao.getById("idTask", idTask);
//                p.setTaskNote(t.getNote());
//            } catch (HibernateException e) {
//                logger.error("[ProjectBoImpl.getProjectById] Error, " + e.getMessage());
//                throw new GeneralBOException("Found problem when saving new data Document, please info to your admin..." + e.getMessage());
//            }
        }
        return p;
    }

    @Override
    public List<Task> getSearchListTask(Task task, String userRole, String idLogin) throws GeneralBOException {
        logger.info("[ProjectBoImpl.getSearchTaskByCriteria] start process >>>");

        List<Task> listOfResult = new ArrayList<Task>();
        List<Task> listOfResultTask = new ArrayList<Task>();


        if (task != null) {
            Map hsCriteria = new HashMap();

            if (task.getIdProject() != null && !"".equalsIgnoreCase(task.getIdProject())) {
                hsCriteria.put("project_id", task.getIdProject());
            }
            if (idLogin != null && !"".equalsIgnoreCase(idLogin)) {
                hsCriteria.put("member_id", idLogin);
            }
            if (task.getNoWo() != null && !"".equalsIgnoreCase(task.getNoWo())) {
                hsCriteria.put("no_wo", task.getNoWo());
            }
            if (task.getNamaProject() != null && !"".equalsIgnoreCase(task.getNamaProject())) {
                hsCriteria.put("project_name", task.getNamaProject());
            }
            if (task.getStatusTask() != null && !"".equalsIgnoreCase(task.getStatusTask())) {
                hsCriteria.put("status", task.getStatusTask());
            }
            if (userRole == "ADMIN") {
                hsCriteria.put("member_id", "admin");
            } else {
                hsCriteria.put("member_id", idLogin);
            }
            if (task.getFlag() != null && !"".equalsIgnoreCase(task.getFlag())) {
                if ("N".equalsIgnoreCase(task.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", task.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            try {

                listOfResult = projectTaskDao.getSearchListTask(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[ProjectBoImpl.getSearchTaskByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

//            if (listOfResult != null) {
//                for (Task dataTask : listOfResult) {
//                    if ("Y".equals(dataTask.getIsTask())){
//                        TaskMember taskMemberData = new TaskMember();
//                        taskMemberData.setIdTask(dataTask.getIdTask());
//
//                        List<TaskMember> listOfTaskMember = new ArrayList<TaskMember>();
//                        listOfTaskMember = taskMemberDao.getCountAndSumTask(taskMemberData);
//                        if (listOfTaskMember != null) {
//                            for (TaskMember dataList : listOfTaskMember) {
//                                dataTask.setSumProgres(dataList.getSumProgres());
//                                dataTask.setCountOfRow(dataList.getCountOfRow());
//                            }
//                        }
//
//                        java.util.Date utilDate = new java.util.Date();
//                        Date today = new Date(utilDate.getTime());
//
//                        if (dataTask.getSumProgres() != null && dataTask.getCountOfRow() != null) {
//                            Double sum = dataTask.getSumProgres().doubleValue();
//                            Double count = dataTask.getCountOfRow().doubleValue();
//                            Double jmlh = sum / count;
//                            Integer intJumlah = jmlh.intValue();
//
//                            if (intJumlah == 100) {
//                                dataTask.setStatusTask("2");
//                            } else if (intJumlah > 100 && today.before(dataTask.getDeadline())) {
//                                dataTask.setStatusTask("1");
//                            } else if (intJumlah > 100 && today.after(dataTask.getDeadline())) {
//                                dataTask.setStatusTask("3");
//                            }
//
//                            ItApbProjectTaskEntity taskEntity = new ItApbProjectTaskEntity();
//                            taskEntity.setIdTask(dataTask.getIdTask());
//                            taskEntity.setIdProject(dataTask.getIdProject());
//                            taskEntity.setNamaTask(dataTask.getNamaTask());
//                            taskEntity.setProgres(intJumlah);
//                            taskEntity.setPriority(dataTask.getPriority());
//                            taskEntity.setStatusTask(dataTask.getStatusTask());
//                            taskEntity.setNote(dataTask.getNote());
//                            taskEntity.setStartDate(dataTask.getStartDate());
//                            taskEntity.setDeadline(dataTask.getDeadline());
//                            taskEntity.setLastUpdate(dataTask.getLastUpdate());
//                            taskEntity.setLastUpdateWho(dataTask.getLastUpdateWho());
//                            taskEntity.setFlag(dataTask.getFlag());
//                            taskEntity.setNote(dataTask.getNote());
//                            taskEntity.setAction("U");
//                            try {
//                                projectTaskDao.updateAndSave(taskEntity);
//                            } catch (HibernateException e) {
//                                logger.error("[ProjectBoImpl.saveEditTask] Error, " + e.getMessage());
//                                throw new GeneralBOException("Found problem when saving new data status, please info to your admin..." + e.getMessage());
//                            }
//                    }
//
////                        try {
////
////                            listOfResultTask = projectTaskDao.getSearchListTask(hsCriteria);
////                        } catch (HibernateException e) {
////                            logger.error("[ProjectBoImpl.getSearchTaskByCriteria] Error, " + e.getMessage());
////                            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
////                        }
//
//                    }
//                }
//            }
        }
        logger.info("[ProjectBoImpl.getSearchTaskByCriteria] end process <<<");
//        return listOfResultTask;
        return listOfResult;
    }

//    @Override
//    public List<TaskLog> getsearchTaskLogByCriteria(String itemId) throws GeneralBOException {
//        return null;
//    }


}
