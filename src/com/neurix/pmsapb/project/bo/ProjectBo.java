package com.neurix.pmsapb.project.bo;

import com.neurix.authorization.user.model.UserRoles;
import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.doc.model.Document;
import com.neurix.pmsapb.master.typeproject.model.TypeProject;
import com.neurix.pmsapb.project.model.*;
import com.neurix.pmsapb.project.model.ProjectIssue;
import com.neurix.pmsapb.project.taskmember.model.TaskMember;

import java.util.List;

/**
 * Created by User on 5/3/2016.
 */
public interface ProjectBo extends BaseMasterBo<Project> {

    public List<Project> getSearchByCriteria(Project searchBean, String roleAs, String LoginId) throws GeneralBOException;
    public List<Project> viewProject(String itemId) throws GeneralBOException;
    public List<Member> getSearchMemberByCriteria(String itamId) throws GeneralBOException;
    public List<Task> getSearchTaskByCriteria(String itamId) throws GeneralBOException;
    public List<Project> getProjectTaskByCriteria(Project task, String userRole, String idLogin) throws GeneralBOException;
    public List<TaskLog> getSearchTaskLogByCriteria(String itamId) throws GeneralBOException;
    public List<ProjectIssue> getSearchIssueByCriteria(String itemId) throws GeneralBOException;
    public List<Document> getSearchDocumentByCriteria(String itemId) throws GeneralBOException;
    public List<Task> getSearchTaskCombo(Task dataTask) throws GeneralBOException;
    public List<UserRoles> getUsersRolesByCriteria(String itemId) throws GeneralBOException;
    public void saveProjectMember(Project project, List<Project> listOfProjectMember);
    public void saveProjectMemberTask(Project project, List<Member> listOfProjectMember, List<Task> listOfProjectTask, List<ProjectIssue> listOfissue, List<TaskMember> listOfTaskMember);
    public void saveEditProject(Project project, List<Member> listOfProjectMember, List<Task> listOfProjectTask, List<ProjectIssue> listOfProjectIssue, List<Document> listOfDocument, List<TaskMember> listOfTaskMember);
    public void updateStatusProject(Project dataTask) throws GeneralBOException;
    public void saveDeleteProject(Project project, List<Member> listOfProjectMember, List<Task> listOfProjectTask, List<ProjectIssue> listOfProjectIssue, List<Document> listOfDocument);
    public void saveProjectTask(Project project, List<Project> listOfProjectTask);
    public void saveProject(Project bean) throws GeneralBOException;
    public void saveIssue(Project issueBean) throws GeneralBOException;
    public void updateStatusByDate() throws GeneralBOException;
    public void updateProgresListTask(String idProject) throws GeneralBOException;

    public String getSeqNumTaskId() throws GeneralBOException;
    public String getSeqNumTaskLogId() throws GeneralBOException;
    public String getSeqNumProjectId() throws GeneralBOException;
    public String getSeqMemberId() throws GeneralBOException;
    public String getSeqDocId() throws GeneralBOException;
    public String getSeqIssueId() throws GeneralBOException;
    public String getSeqMemberTaskId() throws GeneralBOException;

    public void saveTask(Task task, TaskLog taskLog) throws GeneralBOException;
    public void deleteTask(Task task) throws GeneralBOException;


    public void saveEditTask(Task dataTask) throws GeneralBOException;
    public void updateTaskLastProgres(Task dataTask) throws GeneralBOException;
    public void saveEditTaskPIC(Task dataTask) throws GeneralBOException;
    public void saveDocument(Document dataDoc) throws GeneralBOException;

    List<TypeProject> getTypeProjectByCriteria(TypeProject typeProject) throws GeneralBOException;
    public Task getTaskById(String idProject, String idTask) throws GeneralBOException;
    Project getProjectById(String idProject, String idTask) throws GeneralBOException;

    public List<Task> getSearchListTask(Task task, String userRole, String idLogin) throws GeneralBOException;

}