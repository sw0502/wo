package com.neurix.pmsapb.project.taskmember.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.project.model.Task;
import com.neurix.pmsapb.project.taskmember.model.ItApbTaskMemberEntity;
import com.neurix.pmsapb.project.taskmember.model.TaskMember;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 16/11/2017.
 */
public class TaskMemberDao extends GenericDao<ItApbTaskMemberEntity,String> {
    @Override
    protected Class<ItApbTaskMemberEntity> getEntityClass() {
        return ItApbTaskMemberEntity.class;
    }

    @Override
    public List<ItApbTaskMemberEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbTaskMemberEntity.class);

        // Get Collection and sorting
        if (mapCriteria!=null) {
            if (mapCriteria.get("member_task_id")!=null) {
                criteria.add(Restrictions.ilike("idMemberTask", (String) mapCriteria.get("member_task_id")));
            }
            if (mapCriteria.get("member_id")!=null) {
                criteria.add(Restrictions.ilike("idMember", "%" + (String)mapCriteria.get("member_id") + "%"));
            }
            if (mapCriteria.get("task_id")!=null) {
                criteria.add(Restrictions.ilike("idTask", "%" + (String)mapCriteria.get("task_id") + "%"));
            }
        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        // Order by
        criteria.addOrder(Order.asc("idMemberTask"));
        List<ItApbTaskMemberEntity> results = criteria.list();
        return results;
//        return null;
    }

    public String getMemberTaskId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_member_task')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }

    public List<ItApbTaskMemberEntity> getSearchAll(){
        List<ItApbTaskMemberEntity> listOfResult = new ArrayList<ItApbTaskMemberEntity>();
        List<Object[]> result = new ArrayList<Object[]>();


            String SQL = "select * \n" +
                    "from it_apb_task_member where flag = 'Y'\n" ;


            result = this.sessionFactory.getCurrentSession().createSQLQuery(SQL)
                    .list();

            ItApbTaskMemberEntity taskMemberData;
            if (result.size() >= 1){
                for (Object[] obj: result){
                    taskMemberData = new ItApbTaskMemberEntity();
                    taskMemberData.setIdTaskMember((String) obj[0]);
                    taskMemberData.setIdTask((String) obj[1]);
                    taskMemberData.setIdMember((String) obj[2]);
                    taskMemberData.setProgres((Integer) obj[3]);
                    taskMemberData.setPosition((String) obj[4]);
                    taskMemberData.setStatus((String) obj[5]);
                    taskMemberData.setCreatedDate((Timestamp) obj[6]);
                    taskMemberData.setCreatedWho((String) obj[7]);
                    taskMemberData.setLastUpdate((Timestamp) obj[8]);
                    taskMemberData.setLastUpdateWho((String) obj[9]);
                    taskMemberData.setNote((String) obj[10]);
                    taskMemberData.setChangeTo((String) obj[11]);
                    taskMemberData.setFlag((String) obj[12]);
                    taskMemberData.setAction((String) obj[13]);
                    listOfResult.add(taskMemberData);
                }
            }

        return listOfResult;
    }

    public List<TaskMember> getCountAndSumTask(TaskMember taskMember) {
        List<TaskMember> listOfTaskMember = new ArrayList<TaskMember>();
        List<Object[]> result = new ArrayList<Object[]>();

        if (taskMember!= null){
            String taskId = "";
            if (taskMember.getIdTask()!= null && !"".equalsIgnoreCase(taskMember.getIdTask())){
                taskId = taskMember.getIdTask();
            }else {
                taskId = "%";
            }

         String SQL = "select \n" +
                     "sum(progres),\n" +
                     "count(task_member_id)\n " +
                     "from it_apb_task_member\n" +
                     "where task_id LIKE :taskId AND flag = 'Y'";

            result = this.sessionFactory.getCurrentSession().createSQLQuery(SQL)
                    .setParameter("taskId", taskId)
                    .list();

            TaskMember taskMemberData;
            if (result.size() >= 1){
                for (Object[] obj: result){
                    taskMemberData = new TaskMember();
                    taskMemberData.setSumProgres((BigInteger) obj[0]);
                    taskMemberData.setCountOfRow((BigInteger) obj[1]);
                    listOfTaskMember.add(taskMemberData);
                }
            }
        }
        return listOfTaskMember;
    }

    public List<TaskMember> getTaskMemberData(TaskMember taskMember){
        List<TaskMember> listOfTaskMember = new ArrayList<TaskMember>();
        List<Object[]> result = new ArrayList<Object[]>();

        if (taskMember!= null){
            String idTask = "";
            if (taskMember.getIdTask()!= null && !"".equalsIgnoreCase(taskMember.getIdTask())){
                idTask = taskMember.getIdTask();
            }else {
                idTask = "%";
            }

            String SQL = "select * \n" +
                    "from it_apb_task_member\n" +
                    "where task_id LIKE :idTask";

            result = this.sessionFactory.getCurrentSession().createSQLQuery(SQL)
                    .setParameter("idTask", idTask)
                    .list();

            TaskMember taskMemberData;
            if (result.size() >= 1){
                for (Object[] obj: result){
                    taskMemberData = new TaskMember();
                    taskMemberData.setIdTaskMember((String) obj[0]);
                    taskMemberData.setIdTask((String) obj[1]);
                    taskMemberData.setIdMember((String) obj[2]);
                    taskMemberData.setProgres((Integer) obj[3]);
                    taskMemberData.setPosition((String) obj[4]);
                    taskMemberData.setStatus((String) obj[5]);
                    taskMemberData.setCreatedDate((Timestamp) obj[6]);
                    taskMemberData.setCreatedWho((String) obj[7]);
                    taskMemberData.setLastUpdate((Timestamp) obj[8]);
                    taskMemberData.setLastUpdateWho((String) obj[9]);
                    taskMemberData.setNote((String) obj[10]);
                    taskMemberData.setChangeTo((String) obj[11]);
                    taskMemberData.setFlag((String) obj[12]);
                    taskMemberData.setAction((String) obj[13]);
                    listOfTaskMember.add(taskMemberData);
                }
            }
        }
        return listOfTaskMember;


    }
}
