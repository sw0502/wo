package com.neurix.pmsapb.project.taskmember.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.pmsapb.project.taskmember.model.TaskMember;

/**
 * Created by thinkpad on 15/11/2017.
 */
public interface TaskMemberBo extends BaseMasterBo<TaskMember>{

}
