package com.neurix.pmsapb.project.taskmember.bo.impl;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.project.taskmember.bo.TaskMemberBo;
import com.neurix.pmsapb.project.taskmember.dao.TaskMemberDao;
import com.neurix.pmsapb.project.taskmember.dao.TaskMemberDao;
import com.neurix.pmsapb.project.taskmember.model.ItApbTaskMemberEntity;
import com.neurix.pmsapb.project.taskmember.model.TaskMember;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class TaskMemberBoImpl implements TaskMemberBo {
    protected static transient Logger logger = Logger.getLogger(TaskMemberBoImpl.class);
    private TaskMemberDao taskMemberDao;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        TaskMemberBoImpl.logger = logger;
    }

    public TaskMemberDao getTaskMemberDao() {
        return taskMemberDao;
    }

    public void setTaskMemberDao(TaskMemberDao taskMemberDao) {
        this.taskMemberDao = taskMemberDao;
    }

    @Override
    public void saveDelete(TaskMember bean) throws GeneralBOException {

    }

    @Override
    public void saveEdit(TaskMember bean) throws GeneralBOException {

    }

    @Override
    public TaskMember saveAdd(TaskMember bean) throws GeneralBOException {
        return null;
    }

    @Override
    public List<TaskMember> getByCriteria(TaskMember searchBean) throws GeneralBOException {
        logger.info("[TaskMemberBoImpl.getByCriteria] start process >>>");

        // Mapping with collection and put
        List<TaskMember> listOfResult = new ArrayList();

        if (searchBean != null) {
            Map hsCriteria = new HashMap();

            if (searchBean.getIdTaskMember() != null && !"".equalsIgnoreCase(searchBean.getIdTaskMember())) {
                hsCriteria.put("member_task_id", searchBean.getIdTaskMember());
            }
            if (searchBean.getIdMember() != null && !"".equalsIgnoreCase(searchBean.getIdMember())) {
                hsCriteria.put("member_id", searchBean.getIdMember());
            }
            if (searchBean.getIdTask() != null && !"".equalsIgnoreCase(searchBean.getIdTask())) {
                hsCriteria.put("task_id", searchBean.getIdTask());
            }
            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            List<ItApbTaskMemberEntity> itApbTaskMemberEntities = null;
            try {
                itApbTaskMemberEntities = taskMemberDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[TaskMemberBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

        }
        logger.info("[TaskMemberBoImpl.getByCriteria] end process <<<");
        return listOfResult;
    }

    @Override
    public List<TaskMember> getAll() throws GeneralBOException {
        List<TaskMember> listOfTaskMember = new ArrayList<TaskMember>();
        List<ItApbTaskMemberEntity> listEntity = null;

        try {
            listEntity = taskMemberDao.getSearchAll();
        } catch (HibernateException e) {
            logger.error("[TaskMemberBoImpl.getByCriteria] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
        }

        if (listEntity != null){
            for (ItApbTaskMemberEntity listData :listEntity){
                TaskMember taskMember = new TaskMember();
                taskMember.setIdTaskMember(listData.getIdTaskMember());
                taskMember.setIdTask(listData.getIdTask());
                taskMember.setIdMember(listData.getIdMember());
                taskMember.setMemberBf(listData.getIdMember());
                taskMember.setProgres(listData.getProgres());
                taskMember.setPosition(listData.getPosition());
                taskMember.setStatus(listData.getStatus());
                taskMember.setCreatedDate(listData.getCreatedDate());
                taskMember.setCreatedWho(listData.getCreatedWho());
                taskMember.setLastUpdate(listData.getLastUpdate());
                taskMember.setLastUpdateWho(listData.getLastUpdateWho());
                taskMember.setNote(listData.getNote());
                taskMember.setChangeTo(listData.getChangeTo());
                taskMember.setFlag(listData.getFlag());
                taskMember.setAction(listData.getAction());
                listOfTaskMember.add(taskMember);
            }
        }

        return listOfTaskMember;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }
}
