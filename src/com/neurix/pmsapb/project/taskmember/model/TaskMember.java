package com.neurix.pmsapb.project.taskmember.model;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class TaskMember extends BaseModel implements Serializable {
    private String idTaskMember;
    private String idMember;
    private String idTask;
    private Timestamp createdDate;
    private String createdWho;
    private Timestamp lastUpdate;
    private String lastUpdateWho;
    private String flag;
    private String action;
    private String position;
    private Integer progres;
    private String changeTo;
    private String status;
    private String note;
    private String namaMember;
    private String namaTask;
    private String namaStatus;
    private String namaPosition;
    private String statusFlag;
    private String memberBf;
    private String stPosition;

    private boolean added;
    private boolean saved;

    private BigInteger sumProgres;
    private BigInteger countOfRow;

    public String getStPosition() {
        return stPosition;
    }

    public void setStPosition(String stPosition) {
        this.stPosition = stPosition;
    }

    public String getMemberBf() {
        return memberBf;
    }

    public void setMemberBf(String memberBf) {
        this.memberBf = memberBf;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    public boolean isAdded() {
        return added;
    }

    public String getNamaMember() {
        return namaMember;
    }

    public void setNamaMember(String namaMember) {
        this.namaMember = namaMember;
    }

    public String getNamaTask() {
        return namaTask;
    }

    public void setNamaTask(String namaTask) {
        this.namaTask = namaTask;
    }

    public String getNamaStatus() {
        return namaStatus;
    }

    public void setNamaStatus(String namaStatus) {
        this.namaStatus = namaStatus;
    }

    public String getNamaPosition() {
        return namaPosition;
    }

    public void setNamaPosition(String namaPosition) {
        this.namaPosition = namaPosition;
    }

    public void setAdded(boolean added) {
        this.added = added;
    }

    public BigInteger getSumProgres() {
        return sumProgres;
    }

    public void setSumProgres(BigInteger sumProgres) {
        this.sumProgres = sumProgres;
    }

    public BigInteger getCountOfRow() {
        return countOfRow;
    }

    public void setCountOfRow(BigInteger countOfRow) {
        this.countOfRow = countOfRow;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdTaskMember() {
        return idTaskMember;
    }

    public void setIdTaskMember(String idTaskMember) {
        this.idTaskMember = idTaskMember;
    }

    public String getIdMember() {
        return idMember;
    }

    public void setIdMember(String idMember) {
        this.idMember = idMember;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    @Override
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String getCreatedWho() {
        return createdWho;
    }

    @Override
    public void setCreatedWho(String createdWho) {
        this.createdWho = createdWho;
    }

    @Override
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    @Override
    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getProgres() {
        return progres;
    }

    public void setProgres(Integer progres) {
        this.progres = progres;
    }

    public String getChangeTo() {
        return changeTo;
    }

    public void setChangeTo(String changeTo) {
        this.changeTo = changeTo;
    }
}
