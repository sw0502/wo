package com.neurix.pmsapb.project.taskmember.action;

import com.neurix.common.action.BaseMasterAction;
import com.neurix.pmsapb.project.taskmember.bo.TaskMemberBo;

/**
 * Created by thinkpad on 16/11/2017.
 */
public class TaskMemberAction extends BaseMasterAction {
    private TaskMemberBo taskMemberBoProxy;

    public TaskMemberBo getTaskMemberBoProxy() {
        return taskMemberBoProxy;
    }

    public void setTaskMemberBoProxy(TaskMemberBo taskMemberBoProxy) {
        this.taskMemberBoProxy = taskMemberBoProxy;
    }

    @Override
    public String add() {
        return null;
    }

    @Override
    public String edit() {
        return null;
    }

    @Override
    public String delete() {
        return null;
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        return null;
    }

    @Override
    public String search() {
        return null;
    }

    @Override
    public String initForm() {
        return null;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }
}
