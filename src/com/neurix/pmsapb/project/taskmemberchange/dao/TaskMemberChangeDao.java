package com.neurix.pmsapb.project.taskmemberchange.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.project.taskmember.model.ItApbTaskMemberEntity;
import com.neurix.pmsapb.project.taskmember.model.TaskMember;
import com.neurix.pmsapb.project.taskmemberchange.model.ItApbTaskMemberChangeEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 16/11/2017.
 */
public class TaskMemberChangeDao extends GenericDao<ItApbTaskMemberChangeEntity,String> {
    @Override
    protected Class<ItApbTaskMemberChangeEntity> getEntityClass() {
        return ItApbTaskMemberChangeEntity.class;
    }

    @Override
    public List<ItApbTaskMemberChangeEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbTaskMemberChangeEntity.class);

        // Get Collection and sorting
        if (mapCriteria!=null) {
            if (mapCriteria.get("change_id")!=null) {
                criteria.add(Restrictions.ilike("idTaskMemberChange", (String) mapCriteria.get("change_id")));
            }
            if (mapCriteria.get("task_member_id")!=null) {
                criteria.add(Restrictions.ilike("idTaskMember", "%" + (String)mapCriteria.get("task_member_id") + "%"));
            }
            if (mapCriteria.get("task_id")!=null) {
                criteria.add(Restrictions.ilike("idTask", "%" + (String)mapCriteria.get("task_id") + "%"));
            }
            if (mapCriteria.get("project_id")!=null) {
                criteria.add(Restrictions.ilike("idProject", "%" + (String)mapCriteria.get("project_id") + "%"));
            }
        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        // Order by
        criteria.addOrder(Order.asc("idTaskMemberChange"));
        List<ItApbTaskMemberChangeEntity> results = criteria.list();
        return results;
//        return null;
    }

    public String getChangeMemberId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_change_member')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }

}
