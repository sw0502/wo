package com.neurix.pmsapb.project.taskmemberchange.bo.impl;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.project.taskmember.bo.TaskMemberBo;
import com.neurix.pmsapb.project.taskmember.dao.TaskMemberDao;
import com.neurix.pmsapb.project.taskmember.model.ItApbTaskMemberEntity;
import com.neurix.pmsapb.project.taskmember.model.TaskMember;
import com.neurix.pmsapb.project.taskmemberchange.bo.TaskMemberChangeBo;
import com.neurix.pmsapb.project.taskmemberchange.dao.TaskMemberChangeDao;
import com.neurix.pmsapb.project.taskmemberchange.model.ItApbTaskMemberChangeEntity;
import com.neurix.pmsapb.project.taskmemberchange.model.TaskMemberChange;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class TaskMemberChangeBoImpl implements TaskMemberChangeBo {
    protected static transient Logger logger = Logger.getLogger(TaskMemberChangeBoImpl.class);
    private TaskMemberChangeDao taskMemberChangeDao;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        TaskMemberChangeBoImpl.logger = logger;
    }

    public TaskMemberChangeDao getTaskMemberChangeDao() {
        return taskMemberChangeDao;
    }

    public void setTaskMemberChangeDao(TaskMemberChangeDao taskMemberChangeDao) {
        this.taskMemberChangeDao = taskMemberChangeDao;
    }

    @Override
    public void saveDelete(TaskMemberChange bean) throws GeneralBOException {

    }

    @Override
    public void saveEdit(TaskMemberChange bean) throws GeneralBOException {

    }

    @Override
    public TaskMemberChange saveAdd(TaskMemberChange bean) throws GeneralBOException {
        return null;
    }

    @Override
    public List<TaskMemberChange> getByCriteria(TaskMemberChange searchBean) throws GeneralBOException {
        logger.info("[TaskMemberBoImpl.getByCriteria] start process >>>");

        // Mapping with collection and put
        List<TaskMemberChange> listOfResult = new ArrayList();

        if (searchBean != null) {
            Map hsCriteria = new HashMap();

            if (searchBean.getIdTaskMember() != null && !"".equalsIgnoreCase(searchBean.getIdTaskMember())) {
                hsCriteria.put("task_member_id", searchBean.getIdTaskMember());
            }
            if (searchBean.getIdTaskMemberChange() != null && !"".equalsIgnoreCase(searchBean.getIdTaskMemberChange())) {
                hsCriteria.put("change_id", searchBean.getIdTaskMemberChange());
            }
            if (searchBean.getIdProject() != null && !"".equalsIgnoreCase(searchBean.getIdProject())) {
                hsCriteria.put("project_id", searchBean.getIdProject());
            }
            if (searchBean.getIdTask() != null && !"".equalsIgnoreCase(searchBean.getIdTask())) {
                hsCriteria.put("task_id", searchBean.getIdTask());
            }
            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            List<ItApbTaskMemberChangeEntity> itApbTaskMemberChangeEntities = null;
            try {
                itApbTaskMemberChangeEntities = taskMemberChangeDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[TaskMemberBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
            if (itApbTaskMemberChangeEntities != null){
                TaskMemberChange setData;
                for (ItApbTaskMemberChangeEntity dataList :itApbTaskMemberChangeEntities){
                    setData = new TaskMemberChange();
                    setData.setIdTaskMemberChange(dataList.getIdTaskMemberChange());
                    setData.setIdTaskMember(dataList.getIdTaskMember());
                    setData.setIdProject(dataList.getIdProject());
                    setData.setIdTask(dataList.getIdTask());
                    setData.setIdMemberOld(dataList.getIdMemberOld());
                    setData.setIdMemberNew(dataList.getIdMemberNew());
                    setData.setProgres(dataList.getProgres());
                    setData.setNote(dataList.getNote());
                    setData.setCreatedDate(dataList.getCreatedDate());
                    setData.setLastUpdate(dataList.getLastUpdate());
                    setData.setCreatedWho(dataList.getCreatedWho());
                    setData.setLastUpdateWho(dataList.getLastUpdateWho());
                    setData.setFlag(dataList.getFlag());
                    setData.setAction(dataList.getAction());
                    listOfResult.add(setData);
                }
            }

        }
        logger.info("[TaskMemberBoImpl.getByCriteria] end process <<<");
        return listOfResult;
    }

    @Override
    public List<TaskMemberChange> getAll() throws GeneralBOException {
        return null;
    }


    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }
}
