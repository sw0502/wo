package com.neurix.pmsapb.project.taskmemberchange.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.pmsapb.project.taskmember.model.TaskMember;
import com.neurix.pmsapb.project.taskmemberchange.model.TaskMemberChange;

/**
 * Created by thinkpad on 15/11/2017.
 */
public interface TaskMemberChangeBo extends BaseMasterBo<TaskMemberChange>{

}
