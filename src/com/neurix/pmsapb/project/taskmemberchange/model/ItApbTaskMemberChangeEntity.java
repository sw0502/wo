package com.neurix.pmsapb.project.taskmemberchange.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class ItApbTaskMemberChangeEntity implements Serializable {
    private String idTaskMemberChange;
    private String idTaskMember;
    private String idMemberOld;
    private String idMemberNew;
    private String idProject;
    private String idTask;
    private Timestamp createdDate;
    private String createdWho;
    private Timestamp lastUpdate;
    private String lastUpdateWho;
    private String flag;
    private String action;
    private Integer progres;
    private String note;

    public String getIdTaskMemberChange() {
        return idTaskMemberChange;
    }

    public void setIdTaskMemberChange(String idTaskMemberChange) {
        this.idTaskMemberChange = idTaskMemberChange;
    }

    public String getIdTaskMember() {
        return idTaskMember;
    }

    public void setIdTaskMember(String idTaskMember) {
        this.idTaskMember = idTaskMember;
    }

    public String getIdMemberOld() {
        return idMemberOld;
    }

    public void setIdMemberOld(String idMemberOld) {
        this.idMemberOld = idMemberOld;
    }

    public String getIdMemberNew() {
        return idMemberNew;
    }

    public void setIdMemberNew(String idMemberNew) {
        this.idMemberNew = idMemberNew;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedWho() {
        return createdWho;
    }

    public void setCreatedWho(String createdWho) {
        this.createdWho = createdWho;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getProgres() {
        return progres;
    }

    public void setProgres(Integer progres) {
        this.progres = progres;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
