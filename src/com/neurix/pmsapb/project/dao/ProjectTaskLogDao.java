package com.neurix.pmsapb.project.dao;



import com.neurix.common.constant.CommonConstant;
import com.neurix.common.dao.GenericDao;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.project.model.ItApbProjectTaskEntity;
import com.neurix.pmsapb.project.model.ItApbProjectTaskLogEntity;
import com.neurix.pmsapb.project.model.Task;
import com.neurix.pmsapb.project.model.TaskLog;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 3/1/2017.
 */
public class ProjectTaskLogDao extends GenericDao<ItApbProjectTaskLogEntity,String> {


    @Override
    protected Class<ItApbProjectTaskLogEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ItApbProjectTaskLogEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectTaskLogEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("task_log_id")!=null) {
                criteria.add(Restrictions.eq("idTaskLog", (String) mapCriteria.get("task_log_id")));
            }
            if (mapCriteria.get("task_id")!=null) {
                criteria.add(Restrictions.eq("idTask", (String) mapCriteria.get("task_id")));
            }
            if (mapCriteria.get("task_name")!=null) {
                criteria.add(Restrictions.ilike("namaTask", "%" + (String)mapCriteria.get("task_name") + "%"));
            }
        }
//        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.desc("idTaskLog"));
        List<ItApbProjectTaskLogEntity> results = criteria.list();
        return results;
    }

    public String getNextTaskLogId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_task_log')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }

//    public int getSumProgres(String idProject) throws  HibernateException{
//
//        int sumOfProgres = 0;
//        if (idProject != null){
//
//            List<Object[]> results = new ArrayList<Object[]>();
//
//            String query = "select sum(progres) as jumlah_progres\n" +
//                    "from it_apb_task\n" +
//                    "where project_id = :idProject";
//
//            results = this.sessionFactory.getCurrentSession()
//                    .createSQLQuery(query)
//                    .setParameter("idProject", idProject)
//                    .list();
//            Task resultOfTask;
//            for (Object[] row : results) {
//                resultOfTask = new Task();
//                resultOfTask.setSumProgres((Integer) row[0]);
//                sumOfProgres = resultOfTask.getSumProgres();
//            }
//
//        }
//        return sumOfProgres ;
//    }

//    public int setCountProgres(String idProject) throws HibernateException{
//        int countOfProgres = 0;
//        if (idProject != null){
//
//            List<Object[]> results = new ArrayList<Object[]>();
//
//            String query = "select count(project_id) as countofTask\n" +
//                    "from it_apb_task\n" +
//                    "where project_id = :idProject";
//
//            results = this.sessionFactory.getCurrentSession()
//                    .createSQLQuery(query)
//                    .setParameter("idProject", idProject)
//                    .list();
//            Task resultOfTask;
//            for (Object[] row : results) {
//                resultOfTask = new Task();
//                resultOfTask.setCountOfRow((Integer) row[0]);
//                countOfProgres = resultOfTask.getCountOfRow();
//            }
//
//        }
//        return countOfProgres ;
//
//    }

    public List<Task> getSumAndCountByCriteria(Map mapCriteria) throws HibernateException {
        Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectTaskEntity.class);


        List<Task> listOfResult = new ArrayList<Task>();

        String query = "";
        String idProject = "";


        List<Object[]> results = new ArrayList<Object[]>();

        if (mapCriteria != null) {
            if (mapCriteria.get("project_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_id"))) {
                idProject = (String) mapCriteria.get("project_id");
            } else {
                idProject = "%";
            }

            query = "select \n" +
                    "project_id,\n" +
                    "sum(progres)as jml_progres,\n" +
                    "count(project_id) as count_row\n" +
                    "from it_apb_task\n" +
                    "where project_id LIKE :idProject \n" +
                    "and flag = 'Y' \n" +
                    "group by\n" +
                    "project_id";


            results = this.sessionFactory.getCurrentSession()
                    .createSQLQuery(query)
                    .setParameter("idProject", idProject)
                    .list();


            Task resultTask;
            for (Object[] row : results) {
                resultTask = new Task();
                resultTask.setIdProject((String) row[0]);
                resultTask.setSumProgres((BigInteger) row[1]);
                resultTask.setCountOfRow((BigInteger) row[2]);
                listOfResult.add(resultTask);
            }
        }
        return listOfResult;
    }

    public List<TaskLog> getLastStatusUpdate(String taskId) throws HibernateException{
        List<TaskLog> listOfTask = new ArrayList<TaskLog>();
        String id;
        if (taskId != null){
            id = taskId;
        } else {
            id = "%";
        }
        List<Object[]> results = new ArrayList<Object[]>();


        String query= "select last_status_update, last_update from it_apb_task_log\n" +
                "where task_id = :id \n" +
                "order by last_update desc limit 1";


        results = this.sessionFactory.getCurrentSession()
                .createSQLQuery(query)
                .setParameter("id", id)
                .list();

        TaskLog dataTask;
        for (Object[] row: results){
            dataTask = new TaskLog();
            dataTask.setLastUpdate((Timestamp) row[1]);
            dataTask.setLastStatusUpdate((Timestamp) row[0]);
            listOfTask.add(dataTask);
        }

        return listOfTask;
    }
}