package com.neurix.pmsapb.project.dao;



import com.neurix.common.constant.CommonConstant;
import com.neurix.common.dao.GenericDao;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.project.model.ItApbProjectTaskEntity;
import com.neurix.pmsapb.project.model.Task;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by User on 3/1/2017.
 */
public class ProjectTaskDao extends GenericDao<ItApbProjectTaskEntity,String> {


    @Override
    protected Class<ItApbProjectTaskEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ItApbProjectTaskEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectTaskEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("task_id")!=null) {
                criteria.add(Restrictions.eq("idTask", (String) mapCriteria.get("task_id")));
            }
//            if (mapCriteria.get("priority_name")!=null) {
//                criteria.add(Restrictions.ilike("namaPriority", "%" + (String)mapCriteria.get("priority_name") + "%"));
//            }
        }
        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.asc("idTask"));
        List<ItApbProjectTaskEntity> results = criteria.list();
        return results;
    }

    public String getNextTaskId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_task')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }



//    public int getSumProgres(String idProject) throws  HibernateException{
//
//        int sumOfProgres = 0;
//        if (idProject != null){
//
//            List<Object[]> results = new ArrayList<Object[]>();
//
//            String query = "select sum(progres) as jumlah_progres\n" +
//                    "from it_apb_task\n" +
//                    "where project_id = :idProject";
//
//            results = this.sessionFactory.getCurrentSession()
//                    .createSQLQuery(query)
//                    .setParameter("idProject", idProject)
//                    .list();
//            Task resultOfTask;
//            for (Object[] row : results) {
//                resultOfTask = new Task();
//                resultOfTask.setSumProgres((Integer) row[0]);
//                sumOfProgres = resultOfTask.getSumProgres();
//            }
//
//        }
//        return sumOfProgres ;
//    }

//    public int setCountProgres(String idProject) throws HibernateException{
//        int countOfProgres = 0;
//        if (idProject != null){
//
//            List<Object[]> results = new ArrayList<Object[]>();
//
//            String query = "select count(project_id) as countofTask\n" +
//                    "from it_apb_task\n" +
//                    "where project_id = :idProject";
//
//            results = this.sessionFactory.getCurrentSession()
//                    .createSQLQuery(query)
//                    .setParameter("idProject", idProject)
//                    .list();
//            Task resultOfTask;
//            for (Object[] row : results) {
//                resultOfTask = new Task();
//                resultOfTask.setCountOfRow((Integer) row[0]);
//                countOfProgres = resultOfTask.getCountOfRow();
//            }
//
//        }
//        return countOfProgres ;
//
//    }

    public Task getByIdAndProjectId(String idProject, String idTask) throws HibernateException {
        List<Object[]> result = new ArrayList<Object[]>();

        String SQL = "SELECT * FROM it_apb_task " +
                "WHERE project_id = :idProject " +
                "AND task_id = :idTask ";

                result = this.sessionFactory.getCurrentSession().createSQLQuery(SQL)
                        .setParameter("idProject", idProject)
                        .setParameter("idTask", idTask)
                        .list();

        Task task = new Task();
        if(result.size() >= 1){
            for (Object[] obj : result) {
                task.setIdProject((String) obj[0]);
                task.setIdTask((String) obj[1]);
                task.setNamaTask((String) obj[2]);
                task.setDeadline((Date) obj[3]);
                task.setAsignTo((String) obj[4]);
                task.setPriority((String) obj[5]);
                task.setNote((String) obj[6]);
                task.setProgres((Integer) obj[7]);
                task.setNamaStatus((String) obj[8]);
                task.setStatusTask((String) obj[8]);
                task.setStatusBefore((String) obj[8]);
                task.setStartDate((Date) obj[12]);
                if (task.getDeadline() != null){
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    String stDate = df.format(task.getDeadline());
                    String stStartDate = df.format(task.getStartDate());
                    task.setStStartDate(stStartDate);
                    task.setStDeadline(stDate);
                }
            }
        }else{
            task = null;
        }

        return task;
    }

    public List<Task> getSumAndCountByCriteria(Map mapCriteria) throws HibernateException {
        Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectTaskEntity.class);


        List<Task> listOfResult = new ArrayList<Task>();

        String query = "";
        String idProject = "";


        List<Object[]> results = new ArrayList<Object[]>();

        if (mapCriteria != null) {
            if (mapCriteria.get("project_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_id"))) {
                idProject = (String) mapCriteria.get("project_id");
            } else {
                idProject = "%";
            }

            query = "select \n" +
                    "project_id,\n" +
                    "sum(progres)as jml_progres,\n" +
                    "count(project_id) as count_row\n" +
                    "from it_apb_task\n" +
                    "where project_id LIKE :idProject \n" +
                    "and flag = 'Y' \n" +
                    "group by\n" +
                    "project_id";


            results = this.sessionFactory.getCurrentSession()
                    .createSQLQuery(query)
                    .setParameter("idProject", idProject)
                    .list();


            Task resultTask;
            for (Object[] row : results) {
                resultTask = new Task();
                resultTask.setIdProject((String) row[0]);
                resultTask.setSumProgres((BigInteger) row[1]);
                resultTask.setCountOfRow((BigInteger) row[2]);
                listOfResult.add(resultTask);
            }
        }
        return listOfResult;
    }

    public List<Task> getSearchListTask(Map mapCriteria){
        List<Task> listOfTask = new ArrayList<Task>();
        List<Object[]> result = new ArrayList<Object[]>();

        String idProject = "";
        String noWo = "";
        String projectName = "";
        String statusTask = "";
//        String asignTo = "";
        String idMember = "";

        if (mapCriteria != null){
            if(mapCriteria.get("project_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_id"))){
                idProject = (String) mapCriteria.get("project_id");
            } else {
                idProject = "%";
            }
            if(mapCriteria.get("member_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("member_id"))){
                idMember = (String) mapCriteria.get("member_id");
            } else {
                idMember = "%";
            }
            if (mapCriteria.get("no_wo") != null && !"".equalsIgnoreCase((String) mapCriteria.get("no_wo"))){
                noWo = (String) mapCriteria.get("no_wo");
            } else {
                noWo = "%";
            }
            if (mapCriteria.get("project_name") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_name"))){
                projectName = (String) mapCriteria.get("project_name");
            } else {
                projectName = "%";
            }
            if (mapCriteria.get("status_task") != null && !"".equalsIgnoreCase((String) mapCriteria.get("status_task"))){
                statusTask = (String) mapCriteria.get("status_task");
            } else {
                statusTask = "%";
            }

            String SQL = "select a.* \n" +
                    "from (  \n" +
                    "select  \n" +
                    "b.project_id, \n" +
                    "a.task_id, \n" +
                    "b.task_name, \n" +
                    "b.deadline, \n" +
                    "b.asign_to, \n" +
                    "b.priority, \n" +
                    "a.note, \n" +
                    "b.progres, \n" +
                    "b.status, \n" +
                    "a.flag, \n" +
                    "a.action, \n" +
                    "b.start_date, \n" +
                    "a.last_update, \n" +
                    "a.last_update_who, \n" +
                    "b.last_status_update, \n" +
                    "a.member_id, \n" +
                    "a.position_id, \n" +
                    "c.user_name, \n" +
                    "'Y' as is_task, \n" +
                    "d.project_name, \n" +
                    "d.no_wo, \n" +
                    "a.task_member_id \n" +
                    "from it_apb_task_member a \n" +
                    "inner join it_apb_task b on b.task_id = a.task_id \n" +
                    "inner join im_users c on c.user_id = a.member_id \n" +
                    "inner join it_apb_project d on d.project_id = b.project_id \n" +
                    "where a.position_id = '5' \n" +
                    "union all \n" +
                    "select  \n" +
                    "b.project_id, \n" +
                    "a.task_id, \n" +
                    "b.task_name, \n" +
                    "b.deadline, \n" +
                    "b.asign_to, \n" +
                    "b.priority, \n" +
                    "a.note, \n" +
                    "a.progres, \n" +
                    "a.status, \n" +
                    "a.flag, \n" +
                    "a.action, \n" +
                    "b.start_date, \n" +
                    "a.last_update, \n" +
                    "a.last_update_who, \n" +
                    "b.last_status_update, \n" +
                    "a.member_id, \n" +
                    "a.position_id, \n" +
                    "c.user_name, \n" +
                    "'N' as is_task, \n" +
                    "d.project_name, \n" +
                    "d.no_wo, \n" +
                    "a.task_member_id \n" +
                    "from it_apb_task_member a \n" +
                    "inner join it_apb_task b on b.task_id = a.task_id \n" +
                    "inner join im_users c on c.user_id = a.member_id \n" +
                    "inner join it_apb_project d on d.project_id = b.project_id \n" +
                    ")a  \n" +
                    "where flag LIKE 'Y' \n" +
                    "and project_id LIKE :idProject\n" +
                    "and project_name LIKE :projectName \n" +
                    "and member_id LIKE :idMember \n" +
                    "and no_wo LIKE :noWo \n" +
                    "and status LIKE :statusTask \n" +
                    "order by project_id, task_id, is_task desc";

//            String SQL = "SELECT * FROM \n" +
//                    "   (SELECT project_id, no_wo, project_name FROM it_apb_project) project LEFT JOIN\n" +
//                    "   (SELECT task_id, task_name, start_date, deadline, status, priority, progres, project_id, flag, asign_to FROM it_apb_task) task ON project.project_id = task.project_id\n" +
//                    "WHERE project.project_id LIKE :idProject \n" +
//                    "AND project.no_wo LIKE :noWo \n" +
//                    "AND project.project_name LIKE :projectName \n" +
//                    "AND task.status LIKE :statusTask \n" +
//                    "AND task.flag= 'Y' \n " +
//                    "AND task.asign_to = :asignTo";

            result = this.sessionFactory.getCurrentSession().createSQLQuery(SQL)
                    .setParameter("idProject", idProject)
                    .setParameter("noWo", noWo)
                    .setParameter("projectName", projectName)
                    .setParameter("statusTask", statusTask)
                    .setParameter("idMember", idMember)
                    .list();

            Map<String, String> labelPriority = new HashMap<String, String>();
            labelPriority.put("1", "<div align='center' style='background-color:#ff6565;padding:10px;border-radius:14px;color:#fff;'>High</div>");
            labelPriority.put("2", "<div align='center' style='background-color:#8be78f;padding:10px;border-radius:14px;color:#fff;'>Normal</div>");
            labelPriority.put("3", "<div align='center' style='background-color:#6582ff;padding:10px;border-radius:14px;color:#fff;'>Low</div>");

            Map<Integer, String> mapBar = new HashMap<Integer, String>();
            mapBar.put(10, "50");
            mapBar.put(20, "70");
            mapBar.put(30, "90");
            mapBar.put(40, "110");
            mapBar.put(50, "130");
            mapBar.put(60, "150");
            mapBar.put(70, "170");
            mapBar.put(80, "190");
            mapBar.put(90, "210");
            mapBar.put(100, "230");

            Map<String, String> labelStatus = new HashMap<String,String>();
            labelStatus.put("1", "<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
            labelStatus.put("2", "<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
            labelStatus.put("3", "<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
            labelStatus.put("4", "<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Initiate</div>");
            labelStatus.put("5", "<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");


            Task dataTask;
            for (Object[] obj: result){
                dataTask = new Task();
                dataTask.setIdProject((String) obj[0]);
                dataTask.setIdTask((String) obj[1]);
                dataTask.setNamaTask((String) obj[2]);
                dataTask.setDeadline((Date) obj[3]);
                dataTask.setAsignTo((String) obj[4]);
                dataTask.setPriority((String) obj[5]);
                dataTask.setNote((String) obj[6]);
                dataTask.setProgres((Integer) obj[7]);
                dataTask.setStatusTask((String) obj[8]);
                dataTask.setFlag((String) obj[9]);
                dataTask.setAction((String) obj[10]);
                dataTask.setStartDate((Date) obj[11]);
                dataTask.setLastUpdate((Timestamp) obj[12]);
                dataTask.setLastUpdateWho((String) obj[13]);
                dataTask.setLastStatusUpdate((Timestamp) obj[14]);
                dataTask.setIdMember((String) obj[15]);
                dataTask.setPosition((String) obj[16]);
                dataTask.setUserName((String) obj[17]);
                dataTask.setIsTask((String) obj[18]);
                dataTask.setNamaProject((String) obj[19]);
                dataTask.setNoWo((String) obj[20]);
                dataTask.setIdTaskMember((String) obj[21]);

                if ("Y".equals(dataTask.getIsTask())){
                    dataTask.setEnabledEditPIC(true);
                } else {
                    dataTask.setEnabledEdit(true);
                }


                if (dataTask.getDeadline() != null){
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    String stDate = df.format(dataTask.getDeadline());
                    String stStartDate = df.format(dataTask.getStartDate());
                    dataTask.setStStartDate(stStartDate);
                    dataTask.setStDeadline(stDate);
                }

                for(String value : labelStatus.keySet()){
                    if (value.equals(dataTask.getStatusTask())){
                        dataTask.setTaskStatusText(labelStatus.get(value));
                    }
                }

                String y = dataTask.getPriority();
                for(String value : labelPriority.keySet()){
                    if (value.equals(y)){
                        dataTask.setPriorityText(labelPriority.get(value));
                        break;
                    }
                }

                Integer z = dataTask.getProgres();
                String bar = "";
                for (Integer value : mapBar.keySet()){
                    if (value == z){
                        bar = mapBar.get(value);
                        break;
                    }
                }
                dataTask.setProgresBarText("<div style=\"width:230px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                        "<div align=\"center\" style=\"width:"+bar+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+dataTask.getProgres()+"%</div>");

                listOfTask.add(dataTask);
            }
        }
        return  listOfTask;

    }
}