package com.neurix.pmsapb.project.dao;



import com.neurix.common.dao.GenericDao;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.project.model.*;
import com.vividsolutions.jts.geom.MultiPolygon;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.views.xslt.StringAdapter;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 3/1/2017.
 */
public class ProjectMemberDao extends GenericDao<ItApbProjectMemberEntity,String> {


    @Override
    protected Class<ItApbProjectMemberEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ItApbProjectMemberEntity> getByCriteria(Map mapCriteria) {
        return null;
    }


    public String getNextMemberId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_member')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }
}