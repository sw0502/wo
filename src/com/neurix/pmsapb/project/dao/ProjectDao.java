package com.neurix.pmsapb.project.dao;



import com.neurix.common.constant.CommonConstant;
import com.neurix.common.dao.GenericDao;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.project.model.ItApbProjectEntity;
import com.neurix.pmsapb.project.model.Member;
import com.neurix.pmsapb.project.model.Project;
import com.neurix.pmsapb.project.model.Task;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.*;


/**
 * Created by User on 3/1/2017.
 */
public class ProjectDao extends GenericDao<ItApbProjectEntity,String> {

    @Override
    protected Class getEntityClass() {
        return ItApbProjectEntity.class;
    }

    @Override
    public List<ItApbProjectEntity> getByCriteria(Map mapCriteria) throws HibernateException {

        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectEntity.class);

        if (mapCriteria!=null){
            if (mapCriteria.get("project_id") != null) {
                criteria.add(Restrictions.like("project_id", "%" + (String) mapCriteria.get("project_id")));
            }

            if (mapCriteria.get("project_name") != null) {
                criteria.add(Restrictions.eq("project_name", (String) mapCriteria.get("project_name")));
            } else {
                criteria.add(Restrictions.like("project_name", "%" ));
            }

            if (mapCriteria.get("status") != null) {
                criteria.add(Restrictions.eq("status", (String) mapCriteria.get("status")));
            } else {
                criteria.add(Restrictions.like("status", "%" ));
            }

        }
        List<ItApbProjectEntity> results = criteria.list();
        criteria.addOrder(Order.asc("idProject"));

        return results;
    }

    public List<Project> getProjectByCriteria(Map mapCriteria){
        List<Project> listOfTask = new ArrayList<Project>();
        List<Object[]> result = new ArrayList<Object[]>();

        String idProject = "";
        String noWo = "";
        String projectName = "";
        String statusTask = "";
        String asignTo = "";

        if (mapCriteria != null){
            if(mapCriteria.get("project_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_id"))){
                idProject = (String) mapCriteria.get("project_id");
            } else {
                idProject = "%";
            }
            if(mapCriteria.get("asign_to") != null && !"".equalsIgnoreCase((String) mapCriteria.get("asign_to"))){
                asignTo = (String) mapCriteria.get("asign_to");
            } else {
                asignTo = "%";
            }
            if (mapCriteria.get("no_wo") != null && !"".equalsIgnoreCase((String) mapCriteria.get("no_wo"))){
                noWo = (String) mapCriteria.get("no_wo");
            } else {
                noWo = "%";
            }
            if (mapCriteria.get("project_name") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_name"))){
                projectName = (String) mapCriteria.get("project_name");
            } else {
                projectName = "%";
            }
            if (mapCriteria.get("status_task") != null && !"".equalsIgnoreCase((String) mapCriteria.get("status_task"))){
                statusTask = (String) mapCriteria.get("status_task");
            } else {
                statusTask = "%";
            }

            String SQL = "SELECT * FROM \n" +
                        "   (SELECT project_id, no_wo, project_name FROM it_apb_project) project LEFT JOIN\n" +
                        "   (SELECT task_id, task_name, start_date, deadline, status, priority, progres, project_id, flag, asign_to FROM it_apb_task) task ON project.project_id = task.project_id\n" +
                        "WHERE project.project_id LIKE :idProject \n" +
                        "AND project.no_wo LIKE :noWo \n" +
                        "AND project.project_name LIKE :projectName \n" +
                        "AND task.status LIKE :statusTask \n" +
                        "AND task.flag= 'Y' \n " +
                        "AND task.asign_to = :asignTo";

            result = this.sessionFactory.getCurrentSession().createSQLQuery(SQL)
                    .setParameter("idProject", idProject)
                    .setParameter("noWo", noWo)
                    .setParameter("projectName", projectName)
                    .setParameter("statusTask", statusTask)
                    .setParameter("asignTo", asignTo)
                    .list();

            Map<String, String> labelPriority = new HashMap<String, String>();
            labelPriority.put("1", "<div align='center' style='background-color:#ff6565;padding:10px;border-radius:14px;color:#fff;'>High</div>");
            labelPriority.put("2", "<div align='center' style='background-color:#8be78f;padding:10px;border-radius:14px;color:#fff;'>Normal</div>");
            labelPriority.put("3", "<div align='center' style='background-color:#6582ff;padding:10px;border-radius:14px;color:#fff;'>Low</div>");

            Map<Integer, String> mapBar = new HashMap<Integer, String>();
            mapBar.put(10, "50");
            mapBar.put(20, "70");
            mapBar.put(30, "90");
            mapBar.put(40, "110");
            mapBar.put(50, "130");
            mapBar.put(60, "150");
            mapBar.put(70, "170");
            mapBar.put(80, "190");
            mapBar.put(90, "210");
            mapBar.put(100, "230");

            Map<String, String> labelStatus = new HashMap<String,String>();
            labelStatus.put("1", "<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
            labelStatus.put("2", "<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
            labelStatus.put("3", "<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
            labelStatus.put("4", "<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Initiate</div>");
            labelStatus.put("5", "<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");


            Project project;
            for (Object[] obj: result){
                project = new Project();
                project.setIdProject((String) obj[0]);
                project.setNoWo((String) obj[1]);
                project.setNamaProject((String) obj[2]);
                project.setIdTask((String) obj[3]);
                project.setNamaTask((String) obj[4]);
                project.setStartDate((Date) obj[5]);
                project.setDeadline((Date) obj[6]);
                project.setTaskStatus((String) obj[7]);
                project.setTaskStatusBf((String) obj[7]);
//                String x = (String) obj[7];
                project.setPriority((String) obj[8]);
                project.setTaskProgres((Integer) obj[9]);

                if (project.getDeadline() != null){
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    String stDate = df.format(project.getDeadline());
                    String stStartDate = df.format(project.getStartDate());
                    project.setStStartDate(stStartDate);
                    project.setStDeadline(stDate);
                }

                for(String value : labelStatus.keySet()){
                    if (value.equals(project.getTaskStatus())){
                        project.setTaskStatusText(labelStatus.get(value));
                    }
                }

                String y = project.getPriority();
                for(String value : labelPriority.keySet()){
                    if (value.equals(y)){
                        project.setPriorityText(labelPriority.get(value));
                        break;
                    }
                }

                Integer z = project.getTaskProgres();
                String bar = "";
                for (Integer value : mapBar.keySet()){
                    if (value == z){
                        bar = mapBar.get(value);
                        break;
                    }
                }
                project.setProgresBarText("<div style=\"width:230px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                        "<div align=\"center\" style=\"width:"+bar+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+project.getTaskProgres()+"%</div>");


                listOfTask.add(project);
            }
        }
        return  listOfTask;
    }

    public List<Project> getSearchByCriteria(Map mapCriteria) throws HibernateException{
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectEntity.class);


        List<Project> listOfResult = new ArrayList<Project>();

        String query = "";
        String idProject = "";
        String namaProject = "";
        String idTypeProject = "";
        String status = "";
        String idDivisi = "";

        List<Object[]> results = new ArrayList<Object[]>();

        if (mapCriteria!=null) {
            if (mapCriteria.get("project_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_id"))) {
                idProject = (String) mapCriteria.get("project_id");
             } else {
                idProject = "%";
            }
            if (mapCriteria.get("project_name") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_name"))) {
                namaProject = (String) mapCriteria.get("project_name");
            } else {
                namaProject = "%";
            }
            if (mapCriteria.get("status") != null && !"".equalsIgnoreCase((String) mapCriteria.get("status"))) {
                status = (String) mapCriteria.get("status");
            } else {
                status = "%";
            }
            if (mapCriteria.get("type_project_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("type_project_id"))) {
                idTypeProject = (String) mapCriteria.get("type_project_id");
            } else {
                idTypeProject = "%";
            }
            if (mapCriteria.get("divisi") != null && !"".equalsIgnoreCase((String) mapCriteria.get("divisi"))) {
                idDivisi = (String) mapCriteria.get("divisi");
            } else {
                idDivisi = "%";
            }


            query = "select \n" +
                        "a.project_id,\n" +
                        "a.project_name,\n" +
                        "d.member_id,\n" +
//                        "a.project_manager, \n" +
                        "b.user_name,\n" +
                        "a.deadline,\n" +
                        "a.status,\n" +
                        "a.start_date, \n" +
                        "a.no_wo, \n" +
                        "a.type_project_id, \n" +
                        "c.type_project_name, \n" +
                        "a.flag, \n" +
                        "a.action, \n" +
                        "a.alat_detail_id, \n" +
                        "a.last_update, \n" +
                        "a.last_update_who, \n" +
                        "a.progres, \n" +
                        "a.divisi \n" +
                        "from it_apb_project a\n" +
//                        "inner join im_users b on b.user_id = a.project_manager \n" +
                        "inner join im_type_project c on c.type_project_id = a.type_project_id \n" +
                        "inner join it_apb_project_member d on d.project_id = a.project_id \n" +
                        "inner join im_users b on b.user_id = d.member_id \n" +
                        "where a.project_id like :idProject \n" +
                        "and a.project_name like :namaProject \n" +
                        "and a.status like :status \n" +
                        "and a.type_project_id like :idTypeProject \n" +
                        "and a.flag LIKE 'Y' \n" +
                        "and d.flag LIKE 'Y' \n" +
                        "and d.position_id = '5' \n" +
                        "and a.divisi like :idDivisi \n" +
                        "order by a.project_id DESC ";


                results = this.sessionFactory.getCurrentSession()
                        .createSQLQuery(query)
                        .setParameter("idProject", idProject)
                        .setParameter("namaProject", namaProject)
                        .setParameter("status", status)
                        .setParameter("idTypeProject", idTypeProject)
                        .setParameter("idDivisi", idDivisi)
                        .list();

            StringBuilder imgBuf = new StringBuilder();

            String roleAs = CommonUtil.roleAsLogin();

            Project resultProject;
            for (Object[] row : results) {
                resultProject = new Project();
                resultProject.setIdProject((String) row[0]);
                resultProject.setNamaProject((String) row[1]);
                resultProject.setManagerProject((String) row[2]);
                resultProject.setNamaProjectManager((String) row[3]);
                resultProject.setDeadline((Date) row[4]);
                resultProject.setStatus((String) row[5]);
                resultProject.setStartDate((Date) row[6]);
                resultProject.setNoWo((String) row[7]);
                resultProject.setIdTypeProject((String) row[8]);
                resultProject.setNamaTypeProject((String) row[9]);
                resultProject.setFlag((String) row[10]);
                resultProject.setAction((String) row[11]);
                resultProject.setIdAlatDetail((String) row[12]);
                resultProject.setLastUpdate((Timestamp) row[13]);
                resultProject.setLastUpdateWho((String) row[14]);
                resultProject.setProgres((Integer) row[15]);
                resultProject.setIdDivisi((String) row[16]);

                int progres = resultProject.getProgres();
                int hasil = progres + 50;
                resultProject.setBar(String.valueOf(hasil));

                String presentase = String.valueOf(progres);
                String barWidth = resultProject.getBar();

//                resultTask.setProgresBar("<div align='center' style='background-color:#767676;padding:10px;border-radius:10px;color:#fff;width:"+barWidth+"px;'>"+presentase+"%</div>");
                resultProject.setProgresBar("<div style=\"width:150px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                        "<div align=\"center\" style=\"width:"+barWidth+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+presentase+"%</div>");

                if (resultProject.getDeadline() != null){
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    String stDate = df.format(resultProject.getDeadline());
                    String stStartDate = df.format(resultProject.getStartDate());
                    resultProject.setStStartDate(stStartDate);
                    resultProject.setStDeadline(stDate);
                }

                java.util.Date utilDate = new java.util.Date();
                Date today = new Date(utilDate.getTime());
                Date startDate = resultProject.getStartDate();


                if ("1".equalsIgnoreCase(resultProject.getStatus()) && today.after(startDate)){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
                } else if ("2".equalsIgnoreCase(resultProject.getStatus())){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
                }else if ("3".equalsIgnoreCase(resultProject.getStatus())){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
                }else if("1".equalsIgnoreCase(resultProject.getStatus())&& today.before(startDate)){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                }else if("4".equalsIgnoreCase(resultProject.getStatus())){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                }

                listOfResult.add(resultProject);

            }
        }
        return listOfResult;

    }

    public List<Project> getSearchByDate(Project searchProject) throws HibernateException{
        List<Project> listOfResult = new ArrayList<Project>();

        String query = "";
        String bulan = searchProject.getBulan();
        String tahun = searchProject.getTahun();
//
//        String idProject = searchProject.getIdProject();
//        String namaProject = searchProject.getNamaProject();
//        String idTypeProject = searchProject.getIdTypeProject();
//        String status = searchProject.getStatus();


        String idProject = "";
        String namaProject = "";
        String idTypeProject = "";
        String status = "";

        if("".equalsIgnoreCase(searchProject.getIdProject())){
            idProject = "%";
        } else{
            idProject = searchProject.getIdProject();
        } if("".equalsIgnoreCase(searchProject.getNamaProject())){
            namaProject = "%";
        }else{
            namaProject = searchProject.getNamaProject();
        }if("".equalsIgnoreCase(searchProject.getStatus())){
            status = "%";
        }else{
            status = searchProject.getStatus();
        }if("".equalsIgnoreCase(searchProject.getIdTypeProject())){
            idTypeProject = "%";
        }else{
            idTypeProject = searchProject.getIdTypeProject();
        }



        if(bulan != null && tahun != null) {

            double dBulan = Double.valueOf(bulan);
            double dTahun = Double.valueOf(tahun);

            query = "select \n" +
                    "a.project_id,\n" +
                    "a.project_name,\n" +
                    "d.member_id, \n" +
//                    "a.project_manager,\n" +
                    "b.user_name,\n" +
                    "a.start_date,\n" +
                    "a.deadline,\n" +
                    "a.status,\n" +
                    "a.no_wo, \n" +
                    "a.type_project_id, \n" +
                    "c.type_project_name, \n" +
                    "a.flag, \n" +
                    "a.action, \n" +
                    "a.alat_detail_id, \n" +
                    "a.last_update, \n" +
                    "a.last_update_who, \n" +
                    "a.progres, \n" +
                    "a.divisi \n" +
                    "from it_apb_project a\n" +
//                    "inner join im_users b on b.user_id = a.project_manager \n" +
                    "inner join im_type_project c on c.type_project_id = a.type_project_id \n" +
                    "inner join it_apb_project_member d on d.project_id = a.project_id \n" +
                    "inner join im_users b on b.user_id = d.member_id \n" +
                    "where EXTRACT(YEAR from a.start_date) = :dTahun \n" +
                    "and EXTRACT(MONTH from a.start_date) = :dBulan \n" +
                    "and a.project_id like :idProject \n" +
                    "and a.project_name like :namaProject \n" +
                    "and a.type_project_id like :idTypeProject \n" +
                    "and d.flag like 'Y' \n" +
                    "and c.flag like 'Y' \n" +
                    "and a.status like :status ";

            List<Object[]> results = new ArrayList<Object[]>();
            results = this.sessionFactory.getCurrentSession()
                    .createSQLQuery(query)
                    .setParameter("dBulan", dBulan)
                    .setParameter("dTahun", dTahun)
                    .setParameter("idProject", idProject)
                    .setParameter("namaProject", namaProject)
                    .setParameter("idTypeProject", idTypeProject)
                    .setParameter("status",status)
                    .list();

            Project resultProject;
            for (Object[] row : results) {
                resultProject = new Project();
                resultProject.setIdProject((String) row[0]);
                resultProject.setNamaProject((String) row[1]);
                resultProject.setManagerProject((String) row[2]);
                resultProject.setNamaProjectManager((String) row[3]);
                resultProject.setStartDate((Date) row[4]);
                resultProject.setDeadline((Date) row[5]);
                resultProject.setStatus((String) row[6]);
                resultProject.setNoWo((String) row[7]);
                resultProject.setIdTypeProject((String) row[8]);
                resultProject.setNamaTypeProject((String) row[9]);
                resultProject.setFlag((String) row[10]);
                resultProject.setAction((String) row[11]);
                resultProject.setIdAlatDetail((String) row[12]);
                resultProject.setLastUpdate((Timestamp) row[13]);
                resultProject.setLastUpdateWho((String) row[14]);
                resultProject.setProgres((Integer) row[15]);
                resultProject.setIdDivisi((String) row[16]);

                int progres = resultProject.getProgres();
                int hasil = progres + 50;
                resultProject.setBar(String.valueOf(hasil));

                String presentase = String.valueOf(progres);
                String barWidth = resultProject.getBar();

//                resultTask.setProgresBar("<div align='center' style='background-color:#767676;padding:10px;border-radius:10px;color:#fff;width:"+barWidth+"px;'>"+presentase+"%</div>");
                resultProject.setProgresBar("<div style=\"width:150px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                        "<div align=\"center\" style=\"width:"+barWidth+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+presentase+"%</div>");

                java.util.Date utilDate = new java.util.Date();
                Date today = new Date(utilDate.getTime());
                Date startDate = resultProject.getStartDate();


                if ("1".equalsIgnoreCase(resultProject.getStatus()) && today.after(startDate)){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
                } else if ("2".equalsIgnoreCase(resultProject.getStatus())) {
                    resultProject.setNamaStatus("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
                }else if ("3".equalsIgnoreCase(resultProject.getStatus())){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
                }else if("1".equalsIgnoreCase(resultProject.getStatus())&& today.before(startDate)){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                }else if("4".equalsIgnoreCase(resultProject.getStatus())){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                }

                listOfResult.add(resultProject);

            }


        } else if(tahun != null && bulan == null){

            double dTahun = Double.valueOf(tahun);

            query = "select \n" +
                    "a.project_id,\n" +
                    "a.project_name,\n" +
                    "d.member_id, \n" +
//                    "a.project_manager,\n" +
                    "b.user_name,\n" +
                    "a.start_date,\n" +
                    "a.deadline,\n" +
                    "a.status,\n" +
                    "a.no_wo, \n" +
                    "a.type_project_id, \n" +
                    "c.type_project_name, \n" +
                    "a.flag, \n" +
                    "a.action, \n" +
                    "a.alat_detail_id, \n" +
                    "a.last_update, \n" +
                    "a.last_update_who, \n" +
                    "a.progres, \n" +
                    "a.divisi \n" +
                    "from it_apb_project a\n" +
//                    "inner join im_users b on b.user_id = a.project_manager \n" +
                    "inner join im_type_project c on c.type_project_id = a.type_project_id \n" +
                    "inner join it_apb_project_member d on d.project_id = a.project_id \n" +
                    "inner join im_users b on b.user_id = d.member_id \n" +
                    "where EXTRACT(YEAR from a.start_date) = :dTahun \n"+
                    "and a.project_id like :idProject \n" +
                    "and a.project_name like :namaProject \n" +
                    "and a.type_project_id like :idTypeProject \n" +
                    "and d.flag like 'Y' \n" +
                    "and c.flag like 'Y' \n" +
                    "and a.status like :status";

            List<Object[]> results = new ArrayList<Object[]>();
            results = this.sessionFactory.getCurrentSession()
                    .createSQLQuery(query)
//                    .setParameter("dBulan", dBulan)
                    .setParameter("dTahun", dTahun)
                    .setParameter("idProject", idProject)
                    .setParameter("namaProject", namaProject)
                    .setParameter("idTypeProject", idTypeProject)
                    .setParameter("status", status)
                    .list();

            Project resultProject;
            for (Object[] row : results) {
                resultProject = new Project();
                resultProject.setIdProject((String) row[0]);
                resultProject.setNamaProject((String) row[1]);
                resultProject.setManagerProject((String) row[2]);
                resultProject.setNamaProjectManager((String) row[3]);
                resultProject.setStartDate((Date) row[4]);
                resultProject.setDeadline((Date) row[5]);
                resultProject.setStatus((String) row[6]);
                resultProject.setNoWo((String) row[7]);
                resultProject.setIdTypeProject((String) row[8]);
                resultProject.setNamaTypeProject((String) row[9]);
                resultProject.setFlag((String) row[10]);
                resultProject.setAction((String) row[11]);
                resultProject.setIdAlatDetail((String) row[12]);
                resultProject.setLastUpdate((Timestamp) row[13]);
                resultProject.setLastUpdateWho((String) row[14]);
                resultProject.setProgres((Integer) row[15]);
                resultProject.setIdDivisi((String) row[16]);

                int progres = resultProject.getProgres();
                int hasil = progres + 50;
                resultProject.setBar(String.valueOf(hasil));

                String presentase = String.valueOf(progres);
                String barWidth = resultProject.getBar();

//                resultTask.setProgresBar("<div align='center' style='background-color:#767676;padding:10px;border-radius:10px;color:#fff;width:"+barWidth+"px;'>"+presentase+"%</div>");
                resultProject.setProgresBar("<div style=\"width:150px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                        "<div align=\"center\" style=\"width:"+barWidth+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+presentase+"%</div>");

                java.util.Date utilDate = new java.util.Date();
                Date today = new Date(utilDate.getTime());
                Date startDate = resultProject.getStartDate();

                if ("1".equalsIgnoreCase(resultProject.getStatus()) && today.after(startDate)){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
                } else if ("2".equalsIgnoreCase(resultProject.getStatus())) {
                    resultProject.setNamaStatus("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
                }else if ("3".equalsIgnoreCase(resultProject.getStatus())){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
                }else if("1".equalsIgnoreCase(resultProject.getStatus())&& today.before(startDate)){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                }else if("4".equalsIgnoreCase(resultProject.getStatus())){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                }
                listOfResult.add(resultProject);
            }

        }else if(tahun == null && bulan != null){

            double dBulan = Double.valueOf(bulan);

            query = "select \n" +
                    "a.project_id,\n" +
                    "a.project_name,\n" +
                    "d.member_id, \n" +
//                    "a.project_manager,\n" +
                    "b.user_name,\n" +
                    "a.start_date,\n" +
                    "a.deadline,\n" +
                    "a.status,\n" +
                    "a.no_wo, \n" +
                    "a.type_project_id, \n" +
                    "c.type_project_name, \n" +
                    "a.flag, \n" +
                    "a.action, \n" +
                    "a.alat_detail_id, \n" +
                    "a.last_update, \n" +
                    "a.last_update_who, \n" +
                    "a.progres, \n" +
                    "a.divisi \n" +
                    "from it_apb_project a\n" +
//                    "inner join im_users b on b.user_id = a.project_manager \n" +
                    "inner join im_type_project c on c.type_project_id = a.type_project_id \n" +
                    "inner join it_apb_project_member d on d.project_id = a.project_id \n" +
                    "inner join im_users b on b.user_id = d.member_id \n" +
                    "where EXTRACT(MONTH from a.start_date) = :dBulan \n" +
                    "and a.project_id like :idProject \n" +
                    "and a.project_name like :namaProject \n" +
                    "and a.type_project_id like :idTypeProject \n" +
                    "and d.flag like 'Y' \n" +
                    "and c.flag like 'Y' \n" +
                    "and a.status like :status";

            List<Object[]> results = new ArrayList<Object[]>();
            results = this.sessionFactory.getCurrentSession()
                    .createSQLQuery(query)
//                    .setParameter("dBulan", dBulan)
                    .setParameter("dBulan", dBulan)
                    .setParameter("idProject", idProject)
                    .setParameter("namaProject", namaProject)
                    .setParameter("idTypeProject", idTypeProject)
                    .setParameter("status", status)
                    .list();

            Project resultProject;
            for (Object[] row : results) {
                resultProject = new Project();
                resultProject.setIdProject((String) row[0]);
                resultProject.setNamaProject((String) row[1]);
                resultProject.setManagerProject((String) row[2]);
                resultProject.setNamaProjectManager((String) row[3]);
                resultProject.setStartDate((Date) row[4]);
                resultProject.setDeadline((Date) row[5]);
                resultProject.setStatus((String) row[6]);
                resultProject.setNoWo((String) row[7]);
                resultProject.setIdTypeProject((String) row[8]);
                resultProject.setNamaTypeProject((String) row[9]);
                resultProject.setFlag((String) row[10]);
                resultProject.setAction((String) row[11]);
                resultProject.setIdAlatDetail((String) row[12]);
                resultProject.setLastUpdate((Timestamp) row[13]);
                resultProject.setLastUpdateWho((String) row[14]);
                resultProject.setProgres((Integer) row[15]);
                resultProject.setIdDivisi((String) row[16]);

                int progres = resultProject.getProgres();
                int hasil = progres + 50;
                resultProject.setBar(String.valueOf(hasil));

                String presentase = String.valueOf(progres);
                String barWidth = resultProject.getBar();

//                resultTask.setProgresBar("<div align='center' style='background-color:#767676;padding:10px;border-radius:10px;color:#fff;width:"+barWidth+"px;'>"+presentase+"%</div>");
                resultProject.setProgresBar("<div style=\"width:150px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                        "<div align=\"center\" style=\"width:"+barWidth+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+presentase+"%</div>");

                java.util.Date utilDate = new java.util.Date();
                Date today = new Date(utilDate.getTime());
                Date startDate = resultProject.getStartDate();

                if ("1".equalsIgnoreCase(resultProject.getStatus()) && today.after(startDate)){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
                } else if ("2".equalsIgnoreCase(resultProject.getStatus())) {
                    resultProject.setNamaStatus("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
                }else if ("3".equalsIgnoreCase(resultProject.getStatus())){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
                }else if("1".equalsIgnoreCase(resultProject.getStatus())&& today.before(startDate)){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                }else if("4".equalsIgnoreCase(resultProject.getStatus())){
                    resultProject.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                }

                listOfResult.add(resultProject);
            }

        }
        return listOfResult;
    }

    public List<Project> getViewProject(Map mapCriteria) throws HibernateException{
//        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectEntity.class);
        List<Project> listOfResult = new ArrayList<Project>();

        String query = "select \n" +
                "a.project_id,\n" +
                "a.project_name,\n" +
                "b.member_id,\n" +
                "c.user_name,\n" +
                "d.task_id,\n" +
                "d.task_name\n" +
                "from\n" +
                "it_apb_project a\n" +
                "inner join it_apb_project_member b on b.project_id = a.project_id\n" +
                "inner join im_users c on c.user_id = b.member_id\n" +
                "left outer join it_apb_task d on d.asign_to = b.member_id\n" +
                "where a.project_id LIKE :idProject \n";

        String idProject = "";

        List<Object[]> results = new ArrayList<Object[]>();

        if (mapCriteria!=null) {
            if (mapCriteria.get("project_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_id"))) {
                idProject = (String) mapCriteria.get("project_id");
            } else {
                idProject = "%";
            }

            results = this.sessionFactory.getCurrentSession()
                    .createSQLQuery(query)
                    .setParameter("idProject", idProject)
                    .list();

            Project resultProject;
            for (Object[] row : results) {
                resultProject = new Project();
                resultProject.setIdProject((String) row[0]);
                resultProject.setNamaProject((String) row[1]);
                resultProject.setIdMember((String) row[2]);
                resultProject.setNamaMember((String) row[3]);
                resultProject.setIdTask((String) row[4]);
                resultProject.setNamaTask((String) row[5]);
                listOfResult.add(resultProject);
            }
        }
        return listOfResult;

    }

    public List<Task> getTaskByCriteria(Map mapCriteria) throws HibernateException{
        List<Task> listOfResult = new ArrayList<Task>();

        String query = "select\n" +
                "a.task_id,\n" +
                "a.task_name,\n" +
                "a.status,\n" +
                "a.deadline,\n" +
                "a.priority,\n" +
                "a.note,\n" +
                "b.user_name,\n" +
                "a.progres, \n" +
                "a.project_id," +
                "a.asign_to, \n" +
                "a.start_date,\n" +
                "a.flag \n" +
                "from \n" +
                "it_apb_task a\n" +
                "left outer JOIN im_users b on b.user_id = a.asign_to\n" +
                "where \n" +
                "project_id LIKE :idProject \n" +
                "and a.flag LIKE 'Y' \n" +
                "order by a.deadline";

        String idProject = "";

        List<Object[]> results = new ArrayList<Object[]>();

        if (mapCriteria!=null) {
            if (mapCriteria.get("project_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_id"))) {
                idProject = (String) mapCriteria.get("project_id");
            } else {
                idProject = "%";
            }

            results = this.sessionFactory.getCurrentSession()
                    .createSQLQuery(query)
                    .setParameter("idProject", idProject)
                    .list();

            Task resultTask;
            for (Object[] row : results) {
                resultTask = new Task();
                resultTask.setIdTask((String) row[0]);
                resultTask.setNamaTask((String) row[1]);
                resultTask.setStatusTask((String) row[2]);
                resultTask.setDeadline((Date) row[3]);
                resultTask.setPriority((String) row[4]);
                resultTask.setNote((String) row[5]);
                resultTask.setUserAsignTo((String) row[6]);
                resultTask.setProgres((Integer) row[7]);
                resultTask.setIdProject((String) row[8]);
                resultTask.setAsignTo((String) row[9]);
                resultTask.setStartDate((Date) row[10]);
                resultTask.setFlag((String) row[11]);
                resultTask.setStatusBefore((String) row[2]);
                resultTask.setProgresBefore((Integer) row[7]);

                if("Y".equalsIgnoreCase(resultTask.getFlag())){
                    resultTask.setStatusFlag("ACTIVE");
                }else{
                    resultTask.setStatusFlag("NON ACTIVE");
                }

                if (resultTask.getDeadline() != null || resultTask.getStartDate() != null){
                DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String stDate = df.format(resultTask.getDeadline());
                    String stStartDate = df.format(resultTask.getStartDate());
                    resultTask.setStDeadline(stDate);
                    resultTask.setStStartDate(stStartDate);
                }

                java.util.Date utilDate = new java.util.Date();
                Date today = new Date(utilDate.getTime());
                Date startDateTask = resultTask.getStartDate();

                if ("1".equalsIgnoreCase(resultTask.getStatusTask())&& today.after(startDateTask)){
                    resultTask.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
                } else if ("2".equalsIgnoreCase(resultTask.getStatusTask())) {
                    resultTask.setNamaStatus("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
                }else if("3".equalsIgnoreCase(resultTask.getStatusTask())) {
                    resultTask.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
                } else if("4".equalsIgnoreCase(resultTask.getStatusTask()) && today.before(startDateTask)){
                    resultTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Not Asigned To and Waiting Start</div>");
                } else if("4".equalsIgnoreCase(resultTask.getStatusTask())) {
                    resultTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Not Asigned To</div>");
                } else if (resultTask.getProgres() == 100) {
                    resultTask.setNamaStatus("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
                } else {
                    resultTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                }


                if("1".equalsIgnoreCase(resultTask.getPriority())){
                    resultTask.setNamaPriority("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>High</div>");
                } else if("2".equalsIgnoreCase(resultTask.getPriority())){
                    resultTask.setNamaPriority("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Normal</div>");
                } else{
                    resultTask.setNamaPriority("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>Low</div>");
                }

//                if (resultTask.getProgres() == 10){
//                    resultTask.setBar("50");
//                }else if(resultTask.getProgres() == 20){
//                    resultTask.setBar("70");
//                }else if(resultTask.getProgres() == 30){
//                    resultTask.setBar("90");
//                }else if(resultTask.getProgres() == 40){
//                    resultTask.setBar("110");
//                }else if(resultTask.getProgres() == 50){
//                    resultTask.setBar("130");
//                }else if(resultTask.getProgres() == 60){
//                    resultTask.setBar("150");
//                }else if(resultTask.getProgres() == 70){
//                    resultTask.setBar("170");
//                }else if(resultTask.getProgres() == 80){
//                    resultTask.setBar("190");
//                }else if(resultTask.getProgres() == 90){
//                    resultTask.setBar("210");
//                }else if(resultTask.getProgres() == 100){
//                    resultTask.setBar("230");
//                }

                resultTask.getProgres();
                int progres = resultTask.getProgres();
                int hasil = progres + 50;
                resultTask.setBar(String.valueOf(hasil));

                String presentase = String.valueOf(progres);
                String barWidth = resultTask.getBar();

//                resultTask.setProgresBar("<div align='center' style='background-color:#767676;padding:10px;border-radius:10px;color:#fff;width:"+barWidth+"px;'>"+presentase+"%</div>");
                resultTask.setProgresBar("<div style=\"width:150px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                        "<div align=\"center\" style=\"width:"+barWidth+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+presentase+"%</div>");
                listOfResult.add(resultTask);
            }
        }
        return listOfResult;
    }

    public List<Member> getMemberByCriteria(Map mapCriteria) throws HibernateException{
        List<Member> listOfResult = new ArrayList<Member>();

        String query = "select \n" +
                "a.member_id,\n" +
                "b.user_name,\n" +
                "b.photo_url,\n" +
                "a.position_id,\n" +
                "c.position_name, \n" +
                "a.project_member_id, \n" +
                "a.project_id, \n" +
                "a.flag \n" +
                "from\n" +
                "it_apb_project_member a \n" +
                "inner join im_users b on b.user_id = a.member_id \n" +
                "inner join im_position c on c.position_id = a.position_id \n" +
                "where \n" +
                "a.project_id LIKE :idProject \n" +
                "and a.flag LIKE 'Y' \n" +
                "and a.member_id LIKE :idMember";

        String idProject = "";
        String idMember = "";

        List<Object[]> results = new ArrayList<Object[]>();

        if (mapCriteria!=null) {
            if (mapCriteria.get("project_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("project_id"))) {
                idProject = (String) mapCriteria.get("project_id");
            } else {
                idProject = "%";
            }if (mapCriteria.get("member_id") != null && !"".equalsIgnoreCase((String) mapCriteria.get("member_id"))) {
                idMember = (String) mapCriteria.get("member_id");
            } else {
                idMember = "%";
            }

            results = this.sessionFactory.getCurrentSession()
                    .createSQLQuery(query)
                    .setParameter("idProject", idProject)
                    .setParameter("idMember", idMember)
                    .list();


            Member resultMember;
            for (Object[] row : results) {
                resultMember = new Member();
                resultMember.setIdMember((String) row[0]);
                resultMember.setNamaMember((String) row[1]);
                resultMember.setPhotoUrl((String) row[2]);
                resultMember.setbPositionId((BigInteger) row[3]);
                BigInteger b = resultMember.getbPositionId();
                resultMember.setPositionId(b.longValue());
                resultMember.setStPositionId(String.valueOf(resultMember.getPositionId()));
//                resultMember.setStPositionId(String.valueOf((Long) row[3]));
                resultMember.setPositionName((String) row[4]);
                resultMember.setIdProjectMember((String) row[5]);
                resultMember.setIdProject((String) row[6]);
                resultMember.setFlag((String) row[7]);
                if("Y".equalsIgnoreCase(resultMember.getFlag())){
                    resultMember.setStatusFlag("ACTIVE");
                }else{
                    resultMember.setStatusFlag("NON ACTIVE");
                }
                if(resultMember.getPhotoUrl() != null){
                    String filePath = "/pmsapb"+CommonConstant.RESOURCE_PATH_USER_UPLOAD + resultMember.getPhotoUrl();
                    resultMember.setPhotoPath("<img src=\""+filePath+"\" style=\"border-radius:15px;width:30px;height:30px;\" width=\"30px\" height=\"30px\" />\n");
                }
                listOfResult.add(resultMember);
            }
        }
        return listOfResult;
    }

    public List<Task> getSumAndCountTask(String idProject) throws HibernateException{
        List<Task> listOfTask = new ArrayList<Task>();
        String id;
        if (idProject != null){
             id = idProject;
        } else {
             id = "%";
        }

        String query= "SELECT count(task_id), sum(progres) from it_apb_task where project_id LIKE :id and flag = 'Y'";

        List<Object[]> results = new ArrayList<Object[]>();

        results = this.sessionFactory.getCurrentSession()
                .createSQLQuery(query)
                .setParameter("id", id)
                .list();

        Task dataTask;
        for (Object[] row: results){
            dataTask = new Task();
            dataTask.setCountOfRow((BigInteger) row[0]);
            dataTask.setSumProgres((BigInteger) row[1]);
            listOfTask.add(dataTask);
        }

        return listOfTask;
    }

    public String getNextProjectId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_project')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;

    }

//    public <Project> getSumOfProgres

}