package com.neurix.pmsapb.project.dao;



import com.neurix.common.dao.GenericDao;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.project.model.*;
import com.vividsolutions.jts.geom.MultiPolygon;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.views.xslt.StringAdapter;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 3/1/2017.
 */
public class ProjectIssueDao extends GenericDao<ItApbProjectIssueEntity,String> {


    @Override
    protected Class<ItApbProjectIssueEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ItApbProjectIssueEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectIssueEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("project_id")!=null) {
                criteria.add(Restrictions.eq("idProject", (String) mapCriteria.get("project_id")));
            }
            if (mapCriteria.get("issue_id")!=null) {
                criteria.add(Restrictions.ilike("idIssue", "%" + (String)mapCriteria.get("issue_id") + "%"));
            }
            if (mapCriteria.get("issue_name")!=null) {
                criteria.add(Restrictions.ilike("namaIssue", "%" + (String)mapCriteria.get("issue_name") + "%"));
            }
        }
//        criteria.add(Restrictions.eq("IdIssue", mapCriteria.get("issue_id")));
        criteria.addOrder(Order.asc("idIssue"));
        List<ItApbProjectIssueEntity> results = criteria.list();

        return results;
    }

    public List<ItApbProjectIssueEntity> getCriteriaById(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectIssueEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("project_id") != null) {
                criteria.add(Restrictions.ilike("idProject", "%" + (String) mapCriteria.get("project_id")+"%"));
            }
            if (mapCriteria.get("issue_id") != null) {
                criteria.add(Restrictions.ilike("idIssue", "%" + (String) mapCriteria.get("issue_id")+ "%"));
            }
            if (mapCriteria.get("issue_name") != null) {
                criteria.add(Restrictions.ilike("namaIssue", "%" + (String)mapCriteria.get("issue_name") + "%"));
            }
        }
//        criteria.add(Restrictions.ilike("idIssue", mapCriteria.get("issue_id")));
        criteria.addOrder(Order.asc("idIssue"));
        List<ItApbProjectIssueEntity> results = criteria.list();

        return results;
    }


    public String getNextIssueId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_issue')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }


}