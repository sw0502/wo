package com.neurix.pmsapb.project.action;

import com.neurix.authorization.user.bo.UserBo;
import com.neurix.authorization.user.model.User;
import com.neurix.common.action.BaseTransactionAction;
import com.neurix.common.constant.CommonConstant;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.alatapb.alatdetail.bo.AlatDetailBo;
import com.neurix.pmsapb.alatapb.alatdetail.model.AlatDetail;
import com.neurix.pmsapb.master.divisi.bo.DivisiBo;
import com.neurix.pmsapb.master.divisi.model.Divisi;
import com.neurix.pmsapb.master.doc.bo.DocBo;
import com.neurix.pmsapb.master.doc.model.Document;
import com.neurix.pmsapb.master.priority.bo.PriorityBo;
import com.neurix.pmsapb.master.priority.model.Priority;
import com.neurix.pmsapb.master.progres.bo.ProgresBo;
import com.neurix.pmsapb.master.progres.model.Progres;
import com.neurix.pmsapb.master.status.bo.StatusBo;
import com.neurix.pmsapb.master.status.model.Status;
import com.neurix.pmsapb.master.typeproject.model.TypeProject;
import com.neurix.pmsapb.project.bo.ProjectBo;
import com.neurix.pmsapb.project.model.*;
import com.neurix.pmsapb.project.taskmember.bo.TaskMemberBo;
import com.neurix.pmsapb.project.taskmember.model.TaskMember;
import com.neurix.pmsapb.project.taskmemberchange.bo.TaskMemberChangeBo;
import com.neurix.pmsapb.project.taskmemberchange.model.TaskMemberChange;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

//

/**
 * Created by User on 3/1/2017.
 */
public class ProjectAction extends BaseTransactionAction {
    protected static transient Logger logger = Logger.getLogger(ProjectAction.class);
    private Project project;
    private Member member;
    private Task task;
    private Document document;
    private ProjectIssue projectIssue;
    private AlatDetail alatDetail;
    private TaskMember taskMember;
//    private Divisi divisi;

    private String id;
    private String id1;
    private String idTask;
    private String fileName;
    private String atr;

    private ProjectBo projectBoProxy;
    private StatusBo statusBoProxy;
    private PriorityBo priorityBoProxy;
    private ProgresBo progresBoProxy;
    private DocBo docBoProxy;
    private UserBo userBoProxy;
    private AlatDetailBo alatDetailBoProxy;
    private DivisiBo divisiBoProxy;
    private TaskMemberBo taskMemberBoProxy;

    private File fileUploadDoc;
    private String fileUploadDocContentType;
    private String fileUploadDocFileName;

    private List<Status> listOfComboStatus = new ArrayList<Status>();
    private List<Priority> listOfComboPriority = new ArrayList<Priority>();
    private List<Progres> listOfComboProgres = new ArrayList<Progres>();
    private List<Member> listOfComboAsignTo = new ArrayList<Member>();
    private List<TypeProject> listOfComboTypeProject = new ArrayList<TypeProject>();
    private List<Member> listOfComboMemberTask = new ArrayList<Member>();
    private List<Divisi> listOfComboDivisi = new ArrayList<Divisi>();

    public String getAtr() {
        return atr;
    }

    public void setAtr(String atr) {
        this.atr = atr;
    }

    public TaskMemberBo getTaskMemberBoProxy() {
        return taskMemberBoProxy;
    }

    public void setTaskMemberBoProxy(TaskMemberBo taskMemberBoProxy) {
        this.taskMemberBoProxy = taskMemberBoProxy;
    }

    public TaskMember getTaskMember() {
        return taskMember;
    }

    public void setTaskMember(TaskMember taskMember) {
        this.taskMember = taskMember;
    }

    public DivisiBo getDivisiBoProxy() {
        return divisiBoProxy;
    }

    public void setDivisiBoProxy(DivisiBo divisiBoProxy) {
        this.divisiBoProxy = divisiBoProxy;
    }

    public List<Divisi> getListOfComboDivisi() {
        return listOfComboDivisi;
    }

    public void setListOfComboDivisi(List<Divisi> listOfComboDivisi) {
        this.listOfComboDivisi = listOfComboDivisi;
    }

    public AlatDetail getAlatDetail() {
        return alatDetail;
    }

    public void setAlatDetail(AlatDetail alatDetail) {
        this.alatDetail = alatDetail;
    }

    public AlatDetailBo getAlatDetailBoProxy() {
        return alatDetailBoProxy;
    }

    public void setAlatDetailBoProxy(AlatDetailBo alatDetailBoProxy) {
        this.alatDetailBoProxy = alatDetailBoProxy;
    }

    public List<Member> getListOfComboMemberTask() {
        return listOfComboMemberTask;
    }

    public void setListOfComboMemberTask(List<Member> listOfComboMemberTask) {
        this.listOfComboMemberTask = listOfComboMemberTask;
    }

    public ProjectIssue getProjectIssue() {
        return projectIssue;
    }

    public void setProjectIssue(ProjectIssue projectIssue) {
        this.projectIssue = projectIssue;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        ProjectAction.logger = logger;
    }

    public UserBo getUserBoProxy() {
        return userBoProxy;
    }

    public void setUserBoProxy(UserBo userBoProxy) {
        this.userBoProxy = userBoProxy;
    }

    public List<TypeProject> getListOfComboTypeProject() {
        return listOfComboTypeProject;
    }

    public void setListOfComboTypeProject(List<TypeProject> listOfComboTypeProject) {
        this.listOfComboTypeProject = listOfComboTypeProject;
    }

    public File getFileUploadDoc() {
        return fileUploadDoc;
    }

    public void setFileUploadDoc(File fileUploadDoc) {
        this.fileUploadDoc = fileUploadDoc;
    }

    public String getFileUploadDocContentType() {
        return fileUploadDocContentType;
    }

    public void setFileUploadDocContentType(String fileUploadDocContentType) {
        this.fileUploadDocContentType = fileUploadDocContentType;
    }

    public String getFileUploadDocFileName() {
        return fileUploadDocFileName;
    }

    public void setFileUploadDocFileName(String fileUploadDocFileName) {
        this.fileUploadDocFileName = fileUploadDocFileName;
    }

    private List<Task> listOfComboTask = new ArrayList<Task>();

    public List<Task> getListOfComboTask() {
        return listOfComboTask;
    }

    public void setListOfComboTask(List<Task> listOfComboTask) {
        this.listOfComboTask = listOfComboTask;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProjectBo getProjectBoProxy() {
        return projectBoProxy;
    }

    public void setProjectBoProxy(ProjectBo projectBoProxy) {
        this.projectBoProxy = projectBoProxy;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public List<Status> getListOfComboStatus() {
        return listOfComboStatus;
    }

    public void setListOfComboStatus(List<Status> listOfComboStatus) {
        this.listOfComboStatus = listOfComboStatus;
    }

    public List<Priority> getListOfComboPriority() {
        return listOfComboPriority;
    }


    public void setListOfComboPriority(List<Priority> listOfComboPriority) {
        this.listOfComboPriority = listOfComboPriority;
    }

    public List<Progres> getListOfComboProgres() {
        return listOfComboProgres;
    }

    public void setListOfComboProgres(List<Progres> listOfComboProgres) {
        this.listOfComboProgres = listOfComboProgres;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public StatusBo getStatusBoProxy() {
        return statusBoProxy;
    }

    public void setStatusBoProxy(StatusBo statusBoProxy) {
        this.statusBoProxy = statusBoProxy;
    }

    public PriorityBo getPriorityBoProxy() {
        return priorityBoProxy;
    }

    public void setPriorityBoProxy(PriorityBo priorityBoProxy) {
        this.priorityBoProxy = priorityBoProxy;
    }

    public ProgresBo getProgresBoProxy() {
        return progresBoProxy;
    }

    public void setProgresBoProxy(ProgresBo progresBoProxy) {
        this.progresBoProxy = progresBoProxy;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public DocBo getDocBoProxy() {
        return docBoProxy;
    }

    public void setDocBoProxy(DocBo docBoProxy) {
        this.docBoProxy = docBoProxy;
    }

    public List<Member> getListOfComboAsignTo() {
        return listOfComboAsignTo;
    }

    public void setListOfComboAsignTo(List<Member> listOfComboAsignTo) {
        this.listOfComboAsignTo = listOfComboAsignTo;
    }

    public String getId1() {
        return id1;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    @Override
    public String initForm() {
        clearMessages();
        clearActionErrors();
        setProject(new Project());
        HttpSession session = ServletActionContext.getRequest().getSession();
//        Project project = new Project();
//        projectBoProxy.updateStatusProject(project);
        projectBoProxy.updateStatusByDate();
        session.removeAttribute("listOfResult");
        session.removeAttribute("listOfProject");
        return INPUT;
    }

    public String initFormTask() {
        logger.info("[ProjectAction.initFormTask] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        logger.info("[ProjectAction.initFormTask] end process >>>");
        return "input_task";
    }

    public String searchTask(){
        logger.info("[ProjectAction.search] start process >>>");

        Task task = getTask();
        String idLogin = CommonUtil.userIdLogin();
        String userRole = CommonUtil.roleAsLogin();

        List<Task> listOfSearchTask = new ArrayList<Task>();

        try {
            listOfSearchTask = projectBoProxy.getSearchListTask(task, userRole, idLogin);
//            listOfSearchTask = projectBoProxy.getProjectTaskByCriteria(task, userRole, idLogin);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "TaskBO.getSearchTaskByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.save] Error when searching task by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin" );
            return "failure";
        }

//        try {
//            projectBoProxy.updateTaskLastProgres(task);
//        } catch (GeneralBOException e) {
//            Long logId = null;
//            try {
//                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "projectAction.saveEditListTask");
//            } catch (GeneralBOException e1) {
//                logger.error("[projectAction.saveEditListTask] Error when saving error,", e1);
//            }
//            logger.error("[projectAction.saveEditListTask] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
//            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
//            return ERROR;
//        }




        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfSearchTask);

        logger.info("[ProjectAction.search] end process <<<");
        return "succes_task";
    }

    public String editListTask(){
        logger.info("[ProjectAction.editListTask] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfTask = (List) session.getAttribute("listOfResult");

        if(id != null && idTask != null) {
            if (listOfTask!=null){
                for (Task dataTask : listOfTask) {
                    if (idTask.equals(dataTask.getIdTask()) && "N".equals(dataTask.getIsTask())) {
                        setTask(dataTask);
                        break;
                    }
                }
            }else{
                setTask(new Task());
            }
        } else {
            setTask(new Task());
        }
        logger.info("[ProjectAction.editListTask] end process <<<");
        return "view_edit_list_task";
    }

    public String editListTaskPIC(){
        logger.info("[ProjectAction.editListTask] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfTask = (List) session.getAttribute("listOfResult");

        if(id != null && idTask != null) {
            if (listOfTask!=null){
                for (Task dataTask : listOfTask) {
                    if (idTask.equals(dataTask.getIdTask()) && "Y".equals(dataTask.getIsTask())) {
                        setTask(dataTask);
                        break;
                    }
                }
            }else{
                setTask(new Task());
            }
        } else {
            setTask(new Task());
        }
        logger.info("[ProjectAction.editListTask] end process <<<");
        return "view_edit_list_task_pic";
    }

    public String addIssue(){
        logger.info("[ProjectAction.addIssue] start process >>>");

        //get data from session
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Project> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {
            if (listOfResult != null) {
                for (Project projectOfList : listOfResult) {
                    if (id.equalsIgnoreCase(projectOfList.getIdProject())) {
                        setProject(projectOfList);
                        break;
                    }
                }
            } else {
                setProject(new Project());
            }
        } else {
            setProject(new Project());
        }

        logger.info("[ProjectAction.addIssue] end process <<<");

        return "add_issue";
    }

    public String saveIssue(){
        logger.info("[projectAction.saveIssue] start process >>>");
        try{
            Project entryProjectIssue = getProject();
            String idLogin = CommonUtil.userIdLogin();
            entryProjectIssue.setIdMember(idLogin);

            projectBoProxy.saveIssue(entryProjectIssue);

        }  catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "projectAction.saveIssue");
            } catch (GeneralBOException e1) {
                logger.error("[projectAction.saveIssue] Error when saving error,", e1);
            }
            logger.error("[projectAction.saveIssue] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[projectAction.saveIssue] end process <<<");
        return "save_issue";
    }

    public String initComboTaskName(){
        Task dataTask = new Task();
        dataTask.setIdProject(dataTask.getIdProject());

        List<Task> listOfTask = new ArrayList<Task>();
        try {
            listOfTask = projectBoProxy.getSearchTaskCombo(dataTask);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "UserBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboOperatorGps] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboOperatorGps] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        listOfComboTask.addAll(listOfTask);

        return "init_combo_task_name";
    }


    public List initComboUser(String query) {
        logger.info("[PermohonanLahanAction.initComboLokasiKebun] start process >>>");

        List<User> listOfUser = new ArrayList();
        List<User> listOfUserNew = new ArrayList<User>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        UserBo userBo = (UserBo) ctx.getBean("userBoProxy");
//        HttpSession session = WebContextFactory.get().getSession();


        try {
            listOfUser = userBo.getComboUserWithCriteria(query);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = userBo.saveErrorMessage(e.getMessage(), "DesaBo.getComboDesaWithCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when get combo lokasi kebun," + "[" + logId + "] Found problem when retrieving combo lokasi kebun data, please inform to your admin.", e);
        }

        logger.info("[PermohonanLahanAction.initComboLokasiKebun] end process <<<");

//        if (listOfUser != null){
//            User dataUser = null;
//            for (User userData :listOfUser){
//                if (!"1".equalsIgnoreCase(userData.getPositionId())){
//                    dataUser = new User();
//                    dataUser.setUserId(userData.getUserId());
//                    dataUser.setUsername(userData.getUsername());
//                    dataUser.setAreaName(userData.getAreaName());
//                    dataUser.setAction(userData.getAction());
//                    dataUser.setNamaRole(userData.getNamaRole());
//                    dataUser.setAreaId(userData.getAreaId());
//                    dataUser.setBranchId(userData.getBranchId());
//                    dataUser.setRole(userData.getRole());
//                    dataUser.setRoleId(userData.getRoleId());
//                    dataUser.setPositionName(userData.getPositionName());
//                    dataUser.setFlag(userData.getFlag());
//                    dataUser.setStFlag(userData.getStFlag());
//                    dataUser.setCreatedDate(userData.getCreatedDate());
//                    dataUser.setCreatedWho(userData.getCreatedWho());
//                    dataUser.setLastUpdateWho(userData.getLastUpdateWho());
//                    dataUser.setLastUpdate(userData.getLastUpdate());
//                    dataUser.setEmail(userData.getEmail());
//                    listOfUserNew.add(dataUser);
//                }
//            }
//        }
        return listOfUser;
//        return listOfUserNew;
    }

    public List initComboAlat(String query){
        List<AlatDetail> listOfAlat = new ArrayList<AlatDetail>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AlatDetailBo alatDetailBo = (AlatDetailBo) ctx.getBean("alatDetailBoProxy");

        try {
            listOfAlat = alatDetailBo.getComboAlatByCriteria(query);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatDetailBo.saveErrorMessage(e.getMessage(), "DesaBo.getComboDesaWithCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when get combo lokasi kebun," + "[" + logId + "] Found problem when retrieving combo lokasi kebun data, please inform to your admin.", e);
        }

        logger.info("[PermohonanLahanAction.initComboLokasiKebun] end process <<<");


        return listOfAlat;
    }

    public String initAddProject(){

//        HttpSession session = ServletActionContext.getRequest().getSession();
//        if (session.getAttribute("listOfProject")!=null) {
//            setProject((Project) session.getAttribute("listOfProject"));
//        }
//        session.removeAttribute("listOfResult");
    return "add_project";
}


    public String addProject() {
        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        session.removeAttribute("listOfResultTask");
        session.removeAttribute("listOfResultMember");
        session.removeAttribute("listOfProject");
        session.removeAttribute("listOfResultIssue");
        session.removeAttribute("listOfResultAlat");
        session.removeAttribute("listOfTaskMember");
        return "add_project";
    }

    @Override
    public String search() {
        logger.info("[ProjectAction.search] start process >>>");
        String userLoginId = CommonUtil.userIdLogin();
        String roleAs = CommonUtil.roleAsLogin();
//        String branchId = CommonUtil.userBranchLogin();

        logger.info("[ProjectAction.search] roles ="+roleAs+", user"+userLoginId);
        List<Project> listOfSearchProject = new ArrayList();
        Project searchProject = getProject();



        try {
            listOfSearchProject = projectBoProxy.getSearchByCriteria(searchProject, roleAs, userLoginId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfProject");
        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfSearchProject);
        logger.info("[ProjectAction.search] end process <<<");

        return SUCCESS;
    }

    public String saveProject(){
        logger.info("[projectAction.saveProject] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Member> listOfMember = (List) session.getAttribute("listOfResultMember");
        List<Task> listOfTask = (List) session.getAttribute("listOfResultTask");
        List<ProjectIssue> listOfIssue = (List) session.getAttribute("listOfResultIssue");
        List<AlatDetail> listOfAlat = (List) session.getAttribute("listOfResultAlat");
        List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");


        Project entryProject = getProject();

        if(listOfAlat != null){
            for(AlatDetail alatData : listOfAlat){
                entryProject.setIdAlatDetail(alatData.getIdAlatDetail());
            }
        }
        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

        String seqProjectId = null;
        seqProjectId = projectBo.getSeqNumProjectId();
        entryProject.setNoProject("P"+seqProjectId);
        String stDate = entryProject.getStStartDate();
        String subBulan = stDate.substring(3,5);
        String subTahun = stDate.substring(6,10);
        String noProject = entryProject.getNoProject();

        Divisi divisi = new Divisi();
        divisi.setIdDivisi(entryProject.getIdDivisi());
        List<Divisi> listDivisi = new ArrayList<Divisi>();
        listDivisi = divisiBoProxy.getByCriteria(divisi);

        if (listDivisi != null){
            for (Divisi dataDivisi : listDivisi){
                if (dataDivisi.getIdDivisi().equals(entryProject.getIdDivisi())){
                    entryProject.setNamaDivisi(dataDivisi.getNamaDivisi());
                }
            }
        }

        String divName = entryProject.getNamaDivisi();

        entryProject.setIdProject(noProject+"/"+divName+"/"+subBulan+"/"+subTahun);
        entryProject.setLastUpdate(updateTime);
        entryProject.setLastUpdateWho(userLogin);

        try {
            projectBoProxy.saveProjectMemberTask(entryProject, listOfMember, listOfTask, listOfIssue, listOfTaskMember);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.saveProject");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.saveProject] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.saveProject] Error when saving new permohonan ," + "[" + logId + "] Found problem when saving data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving new task, please inform to your admin. Cause : " + e.getMessage());
            return ERROR;
        }


        String idProject = entryProject.getIdProject();

        if(idProject !=null){
            Document addDoc = new Document();
            addDoc.setLastUpdate(updateTime);
            addDoc.setUploadBy(userLogin);
            String idDoc = null;
            try{
                idDoc = "DOC"+projectBo.getSeqDocId();
                File fileToCreate = null;
                //note : for windows directory
                String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                if (this.fileUploadDoc != null) {
                    if (this.fileUploadDoc.length() > 0 && this.fileUploadDoc.length() <= 355242880) {
                        if ("image/jpeg".equalsIgnoreCase(this.fileUploadDocContentType)) {
                            String fileDocName = "IMG_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");
                        }
                        else if ("application/pdf".equalsIgnoreCase(this.fileUploadDocContentType)){
                            String filePathPdf = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                            String fileDocName = "PDF_" +idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePathPdf, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");

                        }
                        else if ("image/png".equalsIgnoreCase(this.fileUploadDocContentType)) {
                            String fileDocName = "IMG_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                        }
                        else {
                            String fileDocName = "FLE_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("file");

                        }
                    } else {
                        logger.error("[projectAction.saveDocUpload] Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        addActionError("Error,  Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
//                        return ERROR;
                        return "error_add_project";
                    }
                }
                addDoc.setIdProject(idProject);
//                addDoc.setIdTask(idTask);
                try{
                    projectBoProxy.saveDocument(addDoc);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
                    } catch (GeneralBOException e1) {
                        logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                    }
                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
                }

            }catch (GeneralBOException e ){
                Long logId = null;
                try {
                    logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                }
                logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
            }
        }


        session.removeAttribute("listOfResult");
        logger.info("[projectAction.saveProject] end process <<<");
        return "save_project";
    }

    public String addUser(){
        logger.info("[projectAction.addUser] start process >>>");
        //lookup popup only view

        logger.info("[projectAction.addUser] end process <<<");
        return "add_user";
    }

    public String addTask(){
        logger.info("[projectAction.addTask] start process >>>");
        //lookup popup only view

        logger.info("[projectAction.addTask] end process <<<");
        return "add_task";
    }

    public String saveMember(String idMember, String namaMember, String memberPosition){
        //update session list lahan, insert new lahan
        HttpSession session = WebContextFactory.get().getSession();
        List<Member> listOfMember = (List) session.getAttribute("listOfResultMember");
        List<Member> listOfMemberNew = new ArrayList<Member>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

        String seqMemberId = null;
        try {
            seqMemberId = projectBo.getSeqMemberId();
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id error,", e1);
            }
            logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
        }

        Long Lposition = Long.parseLong(memberPosition);
        boolean flag = false;

        Member addMember = new Member();
        addMember.setIdProjectMember("MBR"+seqMemberId);
        addMember.setIdMember(idMember);
        addMember.setNamaMember(namaMember);
        addMember.setPositionId(Lposition);
        addMember.setStPositionId(memberPosition);
        addMember.setFlag("Y");
        if(addMember.getPositionId() == 4){
            addMember.setPositionName("STAFF");
        } else if(addMember.getPositionId() == 5){
            addMember.setPositionName("PIC");
        }

        User dataUser = new User();
        String flagMember = addMember.getFlag();

        UserBo userBo = (UserBo) ctx.getBean("userBoProxy");
        dataUser = userBo.getUserById(idMember, flagMember);

        if (dataUser != null){
                addMember.setPhotoUrl(dataUser.getPhotoUserUrl());
                addMember.setPhotoPath(dataUser.getPreviewPhoto());
                addMember.setRoleId(dataUser.getRoleId());
                addMember.setRoleName(dataUser.getRoleName());
        }

        if(listOfMember != null){
            listOfMember.add(addMember);
            session.removeAttribute("listOfResultMember");
            session.setAttribute("listOfResultMember", listOfMember);
            flag = true;
        } else {
            listOfMemberNew.add(addMember);
            session.removeAttribute("listOfResultMember");
            session.setAttribute("listOfResultMember", listOfMemberNew);
            flag = true;
        }
        if (flag) {
            return "0";
        } else {
            return "1";
        }

    }

    public String saveTask(String namaTask, String note, String priority, String deadline, String startDate, String asignto){
        //update session list lahan, insert new lahan
        HttpSession session = WebContextFactory.get().getSession();
        List<Task> listOfTask = (List) session.getAttribute("listOfResultTask");
        List<Task> listOfTaskNew = new ArrayList<Task>();
        java.util.Date utilDate = new java.util.Date();
        Date today = new Date(utilDate.getTime());

        boolean flag = false;

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
        String seqTaskId = null;
        try {
            seqTaskId = projectBo.getSeqNumTaskId();
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id error,", e1);
            }
            logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
        }

        Task addTask = new Task();
        addTask.setNamaTask(namaTask);
        addTask.setNote(note);
        addTask.setPriority(priority);
        addTask.setStDeadline(deadline);
        addTask.setStStartDate(startDate);
        addTask.setAsignTo(asignto);
        addTask.setIdTask("TSK"+seqTaskId);
        addTask.setFlag("Y");

        Date startDateTask = CommonUtil.convertToDate(startDate);
        Date deadlineTask = CommonUtil.convertToDate(deadline);

        if(today.before(startDateTask)){
            addTask.setStatusTask("5");
            addTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
        } else if(today.after(startDateTask) && today.before(deadlineTask)){
            addTask.setStatusTask("1");
            addTask.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
        } else if(today.after(deadlineTask)){
            addTask.setStatusTask("3");
            addTask.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
        } else if(asignto == null){
            addTask.setStatusTask("4");
            addTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Initiate</div>");
        }

        if("1".equalsIgnoreCase(addTask.getPriority())){
            addTask.setNamaPriority("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>High</div>");
        } else if("2".equalsIgnoreCase(addTask.getPriority())){
            addTask.setNamaPriority("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Normal</div>");
        } else{
            addTask.setNamaPriority("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>Low</div>");
        }

        if(listOfTask != null){
            listOfTask.add(addTask);
            session.removeAttribute("listOfResultTask");
            session.setAttribute("listOfResultTask",listOfTask);
            flag = true;
        } else {
            listOfTaskNew.add(addTask);
            session.removeAttribute("listOfResultTask");
            session.setAttribute("listOfResultTask",listOfTaskNew);
            flag = true;
        }
        if (flag) {
            return "0";
        } else {
            return "1";
        }
    }

    public String saveDocUpload(){
        Project dataProject = getProject();
        String idProject = dataProject.getIdProject();
        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

        if(idProject !=null){
            Document addDoc = new Document();
            addDoc.setLastUpdate(updateTime);
            addDoc.setUploadBy(userLogin);
            ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
            ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
            String idDoc = null;
            try{
                idDoc = "DOC"+projectBo.getSeqDocId();
                File fileToCreate = null;
                //note : for windows directory
                String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                if (this.fileUploadDoc != null) {
//                    if (this.fileUploadDoc.length() > 0 && this.fileUploadDoc.length() <= 5242880) {
                    if (this.fileUploadDoc.length() > 0 && this.fileUploadDoc.length() <= 355242880) {
                        if ("image/jpeg".equalsIgnoreCase(this.fileUploadDocContentType)) {
                            String fileDocName = "IMG_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");
                        }
                        else if ("application/pdf".equalsIgnoreCase(this.fileUploadDocContentType)){
                            String filePathPdf = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                            String fileDocName = "PDF_" +idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePathPdf, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");
                        }
                        else {
                            String filePathPdf = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                            String fileDocName = "FLE_" +idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePathPdf, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("file");
//                            logger.error("[projectAction.saveDocUpload] Error when saving project, Found problem when upload file data (content type is unknow), please inform to your admin.");
//                            addActionError("Error,  Error when saving project, Found problem when upload upload file data (content type is unknow), please inform to your admin.");
//                            return ERROR;
                        }
                    } else {
                        logger.error("[projectAction.saveDocUpload] Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        addActionError("Error,  Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        return ERROR;
                    }
                }
                addDoc.setIdProject(idProject);
//                addDoc.setIdTask(idTask);
                try{
                    projectBoProxy.saveDocument(addDoc);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
                    } catch (GeneralBOException e1) {
                        logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                    }
                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
                }

            }catch (GeneralBOException e ){
                Long logId = null;
                try {
                    logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                }
                logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
            }
        }
        return "save_doc_upload";
    }

    public String saveDoc(){
        Boolean flag = false;
        HttpSession session = WebContextFactory.get().getSession();
        Document sessionDoc = (Document) session.getAttribute("listOfProject");
        if(sessionDoc != null){
            List<Document> sessionListOfDocument = sessionDoc.getListOfUploadDocument();
            ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
            ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

            String idDoc = null;
            Document addDoc = getDocument();
            idDoc = projectBo.getSeqDocId();
//            File fileToCreate = null;
            //note : for windows directory
//            String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
            if (this.fileUploadDoc != null) {
                if (this.fileUploadDoc.length() > 0 && this.fileUploadDoc.length() <= 5242880) {
                    if ("image/jpeg".equalsIgnoreCase(this.fileUploadDocContentType)) {
                        String fileDocName = "IMG_"+idDoc+"_"+ this.fileUploadDocFileName;
//                        fileToCreate = new File(filePath, fileDocName);
//                            FileUtils.copyFile(this.fileUploadDoc,fileToCreate);
//                        try {
//                            FileUtils.copyFile(this.fileUploadDoc, fileToCreate );
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                        addDoc.setIdDoc(idDoc);
                        addDoc.setNamaDoc(fileDocName);
                        addDoc.setFileUploadDoc(this.fileUploadDoc);
                        addDoc.setType("Image");
                        flag = true;
                    }
                    else if ("application/pdf".equalsIgnoreCase(this.fileUploadDocContentType)){
//                        String filePathPdf = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                        String fileDocName = "PDF_" +idDoc+"_"+ this.fileUploadDocFileName;
//                        fileToCreate = new File(filePathPdf, fileDocName);
//
//                        try {
//                            FileUtils.copyFile(fileUploadDoc, fileToCreate);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                        addDoc.setIdDoc(idDoc);
                        addDoc.setNamaDoc(fileDocName);
                        addDoc.setFileUploadDoc(this.fileUploadDoc);
                        addDoc.setType("PDF");
                        flag = true;
                    }
                    else {
                        logger.error("[projectAction.saveDocument] Error when saving project, Found problem when upload file data (content type is unknow), please inform to your admin.");
                        addActionError("Error,  Error when saving project, Found problem when upload upload file data (content type is unknow), please inform to your admin.");
                        return ERROR;
                    }

                    sessionListOfDocument.add(addDoc);
                    sessionDoc.setListOfUploadDocument(sessionListOfDocument);
                    session.removeAttribute("listOfResult");
                    session.removeAttribute("listOfProject");
                    session.setAttribute("listOfProject", sessionDoc);
                }
            } else{
                logger.error("[projectAction.saveDocument] Error when saving project, Found problem when upload file data (Content is null), please inform to your admin.");
                addActionError("Error,  Error when saving project, Found problem when upload upload file data (Content is null), please inform to your admin.");
                return ERROR;
            }
        }else {
            Document newEntrySessionDocument = new Document();
            List<Document> sessionListOfDocument = newEntrySessionDocument.getListOfUploadDocument();
            if (sessionListOfDocument == null){
                sessionListOfDocument = new ArrayList<Document>();
            }
            ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
            ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
//            fileUploadDoc = fileUpload;
            String idDoc = null;
            Document addDoc = new Document();
            idDoc = projectBo.getSeqDocId();
//            File fileToCreate = null;
            //note : for windows directory
//            String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
            if (this.fileUploadDoc != null) {
                if (this.fileUploadDoc.length() > 0 && this.fileUploadDoc.length() <= 5242880) {
                    if ("image/jpeg".equalsIgnoreCase(this.fileUploadDocContentType)) {
                        String fileDocName = "IMG_" + idDoc + "_" + this.fileUploadDocFileName;

//                        fileToCreate = new File(filePath, fileDocName);
//                        try {
//                            FileUtils.copyFile(fileUploadDoc, fileToCreate);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                        addDoc.setIdDoc(idDoc);
                        addDoc.setNamaDoc(fileDocName);
                        addDoc.setFileUploadDoc(fileUploadDoc);
                        addDoc.setType("Image");
                        flag = true;
                    } else if ("application/pdf".equalsIgnoreCase(this.fileUploadDocContentType)) {
//                        String filePathPdf = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                        String fileDocName = "PDF_" + idDoc + "_" + this.fileUploadDocFileName;

//                        fileToCreate = new File(filePathPdf, fileDocName);

//                        try {
//                            FileUtils.copyFile(fileUploadDoc, fileToCreate);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                        addDoc.setIdDoc(idDoc);
                        addDoc.setNamaDoc(fileDocName);
                        addDoc.setFileUploadDoc(this.fileUploadDoc);
                        addDoc.setType("PDF");
                        flag = true;
                    } else {
                        logger.error("[projectAction.saveProject] Error when saving project, Found problem when upload file data (content type is unknow), please inform to your admin.");
                        addActionError("Error,  Error when saving project, Found problem when upload upload file data (content type is unknow), please inform to your admin.");
                        return ERROR;
                    }

                    sessionListOfDocument.add(addDoc);
                    sessionDoc.setListOfUploadDocument(sessionListOfDocument);
                    session.removeAttribute("listOfResult");
                    session.removeAttribute("listOfProject");
                    session.setAttribute("listOfProject", newEntrySessionDocument);
                }
            }
        }
        return "save_add_doc";
    }

    public String viewProjectTask(){
        logger.info("[ProjectAction.viewProject] start process >>>");
        String projectId = getId();
        String taskId = getIdTask();
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Project> listOfResult = (List) session.getAttribute("listOfResult");


        if (projectId != null && taskId != null) {
            Project p = projectBoProxy.getProjectById(projectId, taskId);
            setProject(p);
        } else if (taskId == null) {
            for(Project project : listOfResult){
                if(project.getIdProject().equals(projectId)){
                    setProject(project);
                }
            }
        } else{
            setProject(new Project());
        }

        List<Member> listOfSearchMember = new ArrayList();
        try {
            listOfSearchMember = projectBoProxy.getSearchMemberByCriteria(projectId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
        session.removeAttribute("listOfResultMember");
        session.setAttribute("listOfResultMember", listOfSearchMember);
        logger.info("[ProjectAction.search] end process <<<");

        List<Task> listOfTask = new ArrayList<Task>();
        try {
            listOfTask = projectBoProxy.getSearchTaskByCriteria(projectId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
//        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResultTask");
        session.setAttribute("listOfResultTask", listOfTask);

        List<ProjectIssue> listOfIssue = new ArrayList<ProjectIssue>();

        try {
            listOfIssue = projectBoProxy.getSearchIssueByCriteria(projectId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
        logger.info("[ProjectAction.search] end process <<<");
        session.removeAttribute("listOfResultIssue");
        session.setAttribute("listOfResultIssue", listOfIssue);

        List<Document> listOfDocument = new ArrayList<Document>();
        try {
            listOfDocument = projectBoProxy.getSearchDocumentByCriteria(projectId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.search] end process <<<");
        session.removeAttribute("listOfResultDoc");
        session.setAttribute("listOfResultDoc", listOfDocument);

        List<TaskLog> listOfTaskLog = new ArrayList<TaskLog>();
        try {
            listOfTaskLog = projectBoProxy.getSearchTaskLogByCriteria(taskId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.viewProject");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.viewProject] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.viewProject] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

//        Project gProject = getProject();
        String idAlatadeltail = project.getIdAlatDetail();


        AlatDetail alatDetail = new AlatDetail();
        alatDetail.setIdAlatDetail(idAlatadeltail);



        List<AlatDetail> listOfAlatDetail = new ArrayList<AlatDetail>();
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AlatDetailBo alatDetailBo = (AlatDetailBo) ctx.getBean("alatDetailBoProxy");

        try {
            listOfAlatDetail = alatDetailBo.getByCriteria(alatDetail);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.searchAlatDetail] end process <<<");
        session.removeAttribute("listOfResultAlat");
        session.setAttribute("listOfResultAlat", listOfAlatDetail);

        List<TaskMemberChange> listOfTaskMemberChange = new ArrayList<TaskMemberChange>();
        TaskMemberChangeBo taskMemberChangeBo = (TaskMemberChangeBo) ctx.getBean("taskMemberChangeBoProxy");
        TaskMemberChange taskMemberChange = new TaskMemberChange();
        taskMemberChange.setIdProject(projectId);

        try {
            listOfTaskMemberChange = taskMemberChangeBo.getByCriteria(taskMemberChange);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.searchTaskMemberChange] end process <<<");
        session.removeAttribute("listOfTaskMemberChange");
        session.setAttribute("listOfTaskMemberChange", listOfTaskMemberChange);

        session.removeAttribute("listOfResultTaskLog");
        session.setAttribute("listOfResultTaskLog", listOfTaskLog);
        logger.info("[ProjectAction.viewProject] end process <<<");
        return "view_project_task";
    };


    public String viewProject(){
        logger.info("[ProjectAction.viewProject] start process >>>");
        String itemId = getId();
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Project> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (Project projectOfList : listOfResult) {
                    if (id.equalsIgnoreCase(projectOfList.getIdProject())) {
                        setProject(projectOfList);
                        break;
                    }
                }
            } else {
                setProject(new Project());
            }
        } else {
            setProject(new Project());
        }

        List<Member> listOfSearchMember = new ArrayList();
        try {
            listOfSearchMember = projectBoProxy.getSearchMemberByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
        session.removeAttribute("listOfResultMember");
        session.setAttribute("listOfResultMember", listOfSearchMember);
        logger.info("[ProjectAction.search] end process <<<");

        List<Task> listOfTask = new ArrayList<Task>();
        try {
            listOfTask = projectBoProxy.getSearchTaskByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
        HttpSession session2 = ServletActionContext.getRequest().getSession();
        session2.removeAttribute("listOfResultTask");
        session2.setAttribute("listOfResultTask", listOfTask);

        List<ProjectIssue> listOfIssue = new ArrayList<ProjectIssue>();

        try {
            listOfIssue = projectBoProxy.getSearchIssueByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
        logger.info("[ProjectAction.search] end process <<<");
        session.removeAttribute("listOfResultIssue");
        session.setAttribute("listOfResultIssue", listOfIssue);

        List<Document> listOfDocument = new ArrayList<Document>();
        try {
            listOfDocument = projectBoProxy.getSearchDocumentByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.search] end process <<<");
        session.removeAttribute("listOfResultDoc");
        session.setAttribute("listOfResultDoc", listOfDocument);

        Project gProject = getProject();
        String idAlatadeltail = gProject.getIdAlatDetail();

        AlatDetail alatDetail = new AlatDetail();
        alatDetail.setIdAlatDetail(idAlatadeltail);

        List<AlatDetail> listOfAlatDetail = new ArrayList<AlatDetail>();
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AlatDetailBo alatDetailBo = (AlatDetailBo) ctx.getBean("alatDetailBoProxy");

        try {
            listOfAlatDetail = alatDetailBo.getByCriteria(alatDetail);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.searchAlatDetail] end process <<<");
        session.removeAttribute("listOfResultAlat");
        session.setAttribute("listOfResultAlat", listOfAlatDetail);

        List<TaskMemberChange> listOfTaskMemberChange = new ArrayList<TaskMemberChange>();
        TaskMemberChangeBo taskMemberChangeBo = (TaskMemberChangeBo) ctx.getBean("taskMemberChangeBoProxy");
        TaskMemberChange taskMemberChange = new TaskMemberChange();
        taskMemberChange.setIdProject(itemId);

        try {
            listOfTaskMemberChange = taskMemberChangeBo.getByCriteria(taskMemberChange);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.searchTaskMemberChange] end process <<<");
        session.removeAttribute("listOfTaskMemberChange");
        session.setAttribute("listOfTaskMemberChange", listOfTaskMemberChange);

        logger.info("[ProjectAction.viewProject] end process <<<");
        return "view_project";
    }

    public String viewAlatDetail(){
        logger.info("[AlatDetailAction.viewAlatDetail] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfResult = (List) session.getAttribute("listOfResultAlat");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (AlatDetail alatDetailOfList : listOfResult) {
                    if (id.equalsIgnoreCase(alatDetailOfList.getIdAlatDetail())) {
                        setAlatDetail(alatDetailOfList);
                        break;
                    }
                }
            } else {
                setAlatDetail(new AlatDetail());
            }
        } else {
            setAlatDetail(new AlatDetail());
        }
        return "view_alat_detail";
    }


    public String viewTaskList(){
        logger.info("[ProjectAction.viewTaskList] start process >>>");
        String itemId = getId();
        List<Task> listOfTask = new ArrayList<Task>();
        try {
            listOfTask = projectBoProxy.getSearchTaskByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResultTask");
        session.removeAttribute("listOfResultMember");
        session.removeAttribute("listOfResultTaskList");
        session.setAttribute("listOfResultTaskList", listOfTask);
        logger.info("[ProjectAction.viewTaskList] end process <<<");
        return "view_task_list";
    }

    public String viewDoc(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Document> listOfResult = (List) session.getAttribute("listOfResultDoc");

//        boolean flagPdf;

        String gId = getId();
        String subWord = gId.substring(0,3);
        if("PDF".equalsIgnoreCase(subWord)){
            if (gId != null && !"".equalsIgnoreCase(gId)) {
                if (listOfResult != null) {
                    for (Document projectOfList : listOfResult) {
                        if (gId.equalsIgnoreCase(projectOfList.getNamaDoc())) {
//                            flagPdf = true;
                            setDocument(projectOfList);
                            break;
                        }
                    }
                } else {
                    setDocument(new Document());
                }
            }
            return "view_preview_pdf";
        }

        else if("IMG".equalsIgnoreCase(subWord)){
            if (gId != null && !"".equalsIgnoreCase(gId)) {
                if (listOfResult != null) {
                    for (Document projectOfList : listOfResult) {
                        if (gId.equalsIgnoreCase(projectOfList.getNamaDoc())) {
//                            flagPdf = true;
                            setDocument(projectOfList);
                            break;
                        }
                    }
                } else {
                    setDocument(new Document());
                }
            }
            return "view_preview_doc";
        }
        else {
            setDocument(new Document());
            return "view_preview_doc";
        }
    }

    public String viewDocAlat(){

//        HttpSession session = ServletActionContext.getRequest().getSession();
//        List<AlatDetail> listOfResult = (List) session.getAttribute("listOfResultAlat");

        String gId = getId();

//        String subWord = gId.substring(0,3);
//        if("PDF".equalsIgnoreCase(subWord)){
//            if (gId != null && !"".equalsIgnoreCase(gId)) {
//                if (listOfResult != null) {
//                    for (AlatDetail projectOfList : listOfResult) {
//                        if (gId.equalsIgnoreCase(projectOfList.getUrlFoto())) {
////                            flagPdf = true;
//                            setAlatDetail(projectOfList);
//                            break;
//                        }
//                    }
//                } else {
//                    setAlatDetail(new AlatDetail());
//                }
//            }
//            return "view_preview_pdf_alat";
//        }
//        else if("IMG".equalsIgnoreCase(subWord)){
//            if (gId != null && !"".equalsIgnoreCase(gId)) {
//                if (listOfResult != null) {
//                    for (AlatDetail projectOfList : listOfResult) {
//                        if (gId.equalsIgnoreCase(projectOfList.getUrlFoto())) {
////                            flagPdf = true;
//                            setAlatDetail(projectOfList);
//                            break;
//                        }
//                    }
//                } else {
//                    setAlatDetail(new AlatDetail());
//                }
//            }
//            return "view_preview_doc_alat";
//        }
//        else {
//            setAlatDetail(new AlatDetail());
            return "view_preview_doc_alat";
//        }
    }

    public String initComboStatus(){
        Status status = new Status();
        status.setFlag("Y");

        List<Status> listOfStatus = new ArrayList<Status>();
        try {
            listOfStatus = statusBoProxy.getStatusByCriteria(status);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = statusBoProxy.saveErrorMessage(e.getMessage(), "UserBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboOperatorGps] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboOperatorGps] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        listOfComboStatus.addAll(listOfStatus);
        return "init_combo_status";
    }

    public String initComboPriority(){
        Priority priority = new Priority();
        priority.setFlag("Y");

        List<Priority> listOfPriority = new ArrayList<Priority>();
        try {
            listOfPriority = priorityBoProxy.getPriorityByCriteria(priority);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = priorityBoProxy.saveErrorMessage(e.getMessage(), "UserBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboOperatorGps] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboOperatorGps] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        listOfComboPriority.addAll(listOfPriority);
        return "init_combo_priority";
    }

    public String initComboDivisi(){
        Divisi divisi = new Divisi();
        divisi.setFlag("Y");

        List<Divisi> ListOfDivisi = new ArrayList<Divisi>();
        try {
            ListOfDivisi = divisiBoProxy.getByCriteria(divisi);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = priorityBoProxy.saveErrorMessage(e.getMessage(), "UserBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboOperatorGps] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboOperatorGps] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        listOfComboDivisi.addAll(ListOfDivisi);
        return "init_combo_divisi";
    }



    public String initComboMemberTask(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Member> listOfMember = (List) session.getAttribute("listOfResultMember");
        List<Member> listOfComboMember = new ArrayList<Member>();
        for(Member comboData :listOfMember ){
            if("Y".equalsIgnoreCase(comboData.getFlag())){
                listOfComboMember.add(comboData);
            }
        }
        listOfComboMemberTask.addAll(listOfComboMember);
        return "init_combo_member_task";
    }

    public String initComboProgres(){
        Progres progres = new Progres();
        progres.setFlag("Y");

        List<Progres> listOfProgres = new ArrayList<Progres>();
        try {
            listOfProgres = progresBoProxy.getProgresByCriteria(progres);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = priorityBoProxy.saveErrorMessage(e.getMessage(), "UserBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboOperatorGps] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboOperatorGps] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        listOfComboProgres.addAll(listOfProgres);
        return "init_combo_priority";
    }


    public String initComboTypeProject(){
        TypeProject typeProject = new TypeProject();
        typeProject.setFlag("Y");

        List<TypeProject> listOfTypeProject = new ArrayList<TypeProject>();
        try{
            listOfTypeProject = projectBoProxy.getTypeProjectByCriteria(typeProject);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = priorityBoProxy.saveErrorMessage(e.getMessage(), "UserBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.initComboTypeProject] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.initComboTypeProject] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }
        listOfComboTypeProject.addAll(listOfTypeProject);
        return "init_combo_type_project";
    }


    public String viewEditTask(){

        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfResultTaskList = (List) session.getAttribute("listOfResultTaskList");
        session.removeAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResultTaskList != null) {

                for (Task TaskOfList : listOfResultTaskList) {
                    if (id.equalsIgnoreCase(TaskOfList.getIdTask())) {
                        setTask(TaskOfList);
                        break;
                    }
                }
            } else {
                setTask(new Task());
            }
        } else {
            setTask(new Task());
        }
        return "view_edit_task";
    }

    public String saveEditTask(){
        logger.info("[projectAction.saveEditTask] start process >>>");
        try{
            Task entryTask = getTask();
            projectBoProxy.saveEditTask(entryTask);
//            projectBoProxy.updateStatusProject(entryTask);
        }  catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "projectAction.saveEditTask");
            } catch (GeneralBOException e1) {
                logger.error("[projectAction.saveEditTask] Error when saving error,", e1);
            }
            logger.error("[projectAction.saveEditTask] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        session.removeAttribute("listOfResultTaskList");

        logger.info("[projectAction.saveEditTask] end process <<<");

        return "save_edit_task";
    }

    public String saveEditListTask(){
        logger.info("[projectAction.saveEditListTask] start process >>>");
        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

        java.util.Date utilDate = new java.util.Date();
        Date today = new Date(utilDate.getTime());

        Task entryTask = getTask();
        entryTask.setLastUpdate(updateTime);
        entryTask.setLastUpdateWho(userLogin);
        entryTask.setDeadline(CommonUtil.convertToDate(entryTask.getStDeadline()));
        entryTask.setStartDate(CommonUtil.convertToDate(entryTask.getStStartDate()));
        if (today.after(entryTask.getStartDate()) && today.before(entryTask.getDeadline()) && entryTask.getProgres() < 100){
            entryTask.setStatusTask("1");
        } else if (today.after(entryTask.getDeadline()) && entryTask.getProgres() < 100){
            entryTask.setStatusTask("3");
        } else if (entryTask.getProgres() == 100){
            entryTask.setStatusTask("2");
        } else if(today.before(entryTask.getStartDate())){
            entryTask.setStatusTask("5");
        }

        try{
            projectBoProxy.saveEditTask(entryTask);
        }  catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "projectAction.saveEditListTask");
            } catch (GeneralBOException e1) {
                logger.error("[projectAction.saveEditListTask] Error when saving error,", e1);
            }
            logger.error("[projectAction.saveEditListTask] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }
        try {
            projectBoProxy.updateTaskLastProgres(entryTask);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "projectAction.saveEditListTask");
            } catch (GeneralBOException e1) {
                logger.error("[projectAction.saveEditListTask] Error when saving error,", e1);
            }
            logger.error("[projectAction.saveEditListTask] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        session.removeAttribute("listOfResultTaskList");
        projectBoProxy.updateStatusByDate();
        logger.info("[projectAction.saveEditListTask] end process <<<");
        return "success_edit_list_task";
    }
    public String saveEditListTaskPIC(){
        logger.info("[projectAction.saveEditListTask] start process >>>");
        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

        java.util.Date utilDate = new java.util.Date();
        Date today = new Date(utilDate.getTime());

        try{
            Task entryTask = getTask();
            entryTask.setLastUpdate(updateTime);
            entryTask.setLastUpdateWho(userLogin);
            entryTask.setDeadline(CommonUtil.convertToDate(entryTask.getStDeadline()));
            entryTask.setStartDate(CommonUtil.convertToDate(entryTask.getStStartDate()));
            if (today.after(entryTask.getStartDate()) && today.before(entryTask.getDeadline()) && entryTask.getProgres() < 100){
                entryTask.setStatusTask("1");
            } else if (today.after(entryTask.getDeadline()) && entryTask.getProgres() < 100){
                entryTask.setStatusTask("3");
            } else if (entryTask.getProgres() == 100){
                entryTask.setStatusTask("2");
            } else if(today.before(entryTask.getStartDate())){
                entryTask.setStatusTask("5");
            }
            projectBoProxy.saveEditTaskPIC(entryTask);
        }  catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "projectAction.saveEditListTask");
            } catch (GeneralBOException e1) {
                logger.error("[projectAction.saveEditListTask] Error when saving error,", e1);
            }
            logger.error("[projectAction.saveEditListTask] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        session.removeAttribute("listOfResultTaskList");
        projectBoProxy.updateStatusByDate();
        logger.info("[projectAction.saveEditListTask] end process <<<");
        return "success_edit_list_task_pic";
    }

    public String addProjectTask(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Project> listOfProject = (List) session.getAttribute("listOfResult");
        String itemId = getId();
        Task newTask = new Task();
        newTask.setIdProject(itemId);

//        session.removeAttribute("listOfResult");
        if (listOfProject != null){
            for (Project dataProject: listOfProject){
                if (itemId.equalsIgnoreCase(dataProject.getIdProject())){
                    newTask.setpStartDate(dataProject.getStStartDate());
                    newTask.setpDeadline(dataProject.getStDeadline());
                }
            }
        }

        setTask(newTask);

        return "view_add_task";
    }

    public String saveAddTask(){
        logger.info("[projectAction.saveAddTask] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        Task entryTask = getTask();
        TaskLog entryTaskLog = new TaskLog();
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

        String getTaskId = null;
        String getTaskLogId = null;

        try {
            getTaskId = projectBo.getSeqNumTaskId();
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = priorityBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.addProjectTask");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.addProjectTask] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.addProjectTask] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        try {
            getTaskLogId = projectBo.getSeqNumTaskLogId();
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = priorityBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.addProjectTask");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.addProjectTask] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.addProjectTask] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
            return "failure";
        }

        if(getTaskId != null && getTaskLogId != null){
            entryTask.setIdTask("TSK"+getTaskId);
            entryTaskLog.setIdTaskLog("TL"+getTaskLogId);
            try {
                projectBoProxy.saveTask(entryTask, entryTaskLog);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = priorityBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.addProjectTask");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.addProjectTask] Error when saving error,", e1);
                }
                logger.error("[ProjectAction.addProjectTask] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
                return "failure";
            }
        }

        session.removeAttribute("listOfResult");
        projectBoProxy.updateStatusByDate();
        initForm();
        logger.info("[projectAction.saveAddTask] start process >>>");
        return "save_add_task";
    }

    public String deleteProjectTask(){
        Task entryTask = getTask();
        if(entryTask != null){
            try {
                projectBoProxy.deleteTask(entryTask);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = priorityBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.addProjectTask");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.addProjectTask] Error when saving error,", e1);
                }
                logger.error("[ProjectAction.addProjectTask] Error when searching data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin");
                return "failure";
            }
        }
    logger.info("[projectAction.saveAddTask] start process >>>");
        return "delete_task";
    }

    public String viewDeleteTask(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfResultTaskList = (List) session.getAttribute("listOfResultTaskList");
        session.removeAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResultTaskList != null) {

                for (Task TaskOfList : listOfResultTaskList) {
                    if (id.equalsIgnoreCase(TaskOfList.getIdTask())) {
                        setTask(TaskOfList);
                        break;
                    }
                }
            } else {
                setTask(new Task());
            }
        } else {
            setTask(new Task());
        }
        return "view_delete_task";
    }

    public String addDoc(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Project> listOfResultTaskList = (List) session.getAttribute("listOfResult");
        session.removeAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResultTaskList != null) {

                for (Project TaskOfList : listOfResultTaskList) {
                    if (id.equalsIgnoreCase(TaskOfList.getIdProject())) {
                        setProject(TaskOfList);
                        break;
                    }
                }
            } else {
                setProject(new Project());
            }
        } else {
            setProject(new Project());
        }
        return "view_add_doc";
    }

    public String addSessionDoc(){
        return "view_session_doc";
    }

    public String initComboAsignTo(){
        String idProject = getId1();
        List<Member> listOfComboMember = new ArrayList<Member>();
        if(idProject != null){
            try {
                listOfComboMember = projectBoProxy.getSearchMemberByCriteria(idProject);
            } catch (GeneralBOException e){
                logger.error("[ProjectAction.initComboAsignToAddTask] Problem When Search Data",e);
            }
            listOfComboAsignTo.addAll(listOfComboMember);
        }else{
            logger.error("[ProjectAction.initComboAsignToAddTask] idProject is null");
        }
        return "init_combo_asignto";
    }

    public String initComboAsignToAddTask(){
     String idProject = getId();
        List<Member> listOfComboMember = new ArrayList<Member>();

            if(idProject != null){
                try {
                    listOfComboMember = projectBoProxy.getSearchMemberByCriteria(idProject);
                } catch (GeneralBOException e){
                    logger.error("[ProjectAction.initComboAsignToAddTask] Problem When Search Data",e);
                }
                listOfComboAsignTo.addAll(listOfComboMember);
            }else{
                logger.error("[ProjectAction.initComboAsignToAddTask] idProject is null");
            }
        return "init_combo_asignto_add_task";
    }

    public String editMember(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Member> listOfProjectMember = (List) session.getAttribute("listOfResultMember");
        String idProjectMember = getId();
        if(listOfProjectMember != null){
            if (idProjectMember != null && !"".equalsIgnoreCase(idProjectMember)) {

                if (listOfProjectMember != null) {

                    for (Member memberOflist : listOfProjectMember) {
                        if (idProjectMember.equalsIgnoreCase(memberOflist.getIdProjectMember())) {
                            setMember(memberOflist);
                            break;
                        }
                    }
                } else {
                    setMember(new Member());
                }
            } else {
                setMember(new Member());
            }
        }
        session.removeAttribute("listOfResult");
        return "view_edit_member";
    }

    public String deleteMember(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Member> listOfProjectMember = (List) session.getAttribute("listOfResultMember");
        List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");

        String idProjectMember = getId();
        if(listOfProjectMember != null){
            if (idProjectMember != null && !"".equalsIgnoreCase(idProjectMember)) {

                if (listOfProjectMember != null) {
                    for (Member memberOflist : listOfProjectMember) {
                        if (idProjectMember.equalsIgnoreCase(memberOflist.getIdProjectMember())) {
                            if (listOfResultTask.isEmpty()){
                                memberOflist.setIdTask("");
                            }else{
                                for (Task listOftask :listOfResultTask) {
                                    if ("Y".equalsIgnoreCase(listOftask.getFlag())) {
                                        if (memberOflist.getIdMember().equalsIgnoreCase(listOftask.getAsignTo())) {
                                            memberOflist.setIdTask(listOftask.getIdTask());
                                        }else {
                                            memberOflist.setIdTask("");
                                        }
                                    } else {
                                        memberOflist.setIdTask("");
                                    }
                                }
                            }
                            setMember(memberOflist);
                            break;
                        }
                    }
                } else {
                    setMember(new Member());
                }
            } else {
                setMember(new Member());
            }
        }
        session.removeAttribute("listOfResult");
        return "view_delete_member";
    }

    public String saveUpdateMember(String idProjectMember, String idMember, String namaMember, String memberPosition){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Member> listOfProjectMember = (List) session.getAttribute("listOfResultMember");
        if(listOfProjectMember != null){
                for (Member addProjectMember : listOfProjectMember){
                    if(updated == false){
                        if(idProjectMember.equalsIgnoreCase(addProjectMember.getIdProjectMember())){
                        addProjectMember.setIdProjectMember(idProjectMember);
                        addProjectMember.setIdMember(idMember);
                        addProjectMember.setNamaMember(namaMember);

                            User dataUser = new User();
                            String flagMember = addProjectMember.getFlag();

                            ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
                            UserBo userBo = (UserBo) ctx.getBean("userBoProxy");
                            dataUser = userBo.getUserById(idMember, flagMember);

                            if (dataUser != null){
                                addProjectMember.setPhotoUrl(dataUser.getPhotoUserUrl());
                                addProjectMember.setPhotoPath(dataUser.getPreviewPhoto());
                                addProjectMember.setRoleId(dataUser.getRoleId());
                                addProjectMember.setRoleName(dataUser.getRoleName());
                            }

                            Long Lposition = Long.parseLong(memberPosition);
                            addProjectMember.setPositionId(Lposition);
                            if(addProjectMember.getPositionId() == 4){
                                addProjectMember.setPositionName("STAFF");
                            } else if(addProjectMember.getPositionId() == 5){
                                addProjectMember.setPositionName("PIC");
                            }
                        updated = true;
                        listOfProjectMember.remove(addProjectMember);
                        listOfProjectMember.add(addProjectMember);
                            flag = true;
                            if(flag){
                                session.removeAttribute("listOfResultMember");
                                session.setAttribute("listOfResultMember", listOfProjectMember);
                                return "0";
                            } else {
                                return "1";
                            }
                         }
                    }
                }
            } if(flag){
            session.removeAttribute("listOfResultMember");
            session.setAttribute("listOfResultMember", listOfProjectMember);
            return "0";
        } else {
            return "1";
        }
    }

    public String saveDeleteMember(String idProjectMember){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Member> listOfProjectMember = (List) session.getAttribute("listOfResultMember");
        if(listOfProjectMember != null){
            for (Member addProjectMember : listOfProjectMember){
                if(updated == false){
                    if(idProjectMember.equalsIgnoreCase(addProjectMember.getIdProjectMember())){
                        listOfProjectMember.remove(addProjectMember);
                        updated = true;
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultMember");
                            session.setAttribute("listOfResultMember", listOfProjectMember);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultMember");
            session.setAttribute("listOfResultMember", listOfProjectMember);
            return "0";
        } else {
            return "1";
        }
    }

    public String saveUpdateTask(String idTask, String namaTask, String note, String priority, String deadline, String startDate){
        boolean flag =  false;
        boolean updated = false;
        java.util.Date utilDate = new java.util.Date();
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfProjectTask = (List) session.getAttribute("listOfResultTask");
        if(listOfProjectTask != null){
            for (Task addProjectTask : listOfProjectTask){
                if(updated == false){
                    if(idTask.equalsIgnoreCase(addProjectTask.getIdTask())){
                        addProjectTask.setIdTask(idTask);
                        addProjectTask.setNamaTask(namaTask);
                        addProjectTask.setNote(note);
                        addProjectTask.setPriority(priority);
                        addProjectTask.setStDeadline(deadline);
                        addProjectTask.setStStartDate(startDate);
                        addProjectTask.setFlag("Y");

                        Date today = new Date(utilDate.getTime());

                        Date startDateTask = CommonUtil.convertToDate(startDate);
                        Date deadlineTask = CommonUtil.convertToDate(deadline);

                        if(today.before(startDateTask)){
                            addProjectTask.setStatusTask("5");
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                        } else if(today.after(startDateTask) && today.before(deadlineTask)){
                            addProjectTask.setStatusTask("2");
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
                        } else if(today.after(deadlineTask)){
                            addProjectTask.setStatusTask("4");
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
                        }

                        if("1".equalsIgnoreCase(addProjectTask.getPriority())){
                            addProjectTask.setNamaPriority("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>High</div>");
                        } else if("2".equalsIgnoreCase(addProjectTask.getPriority())){
                            addProjectTask.setNamaPriority("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Normal</div>");
                        } else{
                            addProjectTask.setNamaPriority("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>Low</div>");
                        }

                        updated = true;
                        listOfProjectTask.remove(addProjectTask);
                        listOfProjectTask.add(addProjectTask);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultTask");
                            session.setAttribute("listOfResultTask", listOfProjectTask);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultTask");
            session.setAttribute("listOfResultTask", listOfProjectTask);
            return "0";
        } else {
            return "1";
        }
    }

    public String saveDeleteTask(String idTask){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfProjectTask = (List) session.getAttribute("listOfResultTask");
        if(listOfProjectTask != null){
            for (Task addProjectTask : listOfProjectTask){
                if(updated == false){
                    if(idTask.equalsIgnoreCase(addProjectTask.getIdTask())){
                        listOfProjectTask.remove(addProjectTask);
                        updated = true;
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultTask");
                            session.setAttribute("listOfResultTask", listOfProjectTask);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        }
        if(flag){
            session.removeAttribute("listOfResultTask");
            session.setAttribute("listOfResultTask", listOfProjectTask);
            return "0";
        } else {
            return "1";
        }
    }


    public String editSessionTask(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfProjectTask = (List) session.getAttribute("listOfResultTask");
        String idTask = getId();
        if(listOfProjectTask != null){
            if (idTask != null && !"".equalsIgnoreCase(idTask)) {

                if (listOfProjectTask != null) {

                    for (Task taskOfList : listOfProjectTask) {
                        if (idTask.equalsIgnoreCase(taskOfList.getIdTask())) {
                            setTask(taskOfList);
                            break;
                        }
                    }
                } else {
                    setTask(new Task());
                }
            } else {
                setTask(new Task());
            }
        }
        session.removeAttribute("listOfResult");
//        return "view_delete_member";
        return "edit_session_task";
    }

    public String deleteSessionTask(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfProjectTask = (List) session.getAttribute("listOfResultTask");

        String idTask = getId();
        if(listOfProjectTask != null){
            if (idTask != null && !"".equalsIgnoreCase(idTask)) {

                if (listOfProjectTask != null) {

                    for (Task taskOfList : listOfProjectTask) {
                        if (idTask.equalsIgnoreCase(taskOfList.getIdTask())) {
                            setTask(taskOfList);
                            break;
                        }
                    }
                } else {
                    setTask(new Task());
                }
            } else {
                setTask(new Task());
            }
        }
        session.removeAttribute("listOfResult");
        return "delete_session_task";
    }

    public String initDelete(){
        logger.info("[ProjectAction.initDelete] start process >>>");
        String itemId = getId();
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Project> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (Project projectOfList : listOfResult) {
                    if (id.equalsIgnoreCase(projectOfList.getIdProject())) {
                        setProject(projectOfList);
                        break;
                    }
                }
            } else {
                setProject(new Project());
            }
        } else {
            setProject(new Project());
        }

        List<Member> listOfSearchMember = new ArrayList();
        try {
            listOfSearchMember = projectBoProxy.getSearchMemberByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
        session.removeAttribute("listOfResultMember");
        session.setAttribute("listOfResultMember", listOfSearchMember);
        logger.info("[ProjectAction.search] end process <<<");

        List<Task> listOfTask = new ArrayList<Task>();
        try {
            listOfTask = projectBoProxy.getSearchTaskByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
//        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResultTask");
        session.setAttribute("listOfResultTask", listOfTask);

        List<ProjectIssue> listOfIssue = new ArrayList<ProjectIssue>();

        try {
            listOfIssue = projectBoProxy.getSearchIssueByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
        logger.info("[ProjectAction.search] end process <<<");
        session.removeAttribute("listOfResultIssue");
        session.setAttribute("listOfResultIssue", listOfIssue);

        List<Document> listOfDocument = new ArrayList<Document>();
        try {
            listOfDocument = projectBoProxy.getSearchDocumentByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.search] end process <<<");
        session.removeAttribute("listOfResultDoc");
        session.setAttribute("listOfResultDoc", listOfDocument);

        Project gProject = getProject();
        String idAlatadeltail = gProject.getIdAlatDetail();

        AlatDetail alatDetail = new AlatDetail();
        alatDetail.setIdAlatDetail(idAlatadeltail);

        List<AlatDetail> listOfAlatDetail = new ArrayList<AlatDetail>();
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AlatDetailBo alatDetailBo = (AlatDetailBo) ctx.getBean("alatDetailBoProxy");

        try {
            listOfAlatDetail = alatDetailBo.getByCriteria(alatDetail);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.searchAlatDetail] end process <<<");
        session.removeAttribute("listOfResultAlat");
        session.setAttribute("listOfResultAlat", listOfAlatDetail);


        logger.info("[ProjectAction.initDelete] end process <<<");
        return "init_delete_project";
    }

    public String initEditProject(){
        return "edit_project";
    }


    public String editProject(){
        logger.info("[ProjectAction.viewProject] start process >>>");
        String itemId = getId();
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Project> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (Project projectOfList : listOfResult) {
                    if (id.equalsIgnoreCase(projectOfList.getIdProject())) {
                        setProject(projectOfList);
                        break;
                    }
                }
            } else {
                setProject(new Project());
            }
        } else {
            setProject(new Project());
        }
        logger.info("[ProjectAction.search] end process <<<");


        List<Member> listOfSearchMember = new ArrayList();
        try {
            listOfSearchMember = projectBoProxy.getSearchMemberByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
        session.removeAttribute("listOfResultMember");
        session.setAttribute("listOfResultMember", listOfSearchMember);
        logger.info("[ProjectAction.search] end process <<<");

        List<Task> listOfTask = new ArrayList<Task>();
        try {
            listOfTask = projectBoProxy.getSearchTaskByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
//        HttpSession session2 = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResultTask");
        session.setAttribute("listOfResultTask", listOfTask);

        List<ProjectIssue> listOfIssue = new ArrayList<ProjectIssue>();

        try {
            listOfIssue = projectBoProxy.getSearchIssueByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }
        logger.info("[ProjectAction.search] end process <<<");
        session.removeAttribute("listOfResultIssue");
        session.setAttribute("listOfResultIssue", listOfIssue);

        List<Document> listOfDocument = new ArrayList<Document>();
        try {
            listOfDocument = projectBoProxy.getSearchDocumentByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.search] end process <<<");
        session.removeAttribute("listOfResultDoc");
        session.setAttribute("listOfResultDoc", listOfDocument);

        Project gProject = getProject();
        String idAlatadeltail = gProject.getIdAlatDetail();

        AlatDetail alatDetail = new AlatDetail();
        alatDetail.setIdAlatDetail(idAlatadeltail);

        List<AlatDetail> listOfAlatDetail = new ArrayList<AlatDetail>();
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AlatDetailBo alatDetailBo = (AlatDetailBo) ctx.getBean("alatDetailBoProxy");

        try {
            listOfAlatDetail = alatDetailBo.getByCriteria(alatDetail);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.searchAlatDetail] end process <<<");
        session.removeAttribute("listOfResultAlat");
        session.setAttribute("listOfResultAlat", listOfAlatDetail);

        List<TaskMember> listOfTaskMember = new ArrayList<TaskMember>();
        TaskMemberBo taskMemberBo = (TaskMemberBo) ctx.getBean("taskMemberBoProxy");
        TaskMember taskMember = new TaskMember();
        taskMember.setIdTask("");

        try {
            listOfTaskMember = taskMemberBo.getAll();
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.searchTaskMember] end process <<<");
        session.removeAttribute("listOfTaskMember");
        session.setAttribute("listOfTaskMember", listOfTaskMember);

        List<TaskMemberChange> listOfTaskMemberChange = new ArrayList<TaskMemberChange>();
        TaskMemberChangeBo taskMemberChangeBo = (TaskMemberChangeBo) ctx.getBean("taskMemberChangeBoProxy");
        TaskMemberChange taskMemberChange = new TaskMemberChange();
        taskMemberChange.setIdProject(itemId);

        try {
            listOfTaskMemberChange = taskMemberChangeBo.getByCriteria(taskMemberChange);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        logger.info("[ProjectAction.searchTaskMemberChange] end process <<<");
        session.removeAttribute("listOfTaskMemberChange");
        session.setAttribute("listOfTaskMemberChange", listOfTaskMemberChange);

        logger.info("[ProjectAction.viewProject] end process <<<");
        return "edit_project";
    }


    public String initEditMember(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Member> listOfResultMember = (List) session.getAttribute("listOfResultMember");
        session.removeAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResultMember != null) {

                for (Member memberOfList : listOfResultMember) {
                    if (id.equalsIgnoreCase(memberOfList.getIdProjectMember())) {
                        setMember(memberOfList);
                        break;
                    }
                }
            } else {
                setMember(new Member());
            }
        } else {
            setMember(new Member());
        }
        session.removeAttribute("listOfResult");
        return "init_edit_project_edit_member";
    }

    public String initDeleteMember(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Member> listOfResultMember = (List) session.getAttribute("listOfResultMember");
        List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");
        session.removeAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResultMember != null) {

                for (Member memberOfList : listOfResultMember) {
                    if (id.equalsIgnoreCase(memberOfList.getIdProjectMember())) {
                        for (Task listOftask :listOfResultTask){
                            if ("Y".equalsIgnoreCase(listOftask.getFlag())){
                                if (memberOfList.getIdMember().equalsIgnoreCase(listOftask.getAsignTo())){
                                    memberOfList.setIdTask(listOftask.getIdTask());
                                }
                            }else {
                                memberOfList.setIdTask("");
                            }
                        }
                        setMember(memberOfList);
                        break;
                    }
                }
            } else {
                setMember(new Member());
            }
        } else {
            setMember(new Member());
        }
        session.removeAttribute("listOfResult");
        return "init_edit_project_delete_member";
    }


    public String initEditTask(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");
        session.removeAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResultTask != null) {

                for (Task taskOfList : listOfResultTask) {
                    if (id.equalsIgnoreCase(taskOfList.getIdTask())) {
                        setTask(taskOfList);
                        break;
                    }
                }
            } else {
                setTask(new Task());
            }
        } else {
            setTask(new Task());
        }
        session.removeAttribute("listOfResult");
        return "init_edit_project_edit_task";
    }

    public String initDeleteTask(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");
        session.removeAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResultTask != null) {

                for (Task taskOfList : listOfResultTask) {
                    if (id.equalsIgnoreCase(taskOfList.getIdTask())) {
                        setTask(taskOfList);
                        break;
                    }
                }
            } else {
                setTask(new Task());
            }
        } else {
            setTask(new Task());
        }
        session.removeAttribute("listOfResult");
        return "init_edit_project_delete_task";
    }

    public String initLogTask(){
        logger.info("[ProjectAction.viewTaskList] start process >>>");

        String itemId = getId();
        List<TaskLog> listOfTask = new ArrayList<TaskLog>();
        try {
            listOfTask = projectBoProxy.getSearchTaskLogByCriteria(itemId);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.search");
            } catch (GeneralBOException e1) {
                logger.error("[ProjectAction.search] Error when saving error,", e1);
            }
            logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_search";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResultTaskList");
        session.setAttribute("listOfResultTaskList", listOfTask);
        logger.info("[ProjectAction.viewTaskList] end process <<<");

        return "init_log_task";
    }

    public String saveEditProject(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");
        List<Member> listOfResultMember = (List) session.getAttribute("listOfResultMember");
        List<Document> listOfResultDoc = (List) session.getAttribute("listOfResultDoc");
        List<ProjectIssue> listOfResultIssue = (List) session.getAttribute("listOfResultIssue");
        List<AlatDetail> listOfAlat = (List) session.getAttribute("listOfResultAlat");
        List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");

//        if (this.fileUploadDoc != null){
//            if (this.fileUploadDoc.length() <= 5242880 ){
//                return "failure_upload_doc";
//            }
//        }

        Project dataProject = getProject();
        if(listOfAlat != null){
            for(AlatDetail alatData : listOfAlat){
                dataProject.setIdAlatDetail(alatData.getIdAlatDetail());
            }
        }

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

        dataProject.setLastUpdate(updateTime);
        dataProject.setLastUpdateWho(userLogin);

        if(dataProject != null){
            try {
                projectBoProxy.saveEditProject(dataProject, listOfResultMember, listOfResultTask, listOfResultIssue, listOfResultDoc, listOfTaskMember);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.saveEditProject");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.search] Error when saving error,", e1);
                }
                logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return ERROR;
            }
        }

        String idProject = dataProject.getIdProject();

        if(idProject !=null){
            Document addDoc = new Document();
            addDoc.setLastUpdate(updateTime);
            addDoc.setUploadBy(userLogin);
            String idDoc = null;
            try{
                idDoc = "DOC"+projectBo.getSeqDocId();
                File fileToCreate = null;
                //note : for windows directory
                String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                if (this.fileUploadDoc != null) {
                    if (this.fileUploadDoc.length() > 0 && this.fileUploadDoc.length() <= 355242880) {
                        if ("image/jpeg".equalsIgnoreCase(this.fileUploadDocContentType)) {
                            String fileDocName = "IMG_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");
                        }
                        else if ("application/pdf".equalsIgnoreCase(this.fileUploadDocContentType)){
                            String filePathPdf = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                            String fileDocName = "PDF_" +idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePathPdf, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");
                        }
                        else if ("image/png".equalsIgnoreCase(this.fileUploadDocContentType)) {
                            String fileDocName = "IMG_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");
                        }
                        else {
                            String fileDocName = "FLE_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("file");

                        }
                    } else {
                        logger.error("[projectAction.saveDocUpload] Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        addActionError("Error,  Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
//                        return ERROR;
                        return "error_size_edit";

                    }
                }
                addDoc.setIdProject(idProject);
//                addDoc.setIdTask(idTask);
                try{
                    projectBoProxy.saveDocument(addDoc);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
                    } catch (GeneralBOException e1) {
                        logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                    }
                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
                }

            }catch (GeneralBOException e ){
                Long logId = null;
                try {
                    logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                }
                logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
            }
        }

//        projectBoProxy.updateStatusProject(dataProject);
        projectBoProxy.updateProgresListTask(idProject);
        projectBoProxy.updateStatusByDate();
//        initForm();
        logger.info("[ProjectAction.search] end process <<<");
        return "success_save_edit_project";
    }

    public String addDocTask(){
        logger.info("[ProjectAction.addDocTask] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfTask = (List) session.getAttribute("listOfResult");

        if(id != null && idTask != null) {
            if (listOfTask!=null){
                for (Task dataTask : listOfTask) {
                    if (idTask.equals(dataTask.getIdTask()) && "N".equals(dataTask.getIsTask())) {
                        setTask(dataTask);
                        break;
                    }
                }
            }else{
                setTask(new Task());
            }
        } else {
            setTask(new Task());
        }

        logger.info("[ProjectAction.addDocTask] end process <<<");
        return "view_add_doc_task";
    }

    public String saveDocUploadTask(){
        Project dataProject = getProject();
        Task dataTask = getTask();
        String idProject = dataTask.getIdProject();
        String idTask = dataTask.getIdTask();
        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

//        String idProject = dataProject.getIdProject();
        if(idProject !=null && idTask != null){
            Document addDoc = new Document();
            addDoc.setLastUpdate(updateTime);
            addDoc.setUploadBy(userLogin);
            ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
            ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
            String idDoc = null;
            try{
                idDoc = "DOC"+projectBo.getSeqDocId();
                File fileToCreate = null;
                //note : for windows directory
                String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                if (this.fileUploadDoc != null) {
                    if (this.fileUploadDoc.length() > 0 && this.fileUploadDoc.length() <= 5242880) {
                        if ("image/jpeg".equalsIgnoreCase(this.fileUploadDocContentType)) {
                            String fileDocName = "IMG_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");
                        }
                        else if ("application/pdf".equalsIgnoreCase(this.fileUploadDocContentType)){
                            String filePathPdf = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                            String fileDocName = "PDF_" +idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePathPdf, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("doc");
                        }
                        else {
                            String fileDocName = "FLE_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
                            try {
                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            addDoc.setDocType("file");
                        }
                    } else {
                        logger.error("[projectAction.saveProject] Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        addActionError("Error,  Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        return ERROR;
                    }
                }
                addDoc.setIdProject(idProject);
                addDoc.setIdTask(idTask);
                try{
                    projectBoProxy.saveDocument(addDoc);
                }catch (GeneralBOException e){
                    Long logId = null;
                    try {
                        logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
                    } catch (GeneralBOException e1) {
                        logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                    }
                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
                }

            }catch (GeneralBOException e ){
                Long logId = null;
                try {
                    logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                }
                logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
            }
        }
        return "save_doc_upload_task";
    }



    public String saveDeleteProject(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");
        List<Member> listOfResultMember = (List) session.getAttribute("listOfResultMember");
        List<Document> listOfResultDoc = (List) session.getAttribute("listOfResultDoc");
        List<ProjectIssue> listOfResultIssue = (List) session.getAttribute("listOfResultIssue");
        Project dataProject = getProject();

        if(dataProject != null){
            try {
                projectBoProxy.saveDeleteProject(dataProject, listOfResultMember, listOfResultTask, listOfResultIssue, listOfResultDoc);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = projectBoProxy.saveErrorMessage(e.getMessage(), "ProjectAction.saveEditProject");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.search] Error when saving error,", e1);
                }
                logger.error("[ProjectAction.search] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
                return "failure_search";
            }
        }
        session.removeAttribute("listOfResult");
        return "success_save_delete_project";
    }


    public String addProjectIssue(){
        return "init_add_project_issue";
    }

    public String saveAddProjectIssue(String namaIssue){
        HttpSession session = WebContextFactory.get().getSession();
        List<ProjectIssue> listOfProjectIssue = (List) session.getAttribute("listOfResultIssue");
        List<ProjectIssue> listOfProjectIssueNew = new ArrayList<ProjectIssue>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

        String seq;
        seq = projectBo.getSeqIssueId();

        String idIssue = "ISS"+seq;


        boolean flag = false;

        if(listOfProjectIssue != null){
            ProjectIssue entryIssue = new ProjectIssue();
            entryIssue.setIdIssue(idIssue);
            entryIssue.setNamaIssue(namaIssue);
            entryIssue.setIdMember(userLoginId);
            entryIssue.setLastUpdateWho(userLogin);
            entryIssue.setLastUpdate(updateTime);
            listOfProjectIssue.add(entryIssue);
            session.removeAttribute("listOfResultIssue");
            session.setAttribute("listOfResultIssue", listOfProjectIssue);
            logger.info("[ProjectAction.viewTaskList] saveAddProjectIssue <<<");
            flag = true;
        }else{
            ProjectIssue entryIssue = new ProjectIssue();
            entryIssue.setIdIssue(idIssue);
            entryIssue.setNamaIssue(namaIssue);
            entryIssue.setIdMember(userLoginId);
            entryIssue.setLastUpdateWho(userLogin);
            entryIssue.setLastUpdate(updateTime);
            listOfProjectIssueNew.add(entryIssue);
            session.removeAttribute("listOfResultIssue");
            session.setAttribute("listOfResultIssue", listOfProjectIssueNew);
            logger.info("[ProjectAction.viewTaskList] saveAddProjectIssue <<<");
            flag = true;
        }

        if (flag) {
            return "0";
        } else {
            return "1";
        }
    }

    public String editProjectIssue(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<ProjectIssue> listOfResultProjectIssue = (List) session.getAttribute("listOfResultIssue");
        session.removeAttribute("listOfResult");
        if (id != null && !"".equalsIgnoreCase(id)) {
            if (listOfResultProjectIssue != null) {
                for (ProjectIssue ProjectIssueOfList : listOfResultProjectIssue) {
                    if (id.equalsIgnoreCase(ProjectIssueOfList.getIdIssue())){
                        setProjectIssue(ProjectIssueOfList);
                        break;
                    }
                }
            } else {
                setProjectIssue(new ProjectIssue());
            }
        } else {
            setProjectIssue(new ProjectIssue());
        }
        session.removeAttribute("listOfResult");
        return "init_edit_project_issue";
    }

    public String saveEditProjectIssue(String idIssue, String namaIssue, String idMember, String lastUpdateWho ){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<ProjectIssue> listOfProjectIssue = (List) session.getAttribute("listOfResultIssue");
        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        if(listOfProjectIssue != null){
            for (ProjectIssue addIssue : listOfProjectIssue){
                if(updated == false){
                    if(idIssue.equalsIgnoreCase(addIssue.getIdIssue())){
                        addIssue.setIdIssue(idIssue);
                        addIssue.setNamaIssue(namaIssue);
                        addIssue.setIdMember(idMember);
                        addIssue.setLastUpdateWho(lastUpdateWho);
                        addIssue.setLastUpdate(updateTime);
                        updated = true;
                        listOfProjectIssue.remove(addIssue);
                        listOfProjectIssue.add(addIssue);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultIssue");
                            session.setAttribute("listOfResultIssue", listOfProjectIssue);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultIssue");
            session.setAttribute("listOfResultIssue", listOfProjectIssue);
            return "0";
        } else {
            return "1";
        }
    }

    public String deleteProjectIssue(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<ProjectIssue> listOfResultProjectIssue = (List) session.getAttribute("listOfResultIssue");
        session.removeAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResultProjectIssue != null) {
                for (ProjectIssue ProjectIssueOfList : listOfResultProjectIssue) {
                    if (id.equalsIgnoreCase(ProjectIssueOfList.getIdIssue())){
                        setProjectIssue(ProjectIssueOfList);
                        break;
                    }
                }
            } else {
                setProjectIssue(new ProjectIssue());
            }
        } else {
            setProjectIssue(new ProjectIssue());
        }
        session.removeAttribute("listOfResult");
        return "init_delete_project_issue";
    }

    public String saveDeleteProjectIssue(String IdIssue){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<ProjectIssue> listOfProjectIssue = (List) session.getAttribute("listOfResultIssue");
        if(listOfProjectIssue != null){
            for (ProjectIssue addProjectIssue : listOfProjectIssue){
                if(updated == false){
                    if(IdIssue.equalsIgnoreCase(addProjectIssue.getIdIssue())){
                        listOfProjectIssue.remove(addProjectIssue);
                        updated = true;
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultIssue");
                            session.setAttribute("listOfResultIssue", listOfProjectIssue);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultIssue");
            session.setAttribute("listOfResultIssue", listOfProjectIssue);
            return "0";
        } else {
            return "1";
        }

    }

    public String addProjectDoc(){
        return "init_add_project_doc";
    }


    public String saveSessionDoc(){
            HttpSession session = WebContextFactory.get().getSession();
            List<Document> listOfProjectDoc = (List) session.getAttribute("listOfResultDoc");
            List<Document> listOfProjectDocNew = new ArrayList<Document>();

            Document addDoc = new Document();
            ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
            ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
            String idDoc = null;

        if(listOfProjectDoc!=null){
            try{
                idDoc = "DOC"+projectBo.getSeqDocId();
                File fileToCreate = null;
                //note : for windows directory
                String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                if (this.fileUploadDoc != null) {
                    if (this.fileUploadDoc.length() > 0 && this.fileUploadDoc.length() <= 5242880) {
                        if ("image/jpeg".equalsIgnoreCase(this.fileUploadDocContentType)) {
                            String fileDocName = "IMG_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
//                            try {
//                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            listOfProjectDoc.add(addDoc);
                        }
                        else if ("application/pdf".equalsIgnoreCase(this.fileUploadDocContentType)){
                            String filePathPdf = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                            String fileDocName = "PDF_" +idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePathPdf, fileDocName);
//                            try {
//                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            listOfProjectDoc.add(addDoc);
                        }
                        else {
                            logger.error("[projectAction.saveDocUpload] Error when saving project, Found problem when upload file data (content type is unknow), please inform to your admin.");
                            addActionError("Error,  Error when saving project, Found problem when upload upload file data (content type is unknow), please inform to your admin.");
                            return ERROR;
                        }
                    } else {
                        logger.error("[projectAction.saveDocUpload] Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        addActionError("Error,  Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        return ERROR;
                    }
                }

            }catch (GeneralBOException e ){
                Long logId = null;
                try {
                    logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                }
                logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
            }

            session.removeAttribute("listOfResultDoc");
            session.setAttribute("listOfResultDoc", listOfProjectDoc);
            logger.info("[ProjectAction.viewTaskList] listOfResultDoc <<<");

        } else {

            try{
                idDoc = "DOC"+projectBo.getSeqDocId();
                File fileToCreate = null;
                //note : for windows directory
                String filePath = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                if (this.fileUploadDoc != null) {
                    if (this.fileUploadDoc.length() > 0 && this.fileUploadDoc.length() <= 5242880) {
                        if ("image/jpeg".equalsIgnoreCase(this.fileUploadDocContentType)) {
                            String fileDocName = "IMG_"+idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePath, fileDocName);
//                            try {
//                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            listOfProjectDocNew.add(addDoc);
                        }
                        else if ("application/pdf".equalsIgnoreCase(this.fileUploadDocContentType)){
                            String filePathPdf = CommonConstant.RESOURCE_PATH_SAVED_UPLOAD_DIRECTORY + ServletActionContext.getRequest().getContextPath() + CommonConstant.RESOURCE_PATH_USER_UPLOAD_DOC;
                            String fileDocName = "PDF_" +idDoc+"_"+ this.fileUploadDocFileName;
                            fileToCreate = new File(filePathPdf, fileDocName);
//                            try {
//                                FileUtils.copyFile(this.fileUploadDoc, fileToCreate);
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
                            addDoc.setNamaDoc(fileDocName);
                            addDoc.setIdDoc(idDoc);
                            listOfProjectDocNew.add(addDoc);
                        }
                        else {
                            logger.error("[projectAction.saveDocUpload] Error when saving project, Found problem when upload file data (content type is unknow), please inform to your admin.");
                            addActionError("Error,  Error when saving project, Found problem when upload upload file data (content type is unknow), please inform to your admin.");
                            return ERROR;
                        }
                    } else {
                        logger.error("[projectAction.saveDocUpload] Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        addActionError("Error,  Error when saving project, Found problem when upload file data (size file is empty or size file more then 5Mb), please inform to your admin.");
                        return ERROR;
                    }
                }

            }catch (GeneralBOException e ){
                Long logId = null;
                try {
                    logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id error,", e1);
                }
                logger.error("[ProjectAction.saveDocUpload] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
            }
            session.removeAttribute("listOfResultDoc");
            session.setAttribute("listOfResultDoc", listOfProjectDocNew);
            logger.info("[ProjectAction.viewTaskList] listOfResultDoc <<<");
        }
        return "save_project_doc";
    }

    public String addAlat(){
        return "init_add_alat";
    }

    public String saveAlat(String idAlatDetail, String namaAlat, String namaApp, String namaGi){
        HttpSession session = WebContextFactory.get().getSession();
        List<AlatDetail> listOfAlat = new ArrayList<AlatDetail>();

        boolean flag = false;
        AlatDetail addAlat = new AlatDetail();
        addAlat.setIdAlatDetail(idAlatDetail);
//        addAlat.setKodeAlat(kodeAlat);
        addAlat.setNamaAlatDetail(namaAlat);
        addAlat.setNamaApp(namaApp);
        addAlat.setNamaGi(namaGi);
        listOfAlat.add(addAlat);
        session.removeAttribute("listOfResultAlat");
        session.setAttribute("listOfResultAlat",listOfAlat);
        flag = true;

        if (flag) {
            return "0";
        } else {
            return "1";
        }
    }

    public String saveEditMemberEditProject(String idProjectMember, String idMember, String namaMember, String memberPosition, String idProject){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Member> listOfProjectMember = (List) session.getAttribute("listOfResultMember");
        List<Member> listMember = new ArrayList<Member>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
        UserBo userBo = (UserBo) ctx.getBean("userBoProxy");

        List<User> listUser = new ArrayList<User>();
        User dataUser = new User();
        dataUser.setUserId(idMember);
        dataUser.setFlag("Y");

        if(listOfProjectMember != null){
            for (Member addProjectMember : listOfProjectMember){
                String idm = addProjectMember.getIdProjectMember();
                if(updated == false){
                    if(idProjectMember.equalsIgnoreCase(idm)){
                        addProjectMember.setIdProjectMember(idProjectMember);
                        addProjectMember.setIdMember(idMember);
                        addProjectMember.setNamaMember(namaMember);
                        addProjectMember.setStPositionId(memberPosition);
                        addProjectMember.setAction("U");
                        if(addProjectMember.isAdded() != true){
                            addProjectMember.setAdded(false);
                        } else {
                            addProjectMember.setAdded(true);
                        }

                        String flagMember = addProjectMember.getFlag();
                        dataUser = userBo.getUserById(idMember, flagMember);

                        if (dataUser != null) {
                            addProjectMember.setPhotoUrl(dataUser.getPhotoUserUrl());
                            addProjectMember.setPhotoPath(dataUser.getPreviewPhoto());
                            addProjectMember.setRoleId(dataUser.getRoleId());
                            addProjectMember.setRoleName(dataUser.getRoleName());
                        }

                        if("Y".equalsIgnoreCase(addProjectMember.getFlag())){
                            addProjectMember.setStatusFlag("ACTIVE");
                        } else if ("N".equalsIgnoreCase(addProjectMember.getFlag())){
                            addProjectMember.setStatusFlag("NON ACTIVE");
                        }
                        Long Lposition = Long.parseLong(memberPosition);
                        addProjectMember.setPositionId(Lposition);
                        if(addProjectMember.getPositionId() == 4){
                            addProjectMember.setPositionName("STAFF");
                        } else if(addProjectMember.getPositionId() == 5){
                            addProjectMember.setPositionName("PIC");
                        }
                        updated = true;
                        listOfProjectMember.remove(addProjectMember);
                        listOfProjectMember.add(addProjectMember);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultMember");
                            session.setAttribute("listOfResultMember", listOfProjectMember);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultMember");
            session.setAttribute("listOfResultMember", listOfProjectMember);
            return "0";
        } else {
            return "1";
        }

    }

    public String SaveDeleteMemberEditProject(String idProjectMember, String idMember, String namaMember, String memberPosition, String idProject){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Member> listOfProjectMember = (List) session.getAttribute("listOfResultMember");
        List<Member> listMember = new ArrayList<Member>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

        UserBo userBo = (UserBo) ctx.getBean("userBoProxy");

        List<User> listUser = new ArrayList<User>();
        User dataUser = new User();
        dataUser.setUserId(idMember);
//        dataUser.setFlag("Y");

        if(listOfProjectMember != null){
            for (Member addProjectMember : listOfProjectMember){
                String idm = addProjectMember.getIdProjectMember();
                if(updated == false){
                    if(idProjectMember.equalsIgnoreCase(idm)){
                        addProjectMember.setIdProjectMember(idProjectMember);
                        addProjectMember.setIdMember(idMember);
                        addProjectMember.setNamaMember(namaMember);
                        addProjectMember.setFlag("N");
                        addProjectMember.setAction("U");
                        if(addProjectMember.isAdded() != true){
                            addProjectMember.setAdded(false);
                        } else {
                            addProjectMember.setAdded(true);
                        }

                        String flagMember = addProjectMember.getFlag();
                        dataUser = userBo.getUserById(idMember, "Y");

                        if (dataUser != null) {
                            addProjectMember.setPhotoUrl(dataUser.getPhotoUserUrl());
                            addProjectMember.setPhotoPath(dataUser.getPreviewPhoto());
                            addProjectMember.setRoleId(dataUser.getRoleId());
                            addProjectMember.setRoleName(dataUser.getRoleName());
                        }
                        if("Y".equalsIgnoreCase(addProjectMember.getFlag())){
                            addProjectMember.setStatusFlag("ACTIVE");
                        } else if ("N".equalsIgnoreCase(addProjectMember.getFlag())){
                            addProjectMember.setStatusFlag("NON ACTIVE");
                        }
                        Long Lposition = Long.parseLong(memberPosition);
                        addProjectMember.setPositionId(Lposition);
                        if(addProjectMember.getPositionId() == 4){
                            addProjectMember.setPositionName("STAFF");
                        } else if(addProjectMember.getPositionId() == 5){
                            addProjectMember.setPositionName("PIC");
                        }
                        updated = true;
                        listOfProjectMember.remove(addProjectMember);
                        listOfProjectMember.add(addProjectMember);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultMember");
                            session.setAttribute("listOfResultMember", listOfProjectMember);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultMember");
            session.setAttribute("listOfResultMember", listOfProjectMember);
            return "0";
        } else {
            return "1";
        }
    }

    public String SaveEditTaskEditProject(String idTask, String namaTask, String note, String priority, String deadline, String startDate, String status, String asignto, int progres){
        boolean flag =  false;
        boolean updated = false;
        java.util.Date utilDate = new java.util.Date();
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfProjectTask = (List) session.getAttribute("listOfResultTask");
        if(listOfProjectTask != null){
            for (Task addProjectTask : listOfProjectTask){
                if(updated == false){
                    if(idTask.equalsIgnoreCase(addProjectTask.getIdTask())){
                        addProjectTask.setIdTask(idTask);
                        addProjectTask.setNamaTask(namaTask);
                        addProjectTask.setNote(note);
                        addProjectTask.setPriority(priority);
                        addProjectTask.setStDeadline(deadline);
                        addProjectTask.setStStartDate(startDate);
                        addProjectTask.setStatusTask(status);
                        addProjectTask.setAsignTo(asignto);
                        addProjectTask.setFlag("Y");
                        addProjectTask.setAction("U");
                        addProjectTask.setEdited(true);
                        addProjectTask.setProgres(progres);
                        if (addProjectTask.isAdded() != true){
                            addProjectTask.setAdded(false);
                        } else {
                            addProjectTask.setAdded(true);
                        }

                        Date today = new Date(utilDate.getTime());

                        Date startDateTask = CommonUtil.convertToDate(startDate);
                        Date deadlineTask = CommonUtil.convertToDate(deadline);
                        addProjectTask.setStartDate(startDateTask);
                        addProjectTask.setDeadline(deadlineTask);

                        if(today.after(startDateTask) && today.before(deadlineTask) && progres < 100){
                            addProjectTask.setStatusTask("1");
                        }else if(today.after(deadlineTask) && progres < 100){
                            addProjectTask.setStatusTask("3");
                        }else if(today.before(startDateTask) && progres < 100){
                            addProjectTask.setStatusTask("5");
                        }else if("".equalsIgnoreCase(asignto)){
                            addProjectTask.setStatusTask("4");
                        }else if (progres == 100){
                            addProjectTask.setStatusTask("2");
                        }

                        if("1".equalsIgnoreCase(addProjectTask.getStatusTask())){
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
                        }else if("2".equalsIgnoreCase(addProjectTask.getStatusTask())){
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
                        }else if("3".equalsIgnoreCase(addProjectTask.getStatusTask())){
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
                        }else if("4".equalsIgnoreCase(addProjectTask.getStatusTask())){
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Initiate</div>");
                        }else if("5".equalsIgnoreCase(addProjectTask.getStatusTask())){
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                        }

                        if("1".equalsIgnoreCase(addProjectTask.getPriority())){
                            addProjectTask.setNamaPriority("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>High</div>");
                        } else if("2".equalsIgnoreCase(addProjectTask.getPriority())){
                            addProjectTask.setNamaPriority("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Normal</div>");
                        } else{
                            addProjectTask.setNamaPriority("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>Low</div>");
                        }

                        int hasil = progres + 50;
                        addProjectTask.setBar(String.valueOf(hasil));

                        String presentase = String.valueOf(progres);
                        String barWidth = addProjectTask.getBar();

                        addProjectTask.setProgresBar("<div style=\"width:150px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                                "<div align=\"center\" style=\"width:"+barWidth+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+presentase+"%</div>");

                        updated = true;
                        listOfProjectTask.remove(addProjectTask);
                        listOfProjectTask.add(addProjectTask);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultTask");
                            session.setAttribute("listOfResultTask", listOfProjectTask);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultTask");
            session.setAttribute("listOfResultTask", listOfProjectTask);
            return "0";
        } else {
            return "1";
        }
    }

    public String saveDeleteTaskEditProject(String idTask, String namaTask, String note, String priority, String deadline, String startDate, String status, String asignto, int progres){
        boolean flag =  false;
        boolean updated = false;
        java.util.Date utilDate = new java.util.Date();
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Task> listOfProjectTask = (List) session.getAttribute("listOfResultTask");
        if(listOfProjectTask != null){
            for (Task addProjectTask : listOfProjectTask){
                if(updated == false){
                    if(idTask.equalsIgnoreCase(addProjectTask.getIdTask())){
                        addProjectTask.setIdTask(idTask);
                        addProjectTask.setNamaTask(namaTask);
                        addProjectTask.setNote(note);
                        addProjectTask.setPriority(priority);
                        addProjectTask.setStDeadline(deadline);
                        addProjectTask.setStStartDate(startDate);
                        addProjectTask.setStatusTask(status);
                        addProjectTask.setAsignTo(asignto);
                        addProjectTask.setFlag("N");
                        addProjectTask.setAction("U");
                        addProjectTask.setStatusFlag("NON ACTIVE");
                        addProjectTask.setEdited(true);
                        addProjectTask.setProgres(progres);
                        if (addProjectTask.isAdded() != true){
                            addProjectTask.setAdded(false);
                        } else {
                            addProjectTask.setAdded(true);
                        }

                        Date today = new Date(utilDate.getTime());

                        Date startDateTask = CommonUtil.convertToDate(startDate);
                        Date deadlineTask = CommonUtil.convertToDate(deadline);

                        addProjectTask.setDeadline(deadlineTask);
                        addProjectTask.setStartDate(startDateTask);

                        if(today.after(startDateTask) && today.before(deadlineTask)){
                            addProjectTask.setStatusTask("1");
                        }else if(today.after(deadlineTask)){
                            addProjectTask.setStatusTask("3");
                        }else if(today.before(startDateTask)){
                            addProjectTask.setStatusTask("5");
                        }else if("".equalsIgnoreCase(asignto)){
                            addProjectTask.setStatusTask("4");
                        }

                        if("1".equalsIgnoreCase(addProjectTask.getStatusTask())){
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
                        }else if("2".equalsIgnoreCase(addProjectTask.getStatusTask())){
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Done</div>");
                        }else if("3".equalsIgnoreCase(addProjectTask.getStatusTask())){
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
                        }else if("4".equalsIgnoreCase(addProjectTask.getStatusTask())){
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Initiate</div>");
                        }else if("5".equalsIgnoreCase(addProjectTask.getStatusTask())){
                            addProjectTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
                        }

                        if("1".equalsIgnoreCase(addProjectTask.getPriority())){
                            addProjectTask.setNamaPriority("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>High</div>");
                        } else if("2".equalsIgnoreCase(addProjectTask.getPriority())){
                            addProjectTask.setNamaPriority("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Normal</div>");
                        } else{
                            addProjectTask.setNamaPriority("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>Low</div>");
                        }

                        int hasil = progres + 50;
                        addProjectTask.setBar(String.valueOf(hasil));

                        String presentase = String.valueOf(progres);
                        String barWidth = addProjectTask.getBar();

                        addProjectTask.setProgresBar("<div style=\"width:150px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                                "<div align=\"center\" style=\"width:"+barWidth+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+presentase+"%</div>");

                        updated = true;
                        listOfProjectTask.remove(addProjectTask);
                        listOfProjectTask.add(addProjectTask);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultTask");
                            session.setAttribute("listOfResultTask", listOfProjectTask);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultTask");
            session.setAttribute("listOfResultTask", listOfProjectTask);
            return "0";
        } else {
            return "1";
        }
    }

    public String editIssueEditProject(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<ProjectIssue> listOfResultProjectIssue = (List) session.getAttribute("listOfResultIssue");
//        session.removeAttribute("listOfResult");
        if (id != null && !"".equalsIgnoreCase(id)) {
            if (listOfResultProjectIssue != null) {
                for (ProjectIssue ProjectIssueOfList : listOfResultProjectIssue) {
                    if (id.equalsIgnoreCase(ProjectIssueOfList.getIdIssue())){
                        setProjectIssue(ProjectIssueOfList);
                        break;
                    }
                }
            } else {
                setProjectIssue(new ProjectIssue());
            }
        } else {
            setProjectIssue(new ProjectIssue());
        }
//        session.removeAttribute("listOfResult");
        return "init_edit_issue_edit_project";
    }

    public String deleteIssueEditProject(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<ProjectIssue> listOfResultProjectIssue = (List) session.getAttribute("listOfResultIssue");
//        session.removeAttribute("listOfResult");
        if (id != null && !"".equalsIgnoreCase(id)) {
            if (listOfResultProjectIssue != null) {
                for (ProjectIssue ProjectIssueOfList : listOfResultProjectIssue) {
                    if (id.equalsIgnoreCase(ProjectIssueOfList.getIdIssue())){
                        setProjectIssue(ProjectIssueOfList);
                        break;
                    }
                }
            } else {
                setProjectIssue(new ProjectIssue());
            }
        } else {
            setProjectIssue(new ProjectIssue());
        }
//        session.removeAttribute("listOfResult");
        return "init_delete_issue_edit_project";
    }

    public String saveEditIssueEditProject(String idIssue, String namaIssue, String idMember){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<ProjectIssue> listOfProjectIssue = (List) session.getAttribute("listOfResultIssue");
        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

        List<Member> listMember = new ArrayList<Member>();
        if(listOfProjectIssue != null){
            for (ProjectIssue addIssue : listOfProjectIssue){
                if(updated == false){
                    if(idIssue.equalsIgnoreCase(addIssue.getIdIssue())){
//                        addIssue.setIdProject(addIssue.getIdProject());
                        addIssue.setIdIssue(idIssue);
                        addIssue.setNamaIssue(namaIssue);
                        addIssue.setIdMember(userLoginId);
                        addIssue.setLastUpdateWho(userLogin);
                        addIssue.setLastUpdate(updateTime);
                        addIssue.setAction("U");
                        if(addIssue.isAdded() == true){
                            addIssue.setAdded(true);
                        }else{
                            addIssue.setAdded(false);
                        }

                        listMember  = projectBo.getSearchMemberByCriteria(addIssue.getIdProject());

                        if(listMember != null){
                            for (Member dataMember :listMember){
                                if(addIssue.getIdMember().equalsIgnoreCase(dataMember.getIdMember())){
                                    addIssue.setPhotoUrl(dataMember.getPhotoUrl());
                                    addIssue.setPhotoPath(dataMember.getPhotoPath());
                                }
                            }
                        }

//                        addIssue.setFlag();
                        if("Y".equalsIgnoreCase(addIssue.getFlag())){
                            addIssue.setStatusFlag("ACTIVE");
                        } else {
                            addIssue.setStatusFlag("NON ACTIVE");
                        }
                        updated = true;
                        listOfProjectIssue.remove(addIssue);
                        listOfProjectIssue.add(addIssue);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultIssue");
                            session.setAttribute("listOfResultIssue", listOfProjectIssue);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultIssue");
            session.setAttribute("listOfResultIssue", listOfProjectIssue);
            return "0";
        } else {
            return "1";
        }

    }

    public String saveDeleteIssueEditProject(String idIssue, String namaIssue, String idMember){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<ProjectIssue> listOfProjectIssue = (List) session.getAttribute("listOfResultIssue");
        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
        Project gProject = getProject();


        List<Member> listMember = new ArrayList<Member>();
        if(listOfProjectIssue != null){
            for (ProjectIssue addIssue : listOfProjectIssue){
                if(updated == false){
                    if(idIssue.equalsIgnoreCase(addIssue.getIdIssue())){
                        addIssue.setIdIssue(idIssue);
                        addIssue.setNamaIssue(namaIssue);
                        addIssue.setIdMember(userLoginId);
                        addIssue.setLastUpdateWho(userLogin);
                        addIssue.setLastUpdate(updateTime);
                        addIssue.setAction("U");
                        if(addIssue.isAdded() == true){
                            addIssue.setAdded(true);
                        }else{
                            addIssue.setAdded(false);
                        }

                        listMember  = projectBo.getSearchMemberByCriteria(addIssue.getIdProject());

                        if(listMember != null){
                            for (Member dataMember :listMember){
                                if(addIssue.getIdMember().equalsIgnoreCase(dataMember.getIdMember())){
                                    addIssue.setPhotoUrl(dataMember.getPhotoUrl());
                                    addIssue.setPhotoPath(dataMember.getPhotoPath());
                                }
                            }
                        }

                        addIssue.setFlag("N");
                        if("Y".equalsIgnoreCase(addIssue.getFlag())){
                            addIssue.setStatusFlag("ACTIVE");
                        } else {
                            addIssue.setStatusFlag("NON ACTIVE");
                        }
                        updated = true;
                        listOfProjectIssue.remove(addIssue);
                        listOfProjectIssue.add(addIssue);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultIssue");
                            session.setAttribute("listOfResultIssue", listOfProjectIssue);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultIssue");
            session.setAttribute("listOfResultIssue", listOfProjectIssue);
            return "0";
        } else {
            return "1";
        }
    }

    public String editAlatEditProject(){
        logger.info("[AlatDetailAction.initEdit] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfResult = (List) session.getAttribute("listOfResultAlat");
        session.removeAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (AlatDetail alatDetailOfList : listOfResult) {
                    if (id.equalsIgnoreCase(alatDetailOfList.getIdAlatDetail())) {
                        setAlatDetail(alatDetailOfList);
                        break;
                    }
                }
            } else {
                setAlatDetail(new AlatDetail());
            }
        } else {
            setAlatDetail(new AlatDetail());
        }
        session.removeAttribute("listOfResult");
        return "init_edit_alat_edit_project";
    }

    public String saveEditAlatEditProject(String idAlatDetail,String namaAlat, String namaApp, String namaGi){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<AlatDetail> listOfAlat = (List) session.getAttribute("listOfResultAlat");

        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();

        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
        Project gProject = getProject();

//        List<Member> listMember = new ArrayList<Member>();
        if(listOfAlat != null){
            for (AlatDetail addAlat : listOfAlat){
                if(updated == false){
                        addAlat.setIdAlatDetail(idAlatDetail);
                        addAlat.setNamaAlatDetail(namaAlat);
                        addAlat.setNamaApp(namaApp);
                        addAlat.setNamaGi(namaGi);

                        if(addAlat.isDeleted() == true){
                            addAlat.setStatusDelete("NON ACTIVE");
                        } else {
                            addAlat.setStatusDelete("ACTIVE");
                        }
                        updated = true;
                        listOfAlat.remove(addAlat);
                        listOfAlat.add(addAlat);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfResultAlat");
                            session.setAttribute("listOfResultAlat", listOfAlat);
                            return "0";
                        } else {
                            return "1";
                        }
                }
            }
        } if(flag){
            session.removeAttribute("listOfResultAlat");
            session.setAttribute("listOfResultAlat", listOfAlat);
            return "0";
        } else {
            return "1";
        }
    }

    public String addTaskEditProject(){
        return "init_add_task_edit_project";
    }

    public String saveAddTaskEditProject(String namaTask, String note, String priority, String deadline, String startDate, String asignto){
        HttpSession session = WebContextFactory.get().getSession();
        List<Task> listOfTask = (List) session.getAttribute("listOfResultTask");
        List<Task> listOfTaskNew = new ArrayList<Task>();
        java.util.Date utilDate = new java.util.Date();
        Date today = new Date(utilDate.getTime());

        boolean flag = false;

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
        String seqTaskId = null;
        try {
            seqTaskId = projectBo.getSeqNumTaskId();
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id error,", e1);
            }
            logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
        }

        Task addTask = new Task();
        addTask.setIdTask("TSK"+seqTaskId);
        addTask.setNamaTask(namaTask);
        addTask.setNote(note);
        addTask.setPriority(priority);
        addTask.setStDeadline(deadline);
        addTask.setStStartDate(startDate);
        addTask.setAsignTo(asignto);
//        addTask.setIdTask("TSK"+seqTaskId);
        addTask.setFlag("Y");
        addTask.setAction("C");
        addTask.setAdded(true);
        addTask.setEdited(true);
        addTask.setProgres(10);

        if ("Y".equalsIgnoreCase(addTask.getFlag())){
            addTask.setStatusFlag("ACTIVE");
        } else {
            addTask.setStatusFlag("NON ACTIVE");
        }

        Date startDateTask = CommonUtil.convertToDate(startDate);
        Date deadlineTask = CommonUtil.convertToDate(deadline);
        addTask.setDeadline(startDateTask);
        addTask.setStartDate(startDateTask);

        if(today.before(startDateTask)){
            addTask.setStatusTask("5");
            addTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Waiting Start</div>");
        } else if(today.after(startDateTask) && today.before(deadlineTask)){
            addTask.setStatusTask("1");
            addTask.setNamaStatus("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>In Progres</div>");
        } else if(today.after(deadlineTask)){
            addTask.setStatusTask("3");
            addTask.setNamaStatus("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>Overtime</div>");
        } else if(asignto == null){
            addTask.setStatusTask("4");
            addTask.setNamaStatus("<div align='center' style='background-color:#9f9f9f;padding:10px;border-radius:10px;color:#fff;'>Initiate</div>");
        }

        if("1".equalsIgnoreCase(addTask.getPriority())){
            addTask.setNamaPriority("<div align='center' style='background-color:#ff6565;padding:10px;border-radius:10px;color:#fff;'>High</div>");
        } else if("2".equalsIgnoreCase(addTask.getPriority())){
            addTask.setNamaPriority("<div align='center' style='background-color:#8be78f;padding:10px;border-radius:10px;color:#fff;'>Normal</div>");
        } else{
            addTask.setNamaPriority("<div align='center' style='background-color:#6582ff;padding:10px;border-radius:10px;color:#fff;'>Low</div>");
        }
        int hasil = addTask.getProgres() + 50;
        addTask.setBar(String.valueOf(hasil));

        String presentase = String.valueOf(addTask.getProgres());
        String barWidth = addTask.getBar();

        addTask.setProgresBar("<div style=\"width:150px;background-color:#111111;opacity:0.1;padding:20px;border-radius:20px;z-index:1;\"></div>\n" +
                "<div align=\"center\" style=\"width:"+barWidth+"px;background-color:#edcd00;padding:12px;border-radius:20px;margin-top:-40px;color:#fff;z-index:1;\">"+presentase+"%</div>");

        if(listOfTask != null){
            listOfTask.add(addTask);
            session.removeAttribute("listOfResultTask");
            session.setAttribute("listOfResultTask",listOfTask);
            flag = true;
        } else {
            listOfTaskNew.add(addTask);
            session.removeAttribute("listOfResultTask");
            session.setAttribute("listOfResultTask",listOfTaskNew);
            flag = true;
        }
        if (flag) {
            return "0";
        } else {
            return "1";
        }
    }

    public String addMemberEditProject(){
        return "init_add_member_edit_project";
    }

    public String saveAddMemberEditProject(String idMember, String namaMember, String memberPosition, String idProject){
        HttpSession session = WebContextFactory.get().getSession();
        List<Member> listOfMember = (List) session.getAttribute("listOfResultMember");
        List<Member> listOfMemberNew = new ArrayList<Member>();
//        String itemId = project.getIdProject();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
        UserBo userBo = (UserBo) ctx.getBean("userBoProxy");

        String seqMemberId = null;
        try {
            seqMemberId = projectBo.getSeqMemberId();
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id error,", e1);
            }
            logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
        }

        Long Lposition = Long.parseLong(memberPosition);
        boolean flag = false;


        Member addMember = new Member();
        addMember.setIdProjectMember("MBR"+seqMemberId);
        addMember.setIdMember(idMember);
        addMember.setNamaMember(namaMember);
        addMember.setPositionId(Lposition);
        addMember.setStPositionId(memberPosition);
        addMember.setFlag("Y");
        addMember.setAction("C");
        addMember.setAdded(true);

        User dataUser = new User();

        String flagMember = addMember.getFlag();
        dataUser = userBo.getUserById(idMember, flagMember);

        if (dataUser != null) {
            addMember.setPhotoUrl(dataUser.getPhotoUserUrl());
            addMember.setPhotoPath(dataUser.getPreviewPhoto());
            addMember.setRoleId(dataUser.getRoleId());
            addMember.setRoleName(dataUser.getRoleName());
        }

        if(addMember.getPositionId() == 4){
            addMember.setPositionName("STAFF");
        } else if(addMember.getPositionId() == 5){
            addMember.setPositionName("PIC");
        }

        if ("Y".equalsIgnoreCase(addMember.getFlag())){
            addMember.setStatusFlag("ACTIVE");
        }else {
            addMember.setStatusFlag("NON ACTIVE");
        }

        if(listOfMember != null){
            listOfMember.add(addMember);
            session.removeAttribute("listOfResultMember");
            session.setAttribute("listOfResultMember", listOfMember);
            flag = true;
        } else {
            listOfMemberNew.add(addMember);
            session.removeAttribute("listOfResultMember");
            session.setAttribute("listOfResultMember", listOfMemberNew);
            flag = true;
        }
        if (flag) {
            return "0";
        } else {
            return "1";
        }
    }

    public String addIssueEditProject(){
        return "init_add_issue_edit_project";
    }

    public String saveAddIssueEditProject(String namaIssue){
        HttpSession session = WebContextFactory.get().getSession();
        List<ProjectIssue> listOfProjectIssue = (List) session.getAttribute("listOfResultIssue");
        List<ProjectIssue> listOfProjectIssueNew = new ArrayList<ProjectIssue>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");

        String userLogin = CommonUtil.userLogin();
        String userLoginId = CommonUtil.userIdLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

        String seq;
        seq = projectBo.getSeqIssueId();

        String idIssue = "ISS"+seq;


        boolean flag = false;
        ProjectIssue entryIssue = new ProjectIssue();
        entryIssue.setIdIssue(idIssue);
        entryIssue.setNamaIssue(namaIssue);
        entryIssue.setIdMember(userLoginId);
        entryIssue.setLastUpdateWho(userLogin);
        entryIssue.setLastUpdate(updateTime);
        entryIssue.setFlag("Y");
        entryIssue.setAction("C");

        if ("Y".equalsIgnoreCase(entryIssue.getFlag())){
            entryIssue.setStatusFlag("ACTIVE");
        } else {
            entryIssue.setStatusFlag("NON ACTIVE");
        }

        entryIssue.setAdded(true);

        if(listOfProjectIssue != null){
            listOfProjectIssue.add(entryIssue);
            session.removeAttribute("listOfResultIssue");
            session.setAttribute("listOfResultIssue", listOfProjectIssue);
            logger.info("[ProjectAction.viewTaskList] saveAddProjectIssue <<<");
            flag = true;
        }else{
            listOfProjectIssueNew.add(entryIssue);
            session.removeAttribute("listOfResultIssue");
            session.setAttribute("listOfResultIssue", listOfProjectIssueNew);
            logger.info("[ProjectAction.viewTaskList] saveAddProjectIssue <<<");
            flag = true;
        }

        if (flag) {
            return "0";
        } else {
            return "1";
        }
    }


    public String initReport(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        return "init_report";
    }

    public String printReport(){
        Project dataProject = getProject();

        String noWo;
        String bulan;
        String tahun;
        String idProject;

        if (!"".equalsIgnoreCase(dataProject.getNoWo())){
            noWo = dataProject.getNoWo();
        } else{
            noWo = "%";
        }
        if (!"".equalsIgnoreCase(dataProject.getBulan())){
            bulan = dataProject.getBulan();
        }else {
            bulan = "%";
        }
        if (!"".equalsIgnoreCase(dataProject.getTahun())){
            tahun = dataProject.getTahun();
        }else {
            tahun = "%";
        }
        if (!"".equalsIgnoreCase(dataProject.getIdProject())){
            idProject = dataProject.getIdProject();
        }else {
            idProject = "%";
        }

        Map<String,String> mapBulan = new HashMap<String, String>();
        mapBulan.put("1","Januari");
        mapBulan.put("2","Februari");
        mapBulan.put("3","Maret");
        mapBulan.put("4","April");
        mapBulan.put("5","Mei");
        mapBulan.put("6","Juni");
        mapBulan.put("7","Juli");
        mapBulan.put("8","Agustus");
        mapBulan.put("9","September");
        mapBulan.put("10","Oktober");
        mapBulan.put("11","November");
        mapBulan.put("12","Desember");

        String labelBulan = "";
        for(String value : mapBulan.keySet()){
            if (value.equals(bulan)){
                labelBulan = mapBulan.get(value);
                break;
            }
        }

        if(idProject != null){
            reportParams.put("urlLogo", CommonConstant.URL_IMAGE_LOGO_REPORT);
//            reportParams.put("urlLogo", "/opt/tomcat/webapps/pmsapb/pages/images/logo-pln.png");
            reportParams.put("titleReport", "Report Detail Task Working Order");
            reportParams.put("idProject", idProject);
            reportParams.put("noWo", noWo);
            reportParams.put("bulan", bulan);
            reportParams.put("tahun", tahun);
            reportParams.put("labelBulan", labelBulan);

            try {
                preDownload();
            } catch (SQLException e) {
                Long logId = null;
                try {
                    logId = projectBoProxy.saveErrorMessage(e.getMessage(), "printReport");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.printReport] Error when downloading ,", e1);
                }
                logger.error("[ProjectAction.printReport] Error when print report ," + "[" + logId + "] Found problem when downloading data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when downloading data, please inform to your admin.");
                return "failure_print";
            }

        } else {
            logger.error("[ReportAction.printReportRealisasiBibit] Error when print report realiassi bibit, data musim tanam is empty , Found problem when downloading data, please inform to your admin.");
            addActionError("Error, Found problem when downloading data, list notification detail is empty, please inform to your admin.");
            return "failure_print";
        }
        logger.info("[ProjectAction.printReport] end process <<<");

        return "succes_print_report";
    }

    public String initAddMemberTask(){
        TaskMember taskMember = new TaskMember();
        taskMember.setIdTask(this.getId());
        setTaskMember(taskMember);
        return "init_add_member_task";
    }

    public String initAddMemberTaskEditProject(){
        TaskMember taskMember = new TaskMember();
        taskMember.setIdTask(this.getId());
        setTaskMember(taskMember);
        return "init_add_member_task_edit_project";
    }


    public String saveAddMemberTask(String idTask, String idMember, String memberPosition){
        HttpSession session = WebContextFactory.get().getSession();
        List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");
        List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");
        List<TaskMember> listOfTaskMemberNew = new ArrayList<TaskMember>();
        java.util.Date utilDate = new java.util.Date();
        Date today = new Date(utilDate.getTime());

        boolean flag = false;


        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
        String seqMemberTaskId = null;
        try {
            seqMemberTaskId = projectBo.getSeqMemberTaskId();
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id error,", e1);
            }
            logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
        }

        TaskMember addMemberTask = new TaskMember();

        for (Task dataTask:listOfResultTask){
            if (idTask.equals(dataTask.getIdTask())){
                addMemberTask.setStatus(dataTask.getStatusTask());
                addMemberTask.setNote(dataTask.getNote());
            }
        }

        addMemberTask.setIdTaskMember("TSKMBR"+seqMemberTaskId);
        addMemberTask.setIdMember(idMember);
        addMemberTask.setIdTask(idTask);
        addMemberTask.setPosition(memberPosition);
        addMemberTask.setFlag("Y");
        addMemberTask.setAction("C");

        if(listOfTaskMember != null){
            listOfTaskMember.add(addMemberTask);
            session.removeAttribute("listOfTaskMember");
            session.setAttribute("listOfTaskMember",listOfTaskMember);
            session.removeAttribute("listOfResultTaskMember");
            session.setAttribute("listOfResultTaskMember",listOfTaskMember);
            flag = true;


        } else {
            listOfTaskMemberNew.add(addMemberTask);
            session.removeAttribute("listOfTaskMember");
            session.setAttribute("listOfTaskMember",listOfTaskMemberNew);
            flag = true;
        }
        if (flag) {
            return "0";
        } else {
            return "1";
        }
//        return "save_add_member_task";
    }

    public String viewListTaskMember(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<TaskMember> listTaskMember = new ArrayList<TaskMember>();
        List<TaskMember> listTaskMemberNew = new ArrayList<TaskMember>();
        boolean result = false;

        if (session != null){
            List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");
            List<Member> listOfResultMember = (List) session.getAttribute("listOfResultMember");
            List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");

            if (listOfTaskMember!=null){
                for (TaskMember dataTaskMember: listOfTaskMember){
                    TaskMember setData;
                    if (id.equals(dataTaskMember.getIdTask())){
                        setData = new TaskMember();
                        setData.setIdTaskMember(dataTaskMember.getIdTaskMember());
                        setData.setIdMember(dataTaskMember.getIdMember());
                        setData.setIdTask(dataTaskMember.getIdTask());
                        setData.setFlag(dataTaskMember.getFlag());
                        setData.setAction(dataTaskMember.getAction());
                        setData.setPosition(dataTaskMember.getPosition());
                        setData.setCreatedDate(dataTaskMember.getCreatedDate());
                        setData.setCreatedWho(dataTaskMember.getCreatedWho());
                        setData.setLastUpdate(dataTaskMember.getLastUpdate());
                        setData.setLastUpdateWho(dataTaskMember.getLastUpdateWho());
                        setData.setNote(dataTaskMember.getNote());
                        setData.setStatus(dataTaskMember.getStatus());

                        if (listOfResultMember != null && setData.getIdMember() != null){
                            for (Member listMember : listOfResultMember){
                                if (setData.getIdMember().equals(listMember.getIdMember())){
                                    setData.setNamaMember(listMember.getNamaMember());
                                }
                            }
                        }
                        if (listOfResultTask != null && setData.getIdMember() != null){
                            for (Task listTask : listOfResultTask){
                                if (setData.getIdTask().equals(listTask.getIdTask())){
                                    setData.setNamaTask(listTask.getNamaTask());
                                }
                            }
                        }

                        if ("5".equals(setData.getPosition())){
                            setData.setNamaPosition("PIC");
                        } else {
                            setData.setNamaPosition("STAFF");
                        }

                        if ("Y".equals(setData.getFlag())){
                            setData.setStatusFlag("ACTIVE");
                        }else {
                            setData.setStatusFlag("NON ACTIVE");
                        }
                        listTaskMember.add(setData);
                    }
                    session.removeAttribute("listOfResulTaskMember");
                    session.setAttribute("listOfResulTaskMember", listTaskMember);
                }
            }
        }

        if (atr.equals("edit")){
            return "view_list_task_member_edit";
        }
        if (atr.equals("add")){
            return "view_list_task_member";
        }
        return null;
    }

    public String viewDeleteTaskMemberAdd(){
        HttpSession session = ServletActionContext.getRequest().getSession();
//        List<TaskMember> listTaskMember = new ArrayList<TaskMember>();

        if (session != null){
            List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");
//            List<TaskMember> listOfResultTaskMember = (List) session.getAttribute("listOfResultTaskMember");
            List<Member> listOfResultMember = (List) session.getAttribute("listOfResultMember");
            List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");

            if (listOfTaskMember!=null){
                for (TaskMember dataTaskMember: listOfTaskMember){
                    TaskMember setData;
                    if (id.equals(dataTaskMember.getIdTaskMember())){
                        setData = new TaskMember();
                        setData.setIdTaskMember(dataTaskMember.getIdTaskMember());
                        setData.setIdMember(dataTaskMember.getIdMember());
                        setData.setIdTask(dataTaskMember.getIdTask());
                        setData.setFlag(dataTaskMember.getFlag());
                        setData.setAction(dataTaskMember.getAction());
                        setData.setPosition(dataTaskMember.getPosition());
                        setData.setCreatedDate(dataTaskMember.getCreatedDate());
                        setData.setCreatedWho(dataTaskMember.getCreatedWho());
                        setData.setLastUpdate(dataTaskMember.getLastUpdate());
                        setData.setLastUpdateWho(dataTaskMember.getLastUpdateWho());
                        setData.setNote(dataTaskMember.getNote());
                        setData.setStatus(dataTaskMember.getStatus());

                        if (listOfResultMember != null && setData.getIdMember() != null){
                            for (Member listMember : listOfResultMember){
                                if (setData.getIdMember().equals(listMember.getIdMember())){
                                    setData.setNamaMember(listMember.getNamaMember());
                                }
                            }
                        }
                        if (listOfResultTask != null && setData.getIdMember() != null){
                            for (Task listTask : listOfResultTask){
                                if (setData.getIdTask().equals(listTask.getIdTask())){
                                    setData.setNamaTask(listTask.getNamaTask());
                                }
                            }
                        }

                        if ("5".equals(setData.getPosition())){
                            setData.setNamaPosition("PIC");
                        } else {
                            setData.setNamaPosition("STAFF");
                        }
                        setTaskMember(setData);
                    }
                }
            }
        }

        return "view_delete_task_member_add";
    }

    public String saveDeleteTaskMemberAdd(String IdTaskMember){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");
        if(listOfTaskMember != null){
            for (TaskMember listTaskMember : listOfTaskMember){
                if(updated == false){
                    if(IdTaskMember.equals(listTaskMember.getIdTaskMember())){
                        listOfTaskMember.remove(listTaskMember);
                        updated = true;
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfTaskMember");
                            session.setAttribute("listOfTaskMember", listOfTaskMember);
                            session.removeAttribute("listOfResultTaskMember");
                            session.setAttribute("listOfResultTaskMember", listOfTaskMember);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        }
        if(flag){
            session.removeAttribute("listOfTaskMember");
            session.setAttribute("listOfTaskMember", listOfTaskMember);
            session.removeAttribute("listOfResultTaskMember");
            session.setAttribute("listOfResultTaskMember", listOfTaskMember);
            return "0";
        } else {
            return "1";
        }
    }

    public String viewDeleteTaskMemberEdit(){
        HttpSession session = ServletActionContext.getRequest().getSession();
//        List<TaskMember> listTaskMember = new ArrayList<TaskMember>();

        if (session != null){
            List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");
//            List<TaskMember> listOfResultTaskMember = (List) session.getAttribute("listOfResulTaskMember");
            List<Member> listOfResultMember = (List) session.getAttribute("listOfResultMember");
            List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");

            if (listOfTaskMember!=null){
                for (TaskMember dataTaskMember: listOfTaskMember){
                    TaskMember setData;
                    if (id.equals(dataTaskMember.getIdTaskMember())){
                        setData = new TaskMember();
                        setData.setIdTaskMember(dataTaskMember.getIdTaskMember());
                        setData.setIdMember(dataTaskMember.getIdMember());
                        setData.setIdTask(dataTaskMember.getIdTask());
                        setData.setFlag(dataTaskMember.getFlag());
                        setData.setAction(dataTaskMember.getAction());
                        setData.setPosition(dataTaskMember.getPosition());
                        setData.setCreatedDate(dataTaskMember.getCreatedDate());
                        setData.setCreatedWho(dataTaskMember.getCreatedWho());
                        setData.setLastUpdate(dataTaskMember.getLastUpdate());
                        setData.setLastUpdateWho(dataTaskMember.getLastUpdateWho());
                        setData.setNote(dataTaskMember.getNote());
                        setData.setStatus(dataTaskMember.getStatus());

                        if (listOfResultMember != null && setData.getIdMember() != null){
                            for (Member listMember : listOfResultMember){
                                if (setData.getIdMember().equals(listMember.getIdMember())){
                                    setData.setNamaMember(listMember.getNamaMember());
                                }
                            }
                        }
                        if (listOfResultTask != null && setData.getIdMember() != null){
                            for (Task listTask : listOfResultTask){
                                if (setData.getIdTask().equals(listTask.getIdTask())){
                                    setData.setNamaTask(listTask.getNamaTask());
                                }
                            }
                        }

                        if ("5".equals(setData.getPosition())){
                            setData.setNamaPosition("PIC");
                        } else {
                            setData.setNamaPosition("STAFF");
                        }
                        setTaskMember(setData);
                    }
                }
            }
        }
        return "view_delete_task_member_edit";
    }

    public String saveDeleteTaskMemberEdit(String idTaskMember){
        boolean flag =  false;
        boolean updated = false;
        java.util.Date utilDate = new java.util.Date();
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");
        if(listOfTaskMember != null){
            for (TaskMember addProjectTask : listOfTaskMember){
                if(updated == false){
                    if(idTaskMember.equalsIgnoreCase(addProjectTask.getIdTaskMember())){
                        addProjectTask.setIdTaskMember(idTaskMember);
                        addProjectTask.setFlag("N");
                        addProjectTask.setAction("U");

                        if (addProjectTask.isAdded() != true){
                            addProjectTask.setAdded(false);
                        } else {
                            addProjectTask.setAdded(true);
                        }

                        updated = true;
                        listOfTaskMember.remove(addProjectTask);
                        listOfTaskMember.add(addProjectTask);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfTaskMember");
                            session.setAttribute("listOfTaskMember", listOfTaskMember);
                            session.removeAttribute("listOfResulTaskMember");
                            session.setAttribute("listOfResulTaskMember", listOfTaskMember);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfTaskMember");
            session.setAttribute("listOfTaskMember", listOfTaskMember);
            session.removeAttribute("listOfResulTaskMember");
            session.setAttribute("listOfResulTaskMember", listOfTaskMember);
            return "0";
        } else {
            return "1";
        }
    }

    public String addTaskMemberEdit(String idTask, String idMember, String memberPosition){
        HttpSession session = WebContextFactory.get().getSession();
        List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");
        List<TaskMember> listOfResultTaskMember = (List) session.getAttribute("listOfResultTaskMember");
        List<Task> listOfResultTask = (List) session.getAttribute("listOfResultTask");
        List<TaskMember> listOfTaskMemberNew = new ArrayList<TaskMember>();
        java.util.Date utilDate = new java.util.Date();
        Date today = new Date(utilDate.getTime());

        boolean flag = false;


        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
        String seqMemberTaskId = null;
        try {
            seqMemberTaskId = projectBo.getSeqMemberTaskId();
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = projectBo.saveErrorMessage(e.getMessage(), "petaniBo.getSeqNumLahanId");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id error,", e1);
            }
            logger.error("[PermohonanLahanAction.saveLahan] Error when get sequence lahan id," + "[" + logId + "] Found problem when get sequence lahan id, please inform to your admin.", e);
        }

        TaskMember addMemberTask = new TaskMember();

        for (Task dataTask:listOfResultTask){
            if (idTask.equals(dataTask.getIdTask())){
                addMemberTask.setStatus(dataTask.getStatusTask());
                addMemberTask.setNote(dataTask.getNote());
            }
        }

        addMemberTask.setIdTaskMember("TSKMBR"+seqMemberTaskId);
        addMemberTask.setIdMember(idMember);
        addMemberTask.setIdTask(idTask);
        addMemberTask.setPosition(memberPosition);
        addMemberTask.setFlag("Y");
        addMemberTask.setAction("C");
        addMemberTask.setProgres(10);
        addMemberTask.setAdded(true);

        if(listOfTaskMember != null){
            listOfTaskMember.add(addMemberTask);
            session.removeAttribute("listOfTaskMember");
            session.setAttribute("listOfTaskMember",listOfTaskMember);
            session.removeAttribute("listOfResulTaskMember");
            session.setAttribute("listOfResulTaskMember",listOfTaskMember);
            flag = true;


        } else {
            listOfTaskMemberNew.add(addMemberTask);
            session.removeAttribute("listOfTaskMember");
            session.setAttribute("listOfTaskMember",listOfTaskMemberNew);
            session.removeAttribute("listOfResulTaskMember");
            session.setAttribute("listOfResulTaskMember",listOfTaskMember);
            flag = true;
        }
        if (flag) {
            return "0";
        } else {
            return "1";
        }
    }

    public String viewEditTaskMemberEdit(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");
        if (listOfTaskMember!=null){
            for (TaskMember listData : listOfTaskMember){
                if (id.equals(listData.getIdTaskMember())){
                    TaskMember taskMemberData = new TaskMember();
                    taskMemberData.setIdTaskMember(listData.getIdTaskMember());
                    taskMemberData.setIdMember(listData.getIdMember());
                    taskMemberData.setStatus(listData.getStatus());
                    taskMemberData.setIdTask(listData.getIdTask());
                    taskMemberData.setPosition(listData.getPosition());
                    taskMemberData.setStPosition(listData.getPosition());
                    taskMemberData.setNamaPosition(listData.getNamaPosition());
                    setTaskMember(taskMemberData);
                }
            }
        }
        return "view_edit_task_member_edit";
    }

    public String saveEditTaskMemberEdit(String idTaskMember, String idTask, String idMember, String memberPosition){
        boolean flag =  false;
        boolean updated = false;
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<TaskMember> listOfTaskMember = (List) session.getAttribute("listOfTaskMember");
        List<TaskMember> listOfResulTaskMember = (List) session.getAttribute("listOfResulTaskMember");
        List<Member> listOfResultMember = (List) session.getAttribute("listOfResultMember");
        List<Member> listMember = new ArrayList<Member>();

//        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
//        ProjectBo projectBo = (ProjectBo) ctx.getBean("projectBoProxy");
//        UserBo userBo = (UserBo) ctx.getBean("userBoProxy");
//
//        List<User> listUser = new ArrayList<User>();
//        User dataUser = new User();
//        dataUser.setUserId(idMember);
//        dataUser.setFlag("Y");


        if(listOfTaskMember != null){
            for (TaskMember listData : listOfTaskMember){
                if(updated == false){
                    if(idTaskMember.equals(listData.getIdTaskMember())){
                        listData.setIdTaskMember(idTaskMember);
                        listData.setIdMember(idMember);
                        listData.setIdTask(idTask);
                        listData.setPosition(memberPosition);
                        listData.setAction("U");
                        if(listData.isAdded() != true){
                            listData.setAdded(false);
                        } else {
                            listData.setAdded(true);
                        }

                        if (listOfResultMember != null){
                            for (Member listMemberData :listOfResultMember){
                                if(idMember.equals(listMemberData.getIdMember())){
                                    listData.setNamaMember(listMemberData.getNamaMember());
                                }
                            }
                        }


                        if("Y".equals(listData.getFlag())){
                            listData.setStatusFlag("ACTIVE");
                        } else if ("N".equals(listData.getFlag())){
                            listData.setStatusFlag("NON ACTIVE");
                        }

                        if("4".equals(listData.getPosition())){
                            listData.setNamaPosition("STAFF");
                        } else if("5".equals(listData.getPosition())){
                            listData.setNamaPosition("PIC");
                        }
                        updated = true;
                        listOfTaskMember.remove(listData);
                        listOfTaskMember.add(listData);
                        flag = true;
                        if(flag){
                            session.removeAttribute("listOfTaskMember");
                            session.setAttribute("listOfTaskMember", listOfTaskMember);
                            session.removeAttribute("listOfResulTaskMember");
                            session.setAttribute("listOfResulTaskMember", listOfTaskMember);
                            return "0";
                        } else {
                            return "1";
                        }
                    }
                }
            }
        } if(flag){
            session.removeAttribute("listOfTaskMember");
            session.setAttribute("listOfTaskMember", listOfTaskMember);
            session.removeAttribute("listOfResulTaskMember");
            session.setAttribute("listOfResulTaskMember", listOfTaskMember);
            return "0";
        } else {
            return "1";
        }
    }

    public String initReportSumary(){
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        return "init_report_sumary";
    }

    public String printReportSumary(){
        Project dataProject = getProject();

        String noWo;
        String bulan;
        String tahun;
        String idProject;

        if (!"".equalsIgnoreCase(dataProject.getNoWo())){
            noWo = dataProject.getNoWo();
        } else{
            noWo = "%";
        }
        if (!"".equalsIgnoreCase(dataProject.getBulan())){
            bulan = dataProject.getBulan();
        }else {
            bulan = "%";
        }
        if (!"".equalsIgnoreCase(dataProject.getTahun())){
            tahun = dataProject.getTahun();
        }else {
            tahun = "%";
        }
        if (!"".equalsIgnoreCase(dataProject.getIdProject())){
            idProject = dataProject.getIdProject();
        }else {
            idProject = "%";
        }

        Map<String,String> mapBulan = new HashMap<String, String>();
        mapBulan.put("1","Januari");
        mapBulan.put("2","Februari");
        mapBulan.put("3","Maret");
        mapBulan.put("4","April");
        mapBulan.put("5","Mei");
        mapBulan.put("6","Juni");
        mapBulan.put("7","Juli");
        mapBulan.put("8","Agustus");
        mapBulan.put("9","September");
        mapBulan.put("10","Oktober");
        mapBulan.put("11","November");
        mapBulan.put("12","Desember");

        String labelBulan = "";
        for(String value : mapBulan.keySet()){
            if (value.equals(bulan)){
                labelBulan = mapBulan.get(value);
                break;
            }
        }

        if(idProject != null){
            reportParams.put("urlLogo", CommonConstant.URL_IMAGE_LOGO_REPORT);
//            reportParams.put("urlLogo", "/opt/tomcat/webapps/pmsapb/pages/images/logo-pln.png");
            reportParams.put("titleReport", "Report Sumary Project Working Order");
            reportParams.put("idProject", idProject);
            reportParams.put("noWo", noWo);
            reportParams.put("bulan", bulan);
            reportParams.put("tahun", tahun);
            reportParams.put("labelBulan", labelBulan);

            try {
                preDownload();
            } catch (SQLException e) {
                Long logId = null;
                try {
                    logId = projectBoProxy.saveErrorMessage(e.getMessage(), "printReport");
                } catch (GeneralBOException e1) {
                    logger.error("[ProjectAction.printReport] Error when downloading ,", e1);
                }
                logger.error("[ProjectAction.printReport] Error when print report ," + "[" + logId + "] Found problem when downloading data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when downloading data, please inform to your admin.");
                return "failure_print";
            }

        } else {
            logger.error("[ReportAction.printReportRealisasiBibit] Error when print report realiassi bibit, data musim tanam is empty , Found problem when downloading data, please inform to your admin.");
            addActionError("Error, Found problem when downloading data, list notification detail is empty, please inform to your admin.");
            return "failure_print";
        }
        logger.info("[ProjectAction.printReport] end process <<<");

        return "succes_print_report_sumary";
    }



    public String paging(){
        return SUCCESS;
    }

}
