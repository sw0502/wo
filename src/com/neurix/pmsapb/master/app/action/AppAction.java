package com.neurix.pmsapb.master.app.action;

import com.neurix.common.action.BaseMasterAction;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.master.app.bo.AppBo;
import com.neurix.pmsapb.master.app.model.App;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 11/09/17
 * Time: 0:07
 * To change this template use File | Settings | File Templates.
 */
public class AppAction extends BaseMasterAction {
    protected static Logger logger = Logger.getLogger(AppAction.class);
    private App app;
    private AppBo appBoProxy;

    private List<App> initComboAppSearch;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        AppAction.logger = logger;
    }

    public List<App> getInitComboAppSearch() {
        return initComboAppSearch;
    }

    public void setInitComboAppSearch(List<App> initComboAppSearch) {
        this.initComboAppSearch = initComboAppSearch;
    }



    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    public void setAppBoProxy(AppBo appBoProxy) {
        this.appBoProxy = appBoProxy;
    }

    public AppBo getAppBoProxy() {
        return appBoProxy;
    }


    public App init(String kode, String flag){
        logger.info("[AppAction.init] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<App> listOfResult = (List<App>) session.getAttribute("listOfResult");

        if(kode != null && !"".equalsIgnoreCase(kode)){
            if(listOfResult != null){
                for (App app: listOfResult) {
                    if(kode.equalsIgnoreCase(app.getKodeApp()) && flag.equalsIgnoreCase(app.getFlag())){
                        setApp(app);
                        break;
                    }
                }
            } else {
                setApp(new App());
            }

            logger.info("[AppAction.init] end process <<<");
        }
        return getApp();
    }

    @Override
    public String add() {
        logger.info("[AppAction.add] start process >>>");
        App addApp = new App();
        setApp(addApp);

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[AppAction.add] stop process >>>");
        return "init_add";
    }

    @Override
    public String edit() {
        logger.info("[AppAction.edit] start process >>>");
        String itemId = getId();
        String itemFlag = getFlag();

        App app = new App();

        if(itemFlag != null){
            try {
                app = init(itemId, itemFlag);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = appBoProxy.saveErrorMessage(e.getMessage(), "AppBO.getAppByCriteria");
                } catch (GeneralBOException e1) {
                    logger.error("[AppAction.edit] Error when retrieving edit data,", e1);
                }
                logger.error("[AppAction.edit] Error when retrieving item," + "[" + logId + "] Found problem when retrieving data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when retrieving data for edit, please inform to your admin.");
                return "failure";
            }

            if(app != null) {
                setApp(app);
            } else {
                app.setFlag(itemFlag);
                app.setKodeApp(itemId);
                setApp(app);
                addActionError("Error, Unable to find data with id = " + itemId);
                return "failure";
            }
        } else {
            app.setKodeApp(itemId);
            app.setFlag(getFlag());
            setApp(app);
            addActionError("Error, Unable to edit again with flag = N.");
            return "failure";
        }

        logger.info("[AppAction.edit] end process >>>");
        return "init_edit";
    }

    @Override
    public String delete() {
        logger.info("[AppAction.delete] start process >>>");

        String itemId = getId();
        String itemFlag = getFlag();
        App app = new App();

        if (itemFlag != null ) {

            try {
                app = init(itemId, itemFlag);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = appBoProxy.saveErrorMessage(e.getMessage(), "AppBO.getAppById");
                } catch (GeneralBOException e1) {
                    logger.error("[AppAction.delete] Error when retrieving delete data,", e1);
                }
                logger.error("[AppAction.delete] Error when retrieving item," + "[" + logId + "] Found problem when retrieving data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when retrieving data for delete, please inform to your admin.");
                return "failure";
            }

            if (app != null) {
                setApp(app);

            } else {
                app.setKodeApp(itemId);
                app.setFlag(itemFlag);
                setApp(app);
                addActionError("Error, Unable to find data with id = " + itemId);
                return "failure";
            }
        } else {
            app.setKodeApp(itemId);
            app.setFlag(itemFlag);
            setApp(app);
            addActionError("Error, Unable to delete again with flag = N.");
            return "failure";
        }

        logger.info("[AppAction.delete] end process <<<");

        return "init_delete";
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        return null;
    }

    @Override
    public String search() {
        logger.info("[AppAction.search] start process >>>");

        App searchApp = getApp();
        List<App> listOfSearchApp = new ArrayList<App>();

        try {
            listOfSearchApp = appBoProxy.getByCriteria(searchApp);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = appBoProxy.saveErrorMessage(e.getMessage(), "AppBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[AppAction.search] Error when saving error,", e1);
            }
            logger.error("[AppAction.save] Error when searching app by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin" );
            return "failure";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfSearchApp);

        logger.info("[AppAction.search] end process <<<");

        return SUCCESS;
    }

    @Override
    public String initForm() {
        logger.info("[AppAction.initForm] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        logger.info("[AppAction.initForm] end process >>>");
        return INPUT;
    }

    public String saveEdit(){
        logger.info("[AppAction.saveEdit] start process >>>");
        try {

            App editApp = getApp();

            String userLogin = CommonUtil.userLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            editApp.setLastUpdateWho(userLogin);
            editApp.setLastUpdate(updateTime);
            editApp.setAction("U");

            appBoProxy.saveEdit(editApp);

        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = appBoProxy.saveErrorMessage(e.getMessage(), "AppBO.saveEdit");
            } catch (GeneralBOException e1) {
                logger.error("[AppAction.saveEdit] Error when saving error,", e1);
            }
            logger.error("[AppAction.saveEdit] Error when editing item app," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving edit data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        logger.info("[AppAction.saveEdit] end process <<<");

        return "success_save_edit";
    }

    public String saveAdd(){
        logger.info("[AppAction.saveAdd] start process >>>");

        try {
            App app = getApp();
            String userLogin = CommonUtil.userLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            app.setCreatedWho(userLogin);
            app.setLastUpdate(updateTime);
            app.setCreatedDate(updateTime);
            app.setLastUpdateWho(userLogin);
            app.setAction("C");
            app.setFlag("Y");

            appBoProxy.saveAdd(app);
        }catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = appBoProxy.saveErrorMessage(e.getMessage(), "AppBO.saveAdd");
            } catch (GeneralBOException e1) {
                logger.error("[AppAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AppAction.saveAdd] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[AppAction.saveAdd] end process >>>");
        return "success_save_add";
    }

    public String saveDelete(){
        logger.info("[AppAction.saveDelete] start process >>>");
        try {

            App deleteApp = getApp();

            String userLogin = CommonUtil.userLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            deleteApp.setLastUpdate(updateTime);
            deleteApp.setLastUpdateWho(userLogin);
            deleteApp.setAction("U");
            deleteApp.setFlag("N");

            appBoProxy.saveDelete(deleteApp);

        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = appBoProxy.saveErrorMessage(e.getMessage(), "AppBO.saveDelete");
            } catch (GeneralBOException e1) {
                logger.error("[AppAction.saveDelete] Error when saving error,", e1);
            }
            logger.error("[AppAction.saveDelete] Error when editing item app," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving edit data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        logger.info("[AppAction.saveDelete] end process <<<");

        return "success_save_delete";
    }

    public List initComboAppSearch(String query){
        logger.info("[PermohonanLahanAction.initComboLokasiKebun] start process >>>");
        List<App> listOfApp = new ArrayList<App>();

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        AppBo appBo = (AppBo) ctx.getBean("appBoProxy");

        try {
            listOfApp = appBo.getComboAppWithCriteria(query);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = appBo.saveErrorMessage(e.getMessage(), "DesaBo.getComboDesaWithCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when saving error,", e1);
            }
            logger.error("[PermohonanLahanAction.initComboLokasiKebun] Error when get combo lokasi kebun," + "[" + logId + "] Found problem when retrieving combo lokasi kebun data, please inform to your admin.", e);
        }
        logger.info("[PermohonanLahanAction.initComboLokasiKebu" +
                "" +
                "n] end process <<<");
        return listOfApp;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }
}
