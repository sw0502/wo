package com.neurix.pmsapb.master.app.model;

import com.neurix.common.model.BaseModel;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 10/09/17
 * Time: 23:59
 * To change this template use File | Settings | File Templates.
 */
public class App extends BaseModel {
    private String kodeApp;
    private String namaWilayah;

    public String getKodeApp() {
        return kodeApp;
    }

    public void setKodeApp(String kodeApp) {
        this.kodeApp = kodeApp;
    }

    public String getNamaWilayah() {
        return namaWilayah;
    }

    public void setNamaWilayah(String namaWilayah) {
        this.namaWilayah = namaWilayah;
    }
}
