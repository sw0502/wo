package com.neurix.pmsapb.master.app.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.app.model.ImApbAppEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 11/09/17
 * Time: 0:08
 * To change this template use File | Settings | File Templates.
 */
public class AppDao extends GenericDao<ImApbAppEntity, String>{
    @Override
    protected Class<ImApbAppEntity> getEntityClass() {
        return ImApbAppEntity.class;
    }

    @Override
    public List<ImApbAppEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImApbAppEntity.class);

        // Get by collection key
        if (mapCriteria!=null) {
            if (mapCriteria.get("kode_app")!=null) {
                criteria.add(Restrictions.eq("kodeApp", (String) mapCriteria.get("kode_app")));
            }
            if (mapCriteria.get("nama_wilayah")!=null) {
                criteria.add(Restrictions.ilike("namaWilayah", "%" + (String) mapCriteria.get("nama_wilayah") + "%"));
            }

        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));

        // Order by
        criteria.addOrder(Order.asc("kodeApp"));

        List<ImApbAppEntity> results = criteria.list();

        return results;
    }

    // Generate surrogate id from postgre
    public String getNextAppId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_apb_app')");
        Iterator<BigInteger> iter=query.list().iterator();
        return String.valueOf(iter.next().longValue());
    }

    public List<ImApbAppEntity> getListApp(String term) throws HibernateException {

        List<ImApbAppEntity> results = this.sessionFactory.getCurrentSession().createCriteria(ImApbAppEntity.class)
                .add(Restrictions.ilike("namaWilayah",term))
                .add(Restrictions.eq("flag", "Y"))
                .addOrder(Order.asc("kodeApp"))
//                .addOrder(Order.asc("primaryKey.id"))
                .list();
        return results;
    }
}
