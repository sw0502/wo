package com.neurix.pmsapb.master.app.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.app.model.App;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 11/09/17
 * Time: 0:07
 * To change this template use File | Settings | File Templates.
 */
public interface AppBo extends BaseMasterBo<App>{
    void saveDelete(App bean) throws GeneralBOException;
    public List<App> getComboAppWithCriteria(String query) throws GeneralBOException;

}
