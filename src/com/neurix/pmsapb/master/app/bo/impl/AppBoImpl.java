package com.neurix.pmsapb.master.app.bo.impl;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.app.bo.AppBo;
import com.neurix.pmsapb.master.app.dao.AppDao;
import com.neurix.pmsapb.master.app.model.App;
import com.neurix.pmsapb.master.app.model.ImApbAppEntity;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 11/09/17
 * Time: 0:08
 * To change this template use File | Settings | File Templates.
 */
public class AppBoImpl implements AppBo{

    protected static Logger logger = Logger.getLogger(AppBoImpl.class);
    private AppDao appDao;

    public void setAppDao(AppDao appDao) {
        this.appDao = appDao;
    }

    public AppDao getAppDao() {
        return appDao;
    }

    @Override
    public void saveDelete(App bean) throws GeneralBOException {
        logger.info("[AppBoImpl.saveDelete] start process >>>");

        if (bean!=null) {

            String appId = bean.getKodeApp();

            ImApbAppEntity imApbAppEntity = null;

            try {
                // Get data from database by ID
                imApbAppEntity = appDao.getById("kodeApp", appId);
            } catch (HibernateException e) {
                logger.error("[AppBoImpl.saveDelete] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data app by Kode app, please inform to your admin...," + e.getMessage());
            }

            if (imApbAppEntity != null) {

                // Modify from bean to entity serializable
                imApbAppEntity.setKodeApp(bean.getKodeApp());
                imApbAppEntity.setNamaWilayah(bean.getNamaWilayah());
                imApbAppEntity.setFlag(bean.getFlag());
                imApbAppEntity.setAction(bean.getAction());
                imApbAppEntity.setLastUpdateWho(bean.getLastUpdateWho());
                imApbAppEntity.setLastUpdate(bean.getLastUpdate());

                try {
                    // Delete (Edit) into database
                    appDao.updateAndSave(imApbAppEntity);
                } catch (HibernateException e) {
                    logger.error("[AppBoImpl.saveDelete] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving update data app, please info to your admin..." + e.getMessage());
                }


            } else {
                logger.error("[AppBoImpl.saveDelete] Error, not found data app with request id, please check again your data ...");
                throw new GeneralBOException("Error, not found data app with request id, please check again your data ...");

            }
        }
        logger.info("[AppBoImpl.saveDelete] end process <<<");
    }

    @Override
    public List<App> getComboAppWithCriteria(String query) throws GeneralBOException {
        logger.info("[UserBoImpl.getComboUserWithCriteria] start process >>>");

        List<App> listComboApp = new ArrayList();
        String criteria = "%" + query + "%";

        List<ImApbAppEntity> listApp = null;
        try {
            listApp = appDao.getListApp(criteria);
        } catch (HibernateException e) {
            logger.error("[UserBoImpl.getComboUserWithCriteria] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when retieving list user with criteria, please info to your admin..." + e.getMessage());
        }

        if (listApp != null) {
            for (ImApbAppEntity imApbAppEntity : listApp) {
                App itemComboApp = new App();
                itemComboApp.setKodeApp(imApbAppEntity.getKodeApp());
                itemComboApp.setNamaWilayah(imApbAppEntity.getNamaWilayah());
                listComboApp.add(itemComboApp);
            }
        }
        logger.info("[UserBoImpl.getComboUserWithCriteria] end process <<<");
        return listComboApp;
    }

    @Override
    public void saveEdit(App bean) throws GeneralBOException {
        logger.info("[AppBoImpl.saveEdit] start process >>>");

        if (bean!=null) {

            String appId = bean.getKodeApp();

            ImApbAppEntity imApbAppEntity = null;

            try {
                // Get data from database by ID
                imApbAppEntity = appDao.getById("kodeApp", appId);
            } catch (HibernateException e) {
                logger.error("[AppBoImpl.saveEdit] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data app by kode, please inform to your admin...," + e.getMessage());
            }

            if (imApbAppEntity != null) {
                //
                imApbAppEntity.setKodeApp(bean.getKodeApp());
                imApbAppEntity.setNamaWilayah(bean.getNamaWilayah());
                imApbAppEntity.setFlag(bean.getFlag());
                imApbAppEntity.setAction(bean.getAction());
                imApbAppEntity.setLastUpdateWho(bean.getLastUpdateWho());
                imApbAppEntity.setLastUpdate(bean.getLastUpdate());

                try {
                    // Update into database
                    appDao.updateAndSave(imApbAppEntity);
                } catch (HibernateException e) {
                    logger.error("[AppBoImpl.saveEdit] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving update data app, please info to your admin..." + e.getMessage());
                }
            } else {
                logger.error("[AppBoImpl.saveEdit] Error, not found data app with request id, please check again your data ...");
                throw new GeneralBOException("Error, not found data app with request id, please check again your data ...");

            }
        }
        logger.info("[AppBoImpl.saveEdit] end process <<<");
    }

    @Override
    public App saveAdd(App bean) throws GeneralBOException {
        logger.info("[AppBoImpl.saveAdd] start process >>>");

        if (bean!=null) {

//            String appId;
//            try {
//                // Generating ID, get from postgre sequence
//                appId = appDao.getNextAppId();
//            } catch (HibernateException e) {
//                logger.error("[AppBoImpl.saveAdd] Error, " + e.getMessage());
//                throw new GeneralBOException("Found problem when getting sequence app id, please info to your admin..." + e.getMessage());
//            }

            // creating object entity serializable
            ImApbAppEntity imApbAppEntity = new ImApbAppEntity();

            imApbAppEntity.setKodeApp(bean.getKodeApp());
            imApbAppEntity.setNamaWilayah(bean.getNamaWilayah());
            imApbAppEntity.setFlag(bean.getFlag());
            imApbAppEntity.setAction(bean.getAction());
            imApbAppEntity.setCreatedWho(bean.getCreatedWho());
            imApbAppEntity.setLastUpdateWho(bean.getLastUpdateWho());
            imApbAppEntity.setCreatedDate(bean.getCreatedDate());
            imApbAppEntity.setLastUpdate(bean.getLastUpdate());

            try {
                // insert into database
                appDao.addAndSave(imApbAppEntity);
            } catch (HibernateException e) {
                logger.error("[AppBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data app, please info to your admin..." + e.getMessage());
            }
        }

        logger.info("[AppBoImpl.saveAdd] end process <<<");
        return null;
    }

    @Override
    public List<App> getByCriteria(App searchBean) throws GeneralBOException {
        logger.info("[AppBoImpl.getByCriteria] start process >>>");

        // Mapping with collection and put
        List<App> listOfResult = new ArrayList();

        if (searchBean != null) {
            Map hsCriteria = new HashMap();

            if (searchBean.getKodeApp() != null && !"".equalsIgnoreCase(searchBean.getKodeApp())) {
                hsCriteria.put("kode_app", searchBean.getKodeApp());
            }
            if (searchBean.getNamaWilayah() != null && !"".equalsIgnoreCase(searchBean.getNamaWilayah())) {
                hsCriteria.put("nama_wilayah", searchBean.getNamaWilayah());
            }
            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }


            List<ImApbAppEntity> imApbAppEntities = null;
            try {

                imApbAppEntities = appDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AppBoImpl.getSearchAppByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if(imApbAppEntities != null){
                App returnApp;
                // Looping from dao to object and save in collection
                for(ImApbAppEntity appEntity : imApbAppEntities){
                    returnApp = new App();
                    returnApp.setKodeApp(appEntity.getKodeApp());
                    returnApp.setNamaWilayah(appEntity.getNamaWilayah());
                    returnApp.setAction(appEntity.getAction());
                    returnApp.setFlag(appEntity.getFlag());
                    listOfResult.add(returnApp);
                }
            }
        }
        logger.info("[AppBoImpl.getByCriteria] end process <<<");

        return listOfResult;
    }

    @Override
    public List<App> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }
}
