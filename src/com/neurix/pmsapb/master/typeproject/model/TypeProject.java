package com.neurix.pmsapb.master.typeproject.model;

import com.neurix.common.model.BaseModel;

import java.sql.Timestamp;

/**
 * Created by thinkpad on 22/08/2017.
 */
public class TypeProject extends BaseModel {
    private String idTypeProject;
    private String namaTypeProject;
    private Timestamp lastUpdate;
    private String lastUpdateWho;
    private String flag;
    private String action;

    public String getIdTypeProject() {
        return idTypeProject;
    }

    public void setIdTypeProject(String idTypeProject) {
        this.idTypeProject = idTypeProject;
    }

    public String getNamaTypeProject() {
        return namaTypeProject;
    }

    public void setNamaTypeProject(String namaTypeProject) {
        this.namaTypeProject = namaTypeProject;
    }

    @Override
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    @Override
    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }
}
