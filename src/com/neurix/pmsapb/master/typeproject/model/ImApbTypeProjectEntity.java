package com.neurix.pmsapb.master.typeproject.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 22/08/2017.
 */
public class ImApbTypeProjectEntity implements Serializable {

    private String idTypeProject;
    private String namaTypeProject;
    private Timestamp lastUpdate;
    private String lastUpdateWho;
    private String flag;
    private String action;

    public String getIdTypeProject() {
        return idTypeProject;
    }

    public void setIdTypeProject(String idTypeProject) {
        this.idTypeProject = idTypeProject;
    }

    public String getNamaTypeProject() {
        return namaTypeProject;
    }

    public void setNamaTypeProject(String namaTypeProject) {
        this.namaTypeProject = namaTypeProject;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
