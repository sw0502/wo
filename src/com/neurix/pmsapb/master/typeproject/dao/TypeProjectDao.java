package com.neurix.pmsapb.master.typeproject.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.typeproject.model.ImApbTypeProjectEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 22/08/2017.
 */
public class TypeProjectDao extends GenericDao<ImApbTypeProjectEntity,String> {

    @Override
    protected Class<ImApbTypeProjectEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ImApbTypeProjectEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImApbTypeProjectEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("type_project_id")!=null) {
                criteria.add(Restrictions.eq("idTypeProject", (String) mapCriteria.get("type_project_id")));
            }
            if (mapCriteria.get("type_project_name")!=null) {
                criteria.add(Restrictions.ilike("namaTypeProject", "%" + (String)mapCriteria.get("type_project_name") + "%"));
            }
        }
        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.asc("idTypeProject"));
        List<ImApbTypeProjectEntity> results = criteria.list();

        return results;
    }
}
