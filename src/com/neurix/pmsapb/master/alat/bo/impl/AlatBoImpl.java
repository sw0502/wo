package com.neurix.pmsapb.master.alat.bo.impl;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.alat.bo.AlatBo;
import com.neurix.pmsapb.master.alat.dao.AlatDao;
import com.neurix.pmsapb.master.alat.dao.AlatFeatureDao;
import com.neurix.pmsapb.master.alat.model.Alat;
import com.neurix.pmsapb.master.alat.model.AlatFeature;
import com.neurix.pmsapb.master.alat.model.ImApbAlatEntity;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 06/09/17
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */
public class AlatBoImpl implements AlatBo {

    protected static transient Logger logger = Logger.getLogger(AlatBoImpl.class);
    private AlatDao alatDao;
    private AlatFeatureDao alatFeatureDao;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        AlatBoImpl.logger = logger;
    }

    public AlatFeatureDao getAlatFeatureDao() {
        return alatFeatureDao;
    }

    public void setAlatFeatureDao(AlatFeatureDao alatFeatureDao) {
        this.alatFeatureDao = alatFeatureDao;
    }

    public void setAlatDao(AlatDao alatDao) {
        this.alatDao = alatDao;
    }

    public AlatDao getAlatDao() {
        return alatDao;
    }

    @Override
    public void saveDelete(Alat bean) throws GeneralBOException {
        logger.info("[saveDelete.saveDelete] start process >>>");

        if (bean!=null) {

            String alatId = bean.getKodeAlat();

            ImApbAlatEntity imApbAlatEntity = null;

            try {
                // Get data from database by ID
                imApbAlatEntity = alatDao.getById("kodeAlat", alatId);
            } catch (HibernateException e) {
                logger.error("[AlatBoImpl.saveDelete] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data ActivityAlatController by Kode ActivityAlatController, please inform to your admin...," + e.getMessage());
            }

            if (imApbAlatEntity != null) {

                // Modify from bean to entity serializable
                imApbAlatEntity.setKodeAlat(bean.getKodeAlat());
                imApbAlatEntity.setNamaAlat(bean.getNamaAlat());
                imApbAlatEntity.setKeterangan(bean.getKeterangan());
                imApbAlatEntity.setFlag(bean.getFlag());
                imApbAlatEntity.setAction(bean.getAction());
                imApbAlatEntity.setIdSubBidang(bean.getIdSubBidang());
                imApbAlatEntity.setNamaSubBidang(bean.getNamaSubBidang());
                imApbAlatEntity.setLastUpdateWho(bean.getLastUpdateWho());
                imApbAlatEntity.setLastUpdate(bean.getLastUpdate());

                try {
                    // Delete (Edit) into database
                    alatDao.updateAndSave(imApbAlatEntity);
                } catch (HibernateException e) {
                    logger.error("[AlatBoImpl.saveDelete] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving update data ActivityAlatController, please info to your admin..." + e.getMessage());
                }


            } else {
                logger.error("[AlatBoImpl.saveDelete] Error, not found data ActivityAlatController with request id, please check again your data ...");
                throw new GeneralBOException("Error, not found data ActivityAlatController with request id, please check again your data ...");

            }
        }
        logger.info("[AlatBoImpl.saveDelete] end process <<<");
    }

    @Override
    public void saveEdit(Alat bean) throws GeneralBOException {
        logger.info("[AlatBoImpl.saveEdit] start process >>>");

        if (bean!=null) {

            String alatId = bean.getKodeAlat();

            ImApbAlatEntity imApbAlatEntity = null;

            try {
                // Get data from database by ID
                imApbAlatEntity = alatDao.getById("kodeAlat", alatId);
            } catch (HibernateException e) {
                logger.error("[AlatBoImpl.saveEdit] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data ActivityAlatController by Kode ActivityAlatController, please inform to your admin...," + e.getMessage());
            }

            if (imApbAlatEntity != null) {
                //
                imApbAlatEntity.setKodeAlat(bean.getKodeAlat());
                imApbAlatEntity.setNamaAlat(bean.getNamaAlat());
                imApbAlatEntity.setKeterangan(bean.getKeterangan());
                imApbAlatEntity.setFlag(bean.getFlag());
                imApbAlatEntity.setAction(bean.getAction());
                imApbAlatEntity.setIdSubBidang(bean.getIdSubBidang());
                imApbAlatEntity.setNamaSubBidang(bean.getNamaSubBidang());
                imApbAlatEntity.setLastUpdateWho(bean.getLastUpdateWho());
                imApbAlatEntity.setLastUpdate(bean.getLastUpdate());

                try {
                    // Update into database
                    alatDao.updateAndSave(imApbAlatEntity);
                } catch (HibernateException e) {
                    logger.error("[AlatBoImpl.saveEdit] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving update data ActivityAlatController, please info to your admin..." + e.getMessage());
                }
            } else {
                logger.error("[AlatBoImpl.saveEdit] Error, not found data ActivityAlatController with request id, please check again your data ...");
                throw new GeneralBOException("Error, not found data ActivityAlatController with request id, please check again your data ...");

            }
        }
        logger.info("[AlatBoImpl.saveEdit] end process <<<");
    }

    @Override
    public Alat saveAdd(Alat bean) throws GeneralBOException {
        logger.info("[AlatBoImpl.saveAdd] start process >>>");

        if (bean!=null) {

            /*
            String alatId;
            try {
                // Generating ID, get from postgre sequence
                alatId = alatDao.getNextAlatId();
            } catch (HibernateException e) {
                logger.error("[AlatBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when getting sequence ActivityAlatController id, please info to your admin..." + e.getMessage());
            }
            */

            // creating object entity serializable
            ImApbAlatEntity imApbAlatEntity = new ImApbAlatEntity();

            imApbAlatEntity.setKodeAlat(bean.getKodeAlat());
            imApbAlatEntity.setNamaAlat(bean.getNamaAlat());
            imApbAlatEntity.setKeterangan(bean.getKeterangan());
            imApbAlatEntity.setFlag(bean.getFlag());
            imApbAlatEntity.setAction(bean.getAction());
            imApbAlatEntity.setIdSubBidang(bean.getIdSubBidang());
            imApbAlatEntity.setNamaSubBidang(bean.getNamaSubBidang());
            imApbAlatEntity.setCreatedWho(bean.getCreatedWho());
            imApbAlatEntity.setLastUpdateWho(bean.getLastUpdateWho());
            imApbAlatEntity.setCreatedDate(bean.getCreatedDate());
            imApbAlatEntity.setLastUpdate(bean.getLastUpdate());

            try {
                // insert into database
                alatDao.addAndSave(imApbAlatEntity);
            } catch (HibernateException e) {
                logger.error("[AlatBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data ActivityAlatController, please info to your admin..." + e.getMessage());
            }
        }

        logger.info("[AlatBoImpl.saveAdd] end process <<<");
        return null;
    }

    @Override
    public List<Alat> getByCriteria(Alat searchBean) throws GeneralBOException {
        logger.info("[AlatBoImpl.getByCriteria] start process >>>");

        // Mapping with collection and put
        List<Alat> listOfResult = new ArrayList();

        if (searchBean != null) {
            Map hsCriteria = new HashMap();

            if (searchBean.getKodeAlat() != null && !"".equalsIgnoreCase(searchBean.getKodeAlat())) {
                hsCriteria.put("kode_alat", searchBean.getKodeAlat());
            }
            if (searchBean.getNamaAlat() != null && !"".equalsIgnoreCase(searchBean.getNamaAlat())) {
                hsCriteria.put("nama_alat", searchBean.getNamaAlat());
            }
            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }


            List<ImApbAlatEntity> imApbAlatEntities = null;
            try {

                imApbAlatEntities = alatDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[AlatBoImpl.getSearchAlatByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if(imApbAlatEntities != null){
                Alat returnAlat;
                // Looping from dao to object and save in collection
                for(ImApbAlatEntity alatEntity : imApbAlatEntities){
                    returnAlat = new Alat();
                    returnAlat.setNamaAlat(alatEntity.getNamaAlat());
                    returnAlat.setKodeAlat(alatEntity.getKodeAlat());
                    returnAlat.setKeterangan(alatEntity.getKeterangan());
                    returnAlat.setAction(alatEntity.getAction());
                    returnAlat.setIdSubBidang(alatEntity.getIdSubBidang());
                    returnAlat.setNamaSubBidang(alatEntity.getNamaSubBidang());
                    returnAlat.setFlag(alatEntity.getFlag());
//                    Map criteria = new HashMap();
                    listOfResult.add(returnAlat);
                }
            }
        }
        logger.info("[AlatBoImpl.getByCriteria] end process <<<");

        return listOfResult;
    }

    @Override
    public List<Alat> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }

    public List<Alat> getComboAlatWithCriteria(String query) throws GeneralBOException {
        logger.info("[UserBoImpl.getComboUserWithCriteria] start process >>>");

        List<Alat> listComboAlat = new ArrayList();
        String criteria = "%" + query + "%";

        List<ImApbAlatEntity> listAlat = null;
        try {
            listAlat = alatDao.getListAlat(criteria);
        } catch (HibernateException e) {
            logger.error("[UserBoImpl.getComboUserWithCriteria] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when retieving list user with criteria, please info to your admin..." + e.getMessage());
        }

        if (listAlat != null) {
            for (ImApbAlatEntity imApbAlatEntity : listAlat) {
                Alat itemComboAlat = new Alat();
                itemComboAlat.setKodeAlat(imApbAlatEntity.getKodeAlat());
                itemComboAlat.setNamaAlat(imApbAlatEntity.getNamaAlat());
                itemComboAlat.setKeterangan(imApbAlatEntity.getKeterangan());

//                AlatFeature alatFeature = new AlatFeature();
//                alatFeature.setKodeAlat(imApbAlatEntity.getKodeAlat());
//                List<AlatFeature> listOfAlatFeature = null;
//                listOfAlatFeature = alatFeatureDao.getDataAlatFeature(alatFeature);
//                if(listOfAlatFeature != null){
//                    for (AlatFeature alatFeatureData : listOfAlatFeature){
//                        if (alatFeatureData.getKodeAlat().equalsIgnoreCase(imApbAlatEntity.getKodeAlat())){
//                            itemComboAlat.setEnDaya(alatFeatureData.getDaya());
//                            itemComboAlat.setEnTransmisi(alatFeatureData.getTransmisi());
//                            itemComboAlat.setEnJenisTp(alatFeatureData.getJenisTp());
//                            itemComboAlat.setEnTegOprs(alatFeatureData.getTegOprs());
//                            itemComboAlat.setEnFrekuensiTx(alatFeatureData.getFrekuensiTx());
//                            itemComboAlat.setEnFrekuensiRx(alatFeatureData.getFrekuensiRx());
//                        }
//                    }
//                }
                listComboAlat.add(itemComboAlat);
            }
        }
        logger.info("[UserBoImpl.getComboUserWithCriteria] end process <<<");
        return listComboAlat;
    }
}
