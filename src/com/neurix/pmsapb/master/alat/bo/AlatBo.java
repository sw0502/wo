package com.neurix.pmsapb.master.alat.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.alat.model.Alat;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 06/09/17
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */
public interface AlatBo extends BaseMasterBo<Alat>{
    public void saveDelete(Alat bean) throws GeneralBOException;
    public List<Alat> getComboAlatWithCriteria(String query) throws GeneralBOException;
}
