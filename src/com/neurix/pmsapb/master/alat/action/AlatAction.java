package com.neurix.pmsapb.master.alat.action;

import com.neurix.common.action.BaseMasterAction;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.master.alat.bo.AlatBo;
import com.neurix.pmsapb.master.alat.model.Alat;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 06/09/17
 * Time: 13:52
 * To change this template use File | Settings | File Templates.
 */
public class AlatAction extends BaseMasterAction{
    protected static transient Logger logger = Logger.getLogger(AlatAction.class);
    private AlatBo alatBoProxy;
    private Alat alat;

    public AlatBo getAlatBoProxy() {
        return alatBoProxy;
    }

    public void setAlatBoProxy(AlatBo alatBoProxy) {
        this.alatBoProxy = alatBoProxy;
    }

    public Alat getAlat() {
        return alat;
    }

    public void setAlat(Alat alat) {
        this.alat = alat;
    }

    public Alat init(String kode, String flag){
        logger.info("[AlatAction.init] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Alat> listOfResult = (List<Alat>) session.getAttribute("listOfResult");

        if(kode != null && !"".equalsIgnoreCase(kode)){
            if(listOfResult != null){
                for (Alat alat: listOfResult) {
                    if(kode.equalsIgnoreCase(alat.getKodeAlat()) && flag.equalsIgnoreCase(alat.getFlag())){
                        setAlat(alat);
                        break;
                    }
                }
            } else {
                setAlat(new Alat());
            }

            logger.info("[AlatAction.init] end process >>>");
        }
        return getAlat();
    }

    @Override
    public String add() {
        logger.info("[AlatAction.add] start process >>>");
        Alat addAlat = new Alat();
        setAlat(addAlat);

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[AlatAction.add] stop process >>>");
        return "init_add";
    }

    @Override
    public String edit() {
        logger.info("[AlatAction.edit] start process >>>");
        String itemId = getId();
        String itemFlag = getFlag();

        Alat editAlat = new Alat();

        if(itemFlag != null){
            try {
                editAlat = init(itemId, itemFlag);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatBoProxy.saveErrorMessage(e.getMessage(), "AlatBO.getAlatByCriteria");
                } catch (GeneralBOException e1) {
                    logger.error("[AlatAction.edit] Error when retrieving edit data,", e1);
                }
                logger.error("[AlatAction.edit] Error when retrieving item," + "[" + logId + "] Found problem when retrieving data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when retrieving data for edit, please inform to your admin.");
                return "failure";
            }

            if(editAlat != null) {
                setAlat(editAlat);
            } else {
                editAlat.setFlag(itemFlag);
                editAlat.setKodeAlat(itemId);
                setAlat(editAlat);
                addActionError("Error, Unable to find data with id = " + itemId);
                return "failure";
            }
        } else {
            editAlat.setKodeAlat(itemId);
            editAlat.setFlag(getFlag());
            setAlat(editAlat);
            addActionError("Error, Unable to edit again with flag = N.");
            return "failure";
        }

        logger.info("[AlatAction.edit] end process >>>");
        return "init_edit";
    }

    @Override
    public String delete() {
        logger.info("[AlatAction.delete] start process >>>");

        String itemId = getId();
        String itemFlag = getFlag();
        Alat deleteAlat = new Alat();

        if (itemFlag != null ) {

            try {
                deleteAlat = init(itemId, itemFlag);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = alatBoProxy.saveErrorMessage(e.getMessage(), "AlatBO.getAlatById");
                } catch (GeneralBOException e1) {
                    logger.error("[AlatAction.delete] Error when retrieving delete data,", e1);
                }
                logger.error("[AlatAction.delete] Error when retrieving item," + "[" + logId + "] Found problem when retrieving data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when retrieving data for delete, please inform to your admin.");
                return "failure";
            }

            if (deleteAlat != null) {
                setAlat(deleteAlat);

            } else {
                deleteAlat.setKodeAlat(itemId);
                deleteAlat.setFlag(itemFlag);
                setAlat(deleteAlat);
                addActionError("Error, Unable to find data with id = " + itemId);
                return "failure";
            }
        } else {
            deleteAlat.setKodeAlat(itemId);
            deleteAlat.setFlag(itemFlag);
            setAlat(deleteAlat);
            addActionError("Error, Unable to delete again with flag = N.");
            return "failure";
        }

        logger.info("[AlatAction.delete] end process <<<");

        return "init_delete";
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        return null;
    }

    public String saveEdit(){
        logger.info("[AlatAction.saveEdit] start process >>>");
        try {

            Alat editAlat = getAlat();

            String userLogin = CommonUtil.userLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            editAlat.setLastUpdateWho(userLogin);
            editAlat.setLastUpdate(updateTime);
            editAlat.setAction("U");

            alatBoProxy.saveEdit(editAlat);

        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatBoProxy.saveErrorMessage(e.getMessage(), "AlatBO.saveEdit");
            } catch (GeneralBOException e1) {
                logger.error("[AlatAction.saveEdit] Error when saving error,", e1);
            }
            logger.error("[AlatAction.saveEdit] Error when editing item ActivityAlatController," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving edit data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        logger.info("[AlatAction.saveEdit] end process <<<");

        return "success_save_edit";
    }

    public String saveDelete(){
        logger.info("[AlatAction.saveDelete] start process >>>");
        try {

            Alat deleteAlat = getAlat();

            String userLogin = CommonUtil.userLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            deleteAlat.setLastUpdate(updateTime);
            deleteAlat.setLastUpdateWho(userLogin);
            deleteAlat.setAction("U");
            deleteAlat.setFlag("N");

            alatBoProxy.saveDelete(deleteAlat);

        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatBoProxy.saveErrorMessage(e.getMessage(), "AlatBO.saveDelete");
            } catch (GeneralBOException e1) {
                logger.error("[AlatAction.saveDelete] Error when saving error,", e1);
            }
            logger.error("[AlatAction.saveDelete] Error when editing item ActivityAlatController," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving edit data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        logger.info("[AlatAction.saveDelete] end process <<<");

        return "success_save_delete";
    }

    public String saveAdd(){
        logger.info("[AlatAction.saveAdd] start process >>>");

        try {
            Alat alat = getAlat();
            String userLogin = CommonUtil.userLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            alat.setCreatedWho(userLogin);
            alat.setLastUpdate(updateTime);
            alat.setCreatedDate(updateTime);
            alat.setLastUpdateWho(userLogin);
            alat.setAction("C");
            alat.setFlag("Y");

            alatBoProxy.saveAdd(alat);
        }catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatBoProxy.saveErrorMessage(e.getMessage(), "AlatBO.saveAdd");
            } catch (GeneralBOException e1) {
                logger.error("[AlatAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatAction.saveAdd] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[AlatAction.saveAdd] end process >>>");
        return "success_save_add";
    }

    @Override
    public String search() {
        logger.info("[AlatAction.search] start process >>>");

        Alat searchAlat = getAlat();
        List<Alat> listOfSearchAlat = new ArrayList();

        try {
            listOfSearchAlat = alatBoProxy.getByCriteria(searchAlat);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = alatBoProxy.saveErrorMessage(e.getMessage(), "AlatBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[AlatAction.search] Error when saving error,", e1);
            }
            logger.error("[AlatAction.save] Error when searching ActivityAlatController by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin" );
            return "failure";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfSearchAlat);

        logger.info("[AlatAction.search] end process <<<");

        return SUCCESS;
    }

    @Override
    public String initForm() {
        logger.info("[AlatAction.initForm] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        logger.info("[AlatAction.initForm] end process >>>");
        return INPUT;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }
}
