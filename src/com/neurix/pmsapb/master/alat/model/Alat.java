package com.neurix.pmsapb.master.alat.model;

import com.neurix.common.model.BaseModel;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 06/09/17
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
public class Alat extends BaseModel{
    private String kodeAlat;
    private String namaAlat;
    private String keterangan;
    private String idSubBidang;
    private String namaSubBidang;

    private String enJenisTp;
    private String enFrekuensiTx;
    private String enFrekuensiRx;
    private String enDaya;
    private String enTransmisi;
    private String enTegOprs;

    public String getIdSubBidang() {
        return idSubBidang;
    }

    public String getNamaSubBidang() {
        return namaSubBidang;
    }

    public void setNamaSubBidang(String namaSubBidang) {
        this.namaSubBidang = namaSubBidang;
    }

    public void setIdSubBidang(String idSubBidang) {
        this.idSubBidang = idSubBidang;
    }

    public String getEnJenisTp() {
        return enJenisTp;
    }

    public void setEnJenisTp(String enJenisTp) {
        this.enJenisTp = enJenisTp;
    }

    public String getEnFrekuensiTx() {
        return enFrekuensiTx;
    }

    public void setEnFrekuensiTx(String enFrekuensiTx) {
        this.enFrekuensiTx = enFrekuensiTx;
    }

    public String getEnFrekuensiRx() {
        return enFrekuensiRx;
    }

    public void setEnFrekuensiRx(String enFrekuensiRx) {
        this.enFrekuensiRx = enFrekuensiRx;
    }

    public String getEnDaya() {
        return enDaya;
    }

    public void setEnDaya(String enDaya) {
        this.enDaya = enDaya;
    }

    public String getEnTransmisi() {
        return enTransmisi;
    }

    public void setEnTransmisi(String enTransmisi) {
        this.enTransmisi = enTransmisi;
    }

    public String getEnTegOprs() {
        return enTegOprs;
    }

    public void setEnTegOprs(String enTegOprs) {
        this.enTegOprs = enTegOprs;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setNamaAlat(String namaAlat) {
        this.namaAlat = namaAlat;
    }

    public String getNamaAlat() {
        return namaAlat;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
