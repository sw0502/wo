package com.neurix.pmsapb.master.alat.model;

import com.neurix.common.model.BaseModel;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 06/09/17
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
public class AlatFeature extends BaseModel{
    private String idAlatFeature;
    private String kodeAlat;
    private String daya;
    private String transmisi;
    private String frekuensiRx;
    private String frekuensiTx;
    private String tegOprs;
    private String jenisTp;

    public String getJenisTp() {
        return jenisTp;
    }

    public void setJenisTp(String jenisTp) {
        this.jenisTp = jenisTp;
    }

    public String getIdAlatFeature() {
        return idAlatFeature;
    }

    public void setIdAlatFeature(String idAlatFeature) {
        this.idAlatFeature = idAlatFeature;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getDaya() {
        return daya;
    }

    public void setDaya(String daya) {
        this.daya = daya;
    }

    public String getTransmisi() {
        return transmisi;
    }

    public void setTransmisi(String transmisi) {
        this.transmisi = transmisi;
    }

    public String getFrekuensiRx() {
        return frekuensiRx;
    }

    public void setFrekuensiRx(String frekuensiRx) {
        this.frekuensiRx = frekuensiRx;
    }

    public String getFrekuensiTx() {
        return frekuensiTx;
    }

    public void setFrekuensiTx(String frekuensiTx) {
        this.frekuensiTx = frekuensiTx;
    }

    public String getTegOprs() {
        return tegOprs;
    }

    public void setTegOprs(String tegOprs) {
        this.tegOprs = tegOprs;
    }
}
