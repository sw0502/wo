package com.neurix.pmsapb.master.alat.model;

import com.neurix.pmsapb.project.action.ProjectAction;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 06/09/17
 * Time: 14:05
 * To change this template use File | Settings | File Templates.
 */
public class ImApbAlatEntity implements Serializable {
//    private ImApbAlatPK primaryKey;
    private String kodeAlat;
    private String namaAlat;
    private String keterangan;
    private String flag;
    private String action;
    private String idSubBidang;
    private String namaSubBidang;
    private Timestamp createdDate;
    private Timestamp lastUpdate;
    private String createdWho;
    private String lastUpdateWho;

//    public ImApbAlatPK getPrimaryKey() {
//        return primaryKey;
//    }
//
//    public void setPrimaryKey(ImApbAlatPK primaryKey) {
//        this.primaryKey = primaryKey;
//    }


    public String getNamaSubBidang() {
        return namaSubBidang;
    }

    public void setNamaSubBidang(String namaSubBidang) {
        this.namaSubBidang = namaSubBidang;
    }

    public String getIdSubBidang() {
        return idSubBidang;
    }

    public void setIdSubBidang(String idSubBidang) {
        this.idSubBidang = idSubBidang;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setNamaAlat(String namaAlat) {
        this.namaAlat = namaAlat;
    }

    public String getNamaAlat() {
        return namaAlat;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getCreatedWho() {
        return createdWho;
    }

    public void setCreatedWho(String createdWho) {
        this.createdWho = createdWho;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }
}
