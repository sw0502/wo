package com.neurix.pmsapb.master.alat.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 06/09/17
 * Time: 14:05
 * To change this template use File | Settings | File Templates.
 */
public class ImApbAlatFeatureEntity implements Serializable {
    private String idAlatFeature;
    private String kodeAlat;
    private String daya;
    private String transmisi;
    private String frekuensiRx;
    private String frekuensiTx;
    private String tegOprs;
    private String jenisTp;

    public String getJenisTp() {
        return jenisTp;
    }

    public void setJenisTp(String jenisTp) {
        this.jenisTp = jenisTp;
    }

    public String getIdAlatFeature() {
        return idAlatFeature;
    }

    public void setIdAlatFeature(String idAlatFeature) {
        this.idAlatFeature = idAlatFeature;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getDaya() {
        return daya;
    }

    public void setDaya(String daya) {
        this.daya = daya;
    }

    public String getTransmisi() {
        return transmisi;
    }

    public void setTransmisi(String transmisi) {
        this.transmisi = transmisi;
    }

    public String getFrekuensiRx() {
        return frekuensiRx;
    }

    public void setFrekuensiRx(String frekuensiRx) {
        this.frekuensiRx = frekuensiRx;
    }

    public String getFrekuensiTx() {
        return frekuensiTx;
    }

    public void setFrekuensiTx(String frekuensiTx) {
        this.frekuensiTx = frekuensiTx;
    }

    public String getTegOprs() {
        return tegOprs;
    }

    public void setTegOprs(String tegOprs) {
        this.tegOprs = tegOprs;
    }
}
