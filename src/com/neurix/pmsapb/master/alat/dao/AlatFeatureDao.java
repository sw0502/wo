package com.neurix.pmsapb.master.alat.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.alat.model.AlatFeature;
import com.neurix.pmsapb.master.alat.model.ImApbAlatEntity;
import com.neurix.pmsapb.master.alat.model.ImApbAlatFeatureEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 06/09/17
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
public class AlatFeatureDao extends GenericDao<ImApbAlatFeatureEntity, String> {

    @Override
    protected Class<ImApbAlatFeatureEntity> getEntityClass() {
        return ImApbAlatFeatureEntity.class;
    }

    @Override
    public List<ImApbAlatFeatureEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImApbAlatFeatureEntity.class);

        // Get Collection and sorting
        if (mapCriteria!=null) {
            if (mapCriteria.get("id_alat_feature")!=null) {
                criteria.add(Restrictions.eq("idAlatFeature", (String) mapCriteria.get("id_alat_feature")));
            }
            if (mapCriteria.get("kode_alat")!=null) {
                criteria.add(Restrictions.ilike("kodeAlat", "%" + (String)mapCriteria.get("kode_alat") + "%"));
            }
        }
        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        // Order by
        criteria.addOrder(Order.asc("idAlatFeature"));
        List<ImApbAlatFeatureEntity> results = criteria.list();
        return results;
    }

    // Generate surrogate id from postgre
    public String getNextAlatId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_apb_alat')");
        Iterator<BigInteger> iter=query.list().iterator();
        return String.valueOf(iter.next().longValue());
    }

    public List<ImApbAlatEntity> getListAlat(String term) throws HibernateException {

        List<ImApbAlatEntity> results = this.sessionFactory.getCurrentSession().createCriteria(ImApbAlatEntity.class)
                .add(Restrictions.ilike("namaAlat",term))
                .add(Restrictions.eq("flag", "Y"))
                .addOrder(Order.asc("kodeAlat"))
//                .addOrder(Order.asc("primaryKey.id"))
                .list();

        return results;
    }

    public List<AlatFeature> getDataAlatFeature(AlatFeature alatFeature) {
        List<AlatFeature> listOfAlatFeature = new ArrayList<AlatFeature>();
        List<Object[]> result = new ArrayList<Object[]>();

        if (alatFeature!= null){
            String kodeAlat = "";
            if (alatFeature.getKodeAlat()!= null && !"".equalsIgnoreCase(alatFeature.getKodeAlat())){
                kodeAlat = alatFeature.getKodeAlat();
            }else {
                kodeAlat = "%";
            }

            String SQL = "select * from im_apb_alat_feature where kode_alat = :kodeAlat";

            result = this.sessionFactory.getCurrentSession().createSQLQuery(SQL)
                    .setParameter("kodeAlat", kodeAlat)
                    .list();

            AlatFeature alatFeatureData;
            if (result.size() >= 1){
                for (Object[] obj: result){
                    alatFeatureData = new AlatFeature();
                    alatFeatureData.setIdAlatFeature((String) obj[0]);
                    alatFeatureData.setKodeAlat((String) obj[1]);
                    alatFeatureData.setDaya((String) obj[2]);
                    alatFeatureData.setTransmisi((String) obj[3]);
                    alatFeatureData.setFrekuensiTx((String) obj[4]);
                    alatFeatureData.setFrekuensiRx((String) obj[5]);
                    alatFeatureData.setTegOprs((String) obj[6]);
                    alatFeatureData.setJenisTp((String) obj[7]);
                    listOfAlatFeature.add(alatFeatureData);
                }
            }
        }
        return listOfAlatFeature;
    }
}
