package com.neurix.pmsapb.master.alat.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.alat.model.ImApbAlatEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 06/09/17
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
public class AlatDao extends GenericDao<ImApbAlatEntity, String> {

    @Override
    protected Class<ImApbAlatEntity> getEntityClass() {
        return ImApbAlatEntity.class;
    }

    @Override
    public List<ImApbAlatEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImApbAlatEntity.class);

        // Get Collection and sorting
        if (mapCriteria!=null) {
            if (mapCriteria.get("kode_alat")!=null) {
                criteria.add(Restrictions.eq("kodeAlat", (String) mapCriteria.get("kode_alat")));
            }
            if (mapCriteria.get("nama_alat")!=null) {
                criteria.add(Restrictions.ilike("namaAlat", "%" + (String)mapCriteria.get("nama_alat") + "%"));
            }

        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));

        // Order by
        criteria.addOrder(Order.asc("kodeAlat"));

        List<ImApbAlatEntity> results = criteria.list();

        return results;
    }

    // Generate surrogate id from postgre
    public String getNextAlatId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_apb_alat')");
        Iterator<BigInteger> iter=query.list().iterator();
        return String.valueOf(iter.next().longValue());
    }

    public List<ImApbAlatEntity> getListAlat(String term) throws HibernateException {

        List<ImApbAlatEntity> results = this.sessionFactory.getCurrentSession().createCriteria(ImApbAlatEntity.class)
                .add(Restrictions.ilike("namaAlat",term))
                .add(Restrictions.eq("flag", "Y"))
                .addOrder(Order.asc("kodeAlat"))
//                .addOrder(Order.asc("primaryKey.id"))
                .list();

        return results;
    }
}
