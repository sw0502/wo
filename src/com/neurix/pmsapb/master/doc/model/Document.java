package com.neurix.pmsapb.master.doc.model;

import com.neurix.common.model.BaseModel;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by thinkpad on 01/08/2017.
 */
public class Document extends BaseModel {
    private String idProject;
    private String idDoc;
    private String namaDoc;
    private String flag;
    private String action;
    private String type;
    private String idTask;
    private Timestamp lastUpdate;
    private String uploadBy;
    private String statusDocument;
    private String docType;
    private String downloadPath;

    private File fileUploadDoc;
    private String fileUploadDocContentType;
    private String fileUploadDocFileName;
    private String filePath;
    private boolean enabledDownload;
    private boolean enabledView;

    public boolean isEnabledView() {
        return enabledView;
    }

    public void setEnabledView(boolean enabledView) {
        this.enabledView = enabledView;
    }

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public boolean isEnabledDownload() {
        return enabledDownload;
    }

    public void setEnabledDownload(boolean enabledDownload) {
        this.enabledDownload = enabledDownload;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getStatusDocument() {
        return statusDocument;
    }

    public void setStatusDocument(String statusDocument) {
        this.statusDocument = statusDocument;
    }

    @Override
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getUploadBy() {
        return uploadBy;
    }

    public void setUploadBy(String uploadBy) {
        this.uploadBy = uploadBy;
    }

    private List<Document> listOfUploadDocument;

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(String idDoc) {
        this.idDoc = idDoc;
    }

    public String getNamaDoc() {
        return namaDoc;
    }

    public void setNamaDoc(String namaDoc) {
        this.namaDoc = namaDoc;
    }

    public List<Document> getListOfUploadDocument() {
        return listOfUploadDocument;
    }

    public void setListOfUploadDocument(List<Document> listOfUploadDocument) {
        this.listOfUploadDocument = listOfUploadDocument;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    public File getFileUploadDoc() {
        return fileUploadDoc;
    }

    public void setFileUploadDoc(File fileUploadDoc) {
        this.fileUploadDoc = fileUploadDoc;
    }

    public String getFileUploadDocContentType() {
        return fileUploadDocContentType;
    }

    public void setFileUploadDocContentType(String fileUploadDocContentType) {
        this.fileUploadDocContentType = fileUploadDocContentType;
    }

    public String getFileUploadDocFileName() {
        return fileUploadDocFileName;
    }

    public void setFileUploadDocFileName(String fileUploadDocFileName) {
        this.fileUploadDocFileName = fileUploadDocFileName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }
}
