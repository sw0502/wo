package com.neurix.pmsapb.master.doc.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 01/08/2017.
 */
public class ItApbProjectDocEntity implements Serializable {
    private String idProject;
    private String idDoc;
    private String namaDoc;
    private String flag;
    private String action;
    private String idTask;
    private Timestamp lastUpdate;
    private String uploadBy;
    private String docType;

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getUploadBy() {
        return uploadBy;
    }

    public void setUploadBy(String uploadBy) {
        this.uploadBy = uploadBy;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(String idDoc) {
        this.idDoc = idDoc;
    }

    public String getNamaDoc() {
        return namaDoc;
    }

    public void setNamaDoc(String namaDoc) {
        this.namaDoc = namaDoc;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIdTask() {
        return idTask;
    }

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }
}
