package com.neurix.pmsapb.master.doc.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.doc.model.ItApbProjectDocEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.Map;

/**
 * Created by thinkpad on 01/08/2017.
 */
public class DocDao extends GenericDao<ItApbProjectDocEntity,String> {
    @Override
    protected Class<ItApbProjectDocEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ItApbProjectDocEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectDocEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("project_id")!=null) {
                criteria.add(Restrictions.eq("idProject", (String) mapCriteria.get("project_id")));
            }
            if (mapCriteria.get("doc_id")!=null) {
                criteria.add(Restrictions.ilike("idDoc", "%" + (String)mapCriteria.get("doc_id") + "%"));
            }
        }
//        criteria.add(Restrictions.eq("idDoc", mapCriteria.get("doc_id")));
        criteria.addOrder(Order.asc("idDoc"));
        List<ItApbProjectDocEntity> results = criteria.list();

        return results;
    }

    public List<ItApbProjectDocEntity> getCriteriaById(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ItApbProjectDocEntity.class);
        if (mapCriteria!=null) {
//            if (mapCriteria.get("project_id")!=null) {
//                criteria.add(Restrictions.eq("idProject", (String) mapCriteria.get("project_id")));
//            }
            if (mapCriteria.get("doc_id")!=null) {
                criteria.add(Restrictions.eq("idDoc", (String)mapCriteria.get("doc_id")));
            }
        }
        criteria.add(Restrictions.eq("idDoc", mapCriteria.get("doc_id")));
        criteria.addOrder(Order.asc("idDoc"));
        List<ItApbProjectDocEntity> results = criteria.list();

        return results;
    }

    public String getNextDocId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_doc')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;
    }
}
