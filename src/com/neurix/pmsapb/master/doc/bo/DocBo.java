package com.neurix.pmsapb.master.doc.bo;

import com.neurix.common.bo.GeneralBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.model.BaseModel;

/**
 * Created by thinkpad on 01/08/2017.
 */
public interface DocBo extends GeneralBo {
    public String getSeqDocId() throws GeneralBOException;

}
