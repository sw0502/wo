package com.neurix.pmsapb.master.priority.action;

import com.neurix.common.action.BaseMasterAction;
import com.neurix.pmsapb.master.priority.bo.PriorityBo;
import com.neurix.pmsapb.master.priority.model.Priority;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class PriorityAction extends BaseMasterAction {
    private PriorityBo priorityBoProxy;
    private Priority priority;

    public PriorityBo getPriorityBoProxy() {
        return priorityBoProxy;
    }

    public void setPriorityBoProxy(PriorityBo priorityBoProxy) {
        this.priorityBoProxy = priorityBoProxy;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public String add() {
        return null;
    }

    @Override
    public String edit() {
        return null;
    }

    @Override
    public String delete() {
        return null;
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        return null;
    }

    @Override
    public String search() {
        return null;
    }

    @Override
    public String initForm() {
        return null;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }
}
