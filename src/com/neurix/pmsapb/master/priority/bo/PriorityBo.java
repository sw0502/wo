package com.neurix.pmsapb.master.priority.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.priority.model.Priority;

import java.util.List;

/**
 * Created by thinkpad on 23/07/2017.
 */
public interface PriorityBo extends BaseMasterBo {
    public List<Priority> getPriorityByCriteria (Priority searchPriority) throws GeneralBOException;

}
