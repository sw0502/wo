package com.neurix.pmsapb.master.priority.bo.impl;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.priority.bo.PriorityBo;
import com.neurix.pmsapb.master.priority.dao.PriorityDao;
import com.neurix.pmsapb.master.priority.model.ImApbPriorityEntity;
import com.neurix.pmsapb.master.priority.model.Priority;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class PriorityBoImpl implements PriorityBo {
    protected static transient Logger logger = Logger.getLogger(PriorityBoImpl.class);

    private PriorityDao priorityDao;

    public PriorityDao getPriorityDao() {
        return priorityDao;
    }

    public void setPriorityDao(PriorityDao priorityDao) {
        this.priorityDao = priorityDao;
    }

    @Override
    public void saveDelete(Object bean) throws GeneralBOException {

    }

    @Override
    public void saveEdit(Object bean) throws GeneralBOException {

    }

    @Override
    public Object saveAdd(Object bean) throws GeneralBOException {
        return null;
    }

    @Override
    public List getByCriteria(Object searchBean) throws GeneralBOException {
        return null;
    }

    @Override
    public List getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }

    @Override
    public List<Priority> getPriorityByCriteria(Priority searchPriority) throws GeneralBOException {
        List<Priority> resultOfResultPriorty = new ArrayList<Priority>();
        if(searchPriority != null){
            String flag = searchPriority.getFlag();
            Map hsCriteria = new HashMap();

            if (searchPriority.getFlag() != null && !"".equalsIgnoreCase(searchPriority.getFlag())) {
                if ("N".equalsIgnoreCase(searchPriority.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchPriority.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }
            List<ImApbPriorityEntity> listOfSPriority = null;
            try{
                listOfSPriority = priorityDao.getByCriteria(hsCriteria);
            }catch (HibernateException e){
                logger.error("[UserBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
            if(listOfSPriority != null){
                Priority resulPriority;
                for(ImApbPriorityEntity imApbPriority : listOfSPriority){
                    resulPriority = new Priority();
                    resulPriority.setIdPriority(imApbPriority.getIdPriority());
                    resulPriority.setNamaPriority(imApbPriority.getNamaPriority());
                    resultOfResultPriorty.add(resulPriority);
                }
            }

        }
        logger.info("[StatusBoImpl.getStatusByCriteria] start process >>>");
        return resultOfResultPriorty;
    }
}
