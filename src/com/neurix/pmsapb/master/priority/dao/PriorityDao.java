package com.neurix.pmsapb.master.priority.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.priority.model.ImApbPriorityEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class PriorityDao extends GenericDao<ImApbPriorityEntity,String> {
    @Override
    protected Class<ImApbPriorityEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ImApbPriorityEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImApbPriorityEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("priority_id")!=null) {
                criteria.add(Restrictions.eq("idPriority", (String) mapCriteria.get("priority_id")));
            }
            if (mapCriteria.get("priority_name")!=null) {
                criteria.add(Restrictions.ilike("namaPriority", "%" + (String)mapCriteria.get("priority_name") + "%"));
            }
        }
        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.asc("idPriority"));
        List<ImApbPriorityEntity> results = criteria.list();
        return results;
    }
}
