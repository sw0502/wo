package com.neurix.pmsapb.master.priority.model;

import java.io.Serializable;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class ImApbPriorityEntity implements Serializable {
    private String idPriority;
    private String namaPriority;
    private String flag;

    public String getIdPriority() {
        return idPriority;
    }

    public void setIdPriority(String idPriority) {
        this.idPriority = idPriority;
    }

    public String getNamaPriority() {
        return namaPriority;
    }

    public void setNamaPriority(String namaPriority) {
        this.namaPriority = namaPriority;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
