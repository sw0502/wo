package com.neurix.pmsapb.master.priority.model;

import com.neurix.common.model.BaseModel;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class Priority extends BaseModel {
    private String idPriority;
    private String namaPriority;
    private String flag;

    public String getIdPriority() {
        return idPriority;
    }

    public void setIdPriority(String idPriority) {
        this.idPriority = idPriority;
    }

    public String getNamaPriority() {
        return namaPriority;
    }

    public void setNamaPriority(String namaPriority) {
        this.namaPriority = namaPriority;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }
}
