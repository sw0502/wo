package com.neurix.pmsapb.master.gardu.bo.impl;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.gardu.bo.GarduBo;
import com.neurix.pmsapb.master.gardu.dao.GarduDao;
import com.neurix.pmsapb.master.gardu.model.Gardu;
import com.neurix.pmsapb.master.gardu.model.ImApbGarduEntity;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 12/09/17
 * Time: 4:56
 * To change this template use File | Settings | File Templates.
 */
public class GarduBoImpl implements GarduBo{

    private static Logger logger = Logger.getLogger(GarduBoImpl.class);
    private GarduDao garduDao;

    public void setGarduDao(GarduDao garduDao) {
        this.garduDao = garduDao;
    }

    @Override
    public void saveDelete(Gardu bean) throws GeneralBOException {
        logger.info("[saveDelete.saveDelete] start process >>>");

        if (bean!=null) {

            String garduId = bean.getNomorGarduInduk();

            ImApbGarduEntity imApbGarduEntity = null;

            try {
                // Get data from database by ID
                imApbGarduEntity = garduDao.getById("nomorGarduInduk", garduId);
            } catch (HibernateException e) {
                logger.error("[GarduBoImpl.saveDelete] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data gardu by Kode gardu, please inform to your admin...," + e.getMessage());
            }

            if (imApbGarduEntity != null) {

                // Modify from bean to entity serializable
                imApbGarduEntity.setKodeApp(bean.getKodeApp());
                imApbGarduEntity.setKodeApb(bean.getKodeApb());
                imApbGarduEntity.setNamaGarduInduk(bean.getNamaGarduInduk());
                //imApbGarduEntity.setKodeGardu(bean.getKodeGardu());
                imApbGarduEntity.setAlamat(bean.getAlamat());
                imApbGarduEntity.setTegangan(bean.getTegangan());
                imApbGarduEntity.setStatusGarduInduk(bean.getStatusGarduInduk());
                imApbGarduEntity.setJcc(bean.getJcc());

                imApbGarduEntity.setFlag(bean.getFlag());
                imApbGarduEntity.setAction(bean.getAction());
                imApbGarduEntity.setLastUpdateWho(bean.getLastUpdateWho());
                imApbGarduEntity.setLastUpdate(bean.getLastUpdate());

                try {
                    // Delete (Edit) into database
                    garduDao.updateAndSave(imApbGarduEntity);
                } catch (HibernateException e) {
                    logger.error("[GarduBoImpl.saveDelete] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving update data gardu, please info to your admin..." + e.getMessage());
                }


            } else {
                logger.error("[GarduBoImpl.saveDelete] Error, not found data gardu with request id, please check again your data ...");
                throw new GeneralBOException("Error, not found data gardu with request id, please check again your data ...");

            }
        }
        logger.info("[GarduBoImpl.saveDelete] end process <<<");
    }

    @Override
    public List<Gardu> getComboGarduByCriteria(String query) throws GeneralBOException {
        logger.info("[UserBoImpl.getComboUserWithCriteria] start process >>>");

        List<Gardu> listComboGardu = new ArrayList();
        String criteria = "%" + query + "%";

        List<ImApbGarduEntity> listGardu = null;
        try {
            listGardu = garduDao.getListGardu(criteria);
        } catch (HibernateException e) {
            logger.error("[UserBoImpl.getComboUserWithCriteria] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when retieving list user with criteria, please info to your admin..." + e.getMessage());
        }

        if (listGardu != null) {
            for (ImApbGarduEntity imApbGarduEntity : listGardu) {
                Gardu itemComboGardu = new Gardu();
                itemComboGardu.setNomorGarduInduk(imApbGarduEntity.getNomorGarduInduk());
                itemComboGardu.setNamaGarduInduk(imApbGarduEntity.getNamaGarduInduk());
                listComboGardu.add(itemComboGardu);
            }
        }
        logger.info("[UserBoImpl.getComboUserWithCriteria] end process <<<");
        return listComboGardu;
    }


    @Override
    public void saveEdit(Gardu bean) throws GeneralBOException {
        logger.info("[GarduBoImpl.saveEdit] start process >>>");

        if (bean!=null) {

            String garduId = bean.getNomorGarduInduk();

            ImApbGarduEntity imApbGarduEntity = null;

            try {
                // Get data from database by ID
                imApbGarduEntity = garduDao.getById("nomorGarduInduk", garduId);
            } catch (HibernateException e) {
                logger.error("[GarduBoImpl.saveEdit] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data gardu by Kode gardu, please inform to your admin...," + e.getMessage());
            }

            if (imApbGarduEntity != null) {
                imApbGarduEntity.setNomorGarduInduk(garduId);
                imApbGarduEntity.setKodeApp(bean.getKodeApp());
                imApbGarduEntity.setKodeApb(bean.getKodeApb());
                imApbGarduEntity.setNamaGarduInduk(bean.getNamaGarduInduk());
                //imApbGarduEntity.setKodeGardu(bean.getKodeGardu());
                imApbGarduEntity.setAlamat(bean.getAlamat());
                imApbGarduEntity.setTegangan(bean.getTegangan());
                imApbGarduEntity.setStatusGarduInduk(bean.getStatusGarduInduk());
                imApbGarduEntity.setJcc(bean.getJcc());

                imApbGarduEntity.setFlag(bean.getFlag());
                imApbGarduEntity.setAction(bean.getAction());
                imApbGarduEntity.setLastUpdateWho(bean.getLastUpdateWho());
                imApbGarduEntity.setLastUpdate(bean.getLastUpdate());

                try {
                    // Update into database
                    garduDao.updateAndSave(imApbGarduEntity);
                } catch (HibernateException e) {
                    logger.error("[GarduBoImpl.saveEdit] Error, " + e.getMessage());
                    throw new GeneralBOException("Found problem when saving update data gardu, please info to your admin..." + e.getMessage());
                }
            } else {
                logger.error("[GarduBoImpl.saveEdit] Error, not found data gardu with request id, please check again your data ...");
                throw new GeneralBOException("Error, not found data gardu with request id, please check again your data ...");

            }
        }
        logger.info("[GarduBoImpl.saveEdit] end process <<<");
    }

    @Override
    public Gardu saveAdd(Gardu bean) throws GeneralBOException {
        logger.info("[GarduBoImpl.saveAdd] start process >>>");

        if (bean!=null) {

            /*String garduId;
            try {
                // Generating ID, get from postgre sequence
                garduId = garduDao.getNextGarduId();
            } catch (HibernateException e) {
                logger.error("[GarduBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when getting sequence gardu id, please info to your admin..." + e.getMessage());
            }*/

            // creating object entity serializable
            ImApbGarduEntity imApbGarduEntity = new ImApbGarduEntity();

            imApbGarduEntity.setNomorGarduInduk(bean.getNomorGarduInduk());
            imApbGarduEntity.setKodeApp(bean.getKodeApp());
            imApbGarduEntity.setKodeApb(bean.getKodeApb());
            imApbGarduEntity.setNamaGarduInduk(bean.getNamaGarduInduk());
            //imApbGarduEntity.setKodeGardu(bean.getKodeGardu());
            imApbGarduEntity.setAlamat(bean.getAlamat());
            imApbGarduEntity.setTegangan(bean.getTegangan());
            imApbGarduEntity.setStatusGarduInduk(bean.getStatusGarduInduk());
            imApbGarduEntity.setJcc(bean.getJcc());

            imApbGarduEntity.setFlag(bean.getFlag());
            imApbGarduEntity.setAction(bean.getAction());
            imApbGarduEntity.setCreatedWho(bean.getCreatedWho());
            imApbGarduEntity.setLastUpdateWho(bean.getLastUpdateWho());
            imApbGarduEntity.setCreatedDate(bean.getCreatedDate());
            imApbGarduEntity.setLastUpdate(bean.getLastUpdate());

            try {
                // insert into database
                garduDao.addAndSave(imApbGarduEntity);
            } catch (HibernateException e) {
                logger.error("[GarduBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when saving new data gardu, please info to your admin..." + e.getMessage());
            }
        }

        logger.info("[GarduBoImpl.saveAdd] end process <<<");
        return null;
    }

    @Override
    public List<Gardu> getByCriteria(Gardu searchBean) throws GeneralBOException {
        logger.info("[GarduBoImpl.getByCriteria] start process >>>");

        // Mapping with collection and put
        List<Gardu> listOfResult = new ArrayList<Gardu>();

        if (searchBean != null) {
            Map hsCriteria = new HashMap();

            if (searchBean.getNomorGarduInduk() != null && !"".equalsIgnoreCase(searchBean.getNomorGarduInduk())) {
                hsCriteria.put("no_gi", searchBean.getNomorGarduInduk());
            }
            if (searchBean.getKodeApp() != null && !"".equalsIgnoreCase(searchBean.getKodeApp())) {
                hsCriteria.put("kode_app", searchBean.getKodeApp());
            }
            if (searchBean.getKodeApb() != null && !"".equalsIgnoreCase(searchBean.getKodeApb())) {
                hsCriteria.put("kode_apb", searchBean.getKodeApb());
            }
            if (searchBean.getNamaGarduInduk() != null && !"".equalsIgnoreCase(searchBean.getNamaGarduInduk())) {
                hsCriteria.put("nama_gi", searchBean.getNamaGarduInduk());
            }
//            if (searchBean.getKodeGardu() != null && !"".equalsIgnoreCase(searchBean.getKodeGardu())) {
//                hsCriteria.put("kode_gardu", searchBean.getKodeGardu());
//            }
            if (searchBean.getTegangan() != null && !"".equalsIgnoreCase(searchBean.getTegangan())) {
                hsCriteria.put("tegangan", searchBean.getTegangan());
            }
            if (searchBean.getStatusGarduInduk() != null && !"".equalsIgnoreCase(searchBean.getStatusGarduInduk())) {
                hsCriteria.put("status_gi", searchBean.getStatusGarduInduk());
            }
            if (searchBean.getJcc() != null && !"".equalsIgnoreCase(searchBean.getJcc())) {
                hsCriteria.put("jcc", searchBean.getJcc());
            }
            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }


            List<ImApbGarduEntity> imApbGarduEntities = null;
            try {

                imApbGarduEntities = garduDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[GarduBoImpl.getSearchGarduByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if(imApbGarduEntities != null){
                Gardu returnGardu;
                // Looping from dao to object and save in collection
                for(ImApbGarduEntity gardu : imApbGarduEntities){
                    returnGardu = new Gardu();
                    returnGardu.setNomorGarduInduk(gardu.getNomorGarduInduk());
                    returnGardu.setKodeApp(gardu.getKodeApp());
                    returnGardu.setKodeApb(gardu.getKodeApb());
                    returnGardu.setNamaGarduInduk(gardu.getNamaGarduInduk());
                    //returnGardu.setKodeGardu(gardu.getKodeGardu());
                    returnGardu.setAlamat(gardu.getAlamat());
                    returnGardu.setTegangan(gardu.getTegangan());
                    returnGardu.setStatusGarduInduk(gardu.getStatusGarduInduk());
                    returnGardu.setJcc(gardu.getJcc());

                    returnGardu.setAction(gardu.getAction());
                    returnGardu.setFlag(gardu.getFlag());

                    listOfResult.add(returnGardu);
                }
            }
        }
        logger.info("[GarduBoImpl.getByCriteria] end process <<<");

        return listOfResult;
    }

    @Override
    public List<Gardu> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }
}
