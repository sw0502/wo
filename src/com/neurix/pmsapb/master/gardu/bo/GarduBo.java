package com.neurix.pmsapb.master.gardu.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.gardu.model.Gardu;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 11/09/17
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
public interface GarduBo extends BaseMasterBo<Gardu>{
    void saveDelete(Gardu bean) throws GeneralBOException;
    public List<Gardu> getComboGarduByCriteria(String query) throws GeneralBOException;
}
