package com.neurix.pmsapb.master.gardu.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 11/09/17
 * Time: 16:17
 * To change this template use File | Settings | File Templates.
 */
public class Gardu implements Serializable {
    private String nomorGarduInduk;
    private String kodeApp;
    private String kodeApb;
    private String namaGarduInduk;
    private String kodeGardu;
    private String alamat;
    private String tegangan;
    private String statusGarduInduk;
    private String jcc;
    private String flag;
    private String action;
    private Timestamp createdDate;
    private Timestamp lastUpdate;
    private String createdWho;
    private String lastUpdateWho;

    public String getNomorGarduInduk() {
        return nomorGarduInduk;
    }

    public void setNomorGarduInduk(String nomorGarduInduk) {
        this.nomorGarduInduk = nomorGarduInduk;
    }

    public String getKodeApp() {
        return kodeApp;
    }

    public void setKodeApp(String kodeApp) {
        this.kodeApp = kodeApp;
    }

    public String getKodeApb() {
        return kodeApb;
    }

    public void setKodeApb(String kodeApb) {
        this.kodeApb = kodeApb;
    }

    public String getKodeGardu() {
        return kodeGardu;
    }

    public void setKodeGardu(String kodeGardu) {
        this.kodeGardu = kodeGardu;
    }

    public String getNamaGarduInduk() {
        return namaGarduInduk;
    }

    public void setNamaGarduInduk(String namaGarduInduk) {
        this.namaGarduInduk = namaGarduInduk;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTegangan() {
        return tegangan;
    }

    public void setTegangan(String tegangan) {
        this.tegangan = tegangan;
    }

    public String getStatusGarduInduk() {
        return statusGarduInduk;
    }

    public void setStatusGarduInduk(String statusGarduInduk) {
        this.statusGarduInduk = statusGarduInduk;
    }

    public String getJcc() {
        return jcc;
    }

    public void setJcc(String jcc) {
        this.jcc = jcc;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getCreatedWho() {
        return createdWho;
    }

    public void setCreatedWho(String createdWho) {
        this.createdWho = createdWho;
    }

    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }
}
