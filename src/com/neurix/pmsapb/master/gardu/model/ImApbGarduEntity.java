package com.neurix.pmsapb.master.gardu.model;

import com.neurix.common.model.BaseModel;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 11/09/17
 * Time: 16:17
 * To change this template use File | Settings | File Templates.
 */
public class ImApbGarduEntity extends BaseModel {
    private String nomorGarduInduk;
    private String kodeApp;
    private String kodeApb;
    private String namaGarduInduk;
    private String kodeGardu;
    private String alamat;
    private String tegangan;
    private String statusGarduInduk;
    private String jcc;

    public String getNomorGarduInduk() {
        return nomorGarduInduk;
    }

    public void setNomorGarduInduk(String nomorGarduInduk) {
        this.nomorGarduInduk = nomorGarduInduk;
    }

    public String getKodeApp() {
        return kodeApp;
    }

    public void setKodeApp(String kodeApp) {
        this.kodeApp = kodeApp;
    }

    public String getKodeApb() {
        return kodeApb;
    }

    public void setKodeApb(String kodeApb) {
        this.kodeApb = kodeApb;
    }

    public String getNamaGarduInduk() {
        return namaGarduInduk;
    }

    public void setNamaGarduInduk(String namaGarduInduk) {
        this.namaGarduInduk = namaGarduInduk;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTegangan() {
        return tegangan;
    }

    public void setTegangan(String tegangan) {
        this.tegangan = tegangan;
    }

    public String getStatusGarduInduk() {
        return statusGarduInduk;
    }

    public void setStatusGarduInduk(String statusGarduInduk) {
        this.statusGarduInduk = statusGarduInduk;
    }

    public String getJcc() {
        return jcc;
    }

    public void setJcc(String jcc) {
        this.jcc = jcc;
    }

    public String getKodeGardu() {
        return kodeGardu;
    }

    public void setKodeGardu(String kodeGardu) {
        this.kodeGardu = kodeGardu;
    }
}
