package com.neurix.pmsapb.master.gardu.action;

import com.neurix.common.action.BaseMasterAction;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.master.gardu.model.Gardu;
import com.neurix.pmsapb.master.gardu.bo.GarduBo;
import com.neurix.pmsapb.master.gardu.model.Gardu;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 11/09/17
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
public class GarduAction extends BaseMasterAction{
    private static Logger logger = Logger.getLogger(GarduAction.class);
    private GarduBo garduBoProxy;
    private Gardu gardu;

    public void setGarduBoProxy(GarduBo garduBoProxy) {
        this.garduBoProxy = garduBoProxy;
    }

    public void setGardu(Gardu gardu) {
        this.gardu = gardu;
    }

    public Gardu getGardu() {
        return gardu;
    }

    public Gardu init(String kode, String flag){
        logger.info("[GarduAction.init] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Gardu> listOfResult = (List<Gardu>) session.getAttribute("listOfResult");

        if(kode != null && !"".equalsIgnoreCase(kode)){
            if(listOfResult != null){
                for (Gardu gardu: listOfResult) {
                    if(kode.equalsIgnoreCase(gardu.getNomorGarduInduk()) && flag.equalsIgnoreCase(gardu.getFlag())){
                        setGardu(gardu);
                        break;
                    }
                }
            } else {
                setGardu(new Gardu());
            }

            logger.info("[GarduAction.init] end process <<<");
        }
        return getGardu();
    }

    @Override
    public String add() {
        logger.info("[GarduAction.add] start process >>>");
        Gardu addGardu = new Gardu();
        setGardu(addGardu);

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[GarduAction.add] stop process <<<");
        return "init_add";
    }

    @Override
    public String edit() {
        logger.info("[GarduAction.edit] start process >>>");
        String itemId = getId();
        String itemFlag = getFlag();

        Gardu gardu = new Gardu();

        if(itemFlag != null){
            try {
                gardu = init(itemId, itemFlag);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = garduBoProxy.saveErrorMessage(e.getMessage(), "GarduBO.getGarduByCriteria");
                } catch (GeneralBOException e1) {
                    logger.error("[GarduAction.edit] Error when retrieving edit data,", e1);
                }
                logger.error("[GarduAction.edit] Error when retrieving item," + "[" + logId + "] Found problem when retrieving data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when retrieving data for edit, please inform to your admin.");
                return "failure";
            }

            if(gardu != null) {
                setGardu(gardu);
            } else {
                gardu.setFlag(itemFlag);
                gardu.setNomorGarduInduk(itemId);
                setGardu(gardu);
                addActionError("Error, Unable to find data with id = " + itemId);
                return "failure";
            }
        } else {
            gardu.setNomorGarduInduk(itemId);
            gardu.setFlag(getFlag());
            setGardu(gardu);
            addActionError("Error, Unable to edit again with flag = N.");
            return "failure";
        }

        logger.info("[GarduAction.edit] end process <<<");
        return "init_edit";
    }

    public String saveAdd(){
        logger.info("[GarduAction.saveAdd] start process >>>");

        try {
            Gardu gardu = getGardu();
            String userLogin = CommonUtil.userLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            gardu.setCreatedWho(userLogin);
            gardu.setLastUpdate(updateTime);
            gardu.setCreatedDate(updateTime);
            gardu.setLastUpdateWho(userLogin);
            gardu.setAction("C");
            gardu.setFlag("Y");

            garduBoProxy.saveAdd(gardu);
        }catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = garduBoProxy.saveErrorMessage(e.getMessage(), "GarduBO.saveAdd");
            } catch (GeneralBOException e1) {
                logger.error("[GarduAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[GarduAction.saveAdd] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[GarduAction.saveAdd] end process <<<");
        return "success_save_add";
    }

    public String saveAddSpek(){
        logger.info("[GarduAction.saveAdd] start process >>>");

        try {
            Gardu gardu = getGardu();
            String userLogin = CommonUtil.userLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            gardu.setCreatedWho(userLogin);
            gardu.setLastUpdate(updateTime);
            gardu.setCreatedDate(updateTime);
            gardu.setLastUpdateWho(userLogin);
            gardu.setAction("C");
            gardu.setFlag("Y");

            garduBoProxy.saveAdd(gardu);
        }catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = garduBoProxy.saveErrorMessage(e.getMessage(), "GarduBO.saveAdd");
            } catch (GeneralBOException e1) {
                logger.error("[GarduAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[GarduAction.saveAdd] Error when adding item ," + "[" + logId + "] Found problem when saving add data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving add data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");

        logger.info("[GarduAction.saveAdd] end process <<<");
        return "success_save_add_spek";
    }

    public String saveEdit(){
        logger.info("[GarduAction.saveEdit] start process >>>");
        try {

            Gardu gardu = getGardu();

            String userLogin = CommonUtil.userLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            gardu.setLastUpdateWho(userLogin);
            gardu.setLastUpdate(updateTime);
            gardu.setAction("U");

            garduBoProxy.saveEdit(gardu);

        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = garduBoProxy.saveErrorMessage(e.getMessage(), "GarduBO.saveEdit");
            } catch (GeneralBOException e1) {
                logger.error("[GarduAction.saveEdit] Error when saving error,", e1);
            }
            logger.error("[GarduAction.saveEdit] Error when editing item gardu," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving edit data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        logger.info("[GarduAction.saveEdit] end process <<<");

        return "success_save_edit";
    }

    public String saveDelete(){
        logger.info("[GarduAction.saveDelete] start process >>>");
        try {

            Gardu gardu = getGardu();

            String userLogin = CommonUtil.userLogin();
            Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());

            gardu.setLastUpdate(updateTime);
            gardu.setLastUpdateWho(userLogin);
            gardu.setAction("U");
            gardu.setFlag("N");

            garduBoProxy.saveDelete(gardu);

        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = garduBoProxy.saveErrorMessage(e.getMessage(), "GarduBO.saveDelete");
            } catch (GeneralBOException e1) {
                logger.error("[GarduAction.saveDelete] Error when saving error,", e1);
            }
            logger.error("[GarduAction.saveDelete] Error when editing item gardu," + "[" + logId + "] Found problem when saving edit data, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when saving edit data, please inform to your admin.\n" + e.getMessage());
            return ERROR;
        }

        logger.info("[GarduAction.saveDelete] end process <<<");

        return "success_save_delete";
    }

    @Override
    public String delete() {
        logger.info("[GarduAction.delete] start process >>>");

        String itemId = getId();
        String itemFlag = getFlag();
        Gardu gardu = new Gardu();

        if (itemFlag != null ) {

            try {
                gardu = init(itemId, itemFlag);
            } catch (GeneralBOException e) {
                Long logId = null;
                try {
                    logId = garduBoProxy.saveErrorMessage(e.getMessage(), "GarduBO.getGarduById");
                } catch (GeneralBOException e1) {
                    logger.error("[GarduAction.delete] Error when retrieving delete data,", e1);
                }
                logger.error("[GarduAction.delete] Error when retrieving item," + "[" + logId + "] Found problem when retrieving data, please inform to your admin.", e);
                addActionError("Error, " + "[code=" + logId + "] Found problem when retrieving data for delete, please inform to your admin.");
                return "failure";
            }

            if (gardu != null) {
                setGardu(gardu);

            } else {
                gardu.setNomorGarduInduk(itemId);
                gardu.setFlag(itemFlag);
                setGardu(gardu);
                addActionError("Error, Unable to find data with id = " + itemId);
                return "failure";
            }
        } else {
            gardu.setNomorGarduInduk(itemId);
            gardu.setFlag(itemFlag);
            setGardu(gardu);
            addActionError("Error, Unable to delete again with flag = N.");
            return "failure";
        }

        logger.info("[GarduAction.delete] end process <<<");

        return "init_delete";
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        return null;
    }

    @Override
    public String search() {
        logger.info("[GarduAction.search] start process >>>");

        Gardu searchGardu = getGardu();
        List<Gardu> listOfSearchGardu = new ArrayList<Gardu>();

        try {
            listOfSearchGardu = garduBoProxy.getByCriteria(searchGardu);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = garduBoProxy.saveErrorMessage(e.getMessage(), "GarduBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[GarduAction.search] Error when saving error,", e1);
            }
            logger.error("[GarduAction.save] Error when searching gardu by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin" );
            return "failure";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfSearchGardu);

        logger.info("[GarduAction.search] end process <<<");

        return SUCCESS;
    }

    @Override
    public String initForm() {
        logger.info("[GarduAction.initForm] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        logger.info("[GarduAction.initForm] end process <<<");
        return INPUT;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }
}
