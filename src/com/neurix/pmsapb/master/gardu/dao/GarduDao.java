package com.neurix.pmsapb.master.gardu.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.gardu.model.ImApbGarduEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: gondok
 * Date: 11/09/17
 * Time: 16:17
 * To change this template use File | Settings | File Templates.
 */
public class GarduDao extends GenericDao<ImApbGarduEntity, String> {
    @Override
    protected Class<ImApbGarduEntity> getEntityClass() {
        return ImApbGarduEntity.class;
    }

    @Override
    public List<ImApbGarduEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImApbGarduEntity.class);

        // Get by collection key
        if (mapCriteria!=null) {
            if (mapCriteria.get("no_gi")!=null) {
                criteria.add(Restrictions.eq("nomorGarduInduk", (String) mapCriteria.get("no_gi")));
            }
            if (mapCriteria.get("kode_app")!=null) {
                criteria.add(Restrictions.eq("kodeApp", (String) mapCriteria.get("kode_app")));
            }
            if (mapCriteria.get("kode_apb")!=null) {
                criteria.add(Restrictions.eq("kodeApb", (String) mapCriteria.get("kode_apb")));
            }
            if (mapCriteria.get("nama_gi")!=null) {
                criteria.add(Restrictions.eq("namaGarduInduk", (String) mapCriteria.get("nama_gi")));
            }
//            if (mapCriteria.get("kode_gardu")!=null) {
//                criteria.add(Restrictions.eq("kodeGardu", (String) mapCriteria.get("kode_gardu")));
//            }
            if (mapCriteria.get("tegangan")!=null) {
                criteria.add(Restrictions.eq("tegangan", (String) mapCriteria.get("tegangan")));
            }
            if (mapCriteria.get("status_gi")!=null) {
                criteria.add(Restrictions.eq("statusGarduInduk", (String) mapCriteria.get("status_gi")));
            }
            if (mapCriteria.get("jcc")!=null) {
                criteria.add(Restrictions.eq("jcc", (String) mapCriteria.get("jcc")));
            }

        }

        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));

        // Order by
        criteria.addOrder(Order.asc("kodeApp"));

        List<ImApbGarduEntity> results = criteria.list();

        return results;
    }

    // Generate surrogate id from postgre
    public String getNextGarduId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_apb_gardu')");
        Iterator<BigInteger> iter=query.list().iterator();
        return String.valueOf(iter.next().longValue());
    }

    public List<ImApbGarduEntity> getListGardu(String term) throws HibernateException {

        List<ImApbGarduEntity> results = this.sessionFactory.getCurrentSession().createCriteria(ImApbGarduEntity.class)
                .add(Restrictions.ilike("namaGarduInduk",term))
                .add(Restrictions.eq("flag", "Y"))
                .addOrder(Order.asc("nomorGarduInduk"))
//                .addOrder(Order.asc("primaryKey.id"))
                .list();
        return results;
    }


}
