package com.neurix.pmsapb.master.divisi.action;

import com.neurix.common.action.BaseMasterAction;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.master.divisi.bo.DivisiBo;
import com.neurix.pmsapb.master.divisi.model.Divisi;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class DivisiAction extends BaseMasterAction {
    protected static transient Logger logger = Logger.getLogger(DivisiAction.class);

    private DivisiBo divisiBoProxy;
    private Divisi divisi;

    public DivisiBo getDivisiBoProxy() {
        return divisiBoProxy;
    }

    public void setDivisiBoProxy(DivisiBo divisiBoProxy) {
        this.divisiBoProxy = divisiBoProxy;
    }

    public Divisi getDivisi() {
        return divisi;
    }

    public void setDivisi(Divisi divisi) {
        this.divisi = divisi;
    }

    @Override
    public String add() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        return "add";
    }

    @Override
    public String edit() {
        logger.info("[DivisiAction.edit] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Divisi> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (Divisi divisiOfList : listOfResult) {
                    if (id.equalsIgnoreCase(divisiOfList.getIdDivisi())) {
                        setDivisi(divisiOfList);
                        break;
                    }
                }
            } else {
                setDivisi(new Divisi());
            }
        } else {
            setDivisi(new Divisi());
        }

        return "edit";
    }

    @Override
    public String delete() {
        logger.info("[DivisiAction.edit] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Divisi> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (Divisi divisiOfList : listOfResult) {
                    if (id.equalsIgnoreCase(divisiOfList.getIdDivisi())) {
                        setDivisi(divisiOfList);
                        break;
                    }
                }
            } else {
                setDivisi(new Divisi());
            }
        } else {
            setDivisi(new Divisi());
        }

        return "delete";
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Divisi entryDivisi = getDivisi();
        entryDivisi.setCreatedDate(updateTime);
        entryDivisi.setLastUpdate(updateTime);
        entryDivisi.setCreatedWho(userLogin);
        entryDivisi.setLastUpdateWho(userLogin);

        String getSeqId = null;
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        DivisiBo divisiBo = (DivisiBo) ctx.getBean("divisiBoProxy");

        try{
            getSeqId = divisiBo.getSeqDivisiId();
        }catch (GeneralBOException e){
            logger.error("Error when generate sequence id" + e);
        }

        if(getSeqId != null){
            entryDivisi.setIdDivisi("DIV"+getSeqId);
        }

        try {
            divisiBoProxy.saveAdd(entryDivisi);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = divisiBoProxy.saveErrorMessage(e.getMessage(), "DivisiAction.saveAdd");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_add";
        }
        return "save_add";
    }

    public String saveEdit(){
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Divisi entryDivisi = getDivisi();
//        entryDivisi.setCreatedDate(updateTime);
        entryDivisi.setLastUpdate(updateTime);
//        entryDivisi.setCreatedWho(userLogin);
        entryDivisi.setLastUpdateWho(userLogin);

        try {
            divisiBoProxy.saveEdit(entryDivisi);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = divisiBoProxy.saveErrorMessage(e.getMessage(), "DivisiAction.saveEdit");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_edit";
        }
        return "save_edit";
    }

    public String saveDelete(){
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Divisi entryDivisi = getDivisi();
//        entryDivisi.setCreatedDate(updateTime);
        entryDivisi.setLastUpdate(updateTime);
//        entryDivisi.setCreatedWho(userLogin);
        entryDivisi.setLastUpdateWho(userLogin);

        try {
            divisiBoProxy.saveDelete(entryDivisi);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = divisiBoProxy.saveErrorMessage(e.getMessage(), "DivisiAction.saveDelete");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_edit";
        }
        return "save_delete";
    }

    @Override
    public String search() {
        logger.info("[DivisiAction.search] start process >>>");

        Divisi searchDivisi = getDivisi();
        List<Divisi> listOfDivisi = new ArrayList();

        try {
            listOfDivisi = divisiBoProxy.getByCriteria(searchDivisi);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = divisiBoProxy.saveErrorMessage(e.getMessage(), "AlatBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[AlatAction.search] Error when saving error,", e1);
            }
            logger.error("[AlatAction.save] Error when searching ActivityAlatController by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin" );
            return "failure";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfDivisi);

        logger.info("[DivisiAction.search] end process <<<");

        return SUCCESS;
    }

    @Override
    public String initForm() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        return INPUT;
    }


    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }
}
