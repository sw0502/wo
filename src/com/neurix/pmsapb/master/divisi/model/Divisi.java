package com.neurix.pmsapb.master.divisi.model;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class Divisi extends BaseModel implements Serializable {
    private String idDivisi;
    private String namaDivisi;
    private String flag;
    private String Action;
    private Timestamp CreatedDate;
    private String CreatedWho;
    private Timestamp lastUpdate;
    private String lastUpdateWho;

    @Override
    public Timestamp getCreatedDate() {
        return CreatedDate;
    }

    @Override
    public void setCreatedDate(Timestamp createdDate) {
        CreatedDate = createdDate;
    }

    @Override
    public String getCreatedWho() {
        return CreatedWho;
    }

    @Override
    public void setCreatedWho(String createdWho) {
        CreatedWho = createdWho;
    }

    @Override
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    @Override
    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String getLastUpdateWho() {
        return lastUpdateWho;
    }

    @Override
    public void setLastUpdateWho(String lastUpdateWho) {
        this.lastUpdateWho = lastUpdateWho;
    }

    public String getIdDivisi() {
        return idDivisi;
    }

    public void setIdDivisi(String idDivisi) {
        this.idDivisi = idDivisi;
    }

    public String getNamaDivisi() {
        return namaDivisi;
    }

    public void setNamaDivisi(String namaDivisi) {
        this.namaDivisi = namaDivisi;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String getAction() {
        return Action;
    }

    @Override
    public void setAction(String action) {
        Action = action;
    }
}

