package com.neurix.pmsapb.master.divisi.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.divisi.model.ImApbDivisiEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class DivisiDao extends GenericDao<ImApbDivisiEntity, String> {

    @Override
    protected Class<ImApbDivisiEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ImApbDivisiEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImApbDivisiEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("divisi_id")!=null) {
                criteria.add(Restrictions.eq("idDivisi", (String) mapCriteria.get("divisi_id")));
            }
            if (mapCriteria.get("divisi_name")!=null) {
                criteria.add(Restrictions.ilike("namaDivisi", "%" + (String)mapCriteria.get("divisi_name") + "%"));
            }
        }
        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));

        // Order by
        criteria.addOrder(Order.asc("idDivisi"));
        List<ImApbDivisiEntity> results = criteria.list();
        return results;
    }

    public String getNextId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_divisi" + "')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;

    }
}
