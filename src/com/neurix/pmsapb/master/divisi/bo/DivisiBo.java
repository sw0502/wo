package com.neurix.pmsapb.master.divisi.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.divisi.model.Divisi;

/**
 * Created by thinkpad on 15/11/2017.
 */
public interface DivisiBo extends BaseMasterBo<Divisi>{
    public String getSeqDivisiId() throws GeneralBOException;

}
