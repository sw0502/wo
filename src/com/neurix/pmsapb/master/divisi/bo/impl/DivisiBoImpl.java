package com.neurix.pmsapb.master.divisi.bo.impl;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.alat.model.Alat;
import com.neurix.pmsapb.master.divisi.bo.DivisiBo;
import com.neurix.pmsapb.master.divisi.dao.DivisiDao;
import com.neurix.pmsapb.master.divisi.model.Divisi;
import com.neurix.pmsapb.master.divisi.model.ImApbDivisiEntity;
import org.hibernate.HibernateException;
import org.apache.log4j.Logger;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class DivisiBoImpl implements DivisiBo {
    protected static transient Logger logger = Logger.getLogger(DivisiBoImpl.class);
    private DivisiDao divisiDao;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        DivisiBoImpl.logger = logger;
    }

    public DivisiDao getDivisiDao() {
        return divisiDao;
    }

    public void setDivisiDao(DivisiDao divisiDao) {
        this.divisiDao = divisiDao;
    }

    @Override
    public void saveDelete(Divisi bean) throws GeneralBOException {
        if (bean!=null){
            ImApbDivisiEntity divisiEntity = new ImApbDivisiEntity();
            divisiEntity.setIdDivisi(bean.getIdDivisi());
            divisiEntity.setNamaDivisi(bean.getNamaDivisi());
            divisiEntity.setCreatedDate(bean.getCreatedDate());
            divisiEntity.setCreatedWho(bean.getCreatedWho());
            divisiEntity.setLastUpdate(bean.getLastUpdate());
            divisiEntity.setLastUpdateWho(bean.getLastUpdateWho());
            divisiEntity.setFlag("N");
            divisiEntity.setAction("U");
            try {
                divisiDao.updateAndSave(divisiEntity);
            }catch (HibernateException e){
                logger.error("[DivisiBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when Delete data Divisi, please info to your admin..." + e.getMessage());
            }
        }

    }

    @Override
    public void saveEdit(Divisi bean) throws GeneralBOException {
        if (bean!=null){
            ImApbDivisiEntity divisiEntity = new ImApbDivisiEntity();
            divisiEntity.setIdDivisi(bean.getIdDivisi());
            divisiEntity.setNamaDivisi(bean.getNamaDivisi());
            divisiEntity.setCreatedDate(bean.getCreatedDate());
            divisiEntity.setCreatedWho(bean.getCreatedWho());
            divisiEntity.setLastUpdate(bean.getLastUpdate());
            divisiEntity.setLastUpdateWho(bean.getLastUpdateWho());
            divisiEntity.setFlag(bean.getFlag());
            divisiEntity.setAction("U");
            try {
                divisiDao.updateAndSave(divisiEntity);
            }catch (HibernateException e){
                logger.error("[DivisiBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when Update data Divisi, please info to your admin..." + e.getMessage());
            }
        }
    }

    @Override
    public Divisi saveAdd(Divisi bean) throws GeneralBOException {
        if (bean!=null){
            ImApbDivisiEntity divisiEntity = new ImApbDivisiEntity();
            divisiEntity.setIdDivisi(bean.getIdDivisi());
            divisiEntity.setNamaDivisi(bean.getNamaDivisi());
            divisiEntity.setCreatedDate(bean.getCreatedDate());
            divisiEntity.setCreatedWho(bean.getCreatedWho());
            divisiEntity.setLastUpdate(bean.getLastUpdate());
            divisiEntity.setLastUpdateWho(bean.getLastUpdateWho());
            divisiEntity.setFlag("Y");
            divisiEntity.setAction("C");
            try {
                divisiDao.addAndSave(divisiEntity);
            }catch (HibernateException e){
                logger.error("[DivisiBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when Add data Divisi, please info to your admin..." + e.getMessage());
            }
        }
        return null;
    }

    @Override
    public List<Divisi> getByCriteria(Divisi searchBean) throws GeneralBOException {
        logger.info("[DivisiBoImpl.getByCriteria] start process >>>");

        // Mapping with collection and put
        List<Divisi> listOfResult = new ArrayList();

        if (searchBean != null) {
            Map hsCriteria = new HashMap();

            if (searchBean.getIdDivisi() != null && !"".equalsIgnoreCase(searchBean.getIdDivisi())) {
                hsCriteria.put("divisi_id", searchBean.getIdDivisi());
            }
            if (searchBean.getNamaDivisi() != null && !"".equalsIgnoreCase(searchBean.getNamaDivisi())) {
                hsCriteria.put("divisi_name", searchBean.getNamaDivisi());
            }
            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            List<ImApbDivisiEntity> imApbDivisiEntities = null;
            try {
                imApbDivisiEntities = divisiDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[DivisiBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if(imApbDivisiEntities != null){
                Divisi returnDivisi;
                // Looping from dao to object and save in collection
                for(ImApbDivisiEntity divisiEntity : imApbDivisiEntities){
                    returnDivisi = new Divisi();
                    returnDivisi.setIdDivisi(divisiEntity.getIdDivisi());
                    returnDivisi.setNamaDivisi(divisiEntity.getNamaDivisi());
                    returnDivisi.setFlag(divisiEntity.getFlag());
                    returnDivisi.setAction(divisiEntity.getAction());
                    listOfResult.add(returnDivisi);
                }
            }
        }
        logger.info("[DivisiBoImpl.getByCriteria] end process <<<");
        return listOfResult;
    }

    @Override
    public List<Divisi> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }

    @Override
    public String getSeqDivisiId() throws GeneralBOException {
        String id = null;
        try {
            id=divisiDao.getNextId();
        } catch (HibernateException e) {
            logger.error("[DivisiBoImpl.getNextId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }
        return id;
    }
}
