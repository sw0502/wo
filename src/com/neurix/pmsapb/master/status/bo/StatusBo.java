package com.neurix.pmsapb.master.status.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.status.model.Status;

import java.util.List;

/**
 * Created by thinkpad on 23/07/2017.
 */
public interface StatusBo extends BaseMasterBo<Status> {
    public List<Status> getStatusByCriteria ( Status searchStatus ) throws GeneralBOException;
}
