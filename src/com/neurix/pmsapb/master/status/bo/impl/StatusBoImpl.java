package com.neurix.pmsapb.master.status.bo.impl;

import com.neurix.authorization.user.model.ImUsers;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.status.bo.StatusBo;
import com.neurix.pmsapb.master.status.dao.StatusDao;
import com.neurix.pmsapb.master.status.model.ImApbStatusEntity;
import com.neurix.pmsapb.master.status.model.Status;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class StatusBoImpl implements StatusBo {
    protected static transient Logger logger = Logger.getLogger(StatusBoImpl.class);

    private StatusDao statusDao;

    public void setStatusDao(StatusDao statusDao) {
        this.statusDao = statusDao;
    }

    public StatusDao getStatusDao() {
        return statusDao;
    }

    @Override
    public void saveDelete(Status bean) throws GeneralBOException {

    }

    @Override
    public void saveEdit(Status bean) throws GeneralBOException {

    }

    @Override
    public Status saveAdd(Status bean) throws GeneralBOException {
        return null;
    }

    @Override
    public List<Status> getByCriteria(Status searchBean) throws GeneralBOException {
        return null;
    }

    @Override
    public List<Status> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }

    @Override
    public List<Status> getStatusByCriteria(Status searchStatus) throws GeneralBOException {
        logger.info("[StatusBoImpl.getStatusByCriteria] start process >>>");

        List<Status> listOfResultStatus = new ArrayList<Status>();

        if(searchStatus != null){
            String flag = searchStatus.getFlag();
            Map hsCriteria = new HashMap();

            if (searchStatus.getFlag() != null && !"".equalsIgnoreCase(searchStatus.getFlag())) {
                if ("N".equalsIgnoreCase(searchStatus.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchStatus.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }
            List<ImApbStatusEntity> listOfStatus = null;
            try{
                listOfStatus = statusDao.getByCriteria(hsCriteria);
            }catch (HibernateException e){
                logger.error("[UserBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
            if(listOfStatus != null){
                Status resulStatus;
                for(ImApbStatusEntity imApbStatus : listOfStatus){
                    resulStatus = new Status();
                    resulStatus.setIdStatus(imApbStatus.getIdStatus());
                    resulStatus.setNamaStatus(imApbStatus.getNamaStatus());
                    listOfResultStatus.add(resulStatus);
                }
            }

        }
        logger.info("[StatusBoImpl.getStatusByCriteria] start process >>>");
        return listOfResultStatus;
    }
}
