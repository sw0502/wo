package com.neurix.pmsapb.master.status.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.status.model.ImApbStatusEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class StatusDao extends GenericDao<ImApbStatusEntity,String> {

    @Override
    protected Class<ImApbStatusEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ImApbStatusEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImApbStatusEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("status_id")!=null) {
                criteria.add(Restrictions.eq("idStatus", (String) mapCriteria.get("status_id")));
            }
            if (mapCriteria.get("status_name")!=null) {
                criteria.add(Restrictions.ilike("namaStatus", "%" + (String)mapCriteria.get("status_name") + "%"));
            }
        }
        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.asc("idStatus"));
        List<ImApbStatusEntity> results = criteria.list();

        return results;
    }
}
