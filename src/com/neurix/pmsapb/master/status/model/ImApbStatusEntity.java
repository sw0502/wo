package com.neurix.pmsapb.master.status.model;

import java.io.Serializable;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class ImApbStatusEntity implements Serializable {
    private String idStatus;
    private String namaStatus;
    private String flag;

    public String getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(String idStatus) {
        this.idStatus = idStatus;
    }

    public String getNamaStatus() {
        return namaStatus;
    }

    public void setNamaStatus(String namaStatus) {
        this.namaStatus = namaStatus;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
