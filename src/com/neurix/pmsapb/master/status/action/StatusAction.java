package com.neurix.pmsapb.master.status.action;

import com.neurix.common.action.BaseMasterAction;
import com.neurix.pmsapb.master.status.bo.StatusBo;
import com.neurix.pmsapb.master.status.model.Status;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class StatusAction extends BaseMasterAction {
    private StatusBo statusBoProxy;
    private Status status;


    public StatusBo getStatusBoProxy() {
        return statusBoProxy;
    }

    public void setStatusBoProxy(StatusBo statusBoProxy) {
        this.statusBoProxy = statusBoProxy;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String add() {
        return null;
    }

    @Override
    public String edit() {
        return null;
    }

    @Override
    public String delete() {
        return null;
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        return null;
    }

    @Override
    public String search() {
        return null;
    }

    @Override
    public String initForm() {
        return null;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }
}
