package com.neurix.pmsapb.master.progres.model;

import com.neurix.common.model.BaseModel;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class Progres extends BaseModel {
    private String idProgres;
    private String namProgres;
    private String flag;

    public String getIdProgres() {
        return idProgres;
    }

    public void setIdProgres(String idProgres) {
        this.idProgres = idProgres;
    }

    public String getNamProgres() {
        return namProgres;
    }

    public void setNamProgres(String namProgres) {
        this.namProgres = namProgres;
    }

    @Override
    public String getFlag() {
        return flag;
    }

    @Override
    public void setFlag(String flag) {
        this.flag = flag;
    }
}
