package com.neurix.pmsapb.master.progres.model;

import java.io.Serializable;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class ImProgres implements Serializable {
    private String idProgres;
    private String namaProgres;
    private String flag;

    public String getIdProgres() {
        return idProgres;
    }

    public void setIdProgres(String idProgres) {
        this.idProgres = idProgres;
    }

    public String getNamaProgres() {
        return namaProgres;
    }

    public void setNamaProgres(String namaProgres) {
        this.namaProgres = namaProgres;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
