package com.neurix.pmsapb.master.progres.action;

import com.neurix.common.action.BaseMasterAction;
import com.neurix.pmsapb.master.progres.bo.ProgresBo;
import com.neurix.pmsapb.master.progres.model.Progres;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class ProgresAction extends BaseMasterAction {
    private ProgresBo progresBoProxy;
    private Progres progres;

    public ProgresBo getProgresBoProxy() {
        return progresBoProxy;
    }

    public void setProgresBoProxy(ProgresBo progresBoProxy) {
        this.progresBoProxy = progresBoProxy;
    }

    public Progres getProgres() {
        return progres;
    }

    public void setProgres(Progres progres) {
        this.progres = progres;
    }

    @Override
    public String add() {
        return null;
    }

    @Override
    public String edit() {
        return null;
    }

    @Override
    public String delete() {
        return null;
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        return null;
    }

    @Override
    public String search() {
        return null;
    }

    @Override
    public String initForm() {
        return null;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }
}
