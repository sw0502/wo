package com.neurix.pmsapb.master.progres.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.progres.model.Progres;

import java.util.List;

/**
 * Created by thinkpad on 23/07/2017.
 */
public interface ProgresBo extends BaseMasterBo {
    public List<Progres> getProgresByCriteria (Progres searchProgres ) throws GeneralBOException;


}
