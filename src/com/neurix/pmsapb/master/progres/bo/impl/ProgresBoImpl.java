package com.neurix.pmsapb.master.progres.bo.impl;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.progres.bo.ProgresBo;
import com.neurix.pmsapb.master.progres.dao.ProgresDao;
import com.neurix.pmsapb.master.progres.model.ImProgres;
import com.neurix.pmsapb.master.progres.model.Progres;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class ProgresBoImpl implements ProgresBo {
    protected static transient Logger logger = Logger.getLogger(ProgresBoImpl.class);

    private ProgresDao progresDao;

    public ProgresDao getProgresDao() {
        return progresDao;
    }

    public void setProgresDao(ProgresDao progresDao) {
        this.progresDao = progresDao;
    }

    @Override
    public void saveDelete(Object bean) throws GeneralBOException {

    }

    @Override
    public void saveEdit(Object bean) throws GeneralBOException {

    }

    @Override
    public Object saveAdd(Object bean) throws GeneralBOException {
        return null;
    }

    @Override
    public List getByCriteria(Object searchBean) throws GeneralBOException {
        return null;
    }

    @Override
    public List getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }

    @Override
    public List<Progres> getProgresByCriteria(Progres searchProgres) throws GeneralBOException {
        logger.info("[ProgresBoImpl.getProgresByCriteria] start process >>>");
        List<Progres> listOfResultProgres = new ArrayList<Progres>();

        if(searchProgres != null){
            String flag = searchProgres.getFlag();
            Map hsCriteria = new HashMap();

            if (searchProgres.getFlag() != null && !"".equalsIgnoreCase(searchProgres.getFlag())) {
                if ("N".equalsIgnoreCase(searchProgres.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchProgres.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }
            List<ImProgres> listOfProgres = null;
            try{
                listOfProgres = progresDao.getByCriteria(hsCriteria);
            }catch (HibernateException e){
                logger.error("[UserBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }
            if(listOfProgres != null){
                Progres resulProgres;
                for(ImProgres imProgres : listOfProgres){
                    resulProgres = new Progres();
                    resulProgres.setIdProgres(imProgres.getIdProgres());
                    resulProgres.setNamProgres(imProgres.getNamaProgres());
                    resulProgres.setFlag(imProgres.getFlag());
                    listOfResultProgres.add(resulProgres);
                }
            }

        }
        logger.info("[StatusBoImpl.getStatusByCriteria] start process >>>");
        return listOfResultProgres;
    }
}
