package com.neurix.pmsapb.master.progres.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.progres.model.ImProgres;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 23/07/2017.
 */
public class ProgresDao extends GenericDao<ImProgres,String> {
    @Override
    protected Class<ImProgres> getEntityClass() {
        return null;
    }

    @Override
    public List<ImProgres> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImProgres.class);
        if(mapCriteria != null){
            if (mapCriteria.get("progres_id")!=null) {
                criteria.add(Restrictions.eq("idProgres", (String) mapCriteria.get("progres_id")));
            }
            if (mapCriteria.get("progres_name")!=null) {
                criteria.add(Restrictions.ilike("namaProgres", "%" + (String)mapCriteria.get("progres_name") + "%"));
            }
            if (mapCriteria.get("flag")!=null) {
                criteria.add(Restrictions.ilike("flag", "%" + (String)mapCriteria.get("flag") + "%"));
            }
        }
        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));
        criteria.addOrder(Order.asc("idProgres"));
        List<ImProgres> results = criteria.list();
        return results;
    }
}
