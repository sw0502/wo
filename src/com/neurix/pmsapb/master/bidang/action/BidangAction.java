package com.neurix.pmsapb.master.bidang.action;

import com.neurix.common.action.BaseMasterAction;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.master.bidang.bo.BidangBo;
import com.neurix.pmsapb.master.bidang.model.Bidang;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class BidangAction extends BaseMasterAction {
    protected static transient Logger logger = Logger.getLogger(BidangAction.class);

    private BidangBo bidangBoProxy;
    private Bidang bidang;

    public BidangBo getBidangBoProxy() {
        return bidangBoProxy;
    }

    public void setBidangBoProxy(BidangBo bidangBoProxy) {
        this.bidangBoProxy = bidangBoProxy;
    }

    public Bidang getBidang() {
        return bidang;
    }

    public void setBidang(Bidang bidang) {
        this.bidang = bidang;
    }

    @Override
    public String add() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        return "add";
    }

    @Override
    public String edit() {
        logger.info("[BidangAction.edit] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Bidang> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (Bidang bidangOfList : listOfResult) {
                    if (id.equalsIgnoreCase(bidangOfList.getIdBidang())) {
                        setBidang(bidangOfList);
                        break;
                    }
                }
            } else {
                setBidang(new Bidang());
            }
        } else {
            setBidang(new Bidang());
        }

        return "edit";
    }

    @Override
    public String delete() {
        logger.info("[BidangAction.edit] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<Bidang> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (Bidang bidangOfList : listOfResult) {
                    if (id.equalsIgnoreCase(bidangOfList.getIdBidang())) {
                        setBidang(bidangOfList);
                        break;
                    }
                }
            } else {
                setBidang(new Bidang());
            }
        } else {
            setBidang(new Bidang());
        }

        return "delete";
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Bidang entryBidang = getBidang();
        entryBidang.setCreatedDate(updateTime);
        entryBidang.setLastUpdate(updateTime);
        entryBidang.setCreatedWho(userLogin);
        entryBidang.setLastUpdateWho(userLogin);

        String getSeqId = null;
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        BidangBo bidangBo = (BidangBo) ctx.getBean("bidangBoProxy");

        try{
            getSeqId = bidangBo.getSeqBidangId();
        }catch (GeneralBOException e){
            logger.error("Error when generate sequence id" + e);
        }

        if(getSeqId != null){
            entryBidang.setIdBidang("BID"+getSeqId);
        }

        try {
            bidangBoProxy.saveAdd(entryBidang);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = bidangBoProxy.saveErrorMessage(e.getMessage(), "BidangAction.saveAdd");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_add";
        }
        return "save_add";
    }

    public String saveEdit(){
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Bidang entryBidang = getBidang();
//        entryBidang.setCreatedDate(updateTime);
        entryBidang.setLastUpdate(updateTime);
//        entryBidang.setCreatedWho(userLogin);
        entryBidang.setLastUpdateWho(userLogin);

        try {
            bidangBoProxy.saveEdit(entryBidang);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = bidangBoProxy.saveErrorMessage(e.getMessage(), "BidangAction.saveEdit");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_edit";
        }
        return "save_edit";
    }

    public String saveDelete(){
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        Bidang entryBidang = getBidang();
//        entryBidang.setCreatedDate(updateTime);
        entryBidang.setLastUpdate(updateTime);
//        entryBidang.setCreatedWho(userLogin);
        entryBidang.setLastUpdateWho(userLogin);

        try {
            bidangBoProxy.saveDelete(entryBidang);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = bidangBoProxy.saveErrorMessage(e.getMessage(), "BidangAction.saveDelete");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_edit";
        }
        return "save_delete";
    }

    @Override
    public String search() {
        logger.info("[BidangAction.search] start process >>>");

        Bidang searchBidang = getBidang();
        List<Bidang> listOfBidang = new ArrayList();

        try {
            listOfBidang = bidangBoProxy.getByCriteria(searchBidang);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = bidangBoProxy.saveErrorMessage(e.getMessage(), "AlatBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[BelajarAction.search] Error when saving error,", e1);
            }
            logger.error("[BelajarAction.save] Error when searching alat by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin" );
            return "failure";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfBidang);

        logger.info("[BidangAction.search] end process <<<");

        return SUCCESS;
    }

    @Override
    public String initForm() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        return INPUT;
    }

    private List<Bidang> BidangList = new ArrayList<Bidang>();
    public String initComboBidang() {
        logger.info("[BranchAction.search] start process >>>");

        Bidang smkCheckList = new Bidang();
        List<Bidang> BidangList1 = new ArrayList();
        smkCheckList.setFlag("Y");
        try {
            BidangList1 = bidangBoProxy.getByCriteria(smkCheckList);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = bidangBoProxy.saveErrorMessage(e.getMessage(), "BranchBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[BranchAction.search] Error when saving error,", e1);
            }
            logger.error("[BranchAction.save] Error when searching function by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin" );
            return "failure";
        }

        BidangList.addAll(BidangList1);
        logger.info("[BranchAction.search] end process <<<");

        return SUCCESS;
    }

    public List<Bidang> getBidangList() {
        return BidangList;
    }

    public void setBidangList(List<Bidang> bidangList) {
        BidangList = bidangList;
    }

    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }
}
