package com.neurix.pmsapb.master.bidang.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.bidang.model.ImApbBidangEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class BidangDao extends GenericDao<ImApbBidangEntity, String> {

    @Override
    protected Class<ImApbBidangEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ImApbBidangEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImApbBidangEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("bidang_id")!=null) {
                criteria.add(Restrictions.eq("idBidang", (String) mapCriteria.get("bidang_id")));
            }
            if (mapCriteria.get("bidang_name")!=null) {
                criteria.add(Restrictions.ilike("namaBidang", "%" + (String)mapCriteria.get("bidang_name") + "%"));
            }
        }
        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));

        // Order by
        criteria.addOrder(Order.asc("idBidang"));
        List<ImApbBidangEntity> results = criteria.list();
        return results;
    }

    public String getNextId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_bidang" + "')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;

    }
}
