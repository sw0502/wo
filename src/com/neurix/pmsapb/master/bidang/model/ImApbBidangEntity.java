package com.neurix.pmsapb.master.bidang.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class ImApbBidangEntity implements Serializable {
    private String idBidang;
    private String namaBidang;
    private String flag;
    private String Action;
//    private Timestamp CreatedDate;
//    private String CreatedWho;
//    private Timestamp lastUpdate;
//    private String lastUpdateWho;

    public String getIdBidang() {
        return idBidang;
    }

    public void setIdBidang(String idBidang) {
        this.idBidang = idBidang;
    }

    public String getNamaBidang() {
        return namaBidang;
    }

    public void setNamaBidang(String namaBidang) {
        this.namaBidang = namaBidang;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }

//    public Timestamp getCreatedDate() {
//        return CreatedDate;
//    }
//
//    public void setCreatedDate(Timestamp createdDate) {
//        CreatedDate = createdDate;
//    }
//
//    public String getCreatedWho() {
//        return CreatedWho;
//    }
//
//    public void setCreatedWho(String createdWho) {
//        CreatedWho = createdWho;
//    }
//
//    public Timestamp getLastUpdate() {
//        return lastUpdate;
//    }
//
//    public void setLastUpdate(Timestamp lastUpdate) {
//        this.lastUpdate = lastUpdate;
//    }
//
//    public String getLastUpdateWho() {
//        return lastUpdateWho;
//    }
//
//    public void setLastUpdateWho(String lastUpdateWho) {
//        this.lastUpdateWho = lastUpdateWho;
//    }
}
