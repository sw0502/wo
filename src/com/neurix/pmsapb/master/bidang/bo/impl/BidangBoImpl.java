package com.neurix.pmsapb.master.bidang.bo.impl;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.bidang.bo.BidangBo;
import com.neurix.pmsapb.master.bidang.dao.BidangDao;
import com.neurix.pmsapb.master.bidang.model.Bidang;
import com.neurix.pmsapb.master.bidang.model.ImApbBidangEntity;
import org.hibernate.HibernateException;
import org.apache.log4j.Logger;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class BidangBoImpl implements BidangBo {
    protected static transient Logger logger = Logger.getLogger(BidangBoImpl.class);
    private BidangDao bidangDao;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        BidangBoImpl.logger = logger;
    }

    public BidangDao getBidangDao() {
        return bidangDao;
    }

    public void setBidangDao(BidangDao bidangDao) {
        this.bidangDao = bidangDao;
    }

    @Override
    public void saveDelete(Bidang bean) throws GeneralBOException {
        if (bean!=null){
            ImApbBidangEntity bidangEntity = new ImApbBidangEntity();
            bidangEntity.setIdBidang(bean.getIdBidang());
            bidangEntity.setNamaBidang(bean.getNamaBidang());
//            bidangEntity.setCreatedDate(bean.getCreatedDate());
//            bidangEntity.setCreatedWho(bean.getCreatedWho());
//            bidangEntity.setLastUpdate(bean.getLastUpdate());
//            bidangEntity.setLastUpdateWho(bean.getLastUpdateWho());
            bidangEntity.setFlag("N");
            bidangEntity.setAction("U");
            try {
                bidangDao.updateAndSave(bidangEntity);
            }catch (HibernateException e){
                logger.error("[BidangBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when Delete data Bidang, please info to your admin..." + e.getMessage());
            }
        }

    }

    @Override
    public void saveEdit(Bidang bean) throws GeneralBOException {
        if (bean!=null){
            ImApbBidangEntity bidangEntity = new ImApbBidangEntity();
            bidangEntity.setIdBidang(bean.getIdBidang());
            bidangEntity.setNamaBidang(bean.getNamaBidang());
//            bidangEntity.setCreatedDate(bean.getCreatedDate());
//            bidangEntity.setCreatedWho(bean.getCreatedWho());
//            bidangEntity.setLastUpdate(bean.getLastUpdate());
//            bidangEntity.setLastUpdateWho(bean.getLastUpdateWho());
            bidangEntity.setFlag(bean.getFlag());
            bidangEntity.setAction("U");
            try {
                bidangDao.updateAndSave(bidangEntity);
            }catch (HibernateException e){
                logger.error("[BidangBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when Update data Bidang, please info to your admin..." + e.getMessage());
            }
        }
    }

    @Override
    public Bidang saveAdd(Bidang bean) throws GeneralBOException {
        if (bean!=null){
            ImApbBidangEntity bidangEntity = new ImApbBidangEntity();
            bidangEntity.setIdBidang(bean.getIdBidang());
            bidangEntity.setNamaBidang(bean.getNamaBidang());
//            bidangEntity.setCreatedDate(bean.getCreatedDate());
//            bidangEntity.setCreatedWho(bean.getCreatedWho());
//            bidangEntity.setLastUpdate(bean.getLastUpdate());
//            bidangEntity.setLastUpdateWho(bean.getLastUpdateWho());
            bidangEntity.setFlag("Y");
            bidangEntity.setAction("C");
            try {
                bidangDao.addAndSave(bidangEntity);
            }catch (HibernateException e){
                logger.error("[BidangBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when Add data Bidang, please info to your admin..." + e.getMessage());
            }
        }
        return null;
    }

    @Override
    public List<Bidang> getByCriteria(Bidang searchBean) throws GeneralBOException {
        logger.info("[BidangBoImpl.getByCriteria] start process >>>");

        // Mapping with collection and put
        List<Bidang> listOfResult = new ArrayList();

        if (searchBean != null) {
            Map hsCriteria = new HashMap();

            if (searchBean.getIdBidang() != null && !"".equalsIgnoreCase(searchBean.getIdBidang())) {
                hsCriteria.put("bidang_id", searchBean.getIdBidang());
            }
            if (searchBean.getNamaBidang() != null && !"".equalsIgnoreCase(searchBean.getNamaBidang())) {
                hsCriteria.put("bidang_name", searchBean.getNamaBidang());
            }
            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            List<ImApbBidangEntity> imApbBidangEntities = null;
            try {
                imApbBidangEntities = bidangDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[BidangBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if(imApbBidangEntities != null){
                Bidang returnBidang;
                // Looping from dao to object and save in collection
                for(ImApbBidangEntity bidangEntity : imApbBidangEntities){
                    returnBidang = new Bidang();
                    returnBidang.setIdBidang(bidangEntity.getIdBidang());
                    returnBidang.setNamaBidang(bidangEntity.getNamaBidang());
                    returnBidang.setFlag(bidangEntity.getFlag());
                    returnBidang.setAction(bidangEntity.getAction());
                    listOfResult.add(returnBidang);
                }
            }
        }
        logger.info("[BidangBoImpl.getByCriteria] end process <<<");
        return listOfResult;
    }

    @Override
    public List<Bidang> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }

    @Override
    public String getSeqBidangId() throws GeneralBOException {
        String id = null;
        try {
            id=bidangDao.getNextId();
        } catch (HibernateException e) {
            logger.error("[BidangBoImpl.getNextId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }
        return id;
    }
}
