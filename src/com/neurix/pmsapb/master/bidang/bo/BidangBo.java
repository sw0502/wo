package com.neurix.pmsapb.master.bidang.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.bidang.model.Bidang;

/**
 * Created by thinkpad on 15/11/2017.
 */
public interface BidangBo extends BaseMasterBo<Bidang>{
    public String getSeqBidangId() throws GeneralBOException;

}
