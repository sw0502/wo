package com.neurix.pmsapb.master.subbidang.dao;

import com.neurix.common.dao.GenericDao;
import com.neurix.pmsapb.master.subbidang.model.ImSubBidangEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class SubBidangDao extends GenericDao<ImSubBidangEntity, String> {

    @Override
    protected Class<ImSubBidangEntity> getEntityClass() {
        return null;
    }

    @Override
    public List<ImSubBidangEntity> getByCriteria(Map mapCriteria) {
        Criteria criteria=this.sessionFactory.getCurrentSession().createCriteria(ImSubBidangEntity.class);
        if (mapCriteria!=null) {
            if (mapCriteria.get("sub_bidang_id")!=null) {
                criteria.add(Restrictions.ilike("idSubBidang", (String) mapCriteria.get("sub_bidang_id")));
            }
            if (mapCriteria.get("sub_bidang_name")!=null) {
                criteria.add(Restrictions.ilike("namaSubBidang", "%" + (String)mapCriteria.get("sub_bidang_name") + "%"));
            }

        }
        criteria.add(Restrictions.eq("flag", mapCriteria.get("flag")));

        // Order by
        criteria.addOrder(Order.asc("idSubBidang"));
        List<ImSubBidangEntity> results = criteria.list();
        return results;
    }

    public String getNextId() throws HibernateException {
        Query query = this.sessionFactory.getCurrentSession().createSQLQuery("select nextval ('seq_sub_bidang" + "')");
        Iterator<BigInteger> iter=query.list().iterator();
        String sId = String.format("%05d", iter.next());
        return sId;

    }
}
