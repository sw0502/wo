package com.neurix.pmsapb.master.subbidang.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class ImSubBidangEntity implements Serializable {
    private String idSubBidang;
    private String namaSubBidang;
    private String idBidang;
    private String flag;
    private String Action;

    public String getIdSubBidang() {
        return idSubBidang;
    }

    public void setIdSubBidang(String idSubBidang) {
        this.idSubBidang = idSubBidang;
    }

    public String getNamaSubBidang() {
        return namaSubBidang;
    }

    public void setNamaSubBidang(String namaSubBidang) {
        this.namaSubBidang = namaSubBidang;
    }

    public String getIdBidang() {
        return idBidang;
    }

    public void setIdBidang(String idBidang) {
        this.idBidang = idBidang;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }
}
