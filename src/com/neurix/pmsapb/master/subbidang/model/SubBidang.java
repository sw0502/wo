package com.neurix.pmsapb.master.subbidang.model;

import com.neurix.common.model.BaseModel;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class SubBidang extends BaseModel implements Serializable {
    private String idSubBidang;
    private String namaSubBidang;
    private String idBidang;

    public String getIdSubBidang() {
        return idSubBidang;
    }

    public void setIdSubBidang(String idSubBidang) {
        this.idSubBidang = idSubBidang;
    }

    public String getNamaSubBidang() {
        return namaSubBidang;
    }

    public void setNamaSubBidang(String namaSubBidang) {
        this.namaSubBidang = namaSubBidang;
    }

    public String getIdBidang() {
        return idBidang;
    }

    public void setIdBidang(String idBidang) {
        this.idBidang = idBidang;
    }


}

