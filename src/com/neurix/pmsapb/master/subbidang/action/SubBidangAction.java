package com.neurix.pmsapb.master.subbidang.action;

import com.neurix.common.action.BaseMasterAction;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.common.util.CommonUtil;
import com.neurix.pmsapb.master.subbidang.bo.SubBidangBo;
import com.neurix.pmsapb.master.subbidang.model.SubBidang;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class SubBidangAction extends BaseMasterAction {
    protected static transient Logger logger = Logger.getLogger(SubBidangAction.class);

    private SubBidangBo subbidangBoProxy;
    private SubBidang subBidang;

    public SubBidangBo getSubBidangBoProxy() {
        return subbidangBoProxy;
    }

    public void setSubBidangBoProxy(SubBidangBo subbidangBoProxy) {
        this.subbidangBoProxy = subbidangBoProxy;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        SubBidangAction.logger = logger;
    }

    public SubBidangBo getSubbidangBoProxy() {
        return subbidangBoProxy;
    }

    public void setSubbidangBoProxy(SubBidangBo subbidangBoProxy) {
        this.subbidangBoProxy = subbidangBoProxy;
    }

    public SubBidang getSubBidang() {
        return subBidang;
    }

    public void setSubBidang(SubBidang subBidang) {
        this.subBidang = subBidang;
    }

    @Override
    public String add() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        return "add";
    }

    @Override
    public String edit() {
        logger.info("[SubBidangAction.edit] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<SubBidang> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (SubBidang subbidangOfList : listOfResult) {
                    if (id.equalsIgnoreCase(subbidangOfList.getIdSubBidang())) {
                        setSubBidang(subbidangOfList);
                        break;
                    }
                }
            } else {
                setSubBidang(new SubBidang());
            }
        } else {
            setSubBidang(new SubBidang());
        }

        return "edit";
    }

    @Override
    public String delete() {
        logger.info("[SubBidangAction.edit] start process >>>");
        HttpSession session = ServletActionContext.getRequest().getSession();
        List<SubBidang> listOfResult = (List) session.getAttribute("listOfResult");

        if (id != null && !"".equalsIgnoreCase(id)) {

            if (listOfResult != null) {

                for (SubBidang subbidangOfList : listOfResult) {
                    if (id.equalsIgnoreCase(subbidangOfList.getIdSubBidang())) {
                        setSubBidang(subbidangOfList);
                        break;
                    }
                }
            } else {
                setSubBidang(new SubBidang());
            }
        } else {
            setSubBidang(new SubBidang());
        }

        return "delete";
    }

    @Override
    public String view() {
        return null;
    }

    @Override
    public String save() {
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        SubBidang entrySubBidang = getSubBidang();
        entrySubBidang.setCreatedDate(updateTime);
        entrySubBidang.setLastUpdate(updateTime);
        entrySubBidang.setCreatedWho(userLogin);
        entrySubBidang.setLastUpdateWho(userLogin);

        String getSeqId = null;
        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        SubBidangBo subbidangBo = (SubBidangBo) ctx.getBean("subbidangBoProxy");

        try{
            getSeqId = subbidangBo.getSeqSubBidangId();
        }catch (GeneralBOException e){
            logger.error("Error when generate sequence id" + e);
        }

        if(getSeqId != null){
            entrySubBidang.setIdSubBidang("SUB"+getSeqId);
        }

        try {
            subbidangBoProxy.saveAdd(entrySubBidang);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = subbidangBoProxy.saveErrorMessage(e.getMessage(), "SubBidangAction.saveAdd");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_add";
        }
        return "save_add";
    }

    public String saveEdit(){
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        SubBidang entrySubBidang = getSubBidang();
//        entrySubBidang.setCreatedDate(updateTime);
        entrySubBidang.setLastUpdate(updateTime);
//        entrySubBidang.setCreatedWho(userLogin);
        entrySubBidang.setLastUpdateWho(userLogin);

        try {
            subbidangBoProxy.saveEdit(entrySubBidang);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = subbidangBoProxy.saveErrorMessage(e.getMessage(), "SubBidangAction.saveEdit");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_edit";
        }
        return "save_edit";
    }

    public String saveDelete(){
        String userLogin = CommonUtil.userLogin();
        Timestamp updateTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        SubBidang entrySubBidang = getSubBidang();
//        entrySubBidang.setCreatedDate(updateTime);
        entrySubBidang.setLastUpdate(updateTime);
//        entrySubBidang.setCreatedWho(userLogin);
        entrySubBidang.setLastUpdateWho(userLogin);

        try {
            subbidangBoProxy.saveDelete(entrySubBidang);
        }catch (GeneralBOException e){
            Long logId = null;
            try {
                logId = subbidangBoProxy.saveErrorMessage(e.getMessage(), "SubBidangAction.saveDelete");
            } catch (GeneralBOException e1) {
                logger.error("[AlatDetailAction.saveAdd] Error when saving error,", e1);
            }
            logger.error("[AlatDetailAction.saveAdd] Error when searching / inquiring data by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching / inquiring  data by criteria, please inform to your admin. Cause : " + e.getMessage());
            return "failure_save_edit";
        }
        return "save_delete";
    }

    @Override
    public String search() {
        logger.info("[SubBidangAction.search] start process >>>");

        SubBidang searchSubBidang = getSubBidang();
        List<SubBidang> listOfSubBidang = new ArrayList();

        try {
            listOfSubBidang = subbidangBoProxy.getByCriteria(searchSubBidang);
        } catch (GeneralBOException e) {
            Long logId = null;
            try {
                logId = subbidangBoProxy.saveErrorMessage(e.getMessage(), "AlatBO.getByCriteria");
            } catch (GeneralBOException e1) {
                logger.error("[BelajarAction.search] Error when saving error,", e1);
            }
            logger.error("[BelajarAction.save] Error when searching alat by criteria," + "[" + logId + "] Found problem when searching data by criteria, please inform to your admin.", e);
            addActionError("Error, " + "[code=" + logId + "] Found problem when searching data by criteria, please inform to your admin" );
            return "failure";
        }

        HttpSession session = ServletActionContext.getRequest().getSession();

        session.removeAttribute("listOfResult");
        session.setAttribute("listOfResult", listOfSubBidang);

        logger.info("[SubBidangAction.search] end process <<<");

        return SUCCESS;
    }

    @Override
    public String initForm() {
        HttpSession session = ServletActionContext.getRequest().getSession();
        session.removeAttribute("listOfResult");
        return INPUT;
    }


    @Override
    public String downloadPdf() {
        return null;
    }

    @Override
    public String downloadXls() {
        return null;
    }
}
