package com.neurix.pmsapb.master.subbidang.bo.impl;

import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.subbidang.bo.SubBidangBo;
import com.neurix.pmsapb.master.subbidang.dao.SubBidangDao;
import com.neurix.pmsapb.master.subbidang.model.SubBidang;
import com.neurix.pmsapb.master.subbidang.model.ImSubBidangEntity;
import org.hibernate.HibernateException;
import org.apache.log4j.Logger;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 15/11/2017.
 */
public class SubBidangBoImpl implements SubBidangBo {
    protected static transient Logger logger = Logger.getLogger(SubBidangBoImpl.class);
    private SubBidangDao subbidangDao;

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        SubBidangBoImpl.logger = logger;
    }

    public SubBidangDao getSubBidangDao() {
        return subbidangDao;
    }

    public void setSubBidangDao(SubBidangDao subbidangDao) {
        this.subbidangDao = subbidangDao;
    }

    @Override
    public void saveDelete(SubBidang bean) throws GeneralBOException {
        if (bean!=null){
            ImSubBidangEntity subbidangEntity = new ImSubBidangEntity();
            subbidangEntity.setIdSubBidang(bean.getIdSubBidang());
            subbidangEntity.setNamaSubBidang(bean.getNamaSubBidang());
            subbidangEntity.setIdBidang(bean.getIdBidang());
            subbidangEntity.setFlag("N");
            subbidangEntity.setAction("U");
            try {
                subbidangDao.updateAndSave(subbidangEntity);
            }catch (HibernateException e){
                logger.error("[SubBidangBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when Delete data SubBidang, please info to your admin..." + e.getMessage());
            }
        }

    }

    @Override
    public void saveEdit(SubBidang bean) throws GeneralBOException {
        if (bean!=null){
            ImSubBidangEntity subbidangEntity = new ImSubBidangEntity();
            subbidangEntity.setIdSubBidang(bean.getIdSubBidang());
            subbidangEntity.setNamaSubBidang(bean.getNamaSubBidang());
            subbidangEntity.setIdBidang(bean.getIdBidang());
            subbidangEntity.setFlag(bean.getFlag());
            subbidangEntity.setAction("U");
            try {
                subbidangDao.updateAndSave(subbidangEntity);
            }catch (HibernateException e){
                logger.error("[SubBidangBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when Update data SubBidang, please info to your admin..." + e.getMessage());
            }
        }
    }

    @Override
    public SubBidang saveAdd(SubBidang bean) throws GeneralBOException {
        if (bean!=null){
            ImSubBidangEntity subbidangEntity = new ImSubBidangEntity();
            subbidangEntity.setIdSubBidang(bean.getIdSubBidang());
            subbidangEntity.setNamaSubBidang(bean.getNamaSubBidang());
            subbidangEntity.setIdBidang(bean.getIdBidang());
            subbidangEntity.setFlag("Y");
            subbidangEntity.setAction("C");
            try {
                subbidangDao.addAndSave(subbidangEntity);
            }catch (HibernateException e){
                logger.error("[SubBidangBoImpl.saveAdd] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when Add data SubBidang, please info to your admin..." + e.getMessage());
            }
        }
        return null;
    }

    @Override
    public List<SubBidang> getByCriteria(SubBidang searchBean) throws GeneralBOException {
        logger.info("[SubBidangBoImpl.getByCriteria] start process >>>");

        // Mapping with collection and put
        List<SubBidang> listOfResult = new ArrayList();

        if (searchBean != null) {
            Map hsCriteria = new HashMap();

            if (searchBean.getIdSubBidang() != null && !"".equalsIgnoreCase(searchBean.getIdSubBidang())) {
                hsCriteria.put("sub_bidang_id", searchBean.getIdSubBidang());
            }
            if (searchBean.getNamaSubBidang() != null && !"".equalsIgnoreCase(searchBean.getNamaSubBidang())) {
                hsCriteria.put("sub_bidang_name", searchBean.getNamaSubBidang());
            }
            if (searchBean.getFlag() != null && !"".equalsIgnoreCase(searchBean.getFlag())) {
                if ("N".equalsIgnoreCase(searchBean.getFlag())) {
                    hsCriteria.put("flag", "N");
                } else {
                    hsCriteria.put("flag", searchBean.getFlag());
                }
            } else {
                hsCriteria.put("flag", "Y");
            }

            List<ImSubBidangEntity> imSubBidangEntities = null;
            try {
                imSubBidangEntities = subbidangDao.getByCriteria(hsCriteria);
            } catch (HibernateException e) {
                logger.error("[SubBidangBoImpl.getByCriteria] Error, " + e.getMessage());
                throw new GeneralBOException("Found problem when searching data by criteria, please info to your admin..." + e.getMessage());
            }

            if(imSubBidangEntities != null){
                SubBidang returnSubBidang;
                // Looping from dao to object and save in collection
                for(ImSubBidangEntity subbidangEntity : imSubBidangEntities){
                    returnSubBidang = new SubBidang();
                    returnSubBidang.setIdSubBidang(subbidangEntity.getIdSubBidang());
                    returnSubBidang.setNamaSubBidang(subbidangEntity.getNamaSubBidang());
                    returnSubBidang.setIdBidang(subbidangEntity.getIdBidang());
                    returnSubBidang.setFlag(subbidangEntity.getFlag());
                    returnSubBidang.setAction(subbidangEntity.getAction());
                    listOfResult.add(returnSubBidang);
                }
            }
        }
        logger.info("[SubBidangBoImpl.getByCriteria] end process <<<");
        return listOfResult;
    }

    @Override
    public List<SubBidang> getAll() throws GeneralBOException {
        return null;
    }

    @Override
    public Long saveErrorMessage(String message, String moduleMethod) throws GeneralBOException {
        return null;
    }

    @Override
    public String getSeqSubBidangId() throws GeneralBOException {
        String id = null;
        try {
            id=subbidangDao.getNextId();
        } catch (HibernateException e) {
            logger.error("[SubBidangBoImpl.getNextId] Error, " + e.getMessage());
            throw new GeneralBOException("Found problem when getting sequence seq num lahan id, please info to your admin..." + e.getMessage());
        }
        return id;
    }
}
