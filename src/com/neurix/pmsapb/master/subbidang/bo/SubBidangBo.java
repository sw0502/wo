package com.neurix.pmsapb.master.subbidang.bo;

import com.neurix.common.bo.BaseMasterBo;
import com.neurix.common.exception.GeneralBOException;
import com.neurix.pmsapb.master.subbidang.model.SubBidang;

/**
 * Created by thinkpad on 15/11/2017.
 */
public interface SubBidangBo extends BaseMasterBo<SubBidang>{
    public String getSeqSubBidangId() throws GeneralBOException;

}
